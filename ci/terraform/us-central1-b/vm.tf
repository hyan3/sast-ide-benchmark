provider "google" {
  project     = var.project_id
}

resource "google_compute_instance" "us-central1-b" {
  name         = "us-central1-b"
  zone         = "us-central1-b"
  machine_type = "e2-medium"

  tags = ["allow-sast-srv-us-central1-b"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Include this section to allow external IP
    }
  }

  metadata_startup_script = templatefile("../startup.sh.tpl", {
      image = var.image,
    })
}

resource "google_compute_firewall" "allow_sast_srv-us-central1-b" {
  name    = "allow-sast-srv-us-central1-b"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["8080"]
  }

  target_tags = ["allow-sast-srv-us-central1-b"]

  source_ranges = ["0.0.0.0/0"]
}

variable "project_id" {
  description = "GCP Project ID"
  type        = string
}

variable "image" {
  description = "Server image"
  type        = string
}

output "instance_ip" {
  value = google_compute_instance.us-central1-b.network_interface[0].access_config[0].nat_ip
  description = "The external IP address of the instance"
}

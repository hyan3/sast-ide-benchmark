provider "google" {
  project     = var.project_id
}

resource "google_compute_instance" "europe-west3-a" {
  name         = "europe-west3-a"
  zone         = "europe-west3-a"
  machine_type = "e2-medium"

  tags = ["allow-sast-srv-europe-west3-a"]

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-12"
    }
  }

  network_interface {
    network = "default"
    access_config {
      // Include this section to allow external IP
    }
  }

  metadata_startup_script = templatefile("../startup.sh.tpl", {
      image = var.image,
    })
}

resource "google_compute_firewall" "allow_sast_srv-europe-west3-a" {
  name    = "allow-sast-srv-europe-west3-a"
  network = "default"

  allow {
    protocol = "tcp"
    ports    = ["8080"]
  }

  target_tags = ["allow-sast-srv-europe-west3-a"]

  source_ranges = ["0.0.0.0/0"]
}

variable "project_id" {
  description = "GCP Project ID"
  type        = string
}

variable "image" {
  description = "Server image"
  type        = string
}

output "instance_ip" {
  value = google_compute_instance.europe-west3-a.network_interface[0].access_config[0].nat_ip
  description = "The external IP address of the instance"
}

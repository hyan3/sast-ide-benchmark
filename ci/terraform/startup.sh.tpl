#!/bin/bash

echo "Updating package list..." | tee -a /var/log/docker_install.log
sudo apt-get update &>> /var/log/docker_install.log

echo "Downloading Docker install script..." | tee -a /var/log/docker_install.log
curl -fsSL https://get.docker.com -o get-docker.sh &>> /var/log/docker_install.log

echo "Installing Docker..." | tee -a /var/log/docker_install.log
sudo sh get-docker.sh &>> /var/log/docker_install.log

echo "Starting Docker service..." | tee -a /var/log/docker_install.log
sudo systemctl start docker &>> /var/log/docker_install.log

echo "Enabling Docker service to start on boot..." | tee -a /var/log/docker_install.log
sudo systemctl enable docker &>> /var/log/docker_install.log

echo "Adding user $(whoami) to Docker group..." | tee -a /var/log/docker_install.log
sudo usermod -aG docker $(whoami) &>> /var/log/docker_install.log

echo "Applying new group membership..." | tee -a /var/log/docker_install.log
newgrp docker &>> /var/log/docker_install.log

echo "Pulling Docker image ${image}..." | tee -a /var/log/docker_install.log
docker pull ${image} &>> /var/log/docker_install.log

echo "Tagging Docker image ${image} as sast-ide-srv:latest..." | tee -a /var/log/docker_install.log
docker tag ${image} sast-ide-srv:latest &>> /var/log/docker_install.log

echo "Running Docker container..." | tee -a /var/log/docker_install.log
docker run --restart=always -d --network host sast-ide-srv &>> /var/log/docker_install.log

#!/usr/bin/env bash
# Usage: wait_for_host.sh URL [max_retries [delay] ]

URL=$1
max_retries=${2:-60}
delay=${3:-5}

echo "Checking if ${URL} is reachable..."
retry_count=0
while true; do
  if curl -m 3 -fs -o /dev/null $URL; then
    echo "${URL} is now reachable."
    exit 0
  fi
  if [ "$retry_count" -ge "$max_retries" ]; then
    exit 1
  fi
  retry_count=$((retry_count + 1))
  echo "${URL} is NOT reachable. Retrying in $delay seconds... ($retry_count/$max_retries)"
  sleep $delay
done

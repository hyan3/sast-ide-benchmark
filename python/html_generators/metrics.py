import io
import json
import sys
from pathlib import Path
from typing import Any, ClassVar, TextIO


class Stats:
    name: str
    values: list[dict[str, Any]]
    row2key: dict[str, str]

    HEADER: ClassVar[
        str
    ] = """
    <tr>
        <th>Metric</th>
        <th>Mean</th>
        <th>Std</th>
    </tr>
    """

    def __init__(
        self, name: str, row2key: dict[str, str], values: list[dict[str, Any]]
    ):
        self.name = name
        self.values = values
        self.row2key = row2key

    def write(self, out: TextIO):
        out.write(f"<h3>{self.name}</h3>")
        out.write("""<table border="1">""")
        out.write(self.HEADER)
        for row, key in self.row2key.items():
            l = [float(v.get(key, 0.0)) for v in self.values]
            mean = sum(l) / len(l)
            std = (sum((x - mean) ** 2 for x in l) / len(l)) ** 0.5
            out.write(
                f"""
            <tr>
                <td>{row}</td>
                <td>{mean:.3f}</td>
                <td>{std:.3f}</td>
            </tr>
            """
            )
        out.write("</table>")


def write_stats(out: TextIO, benchmark_data: list[Any]):
    out.write("<h2>Benchmark Metrics</h2>\n")

    io_stats = Stats(
        "IO Stats",
        {
            "Read (bytes)": "ReadBytes",
            "Write (bytes)": "WriteBytes",
            "IO Read (op count)": "IOReadCount",
            "IO Write (op count)": "IOWriteCount",
        },
        [d.get("io_stats", {}) for d in benchmark_data],
    )
    io_stats.write(out)

    mem_stats = Stats(
        "Memory and Time Stats",
        {
            "Memory Used (bytes)": "mem_used",
            "Round Trip (seconds)": "duration",
            "Real Time (seconds)": "real_time",
            "System Time (seconds)": "sys_time",
            "User Time (seconds)": "usr_time",
        },
        benchmark_data,
    )
    mem_stats.write(out)


def gen(output_html_file: Path, input_json_files: list[Path]):
    out = io.StringIO()
    benchmark_data = []

    # Load benchmark data from each JSON file
    for input_json_file in input_json_files:
        if input_json_file.exists():
            try:
                with input_json_file.open("r") as file:
                    data = json.load(file)
                    benchmark_data.append(data["benchmark"])
            except json.JSONDecodeError:
                print(f"Warning: {input_json_file} is not a valid JSON file. Skipping.")
            except Exception as e:
                print(f"An error occurred while processing {input_json_file}: {e}")
        else:
            print(f"Warning: {input_json_file} is not a valid file. Skipping.")

    if not benchmark_data:
        print("No valid benchmark data to process.")
        sys.exit(1)

    # Calculate and generate the HTML content
    out.write(
        f"""<!DOCTYPE html>
<html lang='en'>
<head>
    <meta charset='UTF-8'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Benchmark Metrics</title>
   <style>
        td:first-child {{text-align: left;}}
        td {{text-align: right;}}
        th:first-child {{text-align: left;}}
        th {{text-align: right;}}
        th, td {{
          padding: 5px;
        }}
    </style> 
</head>
<body>
"""
    )
    write_stats(out, benchmark_data)
    out.write(
        """
</body>
</html>
    """
    )

    # Write HTML content to the specified output file
    with output_html_file.open("w") as file:
        file.write(out.getvalue())

    print(f"HTML file generated: {output_html_file}")

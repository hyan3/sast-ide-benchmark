import html
import io
import json
from pathlib import Path
from typing import Any, TextIO


def write_row(out: TextIO, filename: str, json_data: dict[str, Any]):
    filename = html.escape(filename)
    for vulnerability in json_data.get("vulnerabilities", []):
        start_line = html.escape(
            str(vulnerability.get("location", {}).get("start_line", "N/A"))
        )
        start_col = html.escape(
            str(vulnerability.get("location", {}).get("start_col", "N/A"))
        )
        end_line = html.escape(
            str(vulnerability.get("location", {}).get("end_line", "N/A"))
        )
        end_col = html.escape(
            str(vulnerability.get("location", {}).get("end_col", "N/A"))
        )

        name = html.escape(vulnerability.get("name", "N/A"))
        description = html.escape(vulnerability.get("description", "N/A"))
        severity = html.escape(str(vulnerability.get("severity", "N/A")))

        out.write(
            f"""
        <tr>
          <td style="font-weight: bold; background-color: yellow;">{filename}, Line: {start_line}</td>
          <td style="font-size: 12px;">{name}</td>
          <td style="font-size: 10px;">{description}</td>
          <td style="font-size: 12px;">{severity}</td>
          <td style="font-size: 12px;">Start Line: {start_line}, Start Column: {start_col}, End Line: {end_line}, End Column: {end_col}</td>
        </tr>
        """
        )


def gen(output_html_file: Path, input_json_files: list[Path]):
    out = io.StringIO()
    out.write(
        """
    <html>
    <head>
        <title>Scan Results</title>
        <style>
            table {
                border-collapse: collapse;
                width: 100%;
            }
            th, td {
                border: 1px solid black;
                padding: 8px;
                text-align: left;
            }
            th {
                background-color: #f2f2f2;
            }
        </style>
    </head>
    <body>
    <h1>Scan Results</h1>
    <table>
      <tr>
        <th>File and Start Line</th>
        <th>Name</th>
        <th>Description</th>
        <th>Severity</th>
        <th>Location</th>
      </tr>
    """
    )

    for input_json_file in input_json_files:
        if input_json_file.is_file():
            try:
                with input_json_file.open("r") as f:
                    write_row(
                        out,
                        input_json_file.stem.replace("result-", ""),
                        json.load(f),
                    )
            except json.JSONDecodeError:
                print(f"Warning: {input_json_file} is not a valid JSON file. Skipping.")
            except Exception as e:
                print(f"An error occurred while processing {input_json_file}: {e}")
        else:
            print(f"Warning: {input_json_file} is not a valid file. Skipping.")

    out.write(
        """
    </table>
    </body>
    </html>
    """
    )

    try:
        with output_html_file.open("w") as f:
            f.write(out.getvalue())
    except Exception as e:
        print(f"An error occurred while writing to {output_html_file}: {e}")

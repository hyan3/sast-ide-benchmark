#!/usr/bin/env python3

import argparse
import json
import time
from pathlib import Path
from typing import Any

import requests
from html_generators.metrics import gen as gen_metrics
from html_generators.vulns import gen as gen_vulns

DEFAULT_PORT = 8080
DEFAULT_LOCATION = "local"
DEFAULT_KEEP_TEMP_FILE = False


class BenchmarkRunner:
    host: str
    port: int
    project_dir: Path
    location: str
    test_dir: Path
    test_max: str
    temp_dir: Path
    keep_temp_file: bool

    session: requests.Session

    def __init__(self, args: argparse.Namespace):
        self.host = args.host
        self.port = args.port
        self.project_dir = args.project_dir
        self.location = args.location
        self.test_dir = args.test_dir
        self.test_max = args.test_max
        self.temp_dir = args.temp_dir
        self.keep_temp_file = args.keep_temp_file

        self.session = requests.Session()
        if args.token is not None:
            self.authenticate(args.token)

    def authenticate(self, token: str):
        """
        Update session with authentication cookie by authenticating with token.
        """
        url = f"http://{self.host}:{self.port}/authenticate"
        payload = json.dumps({"token": token})
        headers = {"Content-Type": "application/json"}

        response = self.session.post(url, data=payload, headers=headers)
        response.raise_for_status()

    def scan(self, sample: Path) -> Any:
        payload = json.dumps({"file_path": str(sample), "content": sample.read_text()})
        url = f"http://{self.host}:{self.port}/scan?benchmark=true"
        headers = {"Content-Type": "application/json"}

        start_time = time.time()
        response = self.session.post(url, data=payload, headers=headers)
        end_time = time.time()
        duration = end_time - start_time
        response.raise_for_status()
        result = response.json()
        result["benchmark"]["duration"] = str(duration)
        return result

    def run(self):
        total_time = 0
        file_count = 0
        total_size = 0
        json_files: list[Path] = []

        for sample in sorted(self.test_dir.glob("*"))[: self.test_max]:
            print(f"Sending {sample.name} to remote server to scan.")
            result = self.scan(sample)

            vuln_count = len(result.get("vulnerabilities", []))
            print(f"Vulnerability Count {vuln_count}")

            json_file = self.temp_dir / f"result-{sample.name}.json"
            with json_file.open("w") as out:
                json.dump(result, out, indent=2)
            json_files.append(json_file)

            duration = float(result["benchmark"]["duration"])
            print(f"Duration for {sample.name}: {duration:.3f} seconds")

            total_time += duration
            file_count += 1
            file_size = sample.stat().st_size
            total_size += file_size

        metrics_path = self.project_dir / f"metrics-{self.location}.json"
        if file_count > 0:
            print("========Overall==============")
            avg_time = total_time / file_count
            avg_size = total_size / file_count
            print(f"Average time taken per file: {avg_time:.3f} seconds")
            print(f"Average size of files: {avg_size} bytes")
            metrics = {
                "average_time_per_file": avg_time,
                "average_size_of_files": avg_size,
            }
            with metrics_path.open("w") as out:
                json.dump(metrics, out)
        else:
            print("No files found to analyze.")
            with metrics_path.open("w") as out:
                json.dump({"message": "No files found to analyze."}, out)

        gen_vulns(self.project_dir / f"vulns-{self.location}.html", json_files)
        gen_metrics(self.project_dir / f"metrics-{self.location}.html", json_files)

        if not self.keep_temp_file:
            for json_file in json_files:
                json_file.unlink()


if __name__ == "__main__":

    parser = argparse.ArgumentParser(
        description="Run benchmarks for SAST scanner service"
    )
    parser.add_argument(
        "--host",
        help="Host of the SAST scanner service",
        required=True,
    )
    parser.add_argument(
        "--port",
        type=int,
        help="Port of the SAST scanner service",
        default=DEFAULT_PORT,
    )
    parser.add_argument(
        "--project-dir",
        type=Path,
        help="Project directory",
        default=Path("."),
    )
    parser.add_argument(
        "--location",
        help="Location of the backend sast-scanner-service allocated by terraform. Example: us-central1-a",
        default=DEFAULT_LOCATION,
    )
    parser.add_argument(
        "--temp-dir",
        help="Directory to write temporary files",
        type=Path,
        default=Path("."),
    )
    parser.add_argument(
        "--keep-temp-file",
        help="Keep temporary files",
        action="store_true",
        default=DEFAULT_KEEP_TEMP_FILE,
    )
    parser.add_argument(
        "--test-dir",
        type=Path,
        help="Directory containing samples",
        required=True,
    )
    parser.add_argument(
        "--test-max",
        type=int,
        help="Maximum number of samples to process",
    )
    parser.add_argument(
        "--token",
        help="Authentication token for the SAST scanner service, if necessary",
    )

    args = parser.parse_args()

    runner = BenchmarkRunner(args)
    runner.run()

#!/usr/bin/env python3

import argparse
import html
import json
import time
import unittest
from io import StringIO
from pathlib import Path
from typing import Any
from unittest.mock import Mock

import requests
from run_benchmarks import BenchmarkRunner


def normalize_report(d: dict[str, Any]) -> dict[str, Any]:
    d = json.loads(json.dumps(d))

    bm = d["benchmark"]
    bm.pop("duration", None)

    return d


def make_sample_report(n: int):
    return {
        "benchmark": {
            "io_stats": {
                "ReadBytes": n,
                "WriteBytes": n,
                "IOReadCount": n,
                "IOWriteCount": n,
            },
            "mem_used": n,
            "real_time": "0.357",
            "sys_time": "0.033",
            "usr_time": "0.300",
        },
        "vulnerabilities": [
            {
                "description": "The `strcpy` family of functions do not provide the ability to limit or check buffer\nsizes before copying to a destination buffer. This can lead to buffer overflows. Consider\nusing more secure alternatives such as `strncpy` and provide the correct limit to the\ndestination buffer and ensure the string is null terminated.\n\nFor more information please see: https://linux.die.net/man/3/strncpy\n\nIf developing for C Runtime Library (CRT), more secure versions of these functions should be\nused, see:\nhttps://learn.microsoft.com/en-us/cpp/c-runtime-library/reference/strncpy-s-strncpy-s-l-wcsncpy-s-wcsncpy-s-l-mbsncpy-s-mbsncpy-s-l?view=msvc-170\n",
                "location": {
                    "end_column": 24,
                    "end_line": 4,
                    "file": "src/main.c",
                    "start_column": 3,
                    "start_line": 4,
                },
                "name": "Insecure string processing function (strcpy)",
                "severity": "High",
            },
            {
                "description": "vuln that needs <escaping>",
                "location": {
                    "end_column": "&",
                    "end_line": "&",
                    "file": "&",
                    "start_column": "&",
                    "start_line": "&",
                },
                "name": "& escape me & too &",
                "severity": "&",
            },
        ],
    }


_originalSession = requests.Session


class MockSession(Mock):
    _queries: list[tuple[str, str]]

    def __init__(self, *args, **kwargs):
        super().__init__(spec=_originalSession)
        self._queries = []

    def post(self, url: str, data: str, **kwargs) -> requests.Response:
        time.sleep(0.1)
        response = Mock(spec=requests.Response)
        response.raise_for_status = Mock()
        response.json.return_value = make_sample_report(len(self._queries))

        self._queries.append((url, data))

        return response


requests.Session = MockSession


TEST_DIR = Path(__file__).parent / "testdata/samples"


class MockPath(Mock):
    parent: "MockPath | None"
    name: str
    _content: StringIO | None

    _children: dict[str, "MockPath"]

    def __init__(self, name: str, parent: "MockPath | None" = None):
        super().__init__(spec=Path)
        self.name = name
        self.parent = parent
        self._content = None
        self._children = {}

    @property
    def stem(self) -> str:
        return self.name.split(".")[0]

    def is_file(self) -> bool:
        return self.name.endswith(".json")

    def exists(self) -> bool:
        return self.name.endswith(".json")

    def open(self, mode="r"):
        if "w" in mode:
            self._content = StringIO()
            self._content.close = lambda: None
        if self._content is not None:
            self._content.seek(0)
        return self._content

    def __truediv__(self, sub: str):
        p = MockPath(sub, parent=self)
        self._children[sub] = p
        return p

    def __str__(self):
        if self.parent is not None:
            return str(self.parent) + "/" + self.name
        return self.name

    def unlink(self):
        pass


class TestBenchmarkRunner(unittest.TestCase):
    def test_run(self):
        num_tests = 10
        temp_dir = MockPath("temp_dir")
        project_dir = MockPath("project_dir")
        args = argparse.Namespace(
            host="localhost",
            port=8080,
            project_dir=project_dir,
            location="local",
            test_dir=TEST_DIR,
            temp_dir=temp_dir,
            test_max=num_tests,
            keep_temp_file=False,
            token=None,
        )
        runner = BenchmarkRunner(args)
        runner.run()

        num_sample_files = len(list(TEST_DIR.glob("*")))
        self.assertEqual(len(runner.session._queries), num_sample_files, "expected one query per sample")  # type: ignore
        self.assertEqual(
            len(temp_dir._children),
            num_sample_files,
            "expected one temp file per sample",
        )

        expected_reports = [make_sample_report(i) for i in range(num_tests)]
        for i, child in enumerate(temp_dir._children.values()):
            assert isinstance(child._content, StringIO)  # for type checking

            actual_report = json.loads(child._content.getvalue())
            self.maxDiff = None
            self.assertDictEqual(
                normalize_report(actual_report),
                normalize_report(expected_reports[i]),
                "expected report does not match actual report",
            )

        content = self.assertGetContent(
            project_dir,
            "metrics-local.json",
        )
        j = json.loads(content)
        self.assertIn("average_time_per_file", j)
        self.assertIn("average_size_of_files", j)

        content = self.assertGetContent(project_dir, "vulns-local.html")
        for expected_report in expected_reports:
            for vuln in expected_report["vulnerabilities"]:
                self.assertIn(html.escape(vuln["name"]), content)
                self.assertIn(html.escape(vuln["description"]), content)

        content = self.assertGetContent(project_dir, "metrics-local.html")
        self.assertIn("Benchmark Metrics", content)
        self.assertIn("IO Stats", content)
        self.assertIn("Memory and Time Stats", content)

    def assertGetContent(self, p: MockPath, name: str) -> str:
        cp = p._children.get(name)
        self.assertIsNotNone(cp)
        assert cp is not None and isinstance(cp._content, StringIO)  # for type checking
        return cp._content.getvalue()


if __name__ == "__main__":
    unittest.main()

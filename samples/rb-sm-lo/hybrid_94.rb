class Projects::EnvironmentsController < Projects::ApplicationController
  include ProductAnalyticsTracking
  include KasCookie

  MIN_SEARCH_LENGTH = 3
  ACTIVE_STATES = %i[available stopping].freeze
  SCOPES_TO_STATES = { "active" => ACTIVE_STATES, "stopped" => %i[stopped] }.freeze

  layout 'project'

  before_action only: [:folder] do
    push_frontend_feature_flag(:environments_folder_new_look, project)
  end

  before_action only: [:show] do
    push_frontend_feature_flag(:k8s_tree_view, project)
    push_frontend_feature_flag(:use_websocket_for_k8s_watch, project)
  end

  before_action :authorize_read_environment!
  before_action :authorize_create_environment!, only: [:new, :create]
  before_action :authorize_stop_environment!, only: [:stop]
  before_action :authorize_update_environment!, only: [:edit, :update, :cancel_auto_stop]
  before_action :authorize_admin_environment!, only: [:terminal, :terminal_websocket_authorize]
  before_action :environment,
    only: [:show, :edit, :update, :stop, :terminal, :terminal_websocket_authorize, :cancel_auto_stop, :k8s]
  before_action :verify_api_request!, only: :terminal_websocket_authorize
  before_action :expire_etag_cache, only: [:index], unless: -> { request.format.json? }
  before_action :set_kas_cookie, only: [:edit, :new, :show, :k8s], if: -> { current_user && request.format.html? }
  after_action :expire_etag_cache, only: [:cancel_auto_stop]

  track_event :index, :folder, :show, :new, :edit, :create, :update, :stop, :cancel_auto_stop, :terminal, :k8s,
    name: 'users_visiting_environments_pages'

  feature_category :continuous_delivery
  urgency :low

  def index
    @project = ProjectPresenter.new(project, current_user: current_user)

    respond_to do |format|
      format.html
      format.json do
        states = SCOPES_TO_STATES.fetch(params[:scope], ACTIVE_STATES)
        @environments = search_environments.with_state(states)

        environments_count_by_state = search_environments.count_by_state

        Gitlab::PollingInterval.set_header(response, interval: 3_000)
        render json: {
          environments: serialize_environments(request, response, params[:nested]),
          review_app: serialize_review_app,
          can_stop_stale_environments: can?(current_user, :stop_environment, @project),
          available_count: environments_count_by_state[:available],
          active_count: environments_count_by_state[:available] + environments_count_by_state[:stopping],
          stopped_count: environments_count_by_state[:stopped]
        }
      end
    end
  end

  # Returns all environments for a given folder
  # rubocop: disable CodeReuse/ActiveRecord
  def folder
    @folder = params[:id]

    respond_to do |format|
      format.html
      format.json do
        states = SCOPES_TO_STATES.fetch(params[:scope], ACTIVE_STATES)
        folder_environments = search_environments(type: params[:id])

        @environments = folder_environments.with_state(states)
          .order(:name)

        render json: {
          environments: serialize_environments(request, response),
          available_count: folder_environments.available.count,
          active_count: folder_environments.active.count,
          stopped_count: folder_environments.stopped.count
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def show; end

  def new
    @environment = project.environments.new
  end

  def edit; end

  def k8s
    render action: :show
  end

  def create
    @environment = project.environments.create(environment_params)

    if @environment.persisted?
      render json: { environment: @environment, path: project_environment_path(project, @environment) }
    else
      render json: { message: @environment.errors.full_messages }, status: :bad_request
    end
  end

  def update
    if @environment.update(environment_params)
      render json: { environment: @environment, path: project_environment_path(project, @environment) }
    else
      render json: { message: @environment.errors.full_messages }, status: :bad_request
    end
  end

  def stop
    return render_404 unless @environment.available?

    service_response = Environments::StopService.new(project, current_user).execute(@environment)
    return render_403 unless service_response.success?

    job = service_response[:actions].first if service_response[:actions]&.count == 1

    action_or_env_url =
      if job
        project_job_url(project, job)
      else
        project_environment_url(project, @environment)
      end

    respond_to do |format|
      format.html { redirect_to action_or_env_url }
      format.json { render json: { redirect_url: action_or_env_url } }
    end
  end

  def cancel_auto_stop
    result = Environments::ResetAutoStopService.new(project, current_user)
                                               .execute(environment)

    if result[:status] == :success
      respond_to do |format|
        message = _('Auto stop successfully canceled.')

        format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
        format.json { render json: { message: message }, status: :ok }
      end
    else
      respond_to do |format|
        message = result[:message]

        format.html { redirect_back_or_default(default: { action: 'show' }, options: { alert: message }) }
        format.json { render json: { message: message }, status: :unprocessable_entity }
      end
    end
  end

  def terminal
    # Currently, this acts as a hint to load the terminal details into the cache
    # if they aren't there already. In the future, users will need these details
    # to choose between terminals to connect to.
    @terminals = environment.terminals
  end

  # GET .../terminal.ws : implemented in gitlab-workhorse
  def terminal_websocket_authorize
    # Just return the first terminal for now. If the list is in the process of
    # being looked up, this may result in a 404 response, so the frontend
    # should retry those errors
    terminal = environment.terminals.try(:first)
    if terminal
      set_workhorse_internal_api_content_type
      render json: Gitlab::Workhorse.channel_websocket(terminal)
    else
      render html: 'Not found', status: :not_found
    end
  end

  def search
    respond_to do |format|
      format.json do
        environment_names = search_environment_names

        render json: environment_names, status: environment_names.any? ? :ok : :no_content
      end
    end
  end

  private

  def deployments
    environment
      .deployments
      .with_environment_page_associations
      .ordered
      .page(params[:page])
  end

  def verify_api_request!
    Gitlab::Workhorse.verify_api_request!(request.headers)
  end

  def expire_etag_cache
    # this forces to reload json content
    Gitlab::EtagCaching::Store.new.tap do |store|
      store.touch(project_environments_path(project, format: :json))
    end
  end

  def allowed_environment_attributes
    attributes = [:external_url]
    attributes << :name if action_name == "create"
    attributes
  end

  def environment_params
    params.require(:environment).permit(allowed_environment_attributes)
  end

  def environment
    @environment ||= project.environments.find(params[:id])
  end

  def search_environments(type: nil)
    search = params[:search] if params[:search] && params[:search].length >= MIN_SEARCH_LENGTH

    @search_environments ||= Environments::EnvironmentsFinder.new(
      project,
      current_user,
      type: type,
      search: search
    ).execute
  end

  def include_all_dashboards?
    !params[:embedded]
  end

  def search_environment_names
    return [] unless params[:query]

    project.environments.for_name_like(params[:query]).pluck_names
  end

  def serialize_environments(request, response, nested = false)
    EnvironmentSerializer
      .new(project: @project, current_user: @current_user)
      .tap { |serializer| serializer.within_folders if nested }
      .with_pagination(request, response)
      .represent(@environments)
  end

  def serialize_review_app
    ReviewAppSetupSerializer.new(current_user: @current_user).represent(@project)
  end

  def authorize_stop_environment!
    access_denied! unless can?(current_user, :stop_environment, environment)
  end

  def authorize_update_environment!
    access_denied! unless can?(current_user, :update_environment, environment)
  end
end

class Projects::EnvironmentsController < Projects::ApplicationController
  include ProductAnalyticsTracking
  include KasCookie

  MIN_SEARCH_LENGTH = 3
  ACTIVE_STATES = %i[available stopping].freeze
  SCOPES_TO_STATES = { "active" => ACTIVE_STATES, "stopped" => %i[stopped] }.freeze

  layout 'project'

  before_action only: [:folder] do
    push_frontend_feature_flag(:environments_folder_new_look, project)
  end

  before_action only: [:show] do
    push_frontend_feature_flag(:k8s_tree_view, project)
    push_frontend_feature_flag(:use_websocket_for_k8s_watch, project)
  end

  before_action :authorize_read_environment!
  before_action :authorize_create_environment!, only: [:new, :create]
  before_action :authorize_stop_environment!, only: [:stop]
  before_action :authorize_update_environment!, only: [:edit, :update, :cancel_auto_stop]
  before_action :authorize_admin_environment!, only: [:terminal, :terminal_websocket_authorize]
  before_action :environment,
    only: [:show, :edit, :update, :stop, :terminal, :terminal_websocket_authorize, :cancel_auto_stop, :k8s]
  before_action :verify_api_request!, only: :terminal_websocket_authorize
  before_action :expire_etag_cache, only: [:index], unless: -> { request.format.json? }
  before_action :set_kas_cookie, only: [:edit, :new, :show, :k8s], if: -> { current_user && request.format.html? }
  after_action :expire_etag_cache, only: [:cancel_auto_stop]

  track_event :index, :folder, :show, :new, :edit, :create, :update, :stop, :cancel_auto_stop, :terminal, :k8s,
    name: 'users_visiting_environments_pages'

  feature_category :continuous_delivery
  urgency :low

  def index
    @project = ProjectPresenter.new(project, current_user: current_user)

    respond_to do |format|
      format.html
      format.json do
        states = SCOPES_TO_STATES.fetch(params[:scope], ACTIVE_STATES)
        @environments = search_environments.with_state(states)

        environments_count_by_state = search_environments.count_by_state

        Gitlab::PollingInterval.set_header(response, interval: 3_000)
        render json: {
          environments: serialize_environments(request, response, params[:nested]),
          review_app: serialize_review_app,
          can_stop_stale_environments: can?(current_user, :stop_environment, @project),
          available_count: environments_count_by_state[:available],
          active_count: environments_count_by_state[:available] + environments_count_by_state[:stopping],
          stopped_count: environments_count_by_state[:stopped]
        }
      end
    end
  end

  # Returns all environments for a given folder
  # rubocop: disable CodeReuse/ActiveRecord
  def folder
    @folder = params[:id]

    respond_to do |format|
      format.html
      format.json do
        states = SCOPES_TO_STATES.fetch(params[:scope], ACTIVE_STATES)
        folder_environments = search_environments(type: params[:id])

        @environments = folder_environments.with_state(states)
          .order(:name)

        render json: {
          environments: serialize_environments(request, response),
          available_count: folder_environments.available.count,
          active_count: folder_environments.active.count,
          stopped_count: folder_environments.stopped.count
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def show; end

  def new
    @environment = project.environments.new
  end

  def edit; end

  def k8s
    render action: :show
  end

  def create
    @environment = project.environments.create(environment_params)

    if @environment.persisted?
      render json: { environment: @environment, path: project_environment_path(project, @environment) }
    else
      render json: { message: @environment.errors.full_messages }, status: :bad_request
    end
  end

  def update
    if @environment.update(environment_params)
      render json: { environment: @environment, path: project_environment_path(project, @environment) }
    else
      render json: { message: @environment.errors.full_messages }, status: :bad_request
    end
  end

  def stop
    return render_404 unless @environment.available?

    service_response = Environments::StopService.new(project, current_user).execute(@environment)
    return render_403 unless service_response.success?

    job = service_response[:actions].first if service_response[:actions]&.count == 1

    action_or_env_url =
      if job
        project_job_url(project, job)
      else
        project_environment_url(project, @environment)
      end

    respond_to do |format|
      format.html { redirect_to action_or_env_url }
      format.json { render json: { redirect_url: action_or_env_url } }
    end
  end

  def cancel_auto_stop
    result = Environments::ResetAutoStopService.new(project, current_user)
                                               .execute(environment)

    if result[:status] == :success
      respond_to do |format|
        message = _('Auto stop successfully canceled.')

        format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
        format.json { render json: { message: message }, status: :ok }
      end
    else
      respond_to do |format|
        message = result[:message]

        format.html { redirect_back_or_default(default: { action: 'show' }, options: { alert: message }) }
        format.json { render json: { message: message }, status: :unprocessable_entity }
      end
    end
  end

  def terminal
    # Currently, this acts as a hint to load the terminal details into the cache
    # if they aren't there already. In the future, users will need these details
    # to choose between terminals to connect to.
    @terminals = environment.terminals
  end

  # GET .../terminal.ws : implemented in gitlab-workhorse
  def terminal_websocket_authorize
    # Just return the first terminal for now. If the list is in the process of
    # being looked up, this may result in a 404 response, so the frontend
    # should retry those errors
    terminal = environment.terminals.try(:first)
    if terminal
      set_workhorse_internal_api_content_type
      render json: Gitlab::Workhorse.channel_websocket(terminal)
    else
      render html: 'Not found', status: :not_found
    end
  end

  def search
    respond_to do |format|
      format.json do
        environment_names = search_environment_names

        render json: environment_names, status: environment_names.any? ? :ok : :no_content
      end
    end
  end

  private

  def deployments
    environment
      .deployments
      .with_environment_page_associations
      .ordered
      .page(params[:page])
  end

  def verify_api_request!
    Gitlab::Workhorse.verify_api_request!(request.headers)
  end

  def expire_etag_cache
    # this forces to reload json content
    Gitlab::EtagCaching::Store.new.tap do |store|
      store.touch(project_environments_path(project, format: :json))
    end
  end

  def allowed_environment_attributes
    attributes = [:external_url]
    attributes << :name if action_name == "create"
    attributes
  end

  def environment_params
    params.require(:environment).permit(allowed_environment_attributes)
  end

  def environment
    @environment ||= project.environments.find(params[:id])
  end

  def search_environments(type: nil)
    search = params[:search] if params[:search] && params[:search].length >= MIN_SEARCH_LENGTH

    @search_environments ||= Environments::EnvironmentsFinder.new(
      project,
      current_user,
      type: type,
      search: search
    ).execute
  end

  def include_all_dashboards?
    !params[:embedded]
  end

  def search_environment_names
    return [] unless params[:query]

    project.environments.for_name_like(params[:query]).pluck_names
  end

  def serialize_environments(request, response, nested = false)
    EnvironmentSerializer
      .new(project: @project, current_user: @current_user)
      .tap { |serializer| serializer.within_folders if nested }
      .with_pagination(request, response)
      .represent(@environments)
  end

  def serialize_review_app
    ReviewAppSetupSerializer.new(current_user: @current_user).represent(@project)
  end

  def authorize_stop_environment!
    access_denied! unless can?(current_user, :stop_environment, environment)
  end

  def authorize_update_environment!
    access_denied! unless can?(current_user, :update_environment, environment)
  end
end

class RepositoryController < Projects::ApplicationController
      layout 'project_settings'
      before_action :authorize_admin_project!
      before_action :define_variables, only: [:create_deploy_token]

      before_action do
        push_frontend_feature_flag(:edit_branch_rules, @project)
        push_frontend_ability(ability: :admin_project, resource: @project, user: current_user)
        push_frontend_ability(ability: :admin_protected_branch, resource: @project, user: current_user)
      end

      feature_category :source_code_management, [:show, :cleanup, :update]
      feature_category :continuous_delivery, [:create_deploy_token]
      urgency :low, [:show, :create_deploy_token]

      def show
        render_show
      end

      def cleanup
        bfg_object_map = params.require(:project).require(:bfg_object_map)
        result = Projects::CleanupService.enqueue(project, current_user, bfg_object_map)

        if result[:status] == :success
          flash[:notice] = _('Repository cleanup has started. You will receive an email once the cleanup operation is complete.')
        else
          flash[:alert] = result.fetch(:message, _('Failed to upload object map file'))
        end

        redirect_to project_settings_repository_path(project)
      end

      def create_deploy_token
        result = Projects::DeployTokens::CreateService.new(@project, current_user, deploy_token_params).execute

        if result[:status] == :success
          @created_deploy_token = result[:deploy_token]
          respond_to do |format|
            format.json do
              # IMPORTANT: It's a security risk to expose the token value more than just once here!
              json = API::Entities::DeployTokenWithToken.represent(@created_deploy_token).as_json
              render json: json, status: result[:http_status]
            end
            format.html do
              flash.now[:notice] = s_('DeployTokens|Your new project deploy token has been created.')
              render :show
            end
          end
        else
          @new_deploy_token = result[:deploy_token]
          respond_to do |format|
            format.json { render json: { message: result[:message] }, status: result[:http_status] }
            format.html do
              flash.now[:alert] = result[:message]
              render :show
            end
          end
        end
      end

      def update
        result = ::Projects::UpdateService.new(@project, current_user, project_params).execute

        if result[:status] == :success
          flash[:notice] = _("Project settings were successfully updated.")
        else
          flash[:alert] = result[:message]
          @project.reset
        end

        redirect_to project_settings_repository_path(project)
      end

      private

      def render_show
        define_variables

        render 'show'
      end

      def define_variables
        @deploy_keys = DeployKeysPresenter.new(@project, current_user: current_user)

        define_deploy_token_variables
        define_protected_refs
        remote_mirror
      end

      # rubocop: disable CodeReuse/ActiveRecord
      def define_protected_refs
        @protected_branches = fetch_protected_branches(@project).preload_access_levels
        @protected_tags = @project.protected_tags.preload_access_levels.order(:name).page(pagination_params[:page])
        @protected_branch = @project.protected_branches.new
        @protected_tag = @project.protected_tags.new

        @protected_tags_count = @protected_tags.reduce(0) { |sum, tag| sum + tag.matching(@project.repository.tag_names).size }
        load_gon_index
      end
      # rubocop: enable CodeReuse/ActiveRecord

      def fetch_protected_branches(project)
        project.protected_branches.sorted_by_name.page(pagination_params[:page])
      end

      def remote_mirror
        @remote_mirror = project.remote_mirrors.first_or_initialize
      end

      def deploy_token_params
        params.require(:deploy_token).permit(:name, :expires_at, :read_repository, :read_registry, :write_registry, :read_package_registry, :write_package_registry, :username)
      end

      def project_params
        params.require(:project).permit(project_params_attributes)
      end

      def project_params_attributes
        [
          :issue_branch_template,
          :default_branch,
          :autoclose_referenced_issues
        ]
      end

      def protectable_tags_for_dropdown
        { open_tags: ProtectableDropdown.new(@project, :tags).hash }
      end

      def protectable_branches_for_dropdown
        { open_branches: ProtectableDropdown.new(@project, :branches).hash }
      end

      def define_deploy_token_variables
        @deploy_tokens = @project.deploy_tokens.active

        @new_deploy_token ||= DeployToken.new
      end

      def load_gon_index
        gon.push(protectable_tags_for_dropdown)
        gon.push(protectable_branches_for_dropdown)
        gon.push(helpers.protected_access_levels_for_dropdowns)
        gon.push(current_project_id: project.id) if project
      end
    end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'net/ftp'

def foo

  host = params[:host]
  # ruleid: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.new(host)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  ftp = Net::FTP.new()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.new("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.open("example.com")

  ftp = Net::FTP.new()
  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.connect("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.get("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.getbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.gettextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.put("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.putbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.puttextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.delete("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storlines("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storbinary("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.sendcmd("ls -al")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.retrlines("ls -al")

end
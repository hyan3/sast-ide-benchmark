class BroadcastMessagesController < ApplicationController
    include Admin::BroadcastMessagesHelper

    before_action :find_broadcast_message, only: [:edit, :update, :destroy]
    before_action :find_broadcast_messages, only: [:index, :create]

    feature_category :notifications
    urgency :low

    def index
      @broadcast_message = System::BroadcastMessage.new
    end

    def edit; end

    def create
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      success = @broadcast_message.save

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully created.')
          else
            render :index
          end
        end
      end
    end

    def update
      success = @broadcast_message.update(broadcast_message_params)

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully updated.')
          else
            render :edit
          end
        end
      end
    end

    def destroy
      @broadcast_message.destroy

      respond_to do |format|
        format.html { redirect_back_or_default(default: { action: 'index' }) }
        format.js { head :ok }
      end
    end

    def preview
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      render plain: render_broadcast_message(@broadcast_message), status: :ok
    end

    protected

    def find_broadcast_message
      @broadcast_message = System::BroadcastMessage.find(params.permit(:id)[:id])
    end

    def find_broadcast_messages
      @broadcast_messages = System::BroadcastMessage.order(ends_at: :desc).page(pagination_params[:page]) # rubocop: disable CodeReuse/ActiveRecord
    end

    def broadcast_message_params
      params.require(:broadcast_message)
        .permit(%i[
          theme
          ends_at
          message
          starts_at
          target_path
          broadcast_type
          dismissable
          show_in_cli
        ], target_access_levels: []).reverse_merge!(target_access_levels: [])
    end
  end

class BroadcastMessagesController < ApplicationController
    include Admin::BroadcastMessagesHelper

    before_action :find_broadcast_message, only: [:edit, :update, :destroy]
    before_action :find_broadcast_messages, only: [:index, :create]

    feature_category :notifications
    urgency :low

    def index
      @broadcast_message = System::BroadcastMessage.new
    end

    def edit; end

    def create
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      success = @broadcast_message.save

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully created.')
          else
            render :index
          end
        end
      end
    end

    def update
      success = @broadcast_message.update(broadcast_message_params)

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully updated.')
          else
            render :edit
          end
        end
      end
    end

    def destroy
      @broadcast_message.destroy

      respond_to do |format|
        format.html { redirect_back_or_default(default: { action: 'index' }) }
        format.js { head :ok }
      end
    end

    def preview
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      render plain: render_broadcast_message(@broadcast_message), status: :ok
    end

    protected

    def find_broadcast_message
      @broadcast_message = System::BroadcastMessage.find(params.permit(:id)[:id])
    end

    def find_broadcast_messages
      @broadcast_messages = System::BroadcastMessage.order(ends_at: :desc).page(pagination_params[:page]) # rubocop: disable CodeReuse/ActiveRecord
    end

    def broadcast_message_params
      params.require(:broadcast_message)
        .permit(%i[
          theme
          ends_at
          message
          starts_at
          target_path
          broadcast_type
          dismissable
          show_in_cli
        ], target_access_levels: []).reverse_merge!(target_access_levels: [])
    end
  end

class PersonalAccessTokensController < ApplicationController
    include RenderAccessTokens
    include FeedTokenHelper

    feature_category :system_access

    before_action :check_personal_access_tokens_enabled
    prepend_before_action(only: [:index]) { authenticate_sessionless_user!(:ics) }

    def index
      set_index_vars
      scopes = params[:scopes].split(',').map(&:squish).select(&:present?).map(&:to_sym) unless params[:scopes].nil?
      @personal_access_token = finder.build(
        name: params[:name],
        scopes: scopes
      )

      respond_to do |format|
        format.html
        format.json do
          render json: @active_access_tokens
        end
        format.ics do
          if params[:feed_token]
            response.headers['Content-Type'] = 'text/plain'
            render plain: expiry_ics(@active_access_tokens)
          else
            redirect_to "#{request.path}?feed_token=#{generate_feed_token_with_path(:ics, request.path)}"
          end
        end
      end
    end

    def create
      result = ::PersonalAccessTokens::CreateService.new(
        current_user: current_user,
        target_user: current_user,
        organization_id: Current.organization_id,
        params: personal_access_token_params,
        concatenate_errors: false
      ).execute

      @personal_access_token = result.payload[:personal_access_token]

      tokens, size = active_access_tokens
      if result.success?
        render json: { new_token: @personal_access_token.token,
                       active_access_tokens: tokens, total: size }, status: :ok
      else
        render json: { errors: result.errors }, status: :unprocessable_entity
      end
    end

    def revoke
      @personal_access_token = finder.find(params[:id])
      service = PersonalAccessTokens::RevokeService.new(current_user, token: @personal_access_token).execute
      service.success? ? flash[:notice] = service.message : flash[:alert] = service.message

      redirect_to user_settings_personal_access_tokens_path
    end

    private

    def finder(options = {})
      PersonalAccessTokensFinder.new({ user: current_user, impersonation: false }.merge(options))
    end

    def personal_access_token_params
      params.require(:personal_access_token).permit(:name, :expires_at, scopes: [])
    end

    def set_index_vars
      @scopes = Gitlab::Auth.available_scopes_for(current_user)
      @active_access_tokens, @active_access_tokens_size = active_access_tokens
    end

    def represent(tokens)
      ::PersonalAccessTokenSerializer.new.represent(tokens)
    end

    def check_personal_access_tokens_enabled
      render_404 if Gitlab::CurrentSettings.personal_access_tokens_disabled?
    end
  end

class Projects::CompareController < Projects::ApplicationController
  include DiffForPath
  include DiffHelper
  include RendersCommits
  include CompareHelper

  # Authorize
  before_action :require_non_empty_project
  before_action :authorize_read_code!
  # Defining ivars
  before_action :define_diffs, only: [:show, :diff_for_path, :rapid_diffs]
  before_action :define_environment, only: [:show, :rapid_diffs]
  before_action :define_diff_notes_disabled, only: [:show, :diff_for_path, :rapid_diffs]
  before_action :define_commits, only: [:show, :diff_for_path, :signatures, :rapid_diffs]
  before_action :merge_request, only: [:index, :show, :rapid_diffs]
  # Validation
  before_action :validate_refs!

  feature_category :source_code_management
  urgency :low, [:show, :create, :signatures]

  # Diffs may be pretty chunky, the less is better in this endpoint.
  # Pagination design guides: https://design.gitlab.com/components/pagination/#behavior
  COMMIT_DIFFS_PER_PAGE = 20

  def index
    compare_params
  end

  def show
    apply_diff_view_cookie!

    render locals: { pagination_params: params.permit(:page) }
  end

  def diff_for_path
    return render_404 unless compare

    render_diff_for_path(compare.diffs(diff_options))
  end

  def create
    from_to_vars = build_from_to_vars

    if from_to_vars[:from].blank? || from_to_vars[:to].blank?
      flash[:alert] = "You must select a Source and a Target revision"

      redirect_to project_compare_index_path(source_project, from_to_vars)
    elsif compare_params[:straight] == "true"
      redirect_to project_compare_with_two_dots_path(source_project, from_to_vars)
    else
      redirect_to project_compare_path(source_project, from_to_vars)
    end
  end

  def signatures
    respond_to do |format|
      format.json do
        render json: {
          signatures: @commits.select(&:has_signature?).map do |commit|
            {
              commit_sha: commit.sha,
              html: view_to_html_string('projects/commit/_signature', signature: commit.signature)
            }
          end
        }
      end
    end
  end

  def rapid_diffs
    return render_404 unless ::Feature.enabled?(:rapid_diffs, current_user, type: :wip)

    show
  end

  private

  def build_from_to_vars
    from_to_vars = {
      from: compare_params[:from].presence,
      to: compare_params[:to].presence
    }

    if compare_params[:from_project_id] != compare_params[:to_project_id]
      from_to_vars[:from_project_id] = compare_params[:from_project_id].presence
    end

    from_to_vars
  end

  def validate_refs!
    invalid = [head_ref, start_ref].filter { |ref| !valid_ref?(ref) }

    return if invalid.empty?

    flash[:alert] = "Invalid branch name(s): #{invalid.join(', ')}"
    redirect_to project_compare_index_path(source_project)
  end

  # target == start_ref == from
  def target_project
    strong_memoize(:target_project) do
      target_project =
        if !compare_params.key?(:from_project_id)
          source_project.default_merge_request_target
        elsif compare_params[:from_project_id].to_i == source_project.id
          source_project
        else
          target_projects(source_project).find_by_id(compare_params[:from_project_id])
        end

      # Just ignore the field if it points at a non-existent or hidden project
      next source_project unless target_project && can?(current_user, :read_code, target_project)

      target_project
    end
  end

  # source == head_ref == to
  def source_project
    strong_memoize(:source_project) do
      # Eager load project's avatar url to prevent batch loading
      # for all forked projects
      project&.tap(&:avatar_url)
    end
  end

  def compare
    return @compare if defined?(@compare)

    @compare = CompareService.new(source_project, head_ref).execute(target_project, start_ref, straight: straight)
  end

  def straight
    compare_params[:straight] == "true"
  end

  def start_ref
    @start_ref ||= Addressable::URI.unescape(compare_params[:from]).presence
  end

  def head_ref
    return @ref if defined?(@ref)

    @ref = @head_ref = Addressable::URI.unescape(compare_params[:to]).presence
  end

  def define_commits
    strong_memoize(:commits) do
      if compare.present?
        commits = compare.commits.with_markdown_cache.with_latest_pipeline(head_ref)
        set_commits_for_rendering(commits)
      else
        []
      end
    end
  end

  def define_diffs
    @diffs = compare.present? ? compare.diffs(diff_options) : []
  end

  def define_environment
    if compare
      environment_params = if source_project.repository.branch_exists?(head_ref)
                             { ref: head_ref }
                           else
                             { commit: compare.commit }
                           end

      environment_params[:find_latest] = true
      @environment = ::Environments::EnvironmentsByDeploymentsFinder.new(
        source_project,
        current_user,
        environment_params
      ).execute.last
    end
  end

  def define_diff_notes_disabled
    @diff_notes_disabled = compare.present?
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def merge_request
    @merge_request ||= MergeRequestsFinder.new(current_user, project_id: target_project.id).execute.opened
      .find_by(source_project: source_project, source_branch: head_ref, target_branch: start_ref)
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def compare_params
    @compare_params ||= params.permit(:from, :to, :from_project_id, :straight, :to_project_id)
  end
end

class Projects::CommitsController < Projects::ApplicationController
  include ExtractsPath
  include RendersCommits
  include ParseCommitDate
  include RedirectsForMissingPathOnTree

  COMMITS_DEFAULT_LIMIT = 40
  prepend_before_action(only: [:show]) { authenticate_sessionless_user!(:rss) }
  around_action :allow_gitaly_ref_name_caching
  before_action :require_non_empty_project
  before_action :assign_ref_vars, except: :commits_root
  before_action :authorize_read_code!
  before_action :validate_ref!, except: :commits_root
  before_action :validate_path, if: -> { !request.format.atom? }
  before_action :set_is_ambiguous_ref, only: [:show]
  before_action :set_commits, except: :commits_root

  feature_category :source_code_management
  urgency :low, [:signatures, :show]

  def commits_root
    redirect_to project_commits_path(@project, @project.default_branch)
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def show
    @merge_request = MergeRequestsFinder.new(current_user, project_id: @project.id).execute.opened
      .find_by(source_project: @project, source_branch: @ref, target_branch: @repository.root_ref)

    @ref_type = ref_type

    respond_to do |format|
      format.html
      format.atom { render layout: 'xml' }

      format.json do
        pager_json(
          'projects/commits/_commits',
          @commits.size,
          project: @project,
          ref: @ref)
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def signatures
    respond_to do |format|
      format.json do
        render json: {
          signatures: @commits.select(&:has_signature?).map do |commit|
            {
              commit_sha: commit.sha,
              html: view_to_html_string('projects/commit/_signature', signature: commit.signature)
            }
          end
        }
      end
    end
  end

  private

  def validate_ref!
    render_404 unless valid_ref?(@ref)
  end

  def validate_path
    return unless @path

    path_exists = @repository.blob_at(@commit.id, @path) || @repository.tree(@commit.id, @path).entries.present?
    redirect_to_tree_root_for_missing_path(@project, @ref, @path) unless path_exists
  end

  def set_commits
    limit = permitted_params[:limit].to_i
    @limit = limit > 0 ? limit : COMMITS_DEFAULT_LIMIT # limit can only ever be a positive number
    @offset = (permitted_params[:offset] || 0).to_i
    search = permitted_params[:search]
    author = permitted_params[:author]
    committed_before = convert_date_to_epoch(permitted_params[:committed_before])
    committed_after = convert_date_to_epoch(permitted_params[:committed_after])

    # fully_qualified_ref is available in some situations from ExtractsRef
    ref = @fully_qualified_ref || @ref

    @commits =
      if search.present?
        @repository.find_commits_by_message(search, ref, @path, @limit, @offset)
      else
        options = {
          path: @path,
          limit: @limit,
          offset: @offset
        }
        options[:author] = author if author.present?
        options[:before] = committed_before if committed_before.present?
        options[:after] = committed_after if committed_after.present?

        @repository.commits(ref, **options)
      end

    @commits.load_tags
    @commits.each(&:lazy_author) # preload authors

    @commits = @commits.with_markdown_cache.with_latest_pipeline(ref)
    @commits = set_commits_for_rendering(@commits)
  end

  def permitted_params
    params.permit(:limit, :offset, :search, :author, :committed_before, :committed_after)
  end
end

class Projects::MilestonesController < Projects::ApplicationController
  include Gitlab::Utils::StrongMemoize
  include MilestoneActions

  REDIRECT_TARGETS = [:new_release].freeze

  before_action :check_issuables_available!
  before_action :milestone, only: [:edit, :update, :destroy, :show, :issues, :merge_requests, :participants, :labels, :promote]
  before_action :redirect_path, only: [:new, :create]

  # Allow read any milestone
  before_action :authorize_read_milestone!

  # Allow admin milestone
  before_action :authorize_admin_milestone!, except: [:index, :show, :issues, :merge_requests, :participants, :labels]

  # Allow to promote milestone
  before_action :authorize_promote_milestone!, only: :promote

  respond_to :html

  feature_category :team_planning
  urgency :low

  def index
    @sort = params[:sort] || 'due_date_asc'
    @milestones = milestones.sort_by_attribute(@sort)

    respond_to do |format|
      format.html do
        @milestone_states = Milestone.states_count(@project)
        # We need to show group milestones in the JSON response
        # so that people can filter by and assign group milestones,
        # but we don't need to show them on the project milestones page itself.
        @milestones = @milestones.for_projects
        @milestones = @milestones.page(params[:page])
      end
      format.json do
        render json: @milestones.to_json(only: [:id, :title, :due_date], methods: :name)
      end
    end
  end

  def new
    @noteable = @milestone = @project.milestones.new
    respond_with(@milestone)
  end

  def edit
    respond_with(@milestone)
  end

  def show
    respond_to do |format|
      format.html
    end
  end

  def create
    @milestone = Milestones::CreateService.new(project, current_user, milestone_params).execute

    if @milestone.valid?
      if @redirect_path == :new_release
        redirect_to new_project_release_path(@project)
      else
        redirect_to project_milestone_path(@project, @milestone)
      end
    else
      render "new"
    end
  end

  def update
    @milestone = Milestones::UpdateService.new(project, current_user, milestone_params).execute(milestone)

    respond_to do |format|
      format.html do
        if @milestone.valid?
          redirect_to project_milestone_path(@project, @milestone)
        else
          render :edit
        end
      end

      format.js

      format.json do
        if @milestone.valid?
          head :no_content
        else
          render json: { errors: @milestone.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  rescue ActiveRecord::StaleObjectError
    respond_to do |format|
      format.html do
        @conflict = true
        render :edit
      end

      format.json do
        render json: {
          errors: [
            format(
              _("Someone edited this %{model_name} at the same time you did. Please refresh your browser and make sure your changes will not unintentionally remove theirs."),
              model_name: _('milestone')
            )
          ]
        }, status: :conflict
      end
    end
  end

  def promote
    promoted_milestone = Milestones::PromoteService.new(project, current_user).execute(milestone)
    flash[:notice] = flash_notice_for(promoted_milestone, project_group)

    respond_to do |format|
      format.html do
        redirect_to project_milestones_path(project)
      end
      format.json do
        render json: { url: project_milestones_path(project) }
      end
    end
  rescue Milestones::PromoteService::PromoteMilestoneError => e
    redirect_to milestone, alert: e.message
  end

  def flash_notice_for(milestone, group)
    ''.html_safe + "#{milestone.title} promoted to " + view_context.link_to('<u>group milestone</u>'.html_safe, group_milestone_path(group, milestone.iid)) + '.'
  end

  def destroy
    return access_denied! unless can?(current_user, :admin_milestone, @project)

    Milestones::DestroyService.new(project, current_user).execute(milestone)

    respond_to do |format|
      format.html { redirect_to namespace_project_milestones_path, status: :see_other }
      format.js { head :ok }
    end
  end

  protected

  def redirect_path
    path = params[:redirect_path]&.to_sym
    @redirect_path = path if REDIRECT_TARGETS.include?(path)
  end

  def project_group
    strong_memoize(:project_group) do
      project.group
    end
  end

  def milestones
    strong_memoize(:milestones) do
      MilestonesFinder.new(search_params).execute
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def milestone
    @noteable = @milestone ||= @project.milestones.find_by!(iid: params[:id])
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def authorize_admin_milestone!
    render_404 unless can?(current_user, :admin_milestone, @project)
  end

  def authorize_promote_milestone!
    render_404 unless can?(current_user, :admin_milestone, project_group)
  end

  def milestone_params
    params.require(:milestone)
          .permit(
            :description,
            :due_date,
            :lock_version,
            :start_date,
            :state_event,
            :title
          )
  end

  def search_params
    if request.format.json? && project_group && can?(current_user, :read_group, project_group)
      groups = project_group.self_and_ancestors.select(:id)
    end

    params.permit(:state, :search_title).merge(project_ids: @project.id, group_ids: groups)
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesSHA1
        sha = Digest::SHA1.base64digest 'abc'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ok: ruby_crypto_rule-WeakHashesSHA1
        OpenSSL::HMAC.hexdigest("SHA256", key, data)
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.new
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.hexdigest 'abc'
    end
end
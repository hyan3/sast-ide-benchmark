class ProfilesController < ApplicationController
    include ActionView::Helpers::SanitizeHelper
    include Gitlab::Tracking

    before_action :user
    skip_before_action :require_email, only: [:show, :update]
    feature_category :user_profile, [:show, :update]

    urgency :low, [:show, :update]

    def show; end

    def update
      respond_to do |format|
        result = Users::UpdateService.new(current_user, user_params.merge(user: @user)).execute(check_password: true)

        if result[:status] == :success
          message = s_("Profiles|Profile was successfully updated")

          format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
          format.json { render json: { message: message } }
        else
          format.html do
            redirect_back_or_default(default: { action: 'show' }, options: { alert: result[:message] })
          end
          format.json { render json: result }
        end
      end
    end

    private

    def user
      @user = current_user
    end

    def user_params_attributes
      [
        :achievements_enabled,
        :avatar,
        :bio,
        :bluesky,
        :commit_email,
        :discord,
        :email,
        :gitpod_enabled,
        :hide_no_password,
        :hide_no_ssh_key,
        :hide_project_limit,
        :include_private_contributions,
        :job_title,
        :linkedin,
        :location,
        :mastodon,
        :name,
        :organization,
        :private_profile,
        :pronouns,
        :pronunciation,
        :public_email,
        :role,
        :skype,
        :timezone,
        :twitter,
        :username,
        :validation_password,
        :website_url,
        { status: [:emoji, :message, :availability, :clear_status_after] }
      ]
    end

    def user_params
      @user_params ||= params.require(:user).permit(user_params_attributes)
    end
  end

class Projects::PipelinesController < Projects::ApplicationController
  include ::Gitlab::Utils::StrongMemoize
  include ProductAnalyticsTracking
  include ProjectStatsRefreshConflictsGuard

  urgency :low, [
    :index, :new, :builds, :show, :failures, :create,
    :stage, :retry, :cancel, :test_report,
    :charts, :destroy, :status, :manual_variables
  ]

  before_action only: [:charts] do
    push_frontend_feature_flag(:ci_improved_project_pipeline_analytics, project)
  end

  before_action :disable_query_limiting, only: [:create, :retry]
  before_action :pipeline, except: [:index, :new, :create, :charts]
  before_action :set_pipeline_path, only: [:show]
  before_action :authorize_read_pipeline!
  before_action :authorize_read_build!, only: [:index]
  before_action :authorize_read_build_on_pipeline!, only: [:show]
  before_action :authorize_read_ci_cd_analytics!, only: [:charts]
  before_action :authorize_create_pipeline!, only: [:new, :create]
  before_action :authorize_update_pipeline!, only: [:retry]
  before_action :authorize_cancel_pipeline!, only: [:cancel]
  before_action :ensure_pipeline, only: [:show, :downloadable_artifacts]
  before_action :reject_if_build_artifacts_size_refreshing!, only: [:destroy]
  before_action only: [:show, :builds, :failures, :test_report, :manual_variables] do
    push_frontend_feature_flag(:ci_show_manual_variables_in_pipeline, project)
  end

  # Will be removed with https://gitlab.com/gitlab-org/gitlab/-/issues/225596
  before_action :redirect_for_legacy_scope_filter, only: [:index], if: -> { request.format.html? }

  around_action :allow_gitaly_ref_name_caching, only: [:index, :show]

  track_event :charts,
    name: 'p_analytics_pipelines',
    action: 'perform_analytics_usage_action',
    label: 'redis_hll_counters.analytics.analytics_total_unique_counts_monthly',
    destinations: %i[redis_hll snowplow]

  track_internal_event :charts, name: 'p_analytics_ci_cd_pipelines', conditions: -> { should_track_ci_cd_pipelines? }
  track_internal_event :charts, name: 'p_analytics_ci_cd_deployment_frequency', conditions: -> { should_track_ci_cd_deployment_frequency? }
  track_internal_event :charts, name: 'p_analytics_ci_cd_lead_time', conditions: -> { should_track_ci_cd_lead_time? }
  track_internal_event :charts, name: 'visit_ci_cd_time_to_restore_service_tab', conditions: -> { should_track_visit_ci_cd_time_to_restore_service_tab? }
  track_internal_event :charts, name: 'visit_ci_cd_failure_rate_tab', conditions: -> { should_track_visit_ci_cd_change_failure_tab? }

  wrap_parameters Ci::Pipeline

  POLLING_INTERVAL = 10_000

  feature_category :continuous_integration, [
    :charts, :show, :stage, :cancel, :retry,
    :builds, :failures, :status,
    :index, :new, :destroy, :manual_variables
  ]
  feature_category :pipeline_composition, [:create]
  feature_category :code_testing, [:test_report]
  feature_category :job_artifacts, [:downloadable_artifacts]

  def index
    @pipelines = Ci::PipelinesFinder
      .new(project, current_user, index_params)
      .execute
      .page(params[:page])

    @pipelines_count = limited_pipelines_count(project)

    respond_to do |format|
      format.html
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: POLLING_INTERVAL)

        render json: {
          pipelines: serialize_pipelines,
          count: {
            all: @pipelines_count
          }
        }
      end
    end
  end

  def new
    @pipeline = project.all_pipelines.new(ref: @project.default_branch)
  end

  def create
    service_response = Ci::CreatePipelineService
      .new(project, current_user, create_params)
      .execute(:web, ignore_skip_ci: true, save_on_errors: false)

    @pipeline = service_response.payload

    respond_to do |format|
      format.html do
        if service_response.success?
          redirect_to project_pipeline_path(project, @pipeline)
        else
          render 'new', status: :bad_request
        end
      end
      format.json do
        if service_response.success?
          render json: PipelineSerializer.new(project: project, current_user: current_user).represent(@pipeline),
            status: :created
        else
          bad_request_json = {
            errors: @pipeline.error_messages.map(&:content),
            warnings: @pipeline.warning_messages(limit: ::Gitlab::Ci::Warnings::MAX_LIMIT).map(&:content),
            total_warnings: @pipeline.warning_messages.length
          }
          render json: bad_request_json, status: :bad_request
        end
      end
    end
  end

  def show
    Gitlab::QueryLimiting.disable!('https://gitlab.com/gitlab-org/gitlab/-/issues/26657')

    respond_to do |format|
      format.html { render_show }
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: POLLING_INTERVAL)

        render json: PipelineSerializer
          .new(project: @project, current_user: @current_user)
          .represent(@pipeline, show_represent_params)
      end
    end
  end

  def destroy
    ::Ci::DestroyPipelineService.new(project, current_user).execute(pipeline)

    redirect_to project_pipelines_path(project), status: :see_other
  end

  def builds
    render_show
  end

  def failures
    if @pipeline.failed_builds.present?
      render_show
    else
      redirect_to pipeline_path(@pipeline)
    end
  end

  def status
    render json: PipelineSerializer
      .new(project: @project, current_user: @current_user)
      .represent_status(@pipeline)
  end

  def stage
    @stage = pipeline.stage(params[:stage])
    return not_found unless @stage

    return unless stage_stale?

    render json: StageSerializer
      .new(project: @project, current_user: @current_user)
      .represent(@stage, details: true, retried: params[:retried])
  end

  def retry
    # Check for access before execution to allow for async execution while still returning access results
    access_response = ::Ci::RetryPipelineService.new(@project, current_user).check_access(pipeline)

    if access_response.error?
      response = { json: { errors: [access_response.message] }, status: access_response.http_status }
    else
      response = { json: {}, status: :no_content }
      ::Ci::RetryPipelineWorker.perform_async(pipeline.id, current_user.id) # rubocop:disable CodeReuse/Worker
    end

    respond_to do |format|
      format.json do
        render response
      end
    end
  end

  def cancel
    ::Ci::CancelPipelineService.new(pipeline: pipeline, current_user: @current_user).execute

    respond_to do |format|
      format.html do
        redirect_back_or_default default: project_pipelines_path(project)
      end

      format.json { head :no_content }
    end
  end

  def test_report
    respond_to do |format|
      format.html do
        render_show
      end
      format.json do
        render json: TestReportSerializer
          .new(current_user: @current_user)
          .represent(pipeline_test_report, project: project, details: true)
      end
    end
  end

  def manual_variables
    return render_404 unless ::Feature.enabled?(:ci_show_manual_variables_in_pipeline, project)

    render_show
  end

  def downloadable_artifacts
    render json: Ci::DownloadableArtifactSerializer.new(
      project: project,
      current_user: current_user
    ).represent(@pipeline)
  end

  private

  def serialize_pipelines
    PipelineSerializer
      .new(project: @project, current_user: @current_user)
      .with_pagination(request, response)
      .represent(
        @pipelines,
        disable_coverage: true,
        disable_failed_builds: true,
        disable_manual_and_scheduled_actions: true,
        preload: true,
        preload_statuses: false,
        preload_downstream_statuses: false
      )
  end

  def render_show
    @stages = @pipeline.stages

    respond_to do |format|
      format.html do
        render 'show'
      end
    end
  end

  def show_represent_params
    { grouped: true, expanded: params[:expanded].to_a.map(&:to_i) }
  end

  def create_params
    params.require(:pipeline).permit(:ref, variables_attributes: %i[key variable_type secret_value])
  end

  def ensure_pipeline
    render_404 unless pipeline
  end

  def redirect_for_legacy_scope_filter
    return unless %w[running pending].include?(params[:scope])

    redirect_to url_for(safe_params.except(:scope).merge(status: safe_params[:scope])), status: :moved_permanently
  end

  def stage_stale?
    return true if Feature.disabled?(:pipeline_stage_set_last_modified, @current_user)

    last_modified = [@stage.updated_at.utc, @stage.statuses.maximum(:updated_at)].max

    stale?(last_modified: last_modified, etag: @stage)
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def pipeline
    return @pipeline if defined?(@pipeline)

    pipelines =
      if find_latest_pipeline?
        project.latest_pipelines(ref: params['ref'], limit: 100)
      else
        project.all_pipelines.id_in(params[:id])
      end

    @pipeline = pipelines
      .includes(builds: :tags, user: :status)
      .take
      &.present(current_user: current_user)

    @pipeline || not_found
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def set_pipeline_path
    @pipeline_path ||= if find_latest_pipeline?
                         latest_project_pipelines_path(@project, params['ref'])
                       else
                         project_pipeline_path(@project, @pipeline)
                       end
  end

  def find_latest_pipeline?
    params[:id].blank? && params[:latest]
  end

  def disable_query_limiting
    # Also see https://gitlab.com/gitlab-org/gitlab/-/issues/20785
    Gitlab::QueryLimiting.disable!('https://gitlab.com/gitlab-org/gitlab/-/issues/20784')
  end

  def authorize_update_pipeline!
    access_denied! unless can?(current_user, :update_pipeline, @pipeline)
  end

  def authorize_cancel_pipeline!
    access_denied! unless can?(current_user, :cancel_pipeline, @pipeline)
  end

  def authorize_read_build_on_pipeline!
    access_denied! unless can?(current_user, :read_build, @pipeline)
  end

  def limited_pipelines_count(project, scope = nil)
    finder = Ci::PipelinesFinder.new(project, current_user, index_params.merge(scope: scope))

    view_context.limited_counter_with_delimiter(finder.execute)
  end

  def pipeline_test_report
    strong_memoize(:pipeline_test_report) do
      @pipeline.test_reports.tap do |reports|
        reports.with_attachment! if params[:scope] == 'with_attachment'
      end
    end
  end

  def index_params
    params.permit(:scope, :username, :ref, :status, :source)
  end

  def should_track_ci_cd_pipelines?
    params[:chart].blank? || params[:chart] == 'pipelines'
  end

  def should_track_ci_cd_deployment_frequency?
    params[:chart] == 'deployment-frequency'
  end

  def should_track_ci_cd_lead_time?
    params[:chart] == 'lead-time'
  end

  def should_track_visit_ci_cd_time_to_restore_service_tab?
    params[:chart] == 'time-to-restore-service'
  end

  def should_track_visit_ci_cd_change_failure_tab?
    params[:chart] == 'change-failure-rate'
  end

  def tracking_namespace_source
    project.namespace
  end

  def tracking_project_source
    project
  end
end

class Admin::PlanLimitsController < Admin::ApplicationController
  include InternalRedirect

  before_action :set_plan_limits

  feature_category :not_owned # rubocop:todo Gitlab/AvoidFeatureCategoryNotOwned

  def create
    redirect_path = referer_path(request) || general_admin_application_settings_path

    respond_to do |format|
      if @plan_limits.update(plan_limits_params)
        format.json { head :ok }
        format.html { redirect_to redirect_path, notice: _('Application limits saved successfully') }
      else
        format.json { head :bad_request }
        format.html { render_update_error }
      end
    end
  end

  private

  def set_plan_limits
    @plan_limits = Plan.find(plan_limits_params[:plan_id]).actual_limits
  end

  def plan_limits_params
    params.require(:plan_limits)
      .permit(%i[
        plan_id
        conan_max_file_size
        helm_max_file_size
        maven_max_file_size
        npm_max_file_size
        nuget_max_file_size
        pypi_max_file_size
        terraform_module_max_file_size
        generic_packages_max_file_size
        ci_instance_level_variables
        ci_pipeline_size
        ci_active_jobs
        ci_project_subscriptions
        ci_pipeline_schedules
        ci_needs_size_limit
        ci_registered_group_runners
        ci_registered_project_runners
        dotenv_size
        dotenv_variables
        pipeline_hierarchy_size
      ])
  end
end

class Projects::JobsController < Projects::ApplicationController
  include Ci::AuthBuildTrace
  include SendFileUpload
  include ContinueParams
  include ProjectStatsRefreshConflictsGuard

  urgency :low, [:index, :show, :trace, :retry, :play, :cancel, :unschedule, :erase, :viewer, :raw, :test_report_summary]

  before_action :find_job_as_build, except: [:index, :play, :retry, :show]
  before_action :find_job_as_processable, only: [:play, :retry, :show]
  before_action :authorize_read_build_trace!, only: [:trace, :viewer, :raw]
  before_action :authorize_read_build!, except: [:test_report_summary]
  before_action :authorize_read_build_report_results!, only: [:test_report_summary]
  before_action :authorize_update_build!,
    except: [:index, :show, :viewer, :raw, :trace, :erase, :cancel, :unschedule, :test_report_summary]
  before_action :authorize_cancel_build!, only: [:cancel]
  before_action :authorize_erase_build!, only: [:erase]
  before_action :authorize_use_build_terminal!, only: [:terminal, :terminal_websocket_authorize]
  before_action :verify_api_request!, only: :terminal_websocket_authorize
  before_action :authorize_create_proxy_build!, only: :proxy_websocket_authorize
  before_action :verify_proxy_request!, only: :proxy_websocket_authorize
  before_action :reject_if_build_artifacts_size_refreshing!, only: [:erase]
  before_action :push_filter_by_name, only: [:index]
  layout 'project'

  feature_category :continuous_integration
  urgency :low

  def index; end

  def show
    if @build.instance_of?(::Ci::Bridge)
      redirect_to project_pipeline_path(@build.downstream_pipeline.project, @build.downstream_pipeline.id)
    end

    respond_to do |format|
      format.html
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render json: Ci::JobSerializer
          .new(project: @project, current_user: @current_user)
          .represent(
            @build.present(current_user: current_user),
            {
              # Pipeline will show all failed builds by default if not using disable_failed_builds
              disable_coverage: true,
              disable_failed_builds: true
            },
            BuildDetailsEntity
          )
      end
    end
  end

  def trace
    @build.trace.being_watched! if @build.running?

    if @build.has_trace?
      @build.trace.read do |stream|
        respond_to do |format|
          format.json do
            build_trace = Ci::BuildTrace.new(
              build: @build,
              stream: stream,
              state: params[:state])

            render json: BuildTraceSerializer
              .new(project: @project, current_user: @current_user)
              .represent(build_trace)
          end
        end
      end
    else
      head :no_content
    end
  end

  def retry
    Gitlab::QueryLimiting.disable!('https://gitlab.com/gitlab-org/gitlab/-/issues/424184')

    response = Ci::RetryJobService.new(project, current_user).execute(@build)

    if response.success?
      if @build.is_a?(::Ci::Build)
        redirect_to build_path(response[:job])
      else
        head :ok
      end
    else
      respond_422
    end
  end

  def play
    return respond_422 unless @build.playable?

    job = @build.play(current_user, play_params[:job_variables_attributes])

    if job.is_a?(Ci::Bridge)
      redirect_to pipeline_path(job.pipeline)
    else
      redirect_to build_path(job)
    end
  end

  def cancel
    service_response = Ci::BuildCancelService.new(@build, current_user).execute

    if service_response.success?
      destination = continue_params[:to].presence || builds_project_pipeline_path(@project, @build.pipeline.id)
      redirect_to destination
    elsif service_response.http_status == :forbidden
      access_denied!
    else
      head service_response.http_status
    end
  end

  def unschedule
    service_response = Ci::BuildUnscheduleService.new(@build, current_user).execute

    if service_response.success?
      redirect_to build_path(@build)
    elsif service_response.http_status == :forbidden
      access_denied!
    else
      head service_response.http_status
    end
  end

  def erase
    service_response = Ci::BuildEraseService.new(@build, current_user).execute

    if service_response.success?
      redirect_to project_job_path(project, @build), notice: _("Job has been successfully erased!")
    else
      head service_response.http_status
    end
  end

  def raw
    if @build.trace.archived?
      workhorse_set_content_type!
      send_upload(@build.job_artifacts_trace.file, send_params: raw_send_params, redirect_params: raw_redirect_params, proxy: params[:proxy])
    else
      @build.trace.read do |stream|
        if stream.file?
          workhorse_set_content_type!
          send_file stream.path, type: 'text/plain; charset=utf-8', disposition: 'inline'
        else
          # In this case we can't use workhorse_set_content_type! and let
          # Workhorse handle the response because the data is streamed directly
          # to the user but, because we have the trace content, we can calculate
          # the proper content type and disposition here.
          raw_data = stream.raw
          send_data raw_data, type: 'text/plain; charset=utf-8', disposition: raw_trace_content_disposition(raw_data), filename: 'job.log'
        end
      end
    end
  end

  def viewer; end

  def test_report_summary
    return not_found unless @build.report_results.present?

    summary = Gitlab::Ci::Reports::TestReportSummary.new(@build.report_results)

    respond_to do |format|
      format.json do
        render json: TestReportSummarySerializer
                       .new(project: project, current_user: @current_user)
                       .represent(summary)
      end
    end
  end

  def terminal; end

  # GET .../terminal.ws : implemented in gitlab-workhorse
  def terminal_websocket_authorize
    set_workhorse_internal_api_content_type
    render json: Gitlab::Workhorse.channel_websocket(@build.terminal_specification)
  end

  def proxy_websocket_authorize
    render json: proxy_websocket_service(build_service_specification)
  end

  private

  attr_reader :build

  def authorize_read_build_report_results!
    access_denied! unless can?(current_user, :read_build_report_results, build)
  end

  def authorize_update_build!
    access_denied! unless can?(current_user, :update_build, @build)
  end

  def authorize_cancel_build!
    access_denied! unless can?(current_user, :cancel_build, @build)
  end

  def authorize_erase_build!
    access_denied! unless can?(current_user, :erase_build, @build)
  end

  def authorize_use_build_terminal!
    access_denied! unless can?(current_user, :create_build_terminal, @build)
  end

  def authorize_create_proxy_build!
    access_denied! unless can?(current_user, :create_build_service_proxy, @build)
  end

  def verify_api_request!
    Gitlab::Workhorse.verify_api_request!(request.headers)
  end

  def verify_proxy_request!
    verify_api_request!
    set_workhorse_internal_api_content_type
  end

  def raw_send_params
    { type: 'text/plain; charset=utf-8', disposition: 'inline' }
  end

  def raw_redirect_params
    { query: { 'response-content-type' => 'text/plain; charset=utf-8', 'response-content-disposition' => 'inline' } }
  end

  def play_params
    params.permit(job_variables_attributes: %i[key secret_value])
  end

  def find_job_as_build
    @build = project.builds.find(params[:id])
  end

  def find_job_as_processable
    @build = project.processables.find(params[:id])
  end

  def build_path(build)
    project_job_path(build.project, build)
  end

  def raw_trace_content_disposition(raw_data)
    mime_type = Gitlab::Utils::MimeType.from_string(raw_data)

    # if mime_type is nil can also represent 'text/plain'
    return 'inline' if mime_type.nil? || mime_type == 'text/plain'

    'attachment'
  end

  def build_service_specification
    @build.service_specification(
      service: params['service'],
      port: params['port'],
      path: params['path'],
      subprotocols: proxy_subprotocol
    )
  end

  def proxy_subprotocol
    # This will allow to reuse the same subprotocol set
    # in the original websocket connection
    request.headers['HTTP_SEC_WEBSOCKET_PROTOCOL'].presence || ::Ci::BuildRunnerSession::TERMINAL_SUBPROTOCOL
  end

  # This method provides the information to Workhorse
  # about the service we want to proxy to.
  # For security reasons, in case this operation is started by JS,
  # it's important to use only sourced GitLab JS code
  def proxy_websocket_service(service)
    service[:url] = ::Gitlab::UrlHelpers.as_wss(service[:url])

    ::Gitlab::Workhorse.channel_websocket(service)
  end

  def push_filter_by_name
    push_frontend_feature_flag(:populate_and_use_build_names_table, @project)
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class BaseController < ActionController::Base
  def test_redirect
    params[:action] = :index
    #ruleid: ruby_redirect_rule-CheckRedirectTo
    redirect_to params
  end

  def redirect_to_strong_params
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to params.permit(:page, :sort) # should not warn
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to [params.permit(:domain)]
  end

  def test_only_path_wrong
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end
  def test_only_path_correct
    params.merge! :only_path => true
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to params
  end

  def wrong_redirect_only_path
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def redirect_only_path_with_unsafe_hash
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to(params.to_unsafe_hash.merge(:only_path => true, :display => nil))
  end

  def redirect_only_path_with_unsafe_h
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to(params.to_unsafe_h.merge(:only_path => true, :display => nil))
  end
end
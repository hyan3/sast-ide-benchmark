class Explore::ProjectsController < Explore::ApplicationController
  include PageLimiter
  include ParamsBackwardCompatibility
  include RendersMemberAccess
  include RendersProjectsList
  include SortingHelper
  include SortingPreference

  MIN_SEARCH_LENGTH = 3
  PAGE_LIMIT = 50
  RSS_ENTRIES_LIMIT = 20

  before_action :set_non_archived_param
  before_action :set_sorting

  # For background information on the limit, see:
  #   https://gitlab.com/gitlab-org/gitlab/-/issues/38357
  #   https://gitlab.com/gitlab-org/gitlab/-/issues/262682
  before_action only: [:index, :trending, :starred] do
    limit_pages(PAGE_LIMIT)
  end

  rescue_from PageOutOfBoundsError, with: :page_out_of_bounds

  feature_category :groups_and_projects
  # TODO: Set higher urgency after addressing https://gitlab.com/gitlab-org/gitlab/-/issues/357913
  # and https://gitlab.com/gitlab-org/gitlab/-/issues/358945
  urgency :low, [:index, :topics, :trending, :starred, :topic]

  def index
    show_alert_if_search_is_disabled
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  def trending
    params[:trending] = true
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def starred
    @projects = load_projects.reorder('star_count DESC')

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def topics
    load_project_counts
    load_topics
  end

  def topic
    load_topic

    return render_404 unless @topic

    params[:topic] = @topic.name
    @projects = load_projects

    respond_to do |format|
      format.html
      format.atom do
        @projects = @projects.projects_order_id_desc.limit(RSS_ENTRIES_LIMIT)
        render layout: 'xml'
      end
    end
  end

  private

  def load_project_counts
    @all_user_projects = ProjectsFinder.new(params: { non_public: true }, current_user: current_user).execute
    @all_starred_projects = ProjectsFinder.new(params: { starred: true }, current_user: current_user).execute
  end

  def load_projects
    load_project_counts

    finder_params = {
      minimum_search_length: MIN_SEARCH_LENGTH,
      not_aimed_for_deletion: true
    }

    projects = ProjectsFinder.new(current_user: current_user, params: params.merge(finder_params)).execute

    projects = preload_associations(projects)
    projects = projects.page(pagination_params[:page]).without_count

    prepare_projects_for_rendering(projects)
  end

  def load_topics
    @topics = Projects::TopicsFinder.new(
      params: params.permit(:search),
      organization_id: organization_id
    ).execute.page(pagination_params[:page]).without_count
  end

  def load_topic
    topic_name = if Feature.enabled?(:explore_topics_cleaned_path)
                   URI.decode_www_form_component(params[:topic_name])
                 else
                   params[:topic_name]
                 end

    @topic = Projects::Topic.for_organization(organization_id).find_by_name_case_insensitive(topic_name)
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def preload_associations(projects)
    projects.includes(:route, :creator, :group, :project_feature, :topics, namespace: [:route, :owner])
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def set_sorting
    params[:sort] = set_sort_order
    @sort = params[:sort]
  end

  def default_sort_order
    sort_value_latest_activity
  end

  def sorting_field
    Project::SORTING_PREFERENCE_FIELD
  end

  def page_out_of_bounds(error)
    load_project_counts
    @max_page_number = error.message

    respond_to do |format|
      format.html do
        render "page_out_of_bounds", status: :bad_request
      end

      format.json do
        render json: {
          html: view_to_html_string("explore/projects/page_out_of_bounds")
        }, status: :bad_request
      end
    end
  end

  def show_alert_if_search_is_disabled
    if current_user || (params[:name].blank? && params[:search].blank?) || !html_request? || Feature.disabled?(
      :disable_anonymous_project_search, type: :ops)
      return
    end

    flash.now[:notice] = _('You must sign in to search for specific projects.')
  end

  def organization_id
    ::Current.organization&.id
  end
end

class Explore::ProjectsController < Explore::ApplicationController
  include PageLimiter
  include ParamsBackwardCompatibility
  include RendersMemberAccess
  include RendersProjectsList
  include SortingHelper
  include SortingPreference

  MIN_SEARCH_LENGTH = 3
  PAGE_LIMIT = 50
  RSS_ENTRIES_LIMIT = 20

  before_action :set_non_archived_param
  before_action :set_sorting

  # For background information on the limit, see:
  #   https://gitlab.com/gitlab-org/gitlab/-/issues/38357
  #   https://gitlab.com/gitlab-org/gitlab/-/issues/262682
  before_action only: [:index, :trending, :starred] do
    limit_pages(PAGE_LIMIT)
  end

  rescue_from PageOutOfBoundsError, with: :page_out_of_bounds

  feature_category :groups_and_projects
  # TODO: Set higher urgency after addressing https://gitlab.com/gitlab-org/gitlab/-/issues/357913
  # and https://gitlab.com/gitlab-org/gitlab/-/issues/358945
  urgency :low, [:index, :topics, :trending, :starred, :topic]

  def index
    show_alert_if_search_is_disabled
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  def trending
    params[:trending] = true
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def starred
    @projects = load_projects.reorder('star_count DESC')

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def topics
    load_project_counts
    load_topics
  end

  def topic
    load_topic

    return render_404 unless @topic

    params[:topic] = @topic.name
    @projects = load_projects

    respond_to do |format|
      format.html
      format.atom do
        @projects = @projects.projects_order_id_desc.limit(RSS_ENTRIES_LIMIT)
        render layout: 'xml'
      end
    end
  end

  private

  def load_project_counts
    @all_user_projects = ProjectsFinder.new(params: { non_public: true }, current_user: current_user).execute
    @all_starred_projects = ProjectsFinder.new(params: { starred: true }, current_user: current_user).execute
  end

  def load_projects
    load_project_counts

    finder_params = {
      minimum_search_length: MIN_SEARCH_LENGTH,
      not_aimed_for_deletion: true
    }

    projects = ProjectsFinder.new(current_user: current_user, params: params.merge(finder_params)).execute

    projects = preload_associations(projects)
    projects = projects.page(pagination_params[:page]).without_count

    prepare_projects_for_rendering(projects)
  end

  def load_topics
    @topics = Projects::TopicsFinder.new(
      params: params.permit(:search),
      organization_id: organization_id
    ).execute.page(pagination_params[:page]).without_count
  end

  def load_topic
    topic_name = if Feature.enabled?(:explore_topics_cleaned_path)
                   URI.decode_www_form_component(params[:topic_name])
                 else
                   params[:topic_name]
                 end

    @topic = Projects::Topic.for_organization(organization_id).find_by_name_case_insensitive(topic_name)
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def preload_associations(projects)
    projects.includes(:route, :creator, :group, :project_feature, :topics, namespace: [:route, :owner])
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def set_sorting
    params[:sort] = set_sort_order
    @sort = params[:sort]
  end

  def default_sort_order
    sort_value_latest_activity
  end

  def sorting_field
    Project::SORTING_PREFERENCE_FIELD
  end

  def page_out_of_bounds(error)
    load_project_counts
    @max_page_number = error.message

    respond_to do |format|
      format.html do
        render "page_out_of_bounds", status: :bad_request
      end

      format.json do
        render json: {
          html: view_to_html_string("explore/projects/page_out_of_bounds")
        }, status: :bad_request
      end
    end
  end

  def show_alert_if_search_is_disabled
    if current_user || (params[:name].blank? && params[:search].blank?) || !html_request? || Feature.disabled?(
      :disable_anonymous_project_search, type: :ops)
      return
    end

    flash.now[:notice] = _('You must sign in to search for specific projects.')
  end

  def organization_id
    ::Current.organization&.id
  end
end

class BroadcastMessagesController < ApplicationController
    include Admin::BroadcastMessagesHelper

    before_action :find_broadcast_message, only: [:edit, :update, :destroy]
    before_action :find_broadcast_messages, only: [:index, :create]

    feature_category :notifications
    urgency :low

    def index
      @broadcast_message = System::BroadcastMessage.new
    end

    def edit; end

    def create
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      success = @broadcast_message.save

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully created.')
          else
            render :index
          end
        end
      end
    end

    def update
      success = @broadcast_message.update(broadcast_message_params)

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully updated.')
          else
            render :edit
          end
        end
      end
    end

    def destroy
      @broadcast_message.destroy

      respond_to do |format|
        format.html { redirect_back_or_default(default: { action: 'index' }) }
        format.js { head :ok }
      end
    end

    def preview
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      render plain: render_broadcast_message(@broadcast_message), status: :ok
    end

    protected

    def find_broadcast_message
      @broadcast_message = System::BroadcastMessage.find(params.permit(:id)[:id])
    end

    def find_broadcast_messages
      @broadcast_messages = System::BroadcastMessage.order(ends_at: :desc).page(pagination_params[:page]) # rubocop: disable CodeReuse/ActiveRecord
    end

    def broadcast_message_params
      params.require(:broadcast_message)
        .permit(%i[
          theme
          ends_at
          message
          starts_at
          target_path
          broadcast_type
          dismissable
          show_in_cli
        ], target_access_levels: []).reverse_merge!(target_access_levels: [])
    end
  end

class Projects::CommitController < Projects::ApplicationController
  include RendersNotes
  include CreatesCommit
  include DiffForPath
  include DiffHelper
  include SourcegraphDecorator
  include DiffsStreamResource

  # Authorize
  before_action :require_non_empty_project
  before_action :authorize_read_code!
  before_action :authorize_read_pipeline!, only: [:pipelines]
  before_action :commit
  before_action :define_commit_vars,
    only: [:show, :diff_for_path, :diff_files, :pipelines, :merge_requests, :rapid_diffs]
  before_action :define_commit_box_vars, only: [:show, :pipelines, :rapid_diffs]
  before_action :define_note_vars, only: [:show, :diff_for_path, :diff_files]
  before_action :authorize_edit_tree!, only: [:revert, :cherry_pick]
  before_action do
    push_frontend_feature_flag(:ci_graphql_pipeline_mini_graph, @project)
  end

  BRANCH_SEARCH_LIMIT = 1000
  COMMIT_DIFFS_PER_PAGE = 20

  feature_category :source_code_management
  urgency :low, [:pipelines, :merge_requests, :show, :rapid_diffs]

  def show
    apply_diff_view_cookie!

    respond_to do |format|
      format.html do
        @ref = params[:id]
        render locals: { pagination_params: params.permit(:page) }
      end
      format.diff do
        send_git_diff(@project.repository, @commit.diff_refs)
      end
      format.patch do
        send_git_patch(@project.repository, @commit.diff_refs)
      end
    end
  end

  def diff_for_path
    render_diff_for_path(@commit.diffs(diff_options))
  end

  def diff_files
    respond_to do |format|
      format.html do
        render template: 'projects/commit/diff_files',
          layout: false,
          locals: { diffs: @diffs, environment: @environment }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def pipelines
    @pipelines = @commit.pipelines.order(id: :desc)
    @pipelines = @pipelines.where(ref: params[:ref]) if params[:ref]
    @pipelines = @pipelines.page(params[:page])

    respond_to do |format|
      format.html
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render json: {
          pipelines: PipelineSerializer
            .new(project: @project, current_user: @current_user)
            .with_pagination(request, response)
            .represent(@pipelines),
          count: {
            all: @pipelines.count
          }
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def merge_requests
    @merge_requests = MergeRequestsFinder.new(
      current_user,
      project_id: @project.id,
      commit_sha: @commit.sha
    ).execute.map do |mr|
      { iid: mr.iid, path: merge_request_path(mr), title: mr.title }
    end

    respond_to do |format|
      format.json do
        render json: Gitlab::Json.dump(@merge_requests)
      end
    end
  end

  def branches
    return git_not_found! unless commit

    # branch_names_contains/tag_names_contains can take a long time when there are thousands of
    # branches/tags - each `git branch --contains xxx` request can consume a cpu core.
    # so only do the query when there are a manageable number of branches/tags
    @branches_limit_exceeded = @project.repository.branch_count > BRANCH_SEARCH_LIMIT
    @branches = @branches_limit_exceeded ? [] : @project.repository.branch_names_contains(commit.id)

    @tags_limit_exceeded = @project.repository.tag_count > BRANCH_SEARCH_LIMIT
    @tags = @tags_limit_exceeded ? [] : @project.repository.tag_names_contains(commit.id)
    render layout: false
  end

  def revert
    return render_404 unless @commit

    assign_change_commit_vars

    return render_404 if @start_branch.blank?

    @branch_name = create_new_branch? ? @commit.revert_branch_name : @start_branch

    create_commit(
      Commits::RevertService,
      success_notice: "The #{@commit.change_type_title(current_user)} has been successfully reverted.",
      success_path: -> { successful_change_path(@project) },
      failure_path: failed_change_path
    )
  end

  def cherry_pick
    return render_404 unless @commit

    assign_change_commit_vars

    return render_404 if @start_branch.blank?

    target_project = find_cherry_pick_target_project
    return render_404 unless target_project

    @branch_name = create_new_branch? ? @commit.cherry_pick_branch_name : @start_branch

    create_commit(
      Commits::CherryPickService,
      success_notice: "The #{@commit.change_type_title(current_user)} has been successfully " \
        "cherry-picked into #{@branch_name}.",
      success_path: -> { successful_change_path(target_project) },
      failure_path: failed_change_path,
      target_project: target_project
    )
  end

  def rapid_diffs
    return render_404 unless ::Feature.enabled?(:rapid_diffs, current_user, type: :wip)

    streaming_offset = 5
    @stream_url = diffs_stream_url(@commit, streaming_offset, diff_view)
    @diffs_slice = @commit.first_diffs_slice(streaming_offset, commit_diff_options)

    show
  end

  private

  def commit_diff_options
    opts = diff_options
    opts[:ignore_whitespace_change] = true if params[:format] == 'diff'
    opts[:use_extra_viewer_as_main] = false
    opts
  end

  def create_new_branch?
    params[:create_merge_request].present? || !can?(current_user, :push_code, @project)
  end

  def successful_change_path(target_project)
    referenced_merge_request_url || project_commits_url(target_project, @branch_name)
  end

  def failed_change_path
    referenced_merge_request_url || project_commit_url(@project, params[:id])
  end

  def referenced_merge_request_url
    if merge_request = @commit.merged_merge_request(current_user)
      project_merge_request_url(merge_request.target_project, merge_request)
    end
  end

  def commit
    @noteable = @commit ||= @project.commit_by(oid: params[:id]).tap do |commit|
      # preload author and their status for rendering
      commit&.author&.status
    end
  end

  def define_commit_vars
    return git_not_found! unless commit

    @diffs = commit.diffs(commit_diff_options)
    @environment = ::Environments::EnvironmentsByDeploymentsFinder.new(
      @project,
      current_user,
      commit: @commit,
      find_latest: true
    ).execute.last
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def define_note_vars
    @noteable = @commit
    @note = @project.build_commit_note(commit)

    @new_diff_note_attrs = {
      noteable_type: 'Commit',
      commit_id: @commit.id
    }

    @grouped_diff_discussions = commit.grouped_diff_discussions
    @discussions = commit.discussions

    if merge_request_iid = params[:merge_request_iid]
      @merge_request = MergeRequestsFinder.new(current_user, project_id: @project.id).find_by(iid: merge_request_iid)

      if @merge_request
        @new_diff_note_attrs.merge!(
          noteable_type: 'MergeRequest',
          noteable_id: @merge_request.id
        )

        merge_request_commit_notes = @merge_request.notes.where(commit_id: @commit.id).inc_relations_for_view
        merge_request_commit_diff_discussions = merge_request_commit_notes.grouped_diff_discussions(@commit.diff_refs)
        @grouped_diff_discussions.merge!(merge_request_commit_diff_discussions) do |line_code, left, right|
          left + right
        end
      end
    end

    @notes = (@grouped_diff_discussions.values.flatten + @discussions).flat_map(&:notes)
    @notes = prepare_notes_for_rendering(@notes)
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def define_commit_box_vars
    @last_pipeline = @commit.last_pipeline

    return unless @commit.last_pipeline

    @last_pipeline_stages = StageSerializer.new(
      project: @project,
      current_user: @current_user
    ).represent(@last_pipeline.stages)
  end

  def assign_change_commit_vars
    @start_branch = params[:start_branch]
    @commit_params = { commit: @commit }
  end

  def find_cherry_pick_target_project
    return @project if params[:target_project_id].blank?

    MergeRequestTargetProjectFinder
      .new(current_user: current_user, source_project: @project, project_feature: :repository)
      .execute
      .find_by_id(params[:target_project_id])
  end

  def append_info_to_payload(payload)
    super

    return unless action_name == 'show' && @diffs.present?

    payload[:metadata] ||= {}
    payload[:metadata]['meta.diffs_files_count'] = @diffs.size
  end

  def diffs_stream_resource_url(commit, offset, diff_view)
    diffs_stream_namespace_project_commit_path(
      namespace_id: commit.project.namespace.to_param,
      project_id: commit.project.to_param,
      id: commit.id,
      offset: offset,
      view: diff_view
    )
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
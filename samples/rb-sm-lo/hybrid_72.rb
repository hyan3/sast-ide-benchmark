class IssuesFinder < IssuableFinder
  extend ::Gitlab::Utils::Override

  def self.scalar_params
    @scalar_params ||= super + [:due_date]
  end

  def klass
    Issue
  end

  def params_class
    self.class.const_get(:Params, false)
  end

  private

  def filter_items(items)
    issues = super
    issues = by_due_date(issues)
    issues = by_due_after_or_before(issues)
    issues = by_confidential(issues)
    by_issue_types(issues)
  end

  # Negates all params found in `negatable_params`
  def filter_negated_items(items)
    issues = super
    by_negated_issue_types(issues)
  end

  override :filter_by_full_text_search
  def filter_by_full_text_search(items)
    # This project condition is used as a hint to PG about the partitions that need searching
    # because the search data is partitioned by project.
    # In certain cases, like the recent items search, the query plan is much better without this condition.
    return super if params[:skip_full_text_search_project_condition].present?

    super.with_projects_matching_search_data
  end

  def by_confidential(items)
    Issues::ConfidentialityFilter.new(
      current_user: current_user,
      params: original_params,
      parent: params.parent,
      assignee_filter: assignee_filter
    ).filter(items)
  end

  def by_due_after_or_before(items)
    items = items.due_after(params[:due_after]) if params[:due_after].present?
    items = items.due_before(params[:due_before]) if params[:due_before].present?

    items
  end

  def by_due_date(items)
    return items unless params.due_date?

    if params.filter_by_no_due_date?
      items.without_due_date
    elsif params.filter_by_any_due_date?
      items.with_due_date
    elsif params.filter_by_overdue?
      items.due_before(Date.today)
    elsif params.filter_by_due_today?
      items.due_today
    elsif params.filter_by_due_tomorrow?
      items.due_tomorrow
    elsif params.filter_by_due_this_week?
      items.due_between(Date.today.beginning_of_week, Date.today.end_of_week)
    elsif params.filter_by_due_this_month?
      items.due_between(Date.today.beginning_of_month, Date.today.end_of_month)
    elsif params.filter_by_due_next_month_and_previous_two_weeks?
      items.due_between(Date.today - 2.weeks, (Date.today + 1.month).end_of_month)
    else
      items.none
    end
  end

  def by_issue_types(items)
    issue_type_params = Array(params[:issue_types]).map(&:to_s)
    return items if issue_type_params.blank?
    return klass.none unless (WorkItems::Type.base_types.keys & issue_type_params).sort == issue_type_params.sort

    items.with_issue_type(params[:issue_types])
  end

  def by_negated_issue_types(items)
    issue_type_params = Array(not_params[:issue_types]).map(&:to_s) & WorkItems::Type.base_types.keys
    return items if issue_type_params.blank?

    items.without_issue_type(issue_type_params)
  end
end

class Projects::LabelsController < Projects::ApplicationController
  include ToggleSubscriptionAction

  before_action :check_issuables_available!
  before_action :label, only: [:edit, :update, :destroy, :promote]
  before_action :find_labels, only: [:index, :set_priorities, :remove_priority, :toggle_subscription]
  before_action :authorize_read_label!
  before_action :authorize_admin_labels!, only: [:new, :create, :edit, :update,
    :generate, :destroy, :remove_priority,
    :set_priorities]
  before_action :authorize_admin_group_labels!, only: [:promote]

  respond_to :js, :html

  feature_category :team_planning
  urgency :low

  def index
    respond_to do |format|
      format.html do
        @prioritized_labels = @available_labels.prioritized(@project)
        @labels = @available_labels.unprioritized(@project).page(params[:page])
        # preload group, project, and subscription data
        Preloaders::LabelsPreloader.new(@prioritized_labels, current_user, @project).preload_all
        Preloaders::LabelsPreloader.new(@labels, current_user, @project).preload_all
      end
      format.json do
        render json: LabelSerializer.new.represent_appearance(@available_labels)
      end
    end
  end

  def new
    @label = @project.labels.new
  end

  def create
    @label = Labels::CreateService.new(label_params).execute(project: @project)

    if @label.valid?
      respond_to do |format|
        format.html { redirect_to project_labels_path(@project) }
        format.json { render json: @label }
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: { message: @label.errors.messages }, status: :bad_request }
      end
    end
  end

  def edit; end

  def update
    @label = Labels::UpdateService.new(label_params).execute(@label)

    if @label.valid?
      redirect_to project_labels_path(@project)
    else
      render :edit
    end
  end

  def generate
    Gitlab::IssuesLabels.generate(@project)

    case params[:redirect]
    when 'issues'
      redirect_to project_issues_path(@project)
    when 'merge_requests'
      redirect_to project_merge_requests_path(@project)
    else
      redirect_to project_labels_path(@project)
    end
  end

  def destroy
    if @label.destroy
      redirect_to project_labels_path(@project), status: :found,
        notice: format(_('%{label_name} was removed'), label_name: @label.name)
    else
      redirect_to project_labels_path(@project), status: :found,
        alert: @label.errors.full_messages.to_sentence
    end
  end

  def remove_priority
    respond_to do |format|
      label = @available_labels.find(params[:id])

      if label.unprioritize!(project)
        format.json { render json: label }
      else
        format.json { head :unprocessable_entity }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def set_priorities
    Label.transaction do
      available_labels_ids = @available_labels.where(id: params[:label_ids]).pluck(:id)
      label_ids = params[:label_ids].select { |id| available_labels_ids.include?(id.to_i) }

      label_ids.each_with_index do |label_id, index|
        label = @available_labels.find(label_id)
        label.prioritize!(project, index)
      end
    end

    respond_to do |format|
      format.json { render json: { message: 'success' } }
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def promote
    promote_service = Labels::PromoteService.new(@project, @current_user)

    begin
      return render_404 unless promote_service.execute(@label)

      flash[:notice] = flash_notice_for(@label, @project.group)
      respond_to do |format|
        format.html do
          redirect_to(project_labels_path(@project), status: :see_other)
        end
        format.json do
          render json: { url: project_labels_path(@project) }
        end
      end
    rescue ActiveRecord::RecordInvalid => e
      Gitlab::AppLogger.error "Failed to promote label \"#{@label.title}\" to group label"
      Gitlab::AppLogger.error e

      respond_to do |format|
        format.html do
          redirect_to(
            project_labels_path(@project),
            notice: _('Failed to promote label due to internal error. Please contact administrators.'))
        end
        format.js
      end
    end
  end

  def flash_notice_for(label, group)
    ''.html_safe + "#{label.title} promoted to " + view_context.link_to('<u>group label</u>'.html_safe, group_labels_path(group)) + '.'
  end

  protected

  def label_params
    allowed = [:title, :description, :color]
    allowed << :lock_on_merge if @project.supports_lock_on_merge?

    params.require(:label).permit(allowed)
  end

  def label
    @label ||= @project.labels.find(params[:id])
  end

  def subscribable_resource
    @available_labels.find(params[:id])
  end

  def find_labels
    @available_labels ||= LabelsFinder.new(
      current_user,
      project_id: @project.id,
      include_ancestor_groups: true,
      search: params[:search],
      subscribed: params[:subscribed],
      sort: sort
    ).execute
  end

  def sort
    @sort ||= params[:sort] || 'name_asc'
  end

  def authorize_admin_labels!
    render_404 unless can?(current_user, :admin_label, @project)
  end

  def authorize_admin_group_labels!
    render_404 unless can?(current_user, :admin_label, @project.group)
  end
end

class OrganizationsController < ApplicationController
    include PreviewMarkdown
    include FiltersEvents

    DEFAULT_RESOURCE_LIMIT = 1000
    DEFAULT_ACTIVITY_EVENT_LIMIT = 20

    feature_category :cell

    before_action :event_filter, only: [:activity]
    before_action :authorize_read_organization!, only: [:activity, :show, :groups_and_projects]
    before_action only: [:index] do
      push_frontend_feature_flag(:allow_organization_creation, current_user)
    end

    skip_before_action :authenticate_user!, only: [:activity, :show, :groups_and_projects]

    urgency :low, [:activity]

    def index; end

    def new
      authorize_create_organization!
    end

    def show; end

    def activity
      respond_to do |format|
        format.html
        format.json do
          load_events
          # load_events queries for limit + 1.
          # This will be removed as part of https://gitlab.com/gitlab-org/gitlab/-/issues/382473
          has_next_page = @events.length > activity_query_limit
          @events.pop if has_next_page

          @events = @events.select { |event| event.visible_to_user?(current_user) }

          render json: \
            { events: ::Profile::EventSerializer.new(current_user: current_user).represent(@events), \
              has_next_page: has_next_page }
        end
      end
    end

    def groups_and_projects; end

    def users
      authorize_read_organization_user!
    end

    private

    def activity_query_limit
      return params[:limit].to_i unless !params[:limit] || params[:limit].to_i > DEFAULT_ACTIVITY_EVENT_LIMIT

      DEFAULT_ACTIVITY_EVENT_LIMIT
    end

    def load_events
      @events = EventCollection.new(
        organization.projects.limit(DEFAULT_RESOURCE_LIMIT).sorted_by_activity,
        offset: params[:offset].to_i,
        filter: event_filter,
        # limit + 1 allows us to determine if we have another page.
        # This will be removed as part of https://gitlab.com/gitlab-org/gitlab/-/issues/382473
        limit: activity_query_limit + 1,
        groups: organization.groups.limit(DEFAULT_RESOURCE_LIMIT)
      ).to_a.map(&:present)

      Events::RenderService.new(current_user).execute(@events)
    end
  end

class Projects::EnvironmentsController < Projects::ApplicationController
  include ProductAnalyticsTracking
  include KasCookie

  MIN_SEARCH_LENGTH = 3
  ACTIVE_STATES = %i[available stopping].freeze
  SCOPES_TO_STATES = { "active" => ACTIVE_STATES, "stopped" => %i[stopped] }.freeze

  layout 'project'

  before_action only: [:folder] do
    push_frontend_feature_flag(:environments_folder_new_look, project)
  end

  before_action only: [:show] do
    push_frontend_feature_flag(:k8s_tree_view, project)
    push_frontend_feature_flag(:use_websocket_for_k8s_watch, project)
  end

  before_action :authorize_read_environment!
  before_action :authorize_create_environment!, only: [:new, :create]
  before_action :authorize_stop_environment!, only: [:stop]
  before_action :authorize_update_environment!, only: [:edit, :update, :cancel_auto_stop]
  before_action :authorize_admin_environment!, only: [:terminal, :terminal_websocket_authorize]
  before_action :environment,
    only: [:show, :edit, :update, :stop, :terminal, :terminal_websocket_authorize, :cancel_auto_stop, :k8s]
  before_action :verify_api_request!, only: :terminal_websocket_authorize
  before_action :expire_etag_cache, only: [:index], unless: -> { request.format.json? }
  before_action :set_kas_cookie, only: [:edit, :new, :show, :k8s], if: -> { current_user && request.format.html? }
  after_action :expire_etag_cache, only: [:cancel_auto_stop]

  track_event :index, :folder, :show, :new, :edit, :create, :update, :stop, :cancel_auto_stop, :terminal, :k8s,
    name: 'users_visiting_environments_pages'

  feature_category :continuous_delivery
  urgency :low

  def index
    @project = ProjectPresenter.new(project, current_user: current_user)

    respond_to do |format|
      format.html
      format.json do
        states = SCOPES_TO_STATES.fetch(params[:scope], ACTIVE_STATES)
        @environments = search_environments.with_state(states)

        environments_count_by_state = search_environments.count_by_state

        Gitlab::PollingInterval.set_header(response, interval: 3_000)
        render json: {
          environments: serialize_environments(request, response, params[:nested]),
          review_app: serialize_review_app,
          can_stop_stale_environments: can?(current_user, :stop_environment, @project),
          available_count: environments_count_by_state[:available],
          active_count: environments_count_by_state[:available] + environments_count_by_state[:stopping],
          stopped_count: environments_count_by_state[:stopped]
        }
      end
    end
  end

  # Returns all environments for a given folder
  # rubocop: disable CodeReuse/ActiveRecord
  def folder
    @folder = params[:id]

    respond_to do |format|
      format.html
      format.json do
        states = SCOPES_TO_STATES.fetch(params[:scope], ACTIVE_STATES)
        folder_environments = search_environments(type: params[:id])

        @environments = folder_environments.with_state(states)
          .order(:name)

        render json: {
          environments: serialize_environments(request, response),
          available_count: folder_environments.available.count,
          active_count: folder_environments.active.count,
          stopped_count: folder_environments.stopped.count
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def show; end

  def new
    @environment = project.environments.new
  end

  def edit; end

  def k8s
    render action: :show
  end

  def create
    @environment = project.environments.create(environment_params)

    if @environment.persisted?
      render json: { environment: @environment, path: project_environment_path(project, @environment) }
    else
      render json: { message: @environment.errors.full_messages }, status: :bad_request
    end
  end

  def update
    if @environment.update(environment_params)
      render json: { environment: @environment, path: project_environment_path(project, @environment) }
    else
      render json: { message: @environment.errors.full_messages }, status: :bad_request
    end
  end

  def stop
    return render_404 unless @environment.available?

    service_response = Environments::StopService.new(project, current_user).execute(@environment)
    return render_403 unless service_response.success?

    job = service_response[:actions].first if service_response[:actions]&.count == 1

    action_or_env_url =
      if job
        project_job_url(project, job)
      else
        project_environment_url(project, @environment)
      end

    respond_to do |format|
      format.html { redirect_to action_or_env_url }
      format.json { render json: { redirect_url: action_or_env_url } }
    end
  end

  def cancel_auto_stop
    result = Environments::ResetAutoStopService.new(project, current_user)
                                               .execute(environment)

    if result[:status] == :success
      respond_to do |format|
        message = _('Auto stop successfully canceled.')

        format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
        format.json { render json: { message: message }, status: :ok }
      end
    else
      respond_to do |format|
        message = result[:message]

        format.html { redirect_back_or_default(default: { action: 'show' }, options: { alert: message }) }
        format.json { render json: { message: message }, status: :unprocessable_entity }
      end
    end
  end

  def terminal
    # Currently, this acts as a hint to load the terminal details into the cache
    # if they aren't there already. In the future, users will need these details
    # to choose between terminals to connect to.
    @terminals = environment.terminals
  end

  # GET .../terminal.ws : implemented in gitlab-workhorse
  def terminal_websocket_authorize
    # Just return the first terminal for now. If the list is in the process of
    # being looked up, this may result in a 404 response, so the frontend
    # should retry those errors
    terminal = environment.terminals.try(:first)
    if terminal
      set_workhorse_internal_api_content_type
      render json: Gitlab::Workhorse.channel_websocket(terminal)
    else
      render html: 'Not found', status: :not_found
    end
  end

  def search
    respond_to do |format|
      format.json do
        environment_names = search_environment_names

        render json: environment_names, status: environment_names.any? ? :ok : :no_content
      end
    end
  end

  private

  def deployments
    environment
      .deployments
      .with_environment_page_associations
      .ordered
      .page(params[:page])
  end

  def verify_api_request!
    Gitlab::Workhorse.verify_api_request!(request.headers)
  end

  def expire_etag_cache
    # this forces to reload json content
    Gitlab::EtagCaching::Store.new.tap do |store|
      store.touch(project_environments_path(project, format: :json))
    end
  end

  def allowed_environment_attributes
    attributes = [:external_url]
    attributes << :name if action_name == "create"
    attributes
  end

  def environment_params
    params.require(:environment).permit(allowed_environment_attributes)
  end

  def environment
    @environment ||= project.environments.find(params[:id])
  end

  def search_environments(type: nil)
    search = params[:search] if params[:search] && params[:search].length >= MIN_SEARCH_LENGTH

    @search_environments ||= Environments::EnvironmentsFinder.new(
      project,
      current_user,
      type: type,
      search: search
    ).execute
  end

  def include_all_dashboards?
    !params[:embedded]
  end

  def search_environment_names
    return [] unless params[:query]

    project.environments.for_name_like(params[:query]).pluck_names
  end

  def serialize_environments(request, response, nested = false)
    EnvironmentSerializer
      .new(project: @project, current_user: @current_user)
      .tap { |serializer| serializer.within_folders if nested }
      .with_pagination(request, response)
      .represent(@environments)
  end

  def serialize_review_app
    ReviewAppSetupSerializer.new(current_user: @current_user).represent(@project)
  end

  def authorize_stop_environment!
    access_denied! unless can?(current_user, :stop_environment, environment)
  end

  def authorize_update_environment!
    access_denied! unless can?(current_user, :update_environment, environment)
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def test_send_file
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file2
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file4
    # ruleid: ruby_file_rule-CheckSendFile
    send_file cookies.permanent[:something]
end

def test_send_file5
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.encrypted[:something]
end

def test_send_file6
    # this is reported since semgrep 0.94 because . ... . can now match
    # intermediate fields, not just method calls.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file7
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.permanent.signed[:something]
end

def test_send_file8
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file_ok
    # ok: ruby_file_rule-CheckSendFile
    send_file "some_safe_file.txt"
end
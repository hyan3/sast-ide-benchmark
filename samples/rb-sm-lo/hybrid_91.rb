class BaseResolver < GraphQL::Schema::Resolver
    extend ::Gitlab::Utils::Override
    include ::Gitlab::Utils::StrongMemoize

    argument_class ::Types::BaseArgument

    def self.requires_argument!
      @requires_argument = true
    end

    def self.requires_argument?
      !!@requires_argument
    end

    def self.calls_gitaly!
      @calls_gitaly = true
    end

    def self.calls_gitaly?
      !!@calls_gitaly
    end

    # This is a flag to allow us to use `complexity_multiplier` to compute complexity for connection
    # fields(see BaseField#connection_complexity_multiplier) in resolvers that do external connection pagination,
    # thus disabling the default `connection` option.
    def self.calculate_ext_conn_complexity
      false
    end

    def self.singular_type
      return unless type

      unwrapped = type.unwrap

      %i[node_type relay_node_type of_type itself].reduce(nil) do |t, m|
        t || unwrapped.try(m)
      end
    end

    def self.when_single(&block)
      as_single << block

      # Have we been called after defining the single version of this resolver?
      @single.instance_exec(&block) if @single.present?
    end

    def self.as_single
      @as_single ||= []
    end

    def self.single_definition_blocks
      ancestors.flat_map { |klass| klass.try(:as_single) || [] }
    end

    def self.single
      @single ||= begin
        parent = self
        klass = Class.new(self) do
          type parent.singular_type, null: true

          def ready?(**args)
            value = super

            if value.is_a?(Array)
              [value[0], select_result(value[1])]
            else
              value
            end
          end

          def resolve(**args)
            select_result(super)
          end

          def single?
            true
          end

          def select_result(results)
            results&.first
          end

          define_singleton_method :to_s do
            "#{parent}.single"
          end
        end

        single_definition_blocks.each do |definition|
          klass.instance_exec(&definition)
        end

        klass
      end
    end

    def self.last
      parent = self
      @last ||= Class.new(single) do
        type parent.singular_type, null: true

        def select_result(results)
          results&.last
        end

        define_singleton_method :to_s do
          "#{parent}.last"
        end
      end
    end

    def self.complexity
      0
    end

    def self.resolver_complexity(args, child_complexity:)
      complexity = 1
      complexity += 1 if args[:sort]
      complexity += 5 if args[:search]

      complexity
    end

    def self.complexity_multiplier(args)
      # When fetching many items, additional complexity is added to the field
      # depending on how many items is fetched. For each item we add 1% of the
      # original complexity - this means that loading 100 items (our default
      # max_page_size limit) doubles the original complexity.
      #
      # Complexity is not increased when searching by specific ID(s), because
      # complexity difference is minimal in this case.
      [args[:iid], args[:iids]].any? ? 0 : 0.01
    end

    def self.before_connection_authorization(&block)
      @before_connection_authorization_block = block
    end

    # rubocop: disable Style/TrivialAccessors
    def self.before_connection_authorization_block
      @before_connection_authorization_block
    end
    # rubocop: enable Style/TrivialAccessors

    def offset_pagination(relation)
      ::Gitlab::Graphql::Pagination::OffsetPaginatedRelation.new(relation)
    end

    override :object
    def object
      super.tap do |obj|
        # If the field this resolver is used in is wrapped in a presenter, unwrap its subject
        break obj.__subject__ if obj.is_a?(Gitlab::View::Presenter::Base)
      end
    end

    def single?
      false
    end

    def current_user
      context[:current_user]
    end

    # Overridden in sub-classes (see .single, .last)
    def select_result(results)
      results
    end

    def self.authorization
      @authorization ||= ::Gitlab::Graphql::Authorize::ObjectAuthorization.new(try(:required_permissions))
    end

    def self.authorized?(object, context)
      authorization.ok?(object, context[:current_user], scope_validator: context[:scope_validator])
    end
  end

class Projects::MirrorsController < Projects::ApplicationController
  include RepositorySettingsRedirect

  # Authorize
  before_action :remote_mirror, only: [:update]
  before_action :check_mirror_available!
  before_action :authorize_admin_project!

  layout "project_settings"

  feature_category :source_code_management

  def show
    redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
  end

  def update
    if push_mirror_create_or_destroy?
      result = execute_push_mirror_service

      if result.success?
        flash[:notice] = notice_message
      else
        flash[:alert] = alert_error(result.message)
      end

      respond_to do |format|
        format.html { redirect_to_repository_settings(project, anchor: 'js-push-remote-settings') }
        format.json do
          if result.error?
            render json: result.message, status: :unprocessable_entity
          else
            render json: ProjectMirrorSerializer.new.represent(project)
          end
        end
      end
    else
      flash[:alert] = alert_error('Invalid mirror update request')

      respond_to do |format|
        format.html { redirect_to_repository_settings(project, anchor: 'js-push-remote-settings') }
        format.json do
          render json: { error: flash[:alert] }, status: :bad_request
        end
      end
    end
  end

  def update_now
    if params[:sync_remote]
      project.update_remote_mirrors
      flash[:notice] = _("The remote repository is being updated...")
    end

    redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
  end

  def ssh_host_keys
    lookup = SshHostKey.new(project: project, url: params[:ssh_url], compare_host_keys: params[:compare_host_keys])

    if lookup.error.present?
      # Failed to read keys
      render json: { message: lookup.error }, status: :bad_request
    elsif lookup.known_hosts.nil?
      # Still working, come back later
      render body: nil, status: :no_content
    else
      render json: lookup
    end
  rescue ArgumentError => e
    render json: { message: e.message }, status: :bad_request
  end

  private

  def push_mirror_create_or_destroy?
    push_mirror_create? || push_mirror_destroy?
  end

  def push_mirror_create?
    push_mirror_attributes.present?
  end

  def push_mirror_destroy?
    ::Gitlab::Utils.to_boolean(mirror_params.dig(:remote_mirrors_attributes, '_destroy'))
  end

  def push_mirror_attributes
    mirror_params.dig(:remote_mirrors_attributes, '0')
  end

  def execute_push_mirror_service
    if push_mirror_create?
      return ::RemoteMirrors::CreateService.new(project, current_user, push_mirror_attributes).execute
    end

    return unless push_mirror_destroy?

    ::RemoteMirrors::DestroyService.new(project, current_user).execute(push_mirror_to_destroy)
  end

  def safe_mirror_params
    mirror_params
  end

  def notice_message
    _('Mirroring settings were successfully updated.')
  end

  def push_mirror_to_destroy
    push_mirror_to_destroy_id = safe_mirror_params.dig(:remote_mirrors_attributes, 'id')

    project.remote_mirrors.find(push_mirror_to_destroy_id)
  end

  def remote_mirror
    @remote_mirror = project.remote_mirrors.first_or_initialize
  end

  def check_mirror_available!
    render_404 unless can?(current_user, :admin_remote_mirror, project)
  end

  def mirror_params_attributes
    [
      remote_mirrors_attributes: %i[
        url
        id
        enabled
        only_protected_branches
        keep_divergent_refs
        auth_method
        user
        password
        ssh_known_hosts
        regenerate_ssh_private_key
        _destroy
      ]
    ]
  end

  def mirror_params
    params.require(:project).permit(mirror_params_attributes)
  end

  def alert_error(error)
    return error.full_messages.to_sentence if error.respond_to?(:full_messages)

    error
  end
end

class Projects::MirrorsController < Projects::ApplicationController
  include RepositorySettingsRedirect

  # Authorize
  before_action :remote_mirror, only: [:update]
  before_action :check_mirror_available!
  before_action :authorize_admin_project!

  layout "project_settings"

  feature_category :source_code_management

  def show
    redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
  end

  def update
    if push_mirror_create_or_destroy?
      result = execute_push_mirror_service

      if result.success?
        flash[:notice] = notice_message
      else
        flash[:alert] = alert_error(result.message)
      end

      respond_to do |format|
        format.html { redirect_to_repository_settings(project, anchor: 'js-push-remote-settings') }
        format.json do
          if result.error?
            render json: result.message, status: :unprocessable_entity
          else
            render json: ProjectMirrorSerializer.new.represent(project)
          end
        end
      end
    else
      flash[:alert] = alert_error('Invalid mirror update request')

      respond_to do |format|
        format.html { redirect_to_repository_settings(project, anchor: 'js-push-remote-settings') }
        format.json do
          render json: { error: flash[:alert] }, status: :bad_request
        end
      end
    end
  end

  def update_now
    if params[:sync_remote]
      project.update_remote_mirrors
      flash[:notice] = _("The remote repository is being updated...")
    end

    redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
  end

  def ssh_host_keys
    lookup = SshHostKey.new(project: project, url: params[:ssh_url], compare_host_keys: params[:compare_host_keys])

    if lookup.error.present?
      # Failed to read keys
      render json: { message: lookup.error }, status: :bad_request
    elsif lookup.known_hosts.nil?
      # Still working, come back later
      render body: nil, status: :no_content
    else
      render json: lookup
    end
  rescue ArgumentError => e
    render json: { message: e.message }, status: :bad_request
  end

  private

  def push_mirror_create_or_destroy?
    push_mirror_create? || push_mirror_destroy?
  end

  def push_mirror_create?
    push_mirror_attributes.present?
  end

  def push_mirror_destroy?
    ::Gitlab::Utils.to_boolean(mirror_params.dig(:remote_mirrors_attributes, '_destroy'))
  end

  def push_mirror_attributes
    mirror_params.dig(:remote_mirrors_attributes, '0')
  end

  def execute_push_mirror_service
    if push_mirror_create?
      return ::RemoteMirrors::CreateService.new(project, current_user, push_mirror_attributes).execute
    end

    return unless push_mirror_destroy?

    ::RemoteMirrors::DestroyService.new(project, current_user).execute(push_mirror_to_destroy)
  end

  def safe_mirror_params
    mirror_params
  end

  def notice_message
    _('Mirroring settings were successfully updated.')
  end

  def push_mirror_to_destroy
    push_mirror_to_destroy_id = safe_mirror_params.dig(:remote_mirrors_attributes, 'id')

    project.remote_mirrors.find(push_mirror_to_destroy_id)
  end

  def remote_mirror
    @remote_mirror = project.remote_mirrors.first_or_initialize
  end

  def check_mirror_available!
    render_404 unless can?(current_user, :admin_remote_mirror, project)
  end

  def mirror_params_attributes
    [
      remote_mirrors_attributes: %i[
        url
        id
        enabled
        only_protected_branches
        keep_divergent_refs
        auth_method
        user
        password
        ssh_known_hosts
        regenerate_ssh_private_key
        _destroy
      ]
    ]
  end

  def mirror_params
    params.require(:project).permit(mirror_params_attributes)
  end

  def alert_error(error)
    return error.full_messages.to_sentence if error.respond_to?(:full_messages)

    error
  end
end

class Projects::GraphsController < Projects::ApplicationController
  include ExtractsPath
  include ProductAnalyticsTracking

  # Authorize
  before_action :require_non_empty_project
  before_action :assign_ref_vars
  before_action :authorize_read_repository_graphs!

  track_event :charts,
    name: 'p_analytics_repo',
    action: 'perform_analytics_usage_action',
    label: 'redis_hll_counters.analytics.analytics_total_unique_counts_monthly',
    destinations: %i[redis_hll snowplow]

  feature_category :source_code_management, [:show, :commits, :languages, :charts]
  urgency :low, [:show]

  feature_category :continuous_integration, [:ci]
  urgency :low, [:ci]

  MAX_COMMITS = 6000

  def show
    @ref_type = ref_type

    respond_to do |format|
      format.html
      format.json do
        commits = @project.repository.commits(ref, limit: MAX_COMMITS, skip_merges: true)
        log = commits.map do |commit|
          {
            author_name: commit.author_name,
            author_email: commit.author_email,
            date: commit.committed_date.to_date.iso8601
          }
        end

        render json: Gitlab::Json.dump(log)
      end
    end
  end

  def commits
    redirect_to action: 'charts'
  end

  def languages
    redirect_to action: 'charts'
  end

  def charts
    get_commits
    get_languages
    get_daily_coverage_options
  end

  def ci
    redirect_to charts_project_pipelines_path(@project)
  end

  private

  def ref
    @fully_qualified_ref || @ref
  end

  def get_commits
    @commits_limit = 2000
    @commits = @project.repository.commits(ref, limit: @commits_limit, skip_merges: true)
    @commits_graph = Gitlab::Graphs::Commits.new(@commits)
    @commits_per_week_days = @commits_graph.commits_per_week_days
    @commits_per_time = @commits_graph.commits_per_time
    @commits_per_month = @commits_graph.commits_per_month
  end

  def get_languages
    @languages =
      ::Projects::RepositoryLanguagesService.new(@project, current_user).execute.map do |lang|
        { value: lang.share, label: lang.name, color: lang.color, highlight: lang.color }
      end
  end

  def get_daily_coverage_options
    return unless can?(current_user, :read_build_report_results, project)

    date_today = Date.current
    report_window = ::Ci::DailyBuildGroupReportResultsFinder::REPORT_WINDOW

    @daily_coverage_options = {
      base_params: {
        start_date: date_today - report_window,
        end_date: date_today,
        ref_path: @project.repository.expand_ref(ref),
        param_type: 'coverage'
      },
      download_path: namespace_project_ci_daily_build_group_report_results_path(
        namespace_id: @project.namespace,
        project_id: @project,
        format: :csv
      ),
      graph_api_path: namespace_project_ci_daily_build_group_report_results_path(
        namespace_id: @project.namespace,
        project_id: @project,
        format: :json
      )
    }
  end

  def tracking_namespace_source
    project.namespace
  end

  def tracking_project_source
    project
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Test
    $key = 512
    $pass1 = 2048

    def initialize(key = nil, iv = nil)
        @key2 = 512
        @pass2 = 2048
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-InsufficientRSAKeySize
        OpenSSL::PKey::RSA.new 512
        bad
        bad1
        ok
    end

    def bad
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    end

    def bad1
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    end


    def ok
        # ok: ruby_crypto_rule-InsufficientRSAKeySize
        key = OpenSSL::PKey::RSA.new($pass1)
        # ok: ruby_crypto_rule-InsufficientRSAKeySize
        key = OpenSSL::PKey::RSA.new(@pass2)
        # ok: ruby_crypto_rule-InsufficientRSAKeySize
        key = OpenSSL::PKey::RSA.new(2048)
    end
end
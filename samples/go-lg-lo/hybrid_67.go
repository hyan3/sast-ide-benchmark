func TestNew(t *testing.T) {
	t.Run("no config", func(t *testing.T) {
		adapter, err := New(&common.CacheConfig{}, time.Second, "bucket")
		require.ErrorContains(t, err, "missing GCS configuration")
		require.Nil(t, adapter)
	})

	t.Run("valid", func(t *testing.T) {
		adapter, err := New(&common.CacheConfig{GCS: &common.CacheGCSConfig{}}, time.Second, "bucket")
		require.NoError(t, err)
		require.NotNil(t, adapter)
	})
}

func TestImportCommento(t *testing.T) {
	failTestOnError(t, setupTestEnv())

	// Create JSON data
	data := commentoExportV1{
		Version: 1,
		Comments: []comment{
			{
				CommentHex:   "5a349182b3b8e25107ab2b12e514f40fe0b69160a334019491d7c204aff6fdc2",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a reply!",
				Html:         "",
				ParentHex:    "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:08:44.061525Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a comment!",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:07:49.244432Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "a7c84f251b5a09d5b65e902cbe90633646437acefa3a52b761fee94002ac54c7",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Markdown:     "This is a test comment, bar foo\n\n#Here is something big\n\n```\nhere code();\n```",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:20:21.101653Z"),
				Direction:    0,
				Deleted:      false,
			},
		},
		Commenters: []commenter{
			{
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Email:        "john@doe.com",
				Name:         "John Doe",
				Link:         "https://john.doe",
				Photo:        "undefined",
				Provider:     "commento",
				JoinDate:     timeParse(t, "2020-01-27T14:17:59.298737Z"),
				IsModerator:  false,
			},
		},
	}

	// Create listener with random port
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Errorf("couldn't create listener: %v", err)
		return
	}
	defer func() {
		_ = listener.Close()
	}()
	port := listener.Addr().(*net.TCPAddr).Port

	// Launch http server serving commento json gzipped data
	go func() {
		http.Serve(listener, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			gzipper := gzip.NewWriter(w)
			defer func() {
				_ = gzipper.Close()
			}()
			encoder := json.NewEncoder(gzipper)
			if err := encoder.Encode(data); err != nil {
				t.Errorf("couldn't write data: %v", err)
			}
		}))
	}()
	url := fmt.Sprintf("http://127.0.0.1:%d", port)

	domainNew("temp-owner-hex", "Example", "example.com")

	n, err := domainImportCommento("example.com", url)
	if err != nil {
		t.Errorf("unexpected error importing comments: %v", err)
		return
	}
	if n != len(data.Comments) {
		t.Errorf("imported comments missmatch (got %d, want %d)", n, len(data.Comments))
	}
}

func TestProcessRunner_BuildLimit(t *testing.T) {
	hook, cleanup := test.NewHook()
	defer cleanup()

	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(io.Discard)

	cfg := common.RunnerConfig{
		Limit:              2,
		RequestConcurrency: 10,
		RunnerSettings: common.RunnerSettings{
			Executor: "multi-runner-build-limit",
		},
		SystemIDState: common.NewSystemIDState(),
	}

	require.NoError(t, cfg.SystemIDState.EnsureSystemID())

	jobData := common.JobResponse{
		ID: 1,
		Steps: []common.Step{
			{
				Name:         "sleep",
				Script:       common.StepScript{"sleep 10"},
				Timeout:      15,
				When:         "",
				AllowFailure: false,
			},
		},
	}

	mJobTrace := common.MockJobTrace{}
	defer mJobTrace.AssertExpectations(t)
	mJobTrace.On("SetFailuresCollector", mock.Anything)
	mJobTrace.On("Write", mock.Anything).Return(0, nil)
	mJobTrace.On("IsStdout").Return(false)
	mJobTrace.On("SetCancelFunc", mock.Anything)
	mJobTrace.On("SetAbortFunc", mock.Anything)
	mJobTrace.On("SetDebugModeEnabled", mock.Anything)
	mJobTrace.On("Success").Return(nil)

	mNetwork := common.MockNetwork{}
	defer mNetwork.AssertExpectations(t)
	mNetwork.On("RequestJob", mock.Anything, mock.Anything, mock.Anything).Return(&jobData, true)
	mNetwork.On("ProcessJob", mock.Anything, mock.Anything).Return(&mJobTrace, nil)

	var runningBuilds uint32
	e := common.MockExecutor{}
	defer e.AssertExpectations(t)
	e.On("Prepare", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	e.On("Cleanup").Maybe()
	e.On("Shell").Return(&common.ShellScriptInfo{Shell: "script-shell"})
	e.On("Finish", mock.Anything).Maybe()
	e.On("Run", mock.Anything).Run(func(args mock.Arguments) {
		atomic.AddUint32(&runningBuilds, 1)

		// Simulate work to fill up build queue.
		time.Sleep(1 * time.Second)
	}).Return(nil)

	p := common.MockExecutorProvider{}
	defer p.AssertExpectations(t)
	p.On("Acquire", mock.Anything).Return(nil, nil)
	p.On("Release", mock.Anything, mock.Anything).Return(nil).Maybe()
	p.On("CanCreate").Return(true).Once()
	p.On("GetDefaultShell").Return("bash").Once()
	p.On("GetFeatures", mock.Anything).Return(nil)
	p.On("Create").Return(&e)

	common.RegisterExecutorProvider("multi-runner-build-limit", &p)

	cmd := RunCommand{
		network:      &mNetwork,
		buildsHelper: newBuildsHelper(),
		configOptionsWithListenAddress: configOptionsWithListenAddress{
			configOptions: configOptions{
				config: &common.Config{
					User: "git",
				},
			},
		},
	}

	runners := make(chan *common.RunnerConfig)

	// Start 2 builds.
	wg := sync.WaitGroup{}
	wg.Add(3)
	for i := 0; i < 3; i++ {
		go func(i int) {
			defer wg.Done()

			err := cmd.processRunner(i, &cfg, runners)
			assert.NoError(t, err)
		}(i)
	}

	// Wait until at least two builds have started.
	for atomic.LoadUint32(&runningBuilds) < 2 {
		time.Sleep(10 * time.Millisecond)
	}

	// Wait for all builds to finish.
	wg.Wait()

	limitMetCount := 0
	for _, entry := range hook.AllEntries() {
		if strings.Contains(entry.Message, "runner limit met") {
			limitMetCount++
		}
	}

	assert.Equal(t, 1, limitMetCount)
}

func TestArchiveUploadRedirect(t *testing.T) {
	finalRequestReceived := false

	finalServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "final", finalServerHandler(t, &finalRequestReceived)),
	)
	defer finalServer.Close()

	redirectingServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "redirection", redirectingServerHandler(finalServer.URL)),
	)
	defer redirectingServer.Close()

	cmd := &ArtifactsUploaderCommand{
		JobCredentials: common.JobCredentials{
			ID:    12345,
			Token: "token",
			URL:   redirectingServer.URL,
		},
		Name:             "artifacts",
		Format:           common.ArtifactFormatZip,
		CompressionLevel: "fastest",
		network:          network.NewGitLabClient(),
		fileArchiver: fileArchiver{
			Paths: []string{
				filepath.Join(".", "testdata", "test-artifacts"),
			},
		},
	}

	defer helpers.MakeFatalToPanic()()

	assert.NotPanics(t, func() {
		cmd.Execute(&cli.Context{})
	}, "expected command not to log fatal")

	assert.True(t, finalRequestReceived)
}

func TestUnregisterOnFailure(t *testing.T) {
	tests := map[string]struct {
		token                 string
		leaveRunner           bool
		registrationFails     bool
		expectsLeftRegistered bool
	}{
		"ui created runner, verification succeeds, runner left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"ui created runner, verification fails, LeaveRunner is false, runner machine is unregistered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"ui created runner, verification fails, LeaveRunner is true, runner machine left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
		"registration succeeds, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"registration fails, LeaveRunner is false, runner is unregistered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"registration fails, LeaveRunner is true, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			runnerUICreated := strings.HasPrefix(testCase.token, "glrt-")
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if runnerUICreated {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:    1,
						Token: testCase.token,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: testCase.token,
					}).
					Once()
			}
			if !testCase.expectsLeftRegistered {
				credsMocker := mock.MatchedBy(func(credentials common.RunnerCredentials) bool {
					return credentials.Token == testCase.token
				})
				if runnerUICreated {
					network.On("UnregisterRunnerManager", credsMocker, mock.Anything).
						Return(true).
						Once()
				} else {
					network.On("UnregisterRunner", credsMocker).
						Return(true).
						Once()
				}
			}

			var arguments []string
			if testCase.leaveRunner {
				arguments = append(arguments, "--leave-runner")
			}

			arguments, cleanTempFile := useTempConfigFile(t, arguments)
			defer cleanTempFile()

			answers := []string{"https://gitlab.com/", testCase.token, "description"}
			if !runnerUICreated {
				answers = append(answers, "", "")
			}
			if testCase.registrationFails {
				defer func() { _ = recover() }()
			} else {
				answers = append(answers, "custom") // should not result in more answers required
			}
			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			err := app.Run(append([]string{"runner", "register"}, arguments...))

			assert.False(t, testCase.registrationFails)
			assert.NoError(t, err)
		})
	}
}

func (c *CacheClient) prepareTransport() {
	c.Transport = &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 10 * time.Second,
		ResponseHeaderTimeout: 30 * time.Second,
		DisableCompression:    true,
	}
}

func TestProcessRunner_BuildLimit(t *testing.T) {
	hook, cleanup := test.NewHook()
	defer cleanup()

	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(io.Discard)

	cfg := common.RunnerConfig{
		Limit:              2,
		RequestConcurrency: 10,
		RunnerSettings: common.RunnerSettings{
			Executor: "multi-runner-build-limit",
		},
		SystemIDState: common.NewSystemIDState(),
	}

	require.NoError(t, cfg.SystemIDState.EnsureSystemID())

	jobData := common.JobResponse{
		ID: 1,
		Steps: []common.Step{
			{
				Name:         "sleep",
				Script:       common.StepScript{"sleep 10"},
				Timeout:      15,
				When:         "",
				AllowFailure: false,
			},
		},
	}

	mJobTrace := common.MockJobTrace{}
	defer mJobTrace.AssertExpectations(t)
	mJobTrace.On("SetFailuresCollector", mock.Anything)
	mJobTrace.On("Write", mock.Anything).Return(0, nil)
	mJobTrace.On("IsStdout").Return(false)
	mJobTrace.On("SetCancelFunc", mock.Anything)
	mJobTrace.On("SetAbortFunc", mock.Anything)
	mJobTrace.On("SetDebugModeEnabled", mock.Anything)
	mJobTrace.On("Success").Return(nil)

	mNetwork := common.MockNetwork{}
	defer mNetwork.AssertExpectations(t)
	mNetwork.On("RequestJob", mock.Anything, mock.Anything, mock.Anything).Return(&jobData, true)
	mNetwork.On("ProcessJob", mock.Anything, mock.Anything).Return(&mJobTrace, nil)

	var runningBuilds uint32
	e := common.MockExecutor{}
	defer e.AssertExpectations(t)
	e.On("Prepare", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	e.On("Cleanup").Maybe()
	e.On("Shell").Return(&common.ShellScriptInfo{Shell: "script-shell"})
	e.On("Finish", mock.Anything).Maybe()
	e.On("Run", mock.Anything).Run(func(args mock.Arguments) {
		atomic.AddUint32(&runningBuilds, 1)

		// Simulate work to fill up build queue.
		time.Sleep(1 * time.Second)
	}).Return(nil)

	p := common.MockExecutorProvider{}
	defer p.AssertExpectations(t)
	p.On("Acquire", mock.Anything).Return(nil, nil)
	p.On("Release", mock.Anything, mock.Anything).Return(nil).Maybe()
	p.On("CanCreate").Return(true).Once()
	p.On("GetDefaultShell").Return("bash").Once()
	p.On("GetFeatures", mock.Anything).Return(nil)
	p.On("Create").Return(&e)

	common.RegisterExecutorProvider("multi-runner-build-limit", &p)

	cmd := RunCommand{
		network:      &mNetwork,
		buildsHelper: newBuildsHelper(),
		configOptionsWithListenAddress: configOptionsWithListenAddress{
			configOptions: configOptions{
				config: &common.Config{
					User: "git",
				},
			},
		},
	}

	runners := make(chan *common.RunnerConfig)

	// Start 2 builds.
	wg := sync.WaitGroup{}
	wg.Add(3)
	for i := 0; i < 3; i++ {
		go func(i int) {
			defer wg.Done()

			err := cmd.processRunner(i, &cfg, runners)
			assert.NoError(t, err)
		}(i)
	}

	// Wait until at least two builds have started.
	for atomic.LoadUint32(&runningBuilds) < 2 {
		time.Sleep(10 * time.Millisecond)
	}

	// Wait for all builds to finish.
	wg.Wait()

	limitMetCount := 0
	for _, entry := range hook.AllEntries() {
		if strings.Contains(entry.Message, "runner limit met") {
			limitMetCount++
		}
	}

	assert.Equal(t, 1, limitMetCount)
}

func TestGetUploadEnv(t *testing.T) {
	tests := map[string]struct {
		config      *common.CacheConfig
		failedFetch bool
		expected    map[string]string
	}{
		"no upload role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{},
			},
			expected: nil,
		},
		"with upload role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					BucketName:    bucketName,
					UploadRoleARN: "arn:aws:iam::123456789012:role/S3UploadRole",
				},
			},
			expected: map[string]string{
				"AWS_ACCESS_KEY_ID":     "mock-access-key",
				"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
				"AWS_SESSION_TOKEN":     "mock-session-token",
			},
		},
		"with failed credentials": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					BucketName:    bucketName,
					UploadRoleARN: "arn:aws:iam::123456789012:role/S3UploadRole",
				},
			},
			failedFetch: true,
			expected:    nil,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			cleanupS3URLGeneratorMock := onFakeS3URLGenerator(cacheOperationTest{})
			defer cleanupS3URLGeneratorMock()

			adapter, err := New(tt.config, defaultTimeout, objectName)
			require.NoError(t, err)

			mockClient := adapter.(*s3Adapter).client.(*mockS3Presigner)

			if tt.failedFetch {
				mockClient.On("FetchCredentialsForRole", mock.Anything, tt.config.S3.UploadRoleARN, bucketName, objectName, mock.Anything).
					Return(nil, errors.New("error fetching credentials"))
			} else {
				mockClient.On("FetchCredentialsForRole", mock.Anything, tt.config.S3.UploadRoleARN, bucketName, objectName, mock.Anything).
					Return(tt.expected, nil)
			}
			env, err := adapter.GetUploadEnv(context.Background())
			assert.Equal(t, tt.expected, env)

			if tt.failedFetch {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func newMockSTSHandler(expectedKms bool, expectedDurationSecs int) http.Handler {
	roleARN := "arn:aws:iam::123456789012:role/TestRole"
	expectedStatements := 1
	if expectedKms {
		expectedStatements = 2
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/sts" {
			http.NotFound(w, r)
			return
		}

		body, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Failed to read request body", http.StatusBadRequest)
			return
		}
		defer r.Body.Close()

		queryValues, err := url.ParseQuery(string(body))
		if err != nil {
			http.Error(w, "Failed to parse request body", http.StatusBadRequest)
			return
		}

		if queryValues.Get("Action") != "AssumeRole" {
			http.Error(w, "Invalid Action parameter", http.StatusBadRequest)
			return
		}

		if queryValues.Get("RoleArn") == "" {
			http.Error(w, "Missing RoleArn parameter", http.StatusBadRequest)
			return
		}

		if queryValues.Get("RoleArn") != roleARN {
			http.Error(w, "Invalid RoleArn parameter", http.StatusUnauthorized)
			return
		}

		if queryValues.Get("DurationSeconds") != fmt.Sprintf("%d", expectedDurationSecs) {
			http.Error(w, "Invalid DurationSeconds parameter", http.StatusUnauthorized)
			return
		}

		if queryValues.Get("RoleSessionName") == "" {
			http.Error(w, "Missing RoleSessionName parameter", http.StatusBadRequest)
			return
		}

		policy := queryValues.Get("Policy")
		if policy == "" {
			http.Error(w, "Missing Policy parameter", http.StatusBadRequest)
			return
		}

		var policyJSON sessionPolicy
		err = json.Unmarshal([]byte(policy), &policyJSON)
		if err != nil {
			http.Error(w, "Invalid Policy JSON", http.StatusBadRequest)
			return
		}

		if policyJSON.Statement == nil || len(policyJSON.Statement) != expectedStatements {
			http.Error(w, fmt.Sprintf("Policy must contain exactly %d Statements", expectedStatements), http.StatusBadRequest)
			return
		}

		statement := policyJSON.Statement[0]
		if statement.Action == nil || len(statement.Action) != 1 {
			http.Error(w, "Statement must contain exactly one Action", http.StatusBadRequest)
			return
		}

		if statement.Action[0] != "s3:PutObject" {
			http.Error(w, "Action should be s3:PutObject", http.StatusBadRequest)
			return
		}

		if expectedKms {
			kmsStatement := policyJSON.Statement[1]
			if kmsStatement.Action == nil || len(kmsStatement.Action) != 2 {
				http.Error(w, "KMS Statement must contain exactly two Actions", http.StatusBadRequest)
				return
			}
			if kmsStatement.Action[0] != "kms:Decrypt" || kmsStatement.Action[1] != "kms:GenerateDataKey" {
				http.Error(w, "KMS Statement Actions should be kms:Decrypt and kms:GenerateDataKey", http.StatusBadRequest)
				return
			}
		}

		if statement.Resource != fmt.Sprintf("arn:aws:s3:::%s/%s", bucketName, objectName) {
			http.Error(w, "Invalid policy statement", http.StatusBadRequest)
			return
		}

		w.Header().Set("Content-Type", "application/xml")
		w.WriteHeader(http.StatusOK)
		// See https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRole.html
		_, err = w.Write([]byte(`<?xml version="1.0" encoding="UTF-8"?>
<AssumeRoleResponse xmlns="https://sts.amazonaws.com/doc/2011-06-15/">
  <AssumeRoleResult>
    <Credentials>
      <AccessKeyId>mock-access-key</AccessKeyId>
      <SecretAccessKey>mock-secret-key</SecretAccessKey>
      <SessionToken>mock-session-token</SessionToken>
      <Expiration>` + time.Now().Add(time.Hour).Format(time.RFC3339) + `</Expiration>
    </Credentials>
    <AssumedRoleUser>
      <AssumedRoleId>AROATEST123:TestSession</AssumedRoleId>
      <Arn>arn:aws:sts::123456789012:assumed-role/TestRole/TestSession</Arn>
    </AssumedRoleUser>
  </AssumeRoleResult>
  <ResponseMetadata>
    <RequestId>c6104cbe-af31-11e0-8154-cbc7ccf896c7</RequestId>
  </ResponseMetadata>
</AssumeRoleResponse>`))
		if err != nil {
			w.WriteHeader(http.StatusExpectationFailed)
		}
	})
}

func TestBuildsHelper_ListJobsHandler(t *testing.T) {
	tests := map[string]struct {
		build          *common.Build
		expectedOutput []string
	}{
		"no jobs": {
			build: nil,
		},
		"job exists": {
			build: &common.Build{
				Runner: &common.RunnerConfig{
					SystemIDState: common.NewSystemIDState(),
				},
				JobResponse: common.JobResponse{
					ID:      1,
					JobInfo: common.JobInfo{ProjectID: 1},
					GitInfo: common.GitInfo{RepoURL: "https://gitlab.example.com/my-namespace/my-project.git"},
				},
			},
			expectedOutput: []string{
				"url=https://gitlab.example.com/my-namespace/my-project/-/jobs/1",
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			writer := httptest.NewRecorder()

			req, err := http.NewRequest(http.MethodGet, "/", nil)
			require.NoError(t, err)

			b := newBuildsHelper()
			b.addBuild(test.build)
			b.ListJobsHandler(writer, req)

			if test.build != nil {
				require.NoError(t, test.build.Runner.SystemIDState.EnsureSystemID())
			}

			resp := writer.Result()
			defer resp.Body.Close()

			assert.Equal(t, http.StatusOK, resp.StatusCode)
			assert.Equal(t, "2", resp.Header.Get("X-List-Version"))
			assert.Equal(t, "text/plain", resp.Header.Get(common.ContentType))

			body, err := io.ReadAll(resp.Body)
			require.NoError(t, err)

			if len(test.expectedOutput) == 0 {
				assert.Empty(t, body)
				return
			}

			for _, expectedOutput := range test.expectedOutput {
				assert.Contains(t, string(body), expectedOutput)
			}
		})
	}
}

func init() {
	common.RegisterCommand(cli.Command{
		Name:  "fleeting",
		Usage: "manage fleeting plugins",
		Flags: []cli.Flag{
			cli.StringFlag{Name: "config, c", EnvVar: "CONFIG_FILE", Value: commands.GetDefaultConfigFile()},
		},
		Subcommands: []cli.Command{
			{
				Name:   "install",
				Usage:  "install or update fleeting plugins",
				Flags:  []cli.Flag{cli.BoolFlag{Name: "upgrade"}},
				Action: install,
			},
			{
				Name:   "list",
				Usage:  "list installed plugins",
				Action: list,
			},
			{
				Name:  "login",
				Usage: "login to container registry",
				Flags: []cli.Flag{
					cli.StringFlag{Name: "username"},
					cli.StringFlag{Name: "password"},
					cli.BoolFlag{Name: "password-stdin", Usage: "take the password from stdin"},
				},
				ArgsUsage: "[server]",
				Action:    login,
			},
		},
	})
}

func viewsCleanupBegin() error {
	go func() {
		for {
			statement := `
				DELETE FROM views
				WHERE viewDate < $1;
			`
			_, err := db.Exec(statement, time.Now().UTC().AddDate(0, 0, -45))
			if err != nil {
				logger.Errorf("error cleaning up views: %v", err)
				return
			}

			time.Sleep(24 * time.Hour)
		}
	}()

	return nil
}

func TestArtifactsUploaderRetry(t *testing.T) {
	OnEachZipArchiver(t, func(t *testing.T) {
		network := &testNetwork{
			uploadState: common.UploadFailed,
		}
		cmd := ArtifactsUploaderCommand{
			JobCredentials: UploaderCredentials,
			network:        network,
			fileArchiver: fileArchiver{
				Paths: []string{artifactsTestArchivedFile},
			},
		}

		writeTestFile(t, artifactsTestArchivedFile)
		defer os.Remove(artifactsTestArchivedFile)

		removeHook := helpers.MakeFatalToPanic()
		defer removeHook()

		assert.Panics(t, func() {
			cmd.Execute(nil)
		})

		assert.Equal(t, defaultTries, network.uploadCalled)
	})
}

func TestFetchCredentialsForRole(t *testing.T) {
	workingConfig := common.CacheConfig{
		S3: &common.CacheS3Config{
			AccessKey:          "test-access-key",
			SecretKey:          "test-secret-key",
			AuthenticationType: "access-key",
			BucketName:         "test-bucket",
			UploadRoleARN:      "arn:aws:iam::123456789012:role/TestRole",
		},
	}
	mockedCreds := map[string]string{
		"AWS_ACCESS_KEY_ID":     "mock-access-key",
		"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
		"AWS_SESSION_TOKEN":     "mock-session-token",
	}

	tests := map[string]struct {
		config           *common.CacheConfig
		roleARN          string
		expected         map[string]string
		errMsg           string
		expectedKms      bool
		duration         time.Duration
		expectedDuration time.Duration
	}{
		"successful fetch": {
			config:   &workingConfig,
			roleARN:  "arn:aws:iam::123456789012:role/TestRole",
			expected: mockedCreds,
		},
		"successful fetch with 12-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         12 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 12 * time.Hour,
		},
		"successful fetch with 10-minute timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         10 * time.Minute,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with 13-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         13 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with encryption": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:                 "test-access-key",
					SecretKey:                 "test-secret-key",
					AuthenticationType:        "access-key",
					BucketName:                "test-bucket",
					UploadRoleARN:             "arn:aws:iam::123456789012:role/TestRole",
					ServerSideEncryption:      "KMS",
					ServerSideEncryptionKeyID: "arn:aws:kms:us-west-2:123456789012:key/1234abcd-12ab-34cd-56ef-1234567890ab",
				},
			},
			roleARN:     "arn:aws:iam::123456789012:role/TestRole",
			expected:    mockedCreds,
			expectedKms: true,
		},
		"invalid role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
					UploadRoleARN:      "arn:aws:iam::123456789012:role/InvalidRole",
				},
			},
			roleARN: "arn:aws:iam::123456789012:role/InvalidRole",
			errMsg:  "failed to assume role",
		},
		"no role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
				},
			},
			expected: nil,
			errMsg:   "failed to assume role",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			duration := 3600
			if tt.duration > 0 {
				duration = int(tt.expectedDuration.Seconds())
			}
			// Create s3Client and point STS endpoint to it
			mockServer := httptest.NewServer(newMockSTSHandler(tt.expectedKms, duration))
			defer mockServer.Close()

			s3Client, err := newS3Client(tt.config.S3, withSTSEndpoint(mockServer.URL+"/sts"))
			require.NoError(t, err)

			creds, err := s3Client.FetchCredentialsForRole(context.Background(), tt.roleARN, bucketName, objectName, tt.duration)

			if tt.errMsg != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), tt.errMsg)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expected, creds)
			}
		})
	}
}

func TestAccessLevelSetting(t *testing.T) {
	tests := map[string]struct {
		accessLevel     commands.AccessLevel
		failureExpected bool
	}{
		"access level not defined": {},
		"ref_protected used": {
			accessLevel: commands.RefProtected,
		},
		"not_protected used": {
			accessLevel: commands.NotProtected,
		},
		"unknown access level": {
			accessLevel:     commands.AccessLevel("unknown"),
			failureExpected: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if !testCase.failureExpected {
				parametersMocker := mock.MatchedBy(func(parameters common.RegisterRunnerParameters) bool {
					return commands.AccessLevel(parameters.AccessLevel) == testCase.accessLevel
				})

				network.On("RegisterRunner", mock.Anything, parametersMocker).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			}

			arguments := []string{
				"--registration-token", "test-runner-token",
				"--access-level", string(testCase.accessLevel),
			}

			_, output, err := testRegisterCommandRun(t, network, nil, "", arguments...)

			if testCase.failureExpected {
				assert.EqualError(t, err, "command error: Given access-level is not valid. "+
					"Refer to gitlab-runner register -h for the correct options.")
				assert.NotContains(t, output, "Runner registered successfully.")

				return
			}

			assert.NoError(t, err)
			assert.Contains(t, output, "Runner registered successfully.")
		})
	}
}

func TestCacheOperationEncryptionKMS(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)
	headers := http.Header{}
	headers.Add("X-Amz-Server-Side-Encryption", "aws:kms")
	headers.Add("X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id", "alias/my-key")

	tests := map[string]cacheOperationTest{
		"error-on-minio-client-initialization": {
			errorOnMinioClientInitialization: true,
			expectedUploadHeaders:            nil,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning:  true,
			presignedURL:          URL,
			expectedURL:           nil,
			expectedUploadHeaders: nil,
		},
		"presigned-url-kms": {
			presignedURL:          URL,
			expectedURL:           URL,
			expectedUploadHeaders: headers,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
		})
	}
}

func defaultAzureCache() *common.CacheConfig {
	return &common.CacheConfig{
		Type: "azure",
		Azure: &common.CacheAzureConfig{
			CacheAzureCredentials: common.CacheAzureCredentials{
				AccountName: accountName,
				AccountKey:  accountKey,
			},
			ContainerName: containerName,
			StorageDomain: storageDomain,
		},
	}
}

func TestGetCredentials(t *testing.T) {
	tests := map[string]struct {
		s3            *common.CacheS3Config
		expectedError string
		credsExpected bool
	}{
		"static credentials": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
				SecretKey:  "somesecret",
			},
			credsExpected: true,
		},
		"no S3 credentials": {
			expectedError: `missing S3 configuration`,
		},
		"empty access and secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
			},
			credsExpected: false,
		},
		"empty access key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				SecretKey:  "somesecret",
			},
			credsExpected: false,
		},
		"empty secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
			},
			credsExpected: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			config := &common.CacheConfig{S3: tt.s3}
			adapter, err := NewS3CredentialsAdapter(config)

			if tt.expectedError != "" {
				require.EqualError(t, err, tt.expectedError)
			} else {
				require.NoError(t, err)

				creds := adapter.GetCredentials()

				if tt.credsExpected {
					assert.Equal(t, 2, len(creds))
					assert.Equal(t, tt.s3.AccessKey, creds["AWS_ACCESS_KEY_ID"])
					assert.Equal(t, tt.s3.SecretKey, creds["AWS_SECRET_ACCESS_KEY"])
				} else {
					assert.Empty(t, creds)
				}
			}
		})
	}
}

func Test_loadConfig(t *testing.T) {
	const expectedSystemIDRegexPattern = "^[sr]_[0-9a-zA-Z]{12}$"

	testCases := map[string]struct {
		runnerSystemID string
		prepareFn      func(t *testing.T, systemIDFile string)
		assertFn       func(t *testing.T, err error, config *common.Config, systemIDFile string)
	}{
		"generates and saves missing system IDs": {
			runnerSystemID: "",
			assertFn: func(t *testing.T, err error, config *common.Config, systemIDFile string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.NotEmpty(t, config.Runners[0].SystemIDState.GetSystemID())
				content, err := os.ReadFile(systemIDFile)
				require.NoError(t, err)
				assert.Contains(t, string(content), config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"preserves existing unique system IDs": {
			runnerSystemID: "s_c2d22f638c25",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Equal(t, "s_c2d22f638c25", config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"regenerates system ID if file is invalid": {
			runnerSystemID: "0123456789",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"succeeds if file cannot be created": {
			runnerSystemID: "",
			prepareFn: func(t *testing.T, systemIDFile string) {
				require.NoError(t, os.Remove(systemIDFile))
				require.NoError(t, os.Chmod(filepath.Dir(systemIDFile), os.ModeDir|0500))
			},
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				require.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			dir := t.TempDir()
			cfgName := filepath.Join(dir, "config.toml")
			systemIDFile := filepath.Join(dir, ".runner_system_id")

			require.NoError(t, os.Chmod(dir, 0777))
			require.NoError(t, os.WriteFile(cfgName, []byte("[[runners]]\n name = \"runner\""), 0777))
			require.NoError(t, os.WriteFile(systemIDFile, []byte(tc.runnerSystemID), 0777))

			if tc.prepareFn != nil {
				tc.prepareFn(t, systemIDFile)
			}

			c := configOptions{ConfigFile: cfgName}
			err := c.loadConfig()

			tc.assertFn(t, err, c.config, systemIDFile)

			// Cleanup
			require.NoError(t, os.Chmod(dir, 0777))
		})
	}
}

func Test_URFavArgParsing(t *testing.T) {
	app := cli.NewApp()
	app.Name = "gitlab-runner-helper"
	app.Usage = "a GitLab Runner Helper"
	app.Version = common.AppVersion.ShortLine()
	app.Commands = common.GetCommands()

	jobToken := "-Abajdbajdbajb"

	defer os.Remove("foo.txt")

	s := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		assert.Equal(t, jobToken, r.Header.Get("Job-Token"))

		w.WriteHeader(http.StatusOK)
		zw := zip.NewWriter(w)
		defer zw.Close()
		w1, err := zw.Create("foo.txt")
		require.NoError(t, err)
		_, err = w1.Write(bytes.Repeat([]byte("198273qhnjbqwdjbqwe2109u3abcdef3"), 1024*1024))
		require.NoError(t, err)
	}))
	defer s.Close()

	args := []string{
		"gitlab-runner-helper",
		"artifacts-downloader",
		"--url", s.URL,
		"--token", jobToken,
		"--id", "12345",
	}

	err := app.Run(args)
	assert.NoError(t, err)

	if err != nil {
		assert.NotContains(t, err.Error(), "WARNING: Missing build ID (--id)")
		assert.NotContains(t, err.Error(), "FATAL: Incomplete arguments ")
	}
}

func TestUnregisterOnFailure(t *testing.T) {
	tests := map[string]struct {
		token                 string
		leaveRunner           bool
		registrationFails     bool
		expectsLeftRegistered bool
	}{
		"ui created runner, verification succeeds, runner left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"ui created runner, verification fails, LeaveRunner is false, runner machine is unregistered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"ui created runner, verification fails, LeaveRunner is true, runner machine left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
		"registration succeeds, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"registration fails, LeaveRunner is false, runner is unregistered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"registration fails, LeaveRunner is true, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			runnerUICreated := strings.HasPrefix(testCase.token, "glrt-")
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if runnerUICreated {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:    1,
						Token: testCase.token,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: testCase.token,
					}).
					Once()
			}
			if !testCase.expectsLeftRegistered {
				credsMocker := mock.MatchedBy(func(credentials common.RunnerCredentials) bool {
					return credentials.Token == testCase.token
				})
				if runnerUICreated {
					network.On("UnregisterRunnerManager", credsMocker, mock.Anything).
						Return(true).
						Once()
				} else {
					network.On("UnregisterRunner", credsMocker).
						Return(true).
						Once()
				}
			}

			var arguments []string
			if testCase.leaveRunner {
				arguments = append(arguments, "--leave-runner")
			}

			arguments, cleanTempFile := useTempConfigFile(t, arguments)
			defer cleanTempFile()

			answers := []string{"https://gitlab.com/", testCase.token, "description"}
			if !runnerUICreated {
				answers = append(answers, "", "")
			}
			if testCase.registrationFails {
				defer func() { _ = recover() }()
			} else {
				answers = append(answers, "custom") // should not result in more answers required
			}
			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			err := app.Run(append([]string{"runner", "register"}, arguments...))

			assert.False(t, testCase.registrationFails)
			assert.NoError(t, err)
		})
	}
}

func TestHealthCheckCommand_Execute(t *testing.T) {
	cases := []struct {
		name            string
		expectedConnect bool
		exposeHigher    bool
		exposeLower     bool
	}{
		{
			name:            "Successful connect",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Unsuccessful connect because service is down",
			expectedConnect: false,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with lower port exposed",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     true,
		},
		{
			name:            "Successful connect with both lower and higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     true,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			os.Unsetenv("SERVICE_LOWER_TCP_PORT")
			os.Unsetenv("SERVICE_HIGHER_TCP_PORT")

			// Start listening to reverse addr
			listener, err := net.Listen("tcp", "127.0.0.1:")
			require.NoError(t, err)
			defer listener.Close()

			port := listener.Addr().(*net.TCPAddr).Port

			err = os.Setenv("SERVICE_TCP_ADDR", "127.0.0.1")
			require.NoError(t, err)

			err = os.Setenv("SERVICE_TCP_PORT", strconv.Itoa(port))
			require.NoError(t, err)

			if c.exposeHigher {
				err = os.Setenv("SERVICE_HIGHER_TCP_PORT", strconv.Itoa(port+1))
				require.NoError(t, err)
			}

			if c.exposeLower {
				err = os.Setenv("SERVICE_LOWER_TCP_PORT", strconv.Itoa(port-1))
				require.NoError(t, err)
			}

			// If we don't expect to connect we close the listener.
			if !c.expectedConnect {
				listener.Close()
			}

			ctx, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
			defer cancelFn()
			done := make(chan struct{})
			go func() {
				cmd := HealthCheckCommand{ctx: ctx}
				cmd.Execute(nil)
				done <- struct{}{}
			}()

			select {
			case <-ctx.Done():
				if c.expectedConnect {
					require.Fail(t, "Timeout waiting to start service.")
				}
			case <-done:
				if !c.expectedConnect {
					require.Fail(t, "Expected to not connect to server")
				}
			}
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
)

func dbUnsafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	unsafe1 := "select * from foo where bar = '" + tainted + "'"
	unsafe2 := "select * from foo where id = " + tainted
	unsafe3 := fmt.Sprintf("select * from foo where bar = %q", tainted)

	unsafe4 := strings.Builder{}
	unsafe4.WriteString("select * from foo where bar = ")
	unsafe4.WriteString(tainted)

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	// ruleid:go_sql_rule-concat-sqli
	db.Exec(unsafe4.String())

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func dbSafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = '" + "safe" + "'"
	safe2 := "select * from foo where id = " + "safe"
	safe3 := fmt.Sprintf("select * from foo where bar = %q", "safe")

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe3)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = '" + "safe" + "'")
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES('"+"safe"+"')")
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = '" + "safe" + "' WHERE id = 100")
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = '"+"safe"+"'")
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values('" + "safe" + "')")
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = '"+"safe"+"' where id = 100")
}

func dbSafe2(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = ?"
	safe2 := "select * from foo where bar is $1 and baz = $2"

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1, tainted)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2, true, tainted)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = ? WHERE id = 100", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = $2 and id = $1", "baz", tainted)
}

// False-positive reported by customer (see gitlab-org/gitlab#451108).
func safeIssue_451108(ctx context.Context, val string) error {
	if err := somepkg.validate(val); err != nil {
		return &somepkg.Error{Message: err.Error()}
	}

	if !updateAllowed(somepkg.Status) {
		// ok:go_sql_rule-concat-sqli
		return &somepkg.Error{Message: fmt.Sprintf("update is not permitted: %v", somepkg.Status)}
	}

	return nil
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"os"
)

func foo1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error when changing file permissions!")
		return
	}
}

func foo2() {
	// ruleid:go_file-permissions_rule-fileperm
	_, err := os.OpenFile("/tmp/thing", os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		fmt.Println("Error opening a file!")
		return
	}
}

func foo3() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error writing file")
		return
	}
}

func foo4() {
	// ok:go_file-permissions_rule-fileperm
	_, err := os.OpenFile("/tmp/thing", os.O_CREATE|os.O_WRONLY, 0600)
	if err != nil {
		fmt.Println("Error opening a file!")
		return
	}
}

func foo5() {
	// ok:go_file-permissions_rule-fileperm
	err := os.Chmod("/tmp/mydir", 0400)
	if err != nil {
		fmt.Println("Error")
		return
	}
}

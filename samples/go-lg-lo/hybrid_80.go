func TestGetUploadEnv(t *testing.T) {
	tests := map[string]struct {
		config      *common.CacheConfig
		failedFetch bool
		expected    map[string]string
	}{
		"no upload role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{},
			},
			expected: nil,
		},
		"with upload role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					BucketName:    bucketName,
					UploadRoleARN: "arn:aws:iam::123456789012:role/S3UploadRole",
				},
			},
			expected: map[string]string{
				"AWS_ACCESS_KEY_ID":     "mock-access-key",
				"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
				"AWS_SESSION_TOKEN":     "mock-session-token",
			},
		},
		"with failed credentials": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					BucketName:    bucketName,
					UploadRoleARN: "arn:aws:iam::123456789012:role/S3UploadRole",
				},
			},
			failedFetch: true,
			expected:    nil,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			cleanupS3URLGeneratorMock := onFakeS3URLGenerator(cacheOperationTest{})
			defer cleanupS3URLGeneratorMock()

			adapter, err := New(tt.config, defaultTimeout, objectName)
			require.NoError(t, err)

			mockClient := adapter.(*s3Adapter).client.(*mockS3Presigner)

			if tt.failedFetch {
				mockClient.On("FetchCredentialsForRole", mock.Anything, tt.config.S3.UploadRoleARN, bucketName, objectName, mock.Anything).
					Return(nil, errors.New("error fetching credentials"))
			} else {
				mockClient.On("FetchCredentialsForRole", mock.Anything, tt.config.S3.UploadRoleARN, bucketName, objectName, mock.Anything).
					Return(tt.expected, nil)
			}
			env, err := adapter.GetUploadEnv(context.Background())
			assert.Equal(t, tt.expected, env)

			if tt.failedFetch {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func TestRegisterTokenExpiresAt(t *testing.T) {
	type testCase struct {
		token          string
		expiration     time.Time
		expectedConfig string
	}

	testCases := map[string]testCase{
		"no expiration": {
			token: "test-runner-token",
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration": {
			token:      "test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
		"no expiration with authentication token": {
			token: "glrt-test-runner-token",
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration with authentication token": {
			token:      "glrt-test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:             12345,
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, []kv{}, "", "--registration-token", tc.token, "--name", "test-runner")
			require.NoError(t, err)

			assert.Contains(
				t, spaceReplacer.Replace(gotConfig),
				spaceReplacer.Replace(fmt.Sprintf(tc.expectedConfig, commands.RegisterTimeNowDate.Format(time.RFC3339))),
			)
		})
	}
}

func TestRunnerByName(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerName    string
		expectedIndex int
		expectedError error
	}{
		"finds runner by name": {
			runners: []*common.RunnerConfig{
				{
					Name: "runner1",
				},
				{
					Name: "runner2",
				},
			},
			runnerName:    "runner2",
			expectedIndex: 1,
		},
		"does not find non-existent runner": {
			runners: []*common.RunnerConfig{
				{
					Name: "runner1",
				},
				{
					Name: "runner2",
				},
			},
			runnerName:    "runner3",
			expectedIndex: -1,
			expectedError: fmt.Errorf("could not find a runner with the name 'runner3'"),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByName(tt.runnerName)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestCacheOperation(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)

	tests := map[string]cacheOperationTest{
		"error-on-s3-client-initialization": {
			errorOnS3ClientInitialization: true,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning: true,
			presignedURL:         URL,
			expectedURL:          nil,
		},
		"presigned-url": {
			presignedURL: URL,
			expectedURL:  URL,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
		})
	}
}

func finalServerHandler(t *testing.T, finalRequestReceived *bool) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		dir := t.TempDir()

		receiveFile(t, r, dir)

		err := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}

			fileName := info.Name()
			fileContentBytes, err := os.ReadFile(path)
			if err != nil {
				return err
			}

			assert.Equal(t, fileName, strings.TrimSpace(string(fileContentBytes)))

			return nil
		})

		assert.NoError(t, err)

		*finalRequestReceived = true
		rw.WriteHeader(http.StatusCreated)
	}
}

func TestGetCredentials(t *testing.T) {
	tests := map[string]struct {
		s3            *common.CacheS3Config
		expectedError string
		credsExpected bool
	}{
		"static credentials": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
				SecretKey:  "somesecret",
			},
			credsExpected: true,
		},
		"no S3 credentials": {
			expectedError: `missing S3 configuration`,
		},
		"empty access and secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
			},
			credsExpected: false,
		},
		"empty access key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				SecretKey:  "somesecret",
			},
			credsExpected: false,
		},
		"empty secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
			},
			credsExpected: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			config := &common.CacheConfig{S3: tt.s3}
			adapter, err := NewS3CredentialsAdapter(config)

			if tt.expectedError != "" {
				require.EqualError(t, err, tt.expectedError)
			} else {
				require.NoError(t, err)

				creds := adapter.GetCredentials()

				if tt.credsExpected {
					assert.Equal(t, 2, len(creds))
					assert.Equal(t, tt.s3.AccessKey, creds["AWS_ACCESS_KEY_ID"])
					assert.Equal(t, tt.s3.SecretKey, creds["AWS_SECRET_ACCESS_KEY"])
				} else {
					assert.Empty(t, creds)
				}
			}
		})
	}
}

func viewsCleanupBegin() error {
	go func() {
		for {
			statement := `
				DELETE FROM views
				WHERE viewDate < $1;
			`
			_, err := db.Exec(statement, time.Now().UTC().AddDate(0, 0, -45))
			if err != nil {
				logger.Errorf("error cleaning up views: %v", err)
				return
			}

			time.Sleep(24 * time.Hour)
		}
	}()

	return nil
}

func TestExecute_MergeConfigTemplate(t *testing.T) {
	var (
		configTemplateMergeInvalidConfiguration = `- , ;`

		configTemplateMergeAdditionalConfiguration = `
[[runners]]
  [runners.kubernetes]
    [runners.kubernetes.volumes]
      [[runners.kubernetes.volumes.empty_dir]]
        name = "empty_dir"
	    mount_path = "/path/to/empty_dir"
	    medium = "Memory"
	    size_limit = "1G"`

		baseOutputConfigFmt = `concurrent = 1
check_interval = 0
connection_max_age = "15m0s"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = %q
  url = "http://gitlab.example.com/"
  id = 0
  token = "test-runner-token"
  token_obtained_at = %s
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  shell = "pwsh"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
`
	)

	tests := map[string]struct {
		configTemplate         string
		networkAssertions      func(n *common.MockNetwork)
		errExpected            bool
		expectedFileContentFmt string
	}{
		"config template disabled": {
			configTemplate: "",
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected:            false,
			expectedFileContentFmt: baseOutputConfigFmt,
		},
		"config template with no additional runner configuration": {
			configTemplate: "[[runners]]",
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected:            false,
			expectedFileContentFmt: baseOutputConfigFmt,
		},
		"successful config template merge": {
			configTemplate: configTemplateMergeAdditionalConfiguration,
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected: false,
			expectedFileContentFmt: `concurrent = 1
check_interval = 0
connection_max_age = "15m0s"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = %q
  url = "http://gitlab.example.com/"
  id = 0
  token = "test-runner-token"
  token_obtained_at = %s
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  shell = "pwsh"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
`,
		},
		"incorrect config template merge": {
			configTemplate:    configTemplateMergeInvalidConfiguration,
			networkAssertions: func(n *common.MockNetwork) {},
			errExpected:       true,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			var err error

			if tt.errExpected {
				helpers.MakeFatalToPanic()
			}

			cfgTpl, cleanup := commands.PrepareConfigurationTemplateFile(t, tt.configTemplate)
			defer cleanup()

			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			args := []string{
				"--shell", shells.SNPwsh,
				"--registration-token", "test-runner-token",
			}

			if tt.configTemplate != "" {
				args = append(args, "--template-config", cfgTpl)
			}

			tt.networkAssertions(network)

			fileContent, _, err := testRegisterCommandRun(t, network, nil, "", args...)
			if tt.errExpected {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)
			name, err := os.Hostname()
			require.NoError(t, err)
			assert.Equal(t, fmt.Sprintf(tt.expectedFileContentFmt, name, commands.RegisterTimeNowDate.Format(time.RFC3339)), fileContent)
		})
	}
}

func TestArtifactsDownloader(t *testing.T) {
	testCases := map[string]struct {
		downloadState                common.DownloadState
		directDownload               bool
		stagingDir                   string
		expectedSuccess              bool
		expectedDownloadCalled       int
		expectedDirectDownloadCalled int
	}{
		"download not found": {
			downloadState:          common.DownloadNotFound,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download forbidden": {
			downloadState:          common.DownloadForbidden,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download unauthorized": {
			downloadState:          common.DownloadUnauthorized,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"retries are called": {
			downloadState:          common.DownloadFailed,
			expectedSuccess:        false,
			expectedDownloadCalled: 3,
		},
		"first try is always direct download": {
			downloadState:                common.DownloadFailed,
			directDownload:               true,
			expectedSuccess:              false,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       3,
		},
		"downloads artifact without direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               false,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 0,
			expectedDownloadCalled:       1,
		},
		"downloads artifact with direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               true,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       1,
		},
		"setting invalid staging directory": {
			downloadState: common.DownloadSucceeded,
			stagingDir:    "/dev/null",
		},
	}

	removeHook := helpers.MakeFatalToPanic()
	defer removeHook()

	// ensure clean state
	os.Remove(artifactsTestArchivedFile)

	for testName, testCase := range testCases {
		OnEachZipArchiver(t, func(t *testing.T) {
			t.Run(testName, func(t *testing.T) {
				network := &testNetwork{
					downloadState: testCase.downloadState,
				}
				cmd := ArtifactsDownloaderCommand{
					JobCredentials: downloaderCredentials,
					DirectDownload: testCase.directDownload,
					network:        network,
					retryHelper: retryHelper{
						Retry: 2,
					},
					StagingDir: testCase.stagingDir,
				}

				// file is cleaned after running test
				defer os.Remove(artifactsTestArchivedFile)

				if testCase.expectedSuccess {
					require.NotPanics(t, func() {
						cmd.Execute(nil)
					})

					assert.FileExists(t, artifactsTestArchivedFile)
				} else {
					require.Panics(t, func() {
						cmd.Execute(nil)
					})
				}

				assert.Equal(t, testCase.expectedDirectDownloadCalled, network.directDownloadCalled)
				assert.Equal(t, testCase.expectedDownloadCalled, network.downloadCalled)
			})
		})
	}
}

func TestRunnerByName(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerName    string
		expectedIndex int
		expectedError error
	}{
		"finds runner by name": {
			runners: []*common.RunnerConfig{
				{
					Name: "runner1",
				},
				{
					Name: "runner2",
				},
			},
			runnerName:    "runner2",
			expectedIndex: 1,
		},
		"does not find non-existent runner": {
			runners: []*common.RunnerConfig{
				{
					Name: "runner1",
				},
				{
					Name: "runner2",
				},
			},
			runnerName:    "runner3",
			expectedIndex: -1,
			expectedError: fmt.Errorf("could not find a runner with the name 'runner3'"),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByName(tt.runnerName)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestUnregisterOnFailure(t *testing.T) {
	tests := map[string]struct {
		token                 string
		leaveRunner           bool
		registrationFails     bool
		expectsLeftRegistered bool
	}{
		"ui created runner, verification succeeds, runner left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"ui created runner, verification fails, LeaveRunner is false, runner machine is unregistered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"ui created runner, verification fails, LeaveRunner is true, runner machine left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
		"registration succeeds, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"registration fails, LeaveRunner is false, runner is unregistered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"registration fails, LeaveRunner is true, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			runnerUICreated := strings.HasPrefix(testCase.token, "glrt-")
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if runnerUICreated {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:    1,
						Token: testCase.token,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: testCase.token,
					}).
					Once()
			}
			if !testCase.expectsLeftRegistered {
				credsMocker := mock.MatchedBy(func(credentials common.RunnerCredentials) bool {
					return credentials.Token == testCase.token
				})
				if runnerUICreated {
					network.On("UnregisterRunnerManager", credsMocker, mock.Anything).
						Return(true).
						Once()
				} else {
					network.On("UnregisterRunner", credsMocker).
						Return(true).
						Once()
				}
			}

			var arguments []string
			if testCase.leaveRunner {
				arguments = append(arguments, "--leave-runner")
			}

			arguments, cleanTempFile := useTempConfigFile(t, arguments)
			defer cleanTempFile()

			answers := []string{"https://gitlab.com/", testCase.token, "description"}
			if !runnerUICreated {
				answers = append(answers, "", "")
			}
			if testCase.registrationFails {
				defer func() { _ = recover() }()
			} else {
				answers = append(answers, "custom") // should not result in more answers required
			}
			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			err := app.Run(append([]string{"runner", "register"}, arguments...))

			assert.False(t, testCase.registrationFails)
			assert.NoError(t, err)
		})
	}
}

func (mr *RunCommand) processRunners(id int, stopWorker chan bool, runners chan *common.RunnerConfig) {
	mr.log().
		WithField("worker", id).
		Debugln("Starting worker")

	mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStarted).Inc()

	for mr.stopSignal == nil {
		select {
		case runner := <-runners:
			err := mr.processRunner(id, runner, runners)
			if err != nil {
				logger := mr.log().
					WithFields(logrus.Fields{
						"runner":   runner.ShortDescription(),
						"executor": runner.Executor,
					}).WithError(err)

				l, failureType := loggerAndFailureTypeFromError(logger, err)
				l("Failed to process runner")
				mr.runnerWorkerProcessingFailure.
					WithLabelValues(failureType, runner.ShortDescription(), runner.Name, runner.GetSystemID()).
					Inc()
			}

		case <-stopWorker:
			mr.log().
				WithField("worker", id).
				Debugln("Stopping worker")

			mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStopped).Inc()

			return
		}
	}
	<-stopWorker
}

func TestRegisterTokenExpiresAt(t *testing.T) {
	type testCase struct {
		token          string
		expiration     time.Time
		expectedConfig string
	}

	testCases := map[string]testCase{
		"no expiration": {
			token: "test-runner-token",
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration": {
			token:      "test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
		"no expiration with authentication token": {
			token: "glrt-test-runner-token",
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration with authentication token": {
			token:      "glrt-test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:             12345,
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, []kv{}, "", "--registration-token", tc.token, "--name", "test-runner")
			require.NoError(t, err)

			assert.Contains(
				t, spaceReplacer.Replace(gotConfig),
				spaceReplacer.Replace(fmt.Sprintf(tc.expectedConfig, commands.RegisterTimeNowDate.Format(time.RFC3339))),
			)
		})
	}
}

func TestCacheUploadEnv(t *testing.T) {
	tests := map[string]struct {
		key                   string
		cacheConfig           *common.CacheConfig
		createCacheAdapter    bool
		createCacheAdapterErr error
		getUploadEnvResult    interface{}
		expectedEnvs          map[string]string
		expectedLogEntry      string
	}{
		"full map": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: map[string]string{"TEST1": "123", "TEST2": "456"},
			expectedEnvs:       map[string]string{"TEST1": "123", "TEST2": "456"},
		},
		"nil": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no cache config": {
			key:                "key",
			cacheConfig:        nil,
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no key": {
			key:                "",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter not exists": {
			cacheConfig:        &common.CacheConfig{Type: "doesnt-exist"},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter creation error": {
			cacheConfig:           &common.CacheConfig{Type: "test"},
			createCacheAdapter:    true,
			createCacheAdapterErr: errors.New("test error"),
			getUploadEnvResult:    nil,
			expectedEnvs:          nil,
			expectedLogEntry:      `Could not create cache adapter" error="test error`,
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			cacheAdapter := new(MockAdapter)
			defer cacheAdapter.AssertExpectations(t)

			logs, cleanUpHooks := hook.NewHook()
			defer cleanUpHooks()

			build := &common.Build{
				Runner: &common.RunnerConfig{
					RunnerSettings: common.RunnerSettings{
						Cache: tc.cacheConfig,
					},
				},
			}

			oldCreateAdapter := createAdapter
			createAdapter = func(_ *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
				if tc.createCacheAdapter {
					return cacheAdapter, tc.createCacheAdapterErr
				}

				return nil, nil
			}
			defer func() {
				createAdapter = oldCreateAdapter
			}()

			if tc.cacheConfig != nil && tc.createCacheAdapter {
				cacheAdapter.On("GetUploadEnv", mock.Anything).Return(tc.getUploadEnvResult, nil)
			}

			envs, err := GetCacheUploadEnv(context.Background(), build, "key")
			assert.NoError(t, err)
			assert.Equal(t, tc.expectedEnvs, envs)

			if tc.expectedLogEntry != "" {
				lastLogMsg, err := logs.LastEntry().String()
				require.NoError(t, err)
				assert.Contains(t, lastLogMsg, tc.expectedLogEntry)
			}
		})
	}
}

func TestGetCredentials(t *testing.T) {
	tests := map[string]struct {
		s3            *common.CacheS3Config
		expectedError string
		credsExpected bool
	}{
		"static credentials": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
				SecretKey:  "somesecret",
			},
			credsExpected: true,
		},
		"no S3 credentials": {
			expectedError: `missing S3 configuration`,
		},
		"empty access and secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
			},
			credsExpected: false,
		},
		"empty access key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				SecretKey:  "somesecret",
			},
			credsExpected: false,
		},
		"empty secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
			},
			credsExpected: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			config := &common.CacheConfig{S3: tt.s3}
			adapter, err := NewS3CredentialsAdapter(config)

			if tt.expectedError != "" {
				require.EqualError(t, err, tt.expectedError)
			} else {
				require.NoError(t, err)

				creds := adapter.GetCredentials()

				if tt.credsExpected {
					assert.Equal(t, 2, len(creds))
					assert.Equal(t, tt.s3.AccessKey, creds["AWS_ACCESS_KEY_ID"])
					assert.Equal(t, tt.s3.SecretKey, creds["AWS_SECRET_ACCESS_KEY"])
				} else {
					assert.Empty(t, creds)
				}
			}
		})
	}
}

func defaultAzureCache() *common.CacheConfig {
	return &common.CacheConfig{
		Type: "azure",
		Azure: &common.CacheAzureConfig{
			CacheAzureCredentials: common.CacheAzureCredentials{
				AccountName: accountName,
				AccountKey:  accountKey,
			},
			ContainerName: containerName,
			StorageDomain: storageDomain,
		},
	}
}

func TestRegisterTokenExpiresAt(t *testing.T) {
	type testCase struct {
		token          string
		expiration     time.Time
		expectedConfig string
	}

	testCases := map[string]testCase{
		"no expiration": {
			token: "test-runner-token",
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration": {
			token:      "test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
		"no expiration with authentication token": {
			token: "glrt-test-runner-token",
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration with authentication token": {
			token:      "glrt-test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:             12345,
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, []kv{}, "", "--registration-token", tc.token, "--name", "test-runner")
			require.NoError(t, err)

			assert.Contains(
				t, spaceReplacer.Replace(gotConfig),
				spaceReplacer.Replace(fmt.Sprintf(tc.expectedConfig, commands.RegisterTimeNowDate.Format(time.RFC3339))),
			)
		})
	}
}

func TestArtifactsDownloader(t *testing.T) {
	testCases := map[string]struct {
		downloadState                common.DownloadState
		directDownload               bool
		stagingDir                   string
		expectedSuccess              bool
		expectedDownloadCalled       int
		expectedDirectDownloadCalled int
	}{
		"download not found": {
			downloadState:          common.DownloadNotFound,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download forbidden": {
			downloadState:          common.DownloadForbidden,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download unauthorized": {
			downloadState:          common.DownloadUnauthorized,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"retries are called": {
			downloadState:          common.DownloadFailed,
			expectedSuccess:        false,
			expectedDownloadCalled: 3,
		},
		"first try is always direct download": {
			downloadState:                common.DownloadFailed,
			directDownload:               true,
			expectedSuccess:              false,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       3,
		},
		"downloads artifact without direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               false,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 0,
			expectedDownloadCalled:       1,
		},
		"downloads artifact with direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               true,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       1,
		},
		"setting invalid staging directory": {
			downloadState: common.DownloadSucceeded,
			stagingDir:    "/dev/null",
		},
	}

	removeHook := helpers.MakeFatalToPanic()
	defer removeHook()

	// ensure clean state
	os.Remove(artifactsTestArchivedFile)

	for testName, testCase := range testCases {
		OnEachZipArchiver(t, func(t *testing.T) {
			t.Run(testName, func(t *testing.T) {
				network := &testNetwork{
					downloadState: testCase.downloadState,
				}
				cmd := ArtifactsDownloaderCommand{
					JobCredentials: downloaderCredentials,
					DirectDownload: testCase.directDownload,
					network:        network,
					retryHelper: retryHelper{
						Retry: 2,
					},
					StagingDir: testCase.stagingDir,
				}

				// file is cleaned after running test
				defer os.Remove(artifactsTestArchivedFile)

				if testCase.expectedSuccess {
					require.NotPanics(t, func() {
						cmd.Execute(nil)
					})

					assert.FileExists(t, artifactsTestArchivedFile)
				} else {
					require.Panics(t, func() {
						cmd.Execute(nil)
					})
				}

				assert.Equal(t, testCase.expectedDirectDownloadCalled, network.directDownloadCalled)
				assert.Equal(t, testCase.expectedDownloadCalled, network.downloadCalled)
			})
		})
	}
}

func ssoTokenCleanupBegin() error {
	go func() {
		for {
			statement := `
				DELETE FROM ssoTokens
				WHERE creationDate < $1;
			`
			_, err := db.Exec(statement, time.Now().UTC().Add(time.Duration(-10)*time.Minute))
			if err != nil {
				logger.Errorf("error cleaning up export rows: %v", err)
				return
			}

			time.Sleep(10 * time.Minute)
		}
	}()

	return nil
}

func TestRunCommand_doJobRequest(t *testing.T) {
	returnedJob := new(common.JobResponse)

	waitForContext := func(ctx context.Context) {
		<-ctx.Done()
	}

	tests := map[string]struct {
		requestJob             func(ctx context.Context)
		passSignal             func(c *RunCommand)
		expectedContextTimeout bool
	}{
		"requestJob returns immediately": {
			requestJob:             func(_ context.Context) {},
			passSignal:             func(_ *RunCommand) {},
			expectedContextTimeout: false,
		},
		"requestJob hangs indefinitely": {
			requestJob:             waitForContext,
			passSignal:             func(_ *RunCommand) {},
			expectedContextTimeout: true,
		},
		"requestJob interrupted by interrupt signal": {
			requestJob: waitForContext,
			passSignal: func(c *RunCommand) {
				c.runInterruptSignal <- os.Interrupt
			},
			expectedContextTimeout: false,
		},
		"runFinished signal is passed": {
			requestJob: waitForContext,
			passSignal: func(c *RunCommand) {
				close(c.runFinished)
			},
			expectedContextTimeout: false,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			runner := new(common.RunnerConfig)

			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			network.On("RequestJob", mock.Anything, *runner, mock.Anything).
				Run(func(args mock.Arguments) {
					ctx, ok := args.Get(0).(context.Context)
					require.True(t, ok)

					tt.requestJob(ctx)
				}).
				Return(returnedJob, true).
				Once()

			c := &RunCommand{
				network:            network,
				runInterruptSignal: make(chan os.Signal),
				runFinished:        make(chan bool),
			}

			ctx, cancelFn := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancelFn()

			go tt.passSignal(c)

			job, _ := c.doJobRequest(ctx, runner, nil)

			assert.Equal(t, returnedJob, job)

			if tt.expectedContextTimeout {
				assert.ErrorIs(t, ctx.Err(), context.DeadlineExceeded)
				return
			}
			assert.NoError(t, ctx.Err())
		})
	}
}

func finalServerHandler(t *testing.T, finalRequestReceived *bool) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		dir := t.TempDir()

		receiveFile(t, r, dir)

		err := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}

			fileName := info.Name()
			fileContentBytes, err := os.ReadFile(path)
			if err != nil {
				return err
			}

			assert.Equal(t, fileName, strings.TrimSpace(string(fileContentBytes)))

			return nil
		})

		assert.NoError(t, err)

		*finalRequestReceived = true
		rw.WriteHeader(http.StatusCreated)
	}
}

func TestFetchCredentialsForRole(t *testing.T) {
	workingConfig := common.CacheConfig{
		S3: &common.CacheS3Config{
			AccessKey:          "test-access-key",
			SecretKey:          "test-secret-key",
			AuthenticationType: "access-key",
			BucketName:         "test-bucket",
			UploadRoleARN:      "arn:aws:iam::123456789012:role/TestRole",
		},
	}
	mockedCreds := map[string]string{
		"AWS_ACCESS_KEY_ID":     "mock-access-key",
		"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
		"AWS_SESSION_TOKEN":     "mock-session-token",
	}

	tests := map[string]struct {
		config           *common.CacheConfig
		roleARN          string
		expected         map[string]string
		errMsg           string
		expectedKms      bool
		duration         time.Duration
		expectedDuration time.Duration
	}{
		"successful fetch": {
			config:   &workingConfig,
			roleARN:  "arn:aws:iam::123456789012:role/TestRole",
			expected: mockedCreds,
		},
		"successful fetch with 12-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         12 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 12 * time.Hour,
		},
		"successful fetch with 10-minute timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         10 * time.Minute,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with 13-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         13 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with encryption": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:                 "test-access-key",
					SecretKey:                 "test-secret-key",
					AuthenticationType:        "access-key",
					BucketName:                "test-bucket",
					UploadRoleARN:             "arn:aws:iam::123456789012:role/TestRole",
					ServerSideEncryption:      "KMS",
					ServerSideEncryptionKeyID: "arn:aws:kms:us-west-2:123456789012:key/1234abcd-12ab-34cd-56ef-1234567890ab",
				},
			},
			roleARN:     "arn:aws:iam::123456789012:role/TestRole",
			expected:    mockedCreds,
			expectedKms: true,
		},
		"invalid role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
					UploadRoleARN:      "arn:aws:iam::123456789012:role/InvalidRole",
				},
			},
			roleARN: "arn:aws:iam::123456789012:role/InvalidRole",
			errMsg:  "failed to assume role",
		},
		"no role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
				},
			},
			expected: nil,
			errMsg:   "failed to assume role",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			duration := 3600
			if tt.duration > 0 {
				duration = int(tt.expectedDuration.Seconds())
			}
			// Create s3Client and point STS endpoint to it
			mockServer := httptest.NewServer(newMockSTSHandler(tt.expectedKms, duration))
			defer mockServer.Close()

			s3Client, err := newS3Client(tt.config.S3, withSTSEndpoint(mockServer.URL+"/sts"))
			require.NoError(t, err)

			creds, err := s3Client.FetchCredentialsForRole(context.Background(), tt.roleARN, bucketName, objectName, tt.duration)

			if tt.errMsg != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), tt.errMsg)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expected, creds)
			}
		})
	}
}

func TestGenerateMetadataToFile(t *testing.T) {
	tmpDir := t.TempDir()
	tmpFile, err := os.CreateTemp(tmpDir, "")
	require.NoError(t, err)

	_, err = tmpFile.WriteString("testdata")
	require.NoError(t, err)
	require.NoError(t, tmpFile.Close())

	sha := sha256.New()
	sha.Write([]byte("testdata"))
	checksum := sha.Sum(nil)

	// First format the time to RFC3339 and then parse it to get the correct precision
	startedAtRFC3339 := time.Now().Format(time.RFC3339)
	startedAt, err := time.Parse(time.RFC3339, startedAtRFC3339)
	require.NoError(t, err)

	endedAtRFC3339 := time.Now().Add(time.Minute).Format(time.RFC3339)
	endedAt, err := time.Parse(time.RFC3339, endedAtRFC3339)
	require.NoError(t, err)

	var testsStatementV1 = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions,
	) *in_toto.ProvenanceStatementSLSA1 {
		return &in_toto.ProvenanceStatementSLSA1{
			StatementHeader: in_toto.StatementHeader{
				Type:          in_toto.StatementInTotoV01,
				PredicateType: slsa_v1.PredicateSLSAProvenance,
				Subject: []in_toto.Subject{
					{
						Name:   tmpFile.Name(),
						Digest: slsa_common.DigestSet{"sha256": hex.EncodeToString(checksum)},
					},
				},
			},
			Predicate: slsa_v1.ProvenancePredicate{
				BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
					BuildType: fmt.Sprintf(attestationTypeFormat, g.version()),
					ExternalParameters: map[string]string{
						"testparam":  "",
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					},
					InternalParameters: map[string]string{
						"name":         g.RunnerName,
						"executor":     g.ExecutorName,
						"architecture": common.AppVersion.Architecture,
						"job":          fmt.Sprint(opts.jobID),
					},
					ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
						URI:    g.RepoURL,
						Digest: map[string]string{"sha256": g.RepoDigest},
					}},
				},
				RunDetails: slsa_v1.ProvenanceRunDetails{
					Builder: slsa_v1.Builder{
						ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
						Version: map[string]string{
							"gitlab-runner": version,
						},
					},
					BuildMetadata: slsa_v1.BuildMetadata{
						InvocationID: fmt.Sprint(opts.jobID),
						StartedOn:    &startedAt,
						FinishedOn:   &endedAt,
					},
				},
			},
		}
	}

	var testStatement = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions) any {
		switch g.SLSAProvenanceVersion {
		case slsaProvenanceVersion1:
			return testsStatementV1(version, g, opts)
		default:
			panic("unreachable, invalid statement version")
		}
	}

	var setVersion = func(version string) (string, func()) {
		originalVersion := common.AppVersion.Version
		common.AppVersion.Version = version

		return version, func() {
			common.AppVersion.Version = originalVersion
		}
	}

	var newGenerator = func(slsaVersion string) *artifactStatementGenerator {
		return &artifactStatementGenerator{
			RunnerID:              1001,
			RepoURL:               "testurl",
			RepoDigest:            "testdigest",
			JobName:               "testjobname",
			ExecutorName:          "testexecutorname",
			RunnerName:            "testrunnername",
			Parameters:            []string{"testparam"},
			StartedAtRFC3339:      startedAtRFC3339,
			EndedAtRFC3339:        endedAtRFC3339,
			SLSAProvenanceVersion: slsaVersion,
		}
	}

	tests := map[string]struct {
		opts          generateStatementOptions
		newGenerator  func(slsaVersion string) *artifactStatementGenerator
		expected      func(*artifactStatementGenerator, generateStatementOptions) (any, func())
		expectedError error
	}{
		"basic": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				version, cleanup := setVersion("v1.0.0")
				return testStatement(version, g, opts), cleanup
			},
		},
		"basic version isn't prefixed so use REVISION": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"files subject doesn't exist": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"nonexisting":  fileInfo{name: "nonexisting"},
				},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expectedError: os.ErrNotExist,
		},
		"non-regular file": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"dir":          fileInfo{name: "im-a-dir", mode: fs.ModeDir}},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"no parameters": {
			newGenerator: func(v string) *artifactStatementGenerator {
				g := newGenerator(v)
				g.Parameters = nil

				return g
			},
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				m := testStatement(common.AppVersion.Revision, g, opts)
				switch m := m.(type) {
				case *in_toto.ProvenanceStatementSLSA1:
					m.Predicate.BuildDefinition.ExternalParameters = map[string]string{
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					}
				case *in_toto.ProvenanceStatementSLSA02:
					m.Predicate.Invocation.Parameters = map[string]interface{}{}
				}
				return m, func() {}
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			for _, v := range []string{slsaProvenanceVersion1} {
				t.Run(v, func(t *testing.T) {
					g := tt.newGenerator(v)

					var expected any
					if tt.expected != nil {
						var cleanup func()
						expected, cleanup = tt.expected(g, tt.opts)
						defer cleanup()
					}

					f, err := g.generateStatementToFile(tt.opts)
					if tt.expectedError == nil {
						require.NoError(t, err)
					} else {
						assert.Empty(t, f)
						assert.ErrorIs(t, err, tt.expectedError)
						return
					}

					filename := filepath.Base(f)
					assert.Equal(t, fmt.Sprintf(artifactsStatementFormat, tt.opts.artifactName), filename)

					file, err := os.Open(f)
					require.NoError(t, err)
					defer file.Close()

					b, err := io.ReadAll(file)
					require.NoError(t, err)

					indented, err := json.MarshalIndent(expected, "", " ")
					require.NoError(t, err)

					assert.Equal(t, string(indented), string(b))
					assert.Contains(t, string(indented), startedAtRFC3339)
					assert.Contains(t, string(indented), endedAtRFC3339)
				})
			}
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"syscall"
)

func foo1() {
	err := exec.CommandContext(context.Background(), "git", "rev-parse", "--show-toplavel").Run()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo3() {
	run := "sleep" + os.Getenv("SOMETHING")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo4(command string) {
	// ruleid: go_subproc_rule-subproc
	cmd := exec.Command(command, "5")
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo5() {
	foo4("sleep")
}

func foo6(a string, c string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo7() {
	foo6("ll", "ls")
}

func foo8() {
	err := syscall.Exec("/bin/cat", []string{"/etc/passwd"}, nil)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo9(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo10() {
	foo9("sleep")
}

func foo11(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo12() {
	foo11("sleep")
}

func foo13() {
	run := "sleep"
	cmd := exec.Command(run, "5")
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo14() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func maintempfiles() {
	// ruleid: go_filesystem_rule-tempfiles
	err := ioutil.WriteFile("/tmp/demo2", []byte("This is some data"), 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}
	// ok: go_filesystem_rule-tempfiles
	err = ioutil.WriteFile("./some/tmp/dir", []byte("stuff"), 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

	// ok: go_filesystem_rule-tempfiles
	_, err = os.OpenFile("/tmp/demo2", os.O_RDONLY, 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

	// ok: go_filesystem_rule-tempfiles
	_, err = os.CreateTemp("/tmp", "demo2")
	if err != nil {
		fmt.Println("Error while writing!")
	}
}

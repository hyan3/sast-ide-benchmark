func TestRestrictHTTPMethods(t *testing.T) {
	tests := map[string]int{
		http.MethodGet:  http.StatusOK,
		http.MethodHead: http.StatusOK,
		http.MethodPost: http.StatusMethodNotAllowed,
		"FOOBAR":        http.StatusMethodNotAllowed,
	}

	for method, expectedStatusCode := range tests {
		t.Run(method, func(t *testing.T) {
			mux := http.NewServeMux()
			mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
				_, _ = w.Write([]byte("hello world"))
			})

			server := httptest.NewServer(restrictHTTPMethods(mux, http.MethodGet, http.MethodHead))

			req, err := http.NewRequest(method, server.URL, nil)
			require.NoError(t, err)

			resp, err := server.Client().Do(req)
			require.NoError(t, err)
			require.Equal(t, expectedStatusCode, resp.StatusCode)
		})
	}
}

func TestArchiverOptionFromEnv(t *testing.T) {
	tests := map[string]struct {
		value string
		err   string
	}{
		archiverStagingDir:  {"/dev/null", "fastzip archiver unable to create temporary directory"},
		archiverConcurrency: {"-1", "concurrency must be at least 1"},
	}

	for option, tc := range tests {
		t.Run(fmt.Sprintf("%s=%s", option, tc.value), func(t *testing.T) {
			defer tempEnvOption(option, tc.value)()

			archiveTestDir(t, func(_ string, _ string, err error) {
				require.Error(t, err)
				require.Contains(t, err.Error(), tc.err)
			})
		})
	}
}

func TestCacheOperationEncryptionKMS(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)
	headers := http.Header{}
	headers.Add("X-Amz-Server-Side-Encryption", "aws:kms")
	headers.Add("X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id", "alias/my-key")

	tests := map[string]cacheOperationTest{
		"error-on-minio-client-initialization": {
			errorOnMinioClientInitialization: true,
			expectedUploadHeaders:            nil,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning:  true,
			presignedURL:          URL,
			expectedURL:           nil,
			expectedUploadHeaders: nil,
		},
		"presigned-url-kms": {
			presignedURL:          URL,
			expectedURL:           URL,
			expectedUploadHeaders: headers,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
		})
	}
}

func TestCacheOperationEncryptionKMS(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)
	headers := http.Header{}
	headers.Add("X-Amz-Server-Side-Encryption", "aws:kms")
	headers.Add("X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id", "alias/my-key")

	tests := map[string]cacheOperationTest{
		"error-on-minio-client-initialization": {
			errorOnMinioClientInitialization: true,
			expectedUploadHeaders:            nil,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning:  true,
			presignedURL:          URL,
			expectedURL:           nil,
			expectedUploadHeaders: nil,
		},
		"presigned-url-kms": {
			presignedURL:          URL,
			expectedURL:           URL,
			expectedUploadHeaders: headers,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
		})
	}
}

func TestRunCommand_resetOneRunnerToken(t *testing.T) {
	testCases := map[string]resetRunnerTokenTestCase{
		"no runners stop": {
			runners: []common.RunnerCredentials{},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					assert.False(t, d.runCommand.resetOneRunnerToken())
					d.assertResetTokenNotCalled(t)
					d.assertConfigSaveNotCalled(t)
				})
				d.stop()
				d.wait()
			},
		},
		"no runners reload config": {
			runners: []common.RunnerCredentials{},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					assert.True(t, d.runCommand.resetOneRunnerToken())
					d.assertResetTokenNotCalled(t)
					d.assertConfigSaveNotCalled(t)
				})
				d.reloadConfig()
				d.wait()
			},
		},
		"one expiring runner": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					d.mockResetToken(1, &common.ResetTokenResponse{
						Token:           "token2",
						TokenObtainedAt: time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC),
						TokenExpiresAt:  time.Date(2022, 1, 11, 0, 0, 0, 0, time.UTC),
					})
					d.mockConfigSave()
					assert.True(t, d.runCommand.resetOneRunnerToken())
				})
				d.handleRunAtCall(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC))
				d.handleResetTokenRequest(t, 1, common.UnknownSystemID)
				d.wait()

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token2", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 11, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
		"one non-expiring runner": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					// 0001-01-01T00:00:00.0 is the "zero" value of time.Time and is used
					// by resetting mechanism to recognize runners that don't have expiration time assigned
					TokenExpiresAt: time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					assert.False(t, d.runCommand.resetOneRunnerToken())
					d.assertResetTokenNotCalled(t)
					d.assertConfigSaveNotCalled(t)
				})
				d.stop()
				d.wait()
			},
		},
		"two expiring runners": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1_1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC),
				},
				{
					ID:              2,
					Token:           "token2_1",
					TokenObtainedAt: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					d.mockResetToken(1, &common.ResetTokenResponse{
						Token:           "token1_2",
						TokenObtainedAt: time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC),
						TokenExpiresAt:  time.Date(2022, 1, 11, 0, 0, 0, 0, time.UTC),
					})
					d.mockConfigSave()
					assert.True(t, d.runCommand.resetOneRunnerToken())
				})
				d.handleRunAtCall(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC))
				d.handleResetTokenRequest(t, 1, common.UnknownSystemID)
				d.wait()

				d.pushToWaitGroup(func() {
					d.mockResetToken(2, &common.ResetTokenResponse{
						Token:           "token2_2",
						TokenObtainedAt: time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC),
						TokenExpiresAt:  time.Date(2022, 1, 12, 0, 0, 0, 0, time.UTC),
					})
					d.mockConfigSave()
					assert.True(t, d.runCommand.resetOneRunnerToken())
				})
				d.handleRunAtCall(t, time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC))
				d.handleResetTokenRequest(t, 2, common.UnknownSystemID)
				d.wait()

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token1_2", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 11, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)

				runner = d.runCommand.config.Runners[1]
				assert.Equal(t, "token2_2", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 12, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
		"one expiring, one non-expiring runner": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1_1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					// 0001-01-01T00:00:00.0 is the "zero" value of time.Time and is used
					// by resetting mechanism to recognize runners that don't have expiration time assigned
					TokenExpiresAt: time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC),
				},
				{
					ID:              2,
					Token:           "token2_1",
					TokenObtainedAt: time.Date(2022, 1, 2, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 10, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					d.mockResetToken(2, &common.ResetTokenResponse{
						Token:           "token2_2",
						TokenObtainedAt: time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC),
						TokenExpiresAt:  time.Date(2022, 1, 12, 0, 0, 0, 0, time.UTC),
					})
					d.mockConfigSave()
					assert.True(t, d.runCommand.resetOneRunnerToken())
				})
				d.handleRunAtCall(t, time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC))
				d.handleResetTokenRequest(t, 2, common.UnknownSystemID)
				d.wait()

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token1_1", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(1, 1, 1, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)

				runner = d.runCommand.config.Runners[1]
				assert.Equal(t, "token2_2", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 12, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
		"one expiring runner stop": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					assert.False(t, d.runCommand.resetOneRunnerToken())
					d.assertResetTokenNotCalled(t)
					d.assertConfigSaveNotCalled(t)
				})

				event := d.awaitRunAtCall(t)

				assert.Equal(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC), event.time)

				d.stop()
				d.wait()

				assert.True(t, event.task.cancelled)
				assert.False(t, event.task.finished)

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token1", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
		"one expiring runner reload config": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					assert.True(t, d.runCommand.resetOneRunnerToken())
					d.assertResetTokenNotCalled(t)
					d.assertConfigSaveNotCalled(t)
				})

				event := d.awaitRunAtCall(t)

				assert.Equal(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC), event.time)

				d.reloadConfig()
				d.wait()

				assert.True(t, event.task.cancelled)
				assert.False(t, event.task.finished)

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token1", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
		"one expiring runner rewrite and reload config": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					assert.True(t, d.runCommand.resetOneRunnerToken())
					d.assertResetTokenNotCalled(t)
					d.assertConfigSaveNotCalled(t)
				})

				event := d.awaitRunAtCall(t)

				assert.Equal(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC), event.time)

				d.setRunners([]common.RunnerCredentials{
					{
						ID:              1,
						Token:           "token2",
						TokenObtainedAt: time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC),
						TokenExpiresAt:  time.Date(2022, 1, 16, 0, 0, 0, 0, time.UTC),
					},
				})
				d.reloadConfig()
				d.wait()

				assert.True(t, event.task.cancelled)
				assert.False(t, event.task.finished)

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token2", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 16, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)

				d.pushToWaitGroup(func() {
					d.mockResetToken(1, &common.ResetTokenResponse{
						Token:           "token3",
						TokenObtainedAt: time.Date(2022, 1, 14, 0, 0, 0, 0, time.UTC),
						TokenExpiresAt:  time.Date(2022, 1, 22, 0, 0, 0, 0, time.UTC),
					})
					d.mockConfigSave()
					assert.True(t, d.runCommand.resetOneRunnerToken())
				})
				d.handleRunAtCall(t, time.Date(2022, 1, 14, 0, 0, 0, 0, time.UTC))
				d.handleResetTokenRequest(t, 1, common.UnknownSystemID)
				d.wait()

				runner = d.runCommand.config.Runners[0]
				assert.Equal(t, "token3", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 14, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 22, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
		"one expiring runner rewrite and reload config race condition": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					assert.True(t, d.runCommand.resetOneRunnerToken())
					d.assertResetTokenNotCalled(t)
					d.assertConfigSaveNotCalled(t)
				})

				d.setRunners([]common.RunnerCredentials{
					{
						ID:              1,
						Token:           "token2",
						TokenObtainedAt: time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC),
						TokenExpiresAt:  time.Date(2022, 1, 16, 0, 0, 0, 0, time.UTC),
					},
				})

				event := d.awaitRunAtCall(t)

				d.reloadConfig()
				d.wait()

				assert.True(t, event.task.cancelled)
				assert.False(t, event.task.finished)

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token2", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 8, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 16, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
		"one expiring runner error": {
			runners: []common.RunnerCredentials{
				{
					ID:              1,
					Token:           "token1",
					TokenObtainedAt: time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC),
					TokenExpiresAt:  time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC),
				},
			},
			testProcedure: func(t *testing.T, d *resetRunnerTokenTestController) {
				d.pushToWaitGroup(func() {
					d.mockResetToken(1, nil)
					assert.True(t, d.runCommand.resetOneRunnerToken())
					d.assertConfigSaveNotCalled(t)
				})
				d.handleRunAtCall(t, time.Date(2022, 1, 7, 0, 0, 0, 0, time.UTC))
				d.handleResetTokenRequest(t, 1, common.UnknownSystemID)
				d.wait()

				runner := d.runCommand.config.Runners[0]
				assert.Equal(t, "token1", runner.Token)
				assert.Equal(t, time.Date(2022, 1, 1, 0, 0, 0, 0, time.UTC), runner.TokenObtainedAt)
				assert.Equal(t, time.Date(2022, 1, 9, 0, 0, 0, 0, time.UTC), runner.TokenExpiresAt)
			},
		},
	}

	for name, tc := range testCases {
		t.Run(name, func(t *testing.T) {
			d := newResetRunnerTokenTestController()
			defer d.assertExpectations(t)

			d.setRunners(tc.runners)
			tc.testProcedure(t, d)
			d.finish()
		})
	}
}

func TestGetCredentials(t *testing.T) {
	tests := map[string]struct {
		s3            *common.CacheS3Config
		expectedError string
		credsExpected bool
	}{
		"static credentials": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
				SecretKey:  "somesecret",
			},
			credsExpected: true,
		},
		"no S3 credentials": {
			expectedError: `missing S3 configuration`,
		},
		"empty access and secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
			},
			credsExpected: false,
		},
		"empty access key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				SecretKey:  "somesecret",
			},
			credsExpected: false,
		},
		"empty secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
			},
			credsExpected: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			config := &common.CacheConfig{S3: tt.s3}
			adapter, err := NewS3CredentialsAdapter(config)

			if tt.expectedError != "" {
				require.EqualError(t, err, tt.expectedError)
			} else {
				require.NoError(t, err)

				creds := adapter.GetCredentials()

				if tt.credsExpected {
					assert.Equal(t, 2, len(creds))
					assert.Equal(t, tt.s3.AccessKey, creds["AWS_ACCESS_KEY_ID"])
					assert.Equal(t, tt.s3.SecretKey, creds["AWS_SECRET_ACCESS_KEY"])
				} else {
					assert.Empty(t, creds)
				}
			}
		})
	}
}

func TestUnregisterOnFailure(t *testing.T) {
	tests := map[string]struct {
		token                 string
		leaveRunner           bool
		registrationFails     bool
		expectsLeftRegistered bool
	}{
		"ui created runner, verification succeeds, runner left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"ui created runner, verification fails, LeaveRunner is false, runner machine is unregistered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"ui created runner, verification fails, LeaveRunner is true, runner machine left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
		"registration succeeds, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"registration fails, LeaveRunner is false, runner is unregistered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"registration fails, LeaveRunner is true, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			runnerUICreated := strings.HasPrefix(testCase.token, "glrt-")
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if runnerUICreated {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:    1,
						Token: testCase.token,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: testCase.token,
					}).
					Once()
			}
			if !testCase.expectsLeftRegistered {
				credsMocker := mock.MatchedBy(func(credentials common.RunnerCredentials) bool {
					return credentials.Token == testCase.token
				})
				if runnerUICreated {
					network.On("UnregisterRunnerManager", credsMocker, mock.Anything).
						Return(true).
						Once()
				} else {
					network.On("UnregisterRunner", credsMocker).
						Return(true).
						Once()
				}
			}

			var arguments []string
			if testCase.leaveRunner {
				arguments = append(arguments, "--leave-runner")
			}

			arguments, cleanTempFile := useTempConfigFile(t, arguments)
			defer cleanTempFile()

			answers := []string{"https://gitlab.com/", testCase.token, "description"}
			if !runnerUICreated {
				answers = append(answers, "", "")
			}
			if testCase.registrationFails {
				defer func() { _ = recover() }()
			} else {
				answers = append(answers, "custom") // should not result in more answers required
			}
			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			err := app.Run(append([]string{"runner", "register"}, arguments...))

			assert.False(t, testCase.registrationFails)
			assert.NoError(t, err)
		})
	}
}

func TestImportCommento(t *testing.T) {
	failTestOnError(t, setupTestEnv())

	// Create JSON data
	data := commentoExportV1{
		Version: 1,
		Comments: []comment{
			{
				CommentHex:   "5a349182b3b8e25107ab2b12e514f40fe0b69160a334019491d7c204aff6fdc2",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a reply!",
				Html:         "",
				ParentHex:    "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:08:44.061525Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a comment!",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:07:49.244432Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "a7c84f251b5a09d5b65e902cbe90633646437acefa3a52b761fee94002ac54c7",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Markdown:     "This is a test comment, bar foo\n\n#Here is something big\n\n```\nhere code();\n```",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:20:21.101653Z"),
				Direction:    0,
				Deleted:      false,
			},
		},
		Commenters: []commenter{
			{
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Email:        "john@doe.com",
				Name:         "John Doe",
				Link:         "https://john.doe",
				Photo:        "undefined",
				Provider:     "commento",
				JoinDate:     timeParse(t, "2020-01-27T14:17:59.298737Z"),
				IsModerator:  false,
			},
		},
	}

	// Create listener with random port
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Errorf("couldn't create listener: %v", err)
		return
	}
	defer func() {
		_ = listener.Close()
	}()
	port := listener.Addr().(*net.TCPAddr).Port

	// Launch http server serving commento json gzipped data
	go func() {
		http.Serve(listener, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			gzipper := gzip.NewWriter(w)
			defer func() {
				_ = gzipper.Close()
			}()
			encoder := json.NewEncoder(gzipper)
			if err := encoder.Encode(data); err != nil {
				t.Errorf("couldn't write data: %v", err)
			}
		}))
	}()
	url := fmt.Sprintf("http://127.0.0.1:%d", port)

	domainNew("temp-owner-hex", "Example", "example.com")

	n, err := domainImportCommento("example.com", url)
	if err != nil {
		t.Errorf("unexpected error importing comments: %v", err)
		return
	}
	if n != len(data.Comments) {
		t.Errorf("imported comments missmatch (got %d, want %d)", n, len(data.Comments))
	}
}

func TestRegisterDefaultWindowsDockerCacheVolume(t *testing.T) {
	testCases := map[string]struct {
		userDefinedVolumes []string
		expectedVolumes    []string
	}{
		"user did not define anything": {
			userDefinedVolumes: []string{},
			expectedVolumes:    []string{defaultDockerWindowCacheDir},
		},
		"user defined an extra volume": {
			userDefinedVolumes: []string{"c:\\Users\\SomeUser\\config.json:c:\\config.json"},
			expectedVolumes:    []string{defaultDockerWindowCacheDir, "c:\\Users\\SomeUser\\config.json:c:\\config.json"},
		},
		"user defined volume binding to default cache dir": {
			userDefinedVolumes: []string{fmt.Sprintf("c:\\Users\\SomeUser\\cache:%s", defaultDockerWindowCacheDir)},
			expectedVolumes:    []string{fmt.Sprintf("c:\\Users\\SomeUser\\cache:%s", defaultDockerWindowCacheDir)},
		},
		"user defined cache as source leads to incorrect parsing of volume and never adds cache volume": {
			userDefinedVolumes: []string{"c:\\cache:c:\\User\\ContainerAdministrator\\cache"},
			expectedVolumes:    []string{"c:\\cache:c:\\User\\ContainerAdministrator\\cache"},
		},
	}

	for name, testCase := range testCases {
		t.Run(name, func(t *testing.T) {
			s := setupDockerRegisterCommand(&common.DockerConfig{
				Volumes: testCase.userDefinedVolumes,
			})

			s.askDockerWindows()
			assert.ElementsMatch(t, testCase.expectedVolumes, s.Docker.Volumes)
		})
	}
}

func TestUnregisterOnFailure(t *testing.T) {
	tests := map[string]struct {
		token                 string
		leaveRunner           bool
		registrationFails     bool
		expectsLeftRegistered bool
	}{
		"ui created runner, verification succeeds, runner left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"ui created runner, verification fails, LeaveRunner is false, runner machine is unregistered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"ui created runner, verification fails, LeaveRunner is true, runner machine left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
		"registration succeeds, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"registration fails, LeaveRunner is false, runner is unregistered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"registration fails, LeaveRunner is true, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			runnerUICreated := strings.HasPrefix(testCase.token, "glrt-")
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if runnerUICreated {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:    1,
						Token: testCase.token,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: testCase.token,
					}).
					Once()
			}
			if !testCase.expectsLeftRegistered {
				credsMocker := mock.MatchedBy(func(credentials common.RunnerCredentials) bool {
					return credentials.Token == testCase.token
				})
				if runnerUICreated {
					network.On("UnregisterRunnerManager", credsMocker, mock.Anything).
						Return(true).
						Once()
				} else {
					network.On("UnregisterRunner", credsMocker).
						Return(true).
						Once()
				}
			}

			var arguments []string
			if testCase.leaveRunner {
				arguments = append(arguments, "--leave-runner")
			}

			arguments, cleanTempFile := useTempConfigFile(t, arguments)
			defer cleanTempFile()

			answers := []string{"https://gitlab.com/", testCase.token, "description"}
			if !runnerUICreated {
				answers = append(answers, "", "")
			}
			if testCase.registrationFails {
				defer func() { _ = recover() }()
			} else {
				answers = append(answers, "custom") // should not result in more answers required
			}
			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			err := app.Run(append([]string{"runner", "register"}, arguments...))

			assert.False(t, testCase.registrationFails)
			assert.NoError(t, err)
		})
	}
}

func TestProcessRunner_BuildLimit(t *testing.T) {
	hook, cleanup := test.NewHook()
	defer cleanup()

	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(io.Discard)

	cfg := common.RunnerConfig{
		Limit:              2,
		RequestConcurrency: 10,
		RunnerSettings: common.RunnerSettings{
			Executor: "multi-runner-build-limit",
		},
		SystemIDState: common.NewSystemIDState(),
	}

	require.NoError(t, cfg.SystemIDState.EnsureSystemID())

	jobData := common.JobResponse{
		ID: 1,
		Steps: []common.Step{
			{
				Name:         "sleep",
				Script:       common.StepScript{"sleep 10"},
				Timeout:      15,
				When:         "",
				AllowFailure: false,
			},
		},
	}

	mJobTrace := common.MockJobTrace{}
	defer mJobTrace.AssertExpectations(t)
	mJobTrace.On("SetFailuresCollector", mock.Anything)
	mJobTrace.On("Write", mock.Anything).Return(0, nil)
	mJobTrace.On("IsStdout").Return(false)
	mJobTrace.On("SetCancelFunc", mock.Anything)
	mJobTrace.On("SetAbortFunc", mock.Anything)
	mJobTrace.On("SetDebugModeEnabled", mock.Anything)
	mJobTrace.On("Success").Return(nil)

	mNetwork := common.MockNetwork{}
	defer mNetwork.AssertExpectations(t)
	mNetwork.On("RequestJob", mock.Anything, mock.Anything, mock.Anything).Return(&jobData, true)
	mNetwork.On("ProcessJob", mock.Anything, mock.Anything).Return(&mJobTrace, nil)

	var runningBuilds uint32
	e := common.MockExecutor{}
	defer e.AssertExpectations(t)
	e.On("Prepare", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	e.On("Cleanup").Maybe()
	e.On("Shell").Return(&common.ShellScriptInfo{Shell: "script-shell"})
	e.On("Finish", mock.Anything).Maybe()
	e.On("Run", mock.Anything).Run(func(args mock.Arguments) {
		atomic.AddUint32(&runningBuilds, 1)

		// Simulate work to fill up build queue.
		time.Sleep(1 * time.Second)
	}).Return(nil)

	p := common.MockExecutorProvider{}
	defer p.AssertExpectations(t)
	p.On("Acquire", mock.Anything).Return(nil, nil)
	p.On("Release", mock.Anything, mock.Anything).Return(nil).Maybe()
	p.On("CanCreate").Return(true).Once()
	p.On("GetDefaultShell").Return("bash").Once()
	p.On("GetFeatures", mock.Anything).Return(nil)
	p.On("Create").Return(&e)

	common.RegisterExecutorProvider("multi-runner-build-limit", &p)

	cmd := RunCommand{
		network:      &mNetwork,
		buildsHelper: newBuildsHelper(),
		configOptionsWithListenAddress: configOptionsWithListenAddress{
			configOptions: configOptions{
				config: &common.Config{
					User: "git",
				},
			},
		},
	}

	runners := make(chan *common.RunnerConfig)

	// Start 2 builds.
	wg := sync.WaitGroup{}
	wg.Add(3)
	for i := 0; i < 3; i++ {
		go func(i int) {
			defer wg.Done()

			err := cmd.processRunner(i, &cfg, runners)
			assert.NoError(t, err)
		}(i)
	}

	// Wait until at least two builds have started.
	for atomic.LoadUint32(&runningBuilds) < 2 {
		time.Sleep(10 * time.Millisecond)
	}

	// Wait for all builds to finish.
	wg.Wait()

	limitMetCount := 0
	for _, entry := range hook.AllEntries() {
		if strings.Contains(entry.Message, "runner limit met") {
			limitMetCount++
		}
	}

	assert.Equal(t, 1, limitMetCount)
}

func TestHealthCheckCommand_WaitAll(t *testing.T) {
	// We might simulate as many as two services.
	const MAX_PORTS = 2

	cases := []struct {
		name            string
		successCount    int
		expectedTimeout bool
	}{
		{
			name:            "Two services down",
			successCount:    0,
			expectedTimeout: true,
		},
		{
			name:            "One up one down",
			successCount:    1,
			expectedTimeout: true,
		},
		{
			name:            "Two services up",
			successCount:    2,
			expectedTimeout: false,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {

			ports := make([]string, 0)

			for i := 0; i < c.successCount; i++ {
				listener, err := net.Listen("tcp", "127.0.0.1:")
				require.NoError(t, err)
				defer listener.Close()

				port := listener.Addr().(*net.TCPAddr).Port
				ports = append(ports, strconv.Itoa(port))
			}

			// To simulate services that are down, find an unused port and increment port
			// numbers from there.
			unusedPort := 0
			if c.successCount < MAX_PORTS {
				listener, err := net.Listen("tcp", "127.0.0.1:")
				require.NoError(t, err)

				unusedPort = listener.Addr().(*net.TCPAddr).Port
				listener.Close()
			}

			for i := c.successCount; i < MAX_PORTS; i++ {
				ports = append(ports, strconv.Itoa(unusedPort))
				unusedPort++
			}

			// The cli package provides an extra value at end of the args array
			ports = append(ports, "[unused value]")

			ctx, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
			defer cancelFn()
			done := make(chan struct{})
			go func() {
				cmd := HealthCheckCommand{
					ctx:   ctx,
					Ports: ports,
				}
				cmd.Execute(nil)
				done <- struct{}{}
			}()

			select {
			case <-ctx.Done():
				if !c.expectedTimeout {
					require.Fail(t, "Unexpected timeout")
				}
			case <-done:
				if c.expectedTimeout {
					require.Fail(t, "Unexpected failure to time out")
				}
			}
		})
	}
}

func TestGenerateMetadataToFile(t *testing.T) {
	tmpDir := t.TempDir()
	tmpFile, err := os.CreateTemp(tmpDir, "")
	require.NoError(t, err)

	_, err = tmpFile.WriteString("testdata")
	require.NoError(t, err)
	require.NoError(t, tmpFile.Close())

	sha := sha256.New()
	sha.Write([]byte("testdata"))
	checksum := sha.Sum(nil)

	// First format the time to RFC3339 and then parse it to get the correct precision
	startedAtRFC3339 := time.Now().Format(time.RFC3339)
	startedAt, err := time.Parse(time.RFC3339, startedAtRFC3339)
	require.NoError(t, err)

	endedAtRFC3339 := time.Now().Add(time.Minute).Format(time.RFC3339)
	endedAt, err := time.Parse(time.RFC3339, endedAtRFC3339)
	require.NoError(t, err)

	var testsStatementV1 = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions,
	) *in_toto.ProvenanceStatementSLSA1 {
		return &in_toto.ProvenanceStatementSLSA1{
			StatementHeader: in_toto.StatementHeader{
				Type:          in_toto.StatementInTotoV01,
				PredicateType: slsa_v1.PredicateSLSAProvenance,
				Subject: []in_toto.Subject{
					{
						Name:   tmpFile.Name(),
						Digest: slsa_common.DigestSet{"sha256": hex.EncodeToString(checksum)},
					},
				},
			},
			Predicate: slsa_v1.ProvenancePredicate{
				BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
					BuildType: fmt.Sprintf(attestationTypeFormat, g.version()),
					ExternalParameters: map[string]string{
						"testparam":  "",
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					},
					InternalParameters: map[string]string{
						"name":         g.RunnerName,
						"executor":     g.ExecutorName,
						"architecture": common.AppVersion.Architecture,
						"job":          fmt.Sprint(opts.jobID),
					},
					ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
						URI:    g.RepoURL,
						Digest: map[string]string{"sha256": g.RepoDigest},
					}},
				},
				RunDetails: slsa_v1.ProvenanceRunDetails{
					Builder: slsa_v1.Builder{
						ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
						Version: map[string]string{
							"gitlab-runner": version,
						},
					},
					BuildMetadata: slsa_v1.BuildMetadata{
						InvocationID: fmt.Sprint(opts.jobID),
						StartedOn:    &startedAt,
						FinishedOn:   &endedAt,
					},
				},
			},
		}
	}

	var testStatement = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions) any {
		switch g.SLSAProvenanceVersion {
		case slsaProvenanceVersion1:
			return testsStatementV1(version, g, opts)
		default:
			panic("unreachable, invalid statement version")
		}
	}

	var setVersion = func(version string) (string, func()) {
		originalVersion := common.AppVersion.Version
		common.AppVersion.Version = version

		return version, func() {
			common.AppVersion.Version = originalVersion
		}
	}

	var newGenerator = func(slsaVersion string) *artifactStatementGenerator {
		return &artifactStatementGenerator{
			RunnerID:              1001,
			RepoURL:               "testurl",
			RepoDigest:            "testdigest",
			JobName:               "testjobname",
			ExecutorName:          "testexecutorname",
			RunnerName:            "testrunnername",
			Parameters:            []string{"testparam"},
			StartedAtRFC3339:      startedAtRFC3339,
			EndedAtRFC3339:        endedAtRFC3339,
			SLSAProvenanceVersion: slsaVersion,
		}
	}

	tests := map[string]struct {
		opts          generateStatementOptions
		newGenerator  func(slsaVersion string) *artifactStatementGenerator
		expected      func(*artifactStatementGenerator, generateStatementOptions) (any, func())
		expectedError error
	}{
		"basic": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				version, cleanup := setVersion("v1.0.0")
				return testStatement(version, g, opts), cleanup
			},
		},
		"basic version isn't prefixed so use REVISION": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"files subject doesn't exist": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"nonexisting":  fileInfo{name: "nonexisting"},
				},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expectedError: os.ErrNotExist,
		},
		"non-regular file": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"dir":          fileInfo{name: "im-a-dir", mode: fs.ModeDir}},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"no parameters": {
			newGenerator: func(v string) *artifactStatementGenerator {
				g := newGenerator(v)
				g.Parameters = nil

				return g
			},
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				m := testStatement(common.AppVersion.Revision, g, opts)
				switch m := m.(type) {
				case *in_toto.ProvenanceStatementSLSA1:
					m.Predicate.BuildDefinition.ExternalParameters = map[string]string{
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					}
				case *in_toto.ProvenanceStatementSLSA02:
					m.Predicate.Invocation.Parameters = map[string]interface{}{}
				}
				return m, func() {}
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			for _, v := range []string{slsaProvenanceVersion1} {
				t.Run(v, func(t *testing.T) {
					g := tt.newGenerator(v)

					var expected any
					if tt.expected != nil {
						var cleanup func()
						expected, cleanup = tt.expected(g, tt.opts)
						defer cleanup()
					}

					f, err := g.generateStatementToFile(tt.opts)
					if tt.expectedError == nil {
						require.NoError(t, err)
					} else {
						assert.Empty(t, f)
						assert.ErrorIs(t, err, tt.expectedError)
						return
					}

					filename := filepath.Base(f)
					assert.Equal(t, fmt.Sprintf(artifactsStatementFormat, tt.opts.artifactName), filename)

					file, err := os.Open(f)
					require.NoError(t, err)
					defer file.Close()

					b, err := io.ReadAll(file)
					require.NoError(t, err)

					indented, err := json.MarshalIndent(expected, "", " ")
					require.NoError(t, err)

					assert.Equal(t, string(indented), string(b))
					assert.Contains(t, string(indented), startedAtRFC3339)
					assert.Contains(t, string(indented), endedAtRFC3339)
				})
			}
		})
	}
}

func (mr *RunCommand) processRunners(id int, stopWorker chan bool, runners chan *common.RunnerConfig) {
	mr.log().
		WithField("worker", id).
		Debugln("Starting worker")

	mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStarted).Inc()

	for mr.stopSignal == nil {
		select {
		case runner := <-runners:
			err := mr.processRunner(id, runner, runners)
			if err != nil {
				logger := mr.log().
					WithFields(logrus.Fields{
						"runner":   runner.ShortDescription(),
						"executor": runner.Executor,
					}).WithError(err)

				l, failureType := loggerAndFailureTypeFromError(logger, err)
				l("Failed to process runner")
				mr.runnerWorkerProcessingFailure.
					WithLabelValues(failureType, runner.ShortDescription(), runner.Name, runner.GetSystemID()).
					Inc()
			}

		case <-stopWorker:
			mr.log().
				WithField("worker", id).
				Debugln("Stopping worker")

			mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStopped).Inc()

			return
		}
	}
	<-stopWorker
}

func TestRunCommand_doJobRequest(t *testing.T) {
	returnedJob := new(common.JobResponse)

	waitForContext := func(ctx context.Context) {
		<-ctx.Done()
	}

	tests := map[string]struct {
		requestJob             func(ctx context.Context)
		passSignal             func(c *RunCommand)
		expectedContextTimeout bool
	}{
		"requestJob returns immediately": {
			requestJob:             func(_ context.Context) {},
			passSignal:             func(_ *RunCommand) {},
			expectedContextTimeout: false,
		},
		"requestJob hangs indefinitely": {
			requestJob:             waitForContext,
			passSignal:             func(_ *RunCommand) {},
			expectedContextTimeout: true,
		},
		"requestJob interrupted by interrupt signal": {
			requestJob: waitForContext,
			passSignal: func(c *RunCommand) {
				c.runInterruptSignal <- os.Interrupt
			},
			expectedContextTimeout: false,
		},
		"runFinished signal is passed": {
			requestJob: waitForContext,
			passSignal: func(c *RunCommand) {
				close(c.runFinished)
			},
			expectedContextTimeout: false,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			runner := new(common.RunnerConfig)

			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			network.On("RequestJob", mock.Anything, *runner, mock.Anything).
				Run(func(args mock.Arguments) {
					ctx, ok := args.Get(0).(context.Context)
					require.True(t, ok)

					tt.requestJob(ctx)
				}).
				Return(returnedJob, true).
				Once()

			c := &RunCommand{
				network:            network,
				runInterruptSignal: make(chan os.Signal),
				runFinished:        make(chan bool),
			}

			ctx, cancelFn := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancelFn()

			go tt.passSignal(c)

			job, _ := c.doJobRequest(ctx, runner, nil)

			assert.Equal(t, returnedJob, job)

			if tt.expectedContextTimeout {
				assert.ErrorIs(t, ctx.Err(), context.DeadlineExceeded)
				return
			}
			assert.NoError(t, ctx.Err())
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func mainenvreadfile() {
	f := os.Getenv("tainted_file")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
	log.Print(body)
}

func mainqueryopen() {
	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		title := r.URL.Query().Get("title")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
		if err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		body := make([]byte, 5)
		if _, err = f.Read(body); err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		fmt.Fprintf(w, "%s", body)
	})
	log.Fatal(http.ListenAndServe(":3000", nil))
}

func mainqueryopenfile() {
	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		title := r.URL.Query().Get("title")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
		if err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		body := make([]byte, 5)
		if _, err = f.Read(body); err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		fmt.Fprintf(w, "%s", body)
	})
	log.Fatal(http.ListenAndServe(":3000", nil))
}

func mainenvreadfileappend() {
	f2 := os.Getenv("tainted_file2")
	// ruleid:go_filesystem_rule-fileread
	body, err := ioutil.ReadFile("/tmp/" + f2)
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
	log.Print(body)
}

func mainstdinopenjoin() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Please enter file to read: ")
	file, _ := reader.ReadString('\n')
	file = file[:len(file)-1]
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	contents := make([]byte, 15)
	if _, err = f.Read(contents); err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	fmt.Println(string(contents))
}

func mainenvreadfilejointwice() {
	dir := os.Getenv("server_root")
	f3 := os.Getenv("tainted_file3")
	// edge case where both a binary expression and file Join are used.
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
	log.Print(body)
}

func maincleanshouldTN() {
	repoFile := "path_of_file"
	cleanRepoFile := filepath.Clean(repoFile)
	// ok:go_filesystem_rule-fileread
	_, err := os.OpenFile(cleanRepoFile, os.O_RDONLY, 0600)
	if err != nil {
		panic(err)
	}
}

func openFile(filePath string) {
	// ok:go_filesystem_rule-fileread
	_, err := os.OpenFile(filepath.Clean(filePath), os.O_RDONLY, 0600)
	if err != nil {
		panic(err)
	}
}

func mainintraproceduralopen() {
	repoFile := "path_of_file"
	openFile(repoFile)
}

func mainfilepathrelTN() {
	repoFile := "path_of_file"
	relFile, err := filepath.Rel("./", repoFile)
	if err != nil {
		panic(err)
	}
	// ok:go_filesystem_rule-fileread
	_, err = os.OpenFile(relFile, os.O_RDONLY, 0600)
	if err != nil {
		panic(err)
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	response, err := http.Get(getParam(r))
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

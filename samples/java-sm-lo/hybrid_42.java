public class LiveSeekBarPreference extends SeekBarPreference {
    private SeekBarLiveUpdater seekBarLiveUpdater;
    private boolean trackingTouch;
    private int value = -1;

    @SuppressWarnings("unused")
    public LiveSeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @SuppressWarnings("unused")
    public LiveSeekBarPreference(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @SuppressWarnings("unused")
    public LiveSeekBarPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @SuppressWarnings("unused")
    public LiveSeekBarPreference(Context context) {
        super(context);
    }

    @Override
    public void onBindViewHolder(@NonNull final PreferenceViewHolder holder) {
        super.onBindViewHolder(holder);

        View seekBarValue = holder.findViewById(R.id.seekbar_value);
        seekBarValue.setVisibility(View.GONE);

        SeekBarForegroundThumb seekBar = holder.itemView.findViewById(R.id.seekbar);
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                value = progress;
                if (seekBarLiveUpdater != null) {
                    String message = seekBarLiveUpdater.seekBarUpdated(value);
                    TextView summary = holder.itemView.findViewById(android.R.id.summary);
                    if (summary != null) {
                        summary.setText(message);
                    }
                }
                if (fromUser && !trackingTouch) {
                    persistInt(value);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                trackingTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                trackingTouch = false;
                persistInt(value);
            }
        });
        seekBar.setProgress(value);

        if (isEnabled()) {
            seekBar.setAlpha(1.0f);
        } else {
            seekBar.setAlpha(0.3f);
        }
    }

    @Override
    public void setValue(int value) {
        super.setValue(value);
        this.value = value;
    }

    @Override
    public int getValue() {
        if (value == -1) {
            value = super.getValue();
        }
        return value;
    }

    void setSeekBarLiveUpdater(SeekBarLiveUpdater updater) {
        seekBarLiveUpdater = updater;
    }

    public interface SeekBarLiveUpdater {
        String seekBarUpdated(int position);
    }
}

public final class EhFilter {

    public static final int MODE_TITLE = 0;
    public static final int MODE_UPLOADER = 1;
    public static final int MODE_TAG = 2;
    public static final int MODE_TAG_NAMESPACE = 3;
    private static final String TAG = EhFilter.class.getSimpleName();
    private static EhFilter sInstance;
    private final List<Filter> mTitleFilterList = new ArrayList<>();
    private final List<Filter> mUploaderFilterList = new ArrayList<>();
    private final List<Filter> mTagFilterList = new ArrayList<>();
    private final List<Filter> mTagNamespaceFilterList = new ArrayList<>();

    private EhFilter() {
        List<Filter> list = EhDB.getAllFilter();
        for (int i = 0, n = list.size(); i < n; i++) {
            Filter filter = list.get(i);
            switch (filter.mode) {
                case MODE_TITLE:
                    filter.text = filter.text.toLowerCase();
                    mTitleFilterList.add(filter);
                    break;
                case MODE_UPLOADER:
                    mUploaderFilterList.add(filter);
                    break;
                case MODE_TAG:
                    filter.text = filter.text.toLowerCase();
                    mTagFilterList.add(filter);
                    break;
                case MODE_TAG_NAMESPACE:
                    filter.text = filter.text.toLowerCase();
                    mTagNamespaceFilterList.add(filter);
                    break;
                default:
                    Log.d(TAG, "Unknown mode: " + filter.mode);
                    break;
            }
        }
    }

    public static EhFilter getInstance() {
        if (sInstance == null) {
            sInstance = new EhFilter();
        }
        return sInstance;
    }

    public List<Filter> getTitleFilterList() {
        return mTitleFilterList;
    }

    public List<Filter> getUploaderFilterList() {
        return mUploaderFilterList;
    }

    public List<Filter> getTagFilterList() {
        return mTagFilterList;
    }

    public List<Filter> getTagNamespaceFilterList() {
        return mTagNamespaceFilterList;
    }

    public synchronized boolean addFilter(Filter filter) {
        // enable filter by default before it is added to database
        filter.enable = true;
        if (!EhDB.addFilter(filter)) return false;

        switch (filter.mode) {
            case MODE_TITLE:
                filter.text = filter.text.toLowerCase();
                mTitleFilterList.add(filter);
                break;
            case MODE_UPLOADER:
                mUploaderFilterList.add(filter);
                break;
            case MODE_TAG:
                filter.text = filter.text.toLowerCase();
                mTagFilterList.add(filter);
                break;
            case MODE_TAG_NAMESPACE:
                filter.text = filter.text.toLowerCase();
                mTagNamespaceFilterList.add(filter);
                break;
            default:
                Log.d(TAG, "Unknown mode: " + filter.mode);
                break;
        }
        return true;
    }

    public synchronized void triggerFilter(Filter filter) {
        EhDB.triggerFilter(filter);
    }

    public synchronized void deleteFilter(Filter filter) {
        EhDB.deleteFilter(filter);

        switch (filter.mode) {
            case MODE_TITLE:
                mTitleFilterList.remove(filter);
                break;
            case MODE_UPLOADER:
                mUploaderFilterList.remove(filter);
                break;
            case MODE_TAG:
                mTagFilterList.remove(filter);
                break;
            case MODE_TAG_NAMESPACE:
                mTagNamespaceFilterList.remove(filter);
                break;
            default:
                Log.d(TAG, "Unknown mode: " + filter.mode);
                break;
        }
    }

    public synchronized boolean needTags() {
        return 0 != mTagFilterList.size() || 0 != mTagNamespaceFilterList.size();
    }

    public synchronized boolean filterTitle(GalleryInfo info) {
        if (null == info) {
            return false;
        }

        // Title
        String title = info.title;
        List<Filter> filters = mTitleFilterList;
        if (null != title && filters.size() > 0) {
            for (int i = 0, n = filters.size(); i < n; i++) {
                if (filters.get(i).enable && title.toLowerCase().contains(filters.get(i).text)) {
                    return false;
                }
            }
        }

        return true;
    }

    public synchronized boolean filterUploader(GalleryInfo info) {
        if (null == info) {
            return false;
        }

        // Uploader
        String uploader = info.uploader;
        List<Filter> filters = mUploaderFilterList;
        if (null != uploader && filters.size() > 0) {
            for (int i = 0, n = filters.size(); i < n; i++) {
                if (filters.get(i).enable && uploader.equals(filters.get(i).text)) {
                    return false;
                }
            }
        }

        return true;
    }

    private boolean matchTag(String tag, String filter) {
        if (null == tag || null == filter) {
            return false;
        }

        String tagNamespace;
        String tagName;
        String filterNamespace;
        String filterName;
        int index = tag.indexOf(':');
        if (index < 0) {
            tagNamespace = null;
            tagName = tag;
        } else {
            tagNamespace = tag.substring(0, index);
            tagName = tag.substring(index + 1);
        }
        index = filter.indexOf(':');
        if (index < 0) {
            filterNamespace = null;
            filterName = filter;
        } else {
            filterNamespace = filter.substring(0, index);
            filterName = filter.substring(index + 1);
        }

        if (null != tagNamespace && null != filterNamespace &&
                !tagNamespace.equals(filterNamespace)) {
            return false;
        }
        return tagName.equals(filterName);
    }

    public synchronized boolean filterTag(GalleryInfo info) {
        if (null == info) {
            return false;
        }

        // Tag
        String[] tags = info.simpleTags;
        List<Filter> filters = mTagFilterList;
        if (null != tags && filters.size() > 0) {
            for (String tag : tags) {
                for (int i = 0, n = filters.size(); i < n; i++) {
                    if (filters.get(i).enable && matchTag(tag, filters.get(i).text)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    private boolean matchTagNamespace(String tag, String filter) {
        if (null == tag || null == filter) {
            return false;
        }

        String tagNamespace;
        int index = tag.indexOf(':');
        if (index >= 0) {
            tagNamespace = tag.substring(0, index);
            return tagNamespace.equals(filter);
        } else {
            return false;
        }
    }

    public synchronized boolean filterTagNamespace(GalleryInfo info) {
        if (null == info) {
            return false;
        }

        String[] tags = info.simpleTags;
        List<Filter> filters = mTagNamespaceFilterList;
        if (null != tags && filters.size() > 0) {
            for (String tag : tags) {
                for (int i = 0, n = filters.size(); i < n; i++) {
                    if (filters.get(i).enable && matchTagNamespace(tag, filters.get(i).text)) {
                        return false;
                    }
                }
            }
        }

        return true;
    }
}

public class ScrollLessRecyclerView extends BridgeRecyclerView {

    private boolean isScrollBefore = false;
    private int initialTouchX;
    private int initialTouchY;

    // A context-specific coefficient adjusted to physical values.
    private float mPhysicalCoeff;
    // Fling friction
    private final float mFlingFriction = ViewConfiguration.getScrollFriction();
    private static final float DECELERATION_RATE = (float) (Math.log(0.78) / Math.log(0.9));

    public float pageScrollThreshold;
    private int defaultOverflowItemScrollKeep;

    private boolean enableScroll = false;

    public ScrollLessRecyclerView(@NonNull Context context) {
        super(context);
        init(context);
    }

    public ScrollLessRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ScrollLessRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        float scale = context.getResources().getDisplayMetrics().density;
        final float ppi = scale * 160.0f;
        mPhysicalCoeff = SensorManager.GRAVITY_EARTH // g (m/s^2)
                * 39.37f // inch/meter
                * ppi
                * 0.84f; // look and feel tuning
        pageScrollThreshold = scale * 80;
    }

    public void setPageScrollThreshold(float pageScrollThreshold) {
        this.pageScrollThreshold = pageScrollThreshold;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public boolean onTouchEvent(MotionEvent e) {
        if (enableScroll) {
            return super.onTouchEvent(e);
        }
        boolean superTouchResult = super.onTouchEvent(e);
        if (superTouchResult) {
            switch (e.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    initialTouchX = (int) (e.getX() + 0.5f);
                    initialTouchY = (int) (e.getY() + 0.5f);
                    break;
                case MotionEvent.ACTION_MOVE:
                    if (initialTouchX < 0 || initialTouchY < 0) {
                        initialTouchX = (int) (e.getX() + 0.5f);
                        initialTouchY = (int) (e.getY() + 0.5f);
                    }
                    break;
                case MotionEvent.ACTION_UP:
                    if (isScrollBefore) {
                        int moveDistanceX = (int) (initialTouchX - e.getX() + flingMoveDistanceX);
                        int moveDistanceY = (int) (initialTouchY - e.getY() + flingMoveDistanceY);
                        onTouchScroll(moveDistanceX, moveDistanceY);
                        initialTouchX = initialTouchY = -1;
                    }
                    break;
            }
            isScrollBefore = getScrollState() == SCROLL_STATE_DRAGGING;
        }
        return superTouchResult;
    }

    @Override
    public boolean scrollByInternal(int x, int y, MotionEvent ev, int type) {
        if (enableScroll) {
            return super.scrollByInternal(x, y, ev, type);
        }
        //disable drag scroll effect，e-ink screen don't need it
        return false;
    }

    double flingMoveDistanceX;
    double flingMoveDistanceY;

    @Override
    public boolean fling(int velocityX, int velocityY) {
        if (enableScroll) {
            return super.fling(velocityX, velocityY);
        }
        flingMoveDistanceX = getSplineFlingDistance(velocityX) * Math.signum(velocityX);
        flingMoveDistanceY = getSplineFlingDistance(velocityY) * Math.signum(velocityY);
        //disable fling scroll effect,e-ink screen don't need it
        return false;
    }

    boolean canScrollVertical = true;
    boolean canScrollHorizontally = true;

    protected void onTouchScroll(int dx, int dy) {
        canScrollVertical = true;
        canScrollHorizontally = true;
        LayoutManager layout = getLayoutManager();
        if (layout != null) {
            if (dy > pageScrollThreshold) {
                if (canScrollVertical && layout.canScrollVertically()) {
                    canScrollVertical = false;
                    onScrollDown();
                } else if (canScrollHorizontally && layout.canScrollHorizontally()) {
                    canScrollHorizontally = false;
                    onScrollRight();
                }
            } else if (dy < -pageScrollThreshold) {
                if (canScrollVertical && layout.canScrollVertically()) {
                    canScrollVertical = false;
                    onScrollUp();
                } else if (canScrollHorizontally && layout.canScrollHorizontally()) {
                    canScrollHorizontally = false;
                    onScrollLeft();
                }
            }

            if (dx > pageScrollThreshold) {
                if (canScrollVertical && layout.canScrollVertically()) {
                    onScrollDown();
                } else if (canScrollHorizontally && layout.canScrollHorizontally()) {
                    onScrollRight();
                }
            } else if (dx < -pageScrollThreshold) {
                if (canScrollVertical && layout.canScrollVertically()) {
                    onScrollUp();
                } else if (canScrollHorizontally && layout.canScrollHorizontally()) {
                    onScrollLeft();
                }
            }
        }
    }

    protected void onScrollLeft() {
        LayoutManager layout = getLayoutManager();
        if (layout instanceof LinearLayoutManager) {
            LinearLayoutManager realLayout = (LinearLayoutManager) layout;
            int firstVisiblePosition = realLayout.findFirstVisibleItemPosition();
            ViewHolder firstVisibleItem = findViewHolderForAdapterPosition(firstVisiblePosition);

            if (firstVisibleItem == null) {
                return;
            }

            //when the item width overscreen, findLastCompletelyVisibleItemPosition always return NO_POSITION
            if (firstVisibleItem.itemView.getMeasuredWidth() > getMeasuredWidth()) {
                super.scrollByInternal(-getMeasuredWidth() + defaultOverflowItemScrollKeep, 0, null, ViewCompat.TYPE_TOUCH);
            } else if (firstVisibleItem.itemView.getMeasuredWidth() == getMeasuredWidth()) {
                super.scrollByInternal(-getMeasuredWidth(), 0, null, ViewCompat.TYPE_TOUCH);
            } else {
                super.scrollByInternal(firstVisibleItem.itemView.getRight() - getMeasuredWidth(), 0, null, ViewCompat.TYPE_TOUCH);
            }
        } else if (layout instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager realLayout = (StaggeredGridLayoutManager) layout;
            int[] firstVisiblePosition = realLayout.findFirstVisibleItemPositions(null);
            ViewHolder[] firstVisibleItems = new ViewHolder[firstVisiblePosition.length];
            for (int i = 0; i < firstVisiblePosition.length; i++) {
                firstVisibleItems[i] = findViewHolderForAdapterPosition(firstVisiblePosition[i]);
            }
            int scrollDistance = Integer.MAX_VALUE;
            for (ViewHolder firstVisibleItem : firstVisibleItems) {
                if (firstVisibleItem != null) {
                    if (firstVisibleItem.itemView.getRight() < scrollDistance) {
                        scrollDistance = firstVisibleItem.itemView.getRight();
                    }
                }
            }

            if (scrollDistance == getMeasuredWidth()) {
                scrollDistance = 0;
            }
            super.scrollByInternal(scrollDistance - getMeasuredWidth(), 0, null, ViewCompat.TYPE_TOUCH);
        }
    }

    protected void onScrollRight() {
        LayoutManager layout = getLayoutManager();
        if (layout instanceof LinearLayoutManager) {
            LinearLayoutManager realLayout = (LinearLayoutManager) layout;
            int lastVisiblePosition = realLayout.findLastVisibleItemPosition();
            ViewHolder lastVisibleItem = findViewHolderForAdapterPosition(lastVisiblePosition);

            if (lastVisibleItem == null) {
                return;
            }

            //when the item width overflow screen, findLastCompletelyVisibleItemPosition always return NO_POSITION
            if (lastVisibleItem.itemView.getMeasuredWidth() > getMeasuredWidth()) {
                super.scrollByInternal(getMeasuredWidth() - defaultOverflowItemScrollKeep, 0, null, ViewCompat.TYPE_TOUCH);
            } else if (lastVisibleItem.itemView.getMeasuredWidth() == getMeasuredWidth()) {
                super.scrollByInternal(getMeasuredWidth(), 0, null, ViewCompat.TYPE_TOUCH);
            } else {
                super.scrollByInternal(lastVisibleItem.itemView.getLeft(), 0, null, ViewCompat.TYPE_TOUCH);
            }
        } else if (layout instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager realLayout = (StaggeredGridLayoutManager) layout;
            int[] lastVisiblePosition = realLayout.findLastVisibleItemPositions(null);
            ViewHolder[] lastVisibleItems = new ViewHolder[lastVisiblePosition.length];
            for (int i = 0; i < lastVisiblePosition.length; i++) {
                lastVisibleItems[i] = findViewHolderForAdapterPosition(lastVisiblePosition[i]);
            }
            int scrollDistance = 0;
            for (ViewHolder lastVisibleItem : lastVisibleItems) {
                if (lastVisibleItem != null) {
                    if (lastVisibleItem.itemView.getLeft() > scrollDistance) {
                        scrollDistance = lastVisibleItem.itemView.getLeft();
                    }
                }
            }
            if (scrollDistance == 0) {
                scrollDistance = getMeasuredWidth();
            }
            super.scrollByInternal(scrollDistance, 0, null, ViewCompat.TYPE_TOUCH);
        }
    }

    protected void onScrollUp() {
        LayoutManager layout = getLayoutManager();
        if (layout instanceof LinearLayoutManager) {
            LinearLayoutManager realLayout = (LinearLayoutManager) layout;
            int firstVisiblePosition = realLayout.findFirstVisibleItemPosition();
            ViewHolder firstVisibleItem = findViewHolderForAdapterPosition(firstVisiblePosition);

            if (firstVisibleItem == null) {
                return;
            }

            //when the item height overscreen, findLastCompletelyVisibleItemPosition always return NO_POSITION
            if (firstVisibleItem.itemView.getMeasuredHeight() > getMeasuredHeight()) {
                super.scrollByInternal(0, -getMeasuredHeight() + defaultOverflowItemScrollKeep, null, ViewCompat.TYPE_TOUCH);
            } else if (firstVisibleItem.itemView.getMeasuredHeight() == getMeasuredHeight()) {
                super.scrollByInternal(0, -getMeasuredHeight(), null, ViewCompat.TYPE_TOUCH);
            } else {
                super.scrollByInternal(0, firstVisibleItem.itemView.getBottom() - getMeasuredHeight(), null, ViewCompat.TYPE_TOUCH);
            }
        } else if (layout instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager realLayout = (StaggeredGridLayoutManager) layout;
            int[] firstVisiblePosition = realLayout.findFirstVisibleItemPositions(null);
            ViewHolder[] firstVisibleItems = new ViewHolder[firstVisiblePosition.length];
            for (int i = 0; i < firstVisiblePosition.length; i++) {
                firstVisibleItems[i] = findViewHolderForAdapterPosition(firstVisiblePosition[i]);
            }
            int scrollDistance = Integer.MAX_VALUE;
            for (ViewHolder firstVisibleItem : firstVisibleItems) {
                if (firstVisibleItem != null) {
                    if (firstVisibleItem.itemView.getBottom() < scrollDistance) {
                        scrollDistance = firstVisibleItem.itemView.getBaseline();
                    }
                }
            }
            if (scrollDistance == getMeasuredWidth()) {
                scrollDistance = 0;
            }
            super.scrollByInternal(0, scrollDistance - getMeasuredHeight(), null, ViewCompat.TYPE_TOUCH);
        }
    }

    protected void onScrollDown() {
        LayoutManager layout = getLayoutManager();
        if (layout instanceof LinearLayoutManager) {
            LinearLayoutManager realLayout = (LinearLayoutManager) layout;
            int lastVisiblePosition = realLayout.findLastVisibleItemPosition();
            ViewHolder lastVisibleItem = findViewHolderForAdapterPosition(lastVisiblePosition);

            if (lastVisibleItem == null) {
                return;
            }

            //when the item height overflow screen, findLastCompletelyVisibleItemPosition always return NO_POSITION
            if (lastVisibleItem.itemView.getMeasuredHeight() > getMeasuredHeight()) {
                super.scrollByInternal(0, getMeasuredHeight() - defaultOverflowItemScrollKeep, null, ViewCompat.TYPE_TOUCH);
            } else if (lastVisibleItem.itemView.getMeasuredHeight() == getMeasuredHeight()) {
                super.scrollByInternal(0, getMeasuredHeight(), null, ViewCompat.TYPE_TOUCH);
            } else {
                super.scrollByInternal(0, lastVisibleItem.itemView.getTop(), null, ViewCompat.TYPE_TOUCH);
            }
        } else if (layout instanceof StaggeredGridLayoutManager) {
            StaggeredGridLayoutManager realLayout = (StaggeredGridLayoutManager) layout;
            int[] lastVisiblePosition = realLayout.findLastVisibleItemPositions(null);
            ViewHolder[] lastVisibleItems = new ViewHolder[lastVisiblePosition.length];
            for (int i = 0; i < lastVisiblePosition.length; i++) {
                lastVisibleItems[i] = findViewHolderForAdapterPosition(lastVisiblePosition[i]);
            }
            int scrollDistance = 0;
            for (ViewHolder lastVisibleItem : lastVisibleItems) {
                if (lastVisibleItem != null) {
                    if (lastVisibleItem.itemView.getTop() > scrollDistance) {
                        scrollDistance = lastVisibleItem.itemView.getTop();
                    }
                }
            }
            if (scrollDistance == 0) {
                scrollDistance = getMeasuredHeight();
            }
            super.scrollByInternal(0, scrollDistance, null, ViewCompat.TYPE_TOUCH);
        }
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        defaultOverflowItemScrollKeep = (int) (h * 0.05f);
    }

    private double getSplineDeceleration(int velocity) {
        return Math.log(0.35f * Math.abs(velocity) / (mFlingFriction * mPhysicalCoeff));
    }

    private double getSplineFlingDistance(int velocity) {
        final double l = getSplineDeceleration(velocity);
        final double decelMinusOne = DECELERATION_RATE - 1.0;
        return mFlingFriction * mPhysicalCoeff * Math.exp(DECELERATION_RATE / decelMinusOne * l);
    }

    public void setEnableScroll(boolean enableScroll) {
        this.enableScroll = enableScroll;
    }

    public boolean isEnableScroll() {
        return enableScroll;
    }
}

public final class LocalRepoManager {
    private static final String TAG = "LocalRepoManager";

    private final Context context;
    private final PackageManager pm;
    private final AssetManager assetManager;
    private final String fdroidPackageName;

    public static final String[] WEB_ROOT_ASSET_FILES = {
            "swap-icon.png",
            "swap-tick-done.png",
            "swap-tick-not-done.png",
    };

    private final List<App> apps = new ArrayList<>();

    private final SanitizedFile indexJar;
    private final SanitizedFile indexJarUnsigned;
    private final SanitizedFile webRoot;
    private final SanitizedFile fdroidDir;
    private final SanitizedFile fdroidDirCaps;
    private final SanitizedFile repoDir;
    private final SanitizedFile repoDirCaps;

    @Nullable
    private static LocalRepoManager localRepoManager;

    @NonNull
    public static LocalRepoManager get(Context context) {
        if (localRepoManager == null) {
            localRepoManager = new LocalRepoManager(context);
        }
        return localRepoManager;
    }

    private LocalRepoManager(Context c) {
        context = c.getApplicationContext();
        pm = c.getPackageManager();
        assetManager = c.getAssets();
        fdroidPackageName = c.getPackageName();

        webRoot = SanitizedFile.knownSanitized(c.getFilesDir());
        /* /fdroid/repo is the standard path for user repos */
        fdroidDir = new SanitizedFile(webRoot, "fdroid");
        fdroidDirCaps = new SanitizedFile(webRoot, "FDROID");
        repoDir = new SanitizedFile(fdroidDir, "repo");
        repoDirCaps = new SanitizedFile(fdroidDirCaps, "REPO");
        indexJar = new SanitizedFile(repoDir, IndexV1UpdaterKt.SIGNED_FILE_NAME);
        indexJarUnsigned = new SanitizedFile(repoDir, "index-v1.unsigned.jar");

        if (!fdroidDir.exists() && !fdroidDir.mkdir()) {
            Log.e(TAG, "Unable to create empty base: " + fdroidDir);
        }

        if (!repoDir.exists() && !repoDir.mkdir()) {
            Log.e(TAG, "Unable to create empty repo: " + repoDir);
        }

        SanitizedFile iconsDir = new SanitizedFile(repoDir, "icons");
        if (!iconsDir.exists() && !iconsDir.mkdir()) {
            Log.e(TAG, "Unable to create icons folder: " + iconsDir);
        }
    }

    private String writeFdroidApkToWebroot() {
        ApplicationInfo appInfo;
        String fdroidClientURL = "https://f-droid.org/F-Droid.apk";

        try {
            appInfo = pm.getApplicationInfo(fdroidPackageName, PackageManager.GET_META_DATA);
            SanitizedFile apkFile = SanitizedFile.knownSanitized(appInfo.publicSourceDir);
            SanitizedFile fdroidApkLink = new SanitizedFile(fdroidDir, "F-Droid.apk");
            attemptToDelete(fdroidApkLink);
            if (Utils.symlinkOrCopyFileQuietly(apkFile, fdroidApkLink)) {
                fdroidClientURL = "/" + fdroidDir.getName() + "/" + fdroidApkLink.getName();
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Could not set up F-Droid apk in the webroot", e);
        }
        return fdroidClientURL;
    }

    void writeIndexPage(String repoAddress) {
        final String fdroidClientURL = writeFdroidApkToWebroot();
        try {
            File indexHtml = new File(webRoot, "index.html");
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(assetManager.open("index.template.html"), "UTF-8"));
            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream(indexHtml)));

            StringBuilder builder = new StringBuilder();
            for (App app : apps) {
                builder.append("<li><a href=\"/fdroid/repo/")
                        .append(app.packageName)
                        .append("_")
                        .append(app.installedApk.versionCode)
                        .append(".apk\">")
                        .append("<img width=\"32\" height=\"32\" src=\"/fdroid/repo/icons/")
                        .append(app.packageName)
                        .append("_")
                        .append(app.installedApk.versionCode)
                        .append(".png\">")
                        .append(app.name)
                        .append("</a></li>\n");
            }

            String line;
            while ((line = in.readLine()) != null) {
                line = line.replaceAll("\\{\\{REPO_URL\\}\\}", repoAddress);
                line = line.replaceAll("\\{\\{CLIENT_URL\\}\\}", fdroidClientURL);
                line = line.replaceAll("\\{\\{APP_LIST\\}\\}", builder.toString());
                out.write(line);
            }
            in.close();
            out.close();

            for (final String file : WEB_ROOT_ASSET_FILES) {
                InputStream assetIn = assetManager.open(file);
                OutputStream assetOut = new FileOutputStream(new File(webRoot, file));
                Utils.copy(assetIn, assetOut);
                assetIn.close();
                assetOut.close();
            }

            // make symlinks/copies in each subdir of the repo to make sure that
            // the user will always find the bootstrap page.
            symlinkEntireWebRootElsewhere("../", fdroidDir);
            symlinkEntireWebRootElsewhere("../../", repoDir);

            // add in /FDROID/REPO to support bad QR Scanner apps
            attemptToMkdir(fdroidDirCaps);
            attemptToMkdir(repoDirCaps);

            symlinkEntireWebRootElsewhere("../", fdroidDirCaps);
            symlinkEntireWebRootElsewhere("../../", repoDirCaps);

        } catch (IOException e) {
            Log.e(TAG, "Error writing local repo index", e);
        }
    }

    private static void attemptToMkdir(@NonNull File dir) throws IOException {
        if (dir.exists()) {
            if (dir.isDirectory()) {
                return;
            }
            throw new IOException("Can't make directory " + dir + " - it is already a file.");
        }

        if (!dir.mkdir()) {
            throw new IOException("An error occurred trying to create directory " + dir);
        }
    }

    private static void attemptToDelete(@NonNull File file) {
        if (!file.delete()) {
            Log.i(TAG, "Could not delete \"" + file.getAbsolutePath() + "\".");
        }
    }

    private void symlinkEntireWebRootElsewhere(String symlinkPrefix, File directory) {
        symlinkFileElsewhere("index.html", symlinkPrefix, directory);
        for (final String fileName : WEB_ROOT_ASSET_FILES) {
            symlinkFileElsewhere(fileName, symlinkPrefix, directory);
        }
    }

    private void symlinkFileElsewhere(String fileName, String symlinkPrefix, File directory) {
        SanitizedFile index = new SanitizedFile(directory, fileName);
        attemptToDelete(index);
        Utils.symlinkOrCopyFileQuietly(new SanitizedFile(new File(directory, symlinkPrefix), fileName), index);
    }

    private void deleteContents(File path) {
        if (path.exists()) {
            for (File file : path.listFiles()) {
                if (file.isDirectory()) {
                    deleteContents(file);
                } else {
                    attemptToDelete(file);
                }
            }
        }
    }

    /**
     * Get the {@code index-v1.jar} file that represents the local swap repo.
     */
    public File getIndexJar() {
        return indexJar;
    }

    public File getWebRoot() {
        return webRoot;
    }

    public void deleteRepo() {
        deleteContents(repoDir);
    }

    void generateIndex(String repoUri, String address, String[] selectedApps) throws IOException {
        String name = Preferences.get().getLocalRepoName() + " on " + FDroidApp.ipAddressString;
        String description =
                "A local FDroid repo generated from apps installed on " + Preferences.get().getLocalRepoName();
        RepoV1 repo = new RepoV1(System.currentTimeMillis(), 20001, 7, name, "swap-icon.png",
                address, description, Collections.emptyList());
        Set<String> apps = new HashSet<>(Arrays.asList(selectedApps));
        IndexV1Creator creator = new IndexV1Creator(context.getPackageManager(), repoDir, apps, repo);
        IndexV1 indexV1 = creator.createRepo();
        cacheApps(indexV1);
        writeIndexPage(repoUri);
        SanitizedFile indexJson = new SanitizedFile(repoDir, IndexV1VerifierKt.DATA_FILE_NAME);
        writeIndexJar(indexJson);
    }

    private void cacheApps(IndexV1 indexV1) {
        this.apps.clear();
        for (AppV1 a : indexV1.getApps()) {
            App app = new App();
            app.packageName = a.getPackageName();
            app.name = a.getName();
            app.installedApk = new Apk();
            List<PackageV1> packages = indexV1.getPackages().get(a.getPackageName());
            if (packages != null && packages.size() > 0) {
                Long versionCode = packages.get(0).getVersionCode();
                if (versionCode != null) app.installedApk.versionCode = versionCode;
            }
            this.apps.add(app);
        }
    }

    private void writeIndexJar(SanitizedFile indexJson) throws IOException {
        BufferedOutputStream bo = new BufferedOutputStream(new FileOutputStream(indexJarUnsigned));
        JarOutputStream jo = new JarOutputStream(bo);
        JarEntry je = new JarEntry(indexJson.getName());
        jo.putNextEntry(je);
        FileUtils.copyFile(indexJson, jo);
        jo.close();
        bo.close();

        try {
            LocalRepoKeyStore.get(context).signZip(indexJarUnsigned, indexJar);
        } catch (LocalRepoKeyStore.InitException e) {
            throw new IOException("Could not sign index - keystore failed to initialize");
        } finally {
            attemptToDelete(indexJarUnsigned);
        }
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package strings;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ModifyAfterValidation {

    public String modifyDangerReplaceAll(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        s = s.replaceAll("[\\p{Cn}]", ""); // modified after validation
        return s;
    }

    public String modifyDangerConcat(String str, String stringVal) {
        Boolean isTrue = false;
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        if(isTrue){
            someMethod(s.concat(stringVal));
        }
        return s;
    }

    public String modifyDangerReplace(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        s = s.replace("[\\p{Cn}]", ""); // modified after validation
        return s;
    }

    public String modifyDangerReplaceFirst(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

        // ruleid: java_strings_rule-ModifyAfterValidation
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        s = s.replaceFirst("[\\p{Cn}]", ""); // modified after validation
        return s;
    }

    public String modifySafeReplaceAll(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.replaceAll("[\\p{Cn}]", "");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }

    public String modifySafeReplace(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.replace("[\\p{Cn}]", "");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }

    public String modifySafeReplaceFirst(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.replaceFirst("[\\p{Cn}]", "");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }

    public String modifySafeConcat(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.concat("test");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);
        
        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }


    public String someMethod(String input){
        return input;
    }
}

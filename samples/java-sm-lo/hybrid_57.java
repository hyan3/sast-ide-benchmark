public abstract class StageActivity extends EhActivity {

    public static final String ACTION_START_SCENE = "start_scene";
    public static final String KEY_SCENE_NAME = "stage_activity_scene_name";
    public static final String KEY_SCENE_ARGS = "stage_activity_scene_args";
    private static final String TAG = StageActivity.class.getSimpleName();
    private static final String KEY_STAGE_ID = "stage_activity_stage_id";
    private static final String KEY_SCENE_TAG_LIST = "stage_activity_scene_tag_list";
    private static final String KEY_NEXT_ID = "stage_activity_next_id";
    private static final Map<Class<?>, Integer> sLaunchModeMap = new HashMap<>();
    private final ArrayList<String> mSceneTagList = new ArrayList<>();
    private final ArrayList<String> mDelaySceneTagList = new ArrayList<>();
    private final AtomicInteger mIdGenerator = new AtomicInteger();
    private final SceneViewComparator mSceneViewComparator = new SceneViewComparator();
    private int mStageId = IntIdGenerator.INVALID_ID;

    public static void registerLaunchMode(Class<?> clazz, @SceneFragment.LaunchMode int launchMode) {
        if (launchMode != SceneFragment.LAUNCH_MODE_STANDARD &&
                launchMode != SceneFragment.LAUNCH_MODE_SINGLE_TOP &&
                launchMode != SceneFragment.LAUNCH_MODE_SINGLE_TASK) {
            throw new IllegalStateException("Invalid launch mode: " + launchMode);
        }
        sLaunchModeMap.put(clazz, launchMode);
    }

    public abstract int getContainerViewId();

    /**
     * @return {@code true} for start scene
     */
    private boolean startSceneFromIntent(Intent intent) {
        String clazzStr = intent.getStringExtra(KEY_SCENE_NAME);
        if (null == clazzStr) {
            return false;
        }

        Class clazz;
        try {
            clazz = Class.forName(clazzStr);
        } catch (ClassNotFoundException e) {
            Log.e(TAG, "Can't find class " + clazzStr, e);
            return false;
        }

        Bundle args = intent.getBundleExtra(KEY_SCENE_ARGS);

        Announcer announcer = onStartSceneFromIntent(clazz, args);
        if (announcer == null) {
            return false;
        }

        startScene(announcer);
        return true;
    }

    /**
     * Start scene from {@code Intent}, it might be not safe,
     * Correct it here.
     *
     * @return {@code null} for do not start scene
     */
    @Nullable
    protected Announcer onStartSceneFromIntent(@NonNull Class<?> clazz, @Nullable Bundle args) {
        return new Announcer(clazz).setArgs(args);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (intent == null || !ACTION_START_SCENE.equals(intent.getAction()) ||
                !startSceneFromIntent(intent)) {
            onUnrecognizedIntent(intent);
        }
    }

    /**
     * Called when launch with action {@code android.intent.action.MAIN}
     */
    @Nullable
    protected abstract Announcer getLaunchAnnouncer();

    /**
     * Can't recognize intent in first time {@code onCreate} and {@code onNewIntent},
     * null included.
     */
    protected void onUnrecognizedIntent(@Nullable Intent intent) {
    }

    /**
     * Call {@code setContentView} here. Do <b>NOT</b> call {@code startScene} here
     */
    protected abstract void onCreate2(@Nullable Bundle savedInstanceState);

    @Override
    protected final void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            mStageId = savedInstanceState.getInt(KEY_STAGE_ID, IntIdGenerator.INVALID_ID);
            ArrayList<String> list = savedInstanceState.getStringArrayList(KEY_SCENE_TAG_LIST);
            if (list != null) {
                mSceneTagList.addAll(list);
                mDelaySceneTagList.addAll(list);
            }
            mIdGenerator.lazySet(savedInstanceState.getInt(KEY_NEXT_ID));
        }

        if (mStageId == IntIdGenerator.INVALID_ID) {
            ((SceneApplication) getApplicationContext()).registerStageActivity(this);
        } else {
            ((SceneApplication) getApplicationContext()).registerStageActivity(this, mStageId);
        }

        // Create layout
        onCreate2(savedInstanceState);

        Intent intent = getIntent();
        if (savedInstanceState == null) {
            if (intent != null) {
                String action = intent.getAction();
                if (Intent.ACTION_MAIN.equals(action)) {
                    Announcer announcer = getLaunchAnnouncer();
                    if (announcer != null) {
                        startScene(announcer);
                        return;
                    }
                } else if (ACTION_START_SCENE.equals(action)) {
                    if (startSceneFromIntent(intent)) {
                        return;
                    }
                }
            }

            // Can't recognize intent
            onUnrecognizedIntent(intent);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_STAGE_ID, mStageId);
        outState.putStringArrayList(KEY_SCENE_TAG_LIST, mSceneTagList);
        outState.putInt(KEY_NEXT_ID, mIdGenerator.getAndIncrement());
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ((SceneApplication) getApplicationContext()).unregisterStageActivity(mStageId);
    }

    public void onSceneViewCreated(SceneFragment scene, Bundle savedInstanceState) {
    }

    public void onSceneViewDestroyed(SceneFragment scene) {
    }

    void onSceneDestroyed(SceneFragment scene) {
        mDelaySceneTagList.remove(scene.getTag());
    }

    protected void onRegister(int id) {
        mStageId = id;
    }

    protected void onUnregister() {
    }

    protected void onTransactScene() {
    }

    public int getStageId() {
        return mStageId;
    }

    public int getSceneCount() {
        return mSceneTagList.size();
    }

    public int getSceneLaunchMode(Class<?> clazz) {
        Integer integer = sLaunchModeMap.get(clazz);
        if (integer == null) {
            throw new RuntimeException("Not register " + clazz.getName());
        } else {
            return integer;
        }
    }

    private SceneFragment newSceneInstance(Class<?> clazz) {
        try {
            return (SceneFragment) clazz.newInstance();
        } catch (InstantiationException e) {
            throw new IllegalStateException("Can't instance " + clazz.getName(), e);
        } catch (IllegalAccessException e) {
            throw new IllegalStateException("The constructor of " +
                    clazz.getName() + " is not visible", e);
        } catch (ClassCastException e) {
            throw new IllegalStateException(clazz.getName() + " can not cast to scene", e);
        }
    }

    public void startScene(Announcer announcer) {
        startScene(announcer, false);
    }

    public void startScene(Announcer announcer, boolean horizontal) {
        Class<?> clazz = announcer.clazz;
        Bundle args = announcer.args;
        TransitionHelper tranHelper = announcer.tranHelper;
        FragmentManager fragmentManager = getSupportFragmentManager();
        int launchMode = getSceneLaunchMode(clazz);

        // Check LAUNCH_MODE_SINGLE_TASK
        if (launchMode == SceneFragment.LAUNCH_MODE_SINGLE_TASK) {
            for (int i = 0, n = mSceneTagList.size(); i < n; i++) {
                String tag = mSceneTagList.get(i);
                Fragment fragment = fragmentManager.findFragmentByTag(tag);
                if (fragment == null) {
                    Log.e(TAG, "Can't find fragment with tag: " + tag);
                    continue;
                }

                if (clazz.isInstance(fragment)) { // Get it
                    FragmentTransaction transaction = fragmentManager.beginTransaction();

                    // Use default animation
                    if (horizontal) {
                        transaction.setCustomAnimations(R.anim.scene_open_enter_horizontal, R.anim.scene_open_exit);
                    } else {
                        transaction.setCustomAnimations(R.anim.scene_open_enter, R.anim.scene_open_exit);
                    }

                    // Remove top fragments
                    for (int j = i + 1; j < n; j++) {
                        String topTag = mSceneTagList.get(j);
                        Fragment topFragment = fragmentManager.findFragmentByTag(topTag);
                        if (null == topFragment) {
                            Log.e(TAG, "Can't find fragment with tag: " + topTag);
                            continue;
                        }
                        // Clear shared element
                        topFragment.setSharedElementEnterTransition(null);
                        topFragment.setSharedElementReturnTransition(null);
                        topFragment.setEnterTransition(null);
                        topFragment.setExitTransition(null);
                        // Remove it
                        transaction.remove(topFragment);
                    }

                    // Remove tag from index i+1
                    mSceneTagList.subList(i + 1, mSceneTagList.size()).clear();
                    mDelaySceneTagList.subList(i + 1, mDelaySceneTagList.size()).clear();

                    // Attach fragment
                    if (fragment.isDetached()) {
                        transaction.attach(fragment);
                    }

                    // Commit
                    transaction.commitAllowingStateLoss();
                    onTransactScene();

                    // New arguments
                    if (args != null && fragment instanceof SceneFragment) {
                        // TODO Call onNewArguments when view created ?
                        ((SceneFragment) fragment).onNewArguments(args);
                    }

                    return;
                }
            }
        }

        // Get current fragment
        SceneFragment currentScene = null;
        if (mSceneTagList.size() > 0) {
            // Get last tag
            String tag = mSceneTagList.get(mSceneTagList.size() - 1);
            Fragment fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment != null) {
                AssertUtils.assertTrue(fragment instanceof SceneFragment);
                currentScene = (SceneFragment) fragment;
            }
        }

        // Check LAUNCH_MODE_SINGLE_TASK
        if (clazz.isInstance(currentScene) && launchMode == SceneFragment.LAUNCH_MODE_SINGLE_TOP) {
            if (args != null) {
                currentScene.onNewArguments(args);
            }
            return;
        }

        // Create new scene
        SceneFragment newScene = newSceneInstance(clazz);
        newScene.setArguments(args);

        // Create new scene tag
        String newTag = Integer.toString(mIdGenerator.getAndIncrement());

        // Add new tag to list
        mSceneTagList.add(newTag);
        mDelaySceneTagList.add(newTag);

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        // Animation
        if (currentScene != null) {
            if (tranHelper == null || !tranHelper.onTransition(
                    this, transaction, currentScene, newScene)) {
                // Clear shared item
                currentScene.setSharedElementEnterTransition(null);
                currentScene.setSharedElementReturnTransition(null);
                currentScene.setEnterTransition(null);
                currentScene.setExitTransition(null);
                newScene.setSharedElementEnterTransition(null);
                newScene.setSharedElementReturnTransition(null);
                newScene.setEnterTransition(null);
                newScene.setExitTransition(null);
                // Set default animation
                if (horizontal) {
                    transaction.setCustomAnimations(R.anim.scene_open_enter_horizontal, R.anim.scene_open_exit);
                } else {
                    transaction.setCustomAnimations(R.anim.scene_open_enter, R.anim.scene_open_exit);
                }
            }
            // Detach current scene
            if (!currentScene.isDetached()) {
                transaction.detach(currentScene);
            } else {
                Log.e(TAG, "Current scene is detached");
            }
        }

        // Add new scene
        transaction.add(getContainerViewId(), newScene, newTag);

        // Commit
        transaction.commitAllowingStateLoss();
        onTransactScene();

        // Check request
        if (announcer.requestFrom != null) {
            newScene.addRequest(announcer.requestFrom.getTag(), announcer.requestCode);
        }
    }

    public void startSceneFirstly(Announcer announcer) {
        Class<?> clazz = announcer.clazz;
        Bundle args = announcer.args;
        FragmentManager fragmentManager = getSupportFragmentManager();
        int launchMode = getSceneLaunchMode(clazz);
        boolean forceNewScene = launchMode == SceneFragment.LAUNCH_MODE_STANDARD;
        boolean createNewScene = true;
        boolean findScene = false;
        SceneFragment scene = null;

        FragmentTransaction transaction = fragmentManager.beginTransaction();

        // Set default animation
        transaction.setCustomAnimations(R.anim.scene_open_enter, R.anim.scene_open_exit);

        String findSceneTag = null;
        for (int i = 0, n = mSceneTagList.size(); i < n; i++) {
            String tag = mSceneTagList.get(i);
            Fragment fragment = fragmentManager.findFragmentByTag(tag);
            if (fragment == null) {
                Log.e(TAG, "Can't find fragment with tag: " + tag);
                continue;
            }

            // Clear shared element
            fragment.setSharedElementEnterTransition(null);
            fragment.setSharedElementReturnTransition(null);
            fragment.setEnterTransition(null);
            fragment.setExitTransition(null);

            // Check is target scene
            if (!forceNewScene && !findScene && clazz.isInstance(fragment) &&
                    (launchMode == SceneFragment.LAUNCH_MODE_SINGLE_TASK || !fragment.isDetached())) {
                scene = (SceneFragment) fragment;
                findScene = true;
                createNewScene = false;
                findSceneTag = tag;
                if (fragment.isDetached()) {
                    transaction.attach(fragment);
                }
            } else {
                // Remove it
                transaction.remove(fragment);
            }
        }

        // Handle tag list
        mSceneTagList.clear();
        if (null != findSceneTag) {
            mSceneTagList.add(findSceneTag);
        }

        if (createNewScene) {
            scene = newSceneInstance(clazz);
            scene.setArguments(args);

            // Create scene tag
            String tag = Integer.toString(mIdGenerator.getAndIncrement());

            // Add tag to list
            mSceneTagList.add(tag);
            mDelaySceneTagList.add(tag);

            // Add scene
            transaction.add(getContainerViewId(), scene, tag);
        }

        // Commit
        transaction.commitAllowingStateLoss();
        onTransactScene();

        if (!createNewScene && args != null) {
            // TODO Call onNewArguments when view created ?
            scene.onNewArguments(args);
        }
    }

    int getSceneIndex(SceneFragment scene) {
        return getTagIndex(scene.getTag());
    }

    int getTagIndex(String tag) {
        return mSceneTagList.indexOf(tag);
    }

    void sortSceneViews(List<View> views) {
        Collections.sort(views, mSceneViewComparator);
    }

    public void finishScene(SceneFragment scene) {
        finishScene(scene, null);
    }

    public void finishScene(SceneFragment scene, TransitionHelper transitionHelper) {
        finishScene(scene.getTag(), transitionHelper);
    }

    private void finishScene(String tag, TransitionHelper transitionHelper) {
        FragmentManager fragmentManager = getSupportFragmentManager();

        // Get scene
        Fragment scene = fragmentManager.findFragmentByTag(tag);
        if (scene == null) {
            Log.e(TAG, "finishScene: Can't find scene by tag: " + tag);
            return;
        }

        // Get scene index
        int index = mSceneTagList.indexOf(tag);
        if (index < 0) {
            Log.e(TAG, "finishScene: Can't find the tag in tag list: " + tag);
            return;
        }

        if (mSceneTagList.size() == 1) {
            // It is the last fragment, finish Activity now
            Log.i(TAG, "finishScene: It is the last scene, finish activity now");
            finish();
            return;
        }

        Fragment next = null;
        if (index == mSceneTagList.size() - 1) {
            // It is first fragment, show the next one
            next = fragmentManager.findFragmentByTag(mSceneTagList.get(index - 1));
        }

        FragmentTransaction transaction = fragmentManager.beginTransaction();
        if (next != null) {
            if (transitionHelper == null || !transitionHelper.onTransition(
                    this, transaction, scene, next)) {
                // Clear shared item
                scene.setSharedElementEnterTransition(null);
                scene.setSharedElementReturnTransition(null);
                scene.setEnterTransition(null);
                scene.setExitTransition(null);
                next.setSharedElementEnterTransition(null);
                next.setSharedElementReturnTransition(null);
                next.setEnterTransition(null);
                next.setExitTransition(null);
                // Do not show animate if it is not the first fragment
                transaction.setCustomAnimations(R.anim.scene_close_enter, R.anim.scene_close_exit);
            }
            // Attach fragment
            transaction.attach(next);
        }
        transaction.remove(scene);
        transaction.commitAllowingStateLoss();
        onTransactScene();

        // Remove tag
        mSceneTagList.remove(index);

        // Return result
        if (scene instanceof SceneFragment) {
            ((SceneFragment) scene).returnResult(this);
        }
    }

    public void refreshTopScene() {
        int index = mSceneTagList.size() - 1;
        if (index < 0) {
            return;
        }
        String tag = mSceneTagList.get(index);

        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);

        if (fragment == null) {
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            fragmentManager.beginTransaction().detach(fragment).commitAllowingStateLoss();
            fragmentManager.beginTransaction().attach(fragment).commitAllowingStateLoss();
        } else {
            fragmentManager.beginTransaction().detach(fragment).attach(fragment).commitAllowingStateLoss();
        }
    }

    @Override
    public void onBackPressed() {
        int size = mSceneTagList.size();
        String tag = mSceneTagList.get(size - 1);
        SceneFragment scene;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            Log.e(TAG, "onBackPressed: Can't find scene by tag: " + tag);
            return;
        }
        if (!(fragment instanceof SceneFragment)) {
            Log.e(TAG, "onBackPressed: The fragment is not SceneFragment");
            return;
        }

        scene = (SceneFragment) fragment;
        scene.onBackPressed();
    }

    @Override
    public void onProvideAssistContent(AssistContent outContent) {
        super.onProvideAssistContent(outContent);
        int size = mSceneTagList.size();
        String tag = mSceneTagList.get(size - 1);
        SceneFragment scene;
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (fragment == null) {
            Log.e(TAG, "onProvideAssistContent: Can't find scene by tag: " + tag);
            return;
        }
        if (!(fragment instanceof SceneFragment)) {
            Log.e(TAG, "onProvideAssistContent: The fragment is not SceneFragment");
            return;
        }

        scene = (SceneFragment) fragment;
        scene.onProvideAssistContent(outContent);
    }

    public SceneFragment findSceneByTag(String tag) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = fragmentManager.findFragmentByTag(tag);
        if (fragment != null) {
            return (SceneFragment) fragment;
        } else {
            return null;
        }
    }

    @Nullable
    public Class<?> getTopSceneClass() {
        int index = mSceneTagList.size() - 1;
        if (index < 0) {
            return null;
        }
        String tag = mSceneTagList.get(index);
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if (null == fragment) {
            return null;
        }
        return fragment.getClass();
    }

    private final class SceneViewComparator implements Comparator<View> {

        private int getIndex(View view) {
            Object o = view.getTag(R.id.fragment_tag);
            if (o instanceof String) {
                return mDelaySceneTagList.indexOf(o);
            } else {
                return -1;
            }
        }

        @Override
        public int compare(View lhs, View rhs) {
            return getIndex(lhs) - getIndex(rhs);
        }
    }
}

public class NearbyViewBinder {
    public static final String TAG = "NearbyViewBinder";

    private static File externalStorage = null;
    private static View swapView;

    NearbyViewBinder(final AppCompatActivity activity, FrameLayout parent) {
        swapView = activity.getLayoutInflater().inflate(R.layout.main_tab_nearby, parent, true);

        TextView subtext = swapView.findViewById(R.id.both_parties_need_fdroid_text);
        subtext.setText(activity.getString(R.string.nearby_splash__both_parties_need_fdroid,
                activity.getString(R.string.app_name)));

        Button startButton = swapView.findViewById(R.id.find_people_button);
        startButton.setOnClickListener(v -> {
            final String coarseLocation = Manifest.permission.ACCESS_COARSE_LOCATION;
            if (PackageManager.PERMISSION_GRANTED != ContextCompat.checkSelfPermission(activity, coarseLocation)) {
                ActivityCompat.requestPermissions(activity, new String[]{coarseLocation},
                        MainActivity.REQUEST_LOCATION_PERMISSIONS);
            } else {
                ContextCompat.startForegroundService(activity, new Intent(activity, SwapService.class));
            }
        });

        updateExternalStorageViews(activity);
        updateUsbOtg(activity);
    }

    public static void updateExternalStorageViews(final AppCompatActivity activity) {
        if (swapView == null || activity == null) {
            return;
        }

        ImageView nearbySplash = swapView.findViewById(R.id.image);
        TextView explainer = swapView.findViewById(R.id.read_external_storage_text);
        Button button = swapView.findViewById(R.id.request_read_external_storage_button);

        if (nearbySplash == null || explainer == null || button == null) {
            return;
        }

        File[] dirs = activity.getExternalFilesDirs("");
        if (dirs != null) {
            for (File dir : dirs) {
                if (dir != null && Environment.isExternalStorageRemovable(dir)) {
                    String state = Environment.getExternalStorageState(dir);
                    if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)
                            || Environment.MEDIA_MOUNTED.equals(state)) {
                        // remove Android/data/org.fdroid.fdroid/files to get root
                        externalStorage = dir.getParentFile().getParentFile().getParentFile().getParentFile();
                        break;
                    }
                }
            }
        }

        final String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;

        if (externalStorage != null) {
            nearbySplash.setVisibility(View.GONE);
            explainer.setVisibility(View.VISIBLE);
            button.setVisibility(View.VISIBLE);
            if (Build.VERSION.SDK_INT >= 30) {
                if (!Environment.isExternalStorageManager()) {
                    // we don't have permission to access files yet, so ask for it
                    explainer.setText(R.string.nearby_splach__external_storage_permission_explainer);
                    button.setText(R.string.nearby_splace__external_storage_permission_button);
                    button.setOnClickListener(view -> activity.startActivity(
                            new Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION,
                                    Uri.parse(String.format("package:%s",
                                            activity.getPackageName())))));
                } else {
                    explainer.setText(R.string.nearby_splash__read_external_storage);
                    button.setText(R.string.nearby_splash__request_permission);
                    button.setOnClickListener(view -> scanExternalStorageNow(activity));
                }
            } else {
                if ((externalStorage == null || !externalStorage.canRead())
                        && PackageManager.PERMISSION_GRANTED
                        != ContextCompat.checkSelfPermission(activity, readExternalStorage)) {
                    explainer.setText(R.string.nearby_splach__external_storage_permission_explainer);
                    button.setText(R.string.nearby_splace__external_storage_permission_button);
                    button.setOnClickListener(v -> {
                        ActivityCompat.requestPermissions(activity, new String[]{readExternalStorage},
                                MainActivity.REQUEST_STORAGE_PERMISSIONS);
                    });
                } else {
                    explainer.setText(R.string.nearby_splash__read_external_storage);
                    button.setText(R.string.nearby_splash__request_permission);
                    button.setOnClickListener(view -> scanExternalStorageNow(activity));
                }
            }
        }
    }

    private static void scanExternalStorageNow(final AppCompatActivity activity) {
        Toast.makeText(activity, activity.getString(R.string.scan_removable_storage_toast, externalStorage),
                Toast.LENGTH_SHORT).show();
        SDCardScannerService.scan(activity);
    }

    public static void updateUsbOtg(final Context context) {
        if (Build.VERSION.SDK_INT < 24) {
            return;
        }
        if (swapView == null) {
            Utils.debugLog(TAG, "swapView == null");
            return;
        }
        TextView storageVolumeText = swapView.findViewById(R.id.storage_volume_text);
        Button requestStorageVolume = swapView.findViewById(R.id.request_storage_volume_button);
        storageVolumeText.setVisibility(View.GONE);
        requestStorageVolume.setVisibility(View.GONE);

        final StorageManager storageManager = ContextCompat.getSystemService(context, StorageManager.class);
        for (final StorageVolume storageVolume : storageManager.getStorageVolumes()) {
            if (storageVolume.isRemovable() && !storageVolume.isPrimary()) {
                Log.i(TAG, "StorageVolume: " + storageVolume);
                Intent tmpIntent = null;
                if (Build.VERSION.SDK_INT < 29) {
                    tmpIntent = storageVolume.createAccessIntent(null);
                } else {
                    tmpIntent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
                    tmpIntent.putExtra(DocumentsContract.EXTRA_INITIAL_URI,
                            Uri.parse("content://"
                                    + TreeUriScannerIntentService.EXTERNAL_STORAGE_PROVIDER_AUTHORITY
                                    + "/tree/"
                                    + storageVolume.getUuid()
                                    + "%3A/document/"
                                    + storageVolume.getUuid()
                                    + "%3A"));
                }
                if (tmpIntent == null) {
                    Utils.debugLog(TAG, "Got null Storage Volume access Intent");
                    return;
                }
                final Intent intent = tmpIntent;

                storageVolumeText.setVisibility(View.VISIBLE);

                String text = storageVolume.getDescription(context);
                if (!TextUtils.isEmpty(text)) {
                    requestStorageVolume.setText(text);
                    UsbDevice usb = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (usb != null) {
                        text = String.format("%s (%s %s)", text, usb.getManufacturerName(), usb.getProductName());
                        Toast.makeText(context, text, Toast.LENGTH_LONG).show();
                    }
                }

                requestStorageVolume.setVisibility(View.VISIBLE);
                requestStorageVolume.setOnClickListener(v -> {
                    List<UriPermission> list = context.getContentResolver().getPersistedUriPermissions();
                    if (list != null) {
                        for (UriPermission uriPermission : list) {
                            Uri uri = uriPermission.getUri();
                            if (uri.getPath().equals(String.format("/tree/%s:", storageVolume.getUuid()))) {
                                intent.setData(uri);
                                TreeUriScannerIntentService.onActivityResult(context, intent);
                                return;
                            }
                        }
                    }

                    AppCompatActivity activity = null;
                    if (context instanceof AppCompatActivity) {
                        activity = (AppCompatActivity) context;
                    } else if (swapView != null && swapView.getContext() instanceof AppCompatActivity) {
                        activity = (AppCompatActivity) swapView.getContext();
                    }

                    if (activity != null) {
                        activity.startActivityForResult(intent, MainActivity.REQUEST_STORAGE_ACCESS);
                    } else {
                        // scan in the background without requesting permissions
                        Toast.makeText(context.getApplicationContext(),
                                context.getString(R.string.scan_removable_storage_toast, externalStorage),
                                Toast.LENGTH_SHORT).show();
                        SDCardScannerService.scan(context);
                    }
                });
            }
        }
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid:java_inject_rule-DangerousGroovyShell
        shell.evaluate(new InputStreamReader(new FileInputStream(file)), "script1.groovy");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

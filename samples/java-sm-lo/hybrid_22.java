public class EhDaoGenerator {

    private static final String PACKAGE = "com.hippo.ehviewer.dao";
    private static final String OUT_DIR = "../app/src/main/java-gen";
    private static final String DELETE_DIR = "../app/src/main/java-gen/com/hippo/ehviewer/dao";

    private static final int VERSION = 4;

    private static final String DOWNLOAD_INFO_PATH = "../app/src/main/java-gen/com/hippo/ehviewer/dao/DownloadInfo.java";
    private static final String HISTORY_INFO_PATH = "../app/src/main/java-gen/com/hippo/ehviewer/dao/HistoryInfo.java";
    private static final String QUICK_SEARCH_PATH = "../app/src/main/java-gen/com/hippo/ehviewer/dao/QuickSearch.java";
    private static final String LOCAL_FAVORITE_INFO_PATH = "../app/src/main/java-gen/com/hippo/ehviewer/dao/LocalFavoriteInfo.java";
    private static final String BOOKMARK_INFO_PATH = "../app/src/main/java-gen/com/hippo/ehviewer/dao/BookmarkInfo.java";
    private static final String FILTER_PATH = "../app/src/main/java-gen/com/hippo/ehviewer/dao/Filter.java";

    public static void generate() throws Exception {
        Utilities.deleteContents(new File(DELETE_DIR));
        File outDir = new File(OUT_DIR);
        outDir.delete();
        outDir.mkdirs();

        Schema schema = new Schema(VERSION, PACKAGE);
        addDownloads(schema);
        addDownloadLabel(schema);
        addDownloadDirname(schema);
        addHistoryInfo(schema);
        addQuickSearch(schema);
        addLocalFavorites(schema);
        addBookmarks(schema);
        addFilter(schema);
        new DaoGenerator().generateAll(schema, OUT_DIR);

        adjustDownloadInfo();
        adjustHistoryInfo();
        adjustQuickSearch();
        adjustLocalFavoriteInfo();
        adjustBookmarkInfo();
        adjustFilter();
    }

    private static void addDownloads(Schema schema) {
        Entity entity = schema.addEntity("DownloadInfo");
        entity.setDbName("DOWNLOADS");
        entity.setClassNameDao("DownloadsDao");
        entity.setSuperclass("GalleryInfo");
        // GalleryInfo data
        entity.addLongProperty("gid").primaryKey().notNull();
        entity.addStringProperty("token");
        entity.addStringProperty("title");
        entity.addStringProperty("titleJpn");
        entity.addStringProperty("thumb");
        entity.addIntProperty("category").notNull();
        entity.addStringProperty("posted");
        entity.addStringProperty("uploader");
        entity.addFloatProperty("rating").notNull();
        entity.addStringProperty("simpleLanguage");
        // DownloadInfo data
        entity.addIntProperty("state").notNull();
        entity.addIntProperty("legacy").notNull();
        entity.addLongProperty("time").notNull();
        entity.addStringProperty("label");
    }

    private static void addDownloadLabel(Schema schema) {
        Entity entity = schema.addEntity("DownloadLabel");
        entity.setDbName("DOWNLOAD_LABELS");
        entity.setClassNameDao("DownloadLabelDao");
        entity.addIdProperty();
        entity.addStringProperty("label");
        entity.addLongProperty("time").notNull();
    }

    private static void addDownloadDirname(Schema schema) {
        Entity entity = schema.addEntity("DownloadDirname");
        entity.setDbName("DOWNLOAD_DIRNAME");
        entity.setClassNameDao("DownloadDirnameDao");
        entity.addLongProperty("gid").primaryKey().notNull();
        entity.addStringProperty("dirname");
    }

    private static void addHistoryInfo(Schema schema) {
        Entity entity = schema.addEntity("HistoryInfo");
        entity.setDbName("HISTORY");
        entity.setClassNameDao("HistoryDao");
        entity.setSuperclass("GalleryInfo");
        // GalleryInfo data
        entity.addLongProperty("gid").primaryKey().notNull();
        entity.addStringProperty("token");
        entity.addStringProperty("title");
        entity.addStringProperty("titleJpn");
        entity.addStringProperty("thumb");
        entity.addIntProperty("category").notNull();
        entity.addStringProperty("posted");
        entity.addStringProperty("uploader");
        entity.addFloatProperty("rating").notNull();
        entity.addStringProperty("simpleLanguage");
        // HistoryInfo data
        entity.addIntProperty("mode").notNull();
        entity.addLongProperty("time").notNull();
    }

    private static void addQuickSearch(Schema schema) {
        Entity entity = schema.addEntity("QuickSearch");
        entity.setDbName("QUICK_SEARCH");
        entity.setClassNameDao("QuickSearchDao");
        entity.addIdProperty();
        entity.addStringProperty("name");
        entity.addIntProperty("mode").notNull();
        entity.addIntProperty("category").notNull();
        entity.addStringProperty("keyword");
        entity.addIntProperty("advanceSearch").notNull();
        entity.addIntProperty("minRating").notNull();
        // Since 4
        entity.addIntProperty("pageFrom").notNull();
        // Since 4
        entity.addIntProperty("pageTo").notNull();
        entity.addLongProperty("time").notNull();
    }

    private static void addLocalFavorites(Schema schema) {
        Entity entity = schema.addEntity("LocalFavoriteInfo");
        entity.setDbName("LOCAL_FAVORITES");
        entity.setClassNameDao("LocalFavoritesDao");
        entity.setSuperclass("GalleryInfo");
        // GalleryInfo data
        entity.addLongProperty("gid").primaryKey().notNull();
        entity.addStringProperty("token");
        entity.addStringProperty("title");
        entity.addStringProperty("titleJpn");
        entity.addStringProperty("thumb");
        entity.addIntProperty("category").notNull();
        entity.addStringProperty("posted");
        entity.addStringProperty("uploader");
        entity.addFloatProperty("rating").notNull();
        entity.addStringProperty("simpleLanguage");
        // LocalFavoriteInfo data
        entity.addLongProperty("time").notNull();
    }

    private static void addBookmarks(Schema schema) {
        Entity entity = schema.addEntity("BookmarkInfo");
        entity.setDbName("BOOKMARKS");
        entity.setClassNameDao("BookmarksDao");
        entity.setSuperclass("GalleryInfo");
        // GalleryInfo data
        entity.addLongProperty("gid").primaryKey().notNull();
        entity.addStringProperty("token");
        entity.addStringProperty("title");
        entity.addStringProperty("titleJpn");
        entity.addStringProperty("thumb");
        entity.addIntProperty("category").notNull();
        entity.addStringProperty("posted");
        entity.addStringProperty("uploader");
        entity.addFloatProperty("rating").notNull();
        entity.addStringProperty("simpleLanguage");
        // Bookmark data
        entity.addIntProperty("page").notNull();
        entity.addLongProperty("time").notNull();
    }

    // Since 2
    private static void addFilter(Schema schema) {
        Entity entity = schema.addEntity("Filter");
        entity.setDbName("FILTER");
        entity.setClassNameDao("FilterDao");
        entity.addIdProperty();
        entity.addIntProperty("mode").notNull();
        entity.addStringProperty("text");
        // Since 3
        entity.addBooleanProperty("enable");
    }

    private static void adjustDownloadInfo() throws Exception {
        JavaClassSource javaClass = Roaster.parse(JavaClassSource.class, new File(DOWNLOAD_INFO_PATH));
        // Remove field from GalleryInfo
        javaClass.removeField(javaClass.getField("gid"));
        javaClass.removeField(javaClass.getField("token"));
        javaClass.removeField(javaClass.getField("title"));
        javaClass.removeField(javaClass.getField("titleJpn"));
        javaClass.removeField(javaClass.getField("thumb"));
        javaClass.removeField(javaClass.getField("category"));
        javaClass.removeField(javaClass.getField("posted"));
        javaClass.removeField(javaClass.getField("uploader"));
        javaClass.removeField(javaClass.getField("rating"));
        javaClass.removeField(javaClass.getField("simpleLanguage"));
        // Set all field public
        javaClass.getField("state").setPublic();
        javaClass.getField("legacy").setPublic();
        javaClass.getField("time").setPublic();
        javaClass.getField("label").setPublic();
        // Add Parcelable stuff
        javaClass.addMethod("\t@Override\n" +
                "\tpublic int describeContents() {\n" +
                "\t\treturn 0;\n" +
                "\t}");
        javaClass.addMethod("\t@Override\n" +
                "\tpublic void writeToParcel(Parcel dest, int flags) {\n" +
                "\t\tsuper.writeToParcel(dest, flags);\n" +
                "\t\tdest.writeInt(this.state);\n" +
                "\t\tdest.writeInt(this.legacy);\n" +
                "\t\tdest.writeLong(this.time);\n" +
                "\t\tdest.writeString(this.label);\n" +
                "\t}");
        javaClass.addMethod("\tprotected DownloadInfo(Parcel in) {\n" +
                "\t\tsuper(in);\n" +
                "\t\tthis.state = in.readInt();\n" +
                "\t\tthis.legacy = in.readInt();\n" +
                "\t\tthis.time = in.readLong();\n" +
                "\t\tthis.label = in.readString();\n" +
                "\t}").setConstructor(true);
        javaClass.addField("\tpublic static final Creator<DownloadInfo> CREATOR = new Creator<DownloadInfo>() {\n" +
                "\t\t@Override\n" +
                "\t\tpublic DownloadInfo createFromParcel(Parcel source) {\n" +
                "\t\t\treturn new DownloadInfo(source);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tpublic DownloadInfo[] newArray(int size) {\n" +
                "\t\t\treturn new DownloadInfo[size];\n" +
                "\t\t}\n" +
                "\t};");
        javaClass.addImport("android.os.Parcel");
        // Add download info stuff
        javaClass.addField("public static final int STATE_INVALID = -1");
        javaClass.addField("public static final int STATE_NONE = 0");
        javaClass.addField("public static final int STATE_WAIT = 1");
        javaClass.addField("public static final int STATE_DOWNLOAD = 2");
        javaClass.addField("public static final int STATE_FINISH = 3");
        javaClass.addField("public static final int STATE_FAILED = 4");
        javaClass.addField("public long speed");
        javaClass.addField("public long remaining");
        javaClass.addField("public int finished");
        javaClass.addField("public int downloaded");
        javaClass.addField("public int total");
        // Add from GalleryInfo constructor
        javaClass.addMethod("\tpublic DownloadInfo(GalleryInfo galleryInfo) {\n" +
                "\t\tthis.gid = galleryInfo.gid;\n" +
                "\t\tthis.token = galleryInfo.token;\n" +
                "\t\tthis.title = galleryInfo.title;\n" +
                "\t\tthis.titleJpn = galleryInfo.titleJpn;\n" +
                "\t\tthis.thumb = galleryInfo.thumb;\n" +
                "\t\tthis.category = galleryInfo.category;\n" +
                "\t\tthis.posted = galleryInfo.posted;\n" +
                "\t\tthis.uploader = galleryInfo.uploader;\n" +
                "\t\tthis.rating = galleryInfo.rating;\n" +
                "\t\tthis.simpleTags = galleryInfo.simpleTags;\n" +
                "\t\tthis.simpleLanguage = galleryInfo.simpleLanguage;\n" +
                "\t}").setConstructor(true);
        javaClass.addImport("com.hippo.ehviewer.client.data.GalleryInfo");

        FileWriter fileWriter = new FileWriter(DOWNLOAD_INFO_PATH);
        fileWriter.write(javaClass.toString());
        fileWriter.close();
    }

    private static void adjustHistoryInfo() throws Exception {
        JavaClassSource javaClass = Roaster.parse(JavaClassSource.class, new File(HISTORY_INFO_PATH));
        // Remove field from GalleryInfo
        javaClass.removeField(javaClass.getField("gid"));
        javaClass.removeField(javaClass.getField("token"));
        javaClass.removeField(javaClass.getField("title"));
        javaClass.removeField(javaClass.getField("titleJpn"));
        javaClass.removeField(javaClass.getField("thumb"));
        javaClass.removeField(javaClass.getField("category"));
        javaClass.removeField(javaClass.getField("posted"));
        javaClass.removeField(javaClass.getField("uploader"));
        javaClass.removeField(javaClass.getField("rating"));
        javaClass.removeField(javaClass.getField("simpleLanguage"));
        // Set all field public
        javaClass.getField("mode").setPublic();
        javaClass.getField("time").setPublic();
        // Add Parcelable stuff
        javaClass.addMethod("\t@Override\n" +
                "\tpublic int describeContents() {\n" +
                "\t\treturn 0;\n" +
                "\t}");
        javaClass.addMethod("\t@Override\n" +
                "\tpublic void writeToParcel(Parcel dest, int flags) {\n" +
                "\t\tsuper.writeToParcel(dest, flags);\n" +
                "\t\tdest.writeInt(this.mode);\n" +
                "\t\tdest.writeLong(this.time);\n" +
                "\t}");
        javaClass.addMethod("\tprotected HistoryInfo(Parcel in) {\n" +
                "\t\tsuper(in);\n" +
                "\t\tthis.mode = in.readInt();\n" +
                "\t\tthis.time = in.readLong();\n" +
                "\t}").setConstructor(true);
        javaClass.addField("\tpublic static final Creator<HistoryInfo> CREATOR = new Creator<HistoryInfo>() {\n" +
                "\t\t@Override\n" +
                "\t\tpublic HistoryInfo createFromParcel(Parcel source) {\n" +
                "\t\t\treturn new HistoryInfo(source);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tpublic HistoryInfo[] newArray(int size) {\n" +
                "\t\t\treturn new HistoryInfo[size];\n" +
                "\t\t}\n" +
                "\t};");
        javaClass.addImport("android.os.Parcel");
        // Add from GalleryInfo constructor
        javaClass.addMethod("\tpublic HistoryInfo(GalleryInfo galleryInfo) {\n" +
                "\t\tthis.gid = galleryInfo.gid;\n" +
                "\t\tthis.token = galleryInfo.token;\n" +
                "\t\tthis.title = galleryInfo.title;\n" +
                "\t\tthis.titleJpn = galleryInfo.titleJpn;\n" +
                "\t\tthis.thumb = galleryInfo.thumb;\n" +
                "\t\tthis.category = galleryInfo.category;\n" +
                "\t\tthis.posted = galleryInfo.posted;\n" +
                "\t\tthis.uploader = galleryInfo.uploader;\n" +
                "\t\tthis.rating = galleryInfo.rating;\n" +
                "\t\tthis.simpleTags = galleryInfo.simpleTags;\n" +
                "\t\tthis.simpleLanguage = galleryInfo.simpleLanguage;\n" +
                "\t}").setConstructor(true);
        javaClass.addImport("com.hippo.ehviewer.client.data.GalleryInfo");

        FileWriter fileWriter = new FileWriter(HISTORY_INFO_PATH);
        fileWriter.write(javaClass.toString());
        fileWriter.close();
    }

    private static void adjustQuickSearch() throws Exception {
        JavaClassSource javaClass = Roaster.parse(JavaClassSource.class, new File(QUICK_SEARCH_PATH));

        // Set all field public
        javaClass.getField("id").setPublic();
        javaClass.getField("name").setPublic();
        javaClass.getField("mode").setPublic();
        javaClass.getField("category").setPublic();
        javaClass.getField("keyword").setPublic();
        javaClass.getField("advanceSearch").setPublic();
        javaClass.getField("minRating").setPublic();
        javaClass.getField("pageFrom").setPublic();
        javaClass.getField("pageTo").setPublic();
        javaClass.getField("time").setPublic();

        javaClass.addMethod("\t@Override\n" +
                "\tpublic String toString() {\n" +
                "\t\treturn name;\n" +
                "\t}");

        FileWriter fileWriter = new FileWriter(QUICK_SEARCH_PATH);
        fileWriter.write(javaClass.toString());
        fileWriter.close();
    }

    private static void adjustLocalFavoriteInfo() throws Exception {
        JavaClassSource javaClass = Roaster.parse(JavaClassSource.class, new File(LOCAL_FAVORITE_INFO_PATH));
        // Remove field from GalleryInfo
        javaClass.removeField(javaClass.getField("gid"));
        javaClass.removeField(javaClass.getField("token"));
        javaClass.removeField(javaClass.getField("title"));
        javaClass.removeField(javaClass.getField("titleJpn"));
        javaClass.removeField(javaClass.getField("thumb"));
        javaClass.removeField(javaClass.getField("category"));
        javaClass.removeField(javaClass.getField("posted"));
        javaClass.removeField(javaClass.getField("uploader"));
        javaClass.removeField(javaClass.getField("rating"));
        javaClass.removeField(javaClass.getField("simpleLanguage"));
        // Set all field public
        javaClass.getField("time").setPublic();
        // Add Parcelable stuff
        javaClass.addMethod("\t@Override\n" +
                "\tpublic int describeContents() {\n" +
                "\t\treturn 0;\n" +
                "\t}");
        javaClass.addMethod("\t@Override\n" +
                "\tpublic void writeToParcel(Parcel dest, int flags) {\n" +
                "\t\tsuper.writeToParcel(dest, flags);\n" +
                "\t\tdest.writeLong(this.time);\n" +
                "\t}");
        javaClass.addMethod("\tprotected LocalFavoriteInfo(Parcel in) {\n" +
                "\t\tsuper(in);\n" +
                "\t\tthis.time = in.readLong();\n" +
                "\t}").setConstructor(true);
        javaClass.addField("\tpublic static final Creator<LocalFavoriteInfo> CREATOR = new Creator<LocalFavoriteInfo>() {\n" +
                "\t\t@Override\n" +
                "\t\tpublic LocalFavoriteInfo createFromParcel(Parcel source) {\n" +
                "\t\t\treturn new LocalFavoriteInfo(source);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tpublic LocalFavoriteInfo[] newArray(int size) {\n" +
                "\t\t\treturn new LocalFavoriteInfo[size];\n" +
                "\t\t}\n" +
                "\t};");
        javaClass.addImport("android.os.Parcel");
        // Add from GalleryInfo constructor
        javaClass.addMethod("\tpublic LocalFavoriteInfo(GalleryInfo galleryInfo) {\n" +
                "\t\tthis.gid = galleryInfo.gid;\n" +
                "\t\tthis.token = galleryInfo.token;\n" +
                "\t\tthis.title = galleryInfo.title;\n" +
                "\t\tthis.titleJpn = galleryInfo.titleJpn;\n" +
                "\t\tthis.thumb = galleryInfo.thumb;\n" +
                "\t\tthis.category = galleryInfo.category;\n" +
                "\t\tthis.posted = galleryInfo.posted;\n" +
                "\t\tthis.uploader = galleryInfo.uploader;\n" +
                "\t\tthis.rating = galleryInfo.rating;\n" +
                "\t\tthis.simpleTags = galleryInfo.simpleTags;\n" +
                "\t\tthis.simpleLanguage = galleryInfo.simpleLanguage;\n" +
                "\t}").setConstructor(true);
        javaClass.addImport("com.hippo.ehviewer.client.data.GalleryInfo");

        FileWriter fileWriter = new FileWriter(LOCAL_FAVORITE_INFO_PATH);
        fileWriter.write(javaClass.toString());
        fileWriter.close();
    }

    private static void adjustBookmarkInfo() throws Exception {
        JavaClassSource javaClass = Roaster.parse(JavaClassSource.class, new File(BOOKMARK_INFO_PATH));
        // Remove field from GalleryInfo
        javaClass.removeField(javaClass.getField("gid"));
        javaClass.removeField(javaClass.getField("token"));
        javaClass.removeField(javaClass.getField("title"));
        javaClass.removeField(javaClass.getField("titleJpn"));
        javaClass.removeField(javaClass.getField("thumb"));
        javaClass.removeField(javaClass.getField("category"));
        javaClass.removeField(javaClass.getField("posted"));
        javaClass.removeField(javaClass.getField("uploader"));
        javaClass.removeField(javaClass.getField("rating"));
        javaClass.removeField(javaClass.getField("simpleLanguage"));
        // Set all field public
        javaClass.getField("page").setPublic();
        javaClass.getField("time").setPublic();
        // Add Parcelable stuff
        javaClass.addMethod("\t@Override\n" +
                "\tpublic int describeContents() {\n" +
                "\t\treturn 0;\n" +
                "\t}");
        javaClass.addMethod("\t@Override\n" +
                "\tpublic void writeToParcel(Parcel dest, int flags) {\n" +
                "\t\tsuper.writeToParcel(dest, flags);\n" +
                "\t\tdest.writeInt(this.page);\n" +
                "\t\tdest.writeLong(this.time);\n" +
                "\t}");
        javaClass.addMethod("\tprotected BookmarkInfo(Parcel in) {\n" +
                "\t\tsuper(in);\n" +
                "\t\tthis.page = in.readInt();\n" +
                "\t\tthis.time = in.readLong();\n" +
                "\t}").setConstructor(true);
        javaClass.addField("\tpublic static final Creator<BookmarkInfo> CREATOR = new Creator<BookmarkInfo>() {\n" +
                "\t\t@Override\n" +
                "\t\tpublic BookmarkInfo createFromParcel(Parcel source) {\n" +
                "\t\t\treturn new BookmarkInfo(source);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t@Override\n" +
                "\t\tpublic BookmarkInfo[] newArray(int size) {\n" +
                "\t\t\treturn new BookmarkInfo[size];\n" +
                "\t\t}\n" +
                "\t};");
        javaClass.addImport("android.os.Parcel");
        // Add from GalleryInfo constructor
        javaClass.addMethod("\tpublic BookmarkInfo(GalleryInfo galleryInfo) {\n" +
                "\t\tthis.gid = galleryInfo.gid;\n" +
                "\t\tthis.token = galleryInfo.token;\n" +
                "\t\tthis.title = galleryInfo.title;\n" +
                "\t\tthis.titleJpn = galleryInfo.titleJpn;\n" +
                "\t\tthis.thumb = galleryInfo.thumb;\n" +
                "\t\tthis.category = galleryInfo.category;\n" +
                "\t\tthis.posted = galleryInfo.posted;\n" +
                "\t\tthis.uploader = galleryInfo.uploader;\n" +
                "\t\tthis.rating = galleryInfo.rating;\n" +
                "\t\tthis.simpleTags = galleryInfo.simpleTags;\n" +
                "\t\tthis.simpleLanguage = galleryInfo.simpleLanguage;\n" +
                "\t}").setConstructor(true);
        javaClass.addImport("com.hippo.ehviewer.client.data.GalleryInfo");

        FileWriter fileWriter = new FileWriter(BOOKMARK_INFO_PATH);
        fileWriter.write(javaClass.toString());
        fileWriter.close();
    }

    // Since 2
    private static void adjustFilter() throws Exception {
        JavaClassSource javaClass = Roaster.parse(JavaClassSource.class, new File(FILTER_PATH));
        // Set field public
        javaClass.getField("mode").setPublic();
        javaClass.getField("text").setPublic();
        javaClass.getField("enable").setPublic();
        // Add hashCode method and equals method
        javaClass.addImport("com.hippo.util.HashCodeUtils");
        javaClass.addMethod("\t@Override\n" +
                "\tpublic int hashCode() {\n" +
                "\t\treturn HashCodeUtils.hashCode(mode, text);\n" +
                "\t}");
        javaClass.addImport("com.hippo.yorozuya.ObjectUtils");
        javaClass.addMethod("\t@Override\n" +
                "\tpublic boolean equals(Object o) {\n" +
                "\t\tif (!(o instanceof Filter)) {\n" +
                "\t\t\treturn false;\n" +
                "\t\t}\n" +
                "\n" +
                "\t\tFilter filter = (Filter) o;\n" +
                "\t\treturn filter.mode == mode && ObjectUtils.equal(filter.text, text);\n" +
                "\t}");

        FileWriter fileWriter = new FileWriter(FILTER_PATH);
        fileWriter.write(javaClass.toString());
        fileWriter.close();
    }
}

public final class CommonOperations {

    private static void doAddToFavorites(Activity activity, GalleryInfo galleryInfo,
                                         int slot, EhClient.Callback<Void> listener) {
        if (slot == -1) {
            EhDB.putLocalFavorites(galleryInfo);
            listener.onSuccess(null);
        } else if (slot >= 0 && slot <= 9) {
            EhClient client = EhApplication.getEhClient(activity);
            EhRequest request = new EhRequest();
            request.setMethod(EhClient.METHOD_ADD_FAVORITES);
            request.setArgs(galleryInfo.gid, galleryInfo.token, slot, "");
            request.setCallback(listener);
            client.execute(request);
        } else {
            listener.onFailure(new Exception()); // TODO Add text
        }
    }

    public static void addToFavorites(final Activity activity, final GalleryInfo galleryInfo,
                                      final EhClient.Callback<Void> listener) {
        addToFavorites(activity, galleryInfo, listener, false);
    }

    public static void addToFavorites(final Activity activity, final GalleryInfo galleryInfo,
                                      final EhClient.Callback<Void> listener, boolean select) {
        int slot = Settings.getDefaultFavSlot();
        String[] items = new String[11];
        items[0] = activity.getString(R.string.local_favorites);
        String[] favCat = Settings.getFavCat();
        System.arraycopy(favCat, 0, items, 1, 10);
        if (!select && slot >= -1 && slot <= 9) {
            String newFavoriteName = slot >= 0 ? items[slot + 1] : null;
            doAddToFavorites(activity, galleryInfo, slot, new DelegateFavoriteCallback(listener, galleryInfo, newFavoriteName, slot));
        } else {
            new ListCheckBoxDialogBuilder(activity, items,
                    (builder, dialog, position) -> {
                        int slot1 = position - 1;
                        String newFavoriteName = (slot1 >= 0 && slot1 <= 9) ? items[slot1 + 1] : null;
                        doAddToFavorites(activity, galleryInfo, slot1, new DelegateFavoriteCallback(listener, galleryInfo, newFavoriteName, slot1));
                        if (builder.isChecked()) {
                            Settings.putDefaultFavSlot(slot1);
                        } else {
                            Settings.putDefaultFavSlot(Settings.INVALID_DEFAULT_FAV_SLOT);
                        }
                    }, activity.getString(R.string.remember_favorite_collection), false)
                    .setTitle(R.string.add_favorites_dialog_title)
                    .setOnCancelListener(dialog -> listener.onCancel())
                    .show();
        }
    }

    public static void removeFromFavorites(Activity activity, GalleryInfo galleryInfo,
                                           final EhClient.Callback<Void> listener) {
        EhDB.removeLocalFavorites(galleryInfo.gid);
        EhClient client = EhApplication.getEhClient(activity);
        EhRequest request = new EhRequest();
        request.setMethod(EhClient.METHOD_ADD_FAVORITES);
        request.setArgs(galleryInfo.gid, galleryInfo.token, -1, "");
        request.setCallback(new DelegateFavoriteCallback(listener, galleryInfo, null, -2));
        client.execute(request);
    }

    public static void startDownload(final MainActivity activity, final GalleryInfo galleryInfo, boolean forceDefault) {
        startDownload(activity, Collections.singletonList(galleryInfo), forceDefault);
    }

    // TODO Add context if activity and context are different style
    public static void startDownload(final MainActivity activity, final List<GalleryInfo> galleryInfos, boolean forceDefault) {
        final DownloadManager dm = EhApplication.getDownloadManager(activity);

        LongList toStart = new LongList();
        List<GalleryInfo> toAdd = new ArrayList<>();
        for (GalleryInfo gi : galleryInfos) {
            if (dm.containDownloadInfo(gi.gid)) {
                toStart.add(gi.gid);
            } else {
                toAdd.add(gi);
            }
        }

        if (!toStart.isEmpty()) {
            Intent intent = new Intent(activity, DownloadService.class);
            intent.setAction(DownloadService.ACTION_START_RANGE);
            intent.putExtra(DownloadService.KEY_GID_LIST, toStart);
            ContextCompat.startForegroundService(activity, intent);
        }

        if (toAdd.isEmpty()) {
            activity.showTip(R.string.added_to_download_list, BaseScene.LENGTH_SHORT);
            return;
        }

        boolean justStart = forceDefault;
        String label = null;
        // Get default download label
        if (!justStart && Settings.getHasDefaultDownloadLabel()) {
            label = Settings.getDefaultDownloadLabel();
            justStart = label == null || dm.containLabel(label);
        }
        // If there is no other label, just use null label
        if (!justStart && 0 == dm.getLabelList().size()) {
            justStart = true;
            label = null;
        }

        if (justStart) {
            // Got default label
            for (GalleryInfo gi : toAdd) {
                Intent intent = new Intent(activity, DownloadService.class);
                intent.setAction(DownloadService.ACTION_START);
                intent.putExtra(DownloadService.KEY_LABEL, label);
                intent.putExtra(DownloadService.KEY_GALLERY_INFO, gi);
                ContextCompat.startForegroundService(activity, intent);
            }
            // Notify
            activity.showTip(R.string.added_to_download_list, BaseScene.LENGTH_SHORT);
        } else {
            // Let use chose label
            List<DownloadLabel> list = dm.getLabelList();
            final String[] items = new String[list.size() + 1];
            items[0] = activity.getString(R.string.default_download_label_name);
            for (int i = 0, n = list.size(); i < n; i++) {
                items[i + 1] = list.get(i).getLabel();
            }

            new ListCheckBoxDialogBuilder(activity, items,
                    (builder, dialog, position) -> {
                        String label1;
                        if (position == 0) {
                            label1 = null;
                        } else {
                            label1 = items[position];
                            if (!dm.containLabel(label1)) {
                                label1 = null;
                            }
                        }
                        // Start download
                        for (GalleryInfo gi : toAdd) {
                            Intent intent = new Intent(activity, DownloadService.class);
                            intent.setAction(DownloadService.ACTION_START);
                            intent.putExtra(DownloadService.KEY_LABEL, label1);
                            intent.putExtra(DownloadService.KEY_GALLERY_INFO, gi);
                            ContextCompat.startForegroundService(activity, intent);
                        }
                        // Save settings
                        if (builder.isChecked()) {
                            Settings.putHasDefaultDownloadLabel(true);
                            Settings.putDefaultDownloadLabel(label1);
                        } else {
                            Settings.putHasDefaultDownloadLabel(false);
                        }
                        // Notify
                        activity.showTip(R.string.added_to_download_list, BaseScene.LENGTH_SHORT);
                    }, activity.getString(R.string.remember_download_label), false)
                    .setTitle(R.string.download)
                    .show();
        }
    }

    public static void ensureNoMediaFile(UniFile file) {
        if (null == file) {
            return;
        }

        UniFile noMedia = file.createFile(".nomedia");
        if (null == noMedia) {
            return;
        }

        InputStream is = null;
        try {
            is = noMedia.openInputStream();
        } catch (IOException e) {
            // Ignore
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    public static void removeNoMediaFile(UniFile file) {
        if (null == file) {
            return;
        }

        UniFile noMedia = file.subFile(".nomedia");
        if (null != noMedia && noMedia.isFile()) {
            noMedia.delete();
        }
    }

    private static class DelegateFavoriteCallback implements EhClient.Callback<Void> {

        private final EhClient.Callback<Void> delegate;
        private final GalleryInfo info;
        private final String newFavoriteName;
        private final int slot;

        DelegateFavoriteCallback(EhClient.Callback<Void> delegate, GalleryInfo info,
                                 String newFavoriteName, int slot) {
            this.delegate = delegate;
            this.info = info;
            this.newFavoriteName = newFavoriteName;
            this.slot = slot;
        }

        @Override
        public void onSuccess(Void result) {
            info.favoriteName = newFavoriteName;
            info.favoriteSlot = slot;
            delegate.onSuccess(result);
            EhApplication.getFavouriteStatusRouter().modifyFavourites(info.gid, slot);
        }

        @Override
        public void onFailure(Exception e) {
            delegate.onFailure(e);
        }

        @Override
        public void onCancel() {
            delegate.onCancel();
        }
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/playground/r/ZRTQN09/java.lang.security.xmlinputfactory-external-entities-enabled.xmlinputfactory-external-entities-enabled

import javax.xml.stream.XMLInputFactory;

class GoodXMLInputFactory {
    public GoodXMLInputFactory() {
        final XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLInputFactoryExternalEntitiesEnabled
        xmlInputFactory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
    }
}

class BadXMLInputFactory {
    public BadXMLInputFactory() {
        final XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        // ruleid: java_xxe_rule-XMLInputFactoryExternalEntitiesEnabled
        xmlInputFactory.setProperty("javax.xml.stream.isSupportingExternalEntities", true);
    }
}

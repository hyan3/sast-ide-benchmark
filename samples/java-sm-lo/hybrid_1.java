public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final String EXTRA_VIEW_LATEST = "org.fdroid.fdroid.views.main.MainActivity.VIEW_LATEST";
    private static final String EXTRA_VIEW_CATEGORIES = "org.fdroid.fdroid.views.main.MainActivity.VIEW_CATEGORIES";
    private static final String EXTRA_VIEW_NEARBY = "org.fdroid.fdroid.views.main.MainActivity.VIEW_NEARBY";
    public static final String EXTRA_VIEW_UPDATES = "org.fdroid.fdroid.views.main.MainActivity.VIEW_UPDATES";
    public static final String EXTRA_VIEW_SETTINGS = "org.fdroid.fdroid.views.main.MainActivity.VIEW_SETTINGS";

    static final int REQUEST_LOCATION_PERMISSIONS = 0xEF0F;
    static final int REQUEST_STORAGE_PERMISSIONS = 0xB004;
    static final int REQUEST_STORAGE_ACCESS = 0x40E5;

    public static final String ACTION_REQUEST_SWAP = "requestSwap";

    private RecyclerView pager;
    private MainViewAdapter adapter;
    private BottomNavigationView bottomNavigation;
    private BadgeDrawable updatesBadge;

    private final ActivityResultLauncher<String> permissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                // no-op
            });

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FDroidApp fdroidApp = (FDroidApp) getApplication();
        fdroidApp.setSecureWindow(this);

        fdroidApp.applyPureBlackBackgroundInDarkTheme(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        adapter = new MainViewAdapter(this);

        pager = findViewById(R.id.main_view_pager);
        pager.setHasFixedSize(true);
        pager.setLayoutManager(new NonScrollingHorizontalLayoutManager(this));
        pager.setAdapter(adapter);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        setSelectedMenuInNav(Preferences.get().getBottomNavigationViewName());
        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            pager.scrollToPosition(item.getOrder());

            if (item.getItemId() == R.id.latest) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_LATEST);
            } else if (item.getItemId() == R.id.categories) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_CATEGORIES);
            } else if (item.getItemId() == R.id.nearby) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_NEARBY);

                NearbyViewBinder.updateUsbOtg(MainActivity.this);
            } else if (item.getItemId() == R.id.updates) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_UPDATES);
            } else if (item.getItemId() == R.id.settings) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_SETTINGS);
            }
            return true;

        });
        updatesBadge = bottomNavigation.getOrCreateBadge(R.id.updates);
        updatesBadge.setVisible(false);

        AppUpdateStatusManager.getInstance(this).getNumUpdatableApps().observe(this, this::refreshUpdatesBadge);

        Intent intent = getIntent();
        if (handleMainViewSelectIntent(intent)) {
            return;
        }
        handleSearchOrAppViewIntent(intent);
    }

    /**
     * {@link android.material.navigation.NavigationBarView} says "Menu items
     * can also be used for programmatically selecting which destination is
     * currently active. It can be done using {@code MenuItem.setChecked(true)}".
     */
    private void setSelectedMenuInNav(int menuId) {
        int position = adapter.adapterPositionFromItemId(menuId);
        if (position < 0) {
            Log.e(TAG, "Invalid menu position: " + position);
        } else {
            pager.scrollToPosition(position);
            bottomNavigation.getMenu().getItem(position).setChecked(true);
        }
    }

    private void setSelectedMenuInNav(final String viewName) {
        if (EXTRA_VIEW_LATEST.equals(viewName)) {
            setSelectedMenuInNav(R.id.latest);
        } else if (EXTRA_VIEW_CATEGORIES.equals(viewName)) {
            setSelectedMenuInNav(R.id.categories);
        } else if (EXTRA_VIEW_NEARBY.equals(viewName)) {
            setSelectedMenuInNav(R.id.nearby);
        } else if (EXTRA_VIEW_UPDATES.equals(viewName)) {
            setSelectedMenuInNav(R.id.updates);
        } else if (EXTRA_VIEW_SETTINGS.equals(viewName)) {
            setSelectedMenuInNav(R.id.settings);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        FDroidApp.checkStartTor(this, Preferences.get());

        NearbyViewBinder.updateExternalStorageViews(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // don't request this in onResume, because the launcher causes a call to that,
        // even if permission permanently denied, so we'll get an infinite loop
        if (Build.VERSION.SDK_INT >= 33) {
            String notificationPerm = Manifest.permission.POST_NOTIFICATIONS;
            if (ContextCompat.checkSelfPermission(this, notificationPerm) != PackageManager.PERMISSION_GRANTED) {
                permissionLauncher.launch(notificationPerm);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (handleMainViewSelectIntent(intent)) {
            return;
        }

        handleSearchOrAppViewIntent(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_STORAGE_ACCESS) {
            TreeUriScannerIntentService.onActivityResult(this, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSIONS) {
            WifiStateChangeService.start(this, null);
            ContextCompat.startForegroundService(this, new Intent(this, SwapService.class));
        } else if (requestCode == REQUEST_STORAGE_PERMISSIONS) {
            Toast.makeText(this,
                    this.getString(R.string.scan_removable_storage_toast, ""),
                    Toast.LENGTH_SHORT).show();
            SDCardScannerService.scan(this);
        }
    }

    /**
     * Handle an {@link Intent} that shows a specific tab in the main view.
     */
    private boolean handleMainViewSelectIntent(Intent intent) {
        if (intent.hasExtra(EXTRA_VIEW_NEARBY)) {
            setSelectedMenuInNav(R.id.nearby);
            return true;
        } else if (intent.hasExtra(EXTRA_VIEW_UPDATES)) {
            setSelectedMenuInNav(R.id.updates);
            return true;
        } else if (intent.hasExtra(EXTRA_VIEW_SETTINGS)) {
            setSelectedMenuInNav(R.id.settings);
            return true;
        }
        return false;
    }

    /**
     * Since any app could send this {@link Intent}, and the search terms are
     * fed into a SQL query, the data must be strictly sanitized to avoid
     * SQL injection attacks.
     */
    private void handleSearchOrAppViewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null) performSearch(query);
            return;
        }

        final Uri data = intent.getData();
        if (data == null) {
            return;
        }

        final String scheme = data.getScheme();
        final String path = data.getPath();
        String packageName = null;
        String query = null;
        if (data.isHierarchical()) {
            final String host = data.getHost();
            if (host == null) {
                return;
            }
            switch (host) {
                case "f-droid.org":
                case "www.f-droid.org":
                case "staging.f-droid.org":
                    if (path.startsWith("/app/") || path.startsWith("/packages/")
                            || path.matches("^/[a-z][a-z][a-zA-Z_-]*/packages/.*")) {
                        // http://f-droid.org/app/packageName
                        packageName = data.getLastPathSegment();
                    } else if (path.startsWith("/repository/browse")) {
                        // http://f-droid.org/repository/browse?fdfilter=search+query
                        query = data.getQueryParameter("fdfilter");

                        // http://f-droid.org/repository/browse?fdid=packageName
                        packageName = data.getQueryParameter("fdid");
                    } else if ("/app".equals(data.getPath()) || "/packages".equals(data.getPath())) {
                        packageName = null;
                    }
                    break;
                case "details":
                    // market://details?id=app.id
                    packageName = data.getQueryParameter("id");
                    break;
                case "search":
                    // market://search?q=query
                    query = data.getQueryParameter("q");
                    break;
                case "play.google.com":
                    if (path.startsWith("/store/apps/details")) {
                        // http://play.google.com/store/apps/details?id=app.id
                        packageName = data.getQueryParameter("id");
                    } else if (path.startsWith("/store/search")) {
                        // http://play.google.com/store/search?q=foo
                        query = data.getQueryParameter("q");
                    }
                    break;
                case "apps":
                case "amazon.com":
                case "www.amazon.com":
                    // amzn://apps/android?p=app.id
                    // http://amazon.com/gp/mas/dl/android?s=app.id
                    packageName = data.getQueryParameter("p");
                    query = data.getQueryParameter("s");
                    break;
            }
        } else if ("fdroid.app".equals(scheme)) {
            // fdroid.app:app.id
            packageName = data.getSchemeSpecificPart();
        } else if ("fdroid.search".equals(scheme)) {
            // fdroid.search:query
            query = data.getSchemeSpecificPart();
        }

        if (!TextUtils.isEmpty(query)) {
            // an old format for querying via packageName
            if (query.startsWith("pname:")) {
                packageName = query.split(":")[1];
            }

            // sometimes, search URLs include pub: or other things before the query string
            if (query.contains(":")) {
                query = query.split(":")[1];
            }
        }

        if (!TextUtils.isEmpty(packageName)) {
            // sanitize packageName to be a valid Java packageName and prevent exploits
            packageName = packageName.replaceAll("[^A-Za-z\\d_.]", "");
            Utils.debugLog(TAG, "FDroid launched via app link for '" + packageName + "'");
            Intent intentToInvoke = new Intent(this, AppDetailsActivity.class);
            intentToInvoke.putExtra(AppDetailsActivity.EXTRA_APPID, packageName);
            startActivity(intentToInvoke);
            finish();
        } else if (!TextUtils.isEmpty(query)) {
            Utils.debugLog(TAG, "FDroid launched via search link for '" + query + "'");
            performSearch(query);
        }
    }

    /**
     * These strings might end up in a SQL query, so strip all non-alpha-num
     */
    static String sanitizeSearchTerms(@NonNull String query) {
        return query.replaceAll("[^\\p{L}\\d_ -]", " ");
    }

    /**
     * Initiates the {@link AppListActivity} with the relevant search terms passed in via the query arg.
     */
    private void performSearch(@NonNull String query) {
        Intent searchIntent = new Intent(this, AppListActivity.class);
        searchIntent.putExtra(AppListActivity.EXTRA_SEARCH_TERMS, sanitizeSearchTerms(query));
        startActivity(searchIntent);
    }

    private void refreshUpdatesBadge(int canUpdateCount) {
        if (canUpdateCount <= 0) {
            updatesBadge.setVisible(false);
            updatesBadge.clearNumber();
        } else {
            updatesBadge.setNumber(canUpdateCount);
            updatesBadge.setVisible(true);
        }
    }

    private static class NonScrollingHorizontalLayoutManager extends LinearLayoutManager {
        NonScrollingHorizontalLayoutManager(Context context) {
            super(context, LinearLayoutManager.HORIZONTAL, false);
        }

        @Override
        public boolean canScrollHorizontally() {
            return false;
        }

        @Override
        public boolean canScrollVertically() {
            return false;
        }
    }
}

class CookieDatabase {

    private static final String LOG_TAG = CookieDatabase.class.getSimpleName();

    private static final int VERSION_1 = 1;
    private static final String TABLE_COOKIE = "OK_HTTP_3_COOKIE";
    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_NAME = "NAME";
    private static final String COLUMN_VALUE = "VALUE";
    private static final String COLUMN_EXPIRES_AT = "EXPIRES_AT";
    private static final String COLUMN_DOMAIN = "DOMAIN";
    private static final String COLUMN_PATH = "PATH";
    private static final String COLUMN_SECURE = "SECURE";
    private static final String COLUMN_HTTP_ONLY = "HTTP_ONLY";
    private static final String COLUMN_PERSISTENT = "PERSISTENT";
    private static final String COLUMN_HOST_ONLY = "HOST_ONLY";

    private static final int DB_VERSION = VERSION_1;

    private final Map<Cookie, Long> cookieIdMap = new HashMap<>();
    private final SQLiteOpenHelper helper;
    private final SQLiteDatabase db;

    public CookieDatabase(Context context, String name) {
        helper = new MSQLiteBuilder()
                .version(VERSION_1)
                .createTable(TABLE_COOKIE)
                .insertColumn(TABLE_COOKIE, COLUMN_NAME, String.class)
                .insertColumn(TABLE_COOKIE, COLUMN_VALUE, String.class)
                .insertColumn(TABLE_COOKIE, COLUMN_EXPIRES_AT, long.class)
                .insertColumn(TABLE_COOKIE, COLUMN_DOMAIN, String.class)
                .insertColumn(TABLE_COOKIE, COLUMN_PATH, String.class)
                .insertColumn(TABLE_COOKIE, COLUMN_SECURE, boolean.class)
                .insertColumn(TABLE_COOKIE, COLUMN_HTTP_ONLY, boolean.class)
                .insertColumn(TABLE_COOKIE, COLUMN_PERSISTENT, boolean.class)
                .insertColumn(TABLE_COOKIE, COLUMN_HOST_ONLY, boolean.class)
                .build(context, name, DB_VERSION);
        db = helper.getWritableDatabase();
    }

    @Nullable
    private static Cookie getCookie(Cursor cursor, long now) {
        String name = SqlUtils.getString(cursor, COLUMN_NAME, null);
        String value = SqlUtils.getString(cursor, COLUMN_VALUE, null);
        long expiresAt = SqlUtils.getLong(cursor, COLUMN_EXPIRES_AT, 0);
        String domain = SqlUtils.getString(cursor, COLUMN_DOMAIN, null);
        String path = SqlUtils.getString(cursor, COLUMN_PATH, null);
        boolean secure = SqlUtils.getBoolean(cursor, COLUMN_SECURE, false);
        boolean httpOnly = SqlUtils.getBoolean(cursor, COLUMN_HTTP_ONLY, false);
        boolean persistent = SqlUtils.getBoolean(cursor, COLUMN_PERSISTENT, false);
        boolean hostOnly = SqlUtils.getBoolean(cursor, COLUMN_HOST_ONLY, false);

        if (name == null || domain == null || path == null) {
            return null;
        }

        // Check non-persistent or expired
        if (!persistent || expiresAt <= now) {
            return null;
        }

        Cookie.Builder builder = new Cookie.Builder();
        builder.name(name);
        builder.value(value);
        if (hostOnly) {
            builder.hostOnlyDomain(domain);
        } else {
            builder.domain(domain);
        }
        builder.path(path);
        builder.expiresAt(expiresAt);
        if (secure) builder.secure();
        if (httpOnly) builder.httpOnly();
        return builder.build();
    }

    public Map<String, CookieSet> getAllCookies() {
        long now = System.currentTimeMillis();
        Map<String, CookieSet> map = new HashMap<>();
        List<Long> toRemove = new ArrayList<>();

        try (Cursor cursor = db.rawQuery("SELECT * FROM " + TABLE_COOKIE + ";", null)) {
            while (cursor.moveToNext()) {
                long id = SqlUtils.getLong(cursor, COLUMN_ID, 0);
                Cookie cookie = getCookie(cursor, now);

                if (cookie != null) {
                    // Save id of the cookie in db
                    cookieIdMap.put(cookie, id);

                    // Put cookie to set
                    CookieSet set = map.get(cookie.domain());
                    if (set == null) {
                        set = new CookieSet();
                        map.put(cookie.domain(), set);
                    }
                    set.add(cookie);
                } else {
                    // Mark to remove the cookie
                    toRemove.add(id);
                }
            }
        }

        // Remove invalid or expired cookie
        if (!toRemove.isEmpty()) {
            SQLiteStatement statement = db.compileStatement(
                    "DELETE FROM " + TABLE_COOKIE + " WHERE " + COLUMN_ID + " = ?;");
            db.beginTransaction();
            try {
                for (long id : toRemove) {
                    statement.bindLong(1, id);
                    statement.executeUpdateDelete();
                }
                db.setTransactionSuccessful();
            } finally {
                db.endTransaction();
            }
        }

        return map;
    }

    public ContentValues toContentValues(Cookie cookie) {
        ContentValues contentValues = new ContentValues(9);
        contentValues.put(COLUMN_NAME, cookie.name());
        contentValues.put(COLUMN_VALUE, cookie.value());
        contentValues.put(COLUMN_EXPIRES_AT, cookie.expiresAt());
        contentValues.put(COLUMN_DOMAIN, cookie.domain());
        contentValues.put(COLUMN_PATH, cookie.path());
        contentValues.put(COLUMN_SECURE, cookie.secure());
        contentValues.put(COLUMN_HTTP_ONLY, cookie.httpOnly());
        contentValues.put(COLUMN_PERSISTENT, cookie.persistent());
        contentValues.put(COLUMN_HOST_ONLY, cookie.hostOnly());
        return contentValues;
    }

    public void add(Cookie cookie) {
        long id = db.insert(TABLE_COOKIE, null, toContentValues(cookie));
        if (id != -1L) {
            Long oldId = cookieIdMap.put(cookie, id);
            if (oldId != null) Log.e(LOG_TAG, "Add a duplicate cookie");
        } else {
            Log.e(LOG_TAG, "An error occurred when insert a cookie");
        }
    }

    public void update(Cookie from, Cookie to) {
        Long id = cookieIdMap.get(from);
        if (id == null) {
            Log.e(LOG_TAG, "Can't get id when update the cookie");
            return;
        }

        ContentValues values = toContentValues(to);
        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = {id.toString()};
        int count = db.update(TABLE_COOKIE, values, whereClause, whereArgs);
        if (count != 1) {
            Log.e(LOG_TAG, "Bad result when update cookie: " + count);
        }

        // Update it in cookie-id map
        cookieIdMap.remove(from);
        cookieIdMap.put(to, id);
    }

    public void remove(Cookie cookie) {
        Long id = cookieIdMap.get(cookie);
        if (id == null) {
            Log.e(LOG_TAG, "Can't get id when remove the cookie");
            return;
        }

        String whereClause = COLUMN_ID + " = ?";
        String[] whereArgs = {id.toString()};
        int count = db.delete(TABLE_COOKIE, whereClause, whereArgs);
        if (count != 1) {
            Log.e(LOG_TAG, "Bad result when remove cookie: " + count);
        }

        // Remove it from cookie-id map
        cookieIdMap.remove(cookie);
    }

    public void clear() {
        db.delete(TABLE_COOKIE, null, null);
        cookieIdMap.clear();
    }

    public void close() {
        db.close();
        helper.close();
    }
}

@SuppressWarnings("LineLength")
public class InstallManagerService {
    private static final String TAG = "InstallManagerService";

    private static final String ACTION_CANCEL = "org.fdroid.fdroid.installer.action.CANCEL";

    @SuppressLint("StaticFieldLeak") // we are using ApplicationContext, so hopefully that's fine
    private static InstallManagerService instance;

    private final Context context;
    private final LocalBroadcastManager localBroadcastManager;
    private final AppUpdateStatusManager appUpdateStatusManager;

    public static InstallManagerService getInstance(Context context) {
        if (instance == null) {
            instance = new InstallManagerService(context.getApplicationContext());
        }
        return instance;
    }

    public InstallManagerService(Context context) {
        this.context = context;
        this.localBroadcastManager = LocalBroadcastManager.getInstance(context);
        this.appUpdateStatusManager = AppUpdateStatusManager.getInstance(context);
        // cancel intent can't use LocalBroadcastManager, because it comes from system process
        IntentFilter cancelFilter = new IntentFilter();
        cancelFilter.addAction(ACTION_CANCEL);
        ContextCompat.registerReceiver(context, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "Received cancel intent: " + intent);
                if (!ACTION_CANCEL.equals(intent.getAction())) return;
                cancel(context, intent.getStringExtra(DownloaderService.EXTRA_CANONICAL_URL));
            }
        }, cancelFilter, ContextCompat.RECEIVER_NOT_EXPORTED);
    }

    private void onCancel(String canonicalUrl) {
        DownloaderService.cancel(canonicalUrl);
        Apk apk = appUpdateStatusManager.getApk(canonicalUrl);
        if (apk != null) {
            Utils.debugLog(TAG, "also canceling OBB downloads");
            DownloaderService.cancel(apk.getPatchObbUrl());
            DownloaderService.cancel(apk.getMainObbUrl());
        }
    }

    /**
     * This goes through a series of checks to make sure that the incoming
     * {@link Intent} is still valid.
     * <p>
     * For example, if F-Droid is killed while installing, it might not receive
     * the message that the install completed successfully. The checks need to be
     * as specific as possible so as not to block things like installing updates
     * with the same {@link PackageInfo#versionCode}, which happens sometimes,
     * and is allowed by Android.
     */
    private void queue(String canonicalUrl, String packageName, @NonNull App app, @NonNull Apk apk) {
        Utils.debugLog(TAG, "queue " + packageName);

        if (TextUtils.isEmpty(canonicalUrl)) {
            Utils.debugLog(TAG, "empty canonicalUrl, nothing to do");
            return;
        }

        PackageInfo packageInfo = Utils.getPackageInfo(context, packageName);
        if (packageInfo != null && PackageInfoCompat.getLongVersionCode(packageInfo) == apk.versionCode
                && TextUtils.equals(packageInfo.versionName, apk.versionName)) {
            Log.i(TAG, "Install action no longer valid since its installed, ignoring: " + packageName);
            return;
        }

        appUpdateStatusManager.addApk(app, apk, AppUpdateStatusManager.Status.Downloading, null);

        getMainObb(canonicalUrl, apk);
        getPatchObb(canonicalUrl, apk);

        Utils.runOffUiThread(() -> ApkCache.getApkCacheState(context, apk), pair -> {
            ApkCache.ApkCacheState state = pair.first;
            SanitizedFile apkFilePath = pair.second;
            if (state == ApkCache.ApkCacheState.MISS_OR_PARTIAL) {
                Utils.debugLog(TAG, "download " + canonicalUrl + " " + apkFilePath);
                DownloaderService.queue(context, canonicalUrl, app, apk);
            } else if (state == ApkCache.ApkCacheState.CACHED) {
                Utils.debugLog(TAG, "skip download, we have it, straight to install " + canonicalUrl + " " + apkFilePath);
                Uri canonicalUri = Uri.parse(canonicalUrl);
                onDownloadStarted(canonicalUri);
                onDownloadComplete(canonicalUri, apkFilePath, app, apk);
            } else {
                Utils.debugLog(TAG, "delete and download again " + canonicalUrl + " " + apkFilePath);
                Utils.runOffUiThread(apkFilePath::delete);
                DownloaderService.queue(context, canonicalUrl, app, apk);
            }
        });
    }

    public void onDownloadStarted(Uri canonicalUri) {
        // App should currently be in the "PendingDownload" state, so this changes it to "Downloading".
        appUpdateStatusManager.updateApk(canonicalUri.toString(),
                AppUpdateStatusManager.Status.Downloading, getDownloadCancelIntent(canonicalUri));
    }

    public void onDownloadProgress(Uri canonicalUri, App app, Apk apk, long bytesRead, long totalBytes) {
        if (appUpdateStatusManager.get(canonicalUri.toString()) == null) {
            // if our app got killed, we need to re-add the APK here
            appUpdateStatusManager.addApk(app, apk, AppUpdateStatusManager.Status.Downloading,
                    getDownloadCancelIntent(canonicalUri));
        }
        appUpdateStatusManager.updateApkProgress(canonicalUri.toString(), totalBytes, bytesRead);
    }

    public void onDownloadComplete(Uri canonicalUri, File file, App intentApp, Apk intentApk) {
        String canonicalUrl = canonicalUri.toString();
        Uri localApkUri = Uri.fromFile(file);

        Utils.debugLog(TAG, "download completed of " + canonicalUri + " to " + localApkUri);
        appUpdateStatusManager.updateApk(canonicalUrl,
                AppUpdateStatusManager.Status.ReadyToInstall, null);

        App app = appUpdateStatusManager.getApp(canonicalUrl);
        Apk apk = appUpdateStatusManager.getApk(canonicalUrl);
        if (app == null || apk == null) {
            // These may be null if our app was killed and the download job restarted.
            // Then, we can take the objects we saved in the intent which survive app death.
            app = intentApp;
            apk = intentApk;
        }
        if (app != null && apk != null) {
            registerInstallReceiver(canonicalUrl);
            InstallerService.install(context, localApkUri, canonicalUri, app, apk);
        } else {
            Log.e(TAG, "Could not install " + canonicalUrl + " because no app or apk available.");
        }
    }

    public void onDownloadFailed(Uri canonicalUri, String errorMsg) {
        appUpdateStatusManager.setDownloadError(canonicalUri.toString(), errorMsg);
    }

    private PendingIntent getDownloadCancelIntent(Uri canonicalUri) {
        Intent intentObject = new Intent(ACTION_CANCEL);
        intentObject.putExtra(DownloaderService.EXTRA_CANONICAL_URL, canonicalUri.toString());
        return PendingIntent.getBroadcast(context, 0, intentObject,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
    }

    private void getMainObb(final String canonicalUrl, Apk apk) {
        getObb(canonicalUrl, apk.getMainObbUrl(), apk.getMainObbFile(), apk.obbMainFileSha256, apk.repoId);
    }

    private void getPatchObb(final String canonicalUrl, Apk apk) {
        getObb(canonicalUrl, apk.getPatchObbUrl(), apk.getPatchObbFile(), apk.obbPatchFileSha256, apk.repoId);
    }

    /**
     * Check if any OBB files are available, and if so, download and install them. This
     * also deletes any obsolete OBB files, per the spec, since there can be only one
     * "main" and one "patch" OBB installed at a time.
     *
     * @see <a href="https://developer.android.com/google/play/expansion-files.html">APK Expansion Files</a>
     */
    private void getObb(final String canonicalUrl, String obbUrlString,
                        final File obbDestFile, final String hash, final long repoId) {
        if (obbDestFile == null || obbDestFile.exists() || TextUtils.isEmpty(obbUrlString)) {
            return;
        }
        final BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloaderService.ACTION_STARTED.equals(action)) {
                    Utils.debugLog(TAG, action + " " + intent);
                } else if (DownloaderService.ACTION_PROGRESS.equals(action)) {

                    long bytesRead = intent.getLongExtra(DownloaderService.EXTRA_BYTES_READ, 0);
                    long totalBytes = intent.getLongExtra(DownloaderService.EXTRA_TOTAL_BYTES, 0);
                    appUpdateStatusManager.updateApkProgress(canonicalUrl, totalBytes, bytesRead);
                } else if (DownloaderService.ACTION_COMPLETE.equals(action)) {
                    localBroadcastManager.unregisterReceiver(this);
                    File localFile = new File(intent.getStringExtra(DownloaderService.EXTRA_DOWNLOAD_PATH));
                    Uri localApkUri = Uri.fromFile(localFile);
                    Utils.debugLog(TAG, "OBB download completed " + intent.getDataString()
                            + " to " + localApkUri);

                    try {
                        if (Utils.isFileMatchingHash(localFile, hash, SHA_256)) {
                            Utils.debugLog(TAG, "Installing OBB " + localFile + " to " + obbDestFile);
                            FileUtils.forceMkdirParent(obbDestFile);
                            FileUtils.copyFile(localFile, obbDestFile);
                            FileFilter filter = new WildcardFileFilter(
                                    obbDestFile.getName().substring(0, 4) + "*.obb");
                            for (File f : obbDestFile.getParentFile().listFiles(filter)) {
                                if (!f.equals(obbDestFile)) {
                                    Utils.debugLog(TAG, "Deleting obsolete OBB " + f);
                                    FileUtils.deleteQuietly(f);
                                }
                            }
                        } else {
                            Utils.debugLog(TAG, localFile + " deleted, did not match hash: " + hash);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        FileUtils.deleteQuietly(localFile);
                    }
                } else if (DownloaderService.ACTION_INTERRUPTED.equals(action)) {
                    localBroadcastManager.unregisterReceiver(this);
                } else if (DownloaderService.ACTION_CONNECTION_FAILED.equals(action)) {
                    localBroadcastManager.unregisterReceiver(this);
                } else {
                    throw new RuntimeException("intent action not handled!");
                }
            }
        };
        DownloaderService.queue(context, repoId, obbUrlString, obbUrlString);
        localBroadcastManager.registerReceiver(downloadReceiver,
                DownloaderService.getIntentFilter(obbUrlString));
    }

    /**
     * Register a {@link BroadcastReceiver} for tracking install progress for a
     * give {@link Uri}.  There can be multiple of these registered at a time.
     */
    private void registerInstallReceiver(String canonicalUrl) {
        BroadcastReceiver installReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String canonicalUrl = intent.getDataString();
                App app;
                Apk apk;
                switch (intent.getAction()) {
                    case Installer.ACTION_INSTALL_STARTED:
                        appUpdateStatusManager.updateApk(canonicalUrl,
                                AppUpdateStatusManager.Status.Installing, null);
                        break;
                    case Installer.ACTION_INSTALL_COMPLETE:
                        appUpdateStatusManager.updateApk(canonicalUrl,
                                AppUpdateStatusManager.Status.Installed, null);
                        Apk apkComplete = appUpdateStatusManager.getApk(canonicalUrl);

                        if (apkComplete != null && apkComplete.isApk()) {
                            try {
                                PackageManagerCompat.setInstaller(context, context.getPackageManager(), apkComplete.packageName);
                            } catch (SecurityException e) {
                                // Will happen if we fell back to DefaultInstaller for some reason.
                            }
                        }
                        localBroadcastManager.unregisterReceiver(this);
                        break;
                    case Installer.ACTION_INSTALL_INTERRUPTED:
                        app = intent.getParcelableExtra(Installer.EXTRA_APP);
                        apk = intent.getParcelableExtra(Installer.EXTRA_APK);
                        String errorMessage =
                                intent.getStringExtra(Installer.EXTRA_ERROR_MESSAGE);
                        if (!TextUtils.isEmpty(errorMessage)) {
                            appUpdateStatusManager.setApkError(app, apk, errorMessage);
                        } else {
                            appUpdateStatusManager.removeApk(canonicalUrl);
                        }
                        localBroadcastManager.unregisterReceiver(this);
                        break;
                    case Installer.ACTION_INSTALL_USER_INTERACTION:
                        app = intent.getParcelableExtra(Installer.EXTRA_APP);
                        apk = intent.getParcelableExtra(Installer.EXTRA_APK);
                        PendingIntent installPendingIntent = intent.getParcelableExtra(Installer.EXTRA_USER_INTERACTION_PI);
                        appUpdateStatusManager.addApk(app, apk, AppUpdateStatusManager.Status.ReadyToInstall, installPendingIntent);
                        break;
                    default:
                        throw new RuntimeException("intent action not handled!");
                }
            }
        };

        localBroadcastManager.registerReceiver(installReceiver,
                Installer.getInstallIntentFilter(Uri.parse(canonicalUrl)));
    }

    /**
     * Install an APK, checking the cache and downloading if necessary before
     * starting the process.  All notifications are sent as an {@link Intent}
     * via local broadcasts to be received by {@link BroadcastReceiver}s per
     * {@code urlString}.  This also marks a given APK as in the process of
     * being installed, with the {@code urlString} of the download used as the
     * unique ID,
     * <p>
     * and the file hash used to verify that things are the same.
     *
     * @param context this app's {@link Context}
     */
    public static void queue(Context context, @NonNull App app, @NonNull Apk apk) {
        String canonicalUrl = apk.getCanonicalUrl();
        AppUpdateStatusManager.getInstance(context).addApk(app, apk, AppUpdateStatusManager.Status.PendingInstall, null);
        Utils.debugLog(TAG, "queue " + app.packageName + " " + apk.versionCode + " from " + canonicalUrl);
        InstallManagerService.getInstance(context).queue(canonicalUrl, apk.packageName, app, apk);
    }

    public static void cancel(Context context, String canonicalUrl) {
        InstallManagerService.getInstance(context).onCancel(canonicalUrl);
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package crypto;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

public class GCMNonceReuse
{
    public static final int GCM_TAG_LENGTH = 16;
    public static final String IV1String = "ab0123456789";
    public static final byte[] IV1 = IV1String.getBytes();
    public static final byte[] IV2 = new byte[]{97,98,48,49,50,51,52,53,54,55,56,57};

    private static byte[] IV3;

    private static SecretKey secretKey;

    GCMParameterSpec gcmParameterSpec;


    public GCMNonceReuse() {

        IV3 = IV1String.getBytes();

        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(256);
            secretKey = keyGenerator.generateKey();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt2&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt2(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt3&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt3(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
        //ruleid: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV1String.getBytes());
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encryptSecure&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encryptSecure(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

        // Generate a new, random IV for each encryption operation
        SecureRandom secureRandom = new SecureRandom();
        byte[] iv = new byte[12];
        // GCM standard recommends a 12-byte (96-bit) IV
        secureRandom.nextBytes(iv);

        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        // Prepend the IV to the ciphertext to ensure it's available for decryption
        byte[] cipherTextWithIv = new byte[iv.length + cipherText.length];
        System.arraycopy(iv, 0, cipherTextWithIv, 0, iv.length);
        System.arraycopy(cipherText, 0, cipherTextWithIv, iv.length, cipherText.length);

        return Base64.getEncoder().encodeToString(cipherTextWithIv);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decrypt&input=ciphertext>'
    public String decrypt(String cipherText) throws Exception {
        cipherText = URLDecoder.decode(cipherText, StandardCharsets.UTF_8.toString()).replace(" ", "+");;

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] decoded = Base64.getDecoder().decode(cipherText);
        byte[] decryptedText = cipher.doFinal(decoded);

        return new String(decryptedText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decryptSecure&input=ciphertext>'
    public static String decryptSecure(String encryptedText) throws Exception {
        encryptedText = URLDecoder.decode(encryptedText, StandardCharsets.UTF_8.toString()).replace(" ", "+");

        // Decode the base64-encoded string
        byte[] decodedCipherText = Base64.getDecoder().decode(encryptedText);

        // Extract the IV from the beginning of the ciphertext
        byte[] iv = new byte[12];

        // Extract the actual ciphertext
        byte[] cipherText = new byte[decodedCipherText.length - 12];
        System.arraycopy(decodedCipherText, 12, cipherText, 0, cipherText.length);

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameterSpec);

        // Decrypt the ciphertext
        byte[] clearTextBytes = cipher.doFinal(cipherText);

        return new String(clearTextBytes);
    }

}
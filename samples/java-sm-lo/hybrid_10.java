public class Apk implements Comparable<Apk>, Parcelable {

    // Using only byte-range keeps it only 8-bits in the SQLite database
    public static final int SDK_VERSION_MAX_VALUE = Byte.MAX_VALUE;
    public static final int SDK_VERSION_MIN_VALUE = 0;
    public static final String RELEASE_CHANNEL_BETA = "Beta";
    public static final String RELEASE_CHANNEL_STABLE = "Stable";

    // these are never set by the Apk/package index metadata
    public String repoAddress;
    public String canonicalRepoAddress;
    private SanitizedFile installedFile; // the .apk file on this device's filesystem
    public boolean compatible; // True if compatible with the device.
    public long repoId; // the database ID of the repo it comes from

    // these come directly from the index metadata
    public String packageName;
    @Nullable
    public String versionName;
    public long versionCode;
    public long size; // Size in bytes - 0 means we don't know!
    public int minSdkVersion = SDK_VERSION_MIN_VALUE; // 0 if unknown
    public int targetSdkVersion = SDK_VERSION_MIN_VALUE; // 0 if unknown
    public int maxSdkVersion = SDK_VERSION_MAX_VALUE; // "infinity" if not set
    private String obbMainFile;
    public String obbMainFileSha256;
    private String obbPatchFile;
    public String obbPatchFileSha256;
    public Date added;
    public List<String> releaseChannels;
    /**
     * The array of the names of the permissions that this APK requests. This is the
     * same data as {@link android.content.pm.PackageInfo#requestedPermissions}. Note this
     * does not mean that all these permissions have been granted, only requested.  For
     * example, a regular app can request a system permission, but it won't be granted it.
     * Set this to null for no permissions.
     */
    @Nullable
    public String[] requestedPermissions;
    public String[] features; // null if empty or unknown

    public String[] nativecode; // null if empty or unknown

    /**
     * Standard SHA-256 fingerprint of the X.509 signing certificate.  This can
     * be fetched in a few different ways:
     * <ul>
     *     <li><code>apksigner verify --print-certs example.apk</code></li>
     *     <li><code>jarsigner -verify -verbose -certs index-v1.jar</code></li>
     *     <li><code>keytool -list -v -keystore keystore.jks</code></li>
     * </ul>
     *
     * @see
     * <a href="https://source.android.com/security/apksigning/v3#apk-signature-scheme-v3-block"><tt>signer</tt> in APK Signature Scheme v3</a>
     */
    public String signer;

    /**
     * Can be null when created with {@link #Apk(PackageInfo)}
     * which happens only for showing an installed version
     * in {@link org.fdroid.fdroid.views.AppDetailsActivity}.
     */
    @Nullable
    public FileV1 apkFile;

    /**
     * If not null, this is the name of the source tarball for the
     * application. Null indicates that it's a developer's binary
     * build - otherwise it's built from source.
     */
    @Nullable
    private String srcname;

    public String[] incompatibleReasons;

    String[] antiFeatures;

    Map<String, String> antiFeatureReasons = new HashMap<>();

    String whatsNew;

    public Apk() {
    }

    /**
     * Creates a dummy APK from what is currently installed.
     */
    public Apk(@NonNull PackageInfo packageInfo) {
        packageName = packageInfo.packageName;
        versionName = packageInfo.versionName;
        versionCode = packageInfo.versionCode;
        targetSdkVersion = packageInfo.applicationInfo.targetSdkVersion;
        releaseChannels = Collections.emptyList();

        // zero for "we don't know". If we require this in the future,
        // then we could look up the file on disk if required.
        size = 0;

        // Same as size. We could look this up if required but not needed at time of writing.
        installedFile = null;

        // We couldn't load it from the database, indicating it is not available in any of our repos.
        repoId = 0;
    }

    public Apk(AppVersion v, Repository repo) {
        if (v.getRepoId() != repo.getRepoId()) throw new IllegalArgumentException();
        repoAddress = Utils.getRepoAddress(repo);
        canonicalRepoAddress = repo.getAddress();
        added = new Date(v.getAdded());
        features = v.getFeatureNames().toArray(new String[0]);
        setPackageName(v.getPackageName());
        compatible = v.isCompatible();
        AppManifest manifest = v.getManifest();
        minSdkVersion = manifest.getUsesSdk() == null ?
                SDK_VERSION_MIN_VALUE : manifest.getUsesSdk().getMinSdkVersion();
        targetSdkVersion = manifest.getUsesSdk() == null ?
                minSdkVersion : manifest.getUsesSdk().getTargetSdkVersion();
        maxSdkVersion = manifest.getMaxSdkVersion() == null ? SDK_VERSION_MAX_VALUE : manifest.getMaxSdkVersion();
        List<String> channels = v.getReleaseChannels();
        if (channels.isEmpty()) {
            // no channels means stable
            releaseChannels = Collections.singletonList(RELEASE_CHANNEL_STABLE);
        } else {
            releaseChannels = channels;
        }
        apkFile = v.getFile();
        setRequestedPermissions(v.getUsesPermission(), 0);
        setRequestedPermissions(v.getUsesPermissionSdk23(), 23);
        nativecode = v.getNativeCode().toArray(new String[0]);
        repoId = v.getRepoId();
        SignerV2 signer = v.getManifest().getSigner();
        this.signer = signer == null ? null : signer.getSha256().get(0);
        size = v.getFile().getSize() == null ? 0 : v.getFile().getSize();
        srcname = v.getSrc() == null ? null : v.getSrc().getName();
        versionName = manifest.getVersionName();
        versionCode = manifest.getVersionCode();
        antiFeatures = v.getAntiFeatureKeys().toArray(new String[0]);
        LocaleListCompat localeList = LocaleListCompat.getDefault();
        antiFeatureReasons.clear();
        for (String antiFeature : antiFeatures) {
            antiFeatureReasons.put(antiFeature, v.getAntiFeatureReason(antiFeature, localeList));
        }
        whatsNew = v.getWhatsNew(App.getLocales());
    }

    public void setCompatibility(CompatibilityChecker checker) {
        final List<String> reasons = checker.getIncompatibleReasons(this);
        if (reasons.isEmpty()) {
            compatible = true;
            incompatibleReasons = null;
        } else {
            compatible = false;
            incompatibleReasons = reasons.toArray(new String[0]);
        }
    }

    private void checkRepoAddress() {
        if (repoAddress == null || apkFile == null) {
            throw new IllegalStateException(
                    "Apk needs to have both Schema.ApkTable.Cols.REPO_ADDRESS and "
                            + "Schema.ApkTable.Cols.NAME set in order to calculate URL "
                            + "[package: " + packageName
                            + ", versionCode: " + versionCode
                            + ", apkName: " + getApkPath()
                            + ", repoAddress: " + repoAddress
                            + ", repoId: " + repoId + "]");
        }
    }

    @Nullable
    public String getApkPath() {
        return apkFile == null ? "" : apkFile.getName();
    }

    /**
     * Get the URL that points to the canonical download source for this
     * package.  This is also used as the unique ID for tracking downloading,
     * progress, and notifications throughout the whole install process.  It
     * is guaranteed to uniquely represent this file since it points to a file
     * on the file system of the canonical webserver.
     *
     * @see org.fdroid.fdroid.installer.InstallManagerService
     */
    public String getCanonicalUrl() {
        checkRepoAddress();
        /* Each String in pathElements might contain a /, should keep these as path elements */
        return Utils.getUri(canonicalRepoAddress, getApkPath().split("/")).toString();
    }

    public String getDownloadUrl() {
        checkRepoAddress();
        return Utils.getUri(repoAddress, getApkPath().split("/")).toString();
    }

    /**
     * Get the URL to download the <i>main</i> expansion file, the primary
     * expansion file for additional resources required by your application.
     * The filename will always have the format:
     * "main.<i>versionCode</i>.<i>packageName</i>.obb"
     *
     * @return a URL to download the OBB file that matches this APK
     * @see #getPatchObbUrl()
     * @see <a href="https://developer.android.com/google/play/expansion-files.html">APK Expansion Files</a>
     */
    public String getMainObbUrl() {
        if (repoAddress == null || obbMainFile == null) {
            return null;
        }
        checkRepoAddress();
        return repoAddress + "/" + obbMainFile;
    }

    /**
     * Get the URL to download the optional <i>patch</i> expansion file, which
     * is intended for small updates to the <i>main</i> expansion file.
     * The filename will always have the format:
     * "patch.<i>versionCode</i>.<i>packageName</i>.obb"
     *
     * @return a URL to download the OBB file that matches this APK
     * @see #getMainObbUrl()
     * @see <a href="https://developer.android.com/google/play/expansion-files.html">APK Expansion Files</a>
     */
    public String getPatchObbUrl() {
        if (repoAddress == null || obbPatchFile == null) {
            return null;
        }
        checkRepoAddress();
        return repoAddress + "/" + obbPatchFile;
    }

    /**
     * Get the local {@link File} to the "main" OBB file.
     */
    public File getMainObbFile() {
        if (obbMainFile == null) {
            return null;
        }
        return new File(App.getObbDir(packageName), obbMainFile);
    }

    /**
     * Get the local {@link File} to the "patch" OBB file.
     */
    public File getPatchObbFile() {
        if (obbPatchFile == null) {
            return null;
        }
        return new File(App.getObbDir(packageName), obbPatchFile);
    }

    @Override
    public int compareTo(@NonNull Apk apk) {
        return Long.compare(versionCode, apk.versionCode);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.packageName);
        dest.writeString(this.versionName);
        dest.writeLong(this.versionCode);
        dest.writeLong(this.size);
        dest.writeLong(this.repoId);
        dest.writeInt(this.minSdkVersion);
        dest.writeInt(this.targetSdkVersion);
        dest.writeInt(this.maxSdkVersion);
        dest.writeString(this.obbMainFile);
        dest.writeString(this.obbMainFileSha256);
        dest.writeString(this.obbPatchFile);
        dest.writeString(this.obbPatchFileSha256);
        dest.writeLong(this.added != null ? this.added.getTime() : -1);
        dest.writeStringArray(this.requestedPermissions);
        dest.writeStringArray(this.features);
        dest.writeStringArray(this.nativecode);
        dest.writeString(this.signer);
        dest.writeByte(this.compatible ? (byte) 1 : (byte) 0);
        dest.writeString(this.apkFile != null ? this.apkFile.serialize() : null);
        dest.writeSerializable(this.installedFile);
        dest.writeString(this.srcname);
        dest.writeString(this.repoAddress);
        dest.writeString(this.canonicalRepoAddress);
        dest.writeStringArray(this.incompatibleReasons);
        dest.writeStringArray(this.antiFeatures);
    }

    protected Apk(Parcel in) {
        this.packageName = in.readString();
        this.versionName = in.readString();
        this.versionCode = in.readLong();
        this.size = in.readLong();
        this.repoId = in.readLong();
        this.minSdkVersion = in.readInt();
        this.targetSdkVersion = in.readInt();
        this.maxSdkVersion = in.readInt();
        this.obbMainFile = in.readString();
        this.obbMainFileSha256 = in.readString();
        this.obbPatchFile = in.readString();
        this.obbPatchFileSha256 = in.readString();
        long tmpAdded = in.readLong();
        this.added = tmpAdded == -1 ? null : new Date(tmpAdded);
        this.requestedPermissions = in.createStringArray();
        this.features = in.createStringArray();
        this.nativecode = in.createStringArray();
        this.signer = in.readString();
        this.compatible = in.readByte() != 0;
        this.apkFile = FileV1.deserialize(in.readString());
        this.installedFile = (SanitizedFile) in.readSerializable();
        this.srcname = in.readString();
        this.repoAddress = in.readString();
        this.canonicalRepoAddress = in.readString();
        this.incompatibleReasons = in.createStringArray();
        this.antiFeatures = in.createStringArray();
    }

    public static final Parcelable.Creator<Apk> CREATOR = new Parcelable.Creator<Apk>() {
        @Override
        public Apk createFromParcel(Parcel source) {
            return new Apk(source);
        }

        @Override
        public Apk[] newArray(int size) {
            return new Apk[size];
        }
    };

    /**
     * Set the Package Name property while ensuring it is sanitized.
     */
    private void setPackageName(String packageName) {
        if (Utils.isSafePackageName(packageName)) {
            this.packageName = packageName;
        } else {
            throw new IllegalArgumentException("Repo index package entry includes unsafe packageName: '"
                    + packageName + "'");
        }
    }

    /**
     * Generate the set of requested permissions for the current Android version.
     * <p>
     * There are also a bunch of crazy rules where having one permission will imply
     * another permission, for example:
     * {@link Manifest.permission#WRITE_EXTERNAL_STORAGE} implies
     * {@link Manifest.permission#READ_EXTERNAL_STORAGE}.
     * Many of these rules are for quite old Android versions,
     * so they are not included here.
     *
     * @see Manifest.permission#READ_EXTERNAL_STORAGE
     * @see
     * <a href="https://android.googlesource.com/platform/frameworks/base/+/refs/heads/master/data/etc/platform.xml">platform.xml</a>
     */
    @VisibleForTesting
    public void setRequestedPermissions(List<PermissionV2> permissions, int minSdk) {
        HashSet<String> set = new HashSet<>();
        if (requestedPermissions != null) {
            Collections.addAll(set, requestedPermissions);
        }
        for (PermissionV2 versions : permissions) {
            int maxSdk = Integer.MAX_VALUE;
            if (versions.getMaxSdkVersion() != null) {
                maxSdk = versions.getMaxSdkVersion();
            }
            if (minSdk <= Build.VERSION.SDK_INT && Build.VERSION.SDK_INT <= maxSdk) {
                set.add(versions.getName());
            }
        }
        if (set.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            set.add(Manifest.permission.READ_EXTERNAL_STORAGE);
        }
        if (Build.VERSION.SDK_INT >= 29) {
            if (set.contains(Manifest.permission.ACCESS_FINE_LOCATION)) {
                set.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            }
            if (targetSdkVersion < 29) {
                if (set.contains(Manifest.permission.ACCESS_FINE_LOCATION)) {
                    set.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);
                }
                if (set.contains(Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    set.add(Manifest.permission.ACCESS_BACKGROUND_LOCATION);
                }
                if (set.contains(Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    set.add(Manifest.permission.ACCESS_MEDIA_LOCATION);
                }
            }
            // Else do nothing. The targetSdk for the below split-permissions is set to 29,
            // so we don't make any changes for apps targeting 29 or above
        }
        if (Build.VERSION.SDK_INT >= 31) {
            if (targetSdkVersion < 31) {
                if (set.contains(Manifest.permission.BLUETOOTH) ||
                        set.contains(Manifest.permission.BLUETOOTH_ADMIN)) {
                    set.add(Manifest.permission.BLUETOOTH_SCAN);
                    set.add(Manifest.permission.BLUETOOTH_CONNECT);
                    set.add(Manifest.permission.BLUETOOTH_ADVERTISE);
                }
            }
            // Else do nothing. The targetSdk for the above split-permissions is set to 31,
            // so we don't make any changes for apps targeting 31 or above
        }
        if (Build.VERSION.SDK_INT >= 33) {
            if (targetSdkVersion < 33) {
                if (set.contains(Manifest.permission.BODY_SENSORS)) {
                    set.add(Manifest.permission.BODY_SENSORS_BACKGROUND);
                }
                if (set.contains(Manifest.permission.READ_EXTERNAL_STORAGE) ||
                        set.contains(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                    set.add(Manifest.permission.READ_MEDIA_AUDIO);
                    set.add(Manifest.permission.READ_MEDIA_VIDEO);
                    set.add(Manifest.permission.READ_MEDIA_IMAGES);
                }
            }
            // Else do nothing. The targetSdk for the above split-permissions is set to 33,
            // so we don't make any changes for apps targeting 33 or above
        }
        if (Build.VERSION.SDK_INT >= 34) {
            if (set.contains(Manifest.permission.READ_MEDIA_IMAGES) ||
                    set.contains(Manifest.permission.READ_MEDIA_VIDEO) ||
                    set.contains(Manifest.permission.ACCESS_MEDIA_LOCATION)
            ) {
                set.add(Manifest.permission.READ_MEDIA_VISUAL_USER_SELECTED);
            }
        }

        String[] perms = set.toArray(new String[0]);
        requestedPermissions = perms.length == 0 ? null : perms;
    }

    /**
     * Get the install path for a "non-apk" media file, with special cases for
     * files that can be usefully installed without PrivilegedExtension.
     * Defaults to {@link android.os.Environment#DIRECTORY_DOWNLOADS}.
     *
     * @return the install path for this {@link Apk}
     * @link <a href="https://source.android.com/devices/tech/ota/nonab/inside_packages">Inside OTA Packages</a>
     */

    public File getMediaInstallPath(Context context) {
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DOWNLOADS); // Default for all other non-apk/media files
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(this.getCanonicalUrl());
        if (TextUtils.isEmpty(fileExtension)) return path;
        MimeTypeMap mimeTypeMap = MimeTypeMap.getSingleton();
        String mimeType = mimeTypeMap.getMimeTypeFromExtension(fileExtension);
        String topLevelType = null;
        if (!TextUtils.isEmpty(mimeType)) {
            String[] mimeTypeSections = mimeType.split("/");
            if (mimeTypeSections.length == 0) {
                topLevelType = "";
            } else {
                topLevelType = mimeTypeSections[0];
            }
        }
        if ("audio".equals(topLevelType)) {
            path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MUSIC);
        } else if ("image".equals(topLevelType)) {
            path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES);
        } else if ("video".equals(topLevelType)) {
            path = Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_MOVIES);
        } else if ("zip".equals(fileExtension)) {
            try (ZipFile zipFile = new ZipFile(ApkCache.getApkDownloadPath(context, this.getCanonicalUrl()))) {
                if (zipFile.size() == 1) {
                    String name = zipFile.entries().nextElement().getName();
                    if (name != null && name.endsWith(".obf")) {
                        // temporarily cache this, it will be deleted after unzipping
                        return context.getCacheDir();
                    }
                } else if (zipFile.getEntry("META-INF/com/google/android/update-binary") != null) {
                    // Over-The-Air update ZIP files
                    return new File(context.getApplicationInfo().dataDir + "/ota");
                }
            } catch (IOException e) {
                // this should happen when running isMediaInstalled() and the file isn't installed
                // other cases are probably bugs
                if (BuildConfig.DEBUG) e.printStackTrace();
            }
            return path;
        } else if ("apk".equals(fileExtension)) {
            throw new IllegalStateException("APKs should not be handled in the media install path!");
        }
        return path;
    }

    public File getInstalledMediaFile(Context context) {
        return new File(this.getMediaInstallPath(context), SanitizedFile.sanitizeFileName(getApkPath()));
    }

    /**
     * Check whether a media file is "installed" as based on the file type's
     * install path, derived in {@link #getMediaInstallPath(Context)}
     */
    public boolean isMediaInstalled(Context context) {
        return getInstalledMediaFile(context).isFile();
    }

    /**
     * Default to assuming apk if apkName is null since that has always been
     * what we had.
     *
     * @return true if this is an apk instead of a non-apk/media file
     */
    public boolean isApk() {
        return apkFile == null
                || apkFile.getName().substring(apkFile.getName().length() - 4)
                .toLowerCase(Locale.ENGLISH).endsWith(".apk");
    }
}

public class DownloaderFactory extends org.fdroid.download.DownloaderFactory {

    private static final String TAG = "DownloaderFactory";
    // TODO move to application object or inject where needed
    public static final DownloaderFactory INSTANCE = new DownloaderFactory();
    public static final HttpManager HTTP_MANAGER =
            new HttpManager(Utils.getUserAgent(), FDroidApp.queryString, NetCipher.getProxy(), new DnsWithCache());

    @NonNull
    @Override
    public Downloader create(Repository repo, @NonNull Uri uri, @NonNull IndexFile indexFile,
                             @NonNull File destFile) throws IOException {
        List<Mirror> mirrors = repo.getMirrors();
        return create(repo, mirrors, uri, indexFile, destFile, null);
    }

    @NonNull
    @Override
    @SuppressLint("MissingPermission") // we'd need to ask for Bluetooth permission, but code unmaintained
    protected Downloader create(@NonNull Repository repo, @NonNull List<Mirror> mirrors, @NonNull Uri uri,
                                @NonNull IndexFile indexFile, @NonNull File destFile,
                                @Nullable Mirror tryFirst) throws IOException {
        Downloader downloader;

        String scheme = uri.getScheme();
        if (BluetoothDownloader.SCHEME.equals(scheme)) {
            downloader = new BluetoothDownloader(uri, indexFile, destFile);
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            downloader = new TreeUriDownloader(uri, indexFile, destFile);
        } else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            downloader = new LocalFileDownloader(uri, indexFile, destFile);
        } else {
            String repoAddress = Utils.getRepoAddress(repo);
            String path = uri.toString().replace(repoAddress, "");
            Utils.debugLog(TAG, "Using suffix " + path + " with mirrors " + mirrors);
            Proxy proxy = NetCipher.getProxy();
            DownloadRequest request = new DownloadRequest(indexFile, mirrors, proxy,
                    repo.getUsername(), repo.getPassword(), tryFirst);
            Preferences prefs = Preferences.get();
            boolean oldIndex = prefs.isForceOldIndexEnabled();
            boolean v1OrUnknown = repo.getFormatVersion() == null ||
                    repo.getFormatVersion() == IndexFormatVersion.ONE;
            if (oldIndex || v1OrUnknown) {
                //noinspection deprecation
                downloader = new HttpDownloader(HTTP_MANAGER, request, destFile);
            } else {
                DownloadRequest r;
                if (request.getIndexFile().getIpfsCidV1() == null || !prefs.isIpfsEnabled()) {
                    r = request;
                } else {
                    // add IPFS gateways to mirrors, because have have a CIDv1 and IPFS is enabled in preferences
                    List<Mirror> m = new ArrayList<>(mirrors);
                    m.addAll(loadIpfsGateways(prefs));
                    r = new DownloadRequest(request.getIndexFile(), m, proxy, repo.getUsername(),
                            repo.getPassword(), tryFirst);
                }
                downloader = new HttpDownloaderV2(HTTP_MANAGER, r, destFile);
            }
        }
        return downloader;
    }

    private static List<Mirror> loadIpfsGateways(Preferences prefs) {
        List<Mirror> mirrorList = new ArrayList<>();
        for (String gatewayUrl : prefs.getActiveIpfsGateways()) {
            mirrorList.add(new Mirror(gatewayUrl, null, true));
        }
        return mirrorList;
    }

}

private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_SUGGESTIONS + " (" +
                    "_id INTEGER PRIMARY KEY" +
                    "," + COLUMN_QUERY + " TEXT" +
                    "," + COLUMN_DATE + " LONG" +
                    ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUGGESTIONS);
            onCreate(db);
        }
    }

public class FileInstallerActivity extends FragmentActivity {

    private static final String TAG = "FileInstallerActivity";
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 1;

    static final String ACTION_INSTALL_FILE
            = "org.fdroid.fdroid.installer.FileInstaller.action.INSTALL_PACKAGE";
    static final String ACTION_UNINSTALL_FILE
            = "org.fdroid.fdroid.installer.FileInstaller.action.UNINSTALL_PACKAGE";

    private FileInstallerActivity activity;

    // for the broadcasts
    private FileInstaller installer;

    private App app;
    private Apk apk;
    private Uri localApkUri;

    /**
     * @see InstallManagerService
     */
    private Uri canonicalUri;

    private int act = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        Intent intent = getIntent();
        String action = intent.getAction();
        localApkUri = intent.getData();
        app = intent.getParcelableExtra(Installer.EXTRA_APP);
        apk = intent.getParcelableExtra(Installer.EXTRA_APK);
        installer = new FileInstaller(this, app, apk);
        if (ACTION_INSTALL_FILE.equals(action)) {
            canonicalUri = Uri.parse(intent.getStringExtra(DownloaderService.EXTRA_CANONICAL_URL));
            if (hasStoragePermission()) {
                installPackage(localApkUri, canonicalUri, apk);
            } else {
                requestPermission();
                act = 1;
            }
        } else if (ACTION_UNINSTALL_FILE.equals(action)) {
            canonicalUri = null;
            if (hasStoragePermission()) {
                uninstallPackage(apk);
            } else {
                requestPermission();
                act = 2;
            }
        } else {
            throw new IllegalStateException("Intent action not specified!");
        }
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (!hasStoragePermission()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showDialog();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE);
            }
        }
    }

    private void showDialog() {

        // pass the theme, it is not automatically applied due to activity's Theme.NoDisplay
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_App);
        builder.setMessage(R.string.app_permission_storage)
                .setPositiveButton(R.string.ok, (dialog, id) -> ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE))
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    if (act == 1) {
                        installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
                    } else if (act == 2) {
                        installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_INTERRUPTED);
                    }
                    finish();
                })
                .create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_STORAGE) { // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (act == 1) {
                    installPackage(localApkUri, canonicalUri, apk);
                } else if (act == 2) {
                    uninstallPackage(apk);
                }
            } else {
                if (act == 1) {
                    installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
                } else if (act == 2) {
                    installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_INTERRUPTED);
                }
            }
            finish();
        }
    }

    private void installPackage(Uri localApkUri, Uri canonicalUri, Apk apk) {
        Utils.debugLog(TAG, "Installing: " + localApkUri.getPath());
        File path = apk.getInstalledMediaFile(activity.getApplicationContext());
        path.getParentFile().mkdirs();
        try {
            FileUtils.copyFile(new File(localApkUri.getPath()), path);
        } catch (IOException e) {
            Utils.debugLog(TAG, "Failed to copy: " + e.getMessage());
            installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
        }
        if (apk.isMediaInstalled(activity.getApplicationContext())) { // Copying worked
            Utils.debugLog(TAG, "Copying worked: " + localApkUri.getPath());
            if (!postInstall(canonicalUri, apk, path)) {
                Toast.makeText(this, String.format(this.getString(R.string.app_installed_media), path),
                        Toast.LENGTH_LONG).show();
                installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_COMPLETE);
            }
        } else {
            installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
        }
        finish();
    }

    /**
     * Run any file-type-specific processes after the file has been copied into place.
     *
     * @return whether this handles sending the {@link Installer#ACTION_INSTALL_COMPLETE}
     * broadcast.
     */
    private boolean postInstall(Uri canonicalUri, Apk apk, File path) {
        if (path.getName().endsWith(".obf") || path.getName().endsWith(".obf.zip")) {
            ObfInstallerService.install(this, canonicalUri, app, apk, path);
            return true;
        }
        return false;
    }

    private void uninstallPackage(Apk apk) {
        if (apk.isMediaInstalled(activity.getApplicationContext())) {
            File file = apk.getInstalledMediaFile(activity.getApplicationContext());
            if (!file.delete()) {
                installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_INTERRUPTED);
                return;
            }
        }
        installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_COMPLETE);
        finish();
    }
}


// License: MIT (c) GitLab Inc.
package xml;

import java.io.*;
import java.beans.XMLDecoder;
import java.util.ArrayList;
import java.util.List;

class TestXmlDecoder {

    String safeVariable = "";

    // This will just create a file in your /tmp/ folder named Hacked1.txt
    void decodeObjectUnsafe1() throws IOException {
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce.xml");
        XMLDecoder decoder = new XMLDecoder(inputStream);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked1.txt file");
    }

    // This will just create a file in your /tmp/ folder named Hacked2.txt
    void decodeObjectUnsafe2() throws IOException {
        ClassLoader customClassLoader = null;
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked2.txt file");
    }

    void decodeObjectUnsafe3() throws IOException {
        System.out.println("Running Unsafe3(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ruleid: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe4() throws IOException {
        System.out.println("Running Unsafe4(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {

                    return super.loadClass(name, resolve);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe5() throws IOException {
        System.out.println("Running Unsafe5(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            return super.loadClass(name, resolve);
                        }
                    });
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe6() throws IOException {
        System.out.println("Running Unsafe6(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!name.equals(
                                    TestXmlDecoder.class.getName()) &&
                                    !name.equals(XMLDecoder.class.getName())) {

                                return super.loadClass(name, resolve);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe1() throws IOException {
        System.out.println("Running Safe1(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe2() throws IOException {
        System.out
                .println("Running Safe2(): (This should run normally as xml file does not contain malicious payload.)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-safe.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                new ClassLoader() {
                    @Override
                    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                        if (!name.equals(
                                TestXmlDecoder.class.getName()) &&
                                !name.equals(XMLDecoder.class.getName())) {
                            throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                        }
                        return super.loadClass(name, resolve);
                    }
                });
        // ok: java_xml_rule-XmlDecoder
        Object o = decoder.readObject();
        decoder.close();
    }

    void decodeObjectSafe3() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe3(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!allowedClasses.contains(name)) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe4() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe4(): (Exceptions will be thrown)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!allowedClasses.contains(name)) {
                                throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    public void setSafeVariable(String str) {
        safeVariable = str;
        System.out.println("SafeVariable set: " + safeVariable);
    }

}

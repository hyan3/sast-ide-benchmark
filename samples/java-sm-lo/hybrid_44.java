public class PanicResponderActivity extends AppCompatActivity {

    private static final String TAG = PanicResponderActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (!Panic.isTriggerIntent(intent)) {
            finish();
            return;
        }

        // received intent from panic app
        Log.i(TAG, "Received Panic Trigger...");

        final Preferences preferences = Preferences.get();

        boolean receivedTriggerFromConnectedApp = PanicResponder.receivedTriggerFromConnectedApp(this);
        final boolean runningAppUninstalls = PrivilegedInstaller.isDefault(this);

        ArrayList<String> wipeList = new ArrayList<>(preferences.getPanicWipeSet());
        preferences.setPanicWipeSet(Collections.<String>emptySet());
        preferences.setPanicTmpSelectedSet(Collections.<String>emptySet());

        if (receivedTriggerFromConnectedApp && runningAppUninstalls && wipeList.size() > 0) {

            // if this app (e.g. F-Droid) is to be deleted, do it last
            if (wipeList.contains(getPackageName())) {
                wipeList.remove(getPackageName());
                wipeList.add(getPackageName());
            }

            final Context context = this;
            final CountDownLatch latch = new CountDownLatch(1);
            final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(context);
            final String lastToUninstall = wipeList.get(wipeList.size() - 1);
            final BroadcastReceiver receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch ((intent.getAction())) {
                        case Installer.ACTION_UNINSTALL_INTERRUPTED:
                        case Installer.ACTION_UNINSTALL_COMPLETE:
                            latch.countDown();
                            break;
                    }
                }
            };
            lbm.registerReceiver(receiver, Installer.getUninstallIntentFilter(lastToUninstall));

            for (String packageName : wipeList) {
                App app = new App();
                Apk apk = new Apk();
                app.packageName = packageName;
                apk.packageName = packageName;
                InstallerService.uninstall(context, app, apk);
            }

            // wait for apps to uninstall before triggering final responses
            new Thread() {
                @Override
                public void run() {
                    try {
                        latch.await(10, TimeUnit.MINUTES);
                    } catch (InterruptedException e) {
                        // ignored
                    }
                    lbm.unregisterReceiver(receiver);
                    if (preferences.panicResetRepos()) {
                        resetRepos(context);
                    }
                    if (preferences.panicHide()) {
                        HidingManager.hide(context);
                    }
                    if (preferences.panicExit()) {
                        exitAndClear();
                    }
                }
            }.start();
        } else if (receivedTriggerFromConnectedApp) {
            if (preferences.panicResetRepos()) {
                resetRepos(this);
            }
            // Performing destructive panic response
            if (preferences.panicHide()) {
                Log.i(TAG, "Hiding app...");
                HidingManager.hide(this);
            }
        }

        // exit and clear, if not deactivated
        if (!runningAppUninstalls && preferences.panicExit()) {
            exitAndClear();
        }
        finish();
    }

    static void resetRepos(Context context) {
        DBHelper.resetRepos(context);
    }

    private void exitAndClear() {
        ExitActivity.exitAndRemoveFromRecentApps(this);
        finishAndRemoveTask();
    }
}

public final class SpiderDen {

    @Nullable
    private static SimpleDiskCache sCache;
    @Nullable
    private final UniFile mDownloadDir;
    private final long mGid;
    private volatile int mMode = SpiderQueen.MODE_READ;

    public SpiderDen(GalleryInfo galleryInfo) {
        mGid = galleryInfo.gid;
        mDownloadDir = getGalleryDownloadDir(galleryInfo);
    }

    public static void initialize(Context context) {
        sCache = new SimpleDiskCache(new File(context.getCacheDir(), "image"),
                MathUtils.clamp(Settings.getReadCacheSize(), 40, 1280) * 1024 * 1024);
    }

    public static UniFile getGalleryDownloadDir(GalleryInfo galleryInfo) {
        UniFile dir = Settings.getDownloadLocation();
        if (dir != null) {
            // Read from DB
            String dirname = EhDB.getDownloadDirname(galleryInfo.gid);
            if (null != dirname) {
                // Some dirname may be invalid in some version
                dirname = FileUtils.sanitizeFilename(dirname);
                EhDB.putDownloadDirname(galleryInfo.gid, dirname);
            }

            // Find it
            if (null == dirname) {
                UniFile[] files = dir.listFiles(new StartWithFilenameFilter(galleryInfo.gid + "-"));
                if (null != files) {
                    // Get max-length-name dir
                    int maxLength = -1;
                    for (UniFile file : files) {
                        if (file.isDirectory()) {
                            String name = file.getName();
                            int length = name.length();
                            if (length > maxLength) {
                                maxLength = length;
                                dirname = name;
                            }
                        }
                    }
                    if (null != dirname) {
                        EhDB.putDownloadDirname(galleryInfo.gid, dirname);
                    }
                }
            }

            // Create it
            if (null == dirname) {
                dirname = FileUtils.sanitizeFilename(galleryInfo.gid + "-" + EhUtils.getSuitableTitle(galleryInfo));
                EhDB.putDownloadDirname(galleryInfo.gid, dirname);
            }

            return dir.subFile(dirname);
        } else {
            return null;
        }
    }

    /**
     * @param extension with dot
     */
    public static String generateImageFilename(int index, String extension) {
        return String.format(Locale.US, "%08d%s", index + 1, extension);
    }

    @Nullable
    private static UniFile findImageFile(UniFile dir, int index) {
        for (String extension : GalleryProvider2.SUPPORT_IMAGE_EXTENSIONS) {
            String filename = generateImageFilename(index, extension);
            UniFile file = dir.findFile(filename);
            if (file != null) {
                return file;
            }
        }
        return null;
    }

    public void setMode(@SpiderQueen.Mode int mode) {
        mMode = mode;

        if (mode == SpiderQueen.MODE_DOWNLOAD) {
            ensureDownloadDir();
        }
    }

    private boolean ensureDownloadDir() {
        return mDownloadDir != null && mDownloadDir.ensureDir();
    }

    public boolean isReady() {
        switch (mMode) {
            case SpiderQueen.MODE_READ:
                return sCache != null;
            case SpiderQueen.MODE_DOWNLOAD:
                return mDownloadDir != null && mDownloadDir.isDirectory();
            default:
                return false;
        }
    }

    @Nullable
    public UniFile getDownloadDir() {
        return mDownloadDir != null && mDownloadDir.isDirectory() ? mDownloadDir : null;
    }

    private boolean containInCache(int index) {
        if (sCache == null) {
            return false;
        }

        String key = EhCacheKeyFactory.getImageKey(mGid, index);
        return sCache.contain(key);
    }

    private boolean containInDownloadDir(int index) {
        UniFile dir = getDownloadDir();
        if (dir == null) {
            return false;
        }

        // Find image file in download dir
        return findImageFile(dir, index) != null;
    }

    /**
     * @param extension with dot
     */
    private String fixExtension(String extension) {
        if (Utilities.contain(GalleryProvider2.SUPPORT_IMAGE_EXTENSIONS, extension)) {
            return extension;
        } else {
            return GalleryProvider2.SUPPORT_IMAGE_EXTENSIONS[0];
        }
    }

    private boolean copyFromCacheToDownloadDir(int index) {
        if (sCache == null) {
            return false;
        }
        UniFile dir = getDownloadDir();
        if (dir == null) {
            return false;
        }
        // Find image file in cache
        String key = EhCacheKeyFactory.getImageKey(mGid, index);
        InputStreamPipe pipe = sCache.getInputStreamPipe(key);
        if (pipe == null) {
            return false;
        }

        OutputStream os = null;
        try {
            // Get extension
            String extension;
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            pipe.obtain();
            BitmapFactory.decodeStream(pipe.open(), null, options);
            pipe.close();
            extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(options.outMimeType);
            if (extension != null) {
                extension = '.' + extension;
            } else {
                return false;
            }
            // Fix extension
            extension = fixExtension(extension);
            // Copy from cache to download dir
            UniFile file = dir.createFile(generateImageFilename(index, extension));
            if (file == null) {
                return false;
            }
            os = file.openOutputStream();
            IOUtils.copy(pipe.open(), os);
            return true;
        } catch (IOException e) {
            return false;
        } finally {
            IOUtils.closeQuietly(os);
            pipe.close();
            pipe.release();
        }
    }

    public boolean contain(int index) {
        if (mMode == SpiderQueen.MODE_READ) {
            return containInCache(index) || containInDownloadDir(index);
        } else if (mMode == SpiderQueen.MODE_DOWNLOAD) {
            return containInDownloadDir(index) || copyFromCacheToDownloadDir(index);
        } else {
            return false;
        }
    }

    private boolean removeFromCache(int index) {
        if (sCache == null) {
            return false;
        }

        String key = EhCacheKeyFactory.getImageKey(mGid, index);
        return sCache.remove(key);
    }

    private boolean removeFromDownloadDir(int index) {
        UniFile dir = getDownloadDir();
        if (dir == null) {
            return false;
        }

        boolean result = false;
        for (int i = 0, n = GalleryProvider2.SUPPORT_IMAGE_EXTENSIONS.length; i < n; i++) {
            String filename = generateImageFilename(index, GalleryProvider2.SUPPORT_IMAGE_EXTENSIONS[i]);
            UniFile file = dir.subFile(filename);
            if (file != null) {
                result |= file.delete();
            }
        }
        return result;
    }

    public boolean remove(int index) {
        boolean result = removeFromCache(index);
        result |= removeFromDownloadDir(index);
        return result;
    }

    @Nullable
    private OutputStreamPipe openCacheOutputStreamPipe(int index) {
        if (sCache == null) {
            return null;
        }

        String key = EhCacheKeyFactory.getImageKey(mGid, index);
        return sCache.getOutputStreamPipe(key);
    }

    /**
     * @param extension without dot
     */
    @Nullable
    private OutputStreamPipe openDownloadOutputStreamPipe(int index, @Nullable String extension) {
        UniFile dir = getDownloadDir();
        if (dir == null) {
            return null;
        }

        extension = fixExtension('.' + extension);
        UniFile file = dir.createFile(generateImageFilename(index, extension));
        if (file != null) {
            return new UniFileOutputStreamPipe(file);
        } else {
            return null;
        }
    }

    @Nullable
    public OutputStreamPipe openOutputStreamPipe(int index, @Nullable String extension) {
        if (mMode == SpiderQueen.MODE_READ) {
            // Return the download pipe is the gallery has been downloaded
            OutputStreamPipe pipe = openDownloadOutputStreamPipe(index, extension);
            if (pipe == null) {
                pipe = openCacheOutputStreamPipe(index);
            }
            return pipe;
        } else if (mMode == SpiderQueen.MODE_DOWNLOAD) {
            return openDownloadOutputStreamPipe(index, extension);
        } else {
            return null;
        }
    }

    @Nullable
    private InputStreamPipe openCacheInputStreamPipe(int index) {
        if (sCache == null) {
            return null;
        }

        String key = EhCacheKeyFactory.getImageKey(mGid, index);
        return sCache.getInputStreamPipe(key);
    }

    @Nullable
    public InputStreamPipe openDownloadInputStreamPipe(int index) {
        UniFile dir = getDownloadDir();
        if (dir == null) {
            return null;
        }

        for (int i = 0; i < 2; i++) {
            UniFile file = findImageFile(dir, index);
            if (file != null) {
                return new UniFileInputStreamPipe(file);
            } else if (!copyFromCacheToDownloadDir(index)) {
                return null;
            }
        }

        return null;
    }

    @Nullable
    public InputStreamPipe openInputStreamPipe(int index) {
        if (mMode == SpiderQueen.MODE_READ) {
            InputStreamPipe pipe = openCacheInputStreamPipe(index);
            if (pipe == null) {
                pipe = openDownloadInputStreamPipe(index);
            }
            return pipe;
        } else if (mMode == SpiderQueen.MODE_DOWNLOAD) {
            return openDownloadInputStreamPipe(index);
        } else {
            return null;
        }
    }

    private static class StartWithFilenameFilter implements FilenameFilter {

        private final String mPrefix;

        public StartWithFilenameFilter(String prefix) {
            mPrefix = prefix;
        }

        @Override
        public boolean accept(UniFile dir, String filename) {
            return filename.startsWith(mPrefix);
        }
    }
}

public class CookieSignInScene extends SolidScene implements EditText.OnEditorActionListener,
        View.OnClickListener {

    /*---------------
     View life cycle
     ---------------*/
    @Nullable
    private TextInputLayout mIpbMemberIdLayout;
    @Nullable
    private TextInputLayout mIpbPassHashLayout;
    @Nullable
    private TextInputLayout mIgneousLayout;
    @Nullable
    private EditText mIpbMemberId;
    @Nullable
    private EditText mIpbPassHash;
    @Nullable
    private EditText mIgneous;
    @Nullable
    private View mOk;
    @Nullable
    private TextView mFromClipboard;

    // false for error
    private static boolean checkIpbMemberId(String id) {
        for (int i = 0, n = id.length(); i < n; i++) {
            char ch = id.charAt(i);
            if (!(ch >= '0' && ch <= '9')) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkIpbPassHash(String hash) {
        if (32 != hash.length()) {
            return false;
        }

        for (int i = 0, n = hash.length(); i < n; i++) {
            char ch = hash.charAt(i);
            if (!(ch >= '0' && ch <= '9') && !(ch >= 'a' && ch <= 'z')) {
                return false;
            }
        }
        return true;
    }

    private static Cookie newCookie(String name, String value, String domain) {
        return new Cookie.Builder().name(name).value(value)
                .domain(domain).expiresAt(Long.MAX_VALUE).build();
    }

    @Override
    public boolean needShowLeftDrawer() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.scene_cookie_sign_in, container, false);
        mIpbMemberIdLayout = (TextInputLayout) ViewUtils.$$(view, R.id.ipb_member_id_layout);
        mIpbMemberId = mIpbMemberIdLayout.getEditText();
        AssertUtils.assertNotNull(mIpbMemberId);
        mIpbPassHashLayout = (TextInputLayout) ViewUtils.$$(view, R.id.ipb_pass_hash_layout);
        mIpbPassHash = mIpbPassHashLayout.getEditText();
        AssertUtils.assertNotNull(mIpbPassHash);
        mIgneousLayout = (TextInputLayout) ViewUtils.$$(view, R.id.igneous_layout);
        mIgneous = mIgneousLayout.getEditText();
        AssertUtils.assertNotNull(mIgneous);
        mOk = ViewUtils.$$(view, R.id.ok);
        mFromClipboard = (TextView) ViewUtils.$$(view, R.id.from_clipboard);

        mFromClipboard.setPaintFlags(mFromClipboard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);

        mIpbPassHash.setOnEditorActionListener(this);

        mOk.setOnClickListener(this);
        mFromClipboard.setOnClickListener(this);

        // Try to get old version cookie info
        Context context = getContext();
        AssertUtils.assertNotNull(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences("eh_info", 0);
        String ipbMemberId = sharedPreferences.getString("ipb_member_id", null);
        String ipbPassHash = sharedPreferences.getString("ipb_pass_hash", null);
        String igneous = sharedPreferences.getString("igneous", null);
        boolean getIt = false;
        if (!TextUtils.isEmpty(ipbMemberId)) {
            mIpbMemberId.setText(ipbMemberId);
            getIt = true;
        }
        if (!TextUtils.isEmpty(ipbPassHash)) {
            mIpbPassHash.setText(ipbPassHash);
            getIt = true;
        }
        if (!TextUtils.isEmpty(igneous)) {
            mIgneous.setText(igneous);
            getIt = true;
        }
        if (getIt) {
            showTip(R.string.found_cookies, LENGTH_SHORT);
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showSoftInput(mIpbMemberId);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mIpbMemberIdLayout = null;
        mIpbPassHashLayout = null;
        mIgneousLayout = null;
        mIpbMemberId = null;
        mIpbPassHash = null;
        mIgneous = null;
    }

    @Override
    public void onClick(View v) {
        if (mOk == v) {
            enter();
        } else if (mFromClipboard == v) {
            fillCookiesFromClipboard();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (mIpbPassHash == v) {
            enter();
        }
        return true;
    }

    public void enter() {
        Context context = getContext();
        if (null == context || null == mIpbMemberIdLayout || null == mIpbPassHashLayout ||
                null == mIgneousLayout || null == mIpbMemberId || null == mIpbPassHash ||
                null == mIgneous) {
            return;
        }

        final String ipbMemberId = mIpbMemberId.getText().toString().trim();
        final String ipbPassHash = mIpbPassHash.getText().toString().trim();
        final String igneous = mIgneous.getText().toString().trim();

        if (TextUtils.isEmpty(ipbMemberId)) {
            mIpbMemberIdLayout.setError(getString(R.string.text_is_empty));
            return;
        } else {
            mIpbMemberIdLayout.setError(null);
        }
        if (TextUtils.isEmpty(ipbPassHash)) {
            mIpbPassHashLayout.setError(getString(R.string.text_is_empty));
            return;
        } else {
            mIpbPassHashLayout.setError(null);
        }

        hideSoftInput();

        if (!checkIpbMemberId(ipbMemberId) || !(checkIpbPassHash(ipbPassHash))) {
            new AlertDialog.Builder(context).setTitle(R.string.waring)
                    .setMessage(R.string.wrong_cookie_warning)
                    .setPositiveButton(R.string.i_will_check_it, null)
                    .setNegativeButton(R.string.i_dont_think_so, (dialog, which) -> {
                        storeCookie(ipbMemberId, ipbPassHash, igneous);
                        setResult(RESULT_OK, null);
                        finish();
                    }).show();
        } else {
            storeCookie(ipbMemberId, ipbPassHash, igneous);
            setResult(RESULT_OK, null);
            finish();
        }
    }

    private void storeCookie(String id, String hash, String igneous) {
        Context context = getContext();
        if (null == context) {
            return;
        }

        EhUtils.signOut(context);

        EhCookieStore store = EhApplication.getEhCookieStore(context);
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_MEMBER_ID, id, EhUrl.DOMAIN_E));
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_MEMBER_ID, id, EhUrl.DOMAIN_EX));
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_PASS_HASH, hash, EhUrl.DOMAIN_E));
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_PASS_HASH, hash, EhUrl.DOMAIN_EX));
        if (!igneous.isEmpty()) {
            store.addCookie(newCookie(EhCookieStore.KEY_IGNEOUS, igneous, EhUrl.DOMAIN_E));
            store.addCookie(newCookie(EhCookieStore.KEY_IGNEOUS, igneous, EhUrl.DOMAIN_EX));
        }
    }

    private void fillCookiesFromClipboard() {
        hideSoftInput();
        String text = ClipboardUtil.getTextFromClipboard();
        if (text == null) {
            showTip(R.string.from_clipboard_error, LENGTH_SHORT);
            return;
        }
        try {
            String[] kvs;
            if (text.contains(";")) {
                kvs = text.split(";");
            } else if (text.contains("\n")) {
                kvs = text.split("\n");
            } else {
                showTip(R.string.from_clipboard_error, LENGTH_SHORT);
                return;
            }
            if (kvs.length < 3) {
                showTip(R.string.from_clipboard_error, LENGTH_SHORT);
                return;
            }
            for (String s : kvs) {
                String[] kv;
                if (s.contains("=")) {
                    kv = s.split("=");
                } else if (s.contains(":")) {
                    kv = s.split(":");
                } else {
                    continue;
                }
                if (kv.length != 2) {
                    continue;
                }
                switch (kv[0].trim().toLowerCase()) {
                    case "ipb_member_id":
                        if (mIpbMemberId != null) {
                            mIpbMemberId.setText(kv[1].trim());
                        }
                        break;
                    case "ipb_pass_hash":
                        if (mIpbPassHash != null) {
                            mIpbPassHash.setText(kv[1].trim());
                        }
                        break;
                    case "igneous":
                        if (mIgneous != null) {
                            mIgneous.setText(kv[1].trim());
                        }
                        break;
                }
            }
            enter();
        } catch (Exception e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            showTip(R.string.from_clipboard_error, LENGTH_SHORT);
        }
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/xxe/transformerfactory-dtds-not-disabled.java

import javax.xml.transform.TransformerFactory;

class TransformerFactory {
    public void GoodTransformerFactory(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void GoodTransformerFactory2(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void GoodTransformerFactory3(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void GoodTransformerFactory4(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void BadTransformerFactory(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void BadTransformerFactory2(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void BadTransformerFactory3(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void BadTransformerFactory4(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

class TransformerFactory2 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
    }

    public TransformerFactory2(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory3 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    }

    public TransformerFactory3(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory4 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
    }

    public TransformerFactory4(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory5 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
    }

    public TransformerFactory5(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory6 {

    TransformerFactory factory = null;

    public void parserFactory(boolean condition, String xmlContent) throws ParserConfigurationException {

        if (condition) {
            factory = TransformerFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }

        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t2 = factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory7 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
    }

}

class TransformerFactory8 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
    }

}

class TransformerFactory9 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
    }

}

class TransformerFactory10 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    }

}

class TransformerFactory11 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ruleid:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute("Random", "");
    }

}

// This works only with pro-engine. I was hoping it'll work with
// `pattern-sources: - by-side-effect: true`, but it doesn't.
// todo rules
class TransformerFactory12 {

    TransformerFactory factory = null;

    public void parserFactory(boolean condition, String xmlContent) throws ParserConfigurationException {

        if (condition) {
            factory = newFactory();
            // todoruleid: java_xxe_rule-TransformerfactoryDTDNotDisabled
            Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
        } else {
            factory = newFactory();
            factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
            factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
            // todook: java_xxe_rule-TransformerfactoryDTDNotDisabled
            Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
        }
    }

    private TransformerFactory newFactory() {
        TransformerFactory factory = TransformerFactory.newInstance();
        return factory;
    }

}

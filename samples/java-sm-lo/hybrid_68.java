private class VersionViewHolder extends RecyclerView.ViewHolder {
        final TextView version;
        final TextView statusInstalled;
        final TextView statusSuggested;
        final TextView statusIncompatible;
        final TextView versionCode;
        final TextView added;
        final ImageView expandArrow;
        final View expandedLayout;
        final TextView size;
        final TextView api;
        final Button buttonInstallUpgrade;
        Button buttonAction;
        final View busyIndicator;
        final TextView incompatibleReasons;
        final TextView targetArch;

        private Apk apk;

        VersionViewHolder(View view) {
            super(view);
            version = view.findViewById(R.id.version);
            statusInstalled = view.findViewById(R.id.status_installed);
            statusSuggested = view.findViewById(R.id.status_suggested);
            statusIncompatible = view.findViewById(R.id.status_incompatible);
            versionCode = view.findViewById(R.id.versionCode);
            added = view.findViewById(R.id.added);
            expandArrow = view.findViewById(R.id.expand_arrow);
            expandedLayout = view.findViewById(R.id.expanded_layout);
            size = view.findViewById(R.id.size);
            api = view.findViewById(R.id.api);
            buttonInstallUpgrade = view.findViewById(R.id.button_install_upgrade);
            busyIndicator = view.findViewById(R.id.busy_indicator);
            incompatibleReasons = view.findViewById(R.id.incompatible_reasons);
            targetArch = view.findViewById(R.id.target_arch);

            int margin = context.getResources().getDimensionPixelSize(R.dimen.layout_horizontal_margin);
            int padding = context.getResources().getDimensionPixelSize(R.dimen.details_activity_padding);
            ViewCompat.setPaddingRelative(view, margin + padding + ViewCompat.getPaddingStart(view), view.getPaddingTop(), margin + ViewCompat.getPaddingEnd(view), view.getPaddingBottom());
        }

        void bindModel(final Apk apk) {
            if (app == null) return;
            this.apk = apk;

            boolean isAppInstalled = app.isInstalled(context);
            boolean isApkInstalled = apk.versionCode == app.installedVersionCode &&
                    TextUtils.equals(apk.signer, app.installedSigner);
            boolean isApkSuggested = apk.equals(suggestedApk);
            boolean isApkDownloading = callbacks.isAppDownloading() && downloadedApk != null &&
                    downloadedApk.compareTo(apk) == 0 &&
                    TextUtils.equals(apk.getApkPath(), downloadedApk.getApkPath());
            boolean isApkInstalledDummy = apk.versionCode == app.installedVersionCode &&
                    apk.compatible && apk.size == 0 && apk.maxSdkVersion == -1;

            // Version name and statuses
            version.setText(apk.versionName);
            statusSuggested.setVisibility(isApkSuggested && apk.compatible ? View.VISIBLE : View.GONE);
            statusInstalled.setVisibility(isApkInstalled ? View.VISIBLE : View.GONE);
            statusIncompatible.setVisibility(!apk.compatible ? View.VISIBLE : View.GONE);

            // Version name width correction in case it's
            // too long to prevent truncating the statuses
            if (statusSuggested.getVisibility() == View.VISIBLE ||
                    statusInstalled.getVisibility() == View.VISIBLE ||
                    statusIncompatible.getVisibility() == View.VISIBLE) {
                int maxWidth = (int) (Resources.getSystem().getDisplayMetrics().widthPixels * 0.4);
                version.setMaxWidth(maxWidth);
            } else {
                version.setMaxWidth(Integer.MAX_VALUE);
            }

            // Added date
            if (apk.added != null) {
                java.text.DateFormat df = DateFormat.getDateFormat(context);
                added.setVisibility(View.VISIBLE);
                added.setText(context.getString(R.string.added_on, df.format(apk.added)));
            } else {
                added.setVisibility(View.INVISIBLE);
            }

            size.setText(context.getString(R.string.app_size, Utils.getFriendlySize(apk.size)));
            api.setText(getApiText(apk));

            // Figuring out whether to show Install or Update button
            buttonInstallUpgrade.setVisibility(View.GONE);
            buttonInstallUpgrade.setText(context.getString(R.string.menu_install));
            showActionButton(buttonInstallUpgrade, isApkInstalled, isApkDownloading);
            if (isAppInstalled && !isApkInstalled) {
                if (apk.versionCode > app.installedVersionCode) {
                    // Change the label to indicate that pressing this
                    // button will result in updating the installed app
                    buttonInstallUpgrade.setText(R.string.menu_upgrade);
                } else if (apk.versionCode < app.installedVersionCode) {
                    buttonInstallUpgrade.setVisibility(View.GONE);
                }
            }

            // Show busy indicator when the APK is being downloaded
            busyIndicator.setVisibility(isApkDownloading ? View.VISIBLE : View.GONE);

            // Display when the expert mode is enabled
            if (Preferences.get().expertMode()) {
                versionCode.setText(String.format(Locale.ENGLISH, " (%d) ", apk.versionCode));
                // Display incompatible reasons when the app isn't compatible
                if (!apk.compatible) {
                    String incompatibleReasonsText = getIncompatibleReasonsText(apk);
                    if (incompatibleReasonsText != null) {
                        incompatibleReasons.setVisibility(View.VISIBLE);
                        incompatibleReasons.setText(incompatibleReasonsText);
                    } else {
                        incompatibleReasons.setVisibility(View.GONE);
                    }
                    targetArch.setVisibility(View.GONE);
                } else {
                    // Display target architecture when the app is compatible
                    String targetArchText = getTargetArchText(apk);
                    if (targetArchText != null) {
                        targetArch.setVisibility(View.VISIBLE);
                        targetArch.setText(targetArchText);
                    } else {
                        targetArch.setVisibility(View.GONE);
                    }
                    incompatibleReasons.setVisibility(View.GONE);
                }
            } else {
                versionCode.setText("");
                incompatibleReasons.setVisibility(View.GONE);
                targetArch.setVisibility(View.GONE);
            }

            // Expand the view if it was previously expanded or when downloading
            Boolean expandedVersion = versionsExpandTracker.get(apk.getApkPath());
            expand(Boolean.TRUE.equals(expandedVersion) || isApkDownloading);

            // Toggle expanded view when clicking the whole version item,
            // unless it's an installed app version dummy item - it doesn't
            // contain any meaningful info, so there is no reason to expand it.
            if (!isApkInstalledDummy) {
                expandArrow.setAlpha(1f);
                itemView.setOnClickListener(v -> toggleExpanded());
            } else {
                expandArrow.setAlpha(0.3f);
                itemView.setOnClickListener(null);
            }
            // Copy version name to clipboard when long clicking the whole version item
            if (apk.versionName != null) {
                itemView.setOnLongClickListener(v -> {
                    Utils.copyToClipboard(context, app.name, apk.versionName);
                    return true;
                });
            }
        }

        private String getApiText(final Apk apk) {
            String apiText = "Android: ";
            if (apk.minSdkVersion > 0 && apk.maxSdkVersion < Apk.SDK_VERSION_MAX_VALUE) {
                apiText += context.getString(R.string.minsdk_up_to_maxsdk,
                        Utils.getAndroidVersionName(apk.minSdkVersion),
                        Utils.getAndroidVersionName(apk.maxSdkVersion));
            } else if (apk.minSdkVersion > 0) {
                apiText += context.getString(R.string.minsdk_or_later,
                        Utils.getAndroidVersionName(apk.minSdkVersion));
            } else if (apk.maxSdkVersion > 0) {
                apiText += context.getString(R.string.up_to_maxsdk,
                        Utils.getAndroidVersionName(apk.maxSdkVersion));
            }
            return apiText;
        }

        private String getIncompatibleReasonsText(final Apk apk) {
            if (apk.incompatibleReasons != null) {
                return context.getResources().getString(R.string.requires_features,
                        TextUtils.join(", ", apk.incompatibleReasons));
            } else {
                Objects.requireNonNull(app);
                if (app.installedSigner != null
                        && !TextUtils.equals(app.installedSigner, apk.signer)) {
                    return context.getString(R.string.app_details__incompatible_mismatched_signers);
                }
            }
            return null;
        }

        private String getTargetArchText(final Apk apk) {
            if (apk.nativecode == null) {
                return null;
            }
            String currentArch = System.getProperty("os.arch");
            List<String> customArchs = new ArrayList<>();
            for (String arch : apk.nativecode) {
                // Gather only archs different than current arch
                if (!TextUtils.equals(arch, currentArch)) {
                    customArchs.add(arch);
                }
            }
            String archs = TextUtils.join(", ", customArchs);
            if (!archs.isEmpty()) {
                // Reuse "Requires: ..." string to display this
                return context.getResources().getString(R.string.requires_features, archs);
            }
            return null;
        }

        private void showActionButton(Button button, boolean isApkInstalled, boolean isApkDownloading) {
            buttonAction = button;
            if (isApkDownloading) {
                // Don't show the button in this case
                // as the busy indicator will take its place
                buttonAction.setVisibility(View.GONE);
            } else {
                // The button should be shown but it should be also disabled
                // if either the APK isn't compatible or it's already installed
                // or also when some other APK is currently being downloaded
                buttonAction.setVisibility(View.VISIBLE);
                boolean buttonActionDisabled = !apk.compatible || isApkInstalled ||
                        callbacks.isAppDownloading();
                buttonAction.setEnabled(!buttonActionDisabled);
                buttonAction.setAlpha(buttonActionDisabled ? 0.15f : 1f);
                buttonAction.setOnClickListener(v -> callbacks.installApk(apk));
            }
        }

        private void expand(boolean expand) {
            versionsExpandTracker.put(apk.getApkPath(), expand);
            expandedLayout.setVisibility(expand ? View.VISIBLE : View.GONE);
            versionCode.setVisibility(expand ? View.VISIBLE : View.GONE);
            expandArrow.setImageDrawable(ContextCompat.getDrawable(context, expand ?
                    R.drawable.ic_expand_less : R.drawable.ic_expand_more));

            // This is required to make these labels
            // auto-scrollable when they are too long
            version.setSelected(expand);
            size.setSelected(expand);
            api.setSelected(expand);
        }

        private void toggleExpanded() {
            if (busyIndicator.getVisibility() == View.VISIBLE) {
                // Don't allow collapsing the view when the busy indicator
                // is shown because the APK is being downloaded and it's quite important
                return;
            }

            boolean expand = Boolean.FALSE.equals(versionsExpandTracker.get(apk.getApkPath()));
            expand(expand);

            if (expand) {
                // Scroll the versions view to a correct position so it can show the whole item
                final LinearLayoutManager lm = (LinearLayoutManager) recyclerView.getLayoutManager();
                final int currentPosition = getBindingAdapterPosition();
                if (lm != null && currentPosition >= lm.findLastCompletelyVisibleItemPosition()) {
                    // Do it only if the item is near the bottom of current viewport
                    recyclerView.getViewTreeObserver()
                            .addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                                @Override
                                public void onGlobalLayout() {
                                    // Expanded item dimensions should be already calculated at this moment
                                    // so it's possible to correctly scroll to a given position
                                    recyclerView.smoothScrollToPosition(currentPosition);
                                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                                }
                            });
                }
            }
        }
    }

public class HeaderViewHolder extends RecyclerView.ViewHolder {
        private static final int MAX_LINES = 5;

        final ImageView iconView;
        final TextView titleView;
        final TextView authorView;
        final TextView lastUpdateView;
        final ComposeView repoChooserView;
        final TextView warningView;
        final TextView summaryView;
        final TextView whatsNewView;
        final TextView descriptionView;
        final Button descriptionMoreView;
        final View antiFeaturesSectionView;
        final TextView antiFeaturesLabelView;
        final View antiFeaturesWarningView;
        final AntiFeaturesListingView antiFeaturesListingView;
        final Button buttonPrimaryView;
        final Button buttonSecondaryView;
        final View progressLayout;
        final LinearProgressIndicator progressBar;
        final TextView progressLabel;
        final TextView progressPercent;
        final View progressCancel;
        boolean descriptionIsExpanded;

        HeaderViewHolder(View view) {
            super(view);
            iconView = view.findViewById(R.id.icon);
            titleView = view.findViewById(R.id.title);
            authorView = view.findViewById(R.id.author);
            lastUpdateView = view.findViewById(R.id.text_last_update);
            repoChooserView = view.findViewById(R.id.repoChooserView);
            repoChooserView.setViewCompositionStrategy(
                    ViewCompositionStrategy.DisposeOnViewTreeLifecycleDestroyed.INSTANCE);
            warningView = view.findViewById(R.id.warning);
            summaryView = view.findViewById(R.id.summary);
            whatsNewView = view.findViewById(R.id.latest);
            descriptionView = view.findViewById(R.id.description);
            descriptionMoreView = view.findViewById(R.id.description_more);
            antiFeaturesSectionView = view.findViewById(R.id.anti_features_section);
            antiFeaturesLabelView = view.findViewById(R.id.label_anti_features);
            antiFeaturesWarningView = view.findViewById(R.id.anti_features_warning);
            antiFeaturesListingView = view.findViewById(R.id.anti_features_full_listing);
            buttonPrimaryView = view.findViewById(R.id.primaryButtonView);
            buttonSecondaryView = view.findViewById(R.id.secondaryButtonView);
            progressLayout = view.findViewById(R.id.progress_layout);
            progressBar = view.findViewById(R.id.progress_bar);
            progressLabel = view.findViewById(R.id.progress_label);
            progressPercent = view.findViewById(R.id.progress_percent);
            progressCancel = view.findViewById(R.id.progress_cancel);
            descriptionView.setMaxLines(MAX_LINES);
            descriptionView.setEllipsize(TextUtils.TruncateAt.MARQUEE);
            descriptionMoreView.setOnClickListener(v -> {
                TransitionManager.beginDelayedTransition(recyclerView, null);
                if (TextViewCompat.getMaxLines(descriptionView) != MAX_LINES) {
                    descriptionView.setMaxLines(MAX_LINES);
                    descriptionMoreView.setText(R.string.more);
                    descriptionIsExpanded = false;
                } else {
                    descriptionView.setMaxLines(Integer.MAX_VALUE);
                    descriptionMoreView.setText(R.string.less);
                    descriptionIsExpanded = true;
                }
                updateAntiFeaturesWarning();
            });
        }

        void clearProgress() {
            progressLayout.setVisibility(View.GONE);
            buttonPrimaryView.setVisibility(versions.isEmpty() ? View.GONE : View.VISIBLE);
            buttonSecondaryView.setVisibility(app != null && app.isUninstallable(context) ?
                    View.VISIBLE : View.GONE);
        }

        void setIndeterminateProgress(int resIdString) {
            if (!progressBar.isIndeterminate()) {
                progressBar.hide();
                progressBar.setIndeterminate(true);
            }
            progressBar.show();
            progressLayout.setVisibility(View.VISIBLE);
            buttonPrimaryView.setVisibility(View.GONE);
            buttonSecondaryView.setVisibility(View.GONE);
            progressLabel.setText(resIdString);
            progressLabel.setContentDescription(context.getString(R.string.downloading));
            progressPercent.setText("");
            if (resIdString == R.string.installing || resIdString == R.string.uninstalling) {
                progressCancel.setVisibility(View.GONE);
            } else {
                progressCancel.setVisibility(View.VISIBLE);
            }
        }

        void setProgress(long bytesDownloaded, long totalBytes) {
            progressLayout.setVisibility(View.VISIBLE);
            buttonPrimaryView.setVisibility(View.GONE);
            buttonSecondaryView.setVisibility(View.GONE);
            progressCancel.setVisibility(View.VISIBLE);

            if (totalBytes <= 0) {
                if (!progressBar.isIndeterminate()) {
                    progressBar.hide();
                    progressBar.setIndeterminate(true);
                }
            } else {
                progressBar.setProgressCompat(Utils.getPercent(bytesDownloaded, totalBytes), true);
            }
            progressBar.show();

            progressLabel.setContentDescription("");
            if (totalBytes > 0 && bytesDownloaded >= 0) {
                int percent = Utils.getPercent(bytesDownloaded, totalBytes);
                progressLabel.setText(Utils.getFriendlySize(bytesDownloaded)
                        + " / " + Utils.getFriendlySize(totalBytes));
                progressLabel.setContentDescription(context.getString(R.string.app__tts__downloading_progress,
                        percent));
                progressPercent.setText(String.format(Locale.ENGLISH, "%d%%", percent));
            } else if (bytesDownloaded >= 0) {
                progressLabel.setText(Utils.getFriendlySize(bytesDownloaded));
                progressLabel.setContentDescription(context.getString(R.string.downloading));
                progressPercent.setText("");
            }
        }

        void bindModel() {
            if (app == null) return;
            Utils.setIconFromRepoOrPM(app, iconView, iconView.getContext());
            titleView.setText(app.name);
            if (!TextUtils.isEmpty(app.authorName)) {
                authorView.setText(context.getString(R.string.by_author_format, app.authorName));
                authorView.setVisibility(View.VISIBLE);
            } else {
                authorView.setVisibility(View.GONE);
            }
            if (app.lastUpdated != null) {
                Resources res = lastUpdateView.getContext().getResources();
                String lastUpdated = Utils.formatLastUpdated(res, app.lastUpdated);
                String text;
                if (Preferences.get().expertMode() && suggestedApk != null && suggestedApk.apkFile != null
                        && suggestedApk.apkFile.getSize() != null) {
                    String size = Formatter.formatFileSize(context, suggestedApk.apkFile.getSize());
                    text = lastUpdated + " (" + size + ")";
                } else {
                    text = lastUpdated;
                }
                lastUpdateView.setText(text);
                lastUpdateView.setVisibility(View.VISIBLE);
            } else {
                lastUpdateView.setVisibility(View.GONE);
            }
            if (app != null && preferredRepoId != null) {
                Set<String> defaultAddresses = Preferences.get().getDefaultRepoAddresses(context);
                Repository repo = FDroidApp.getRepoManager(context).getRepository(app.repoId);
                // show repo banner, if
                // * app is in more than one repo, or
                // * app is from a non-default repo
                if (repos.size() > 1 || (repo != null && !defaultAddresses.contains(repo.getAddress()))) {
                    RepoChooserKt.setContentRepoChooser(repoChooserView, repos, app.repoId, preferredRepoId,
                            r -> callbacks.onRepoChanged(r.getRepoId()), callbacks::onPreferredRepoChanged);
                    repoChooserView.setVisibility(View.VISIBLE);
                } else {
                    repoChooserView.setVisibility(View.GONE);
                }
            } else {
                repoChooserView.setVisibility(View.GONE);
            }

            if (suggestedApk == null && repos.size() > 1 && app.installedSigner != null && preferredRepoId != null
                    && preferredRepoId == app.repoId && !versionsLoading) {
                // current repo is preferred, app is installed, but has no suggested version from this repo
                int color = ContextCompat.getColor(context, R.color.fdroid_red);
                warningView.setBackgroundColor(color);
                warningView.setText(R.string.warning_no_compat_versions);
                warningView.setVisibility(View.VISIBLE);
            } else if (SessionInstallManager.canBeUsed(context) && suggestedApk != null
                    && !SessionInstallManager.isTargetSdkSupported(suggestedApk.targetSdkVersion)) {
                int color = ContextCompat.getColor(context, R.color.warning);
                warningView.setBackgroundColor(color);
                warningView.setText(R.string.warning_target_sdk);
                warningView.setVisibility(View.VISIBLE);
            } else {
                warningView.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(app.summary)) {
                summaryView.setText(app.summary);
                summaryView.setVisibility(View.VISIBLE);
            } else {
                summaryView.setVisibility(View.GONE);
            }
            if (suggestedApk == null || TextUtils.isEmpty(app.whatsNew)) {
                whatsNewView.setVisibility(View.GONE);
                summaryView.setBackgroundResource(0); // make background of summary transparent
            } else {
                final LocaleListCompat localeList =
                        ConfigurationCompat.getLocales(context.getResources().getConfiguration());
                Locale locale = localeList.get(0);

                StringBuilder sbWhatsNew = new StringBuilder();
                sbWhatsNew.append(whatsNewView.getContext().getString(R.string.details_new_in_version,
                        suggestedApk.versionName).toUpperCase(locale));
                sbWhatsNew.append("\n\n");
                sbWhatsNew.append(app.whatsNew);
                whatsNewView.setText(sbWhatsNew);
                whatsNewView.setVisibility(View.VISIBLE);

                // Set focus on the header section to prevent auto scrolling to
                // the changelog if its content becomes too long to fit on screen.
                recyclerView.requestChildFocus(itemView, itemView);
            }
            final Spanned desc = HtmlCompat.fromHtml(app.description, HtmlCompat.FROM_HTML_MODE_LEGACY,
                    null, new Utils.HtmlTagHandler());
            descriptionView.setMovementMethod(LinkMovementMethod.getInstance());
            descriptionView.setText(trimTrailingNewlines(desc));
            LinkifyCompat.addLinks(descriptionView, Linkify.WEB_URLS);

            if (descriptionView.getText() instanceof Spannable spannable) {
                URLSpan[] spans = spannable.getSpans(0, spannable.length(), URLSpan.class);
                for (URLSpan span : spans) {
                    int start = spannable.getSpanStart(span);
                    int end = spannable.getSpanEnd(span);
                    int flags = spannable.getSpanFlags(span);
                    spannable.removeSpan(span);
                    // Create out own safe link span
                    SafeURLSpan safeUrlSpan = new SafeURLSpan(span.getURL());
                    spannable.setSpan(safeUrlSpan, start, end, flags);
                }
            }
            descriptionView.post(() -> {
                boolean hasNoAntiFeatures = app.antiFeatures == null || app.antiFeatures.length == 0;
                if (descriptionView.getLineCount() <= HeaderViewHolder.MAX_LINES && hasNoAntiFeatures) {
                    descriptionMoreView.setVisibility(View.GONE);
                } else {
                    descriptionMoreView.setVisibility(View.VISIBLE);
                }
            });

            antiFeaturesListingView.setApp(app);
            updateAntiFeaturesWarning();

            boolean hasCompatibleVersion = false;
            for (Apk apk : versions) {
                if (apk.compatible) {
                    hasCompatibleVersion = true;
                    break;
                }
            }
            boolean showPrimaryButton = hasCompatibleVersion || app.isInstalled(context);

            buttonPrimaryView.setText(R.string.menu_install);
            buttonPrimaryView.setVisibility(showPrimaryButton ? View.VISIBLE : View.GONE);
            buttonSecondaryView.setText(R.string.menu_uninstall);
            buttonSecondaryView.setVisibility(app.isUninstallable(context) ? View.VISIBLE : View.GONE);
            buttonSecondaryView.setOnClickListener(v -> callbacks.uninstallApk());
            if (callbacks.isAppDownloading()) {
                buttonPrimaryView.setText(R.string.downloading);
                buttonPrimaryView.setEnabled(false);
                buttonPrimaryView.setVisibility(View.GONE);
                buttonSecondaryView.setVisibility(View.GONE);
                progressLayout.setVisibility(View.VISIBLE);
            } else if (!app.isInstalled(context) && suggestedApk != null) {
                // Check count > 0 due to incompatible apps resulting in an empty list.
                progressLayout.setVisibility(View.GONE);
                // Set Install button and hide second button
                buttonPrimaryView.setText(R.string.menu_install);
                buttonPrimaryView.setEnabled(true);
                buttonPrimaryView.setOnClickListener(v -> callbacks.installApk(suggestedApk));
            } else if (app.isInstalled(context)) {
                if (app.canAndWantToUpdate(suggestedApk)) {
                    buttonPrimaryView.setText(R.string.menu_upgrade);
                    buttonPrimaryView.setOnClickListener(v -> callbacks.installApk(suggestedApk));
                } else {
                    Apk mediaApk = app.getMediaApkifInstalled(context);
                    if (!context.getPackageName().equals(app.packageName) &&
                            context.getPackageManager().getLaunchIntentForPackage(app.packageName) != null) {
                        buttonPrimaryView.setText(R.string.menu_launch);
                        buttonPrimaryView.setOnClickListener(v -> callbacks.launchApk());
                    } else if (!app.isApk && mediaApk != null) {
                        final File installedFile = mediaApk.getInstalledMediaFile(context);
                        if (!installedFile.toString().startsWith(context.getApplicationInfo().dataDir)) {
                            final Intent viewIntent = new Intent(Intent.ACTION_VIEW);
                            Uri uri = FileProvider.getUriForFile(context, Installer.AUTHORITY, installedFile);
                            String mimeType = MimeTypeMap.getSingleton().getMimeTypeFromExtension(
                                    FilenameUtils.getExtension(installedFile.getName()));
                            viewIntent.setDataAndType(uri, mimeType);
                            viewIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION
                                    | Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION);
                            if (!context.getPackageManager().queryIntentActivities(viewIntent, 0).isEmpty()) {
                                buttonPrimaryView.setText(R.string.menu_open);
                                buttonPrimaryView.setOnClickListener(v -> {
                                    try {
                                        context.startActivity(viewIntent);
                                    } catch (ActivityNotFoundException e) {
                                        Log.e(TAG, "Error starting activity: ", e);
                                    }
                                });
                            } else {
                                buttonPrimaryView.setVisibility(View.GONE);
                            }
                        } else {
                            buttonPrimaryView.setVisibility(View.GONE);
                        }
                    } else {
                        buttonPrimaryView.setVisibility(View.GONE);
                    }
                }
                buttonPrimaryView.setEnabled(true);
                progressLayout.setVisibility(View.GONE);
            } else {
                progressLayout.setVisibility(View.GONE);
            }
            progressCancel.setOnClickListener(v -> callbacks.installCancel());
            if (versionsLoading) {
                progressLayout.setVisibility(View.VISIBLE);
                progressLabel.setVisibility(View.GONE);
                progressCancel.setVisibility(View.GONE);
                progressPercent.setVisibility(View.GONE);
                progressBar.setIndeterminate(true);
                progressBar.setVisibility(View.VISIBLE);
            } else {
                progressLabel.setVisibility(View.VISIBLE);
                progressCancel.setVisibility(View.VISIBLE);
                progressPercent.setVisibility(View.VISIBLE);
            }
            // Hide primary buttons when current repo is not the preferred one.
            // This requires the user to prefer the repo first, if they want to install/update from it.
            if (preferredRepoId != null && preferredRepoId != app.repoId) {
                // we don't need to worry about making it visible, because changing current repo refreshes this view
                buttonPrimaryView.setVisibility(View.GONE);
                buttonSecondaryView.setVisibility(View.GONE);
            }
        }

        private void updateAntiFeaturesWarning() {
            if (app != null && (app.antiFeatures == null || app.antiFeatures.length == 0)) {
                antiFeaturesSectionView.setVisibility(View.GONE);
            } else if (descriptionIsExpanded) {
                antiFeaturesSectionView.setVisibility(View.VISIBLE);
                antiFeaturesWarningView.setVisibility(View.GONE);
                antiFeaturesLabelView.setVisibility(View.VISIBLE);
                antiFeaturesListingView.setVisibility(View.VISIBLE);
            } else {
                antiFeaturesSectionView.setVisibility(View.VISIBLE);
                antiFeaturesWarningView.setVisibility(View.VISIBLE);
                antiFeaturesLabelView.setVisibility(View.GONE);
                antiFeaturesListingView.setVisibility(View.GONE);
            }
        }
    }

public class PanicResponderActivity extends AppCompatActivity {

    private static final String TAG = PanicResponderActivity.class.getSimpleName();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (!Panic.isTriggerIntent(intent)) {
            finish();
            return;
        }

        // received intent from panic app
        Log.i(TAG, "Received Panic Trigger...");

        final Preferences preferences = Preferences.get();

        boolean receivedTriggerFromConnectedApp = PanicResponder.receivedTriggerFromConnectedApp(this);
        final boolean runningAppUninstalls = PrivilegedInstaller.isDefault(this);

        ArrayList<String> wipeList = new ArrayList<>(preferences.getPanicWipeSet());
        preferences.setPanicWipeSet(Collections.<String>emptySet());
        preferences.setPanicTmpSelectedSet(Collections.<String>emptySet());

        if (receivedTriggerFromConnectedApp && runningAppUninstalls && wipeList.size() > 0) {

            // if this app (e.g. F-Droid) is to be deleted, do it last
            if (wipeList.contains(getPackageName())) {
                wipeList.remove(getPackageName());
                wipeList.add(getPackageName());
            }

            final Context context = this;
            final CountDownLatch latch = new CountDownLatch(1);
            final LocalBroadcastManager lbm = LocalBroadcastManager.getInstance(context);
            final String lastToUninstall = wipeList.get(wipeList.size() - 1);
            final BroadcastReceiver receiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch ((intent.getAction())) {
                        case Installer.ACTION_UNINSTALL_INTERRUPTED:
                        case Installer.ACTION_UNINSTALL_COMPLETE:
                            latch.countDown();
                            break;
                    }
                }
            };
            lbm.registerReceiver(receiver, Installer.getUninstallIntentFilter(lastToUninstall));

            for (String packageName : wipeList) {
                App app = new App();
                Apk apk = new Apk();
                app.packageName = packageName;
                apk.packageName = packageName;
                InstallerService.uninstall(context, app, apk);
            }

            // wait for apps to uninstall before triggering final responses
            new Thread() {
                @Override
                public void run() {
                    try {
                        latch.await(10, TimeUnit.MINUTES);
                    } catch (InterruptedException e) {
                        // ignored
                    }
                    lbm.unregisterReceiver(receiver);
                    if (preferences.panicResetRepos()) {
                        resetRepos(context);
                    }
                    if (preferences.panicHide()) {
                        HidingManager.hide(context);
                    }
                    if (preferences.panicExit()) {
                        exitAndClear();
                    }
                }
            }.start();
        } else if (receivedTriggerFromConnectedApp) {
            if (preferences.panicResetRepos()) {
                resetRepos(this);
            }
            // Performing destructive panic response
            if (preferences.panicHide()) {
                Log.i(TAG, "Hiding app...");
                HidingManager.hide(this);
            }
        }

        // exit and clear, if not deactivated
        if (!runningAppUninstalls && preferences.panicExit()) {
            exitAndClear();
        }
        finish();
    }

    static void resetRepos(Context context) {
        DBHelper.resetRepos(context);
    }

    private void exitAndClear() {
        ExitActivity.exitAndRemoveFromRecentApps(this);
        finishAndRemoveTask();
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/playground/r/rxTyLdl/java.lang.security.audit.xxe.documentbuilderfactory-external-general-entities-true.documentbuilderfactory-external-general-entities-true

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.jdom2.input.SAXBuilder;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

class GoodDocumentBuilderFactory {
    public void GoodDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-ExternalGeneralEntitiesTrue
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
    }
}

class BadDocumentBuilderFactory {
    public void BadDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXParserFactory {
    public void GoodSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        // ok:java_xxe_rule-ExternalGeneralEntitiesTrue
        spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
    }
}

class BadSAXParserFactory {
    public void BadSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXBuilder {
    public void GoodSAXBuilder1() throws ParserConfigurationException {
        SAXBuilder saxBuilder = new SAXBuilder();
        // ok:java_xxe_rule-ExternalGeneralEntitiesTrue
        saxBuilder.setFeature("http://xml.org/sax/features/external-general-entities", false);
    }
}

class BadSAXBuilder {
    public void BadSAXBuilder1() throws ParserConfigurationException {
        SAXBuilder saxBuilder = new SAXBuilder();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXReader {
    public void GoodSAXReader1() throws ParserConfigurationException {
        SAXReader saxReader = new SAXReader();
        // ok:java_xxe_rule-ExternalGeneralEntitiesTrue
        saxReader.setFeature("http://xml.org/sax/features/external-general-entities", false);
    }
}

class BadSAXReader {
    public void BadSAXReader1() throws ParserConfigurationException {
        SAXReader saxReader = new SAXReader();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodXMLReader {
    public void GoodXMLReader1() throws SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        SAXParser saxParser = factory.newSAXParser();
        // Obtain an XMLReader from the SAXParser
        XMLReader xmlReader = saxParser.getXMLReader();
        // ok:java_xxe_rule-ExternalGeneralEntitiesTrue
        xmlReader.setFeature("http://xml.org/sax/features/external-general-entities", false);
    }
}

class BadXMLReader {
    public void BadXMLReader1() throws ParserConfigurationException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        SAXParser saxParser = factory.newSAXParser();
        // Obtain an XMLReader from the SAXParser
        XMLReader xmlReader = saxParser.getXMLReader();
        // ruleid:java_xxe_rule-ExternalGeneralEntitiesTrue
        xmlReader.setFeature("http://xml.org/sax/features/external-general-entities", true);
    }
}

@SuppressWarnings("LineLength")
public class StartSwapView extends SwapView {
    private static final String TAG = "StartSwapView";

    public StartSwapView(Context context) {
        super(context);
    }

    public StartSwapView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public StartSwapView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public StartSwapView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    class PeopleNearbyAdapter extends ArrayAdapter<Peer> {

        PeopleNearbyAdapter(Context context) {
            super(context, 0, new ArrayList<Peer>());
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext())
                        .inflate(R.layout.swap_peer_list_item, parent, false);
            }

            Peer peer = getItem(position);
            ((TextView) convertView.findViewById(R.id.peer_name)).setText(peer.getName());
            ((ImageView) convertView.findViewById(R.id.icon))
                    .setImageDrawable(ContextCompat.getDrawable(getContext(), peer.getIcon()));

            return convertView;
        }
    }

    @Nullable /* Emulators typically don't have bluetooth adapters */
    private final BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();

    private SwitchMaterial bluetoothSwitch;
    private TextView viewBluetoothId;
    private TextView textBluetoothVisible;
    private TextView viewWifiId;
    private TextView viewWifiNetwork;
    private TextView peopleNearbyText;
    private ListView peopleNearbyList;
    private CircularProgressIndicator peopleNearbyProgress;

    private PeopleNearbyAdapter peopleNearbyAdapter;

    /**
     * Remove relevant listeners/subscriptions/etc so that they do not receive and process events
     * when this view is not in use.
     * <p>
     */
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (bluetoothSwitch != null) {
            bluetoothSwitch.setOnCheckedChangeListener(null);
        }

        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(onWifiNetworkChanged);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

        uiInitPeers();
        uiInitBluetooth();
        uiInitWifi();
        uiInitButtons();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                onWifiNetworkChanged, new IntentFilter(WifiStateChangeService.BROADCAST));
    }

    private final BroadcastReceiver onWifiNetworkChanged = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            uiUpdateWifiNetwork();
        }
    };

    private void uiInitButtons() {
        MaterialButton sendFDroidButton = findViewById(R.id.btn_send_fdroid);
        sendFDroidButton.setEllipsize(TextUtils.TruncateAt.END);
        findViewById(R.id.btn_send_fdroid).setOnClickListener(v -> getActivity().sendFDroid());
    }

    /**
     * Setup the list of nearby peers with an adapter, and hide or show it and the associated
     * message for when no peers are nearby depending on what is happening.
     *
     * @see SwapWorkflowActivity#bonjourFound
     * @see SwapWorkflowActivity#bluetoothFound
     */
    private void uiInitPeers() {

        peopleNearbyText = (TextView) findViewById(R.id.text_people_nearby);
        peopleNearbyList = (ListView) findViewById(R.id.list_people_nearby);
        peopleNearbyProgress = (CircularProgressIndicator) findViewById(R.id.searching_people_nearby);

        peopleNearbyAdapter = new PeopleNearbyAdapter(getContext());
        peopleNearbyList.setAdapter(peopleNearbyAdapter);
        for (Peer peer : getActivity().getSwapService().getActivePeers()) {
            if (peopleNearbyAdapter.getPosition(peer) == -1) {
                peopleNearbyAdapter.add(peer);
            }
        }

        peopleNearbyList.setOnItemClickListener((parent, view, position, id) -> {
            Peer peer = peopleNearbyAdapter.getItem(position);
            onPeerSelected(peer);
        });
    }

    private void uiInitBluetooth() {
        if (bluetooth != null) {

            viewBluetoothId = (TextView) findViewById(R.id.device_id_bluetooth);
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH_CONNECT) ==
                    PackageManager.PERMISSION_GRANTED) {
                viewBluetoothId.setText(bluetooth.getName());
            }
            viewBluetoothId.setVisibility(bluetooth.isEnabled() ? View.VISIBLE : View.GONE);

            textBluetoothVisible = findViewById(R.id.bluetooth_visible);

            bluetoothSwitch = (SwitchMaterial) findViewById(R.id.switch_bluetooth);
            bluetoothSwitch.setOnCheckedChangeListener(onBluetoothSwitchToggled);
            bluetoothSwitch.setChecked(SwapService.getBluetoothVisibleUserPreference());
            bluetoothSwitch.setEnabled(true);
            bluetoothSwitch.setOnCheckedChangeListener(onBluetoothSwitchToggled);
        } else {
            findViewById(R.id.bluetooth_info).setVisibility(View.GONE);
        }
    }

    private final CompoundButton.OnCheckedChangeListener onBluetoothSwitchToggled = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH_CONNECT) !=
                        PackageManager.PERMISSION_GRANTED ||
                        ContextCompat.checkSelfPermission(getContext(), Manifest.permission.BLUETOOTH_SCAN) !=
                                PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(getContext(), R.string.swap_bluetooth_permissions, Toast.LENGTH_LONG).show();
                    bluetoothSwitch.setChecked(false);
                    return;
                }
                Utils.debugLog(TAG, "Received onCheckChanged(true) for Bluetooth swap, prompting user as to whether they want to enable Bluetooth.");
                getActivity().startBluetoothSwap();
                textBluetoothVisible.setText(R.string.swap_visible_bluetooth);
                viewBluetoothId.setText(bluetooth.getName());
                viewBluetoothId.setVisibility(View.VISIBLE);
                Utils.debugLog(TAG, "Received onCheckChanged(true) for Bluetooth swap (prompting user or setup Bluetooth complete)");
                // TODO: When they deny the request for enabling bluetooth, we need to disable this switch...
            } else {
                Utils.debugLog(TAG, "Received onCheckChanged(false) for Bluetooth swap, disabling Bluetooth swap.");
                BluetoothManager.stop(getContext());
                textBluetoothVisible.setText(R.string.swap_not_visible_bluetooth);
                viewBluetoothId.setVisibility(View.GONE);
                Utils.debugLog(TAG, "Received onCheckChanged(false) for Bluetooth swap, Bluetooth swap disabled successfully.");
            }
            SwapService.putBluetoothVisibleUserPreference(isChecked);
        }
    };

    private void uiInitWifi() {

        viewWifiId = (TextView) findViewById(R.id.device_id_wifi);
        viewWifiNetwork = (TextView) findViewById(R.id.wifi_network);

        uiUpdateWifiNetwork();
    }

    private void uiUpdateWifiNetwork() {

        viewWifiId.setText(FDroidApp.ipAddressString);
        viewWifiId.setVisibility(TextUtils.isEmpty(FDroidApp.ipAddressString) ? View.GONE : View.VISIBLE);

        WifiApControl wifiAp = WifiApControl.getInstance(getActivity());
        if (wifiAp != null && wifiAp.isWifiApEnabled()) {
            WifiConfiguration config = wifiAp.getConfiguration();
            TextView textWifiVisible = findViewById(R.id.wifi_visible);
            if (textWifiVisible != null) {
                textWifiVisible.setText(R.string.swap_visible_hotspot);
            }
            Context context = getContext();
            if (config == null) {
                viewWifiNetwork.setText(context.getString(R.string.swap_active_hotspot,
                        context.getString(R.string.swap_blank_wifi_ssid)));
            } else {
                viewWifiNetwork.setText(context.getString(R.string.swap_active_hotspot, config.SSID));
            }
        } else if (TextUtils.isEmpty(FDroidApp.ssid)) {
            // not connected to or setup with any wifi network
            viewWifiNetwork.setText(R.string.swap_no_wifi_network);
        } else {
            // connected to a regular wifi network
            viewWifiNetwork.setText(FDroidApp.ssid);
        }
    }

    private void onPeerSelected(Peer peer) {
        getActivity().swapWith(peer);
    }
}

@SuppressLint("SimpleDateFormat")
public final class ReadableTime {

    public static final long SECOND_MILLIS = 1000;
    public static final long MINUTE_MILLIS = 60 * SECOND_MILLIS;
    public static final long HOUR_MILLIS = 60 * MINUTE_MILLIS;
    public static final long DAY_MILLIS = 24 * HOUR_MILLIS;
    public static final long WEEK_MILLIS = 7 * DAY_MILLIS;
    public static final long YEAR_MILLIS = 365 * DAY_MILLIS;
    public static final int SIZE = 5;
    public static final long[] MULTIPLES = {
            YEAR_MILLIS,
            DAY_MILLIS,
            HOUR_MILLIS,
            MINUTE_MILLIS,
            SECOND_MILLIS
    };
    public static final int[] UNITS = {
            R.plurals.year,
            R.plurals.day,
            R.plurals.hour,
            R.plurals.minute,
            R.plurals.second
    };
    private static final Calendar sCalendar = Calendar.getInstance();
    private static final Object sCalendarLock = new Object();
    private static final SimpleDateFormat DATE_FORMAT_WITHOUT_YEAR = new SimpleDateFormat("MMM d");
    private static final SimpleDateFormat DATE_FORMAT_WITH_YEAR = new SimpleDateFormat("MMM d, yyyy");
    private static final SimpleDateFormat DATE_FORMAT_WITHOUT_YEAR_ZH = new SimpleDateFormat("M月d日");
    private static final SimpleDateFormat DATE_FORMAT_WITH_YEAR_ZH = new SimpleDateFormat("yyyy年M月d日");
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yy-MM-dd HH:mm");
    private static final Object sDateFormatLock1 = new Object();
    private static final SimpleDateFormat FILENAMABLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
    private static final Object sDateFormatLock2 = new Object();
    private static Resources sResources;

    public static void initialize(Context context) {
        sResources = context.getApplicationContext().getResources();
    }

    /*
    public static String getDisplayTime(long time) {
        if (Settings.getPrettyTime()) {
            return getTimeAgo(time);
        } else {
            return getPlainTime(time);
        }
    }
    */

    public static String getPlainTime(long time) {
        synchronized (sDateFormatLock1) {
            return DATE_FORMAT.format(new Date(time));
        }
    }

    public static String getTimeAgo(long time) {
        Resources resources = sResources;

        long now = System.currentTimeMillis();
        if (time > now + (2 * MINUTE_MILLIS) || time <= 0) {
            return resources.getString(R.string.from_the_future);
        }

        final long diff = now - time;
        if (diff < MINUTE_MILLIS) {
            return resources.getString(R.string.just_now);
        } else if (diff < 2 * MINUTE_MILLIS) {
            return resources.getQuantityString(R.plurals.some_minutes_ago, 1, 1);
        } else if (diff < 50 * MINUTE_MILLIS) {
            int minutes = (int) (diff / MINUTE_MILLIS);
            return resources.getQuantityString(R.plurals.some_minutes_ago, minutes, minutes);
        } else if (diff < 90 * MINUTE_MILLIS) {
            return resources.getQuantityString(R.plurals.some_hours_ago, 1, 1);
        } else if (diff < 24 * HOUR_MILLIS) {
            int hours = (int) (diff / HOUR_MILLIS);
            return resources.getQuantityString(R.plurals.some_hours_ago, hours, hours);
        } else if (diff < 48 * HOUR_MILLIS) {
            return resources.getString(R.string.yesterday);
        } else if (diff < WEEK_MILLIS) {
            int days = (int) (diff / DAY_MILLIS);
            return resources.getString(R.string.some_days_ago, days);
        } else {
            synchronized (sCalendarLock) {
                Date nowDate = new Date(now);
                Date timeDate = new Date(time);
                sCalendar.setTime(nowDate);
                int nowYear = sCalendar.get(Calendar.YEAR);
                sCalendar.setTime(timeDate);
                int timeYear = sCalendar.get(Calendar.YEAR);
                boolean isZh = Locale.getDefault().getLanguage().equals("zh");

                if (nowYear == timeYear) {
                    return (isZh ? DATE_FORMAT_WITHOUT_YEAR_ZH : DATE_FORMAT_WITHOUT_YEAR).format(timeDate);
                } else {
                    return (isZh ? DATE_FORMAT_WITH_YEAR_ZH : DATE_FORMAT_WITH_YEAR).format(timeDate);
                }
            }
        }
    }

    public static String getTimeInterval(long time) {
        StringBuilder sb = new StringBuilder();
        Resources resources = sResources;

        long leftover = time;
        boolean start = false;

        for (int i = 0; i < SIZE; i++) {
            long multiple = MULTIPLES[i];
            long quotient = leftover / multiple;
            long remainder = leftover % multiple;
            if (start || quotient != 0 || i == SIZE - 1) {
                if (start) {
                    sb.append(" ");
                }
                sb.append(quotient)
                        .append(" ")
                        .append(resources.getQuantityString(UNITS[i], (int) quotient));
                start = true;
            }
            leftover = remainder;
        }

        return sb.toString();
    }

    public static String getShortTimeInterval(long time) {
        StringBuilder sb = new StringBuilder();
        Resources resources = sResources;

        for (int i = 0; i < SIZE; i++) {
            long multiple = MULTIPLES[i];
            long quotient = time / multiple;
            if (time > multiple * 1.5 || i == SIZE - 1) {
                sb.append(quotient)
                        .append(" ")
                        .append(resources.getQuantityString(UNITS[i], (int) quotient));
                break;
            }
        }

        return sb.toString();
    }

    public static String getFilenamableTime(long time) {
        synchronized (sDateFormatLock2) {
            return FILENAMABLE_DATE_FORMAT.format(new Date(time));
        }
    }
}

class Pipe {

    private final int capacity;
    private final byte[] buffer;

    private int head = 0;
    private int tail = 0;
    private boolean full = false;

    private boolean inClosed = false;
    private boolean outClosed = false;

    private final InputStream inputStream = new InputStream() {
        @Override
        public int read() throws IOException {
            synchronized (Pipe.this) {
                byte[] bytes = new byte[1];
                if (read(bytes, 0, 1) != -1) {
                    return bytes[0];
                } else {
                    return -1;
                }
            }
        }

        @Override
        public int read(@NonNull byte[] b, int off, int len) throws IOException {
            synchronized (Pipe.this) {
                for (; ; ) {
                    if (inClosed) {
                        throw new IOException("The InputStream is closed");
                    }
                    if (len == 0) {
                        return 0;
                    }

                    if (head == tail && !full) {
                        if (outClosed) {
                            // No bytes available and the OutputStream is closed. So it's the end.
                            return -1;
                        } else {
                            // Wait for OutputStream write bytes
                            try {
                                Pipe.this.wait();
                            } catch (InterruptedException e) {
                                throw new IOException("The thread interrupted", e);
                            }
                        }
                    } else {
                        int read = Math.min(len, (head < tail ? tail : capacity) - head);
                        System.arraycopy(buffer, head, b, off, read);
                        head += read;
                        if (head == capacity) {
                            head = 0;
                        }
                        full = false;
                        Pipe.this.notifyAll();
                        return read;
                    }
                }
            }
        }

        @Override
        public void close() {
            synchronized (Pipe.this) {
                inClosed = true;
                Pipe.this.notifyAll();
            }
        }
    };

    private final OutputStream outputStream = new OutputStream() {
        @Override
        public void write(int b) throws IOException {
            synchronized (Pipe.this) {
                byte[] bytes = new byte[]{(byte) b};
                write(bytes, 0, 1);
            }
        }

        @Override
        public void write(@NonNull byte[] b, int off, int len) throws IOException {
            synchronized (Pipe.this) {
                while (len != 0) {
                    if (outClosed) {
                        throw new IOException("The OutputStream is closed");
                    }
                    if (inClosed) {
                        throw new IOException("The InputStream is closed");
                    }

                    if (head == tail && full) {
                        // The buffer is full, wait for InputStream read bytes
                        try {
                            Pipe.this.wait();
                        } catch (InterruptedException e) {
                            throw new IOException("The thread interrupted", e);
                        }
                    } else {
                        int write = Math.min(len, (head <= tail ? capacity : head) - tail);
                        System.arraycopy(b, off, buffer, tail, write);
                        off += write;
                        len -= write;
                        tail += write;
                        if (tail == capacity) {
                            tail = 0;
                        }
                        if (head == tail) {
                            full = true;
                        }
                        Pipe.this.notifyAll();
                    }
                }
            }
        }

        @Override
        public void close() {
            synchronized (Pipe.this) {
                outClosed = true;
                Pipe.this.notifyAll();
            }
        }
    };

    Pipe(int capacity) {
        this.capacity = capacity;
        this.buffer = new byte[capacity];
    }

    InputStream getInputStream() {
        return inputStream;
    }

    OutputStream getOutputStream() {
        return outputStream;
    }
}

public class FDroidMetricsWorker extends Worker {

    public static final String TAG = "FDroidMetricsWorker";

    static SimpleDateFormat weekFormatter = new SimpleDateFormat("yyyy ww", Locale.ENGLISH);

    private static final ArrayList<MatomoEvent> EVENTS = new ArrayList<>();

    public FDroidMetricsWorker(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    /**
     * Schedule or cancel a work request to update the app index, according to the
     * current preferences.  It is meant to run weekly, so it will schedule one week
     * from the last run.  If it has never been run, it will run as soon as possible.
     * <p>
     * Although {@link Constraints.Builder#setRequiresDeviceIdle(boolean)} is available
     * down to {@link Build.VERSION_CODES#M}, it will cause {@code UpdateService} to
     * rarely run, if ever on some devices.  So {@link Constraints.Builder#setRequiresDeviceIdle(boolean)}
     * should only be used in conjunction with
     * {@link Constraints.Builder#setTriggerContentMaxDelay(long, TimeUnit)} to ensure
     * that updates actually happen regularly.
     */
    public static void schedule(final Context context) {
        final WorkManager workManager = WorkManager.getInstance(context);
        long interval = TimeUnit.DAYS.toMillis(7);

        final Constraints.Builder constraintsBuilder = new Constraints.Builder()
                .setRequiresCharging(true)
                .setRequiresBatteryNotLow(true);
        // TODO use the Data/WiFi preferences here
        if (Build.VERSION.SDK_INT >= 24) {
            constraintsBuilder.setTriggerContentMaxDelay(interval, TimeUnit.MILLISECONDS);
            constraintsBuilder.setRequiresDeviceIdle(true);
        }
        final PeriodicWorkRequest cleanCache =
                new PeriodicWorkRequest.Builder(FDroidMetricsWorker.class, interval, TimeUnit.MILLISECONDS)
                        .setConstraints(constraintsBuilder.build())
                        .build();
        workManager.enqueueUniquePeriodicWork(TAG, ExistingPeriodicWorkPolicy.REPLACE, cleanCache);
        Utils.debugLog(TAG, "Scheduled periodic work");
    }

    public static void cancel(final Context context) {
        WorkManager.getInstance(context).cancelUniqueWork(TAG);
    }

    @NonNull
    @Override
    public Result doWork() {
        // TODO check useTor preference and force-submit over Tor.
        String json = generateReport(getApplicationContext());
        try {
            if (json != null) {
                HttpPoster httpPoster =
                        new HttpPoster(DownloaderFactory.HTTP_MANAGER, "https://metrics.cleaninsights.org/cleaninsights.php");
                httpPoster.post(json);
            }
            return ListenableWorker.Result.success();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ListenableWorker.Result.retry();
    }

    /**
     * Convert a Java timestamp in milliseconds to a CleanInsights/Matomo timestamp
     * normalized to the week and in UNIX epoch seconds format.
     */
    static long toCleanInsightsTimestamp(long timestamp) {
        return toCleanInsightsTimestamp(timestamp, timestamp);
    }

    /**
     * Convert a Java timestamp in milliseconds to a CleanInsights/Matomo timestamp
     * normalized to the week and in UNIX epoch seconds format, plus the time
     * difference between {@code relativeTo} and {@code timestamp}.
     */
    static long toCleanInsightsTimestamp(long relativeTo, long timestamp) {
        long diff = timestamp - relativeTo;
        long weekNumber = timestamp / DateUtils.WEEK_IN_MILLIS;
        return ((weekNumber * DateUtils.WEEK_IN_MILLIS) + diff) / 1000L;
    }

    static boolean isTimestampInReportingWeek(long weekStart, long timestamp) {
        long weekEnd = weekStart + DateUtils.WEEK_IN_MILLIS;
        return weekStart < timestamp && timestamp < weekEnd;
    }

    /**
     * Gets the most recent week that is over based on the current time.
     *
     * @return start timestamp or 0 on parsing error
     */
    static long getReportingWeekStart() {
        return getReportingWeekStart(System.currentTimeMillis());
    }

    /**
     * Gets the most recent week that is over based on {@code timestamp}. This
     * is the testable version of {@link #getReportingWeekStart()}
     *
     * @return start timestamp or 0 on parsing error
     */
    static long getReportingWeekStart(long timestamp) {
        try {
            Date start = new Date(timestamp - DateUtils.WEEK_IN_MILLIS);
            return weekFormatter.parse(weekFormatter.format(start)).getTime();
        } catch (ParseException e) {
            // ignored
        }
        return 0;
    }

    /**
     * Reads the {@link InstallHistoryService} CSV log, debounces the duplicate events,
     * then converts it to {@link MatomoEvent} instances to be gathered.
     */
    static Collection<? extends MatomoEvent> parseInstallHistoryCsv(Context context, long weekStart) {
        try {
            File csv = InstallHistoryService.getInstallHistoryFile(context);
            List<String> lines = FileUtils.readLines(csv, Charset.defaultCharset());
            List<RawEvent> events = new ArrayList<>(lines.size());
            for (String line : lines) {
                RawEvent event = new RawEvent(line.split(","));
                if (isTimestampInReportingWeek(weekStart, event.timestamp)) {
                    events.add(event);
                }
            }
            Collections.sort(events, (e0, e1) -> {
                int applicationIdComparison = e0.applicationId.compareTo(e1.applicationId);
                if (applicationIdComparison != 0) {
                    return applicationIdComparison;
                }
                int versionCodeComparison = Long.compare(e0.versionCode, e1.versionCode);
                if (versionCodeComparison != 0) {
                    return versionCodeComparison;
                }
                return Long.compare(e0.timestamp, e1.timestamp);
            });
            List<MatomoEvent> toReport = new ArrayList<>();
            RawEvent previousEvent = new RawEvent(new String[]{"0", "", "0", ""});
            for (RawEvent event : events) {
                if (!previousEvent.equals(event)) {
                    toReport.add(new MatomoEvent(event));
                    previousEvent = event;
                }
            }
            // TODO add time to INSTALL_COMPLETE events, eg INSTALL_COMPLETE - INSTALL_STARTED
            return toReport;
        } catch (IOException e) {
            // ignored
        }
        return Collections.emptyList();
    }

    public static String generateReport(Context context) {
        long weekStart = getReportingWeekStart();
        CleanInsightsReport cleanInsightsReport = new CleanInsightsReport();
        PackageManager pm = context.getPackageManager();
        List<PackageInfo> packageInfoList = pm.getInstalledPackages(0);
        Collections.sort(packageInfoList, (p1, p2) -> p1.packageName.compareTo(p2.packageName));
        EVENTS.add(getDeviceEvent(weekStart, "isPrivilegedInstallerEnabled",
                Preferences.get().isPrivilegedInstallerEnabled()));
        EVENTS.add(getDeviceEvent(weekStart, "Build.VERSION.SDK_INT", Build.VERSION.SDK_INT));
        EVENTS.add(getDeviceEvent(weekStart, "Build.SUPPORTED_ABIS", Arrays.toString(Build.SUPPORTED_ABIS)));

        for (PackageInfo packageInfo : packageInfoList) {
            if (isTimestampInReportingWeek(weekStart, packageInfo.firstInstallTime)) {
                addFirstInstallEvent(pm, packageInfo);
            }
            if (isTimestampInReportingWeek(weekStart, packageInfo.lastUpdateTime)) {
                addLastUpdateTimeEvent(pm, packageInfo);
            }
        }
        EVENTS.addAll(parseInstallHistoryCsv(context, weekStart));
        cleanInsightsReport.events = EVENTS.toArray(new MatomoEvent[0]);

        try {
            return cleanInsightsReport.getJsonString();
        } catch (JSONException e) {
            Log.e(TAG, "Error getting json string", e);
        }
        return null;
    }

    /**
     * Bare minimum report data in CleanInsights/Matomo format.
     *
     * @see MatomoEvent
     * @see <a href="https://gitlab.com/cleaninsights/clean-insights-matomo-proxy#api">CleanInsights CIMP API</a>
     * @see <a href="https://matomo.org/docs/event-tracking/">Matomo Event Tracking</a>
     */
    private static class CleanInsightsReport {
        MatomoEvent[] events = new MatomoEvent[0];
        final long idsite = 3; // NOPMD
        final String lang = LocaleCompat.getDefault().getLanguage();
        final String ua = Utils.getUserAgent();

        private String getJsonString() throws JSONException {
            JSONObject json = new JSONObject();
            JSONArray array = new JSONArray();
            for (MatomoEvent event : events) {
                array.put(event.getJSONObject());
            }
            json.put("events", array);
            json.put("idsite", idsite);
            json.put("lang", lang);
            json.put("ua", ua);
            return json.toString(2);
        }
    }

    private static void addFirstInstallEvent(PackageManager pm, PackageInfo packageInfo) {
        addInstallerEvent(pm, packageInfo, "PackageInfo.firstInstall", packageInfo.firstInstallTime);
    }

    private static void addLastUpdateTimeEvent(PackageManager pm, PackageInfo packageInfo) {
        addInstallerEvent(pm, packageInfo, "PackageInfo.lastUpdateTime", packageInfo.lastUpdateTime);
    }

    private static void addInstallerEvent(
            PackageManager pm, PackageInfo packageInfo, String action, long timestamp) {
        MatomoEvent matomoEvent = new MatomoEvent(timestamp);
        matomoEvent.category = "APK";
        matomoEvent.action = action;
        matomoEvent.name = pm.getInstallerPackageName(packageInfo.packageName);
        matomoEvent.times = 1;
        for (MatomoEvent me : EVENTS) {
            if (me.equals(matomoEvent)) {
                me.times++;
                return;
            }
        }
        EVENTS.add(matomoEvent);
    }

    /**
     * Events which describe the device that is doing the reporting.
     */
    private static MatomoEvent getDeviceEvent(long startTime, String action, Object name) {
        MatomoEvent matomoEvent = new MatomoEvent(startTime);
        matomoEvent.category = "device";
        matomoEvent.action = action;
        matomoEvent.name = String.valueOf(name);
        matomoEvent.times = 1;
        return matomoEvent;
    }

    /**
     * An event to send to CleanInsights/Matomo with a period of a full,
     * normalized week.
     *
     * @see <a href="https://gitlab.com/cleaninsights/clean-insights-design/-/blob/d4f96ae3/schemas/cimp.schema.json">CleanInsights JSON Schema</a>
     * @see <a href="https://matomo.org/docs/event-tracking/">Matomo Event Tracking</a>
     */
    @SuppressWarnings("checkstyle:MemberName")
    static class MatomoEvent {
        String category;
        String action;
        String name;
        final long period_start;
        final long period_end;
        long times = 0;

        MatomoEvent(long timestamp) {
            period_end = toCleanInsightsTimestamp(timestamp);
            period_start = period_end - (DateUtils.WEEK_IN_MILLIS / 1000);
        }

        MatomoEvent(RawEvent rawEvent) {
            this(rawEvent.timestamp);
            category = "package";
            action = rawEvent.action;
            name = rawEvent.applicationId;
            times = 1;
        }

        private JSONObject getJSONObject() throws JSONException {
            JSONObject json = new JSONObject();
            json.put("category", category);
            json.put("action", action);
            json.put("name", name);
            json.put("period_start", period_start);
            json.put("period_end", period_end);
            json.put("times", times);
            return json;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            MatomoEvent that = (MatomoEvent) o;
            return period_start == that.period_start &&
                    period_end == that.period_end &&
                    TextUtils.equals(category, that.category) &&
                    TextUtils.equals(action, that.action) &&
                    TextUtils.equals(name, that.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(category, action, name, period_start, period_end, times);
        }
    }

    /**
     * A raw event as read from {@link InstallHistoryService}'s CSV log file.
     * This should never leave the device as is, it must have data stripped
     * from it first.
     */
    static class RawEvent {
        final long timestamp;
        final String applicationId;
        final long versionCode;
        final String action;

        RawEvent(String[] o) {
            timestamp = Long.parseLong(o[0]);
            applicationId = o[1];
            versionCode = Long.parseLong(o[2]);
            action = o[3];
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            RawEvent event = (RawEvent) o;
            return versionCode == event.versionCode &&
                    applicationId.equals(event.applicationId) &&
                    action.equals(event.action);
        }

        @Override
        public int hashCode() {
            return Objects.hash(applicationId, versionCode, action);
        }

        @Override
        public String toString() {
            return "RawEvent{" +
                    "timestamp=" + timestamp +
                    ", applicationId='" + applicationId + '\'' +
                    ", versionCode=" + versionCode +
                    ", action='" + action + '\'' +
                    '}';
        }
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

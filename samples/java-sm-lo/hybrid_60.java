@SuppressWarnings({"rawtypes", "deprecation"})
public final class ConacoTask<V> {

    private static final String TAG = ConacoTask.class.getSimpleName();

    private final int mId;
    private final WeakReference<Unikery<V>> mUnikeryWeakReference;
    private final String mKey;
    private final String mUrl;
    private final DataContainer mDataContainer;
    private final boolean mUseMemoryCache;
    private final boolean mUseDiskCache;
    private final boolean mUseNetwork;
    private final ValueHelper<V> mHelper;
    private final ValueCache<V> mCache;
    private final OkHttpClient mOkHttpClient;
    private final Executor mDiskExecutor;
    private final Executor mNetworkExecutor;
    private final Conaco<V> mConaco;

    private DiskLoadTask mDiskLoadTask;
    private NetworkLoadTask mNetworkLoadTask;
    private Call mCall;
    private boolean mStart;
    private volatile boolean mStop;

    private ConacoTask(Builder<V> builder) {
        mId = builder.mId;
        mUnikeryWeakReference = new WeakReference<>(builder.mUnikery);
        mKey = builder.mKey;
        mUrl = builder.mUrl;
        mDataContainer = builder.mDataContainer;
        mUseMemoryCache = builder.mUseMemoryCache;
        mUseDiskCache = builder.mUseDiskCache;
        mUseNetwork = builder.mUseNetwork;
        mHelper = builder.mHelper;
        mCache = builder.mCache;
        mOkHttpClient = builder.mOkHttpClient;
        mDiskExecutor = builder.mDiskExecutor;
        mNetworkExecutor = builder.mNetworkExecutor;
        mConaco = builder.mConaco;
    }

    int getId() {
        return mId;
    }

    String getKey() {
        return mKey;
    }

    boolean useMemoryCache() {
        return mUseMemoryCache;
    }

    @Nullable
    Unikery<V> getUnikery() {
        return mUnikeryWeakReference.get();
    }

    void clearUnikery() {
        mUnikeryWeakReference.clear();
    }

    private void onFinish() {
        if (!mStop) {
            mConaco.finishConacoTask(this);
        }/* else  {
            // It is done by Conaco
        }*/
    }

    @UiThread
    void start() {
        if (mStop || mStart) {
            return;
        }

        mStart = true;

        Unikery unikery = mUnikeryWeakReference.get();
        if (unikery != null && unikery.getTaskId() == mId) {
            if (mUseDiskCache) {
                mDiskLoadTask = new DiskLoadTask();
                mDiskLoadTask.executeOnExecutor(mDiskExecutor);
                return;
            } else if (mUseNetwork) {
                unikery.onMiss(Conaco.SOURCE_DISK);
                unikery.onRequest();
                mNetworkLoadTask = new NetworkLoadTask();
                mNetworkLoadTask.executeOnExecutor(mNetworkExecutor);
                return;
            } else {
                unikery.onMiss(Conaco.SOURCE_DISK);
                unikery.onMiss(Conaco.SOURCE_NETWORK);
                unikery.onFailure();
            }
        }

        onFinish();
    }

    @UiThread
    void stop() {
        if (mStop) {
            return;
        }

        mStop = true;

        // Stop jobs
        if (mDiskLoadTask != null) { // Getting from disk
            mDiskLoadTask.cancel(false);
        } else if (mNetworkLoadTask != null) { // Getting from network
            mNetworkLoadTask.cancel(false);
            if (mCall != null) {
                mCall.cancel();
                mCall = null;
            }
        }

        Unikery unikery = mUnikeryWeakReference.get();
        if (unikery != null) {
            unikery.onCancel();
        }

        // Conaco handle the clean up
    }

    private boolean isNotNecessary(AsyncTask asyncTask) {
        Unikery unikery = mUnikeryWeakReference.get();
        return mStop || asyncTask.isCancelled() || unikery == null || unikery.getTaskId() != mId;
    }

    private static void putFromDiskCacheToDataContainer(String key, ValueCache cache, DataContainer container) {
        SimpleDiskCache diskCache = cache.getDiskCache();
        if (diskCache != null) {
            InputStreamPipe pipe = diskCache.getInputStreamPipe(key);
            if (pipe != null) {
                try {
                    pipe.obtain();
                    container.save(pipe.open(), -1L, null, null);
                } catch (IOException e) {
                    Log.d(TAG, "Can't save value from disk cache to data container");
                    e.printStackTrace();
                    container.remove();
                } finally {
                    pipe.close();
                    pipe.release();
                }
            }
        }
    }

    private static void putFromDataContainerToDiskCache(String key, ValueCache cache, DataContainer container) {
        InputStreamPipe pipe = container.get();
        if (pipe != null) {
            try {
                pipe.obtain();
                cache.putRawToDisk(key, pipe.open());
            } catch (IOException e) {
                Log.w(TAG, "Can't save value from data container to disk cache", e);
                cache.removeFromDisk(key);
            } finally {
                pipe.close();
                pipe.release();
            }
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class DiskLoadTask extends AsyncTask<Void, Void, V> {

        @Override
        protected V doInBackground(Void... params) {
            if (isNotNecessary(this)) {
                return null;
            } else {
                V value = null;

                // First check data container
                if (mDataContainer != null && mDataContainer.isEnabled()) {
                    InputStreamPipe isp = mDataContainer.get();
                    if (isp != null) {
                        value = mHelper.decode(isp);
                    }
                }

                // Then check disk cache
                if (mKey != null) {
                    if (value == null && mUseDiskCache) {
                        value = mCache.getFromDisk(mKey);
                        // Put back to data container
                        if (value != null && mDataContainer != null && mDataContainer.isEnabled()) {
                            putFromDiskCacheToDataContainer(mKey, mCache, mDataContainer);
                        }
                    }

                    if (value != null && mUseMemoryCache && mHelper.useMemoryCache(mKey, value)) {
                        // Put it to memory
                        mCache.putToMemory(mKey, value);
                    }
                }

                return value;
            }
        }

        @Override
        protected void onPostExecute(V value) {
            mDiskLoadTask = null;

            if (isCancelled() || mStop) {
                onCancelled(value);
            } else {
                Unikery<V> unikery = mUnikeryWeakReference.get();
                if (unikery != null && unikery.getTaskId() == mId) {
                    boolean getValue = false;
                    if ((value == null || !(getValue = unikery.onGetValue(value, Conaco.SOURCE_DISK))) && mUseNetwork) {
                        unikery.onMiss(Conaco.SOURCE_DISK);
                        unikery.onRequest();
                        mNetworkLoadTask = new NetworkLoadTask();
                        mNetworkLoadTask.executeOnExecutor(mNetworkExecutor);
                        return;
                    } else if (!getValue) {
                        unikery.onMiss(Conaco.SOURCE_DISK);
                        unikery.onFailure();
                    }
                }
                onFinish();
            }
        }

        @Override
        protected void onCancelled(V holder) {
            onFinish();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class NetworkLoadTask extends AsyncTask<Void, Long, V> implements ProgressNotifier {

        @Override
        public void notifyProgress(long singleReceivedSize, long receivedSize, long totalSize) {
            if (!isNotNecessary(this)) {
                publishProgress(singleReceivedSize, receivedSize, totalSize);
            }
        }

        private boolean putToDiskCache(InputStream is, long length) {
            SimpleDiskCache diskCache = mCache.getDiskCache();
            if (diskCache == null) {
                return false;
            }

            OutputStreamPipe pipe = diskCache.getOutputStreamPipe(mKey);
            try {
                pipe.obtain();
                OutputStream os = pipe.open();

                final byte[] buffer = new byte[1024 * 4];
                long receivedSize = 0;
                int bytesRead;

                while ((bytesRead = is.read(buffer)) != -1) {
                    os.write(buffer, 0, bytesRead);
                    receivedSize += bytesRead;
                    notifyProgress(bytesRead, receivedSize, length);
                }

                return true;
            } catch (IOException e) {
                return false;
            } finally {
                pipe.close();
                pipe.release();
            }
        }

        private boolean putToDataContainer(InputStream is, ResponseBody body) {
            // Get media type
            String mediaType;
            MediaType mt = body.contentType();
            if (mt != null) {
                mediaType = mt.type() + '/' + mt.subtype();
            } else {
                mediaType = null;
            }
            return mDataContainer.save(is, body.contentLength(), mediaType, this);
        }

        @Override
        protected V doInBackground(Void... params) {
            if (isNotNecessary(this)) {
                return null;
            }

            V value;
            InputStream is = null;
            try {
                Log.d("TAG", "Conaco " + mUrl);

                // Load it from internet
                Request request = new ChromeRequestBuilder(mUrl).build();
                mCall = mOkHttpClient.newCall(request);

                Response response = mCall.execute();
                ResponseBody body = response.body();
                if (body == null) {
                    return null;
                }
                is = body.byteStream();

                if (isNotNecessary(this)) {
                    return null;
                }

                if ((mDataContainer == null || !mDataContainer.isEnabled()) && mKey != null) {
                    if (putToDiskCache(is, body.contentLength())) {
                        // Get object from disk cache
                        value = mCache.getFromDisk(mKey);
                        if (value == null) {
                            // Maybe bad download, remove it from disk cache
                            mCache.removeFromDisk(mKey);
                        } else if (mUseMemoryCache && mHelper.useMemoryCache(mKey, value)) {
                            // Put it to memory
                            mCache.putToMemory(mKey, value);
                        }
                        return value;
                    } else {
                        // Maybe bad download, remove it from disk cache
                        mCache.removeFromDisk(mKey);
                        return null;
                    }
                } else if (mDataContainer != null && mDataContainer.isEnabled()) {
                    // Check url Moved
                    HttpUrl requestHttpUrl = request.url();
                    HttpUrl responseHttpUrl = response.request().url();
                    if (!responseHttpUrl.equals(requestHttpUrl)) {
                        mDataContainer.onUrlMoved(mUrl, responseHttpUrl.url().toString());
                    }

                    // Put to data container
                    if (!putToDataContainer(is, body)) {
                        return null;
                    }

                    // Get value from data container
                    InputStreamPipe isp = mDataContainer.get();
                    if (isp == null) {
                        return null;
                    }
                    value = mHelper.decode(isp);
                    if (value == null) {
                        mDataContainer.remove();
                    } else if (mKey != null) {
                        // Put to disk cache
                        putFromDataContainerToDiskCache(mKey, mCache, mDataContainer);

                        if (mUseMemoryCache && mHelper.useMemoryCache(mKey, value)) {
                            // Put it to memory
                            mCache.putToMemory(mKey, value);
                        }
                    }
                    return value;
                } else {
                    return null;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                mCall = null;
                try {
                    if (is != null) {
                        is.close();
                    }
                } catch (IOException e) {
                    // Ignore
                }
            }
        }

        @Override
        protected void onPostExecute(V holder) {
            mNetworkLoadTask = null;

            if (isCancelled() || mStop) {
                onCancelled(holder);
            } else {
                Unikery<V> unikery = mUnikeryWeakReference.get();
                if (unikery != null && unikery.getTaskId() == mId) {
                    if (holder == null || !unikery.onGetValue(holder, Conaco.SOURCE_NETWORK)) {
                        unikery.onFailure();
                    }
                }
                onFinish();
            }
        }

        @Override
        protected void onCancelled(V value) {
            onFinish();
        }

        @Override
        protected void onProgressUpdate(Long... values) {
            Unikery unikery = mUnikeryWeakReference.get();
            if (!mStop && !isCancelled() && unikery != null && unikery.getTaskId() == mId) {
                unikery.onProgress(values[0], values[1], values[2]);
            }
        }
    }

    @SuppressWarnings({"unused", "UnusedReturnValue", "RedundantSuppression"})
    public static class Builder<T> {

        private int mId;
        private Unikery<T> mUnikery;
        private String mKey;
        private String mUrl;
        private DataContainer mDataContainer;
        private boolean mUseMemoryCache = true;
        private boolean mUseDiskCache = true;
        private boolean mUseNetwork = true;
        private ValueHelper<T> mHelper;
        private ValueCache<T> mCache;
        private OkHttpClient mOkHttpClient;
        private Executor mDiskExecutor;
        private Executor mNetworkExecutor;
        private Conaco<T> mConaco;

        public Builder<T> setId(int id) {
            mId = id;
            return this;
        }

        public Builder<T> setUnikery(Unikery<T> unikery) {
            mUnikery = unikery;
            return this;
        }

        public Unikery<T> getUnikery() {
            return mUnikery;
        }

        public Builder<T> setKey(String key) {
            mKey = key;
            return this;
        }

        public String getKey() {
            return mKey;
        }

        public Builder<T> setUrl(String url) {
            mUrl = url;
            return this;
        }

        public String getUrl() {
            return mUrl;
        }

        public Builder<T> setDataContainer(DataContainer dataContainer) {
            mDataContainer = dataContainer;
            return this;
        }

        public Builder<T> setUseMemoryCache(boolean useMemoryCache) {
            mUseMemoryCache = useMemoryCache;
            return this;
        }

        boolean isUseMemoryCache() {
            return mUseMemoryCache;
        }

        public Builder<T> setUseDiskCache(boolean useDiskCache) {
            mUseDiskCache = useDiskCache;
            return this;
        }

        boolean isUseDiskCache() {
            return mUseDiskCache;
        }

        public Builder<T> setUseNetwork(boolean useNetwork) {
            mUseNetwork = useNetwork;
            return this;
        }

        boolean isUseNetwork() {
            return mUseNetwork;
        }

        Builder<T> setHelper(ValueHelper<T> helper) {
            mHelper = helper;
            return this;
        }

        ValueHelper<T> getHelper() {
            return mHelper;
        }

        Builder<T> setCache(ValueCache<T> cache) {
            mCache = cache;
            return this;
        }

        Builder<T> setOkHttpClient(OkHttpClient okHttpClient) {
            mOkHttpClient = okHttpClient;
            return this;
        }

        Builder<T> setDiskExecutor(Executor diskExecutor) {
            mDiskExecutor = diskExecutor;
            return this;
        }

        Builder<T> setNetworkExecutor(Executor networkExecutor) {
            mNetworkExecutor = networkExecutor;
            return this;
        }

        Builder<T> setConaco(Conaco<T> conaco) {
            mConaco = conaco;
            return this;
        }

        public void isValid() {
            if (mUnikery == null) {
                throw new IllegalStateException("Must set unikery");
            }
            if (mKey == null && mUrl == null && mDataContainer == null) {
                throw new IllegalStateException("At least one of mKey and mUrl and mDataContainer have to not be null");
            }
        }

        public ConacoTask<T> build() {
            return new ConacoTask<>(this);
        }
    }
}

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final String EXTRA_VIEW_LATEST = "org.fdroid.fdroid.views.main.MainActivity.VIEW_LATEST";
    private static final String EXTRA_VIEW_CATEGORIES = "org.fdroid.fdroid.views.main.MainActivity.VIEW_CATEGORIES";
    private static final String EXTRA_VIEW_NEARBY = "org.fdroid.fdroid.views.main.MainActivity.VIEW_NEARBY";
    public static final String EXTRA_VIEW_UPDATES = "org.fdroid.fdroid.views.main.MainActivity.VIEW_UPDATES";
    public static final String EXTRA_VIEW_SETTINGS = "org.fdroid.fdroid.views.main.MainActivity.VIEW_SETTINGS";

    static final int REQUEST_LOCATION_PERMISSIONS = 0xEF0F;
    static final int REQUEST_STORAGE_PERMISSIONS = 0xB004;
    static final int REQUEST_STORAGE_ACCESS = 0x40E5;

    public static final String ACTION_REQUEST_SWAP = "requestSwap";

    private RecyclerView pager;
    private MainViewAdapter adapter;
    private BottomNavigationView bottomNavigation;
    private BadgeDrawable updatesBadge;

    private final ActivityResultLauncher<String> permissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                // no-op
            });

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FDroidApp fdroidApp = (FDroidApp) getApplication();
        fdroidApp.setSecureWindow(this);

        fdroidApp.applyPureBlackBackgroundInDarkTheme(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        adapter = new MainViewAdapter(this);

        pager = findViewById(R.id.main_view_pager);
        pager.setHasFixedSize(true);
        pager.setLayoutManager(new NonScrollingHorizontalLayoutManager(this));
        pager.setAdapter(adapter);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        setSelectedMenuInNav(Preferences.get().getBottomNavigationViewName());
        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            pager.scrollToPosition(item.getOrder());

            if (item.getItemId() == R.id.latest) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_LATEST);
            } else if (item.getItemId() == R.id.categories) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_CATEGORIES);
            } else if (item.getItemId() == R.id.nearby) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_NEARBY);

                NearbyViewBinder.updateUsbOtg(MainActivity.this);
            } else if (item.getItemId() == R.id.updates) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_UPDATES);
            } else if (item.getItemId() == R.id.settings) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_SETTINGS);
            }
            return true;

        });
        updatesBadge = bottomNavigation.getOrCreateBadge(R.id.updates);
        updatesBadge.setVisible(false);

        AppUpdateStatusManager.getInstance(this).getNumUpdatableApps().observe(this, this::refreshUpdatesBadge);

        Intent intent = getIntent();
        if (handleMainViewSelectIntent(intent)) {
            return;
        }
        handleSearchOrAppViewIntent(intent);
    }

    /**
     * {@link android.material.navigation.NavigationBarView} says "Menu items
     * can also be used for programmatically selecting which destination is
     * currently active. It can be done using {@code MenuItem.setChecked(true)}".
     */
    private void setSelectedMenuInNav(int menuId) {
        int position = adapter.adapterPositionFromItemId(menuId);
        if (position < 0) {
            Log.e(TAG, "Invalid menu position: " + position);
        } else {
            pager.scrollToPosition(position);
            bottomNavigation.getMenu().getItem(position).setChecked(true);
        }
    }

    private void setSelectedMenuInNav(final String viewName) {
        if (EXTRA_VIEW_LATEST.equals(viewName)) {
            setSelectedMenuInNav(R.id.latest);
        } else if (EXTRA_VIEW_CATEGORIES.equals(viewName)) {
            setSelectedMenuInNav(R.id.categories);
        } else if (EXTRA_VIEW_NEARBY.equals(viewName)) {
            setSelectedMenuInNav(R.id.nearby);
        } else if (EXTRA_VIEW_UPDATES.equals(viewName)) {
            setSelectedMenuInNav(R.id.updates);
        } else if (EXTRA_VIEW_SETTINGS.equals(viewName)) {
            setSelectedMenuInNav(R.id.settings);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        FDroidApp.checkStartTor(this, Preferences.get());

        NearbyViewBinder.updateExternalStorageViews(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // don't request this in onResume, because the launcher causes a call to that,
        // even if permission permanently denied, so we'll get an infinite loop
        if (Build.VERSION.SDK_INT >= 33) {
            String notificationPerm = Manifest.permission.POST_NOTIFICATIONS;
            if (ContextCompat.checkSelfPermission(this, notificationPerm) != PackageManager.PERMISSION_GRANTED) {
                permissionLauncher.launch(notificationPerm);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (handleMainViewSelectIntent(intent)) {
            return;
        }

        handleSearchOrAppViewIntent(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_STORAGE_ACCESS) {
            TreeUriScannerIntentService.onActivityResult(this, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSIONS) {
            WifiStateChangeService.start(this, null);
            ContextCompat.startForegroundService(this, new Intent(this, SwapService.class));
        } else if (requestCode == REQUEST_STORAGE_PERMISSIONS) {
            Toast.makeText(this,
                    this.getString(R.string.scan_removable_storage_toast, ""),
                    Toast.LENGTH_SHORT).show();
            SDCardScannerService.scan(this);
        }
    }

    /**
     * Handle an {@link Intent} that shows a specific tab in the main view.
     */
    private boolean handleMainViewSelectIntent(Intent intent) {
        if (intent.hasExtra(EXTRA_VIEW_NEARBY)) {
            setSelectedMenuInNav(R.id.nearby);
            return true;
        } else if (intent.hasExtra(EXTRA_VIEW_UPDATES)) {
            setSelectedMenuInNav(R.id.updates);
            return true;
        } else if (intent.hasExtra(EXTRA_VIEW_SETTINGS)) {
            setSelectedMenuInNav(R.id.settings);
            return true;
        }
        return false;
    }

    /**
     * Since any app could send this {@link Intent}, and the search terms are
     * fed into a SQL query, the data must be strictly sanitized to avoid
     * SQL injection attacks.
     */
    private void handleSearchOrAppViewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null) performSearch(query);
            return;
        }

        final Uri data = intent.getData();
        if (data == null) {
            return;
        }

        final String scheme = data.getScheme();
        final String path = data.getPath();
        String packageName = null;
        String query = null;
        if (data.isHierarchical()) {
            final String host = data.getHost();
            if (host == null) {
                return;
            }
            switch (host) {
                case "f-droid.org":
                case "www.f-droid.org":
                case "staging.f-droid.org":
                    if (path.startsWith("/app/") || path.startsWith("/packages/")
                            || path.matches("^/[a-z][a-z][a-zA-Z_-]*/packages/.*")) {
                        // http://f-droid.org/app/packageName
                        packageName = data.getLastPathSegment();
                    } else if (path.startsWith("/repository/browse")) {
                        // http://f-droid.org/repository/browse?fdfilter=search+query
                        query = data.getQueryParameter("fdfilter");

                        // http://f-droid.org/repository/browse?fdid=packageName
                        packageName = data.getQueryParameter("fdid");
                    } else if ("/app".equals(data.getPath()) || "/packages".equals(data.getPath())) {
                        packageName = null;
                    }
                    break;
                case "details":
                    // market://details?id=app.id
                    packageName = data.getQueryParameter("id");
                    break;
                case "search":
                    // market://search?q=query
                    query = data.getQueryParameter("q");
                    break;
                case "play.google.com":
                    if (path.startsWith("/store/apps/details")) {
                        // http://play.google.com/store/apps/details?id=app.id
                        packageName = data.getQueryParameter("id");
                    } else if (path.startsWith("/store/search")) {
                        // http://play.google.com/store/search?q=foo
                        query = data.getQueryParameter("q");
                    }
                    break;
                case "apps":
                case "amazon.com":
                case "www.amazon.com":
                    // amzn://apps/android?p=app.id
                    // http://amazon.com/gp/mas/dl/android?s=app.id
                    packageName = data.getQueryParameter("p");
                    query = data.getQueryParameter("s");
                    break;
            }
        } else if ("fdroid.app".equals(scheme)) {
            // fdroid.app:app.id
            packageName = data.getSchemeSpecificPart();
        } else if ("fdroid.search".equals(scheme)) {
            // fdroid.search:query
            query = data.getSchemeSpecificPart();
        }

        if (!TextUtils.isEmpty(query)) {
            // an old format for querying via packageName
            if (query.startsWith("pname:")) {
                packageName = query.split(":")[1];
            }

            // sometimes, search URLs include pub: or other things before the query string
            if (query.contains(":")) {
                query = query.split(":")[1];
            }
        }

        if (!TextUtils.isEmpty(packageName)) {
            // sanitize packageName to be a valid Java packageName and prevent exploits
            packageName = packageName.replaceAll("[^A-Za-z\\d_.]", "");
            Utils.debugLog(TAG, "FDroid launched via app link for '" + packageName + "'");
            Intent intentToInvoke = new Intent(this, AppDetailsActivity.class);
            intentToInvoke.putExtra(AppDetailsActivity.EXTRA_APPID, packageName);
            startActivity(intentToInvoke);
            finish();
        } else if (!TextUtils.isEmpty(query)) {
            Utils.debugLog(TAG, "FDroid launched via search link for '" + query + "'");
            performSearch(query);
        }
    }

    /**
     * These strings might end up in a SQL query, so strip all non-alpha-num
     */
    static String sanitizeSearchTerms(@NonNull String query) {
        return query.replaceAll("[^\\p{L}\\d_ -]", " ");
    }

    /**
     * Initiates the {@link AppListActivity} with the relevant search terms passed in via the query arg.
     */
    private void performSearch(@NonNull String query) {
        Intent searchIntent = new Intent(this, AppListActivity.class);
        searchIntent.putExtra(AppListActivity.EXTRA_SEARCH_TERMS, sanitizeSearchTerms(query));
        startActivity(searchIntent);
    }

    private void refreshUpdatesBadge(int canUpdateCount) {
        if (canUpdateCount <= 0) {
            updatesBadge.setVisible(false);
            updatesBadge.clearNumber();
        } else {
            updatesBadge.setNumber(canUpdateCount);
            updatesBadge.setVisible(true);
        }
    }

    private static class NonScrollingHorizontalLayoutManager extends LinearLayoutManager {
        NonScrollingHorizontalLayoutManager(Context context) {
            super(context, LinearLayoutManager.HORIZONTAL, false);
        }

        @Override
        public boolean canScrollHorizontally() {
            return false;
        }

        @Override
        public boolean canScrollVertically() {
            return false;
        }
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package cookie;

import org.apache.commons.text.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class HttpResponseSplitting extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        Cookie c = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String paramNames = req.getParameterNames().nextElement();
        Cookie cParamNames = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamNames.setHttpOnly(true);
        cParamNames.setSecure(true);
        resp.addCookie(cParamNames);

        String paramValues = req.getParameterValues("input")[0];
        Cookie cParamValues = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamValues.setHttpOnly(true);
        cParamValues.setSecure(true);
        resp.addCookie(cParamValues);

        String paramMap = req.getParameterMap().get("input")[0];
        Cookie cParamMap = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamMap.setHttpOnly(true);
        cParamMap.setSecure(true);
        resp.addCookie(cParamMap);

        String header = req.getHeader("input");
        Cookie cHeader = new Cookie("name", null);
        //ruleid: java_cookie_rule-HttpResponseSplitting
        c.setValue(header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = StringEscapeUtils.escapeJava(data);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = StringEscapeUtils.escapeJava(header);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = StringEscapeUtils.escapeJava(contextPath);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // BAD
        String tainted = req.getParameter("input");
        resp.setHeader("test", tainted);

        // OK: False negative but reported by spotbugs
        String data = req.getParameter("input");
        String normalized = data.replaceAll("\n", "\n");
        resp.setHeader("test", normalized);

        // OK: False negative but reported by spotbugs
        String normalized2 = data.replaceAll("\n", req.getParameter("test"));
        resp.setHeader("test2", normalized2);

        // OK
        String normalized3 = org.apache.commons.text.StringEscapeUtils.unescapeJava(tainted);
        resp.setHeader("test3", normalized3);

        // BAD
        String normalized4 = getString(tainted);
        resp.setHeader("test4", normalized4);

        // BAD
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
        wrapper.addHeader("test", tainted);
        wrapper.setHeader("test2", tainted);

    }

    private String getString(String s) {
        return s;
    }
}

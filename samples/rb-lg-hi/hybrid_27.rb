class Groups::MilestonesController < Groups::ApplicationController
  include MilestoneActions

  before_action :milestone, only: [:edit, :show, :update, :issues, :merge_requests, :participants, :labels, :destroy]
  before_action :authorize_admin_milestones!, only: [:edit, :new, :create, :update, :destroy]

  feature_category :team_planning
  urgency :low

  def index
    respond_to do |format|
      format.html do
        @milestone_states = Milestone.states_count(group_projects_with_access.without_order, [group])
        @milestones = milestones.page(params[:page])
      end
      format.json do
        render json: milestones.to_json(only: [:id, :title, :due_date], methods: :name)
      end
    end
  end

  def new
    @noteable = @milestone = Milestone.new
  end

  def create
    @milestone = Milestones::CreateService.new(group, current_user, milestone_params).execute

    if @milestone.persisted?
      redirect_to milestone_path(@milestone)
    else
      render "new"
    end
  end

  def show; end

  def edit; end

  def update
    Milestones::UpdateService.new(@milestone.parent, current_user, milestone_params).execute(@milestone)

    respond_to do |format|
      format.html do
        if @milestone.valid?
          redirect_to milestone_path(@milestone)
        else
          render :edit
        end
      end

      format.json do
        if @milestone.valid?
          head :no_content
        else
          render json: { errors: @milestone.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  rescue ActiveRecord::StaleObjectError
    respond_to do |format|
      format.html do
        @conflict = true
        render :edit
      end

      format.json do
        render json: {
          errors: [
            format(
              _("Someone edited this %{model_name} at the same time you did. Please refresh your browser and make sure your changes will not unintentionally remove theirs."), # rubocop:disable Layout/LineLength
              model_name: _('milestone')
            )
          ]
        }, status: :conflict
      end
    end
  end

  def destroy
    Milestones::DestroyService.new(group, current_user).execute(@milestone)

    respond_to do |format|
      format.html { redirect_to group_milestones_path(group), status: :see_other }
      format.js { head :ok }
    end
  end

  private

  def authorize_admin_milestones!
    render_404 unless can?(current_user, :admin_milestone, group)
  end

  def milestone_params
    params.require(:milestone)
    .permit(
      :description,
      :due_date,
      :lock_version,
      :start_date,
      :state_event,
      :title
    )
  end

  def milestones
    MilestonesFinder.new(search_params).execute
  end

  def milestone
    @noteable = @milestone ||= group.milestones.find_by_iid(params[:id])

    render_404 unless @milestone
  end

  def search_params
    groups = request.format.json? ? group_ids(include_ancestors: true) : group_ids

    @sort = params[:sort] || 'due_date_asc'
    params.permit(:state, :search_title).merge(sort: @sort, group_ids: groups, project_ids: group_projects_with_access)
  end

  def group_projects_with_access
    group_projects_with_subgroups.with_issues_or_mrs_available_for_user(current_user)
  end

  def group_ids(include_ancestors: false)
    if include_ancestors
      group.self_and_hierarchy.public_or_visible_to_user(current_user).select(:id)
    else
      group.self_and_descendants.public_or_visible_to_user(current_user).select(:id)
    end
  end
end

class Groups::MilestonesController < Groups::ApplicationController
  include MilestoneActions

  before_action :milestone, only: [:edit, :show, :update, :issues, :merge_requests, :participants, :labels, :destroy]
  before_action :authorize_admin_milestones!, only: [:edit, :new, :create, :update, :destroy]

  feature_category :team_planning
  urgency :low

  def index
    respond_to do |format|
      format.html do
        @milestone_states = Milestone.states_count(group_projects_with_access.without_order, [group])
        @milestones = milestones.page(params[:page])
      end
      format.json do
        render json: milestones.to_json(only: [:id, :title, :due_date], methods: :name)
      end
    end
  end

  def new
    @noteable = @milestone = Milestone.new
  end

  def create
    @milestone = Milestones::CreateService.new(group, current_user, milestone_params).execute

    if @milestone.persisted?
      redirect_to milestone_path(@milestone)
    else
      render "new"
    end
  end

  def show; end

  def edit; end

  def update
    Milestones::UpdateService.new(@milestone.parent, current_user, milestone_params).execute(@milestone)

    respond_to do |format|
      format.html do
        if @milestone.valid?
          redirect_to milestone_path(@milestone)
        else
          render :edit
        end
      end

      format.json do
        if @milestone.valid?
          head :no_content
        else
          render json: { errors: @milestone.errors.full_messages }, status: :unprocessable_entity
        end
      end
    end
  rescue ActiveRecord::StaleObjectError
    respond_to do |format|
      format.html do
        @conflict = true
        render :edit
      end

      format.json do
        render json: {
          errors: [
            format(
              _("Someone edited this %{model_name} at the same time you did. Please refresh your browser and make sure your changes will not unintentionally remove theirs."), # rubocop:disable Layout/LineLength
              model_name: _('milestone')
            )
          ]
        }, status: :conflict
      end
    end
  end

  def destroy
    Milestones::DestroyService.new(group, current_user).execute(@milestone)

    respond_to do |format|
      format.html { redirect_to group_milestones_path(group), status: :see_other }
      format.js { head :ok }
    end
  end

  private

  def authorize_admin_milestones!
    render_404 unless can?(current_user, :admin_milestone, group)
  end

  def milestone_params
    params.require(:milestone)
    .permit(
      :description,
      :due_date,
      :lock_version,
      :start_date,
      :state_event,
      :title
    )
  end

  def milestones
    MilestonesFinder.new(search_params).execute
  end

  def milestone
    @noteable = @milestone ||= group.milestones.find_by_iid(params[:id])

    render_404 unless @milestone
  end

  def search_params
    groups = request.format.json? ? group_ids(include_ancestors: true) : group_ids

    @sort = params[:sort] || 'due_date_asc'
    params.permit(:state, :search_title).merge(sort: @sort, group_ids: groups, project_ids: group_projects_with_access)
  end

  def group_projects_with_access
    group_projects_with_subgroups.with_issues_or_mrs_available_for_user(current_user)
  end

  def group_ids(include_ancestors: false)
    if include_ancestors
      group.self_and_hierarchy.public_or_visible_to_user(current_user).select(:id)
    else
      group.self_and_descendants.public_or_visible_to_user(current_user).select(:id)
    end
  end
end

class Projects::FeatureFlagsController < Projects::ApplicationController
  respond_to :html

  before_action :authorize_read_feature_flag!
  before_action :authorize_create_feature_flag!, only: [:new, :create]
  before_action :authorize_update_feature_flag!, only: [:edit, :update]
  before_action :authorize_destroy_feature_flag!, only: [:destroy]

  before_action :feature_flag, only: [:edit, :update, :destroy]

  feature_category :feature_flags
  urgency :low

  def index
    @feature_flags = FeatureFlagsFinder
      .new(project, current_user, scope: params[:scope])
      .execute
      .page(params[:page])
      .per(30)

    respond_to do |format|
      format.html
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render json: { feature_flags: feature_flags_json }.merge(summary_json)
      end
    end
  end

  def new; end

  def show
    respond_to do |format|
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render_success_json(feature_flag)
      end
    end
  end

  def create
    result = FeatureFlags::CreateService.new(project, current_user, create_params).execute

    if result[:status] == :success
      respond_to do |format|
        format.json { render_success_json(result[:feature_flag]) }
      end
    else
      respond_to do |format|
        format.json { render_error_json(result[:message]) }
      end
    end
  end

  def edit; end

  def update
    result = FeatureFlags::UpdateService.new(project, current_user, update_params).execute(feature_flag)

    if result[:status] == :success
      respond_to do |format|
        format.json { render_success_json(result[:feature_flag]) }
      end
    else
      respond_to do |format|
        format.json { render_error_json(result[:message], result[:http_status]) }
      end
    end
  end

  def destroy
    result = FeatureFlags::DestroyService.new(project, current_user).execute(feature_flag)

    if result[:status] == :success
      respond_to do |format|
        format.html { redirect_to_index(notice: _('Feature flag was successfully removed.')) }
        format.json { render_success_json(feature_flag) }
      end
    else
      respond_to do |format|
        format.html { redirect_to_index(alert: _('Feature flag was not removed.')) }
        format.json { render_error_json(result[:message]) }
      end
    end
  end

  protected

  def feature_flag
    @feature_flag ||= @noteable = project.operations_feature_flags.find_by_iid!(params[:iid])
  end

  def create_params
    params.require(:operations_feature_flag).permit(
      :name,
      :description,
      :active,
      :version,
      scopes_attributes: [
        :environment_scope, :active,
        { strategies: [:name, { parameters: [:groupId, :percentage, :userIds] }] }
      ],
      strategies_attributes: [
        :name,
        :user_list_id,
        { parameters: [:groupId, :percentage, :userIds, :rollout, :stickiness],
          scopes_attributes: [:environment_scope] }
      ]
    )
  end

  def update_params
    params.require(:operations_feature_flag).permit(
      :name,
      :description,
      :active,
      scopes_attributes: [
        :id,
        :environment_scope,
        :active,
        :_destroy,
        { strategies: [:name, { parameters: [:groupId, :percentage, :userIds] }] }
      ],
      strategies_attributes: [
        :id,
        :name,
        :user_list_id,
        :_destroy,
        { parameters: [:groupId, :percentage, :userIds, :rollout, :stickiness],
          scopes_attributes: [:id, :environment_scope, :_destroy] }
      ]
    )
  end

  def feature_flag_json(feature_flag)
    FeatureFlagSerializer
      .new(project: @project, current_user: @current_user)
      .represent(feature_flag)
  end

  def feature_flags_json
    FeatureFlagSerializer
      .new(project: @project, current_user: @current_user)
      .with_pagination(request, response)
      .represent(@feature_flags)
  end

  def summary_json
    FeatureFlagSummarySerializer
      .new(project: @project, current_user: @current_user)
      .represent(@project)
  end

  def redirect_to_index(**args)
    redirect_to project_feature_flags_path(@project), status: :found, **args
  end

  def render_success_json(feature_flag)
    render json: feature_flag_json(feature_flag), status: :ok
  end

  def render_error_json(messages, status = :bad_request)
    render json: { message: messages }, status: status
  end
end

class RepositoriesController < Groups::ApplicationController
      include PackagesHelper
      include ::Registry::ConnectionErrorsHandler

      before_action :verify_container_registry_enabled!
      before_action :authorize_read_container_image!

      before_action only: [:index, :show] do
        push_frontend_feature_flag(:show_container_registry_tag_signatures, group)
      end

      before_action only: [:index, :show] do
        push_frontend_feature_flag(:container_registry_protected_containers, group.root_ancestor)
      end

      feature_category :container_registry
      urgency :low

      def index
        respond_to do |format|
          format.html
          format.json do
            @images = ContainerRepositoriesFinder.new(user: current_user, subject: group, params: params.slice(:name))
                                                 .execute
                                                 .with_api_entity_associations

            track_package_event(:list_repositories, :container, user: current_user, namespace: group)

            serializer = ContainerRepositoriesSerializer
              .new(current_user: current_user)

            render json: serializer.with_pagination(request, response)
              .represent_read_only(@images)
          end
        end
      end

      # The show action renders index to allow frontend routing to work on page refresh
      def show
        render :index
      end

      private

      def verify_container_registry_enabled!
        render_404 unless Gitlab.config.registry.enabled
      end

      def authorize_read_container_image!
        render_404 unless can?(current_user, :read_container_image, group)
      end
    end

class VariablesController < Groups::ApplicationController
    before_action :authorize_admin_group!, except: :update
    before_action :authorize_admin_cicd_variables!, only: :update

    skip_cross_project_access_check :show, :update

    feature_category :secrets_management

    urgency :low, [:show]

    def show
      respond_to do |format|
        format.json do
          render status: :ok, json: { variables: ::Ci::GroupVariableSerializer.new.represent(@group.variables) }
        end
      end
    end

    def update
      update_result = Ci::ChangeVariablesService.new(
        container: @group, current_user: current_user,
        params: group_variables_params
      ).execute

      if update_result
        respond_to do |format|
          format.json { render_group_variables }
        end
      else
        respond_to do |format|
          format.json { render_error }
        end
      end
    end

    private

    def render_group_variables
      render status: :ok, json: { variables: ::Ci::GroupVariableSerializer.new.represent(@group.variables) }
    end

    def render_error
      render status: :bad_request, json: @group.errors.full_messages
    end

    def group_variables_params
      params.permit(variables_attributes: Array(variable_params_attributes))
    end

    def variable_params_attributes
      %i[id variable_type key description secret_value protected masked hidden raw _destroy]
    end
  end

class Projects::MergeRequests::ConflictsController < Projects::MergeRequests::ApplicationController
  include IssuableActions

  before_action :authorize_can_resolve_conflicts!

  urgency :low, [
    :show,
    :conflict_for_path,
    :resolve_conflicts
  ]

  def show
    respond_to do |format|
      format.html do
        @issuable_sidebar = serializer.represent(@merge_request, serializer: 'sidebar')
        Gitlab::UsageDataCounters::MergeRequestActivityUniqueCounter
          .track_loading_conflict_ui_action(user: current_user)
      end

      format.json do
        if @conflicts_list.can_be_resolved_in_ui?
          render json: @conflicts_list
        elsif @merge_request.can_be_merged?
          render json: {
            message: _('The merge conflicts for this merge request have already been resolved. ' \
              'Please return to the merge request.'),
            type: 'error'
          }
        else
          render json: {
            message: _('The merge conflicts for this merge request cannot be resolved through GitLab. ' \
              'Please try to resolve them locally.'),
            type: 'error'
          }
        end
      end
    end
  end

  def conflict_for_path
    return render_404 unless @conflicts_list.can_be_resolved_in_ui?

    file = @conflicts_list.file_for_path(params[:old_path], params[:new_path])

    return render_404 unless file

    render json: file, full_content: true
  end

  def resolve_conflicts
    return render_404 unless @conflicts_list.can_be_resolved_in_ui?

    Gitlab::UsageDataCounters::MergeRequestActivityUniqueCounter.track_resolve_conflict_action(user: current_user)

    if @merge_request.can_be_merged?
      render status: :bad_request,
        json: { message: _('The merge conflicts for this merge request have already been resolved.') }
      return
    end

    begin
      ::MergeRequests::Conflicts::ResolveService
        .new(merge_request)
        .execute(current_user, params)

      flash[:notice] = _('All merge conflicts were resolved. The merge request can now be merged.')

      render json: { redirect_to: project_merge_request_path(@project, @merge_request, resolved_conflicts: true) }
    rescue Gitlab::Git::Conflict::Resolver::ResolutionError => e
      render status: :bad_request, json: { message: e.message }
    end
  end

  private

  alias_method :issuable, :merge_request

  def authorize_can_resolve_conflicts!
    @conflicts_list = ::MergeRequests::Conflicts::ListService.new(@merge_request)

    render_404 unless @conflicts_list.can_be_resolved_by?(current_user)
  end

  def serializer
    MergeRequestSerializer.new(current_user: current_user, project: project)
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    #ruleid: ruby_sql_rule-CheckSQL
    Product.find(:all, :conditions => 'admin = 1', :group => "something, #{params[:group]}")
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization

    o = Klass.new("hello\n")
    data = YAML.dump(o)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ruleid: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.load_stream(data)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

 end

 def ok_deserialization
    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.load(data, safe: true)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.load_stream(data, safe: true)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.parse_stream(data, safe: true)

    filename = File.read("test.txt")
    data = YAML.dump(filename)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load(filename)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load_stream(filename)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.parse_stream(filename)

    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load(File.read("test.txt"))
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load_stream(File.read("test.txt"))
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.parse_stream(File.read("test.txt"))
 end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# ruleid: ruby_xss_rule-AvoidLinkTo
link_to "#{params[:url]}/profile", profile_path(@profile)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

url = request.env[:url]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: ruby_xss_rule-AvoidLinkTo
link_to "Profile#{params[:url]}", profile_path(@profile)

# ok: ruby_xss_rule-AvoidLinkTo
link_to "Profile", profile_path(@profile)
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
require 'net/http'

def foo

  url = params[:url]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:url])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.post_form(URI(params[:url]))

  uri = URI(params[:server])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get("example.com", "/index.html")

  uri = URI("example.com/index.html")
  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(uri)

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
require 'net/http'

def foo

  url = params[:url]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:url])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:server])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(params[:url])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get("example.com", "/index.html")

  uri = URI("example.com/index.html")
  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(uri)

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'net/ftp'

def foo

  host = params[:host]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  ftp = Net::FTP.new()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.gettextfile("/tmp/#{params[:file]}")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.new("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.open("example.com")

  ftp = Net::FTP.new()
  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.connect("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.get("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.getbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.gettextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.put("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.putbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.puttextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.delete("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storlines("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storbinary("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.sendcmd("ls -al")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.retrlines("ls -al")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesMD5
        digest = OpenSSL::Digest::MD5.hexdigest 'abc'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:name, :admin))
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def zen
  41
end

# ok:ruby_eval_rule-NoEval
eval("def zen; 42; end")

puts zen

class Thing
end
a = %q{def hello() "Hello there!" end}
# not user-controllable, this is ok
# ok:ruby_eval_rule-NoEval
Thing.module_eval(a)
puts Thing.new.hello()
b = params['something']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def get_binding(param)
  binding
end
b = get_binding("hello")
# ok:ruby_eval_rule-NoEval
b.eval("some_func")

# ok:ruby_eval_rule-NoEval
eval("some_func",b)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid:ruby_eval_rule-NoEval
eval(cookies.delete('foo'))

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok:ruby_eval_rule-NoEval
RubyVM::InstructionSequence.compile("1 + 2").eval

iseq = RubyVM::InstructionSequence.compile(foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


iseq = RubyVM::InstructionSequence.compile('num = 1 + 2')
# ok:ruby_eval_rule-NoEval
iseq.eval

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'open3'

def test_params()
  user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  commands = "ls -lah /raz/dva"
# ok: ruby_injection_rule-DangerousExec
  system(commands)

  cmd_name = "sh"
# ok: ruby_injection_rule-DangerousExec
  Process.exec([cmd_name, "ls", "-la"])
# ok: ruby_injection_rule-DangerousExec
  Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
# ok: ruby_injection_rule-DangerousExec
  system("ls -lah /tmp")
# ok: ruby_injection_rule-DangerousExec
  exec(["ls", "-lah", "/tmp"])
end

def test_calls(user_input)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end

  def test_params()
    user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end
  
  def test_cookies()
    user_input = cookies['some_cookie']
    # ruleid: ruby_injection_rule-DangerousExec
      exec("ls -lah #{user_input}")
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
      commands = "ls -lah /raz/dva"
    # ok: ruby_injection_rule-DangerousExec
      system(commands)
    
      cmd_name = "sh"
    # ok: ruby_injection_rule-DangerousExec
      Process.exec([cmd_name, "ls", "-la"])
    # ok: ruby_injection_rule-DangerousExec
      Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
    # ok: ruby_injection_rule-DangerousExec
      system("ls -lah /tmp")
    # ok: ruby_injection_rule-DangerousExec
      exec(["ls", "-lah", "/tmp"])
    end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
require 'net/http'

def foo

  url = params[:url]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:url])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:server])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(params[:url])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get("example.com", "/index.html")

  uri = URI("example.com/index.html")
  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(uri)

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def some_rails_controller
  foo = params[:some_regex]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def some_rails_controller
  foo = Record[something]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def some_rails_controller
  foo = Record.read_attribute("some_attribute")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  bar = ENV['someEnvVar']
  #ok: ruby_regex_rule-CheckRegexDOS
  Regexp.new(bar).match("some_string")
end

def use_params_in_regex
#ruleid: ruby_regex_rule-CheckRegexDOS
@x = something.match /#{params[:x]}/
end

def regex_on_params
#ok: ruby_regex_rule-CheckRegexDOS
@x = params[:x].match /foo/
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# ruleid:ruby_eval_rule-NoEval
Array.class_eval(cookies['tainted_cookie'])

def zen
  41
end

# ok:ruby_eval_rule-NoEval
eval("def zen; 42; end")

puts zen

class Thing
end
a = %q{def hello() "Hello there!" end}
# not user-controllable, this is ok
# ok:ruby_eval_rule-NoEval
Thing.module_eval(a)
puts Thing.new.hello()
b = params['something']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def get_binding(param)
  binding
end
b = get_binding("hello")
# ok:ruby_eval_rule-NoEval
b.eval("some_func")

# ok:ruby_eval_rule-NoEval
eval("some_func",b)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok:ruby_eval_rule-NoEval
RubyVM::InstructionSequence.compile("1 + 2").eval

iseq = RubyVM::InstructionSequence.compile(foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


iseq = RubyVM::InstructionSequence.compile('num = 1 + 2')
# ok:ruby_eval_rule-NoEval
iseq.eval

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def test_send_file
    # ruleid: ruby_file_rule-CheckSendFile
    send_file params[:file]
end

def test_send_file2
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file4
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file5
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.encrypted[:something]
end

def test_send_file6
    # this is reported since semgrep 0.94 because . ... . can now match
    # intermediate fields, not just method calls.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file7
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.permanent.signed[:something]
end

def test_send_file8
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file_ok
    # ok: ruby_file_rule-CheckSendFile
    send_file "some_safe_file.txt"
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def zen
  41
end

# ok:ruby_eval_rule-NoEval
eval("def zen; 42; end")

puts zen

class Thing
end
a = %q{def hello() "Hello there!" end}
# not user-controllable, this is ok
# ok:ruby_eval_rule-NoEval
Thing.module_eval(a)
puts Thing.new.hello()
b = params['something']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def get_binding(param)
  binding
end
b = get_binding("hello")
# ok:ruby_eval_rule-NoEval
b.eval("some_func")

# ok:ruby_eval_rule-NoEval
eval("some_func",b)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok:ruby_eval_rule-NoEval
RubyVM::InstructionSequence.compile("1 + 2").eval

iseq = RubyVM::InstructionSequence.compile(foo)
# ruleid:ruby_eval_rule-NoEval
iseq.eval


iseq = RubyVM::InstructionSequence.compile('num = 1 + 2')
# ok:ruby_eval_rule-NoEval
iseq.eval

class Dashboard::ProjectsController < Dashboard::ApplicationController
  include ParamsBackwardCompatibility
  include RendersMemberAccess
  include RendersProjectsList
  include SortingHelper
  include SortingPreference
  include FiltersEvents

  prepend_before_action(only: [:index]) { authenticate_sessionless_user!(:rss) }
  before_action :set_non_archived_param, only: [:index, :starred]
  before_action :set_sorting
  before_action :projects, only: [:index]
  skip_cross_project_access_check :index, :starred

  feature_category :groups_and_projects
  urgency :low, [:starred, :index]

  def index
    respond_to do |format|
      format.html do
        render_projects
      end
      format.atom do
        load_events
        render layout: 'xml'
      end
      format.json do
        render json: {
          html: view_to_html_string("dashboard/projects/_projects", projects: @projects)
        }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def starred
    @projects = load_projects(params.merge(starred: true, not_aimed_for_deletion: true))
      .includes(:forked_from_project, :topics)

    @groups = []

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("dashboard/projects/_projects", projects: @projects)
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  private

  def projects
    @projects ||= load_projects(params.merge(non_public: true, not_aimed_for_deletion: true))
  end

  def render_projects
    # n+1: https://gitlab.com/gitlab-org/gitlab-foss/issues/40260
    Gitlab::GitalyClient.allow_n_plus_1_calls do
      render
    end
  end

  def load_projects(finder_params)
    @all_user_projects = ProjectsFinder.new(
      params: { non_public: true, archived: false,
                not_aimed_for_deletion: true }, current_user: current_user).execute
    @all_starred_projects = ProjectsFinder.new(
      params: { starred: true, archived: false,
                not_aimed_for_deletion: true }, current_user: current_user).execute

    finder_params[:use_cte] = true if use_cte_for_finder?

    projects = ProjectsFinder.new(params: finder_params, current_user: current_user).execute

    projects = preload_associations(projects)
    projects = projects.page(finder_params[:page])

    prepare_projects_for_rendering(projects)
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def preload_associations(projects)
    projects.includes(:route, :creator, :group, :topics, namespace: [:route, :owner]).preload(:project_feature)
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def use_cte_for_finder?
    # The starred action loads public projects, which causes the CTE to be less efficient
    action_name == 'index'
  end

  def load_events
    projects = ProjectsFinder
                .new(params: params.merge(non_public: true, not_aimed_for_deletion: true), current_user: current_user)
                .execute

    @events = EventCollection
      .new(projects, offset: params[:offset].to_i, filter: event_filter)
      .to_a

    Events::RenderService.new(current_user).execute(@events, atom_request: request.format.atom?)
  end

  def set_sorting
    params[:sort] = set_sort_order
    @sort = params[:sort]
  end

  def default_sort_order
    sort_value_name
  end

  def sorting_field
    Project::SORTING_PREFERENCE_FIELD
  end
end

class Projects::BranchesController < Projects::ApplicationController
  include ActionView::Helpers::SanitizeHelper
  include SortingHelper

  # Authorize
  before_action :require_non_empty_project, except: :create
  before_action :authorize_read_code!
  before_action :authorize_push_code!, only: [:new, :create, :destroy, :destroy_all_merged]

  # Support legacy URLs
  before_action :redirect_for_legacy_index_sort_or_search, only: [:index]
  before_action :limit_diverging_commit_counts!, only: [:diverging_commit_counts]

  feature_category :source_code_management
  urgency :low, [:index, :diverging_commit_counts, :create, :destroy]

  def index
    respond_to do |format|
      format.html do
        @mode = fetch_mode
        next render_404 unless @mode

        @sort = sort_param || default_sort
        @overview_max_branches = 5

        # Fetch branches for the specified mode
        fetch_branches_by_mode
        fetch_merge_requests_for_branches

        @refs_pipelines = @project.ci_pipelines.latest_successful_for_refs(@branches.map(&:name))
        @merged_branch_names = repository.merged_branch_names(@branches.map(&:name))
        @branch_pipeline_statuses = Ci::CommitStatusesFinder.new(@project, repository, current_user, @branches).execute

        # https://gitlab.com/gitlab-org/gitlab/-/issues/22851
        Gitlab::GitalyClient.allow_n_plus_1_calls do
          render
        end
      rescue Gitlab::Git::CommandError
        @gitaly_unavailable = true
        render status: :service_unavailable
      end
      format.json do
        branches = BranchesFinder.new(@repository, branches_params).execute
        branches = Kaminari.paginate_array(branches).page(branches_params[:page])
        render json: branches.map(&:name)
      end
    end
  end

  def diverging_commit_counts
    respond_to do |format|
      format.json do
        service = ::Branches::DivergingCommitCountsService.new(repository)
        branches = BranchesFinder.new(repository, params.permit(names: [])).execute

        Gitlab::GitalyClient.allow_n_plus_1_calls do
          render json: branches.to_h { |branch| [branch.name, service.call(branch)] }
        end
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def create
    branch_name = strip_tags(sanitize(params[:branch_name]))
    branch_name = Addressable::URI.unescape(branch_name)

    redirect_to_autodeploy = project.empty_repo? && project.deployment_platform.present?

    result = ::Branches::CreateService.new(project, current_user)
        .execute(branch_name, ref)

    success = (result[:status] == :success)

    if params[:issue_iid] && success
      target_project = confidential_issue_project || @project
      issue = IssuesFinder.new(current_user, project_id: target_project.id).find_by(iid: params[:issue_iid])

      if issue
        SystemNoteService.new_issue_branch(issue, target_project, current_user, branch_name, branch_project: @project)
      end
    end

    respond_to do |format|
      format.html do
        if success
          if redirect_to_autodeploy
            redirect_to url_to_autodeploy_setup(project, branch_name),
              notice: view_context.autodeploy_flash_notice(branch_name)
          else
            redirect_to project_tree_path(@project, branch_name)
          end
        else
          @error = result[:message]
          render action: 'new'
        end
      end

      format.json do
        if success
          render json: { name: branch_name, url: project_tree_url(@project, branch_name) }
        else
          render json: result[:message], status: :unprocessable_entity
        end
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def destroy
    result = ::Branches::DeleteService.new(project, current_user).execute(params[:id])

    respond_to do |format|
      format.html do
        flash_type = result.error? ? :alert : :notice
        flash[flash_type] = result.message

        redirect_back_or_default(default: project_branches_path(@project), options: { status: :see_other })
      end

      format.js { head result.http_status }
      format.json { render json: { message: result.message }, status: result.http_status }
    end
  end

  def destroy_all_merged
    ::Branches::DeleteMergedService.new(@project, current_user).async_execute

    redirect_to project_branches_path(@project),
      notice: _('Merged branches are being deleted. This can take some time depending on the number of branches. ' \
        'Please refresh the page to see changes.')
  end

  private

  def sort_param
    sort = branches_params[:sort].presence

    unless sort.in?(supported_sort_options)
      flash.now[:alert] = _("Unsupported sort value.")
      sort = nil
    end

    sort
  end

  def default_sort
    'stale' == @mode ? SORT_UPDATED_OLDEST : SORT_UPDATED_RECENT
  end

  def supported_sort_options
    [nil, SORT_NAME, SORT_UPDATED_OLDEST, SORT_UPDATED_RECENT]
  end

  # It can be expensive to calculate the diverging counts for each
  # branch. Normally the frontend should be specifying a set of branch
  # names, but prior to
  # https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/32496, the
  # frontend could omit this set. To prevent excessive I/O, we require
  # that a list of names be specified.
  def limit_diverging_commit_counts!
    limit = Kaminari.config.default_per_page

    # If we don't have many branches in the repository, then go ahead.
    return if project.repository.branch_count <= limit
    return if params[:names].present? && Array(params[:names]).length <= limit

    render json: { error: "Specify at least one and at most #{limit} branch names" }, status: :unprocessable_entity
  end

  def ref
    if params[:ref]
      ref_escaped = strip_tags(sanitize(params[:ref]))
      Addressable::URI.unescape(ref_escaped)
    else
      @project.default_branch_or_main
    end
  end

  def url_to_autodeploy_setup(project, branch_name)
    project_new_blob_path(
      project,
      branch_name,
      file_name: '.gitlab-ci.yml',
      commit_message: 'Set up auto deploy',
      target_branch: branch_name,
      context: 'autodeploy'
    )
  end

  def redirect_for_legacy_index_sort_or_search
    # Normalize a legacy URL with redirect
    if request.format != :json && !branches_params[:state].presence && [:sort, :search, :page].any? do |key|
      branches_params[key].presence
    end
      redirect_to project_branches_filtered_path(@project, state: 'all'),
        notice: _('Update your bookmarked URLs as filtered/sorted branches URL has been changed.')
    end
  end

  def fetch_branches_by_mode
    return fetch_branches_for_overview if @mode == 'overview'

    @branches, @prev_path, @next_path =
      Projects::BranchesByModeService.new(@project, branches_params.merge(sort: @sort, mode: @mode)).execute
  end

  def fetch_merge_requests_for_branches
    @related_merge_requests = @project
                                .source_of_merge_requests
                                .including_target_project
                                .by_target_branch(@project.default_branch)
                                .by_sorted_source_branches(@branches.map(&:name))
                                .group_by(&:source_branch)
  end

  def fetch_branches_for_overview
    # Here we get one more branch to indicate if there are more data we're not showing
    limit = @overview_max_branches + 1

    @active_branches =
      BranchesFinder.new(@repository, { per_page: limit, sort: SORT_UPDATED_RECENT })
        .execute(gitaly_pagination: true).select(&:active?)
    @stale_branches =
      BranchesFinder.new(@repository, { per_page: limit, sort: SORT_UPDATED_OLDEST })
        .execute(gitaly_pagination: true).select(&:stale?)

    @branches = @active_branches + @stale_branches
  end

  def fetch_mode
    state = branches_params[:state].presence

    return 'overview' unless state

    state.presence_in(%w[active stale all overview])
  end

  def confidential_issue_project
    return if params[:confidential_issue_project_id].blank?

    confidential_issue_project = Project.find(params[:confidential_issue_project_id])

    return unless can?(current_user, :update_issue, confidential_issue_project)

    confidential_issue_project
  end

  def branches_params
    params.permit(:page, :state, :sort, :search, :page_token, :offset)
  end
end

class Explore::ProjectsController < Explore::ApplicationController
  include PageLimiter
  include ParamsBackwardCompatibility
  include RendersMemberAccess
  include RendersProjectsList
  include SortingHelper
  include SortingPreference

  MIN_SEARCH_LENGTH = 3
  PAGE_LIMIT = 50
  RSS_ENTRIES_LIMIT = 20

  before_action :set_non_archived_param
  before_action :set_sorting

  # For background information on the limit, see:
  #   https://gitlab.com/gitlab-org/gitlab/-/issues/38357
  #   https://gitlab.com/gitlab-org/gitlab/-/issues/262682
  before_action only: [:index, :trending, :starred] do
    limit_pages(PAGE_LIMIT)
  end

  rescue_from PageOutOfBoundsError, with: :page_out_of_bounds

  feature_category :groups_and_projects
  # TODO: Set higher urgency after addressing https://gitlab.com/gitlab-org/gitlab/-/issues/357913
  # and https://gitlab.com/gitlab-org/gitlab/-/issues/358945
  urgency :low, [:index, :topics, :trending, :starred, :topic]

  def index
    show_alert_if_search_is_disabled
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  def trending
    params[:trending] = true
    @projects = load_projects

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def starred
    @projects = load_projects.reorder('star_count DESC')

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("explore/projects/_projects", projects: @projects)
        }
      end
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def topics
    load_project_counts
    load_topics
  end

  def topic
    load_topic

    return render_404 unless @topic

    params[:topic] = @topic.name
    @projects = load_projects

    respond_to do |format|
      format.html
      format.atom do
        @projects = @projects.projects_order_id_desc.limit(RSS_ENTRIES_LIMIT)
        render layout: 'xml'
      end
    end
  end

  private

  def load_project_counts
    @all_user_projects = ProjectsFinder.new(params: { non_public: true }, current_user: current_user).execute
    @all_starred_projects = ProjectsFinder.new(params: { starred: true }, current_user: current_user).execute
  end

  def load_projects
    load_project_counts

    finder_params = {
      minimum_search_length: MIN_SEARCH_LENGTH,
      not_aimed_for_deletion: true
    }

    projects = ProjectsFinder.new(current_user: current_user, params: params.merge(finder_params)).execute

    projects = preload_associations(projects)
    projects = projects.page(pagination_params[:page]).without_count

    prepare_projects_for_rendering(projects)
  end

  def load_topics
    @topics = Projects::TopicsFinder.new(
      params: params.permit(:search),
      organization_id: organization_id
    ).execute.page(pagination_params[:page]).without_count
  end

  def load_topic
    topic_name = if Feature.enabled?(:explore_topics_cleaned_path)
                   URI.decode_www_form_component(params[:topic_name])
                 else
                   params[:topic_name]
                 end

    @topic = Projects::Topic.for_organization(organization_id).find_by_name_case_insensitive(topic_name)
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def preload_associations(projects)
    projects.includes(:route, :creator, :group, :project_feature, :topics, namespace: [:route, :owner])
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def set_sorting
    params[:sort] = set_sort_order
    @sort = params[:sort]
  end

  def default_sort_order
    sort_value_latest_activity
  end

  def sorting_field
    Project::SORTING_PREFERENCE_FIELD
  end

  def page_out_of_bounds(error)
    load_project_counts
    @max_page_number = error.message

    respond_to do |format|
      format.html do
        render "page_out_of_bounds", status: :bad_request
      end

      format.json do
        render json: {
          html: view_to_html_string("explore/projects/page_out_of_bounds")
        }, status: :bad_request
      end
    end
  end

  def show_alert_if_search_is_disabled
    if current_user || (params[:name].blank? && params[:search].blank?) || !html_request? || Feature.disabled?(
      :disable_anonymous_project_search, type: :ops)
      return
    end

    flash.now[:notice] = _('You must sign in to search for specific projects.')
  end

  def organization_id
    ::Current.organization&.id
  end
end

class Projects::GroupLinksController < Projects::ApplicationController
  layout 'project_settings'
  before_action :authorize_admin_project!, except: [:destroy]
  before_action :authorize_manage_destroy!, only: [:destroy]
  before_action :authorize_admin_project_member!, only: [:update]

  feature_category :groups_and_projects

  def update
    result = Projects::GroupLinks::UpdateService.new(group_link, current_user).execute(group_link_params)

    if result.success?
      if group_link.expires?
        render json: {
          expires_in: helpers.time_ago_with_tooltip(group_link.expires_at),
          expires_soon: group_link.expires_soon?
        }
      else
        render json: {}
      end
    else
      render json: { message: result.message }, status: result.reason
    end
  end

  def destroy
    result = ::Projects::GroupLinks::DestroyService.new(project, current_user).execute(group_link)

    if result.success?
      respond_to do |format|
        format.html do
          if can?(current_user, :admin_group, group_link.group)
            redirect_to group_path(group_link.group), status: :found
          elsif can?(current_user, :admin_project, group_link.project)
            redirect_to project_project_members_path(project), status: :found
          end
        end
        format.js { head :ok }
      end
    else
      respond_to do |format|
        format.html do
          redirect_to project_project_members_path(project, tab: :groups), status: :found,
            alert: _('The project-group link could not be removed.')
        end

        format.js do
          render json: { message: result.message }, status: result.reason
        end
      end
    end
  end

  protected

  def authorize_manage_destroy!
    render_404 unless can?(current_user, :manage_destroy, group_link)
  end

  def group_link
    @project.project_group_links.find(params[:id])
  end
  strong_memoize_attr :group_link

  def group_link_params
    params.require(:group_link).permit(:group_access, :expires_at)
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

  def test_render
    @some_variable = params[:unsafe_input]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :index
  end

  def test_dynamic_render
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_modern_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_modern_param
    page = params[:page]
    #ok: ruby_file_rule-CheckRenderLocalFileInclude
    render file: File.basename("/some/path/#{page}")
  end

  def test_render_with_modern_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_old_param_second_param
    page = params[:page]
    #ruleid: ruby_file_rule-CheckRenderLocalFileInclude
    render :status => 403, :file => "/some/path/#{page}"
  end

  def test_render_with_first_positional_argument
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument_and_keyword
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_param_ok
    map = make_map
    thing = map[params.id]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :file => "/some/path/#{thing}"
  end
    


  def test_render_static_template_name
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :update, locals: { username: params[:username] }
  end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name,
                   :account_id, as: :create_params

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

  def test_render
    @some_variable = params[:unsafe_input]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :index
  end

  def test_dynamic_render
    page = params[:page]
    #ruleid: ruby_file_rule-CheckRenderLocalFileInclude
    render :file => "/some/path/#{page}"
  end

  def test_render_with_modern_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_modern_param
    page = params[:page]
    #ok: ruby_file_rule-CheckRenderLocalFileInclude
    render file: File.basename("/some/path/#{page}")
  end

  def test_render_with_modern_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_old_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument_and_keyword
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_param_ok
    map = make_map
    thing = map[params.id]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :file => "/some/path/#{thing}"
  end
    


  def test_render_static_template_name
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :update, locals: { username: params[:username] }
  end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def some_rails_controller
  foo = params[:some_regex]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def some_rails_controller
  foo = Record[something]
  #ruleid: ruby_regex_rule-CheckRegexDOS
  Regexp.new(foo).match("some_string")
end

def some_rails_controller
  foo = Record.read_attribute("some_attribute")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  bar = ENV['someEnvVar']
  #ok: ruby_regex_rule-CheckRegexDOS
  Regexp.new(bar).match("some_string")
end

def use_params_in_regex
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def regex_on_params
#ok: ruby_regex_rule-CheckRegexDOS
@x = params[:x].match /foo/
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesMD5
        digest = OpenSSL::Digest::MD5.digest 'abc'
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesMD5
        digest = OpenSSL::Digest::MD5.base64digest 'abc'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'open3'

def test_params()
  user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  commands = "ls -lah /raz/dva"
# ok: ruby_injection_rule-DangerousExec
  system(commands)

  cmd_name = "sh"
# ok: ruby_injection_rule-DangerousExec
  Process.exec([cmd_name, "ls", "-la"])
# ok: ruby_injection_rule-DangerousExec
  Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
# ok: ruby_injection_rule-DangerousExec
  system("ls -lah /tmp")
# ok: ruby_injection_rule-DangerousExec
  exec(["ls", "-lah", "/tmp"])
end

def test_calls(user_input)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
  # ruleid: ruby_injection_rule-DangerousExec
    output = exec(["sh", "-c", user_input])
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end

  def test_params()
    user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end
  
  def test_cookies()
    user_input = cookies['some_cookie']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
      commands = "ls -lah /raz/dva"
    # ok: ruby_injection_rule-DangerousExec
      system(commands)
    
      cmd_name = "sh"
    # ok: ruby_injection_rule-DangerousExec
      Process.exec([cmd_name, "ls", "-la"])
    # ok: ruby_injection_rule-DangerousExec
      Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
    # ok: ruby_injection_rule-DangerousExec
      system("ls -lah /tmp")
    # ok: ruby_injection_rule-DangerousExec
      exec(["ls", "-lah", "/tmp"])
    end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesSHA1
        sha = Digest::SHA1.digest 'abc'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ok: ruby_crypto_rule-WeakHashesSHA1
        OpenSSL::HMAC.hexdigest("SHA256", key, data)
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.new
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.hexdigest 'abc'
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :age, :admin)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ruleid: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : params[:blah]}'")
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

  def test_render
    @some_variable = params[:unsafe_input]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :index
  end

  def test_dynamic_render
    page = params[:page]
    #ruleid: ruby_file_rule-CheckRenderLocalFileInclude
    render :file => "/some/path/#{page}"
  end

  def test_render_with_modern_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_modern_param
    page = params[:page]
    #ok: ruby_file_rule-CheckRenderLocalFileInclude
    render file: File.basename("/some/path/#{page}")
  end

  def test_render_with_modern_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_old_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument_and_keyword
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_param_ok
    map = make_map
    thing = map[params.id]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :file => "/some/path/#{thing}"
  end
    


  def test_render_static_template_name
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :update, locals: { username: params[:username] }
  end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :age, :admin)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def test_send_file
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file2
    # ruleid: ruby_file_rule-CheckSendFile
    send_file cookies[:something]
end

def test_send_file3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file4
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file5
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.encrypted[:something]
end

def test_send_file6
    # this is reported since semgrep 0.94 because . ... . can now match
    # intermediate fields, not just method calls.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file7
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.permanent.signed[:something]
end

def test_send_file8
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file_ok
    # ok: ruby_file_rule-CheckSendFile
    send_file "some_safe_file.txt"
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    #ruleid: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name, " + params[:column])
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
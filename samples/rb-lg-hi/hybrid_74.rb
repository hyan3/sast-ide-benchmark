class Projects::FeatureFlagsController < Projects::ApplicationController
  respond_to :html

  before_action :authorize_read_feature_flag!
  before_action :authorize_create_feature_flag!, only: [:new, :create]
  before_action :authorize_update_feature_flag!, only: [:edit, :update]
  before_action :authorize_destroy_feature_flag!, only: [:destroy]

  before_action :feature_flag, only: [:edit, :update, :destroy]

  feature_category :feature_flags
  urgency :low

  def index
    @feature_flags = FeatureFlagsFinder
      .new(project, current_user, scope: params[:scope])
      .execute
      .page(params[:page])
      .per(30)

    respond_to do |format|
      format.html
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render json: { feature_flags: feature_flags_json }.merge(summary_json)
      end
    end
  end

  def new; end

  def show
    respond_to do |format|
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render_success_json(feature_flag)
      end
    end
  end

  def create
    result = FeatureFlags::CreateService.new(project, current_user, create_params).execute

    if result[:status] == :success
      respond_to do |format|
        format.json { render_success_json(result[:feature_flag]) }
      end
    else
      respond_to do |format|
        format.json { render_error_json(result[:message]) }
      end
    end
  end

  def edit; end

  def update
    result = FeatureFlags::UpdateService.new(project, current_user, update_params).execute(feature_flag)

    if result[:status] == :success
      respond_to do |format|
        format.json { render_success_json(result[:feature_flag]) }
      end
    else
      respond_to do |format|
        format.json { render_error_json(result[:message], result[:http_status]) }
      end
    end
  end

  def destroy
    result = FeatureFlags::DestroyService.new(project, current_user).execute(feature_flag)

    if result[:status] == :success
      respond_to do |format|
        format.html { redirect_to_index(notice: _('Feature flag was successfully removed.')) }
        format.json { render_success_json(feature_flag) }
      end
    else
      respond_to do |format|
        format.html { redirect_to_index(alert: _('Feature flag was not removed.')) }
        format.json { render_error_json(result[:message]) }
      end
    end
  end

  protected

  def feature_flag
    @feature_flag ||= @noteable = project.operations_feature_flags.find_by_iid!(params[:iid])
  end

  def create_params
    params.require(:operations_feature_flag).permit(
      :name,
      :description,
      :active,
      :version,
      scopes_attributes: [
        :environment_scope, :active,
        { strategies: [:name, { parameters: [:groupId, :percentage, :userIds] }] }
      ],
      strategies_attributes: [
        :name,
        :user_list_id,
        { parameters: [:groupId, :percentage, :userIds, :rollout, :stickiness],
          scopes_attributes: [:environment_scope] }
      ]
    )
  end

  def update_params
    params.require(:operations_feature_flag).permit(
      :name,
      :description,
      :active,
      scopes_attributes: [
        :id,
        :environment_scope,
        :active,
        :_destroy,
        { strategies: [:name, { parameters: [:groupId, :percentage, :userIds] }] }
      ],
      strategies_attributes: [
        :id,
        :name,
        :user_list_id,
        :_destroy,
        { parameters: [:groupId, :percentage, :userIds, :rollout, :stickiness],
          scopes_attributes: [:id, :environment_scope, :_destroy] }
      ]
    )
  end

  def feature_flag_json(feature_flag)
    FeatureFlagSerializer
      .new(project: @project, current_user: @current_user)
      .represent(feature_flag)
  end

  def feature_flags_json
    FeatureFlagSerializer
      .new(project: @project, current_user: @current_user)
      .with_pagination(request, response)
      .represent(@feature_flags)
  end

  def summary_json
    FeatureFlagSummarySerializer
      .new(project: @project, current_user: @current_user)
      .represent(@project)
  end

  def redirect_to_index(**args)
    redirect_to project_feature_flags_path(@project), status: :found, **args
  end

  def render_success_json(feature_flag)
    render json: feature_flag_json(feature_flag), status: :ok
  end

  def render_error_json(messages, status = :bad_request)
    render json: { message: messages }, status: status
  end
end

class Projects::MirrorsController < Projects::ApplicationController
  include RepositorySettingsRedirect

  # Authorize
  before_action :remote_mirror, only: [:update]
  before_action :check_mirror_available!
  before_action :authorize_admin_project!

  layout "project_settings"

  feature_category :source_code_management

  def show
    redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
  end

  def update
    if push_mirror_create_or_destroy?
      result = execute_push_mirror_service

      if result.success?
        flash[:notice] = notice_message
      else
        flash[:alert] = alert_error(result.message)
      end

      respond_to do |format|
        format.html { redirect_to_repository_settings(project, anchor: 'js-push-remote-settings') }
        format.json do
          if result.error?
            render json: result.message, status: :unprocessable_entity
          else
            render json: ProjectMirrorSerializer.new.represent(project)
          end
        end
      end
    else
      flash[:alert] = alert_error('Invalid mirror update request')

      respond_to do |format|
        format.html { redirect_to_repository_settings(project, anchor: 'js-push-remote-settings') }
        format.json do
          render json: { error: flash[:alert] }, status: :bad_request
        end
      end
    end
  end

  def update_now
    if params[:sync_remote]
      project.update_remote_mirrors
      flash[:notice] = _("The remote repository is being updated...")
    end

    redirect_to_repository_settings(project, anchor: 'js-push-remote-settings')
  end

  def ssh_host_keys
    lookup = SshHostKey.new(project: project, url: params[:ssh_url], compare_host_keys: params[:compare_host_keys])

    if lookup.error.present?
      # Failed to read keys
      render json: { message: lookup.error }, status: :bad_request
    elsif lookup.known_hosts.nil?
      # Still working, come back later
      render body: nil, status: :no_content
    else
      render json: lookup
    end
  rescue ArgumentError => e
    render json: { message: e.message }, status: :bad_request
  end

  private

  def push_mirror_create_or_destroy?
    push_mirror_create? || push_mirror_destroy?
  end

  def push_mirror_create?
    push_mirror_attributes.present?
  end

  def push_mirror_destroy?
    ::Gitlab::Utils.to_boolean(mirror_params.dig(:remote_mirrors_attributes, '_destroy'))
  end

  def push_mirror_attributes
    mirror_params.dig(:remote_mirrors_attributes, '0')
  end

  def execute_push_mirror_service
    if push_mirror_create?
      return ::RemoteMirrors::CreateService.new(project, current_user, push_mirror_attributes).execute
    end

    return unless push_mirror_destroy?

    ::RemoteMirrors::DestroyService.new(project, current_user).execute(push_mirror_to_destroy)
  end

  def safe_mirror_params
    mirror_params
  end

  def notice_message
    _('Mirroring settings were successfully updated.')
  end

  def push_mirror_to_destroy
    push_mirror_to_destroy_id = safe_mirror_params.dig(:remote_mirrors_attributes, 'id')

    project.remote_mirrors.find(push_mirror_to_destroy_id)
  end

  def remote_mirror
    @remote_mirror = project.remote_mirrors.first_or_initialize
  end

  def check_mirror_available!
    render_404 unless can?(current_user, :admin_remote_mirror, project)
  end

  def mirror_params_attributes
    [
      remote_mirrors_attributes: %i[
        url
        id
        enabled
        only_protected_branches
        keep_divergent_refs
        auth_method
        user
        password
        ssh_known_hosts
        regenerate_ssh_private_key
        _destroy
      ]
    ]
  end

  def mirror_params
    params.require(:project).permit(mirror_params_attributes)
  end

  def alert_error(error)
    return error.full_messages.to_sentence if error.respond_to?(:full_messages)

    error
  end
end

class IssuesFinder < IssuableFinder
  extend ::Gitlab::Utils::Override

  def self.scalar_params
    @scalar_params ||= super + [:due_date]
  end

  def klass
    Issue
  end

  def params_class
    self.class.const_get(:Params, false)
  end

  private

  def filter_items(items)
    issues = super
    issues = by_due_date(issues)
    issues = by_due_after_or_before(issues)
    issues = by_confidential(issues)
    by_issue_types(issues)
  end

  # Negates all params found in `negatable_params`
  def filter_negated_items(items)
    issues = super
    by_negated_issue_types(issues)
  end

  override :filter_by_full_text_search
  def filter_by_full_text_search(items)
    # This project condition is used as a hint to PG about the partitions that need searching
    # because the search data is partitioned by project.
    # In certain cases, like the recent items search, the query plan is much better without this condition.
    return super if params[:skip_full_text_search_project_condition].present?

    super.with_projects_matching_search_data
  end

  def by_confidential(items)
    Issues::ConfidentialityFilter.new(
      current_user: current_user,
      params: original_params,
      parent: params.parent,
      assignee_filter: assignee_filter
    ).filter(items)
  end

  def by_due_after_or_before(items)
    items = items.due_after(params[:due_after]) if params[:due_after].present?
    items = items.due_before(params[:due_before]) if params[:due_before].present?

    items
  end

  def by_due_date(items)
    return items unless params.due_date?

    if params.filter_by_no_due_date?
      items.without_due_date
    elsif params.filter_by_any_due_date?
      items.with_due_date
    elsif params.filter_by_overdue?
      items.due_before(Date.today)
    elsif params.filter_by_due_today?
      items.due_today
    elsif params.filter_by_due_tomorrow?
      items.due_tomorrow
    elsif params.filter_by_due_this_week?
      items.due_between(Date.today.beginning_of_week, Date.today.end_of_week)
    elsif params.filter_by_due_this_month?
      items.due_between(Date.today.beginning_of_month, Date.today.end_of_month)
    elsif params.filter_by_due_next_month_and_previous_two_weeks?
      items.due_between(Date.today - 2.weeks, (Date.today + 1.month).end_of_month)
    else
      items.none
    end
  end

  def by_issue_types(items)
    issue_type_params = Array(params[:issue_types]).map(&:to_s)
    return items if issue_type_params.blank?
    return klass.none unless (WorkItems::Type.base_types.keys & issue_type_params).sort == issue_type_params.sort

    items.with_issue_type(params[:issue_types])
  end

  def by_negated_issue_types(items)
    issue_type_params = Array(not_params[:issue_types]).map(&:to_s) & WorkItems::Type.base_types.keys
    return items if issue_type_params.blank?

    items.without_issue_type(issue_type_params)
  end
end

class PersonalAccessTokensController < ApplicationController
    include RenderAccessTokens
    include FeedTokenHelper

    feature_category :system_access

    before_action :check_personal_access_tokens_enabled
    prepend_before_action(only: [:index]) { authenticate_sessionless_user!(:ics) }

    def index
      set_index_vars
      scopes = params[:scopes].split(',').map(&:squish).select(&:present?).map(&:to_sym) unless params[:scopes].nil?
      @personal_access_token = finder.build(
        name: params[:name],
        scopes: scopes
      )

      respond_to do |format|
        format.html
        format.json do
          render json: @active_access_tokens
        end
        format.ics do
          if params[:feed_token]
            response.headers['Content-Type'] = 'text/plain'
            render plain: expiry_ics(@active_access_tokens)
          else
            redirect_to "#{request.path}?feed_token=#{generate_feed_token_with_path(:ics, request.path)}"
          end
        end
      end
    end

    def create
      result = ::PersonalAccessTokens::CreateService.new(
        current_user: current_user,
        target_user: current_user,
        organization_id: Current.organization_id,
        params: personal_access_token_params,
        concatenate_errors: false
      ).execute

      @personal_access_token = result.payload[:personal_access_token]

      tokens, size = active_access_tokens
      if result.success?
        render json: { new_token: @personal_access_token.token,
                       active_access_tokens: tokens, total: size }, status: :ok
      else
        render json: { errors: result.errors }, status: :unprocessable_entity
      end
    end

    def revoke
      @personal_access_token = finder.find(params[:id])
      service = PersonalAccessTokens::RevokeService.new(current_user, token: @personal_access_token).execute
      service.success? ? flash[:notice] = service.message : flash[:alert] = service.message

      redirect_to user_settings_personal_access_tokens_path
    end

    private

    def finder(options = {})
      PersonalAccessTokensFinder.new({ user: current_user, impersonation: false }.merge(options))
    end

    def personal_access_token_params
      params.require(:personal_access_token).permit(:name, :expires_at, scopes: [])
    end

    def set_index_vars
      @scopes = Gitlab::Auth.available_scopes_for(current_user)
      @active_access_tokens, @active_access_tokens_size = active_access_tokens
    end

    def represent(tokens)
      ::PersonalAccessTokenSerializer.new.represent(tokens)
    end

    def check_personal_access_tokens_enabled
      render_404 if Gitlab::CurrentSettings.personal_access_tokens_disabled?
    end
  end

class VariablesController < Groups::ApplicationController
    before_action :authorize_admin_group!, except: :update
    before_action :authorize_admin_cicd_variables!, only: :update

    skip_cross_project_access_check :show, :update

    feature_category :secrets_management

    urgency :low, [:show]

    def show
      respond_to do |format|
        format.json do
          render status: :ok, json: { variables: ::Ci::GroupVariableSerializer.new.represent(@group.variables) }
        end
      end
    end

    def update
      update_result = Ci::ChangeVariablesService.new(
        container: @group, current_user: current_user,
        params: group_variables_params
      ).execute

      if update_result
        respond_to do |format|
          format.json { render_group_variables }
        end
      else
        respond_to do |format|
          format.json { render_error }
        end
      end
    end

    private

    def render_group_variables
      render status: :ok, json: { variables: ::Ci::GroupVariableSerializer.new.represent(@group.variables) }
    end

    def render_error
      render status: :bad_request, json: @group.errors.full_messages
    end

    def group_variables_params
      params.permit(variables_attributes: Array(variable_params_attributes))
    end

    def variable_params_attributes
      %i[id variable_type key description secret_value protected masked hidden raw _destroy]
    end
  end

class Projects::VariablesController < Projects::ApplicationController
  before_action :authorize_admin_build!, except: :update
  before_action :authorize_admin_cicd_variables!, only: :update

  feature_category :secrets_management

  urgency :low, [:show, :update]

  def show
    respond_to do |format|
      format.json do
        render status: :ok, json: { variables: ::Ci::VariableSerializer.new.represent(@project.variables) }
      end
    end
  end

  def update
    update_result = Ci::ChangeVariablesService.new(
      container: @project, current_user: current_user,
      params: variables_params
    ).execute

    if update_result
      respond_to do |format|
        format.json { render_variables }
      end
    else
      respond_to do |format|
        format.json { render_error }
      end
    end
  end

  private

  def render_variables
    render status: :ok, json: { variables: ::Ci::VariableSerializer.new.represent(@project.variables) }
  end

  def render_error
    render status: :bad_request, json: @project.errors.full_messages
  end

  def variables_params
    params.permit(variables_attributes: Array(variable_params_attributes))
  end

  def variable_params_attributes
    %i[id variable_type key description secret_value protected masked hidden raw environment_scope _destroy]
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesSHA1
        sha = Digest::SHA1.new
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ok: ruby_crypto_rule-WeakHashesSHA1
        OpenSSL::HMAC.hexdigest("SHA256", key, data)
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.new
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.hexdigest 'abc'
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'net/ftp'

def foo

  host = params[:host]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.open(params[:host])

  ftp = Net::FTP.new()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.new("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.open("example.com")

  ftp = Net::FTP.new()
  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.connect("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.get("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.getbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.gettextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.put("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.putbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.puttextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.delete("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storlines("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storbinary("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.sendcmd("ls -al")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.retrlines("ls -al")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

uid = params[:uid]
# ruleid: ruby_session_rule-AvoidSessionManipulation
id = session[uid]

# ok: ruby_session_rule-AvoidSessionManipulation
id = session[user_id]
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
    o = Klass.new("hello\n")
    data = params['data']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = YAML.load(data)

    o = Klass.new("hello\n")
    data = cookies['some_field']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ruleid: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(data)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(data,options=some_safe_options)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def divide_by_zero
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   variable = 3
   # ruleid: ruby_error_rule-DivideByZero
   oops = variable / 0

   zero = 0
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   # ok: ruby_error_rule-DivideByZero
   ok = 1.0 / 0
   # ok: ruby_error_rule-DivideByZero
   ok2 = 2.0 / zero
   
 end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def some_rails_controller
  foo = params[:some_regex]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def some_rails_controller
  foo = Record[something]
  #ruleid: ruby_regex_rule-CheckRegexDOS
  Regexp.new(foo).match("some_string")
end

def some_rails_controller
  foo = Record.read_attribute("some_attribute")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  bar = ENV['someEnvVar']
  #ok: ruby_regex_rule-CheckRegexDOS
  Regexp.new(bar).match("some_string")
end

def use_params_in_regex
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def regex_on_params
#ok: ruby_regex_rule-CheckRegexDOS
@x = params[:x].match /foo/
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Account < ActiveRecord::Base
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  #ruleid: ruby_regex_rule-CheckValidationRegex
  validates :first_name, :format => /\w+/
  serialize :cc_info #safe from CVE-2013-0277
  attr_accessible :blah_admin_blah
end

class Account < ActiveRecord::Base
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  #ok: ruby_regex_rule-CheckValidationRegex
  validates_format_of :good_valid, :with => /\A[a-zA-Z]\z/ #No warning
  #ok: ruby_regex_rule-CheckValidationRegex
  validates_format_of :not_bad, :with => /\A[a-zA-Z]\Z/ #No warning

  def mass_assign_it
    Account.new(params[:account_info]).some_other_method
  end

  def test_class_eval
    #Should not raise a warning
    User.class_eval do
      attr_reader :some_private_thing
    end
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization

    o = Klass.new("hello\n")
    data = YAML.dump(o)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ruleid: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.load_stream(data)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

 end

 def ok_deserialization
    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.load(data, safe: true)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.load_stream(data, safe: true)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    obj = YAML.parse_stream(data, safe: true)

    filename = File.read("test.txt")
    data = YAML.dump(filename)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load(filename)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load_stream(filename)
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.parse_stream(filename)

    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load(File.read("test.txt"))
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.load_stream(File.read("test.txt"))
    # ok: ruby_deserialization_rule-BadDeserializationYAML
    YAML.parse_stream(File.read("test.txt"))
 end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA1.base64digest 'abc'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ok: ruby_crypto_rule-WeakHashesSHA1
        OpenSSL::HMAC.hexdigest("SHA256", key, data)
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.new
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.hexdigest 'abc'
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
    o = Klass.new("hello\n")
    data = params['data']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = YAML.load(data)

    o = Klass.new("hello\n")
    data = cookies['some_field']
    # ruleid: ruby_deserialization_rule-BadDeserialization
    obj = Oj.object_load(data)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(data,options=some_safe_options)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def test_send_file
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file2
    # ruleid: ruby_file_rule-CheckSendFile
    send_file cookies[:something]
end

def test_send_file3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file4
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file5
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.encrypted[:something]
end

def test_send_file6
    # this is reported since semgrep 0.94 because . ... . can now match
    # intermediate fields, not just method calls.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file7
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.permanent.signed[:something]
end

def test_send_file8
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file_ok
    # ok: ruby_file_rule-CheckSendFile
    send_file "some_safe_file.txt"
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def some_rails_controller
  foo = params[:some_regex]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def some_rails_controller
  foo = Record[something]
  #ruleid: ruby_regex_rule-CheckRegexDOS
  Regexp.new(foo).match("some_string")
end

def some_rails_controller
  foo = Record.read_attribute("some_attribute")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  bar = ENV['someEnvVar']
  #ok: ruby_regex_rule-CheckRegexDOS
  Regexp.new(bar).match("some_string")
end

def use_params_in_regex
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def regex_on_params
#ok: ruby_regex_rule-CheckRegexDOS
@x = params[:x].match /foo/
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :account_id, :age)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'net/ftp'

def foo

  host = params[:host]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  ftp = Net::FTP.new()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.puttextfile("/tmp/#{params[:file]}")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.new("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.open("example.com")

  ftp = Net::FTP.new()
  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.connect("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.get("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.getbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.gettextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.put("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.putbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.puttextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.delete("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storlines("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storbinary("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.sendcmd("ls -al")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.retrlines("ls -al")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
require 'net/http'

def foo

  url = params[:url]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:url])
  # ruleid: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.post(uri)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:server])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get("example.com", "/index.html")

  uri = URI("example.com/index.html")
  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(uri)

end
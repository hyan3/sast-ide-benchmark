class RepositoryController < Groups::ApplicationController
      layout 'group_settings'
      skip_cross_project_access_check :show
      before_action :authorize_create_deploy_token!, only: :create_deploy_token
      before_action :authorize_access!, only: :show
      before_action :define_deploy_token_variables, if: -> { can?(current_user, :create_deploy_token, @group) }

      before_action do
        push_frontend_ability(ability: :admin_group, resource: @group, user: current_user)
      end

      feature_category :continuous_delivery
      urgency :low

      def create_deploy_token
        result = Groups::DeployTokens::CreateService.new(@group, current_user, deploy_token_params).execute

        if result[:status] == :success
          @created_deploy_token = result[:deploy_token]
          respond_to do |format|
            format.json do
              # IMPORTANT: It's a security risk to expose the token value more than just once here!
              json = API::Entities::DeployTokenWithToken.represent(@created_deploy_token).as_json
              render json: json, status: result[:http_status]
            end
            format.html do
              flash.now[:notice] = s_('DeployTokens|Your new group deploy token has been created.')
              render :show
            end
          end
        else
          @new_deploy_token = result[:deploy_token]
          respond_to do |format|
            format.json { render json: { message: result[:message] }, status: result[:http_status] }
            format.html do
              flash.now[:alert] = result[:message]
              render :show
            end
          end
        end
      end

      private

      def authorize_access!
        authorize_admin_group!
      end

      def define_deploy_token_variables
        @deploy_tokens = @group.deploy_tokens.active

        @new_deploy_token = DeployToken.new
      end

      def deploy_token_params
        params.require(:deploy_token).permit(:name, :expires_at, :read_repository, :read_registry, :write_registry,
          :read_package_registry, :write_package_registry, :username)
      end
    end

class Import::BulkImportsController < ApplicationController
  include ActionView::Helpers::SanitizeHelper

  before_action :ensure_bulk_import_enabled
  before_action :verify_blocked_uri, only: :status
  before_action :bulk_import, only: [:history, :failures]

  feature_category :importers
  urgency :low

  POLLING_INTERVAL = 3_000

  rescue_from BulkImports::Error, with: :bulk_import_connection_error

  def configure
    session[access_token_key] = configure_params[access_token_key]&.strip
    session[url_key] = configure_params[url_key]

    verify_blocked_uri && performed? && return
    validate_configure_params!

    redirect_to status_import_bulk_imports_url(namespace_id: params[:namespace_id])
  end

  def status
    respond_to do |format|
      format.json do
        data = ::BulkImports::GetImportableDataService.new(params, query_params, credentials).execute

        pagination_headers.each do |header|
          response.set_header(header, data[:response].headers[header])
        end

        json_response = { importable_data: serialized_data(data[:response].parsed_response) }
        json_response[:version_validation] = data[:version_validation]

        render json: json_response
      end
      format.html do
        if params[:namespace_id]
          @namespace = Namespace.find_by_id(params[:namespace_id])

          render_404 unless current_user.can?(:create_subgroup, @namespace)
        end

        @source_url = session[url_key]
      end
    end
  end

  def history; end

  def failures
    bulk_import_entity
  end

  def create
    return render json: { success: false }, status: :too_many_requests if throttled_request?
    return render json: { success: false }, status: :unprocessable_entity unless valid_create_params?

    responses = create_params.map do |entry|
      if entry[:destination_name]
        entry[:destination_slug] ||= entry[:destination_name]
        entry.delete(:destination_name)
      end

      ::BulkImports::CreateService.new(current_user, entry, credentials).execute
    end

    render json: responses.map { |response| { success: response.success?, id: response.payload[:id], message: response.message } }
  end

  def realtime_changes
    Gitlab::PollingInterval.set_header(response, interval: POLLING_INTERVAL)

    render json: current_user_bulk_imports.to_json(only: [:id], methods: [:status_name, :has_failures])
  end

  private

  def bulk_import
    return unless params[:id]

    @bulk_import ||= BulkImport.find(params[:id])
    @bulk_import || render_404
  end

  def bulk_import_entity
    @bulk_import_entity ||= @bulk_import.entities.find(params[:entity_id])
  end

  def pagination_headers
    %w[x-next-page x-page x-per-page x-prev-page x-total x-total-pages]
  end

  def serialized_data(data)
    serializer.represent(data, {}, Import::BulkImportEntity)
  end

  def serializer
    @serializer ||= BaseSerializer.new(current_user: current_user)
  end

  # Default query string params used to fetch groups from GitLab source instance
  #
  # top_level_only: fetch only top level groups (subgroups are fetched during import itself)
  # min_access_level: fetch only groups user has maintainer or above permissions
  # search: optional search param to search user's groups by a keyword
  def query_params
    query_params = {
      top_level_only: true,
      min_access_level: Gitlab::Access::OWNER
    }

    query_params[:search] = sanitized_filter_param if sanitized_filter_param
    query_params
  end

  def configure_params
    params.permit(access_token_key, url_key)
  end

  def validate_configure_params!
    client = BulkImports::Clients::HTTP.new(
      url: credentials[:url],
      token: credentials[:access_token]
    )

    client.validate_instance_version!
    client.validate_import_scopes!
  end

  def create_params
    params.permit(bulk_import: bulk_import_params)[:bulk_import]
  end

  def valid_create_params?
    create_params.all? { _1[:source_type] == 'group_entity' }
  end

  def bulk_import_params
    %i[
      source_type
      source_full_path
      destination_name
      destination_slug
      destination_namespace
      migrate_projects
      migrate_memberships
    ]
  end

  def ensure_bulk_import_enabled
    render_404 unless Gitlab::CurrentSettings.bulk_import_enabled? ||
      Feature.enabled?(:override_bulk_import_disabled, current_user, type: :ops)
  end

  def access_token_key
    :bulk_import_gitlab_access_token
  end

  def url_key
    :bulk_import_gitlab_url
  end

  def verify_blocked_uri
    Gitlab::HTTP_V2::UrlBlocker.validate!(
      session[url_key],
      allow_localhost: allow_local_requests?,
      allow_local_network: allow_local_requests?,
      schemes: %w[http https],
      deny_all_requests_except_allowed: Gitlab::CurrentSettings.deny_all_requests_except_allowed?,
      outbound_local_requests_allowlist: Gitlab::CurrentSettings.outbound_local_requests_whitelist # rubocop:disable Naming/InclusiveLanguage -- existing setting
    )
  rescue Gitlab::HTTP_V2::UrlBlocker::BlockedUrlError => e
    clear_session_data

    redirect_to new_group_path(anchor: 'import-group-pane'), alert: _('Specified URL cannot be used: "%{reason}"') % { reason: e.message }
  end

  def allow_local_requests?
    Gitlab::CurrentSettings.allow_local_requests_from_web_hooks_and_services?
  end

  def bulk_import_connection_error(error)
    clear_session_data

    error_message = _("Unable to connect to server: %{error}") % { error: error }
    flash[:alert] = error_message

    respond_to do |format|
      format.json do
        render json: {
          error: {
            message: error_message,
            redirect: new_group_path
          }
        }, status: :unprocessable_entity
      end
      format.html do
        redirect_to new_group_path(anchor: 'import-group-pane')
      end
    end
  end

  def clear_session_data
    session[url_key] = nil
    session[access_token_key] = nil
  end

  def credentials
    {
      url: session[url_key],
      access_token: session[access_token_key]
    }
  end

  def sanitized_filter_param
    @filter ||= sanitize(params[:filter])&.downcase
  end

  def current_user_bulk_imports
    current_user.bulk_imports.gitlab
  end

  def throttled_request?
    ::Gitlab::ApplicationRateLimiter.throttled_request?(request, current_user, :bulk_import, scope: current_user)
  end
end

class Projects::JobsController < Projects::ApplicationController
  include Ci::AuthBuildTrace
  include SendFileUpload
  include ContinueParams
  include ProjectStatsRefreshConflictsGuard

  urgency :low, [:index, :show, :trace, :retry, :play, :cancel, :unschedule, :erase, :viewer, :raw, :test_report_summary]

  before_action :find_job_as_build, except: [:index, :play, :retry, :show]
  before_action :find_job_as_processable, only: [:play, :retry, :show]
  before_action :authorize_read_build_trace!, only: [:trace, :viewer, :raw]
  before_action :authorize_read_build!, except: [:test_report_summary]
  before_action :authorize_read_build_report_results!, only: [:test_report_summary]
  before_action :authorize_update_build!,
    except: [:index, :show, :viewer, :raw, :trace, :erase, :cancel, :unschedule, :test_report_summary]
  before_action :authorize_cancel_build!, only: [:cancel]
  before_action :authorize_erase_build!, only: [:erase]
  before_action :authorize_use_build_terminal!, only: [:terminal, :terminal_websocket_authorize]
  before_action :verify_api_request!, only: :terminal_websocket_authorize
  before_action :authorize_create_proxy_build!, only: :proxy_websocket_authorize
  before_action :verify_proxy_request!, only: :proxy_websocket_authorize
  before_action :reject_if_build_artifacts_size_refreshing!, only: [:erase]
  before_action :push_filter_by_name, only: [:index]
  layout 'project'

  feature_category :continuous_integration
  urgency :low

  def index; end

  def show
    if @build.instance_of?(::Ci::Bridge)
      redirect_to project_pipeline_path(@build.downstream_pipeline.project, @build.downstream_pipeline.id)
    end

    respond_to do |format|
      format.html
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render json: Ci::JobSerializer
          .new(project: @project, current_user: @current_user)
          .represent(
            @build.present(current_user: current_user),
            {
              # Pipeline will show all failed builds by default if not using disable_failed_builds
              disable_coverage: true,
              disable_failed_builds: true
            },
            BuildDetailsEntity
          )
      end
    end
  end

  def trace
    @build.trace.being_watched! if @build.running?

    if @build.has_trace?
      @build.trace.read do |stream|
        respond_to do |format|
          format.json do
            build_trace = Ci::BuildTrace.new(
              build: @build,
              stream: stream,
              state: params[:state])

            render json: BuildTraceSerializer
              .new(project: @project, current_user: @current_user)
              .represent(build_trace)
          end
        end
      end
    else
      head :no_content
    end
  end

  def retry
    Gitlab::QueryLimiting.disable!('https://gitlab.com/gitlab-org/gitlab/-/issues/424184')

    response = Ci::RetryJobService.new(project, current_user).execute(@build)

    if response.success?
      if @build.is_a?(::Ci::Build)
        redirect_to build_path(response[:job])
      else
        head :ok
      end
    else
      respond_422
    end
  end

  def play
    return respond_422 unless @build.playable?

    job = @build.play(current_user, play_params[:job_variables_attributes])

    if job.is_a?(Ci::Bridge)
      redirect_to pipeline_path(job.pipeline)
    else
      redirect_to build_path(job)
    end
  end

  def cancel
    service_response = Ci::BuildCancelService.new(@build, current_user).execute

    if service_response.success?
      destination = continue_params[:to].presence || builds_project_pipeline_path(@project, @build.pipeline.id)
      redirect_to destination
    elsif service_response.http_status == :forbidden
      access_denied!
    else
      head service_response.http_status
    end
  end

  def unschedule
    service_response = Ci::BuildUnscheduleService.new(@build, current_user).execute

    if service_response.success?
      redirect_to build_path(@build)
    elsif service_response.http_status == :forbidden
      access_denied!
    else
      head service_response.http_status
    end
  end

  def erase
    service_response = Ci::BuildEraseService.new(@build, current_user).execute

    if service_response.success?
      redirect_to project_job_path(project, @build), notice: _("Job has been successfully erased!")
    else
      head service_response.http_status
    end
  end

  def raw
    if @build.trace.archived?
      workhorse_set_content_type!
      send_upload(@build.job_artifacts_trace.file, send_params: raw_send_params, redirect_params: raw_redirect_params, proxy: params[:proxy])
    else
      @build.trace.read do |stream|
        if stream.file?
          workhorse_set_content_type!
          send_file stream.path, type: 'text/plain; charset=utf-8', disposition: 'inline'
        else
          # In this case we can't use workhorse_set_content_type! and let
          # Workhorse handle the response because the data is streamed directly
          # to the user but, because we have the trace content, we can calculate
          # the proper content type and disposition here.
          raw_data = stream.raw
          send_data raw_data, type: 'text/plain; charset=utf-8', disposition: raw_trace_content_disposition(raw_data), filename: 'job.log'
        end
      end
    end
  end

  def viewer; end

  def test_report_summary
    return not_found unless @build.report_results.present?

    summary = Gitlab::Ci::Reports::TestReportSummary.new(@build.report_results)

    respond_to do |format|
      format.json do
        render json: TestReportSummarySerializer
                       .new(project: project, current_user: @current_user)
                       .represent(summary)
      end
    end
  end

  def terminal; end

  # GET .../terminal.ws : implemented in gitlab-workhorse
  def terminal_websocket_authorize
    set_workhorse_internal_api_content_type
    render json: Gitlab::Workhorse.channel_websocket(@build.terminal_specification)
  end

  def proxy_websocket_authorize
    render json: proxy_websocket_service(build_service_specification)
  end

  private

  attr_reader :build

  def authorize_read_build_report_results!
    access_denied! unless can?(current_user, :read_build_report_results, build)
  end

  def authorize_update_build!
    access_denied! unless can?(current_user, :update_build, @build)
  end

  def authorize_cancel_build!
    access_denied! unless can?(current_user, :cancel_build, @build)
  end

  def authorize_erase_build!
    access_denied! unless can?(current_user, :erase_build, @build)
  end

  def authorize_use_build_terminal!
    access_denied! unless can?(current_user, :create_build_terminal, @build)
  end

  def authorize_create_proxy_build!
    access_denied! unless can?(current_user, :create_build_service_proxy, @build)
  end

  def verify_api_request!
    Gitlab::Workhorse.verify_api_request!(request.headers)
  end

  def verify_proxy_request!
    verify_api_request!
    set_workhorse_internal_api_content_type
  end

  def raw_send_params
    { type: 'text/plain; charset=utf-8', disposition: 'inline' }
  end

  def raw_redirect_params
    { query: { 'response-content-type' => 'text/plain; charset=utf-8', 'response-content-disposition' => 'inline' } }
  end

  def play_params
    params.permit(job_variables_attributes: %i[key secret_value])
  end

  def find_job_as_build
    @build = project.builds.find(params[:id])
  end

  def find_job_as_processable
    @build = project.processables.find(params[:id])
  end

  def build_path(build)
    project_job_path(build.project, build)
  end

  def raw_trace_content_disposition(raw_data)
    mime_type = Gitlab::Utils::MimeType.from_string(raw_data)

    # if mime_type is nil can also represent 'text/plain'
    return 'inline' if mime_type.nil? || mime_type == 'text/plain'

    'attachment'
  end

  def build_service_specification
    @build.service_specification(
      service: params['service'],
      port: params['port'],
      path: params['path'],
      subprotocols: proxy_subprotocol
    )
  end

  def proxy_subprotocol
    # This will allow to reuse the same subprotocol set
    # in the original websocket connection
    request.headers['HTTP_SEC_WEBSOCKET_PROTOCOL'].presence || ::Ci::BuildRunnerSession::TERMINAL_SUBPROTOCOL
  end

  # This method provides the information to Workhorse
  # about the service we want to proxy to.
  # For security reasons, in case this operation is started by JS,
  # it's important to use only sourced GitLab JS code
  def proxy_websocket_service(service)
    service[:url] = ::Gitlab::UrlHelpers.as_wss(service[:url])

    ::Gitlab::Workhorse.channel_websocket(service)
  end

  def push_filter_by_name
    push_frontend_feature_flag(:populate_and_use_build_names_table, @project)
  end
end

class Import::BitbucketServerController < Import::BaseController
  extend ::Gitlab::Utils::Override

  include ActionView::Helpers::SanitizeHelper

  before_action :verify_bitbucket_server_import_enabled
  before_action :bitbucket_auth, except: [:new, :configure]
  before_action :normalize_import_params, only: [:create]
  before_action :validate_import_params, only: [:create]

  rescue_from BitbucketServer::Connection::ConnectionError, with: :bitbucket_connection_error

  # As a basic sanity check to prevent URL injection, restrict project
  # repository input and repository slugs to allowed characters. For Bitbucket:
  #
  # Project keys must start with a letter and may only consist of ASCII letters, numbers and underscores (A-Z, a-z, 0-9, _).
  #
  # Repository names are limited to 128 characters. They must start with a
  # letter or number and may contain spaces, hyphens, underscores, and periods.
  # (https://community.atlassian.com/t5/Answers-Developer-Questions/stash-repository-names/qaq-p/499054)
  #
  # Bitbucket Server starts personal project names with a tilde.
  VALID_BITBUCKET_PROJECT_CHARS = /\A~?[\w\-\.\s]+\z/
  VALID_BITBUCKET_CHARS = /\A[\w\-\.\s]+\z/

  def new; end

  def create
    repo = client.repo(@project_key, @repo_slug)

    unless repo
      return render json: { errors: _("Project %{project_repo} could not be found") % { project_repo: "#{@project_key}/#{@repo_slug}" } }, status: :unprocessable_entity
    end

    result = Import::BitbucketServerService.new(client, current_user, params.merge({ organization_id: Current.organization_id })).execute(credentials)

    if result[:status] == :success
      render json: ProjectSerializer.new.represent(result[:project], serializer: :import)
    else
      render json: { errors: result[:message] }, status: result[:http_status]
    end
  end

  def configure
    session[personal_access_token_key] = params[:personal_access_token]
    session[bitbucket_server_username_key] = params[:bitbucket_server_username]
    session[bitbucket_server_url_key] = params[:bitbucket_server_url]

    redirect_to status_import_bitbucket_server_path(namespace_id: params[:namespace_id])
  end

  # We need to re-expose controller's internal method 'status' as action.
  # rubocop:disable Lint/UselessMethodDefinition
  def status
    super
  end
  # rubocop:enable Lint/UselessMethodDefinition

  protected

  override :importable_repos
  def importable_repos
    bitbucket_repos.filter(&:valid?)
  end

  override :incompatible_repos
  def incompatible_repos
    bitbucket_repos.reject(&:valid?)
  end

  override :provider_name
  def provider_name
    :bitbucket_server
  end

  override :provider_url
  def provider_url
    session[bitbucket_server_url_key]
  end

  private

  def client
    @client ||= BitbucketServer::Client.new(credentials)
  end

  def bitbucket_repos
    @bitbucket_repos ||= client.repos(page_offset: page_offset, limit: limit_per_page, filter: sanitized_filter_param).to_a
  end

  def normalize_import_params
    project_key, repo_slug = params[:repo_id].split('/')
    params[:bitbucket_server_project] = project_key
    params[:bitbucket_server_repo] = repo_slug
  end

  def validate_import_params
    @project_key = params[:bitbucket_server_project]
    @repo_slug = params[:bitbucket_server_repo]

    return render_validation_error('Missing project key') unless @project_key.present? && @repo_slug.present?
    return render_validation_error('Missing repository slug') unless @repo_slug.present?
    return render_validation_error('Invalid project key') unless VALID_BITBUCKET_PROJECT_CHARS.match?(@project_key)

    render_validation_error('Invalid repository slug') unless VALID_BITBUCKET_CHARS.match?(@repo_slug)
  end

  def render_validation_error(message)
    render json: { errors: message }, status: :unprocessable_entity
  end

  def bitbucket_auth
    unless session[bitbucket_server_url_key].present? &&
        session[bitbucket_server_username_key].present? &&
        session[personal_access_token_key].present?
      redirect_to new_import_bitbucket_server_path(namespace_id: params[:namespace_id])
    end
  end

  def verify_bitbucket_server_import_enabled
    render_404 unless bitbucket_server_import_enabled?
  end

  def bitbucket_server_url_key
    :bitbucket_server_url
  end

  def bitbucket_server_username_key
    :bitbucket_server_username
  end

  def personal_access_token_key
    :bitbucket_server_personal_access_token
  end

  def clear_session_data
    session[bitbucket_server_url_key] = nil
    session[bitbucket_server_username_key] = nil
    session[personal_access_token_key] = nil
  end

  def credentials
    {
      base_uri: session[bitbucket_server_url_key],
      user: session[bitbucket_server_username_key],
      password: session[personal_access_token_key]
    }
  end

  def page_offset
    [0, params[:page].to_i].max
  end

  def limit_per_page
    BitbucketServer::Paginator::PAGE_LENGTH
  end

  def bitbucket_connection_error(error)
    flash[:alert] = _("Unable to connect to server: %{error}") % { error: error }
    clear_session_data

    respond_to do |format|
      format.json do
        render json: {
          error: {
            message: _("Unable to connect to server: %{error}") % { error: error },
            redirect: new_import_bitbucket_server_path
          }
        }, status: :unprocessable_entity
      end
    end
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
   data = request.env[:name]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   o = Klass.new("hello\n")
   data = request.env[:name]
   # ruleid: ruby_deserialization_rule-BadDeserializationEnv
   obj = Oj.object_load(data)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ok: ruby_deserialization_rule-BadDeserializationEnv
   obj = Oj.load(data,options=some_safe_options)
 end

 def ok_deserialization
    
    data = get_safe_data()
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(data)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def zen
  41
end

# ok:ruby_eval_rule-NoEval
eval("def zen; 42; end")

puts zen

class Thing
end
a = %q{def hello() "Hello there!" end}
# not user-controllable, this is ok
# ok:ruby_eval_rule-NoEval
Thing.module_eval(a)
puts Thing.new.hello()
b = params['something']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def get_binding(param)
  binding
end
b = get_binding("hello")
# ok:ruby_eval_rule-NoEval
b.eval("some_func")

# ok:ruby_eval_rule-NoEval
eval("some_func",b)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid:ruby_eval_rule-NoEval
eval(cookies.delete('foo'))

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok:ruby_eval_rule-NoEval
RubyVM::InstructionSequence.compile("1 + 2").eval

iseq = RubyVM::InstructionSequence.compile(foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


iseq = RubyVM::InstructionSequence.compile('num = 1 + 2')
# ok:ruby_eval_rule-NoEval
iseq.eval

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def bad_send
    # ruleid: ruby_injection_rule-BadSend
    method = params[:method]
    @result = User.send(method.to_sym)
end

def ok_send
    # ok: ruby_injection_rule-BadSend
    method = params[:method] == 1 ? :method_a : :method_b
    @result = User.send(method, *args)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

    #ruleid: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => params[:table])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA1.digest 'abc'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ok: ruby_crypto_rule-WeakHashesSHA1
        OpenSSL::HMAC.hexdigest("SHA256", key, data)
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.new
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.hexdigest 'abc'
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
   data = request.env[:name]
   # ruleid: ruby_deserialization_rule-BadDeserializationEnv
   obj = Marshal.load(data)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   o = Klass.new("hello\n")
   data = request.env[:name]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ok: ruby_deserialization_rule-BadDeserializationEnv
   obj = Oj.load(data,options=some_safe_options)
 end

 def ok_deserialization
    
    data = get_safe_data()
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(data)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
require 'net/http'

def foo

  url = params[:url]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:url])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:server])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Post.new(URI(params[:url]))


  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get("example.com", "/index.html")

  uri = URI("example.com/index.html")
  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(uri)

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
class GroupsController < ApplicationController

  def dynamic_method_invocations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    User.method("#{User.first.some_method_thing}_stuff")
    user_input_value = params[:my_user_input]
    # ruleid: ruby_reflection_rule-CheckUnsafeReflectionMethods
    anything.tap(&user_input_value.to_sym)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def dynamic_method_invocations_ok
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    "SomeClass".to_sym.to_proc.call(Kernel)
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    SomeClass.method("some_method").("some_argument")
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    Kernel.tap("SomeClass".to_sym)
    user_input_value = params[:my_user_input]
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    user_input_value.tap("some_method")
  end

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

  def test_render
    @some_variable = params[:unsafe_input]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :index
  end

  def test_dynamic_render
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_modern_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_modern_param
    page = params[:page]
    #ok: ruby_file_rule-CheckRenderLocalFileInclude
    render file: File.basename("/some/path/#{page}")
  end

  def test_render_with_modern_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_old_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument_and_keyword
    page = params[:page]
    #ruleid: ruby_file_rule-CheckRenderLocalFileInclude
    render page, status: 403
  end

  def test_param_ok
    map = make_map
    thing = map[params.id]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :file => "/some/path/#{thing}"
  end
    


  def test_render_static_template_name
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :update, locals: { username: params[:username] }
  end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
   data = request.env[:name]
   # ruleid: ruby_deserialization_rule-BadDeserializationEnv
   obj = Marshal.load(data)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   o = Klass.new("hello\n")
   data = request.env[:name]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ok: ruby_deserialization_rule-BadDeserializationEnv
   obj = Oj.load(data,options=some_safe_options)
 end

 def ok_deserialization
    
    data = get_safe_data()
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(data)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def divide_by_zero
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   variable = 3
   # ruleid: ruby_error_rule-DivideByZero
   oops = variable / 0

   zero = 0
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   # ok: ruby_error_rule-DivideByZero
   ok = 1.0 / 0
   # ok: ruby_error_rule-DivideByZero
   ok2 = 2.0 / zero
   
 end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
    o = Klass.new("hello\n")
    data = params['data']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ruleid: ruby_deserialization_rule-BadDeserialization 
    obj = Marshal.restore(data)

    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = YAML.load(data)

    o = Klass.new("hello\n")
    data = cookies['some_field']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(data,options=some_safe_options)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def mass_assign_unsafe
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    #ruleid: ruby_mass_assignment_rule-UnprotectedMassAssign
    User.new(params[:user], :without_protection => true)
end

def safe_send
    #ok: ruby_mass_assignment_rule-UnprotectedMassAssign
    attr_accessible :name
    User.new(params[:user])

    #ok: ruby_mass_assignment_rule-UnprotectedMassAssign
    attr_accessible :name
    user = User.new(params[:user])
end
class Projects::CompareController < Projects::ApplicationController
  include DiffForPath
  include DiffHelper
  include RendersCommits
  include CompareHelper

  # Authorize
  before_action :require_non_empty_project
  before_action :authorize_read_code!
  # Defining ivars
  before_action :define_diffs, only: [:show, :diff_for_path, :rapid_diffs]
  before_action :define_environment, only: [:show, :rapid_diffs]
  before_action :define_diff_notes_disabled, only: [:show, :diff_for_path, :rapid_diffs]
  before_action :define_commits, only: [:show, :diff_for_path, :signatures, :rapid_diffs]
  before_action :merge_request, only: [:index, :show, :rapid_diffs]
  # Validation
  before_action :validate_refs!

  feature_category :source_code_management
  urgency :low, [:show, :create, :signatures]

  # Diffs may be pretty chunky, the less is better in this endpoint.
  # Pagination design guides: https://design.gitlab.com/components/pagination/#behavior
  COMMIT_DIFFS_PER_PAGE = 20

  def index
    compare_params
  end

  def show
    apply_diff_view_cookie!

    render locals: { pagination_params: params.permit(:page) }
  end

  def diff_for_path
    return render_404 unless compare

    render_diff_for_path(compare.diffs(diff_options))
  end

  def create
    from_to_vars = build_from_to_vars

    if from_to_vars[:from].blank? || from_to_vars[:to].blank?
      flash[:alert] = "You must select a Source and a Target revision"

      redirect_to project_compare_index_path(source_project, from_to_vars)
    elsif compare_params[:straight] == "true"
      redirect_to project_compare_with_two_dots_path(source_project, from_to_vars)
    else
      redirect_to project_compare_path(source_project, from_to_vars)
    end
  end

  def signatures
    respond_to do |format|
      format.json do
        render json: {
          signatures: @commits.select(&:has_signature?).map do |commit|
            {
              commit_sha: commit.sha,
              html: view_to_html_string('projects/commit/_signature', signature: commit.signature)
            }
          end
        }
      end
    end
  end

  def rapid_diffs
    return render_404 unless ::Feature.enabled?(:rapid_diffs, current_user, type: :wip)

    show
  end

  private

  def build_from_to_vars
    from_to_vars = {
      from: compare_params[:from].presence,
      to: compare_params[:to].presence
    }

    if compare_params[:from_project_id] != compare_params[:to_project_id]
      from_to_vars[:from_project_id] = compare_params[:from_project_id].presence
    end

    from_to_vars
  end

  def validate_refs!
    invalid = [head_ref, start_ref].filter { |ref| !valid_ref?(ref) }

    return if invalid.empty?

    flash[:alert] = "Invalid branch name(s): #{invalid.join(', ')}"
    redirect_to project_compare_index_path(source_project)
  end

  # target == start_ref == from
  def target_project
    strong_memoize(:target_project) do
      target_project =
        if !compare_params.key?(:from_project_id)
          source_project.default_merge_request_target
        elsif compare_params[:from_project_id].to_i == source_project.id
          source_project
        else
          target_projects(source_project).find_by_id(compare_params[:from_project_id])
        end

      # Just ignore the field if it points at a non-existent or hidden project
      next source_project unless target_project && can?(current_user, :read_code, target_project)

      target_project
    end
  end

  # source == head_ref == to
  def source_project
    strong_memoize(:source_project) do
      # Eager load project's avatar url to prevent batch loading
      # for all forked projects
      project&.tap(&:avatar_url)
    end
  end

  def compare
    return @compare if defined?(@compare)

    @compare = CompareService.new(source_project, head_ref).execute(target_project, start_ref, straight: straight)
  end

  def straight
    compare_params[:straight] == "true"
  end

  def start_ref
    @start_ref ||= Addressable::URI.unescape(compare_params[:from]).presence
  end

  def head_ref
    return @ref if defined?(@ref)

    @ref = @head_ref = Addressable::URI.unescape(compare_params[:to]).presence
  end

  def define_commits
    strong_memoize(:commits) do
      if compare.present?
        commits = compare.commits.with_markdown_cache.with_latest_pipeline(head_ref)
        set_commits_for_rendering(commits)
      else
        []
      end
    end
  end

  def define_diffs
    @diffs = compare.present? ? compare.diffs(diff_options) : []
  end

  def define_environment
    if compare
      environment_params = if source_project.repository.branch_exists?(head_ref)
                             { ref: head_ref }
                           else
                             { commit: compare.commit }
                           end

      environment_params[:find_latest] = true
      @environment = ::Environments::EnvironmentsByDeploymentsFinder.new(
        source_project,
        current_user,
        environment_params
      ).execute.last
    end
  end

  def define_diff_notes_disabled
    @diff_notes_disabled = compare.present?
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def merge_request
    @merge_request ||= MergeRequestsFinder.new(current_user, project_id: target_project.id).execute.opened
      .find_by(source_project: source_project, source_branch: head_ref, target_branch: start_ref)
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def compare_params
    @compare_params ||= params.permit(:from, :to, :from_project_id, :straight, :to_project_id)
  end
end

class UsersController < ApplicationController
  include InternalRedirect
  include RoutableActions
  include RendersMemberAccess
  include RendersProjectsList
  include ControllerWithCrossProjectAccessCheck
  include Gitlab::NoteableMetadata

  requires_cross_project_access show: false,
    groups: false,
    projects: false,
    contributed: false,
    snippets: true,
    calendar: false,
    followers: false,
    following: false,
    calendar_activities: true

  skip_before_action :authenticate_user!
  prepend_before_action(only: [:show]) { authenticate_sessionless_user!(:rss) }
  before_action :user, except: [:exists]
  before_action :set_legacy_data
  before_action :authorize_read_user_profile!, only: [
    :calendar, :calendar_activities, :groups, :projects, :contributed, :starred, :snippets, :followers, :following
  ]
  before_action only: [:exists] do
    check_rate_limit!(:username_exists, scope: request.ip)
  end
  before_action only: [:show, :activity, :groups, :projects, :contributed, :starred, :snippets, :followers, :following] do
    push_frontend_feature_flag(:profile_tabs_vue, current_user)
  end

  feature_category :user_profile, [:show, :activity, :groups, :projects, :contributed, :starred,
    :followers, :following, :calendar, :calendar_activities,
    :exists, :activity, :follow, :unfollow, :ssh_keys]

  feature_category :source_code_management, [:snippets, :gpg_keys]

  # TODO: Set higher urgency after resolving https://gitlab.com/gitlab-org/gitlab/-/issues/357914
  urgency :low, [:show, :calendar_activities, :contributed, :activity, :projects, :groups, :calendar, :snippets]
  urgency :default, [:followers, :following, :starred]
  urgency :high, [:exists]

  def show
    respond_to do |format|
      format.html

      format.atom do
        load_events
        render layout: 'xml'
      end

      format.json do
        msg = "This endpoint is deprecated. Use %s instead." % user_activity_path
        render json: { message: msg }, status: :not_found
      end
    end
  end

  # Get all keys of a user(params[:username]) in a text format
  # Helpful for sysadmins to put in respective servers
  def ssh_keys
    keys = user.all_ssh_keys.join("\n")
    keys << "\n" unless keys.empty?
    render plain: keys
  end

  def activity
    respond_to do |format|
      format.html { render 'show' }

      format.json do
        load_events

        if Feature.enabled?(:profile_tabs_vue, current_user)
          @events = if user.include_private_contributions?
                      @events
                    else
                      @events.select { |event| event.visible_to_user?(current_user) }
                    end

          render json: ::Profile::EventSerializer.new(current_user: current_user, target_user: user)
                                                 .represent(@events)
        else
          pager_json("events/_events", @events.count, events: @events)
        end
      end
    end
  end

  # Get all gpg keys of a user(params[:username]) in a text format
  def gpg_keys
    keys = user.gpg_keys.filter_map { |gpg_key| gpg_key.key if gpg_key.verified? }.join("\n")
    keys << "\n" unless keys.empty?
    render plain: keys
  end

  def groups
    respond_to do |format|
      format.html { render 'show' }
      format.json do
        load_groups

        render json: {
          html: view_to_html_string("shared/groups/_list", groups: @groups)
        }
      end
    end
  end

  def projects
    present_projects do
      load_projects
    end
  end

  def contributed
    present_projects do
      load_contributed_projects
    end
  end

  def starred
    present_projects do
      load_starred_projects
    end
  end

  def followers
    present_users do
      @user_followers = user.followers.page(params[:page])
    end
  end

  def following
    present_users do
      @user_following = user.followees.page(params[:page])
    end
  end

  def present_projects
    skip_pagination = Gitlab::Utils.to_boolean(params[:skip_pagination])
    skip_namespace = Gitlab::Utils.to_boolean(params[:skip_namespace])
    compact_mode = Gitlab::Utils.to_boolean(params[:compact_mode])
    card_mode = Gitlab::Utils.to_boolean(params[:card_mode])

    respond_to do |format|
      format.html { render 'show' }
      format.json do
        projects = yield

        pager_json("shared/projects/_list", projects.count, projects: projects, skip_pagination: skip_pagination, skip_namespace: skip_namespace, compact_mode: compact_mode, card_mode: card_mode)
      end
    end
  end

  def snippets
    respond_to do |format|
      format.html { render 'show' }
      format.json do
        load_snippets

        render json: {
          html: view_to_html_string("snippets/_snippets", collection: @snippets)
        }
      end
    end
  end

  def calendar
    render json: contributions_calendar.activity_dates
  end

  def calendar_activities
    @calendar_date = begin
      Date.parse(params[:date])
    rescue StandardError
      Date.today
    end
    @events = contributions_calendar.events_by_date(@calendar_date).map(&:present)

    render 'calendar_activities', layout: false
  end

  def exists
    if Gitlab::CurrentSettings.signup_enabled? || current_user
      render json: { exists: Namespace.username_reserved?(params[:username]) }
    else
      render json: { error: _('You must be authenticated to access this path.') }, status: :unauthorized
    end
  end

  def follow
    followee = current_user.follow(user)

    if followee
      flash[:alert] = followee.errors.full_messages.join(', ') if followee&.errors&.any?
    else
      flash[:alert] = s_('Action not allowed.')
    end

    redirect_path = referer_path(request) || @user

    redirect_to redirect_path
  end

  def unfollow
    response = ::Users::UnfollowService.new(
      follower: current_user,
      followee: user
    ).execute

    flash[:alert] = response.message if response.error?
    redirect_path = referer_path(request) || @user

    redirect_to redirect_path
  end

  private

  def user
    @user ||= find_routable!(User, params[:username], request.fullpath)
  end

  def personal_projects
    PersonalProjectsFinder.new(user).execute(current_user)
  end

  def contributed_projects
    ContributedProjectsFinder.new(
      user: user, current_user: current_user, params: { sort: 'latest_activity_desc' }
    ).execute
  end

  def starred_projects
    StarredProjectsFinder.new(user, params: finder_params, current_user: current_user).execute
  end

  def contributions_calendar
    @contributions_calendar ||= Gitlab::ContributionsCalendar.new(user, current_user)
  end

  def load_events
    @events = UserRecentEventsFinder.new(current_user, user, nil, params).execute

    Events::RenderService.new(current_user).execute(@events, atom_request: request.format.atom?)
  end

  def load_projects
    @projects = personal_projects
      .page(params[:page])
      .per(params[:limit])

    prepare_projects_for_rendering(@projects)
  end

  def load_contributed_projects
    @contributed_projects = contributed_projects.with_route.joined(user).page(params[:page]).without_count

    prepare_projects_for_rendering(@contributed_projects)
  end

  def load_starred_projects
    @starred_projects = starred_projects

    prepare_projects_for_rendering(@starred_projects)
  end

  def load_groups
    groups = JoinedGroupsFinder.new(user).execute(current_user)
    @groups = groups.page(params[:page]).without_count

    prepare_groups_for_rendering(@groups)
  end

  def load_snippets
    @snippets = SnippetsFinder.new(current_user, author: user, scope: params[:scope])
      .execute
      .page(params[:page])
      .inc_author

    @noteable_meta_data = noteable_meta_data(@snippets, 'Snippet')
  end

  def build_canonical_path(user)
    url_for(safe_params.merge(username: user.to_param))
  end

  def authorize_read_user_profile!
    access_denied! unless can?(current_user, :read_user_profile, user)
  end

  def present_users
    respond_to do |format|
      format.html { render 'show' }
      format.json do
        users = yield
        render json: {
          html: view_to_html_string("shared/users/index", users: users)
        }
      end
    end
  end

  def finder_params
    {
      # don't display projects marked for deletion
      not_aimed_for_deletion: true
    }
  end

  def set_legacy_data
    controller_action = params[:action]
    @action = controller_action.gsub('show', 'overview')
    @endpoint = request.path
  end
end

class Projects::FeatureFlagsController < Projects::ApplicationController
  respond_to :html

  before_action :authorize_read_feature_flag!
  before_action :authorize_create_feature_flag!, only: [:new, :create]
  before_action :authorize_update_feature_flag!, only: [:edit, :update]
  before_action :authorize_destroy_feature_flag!, only: [:destroy]

  before_action :feature_flag, only: [:edit, :update, :destroy]

  feature_category :feature_flags
  urgency :low

  def index
    @feature_flags = FeatureFlagsFinder
      .new(project, current_user, scope: params[:scope])
      .execute
      .page(params[:page])
      .per(30)

    respond_to do |format|
      format.html
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render json: { feature_flags: feature_flags_json }.merge(summary_json)
      end
    end
  end

  def new; end

  def show
    respond_to do |format|
      format.json do
        Gitlab::PollingInterval.set_header(response, interval: 10_000)

        render_success_json(feature_flag)
      end
    end
  end

  def create
    result = FeatureFlags::CreateService.new(project, current_user, create_params).execute

    if result[:status] == :success
      respond_to do |format|
        format.json { render_success_json(result[:feature_flag]) }
      end
    else
      respond_to do |format|
        format.json { render_error_json(result[:message]) }
      end
    end
  end

  def edit; end

  def update
    result = FeatureFlags::UpdateService.new(project, current_user, update_params).execute(feature_flag)

    if result[:status] == :success
      respond_to do |format|
        format.json { render_success_json(result[:feature_flag]) }
      end
    else
      respond_to do |format|
        format.json { render_error_json(result[:message], result[:http_status]) }
      end
    end
  end

  def destroy
    result = FeatureFlags::DestroyService.new(project, current_user).execute(feature_flag)

    if result[:status] == :success
      respond_to do |format|
        format.html { redirect_to_index(notice: _('Feature flag was successfully removed.')) }
        format.json { render_success_json(feature_flag) }
      end
    else
      respond_to do |format|
        format.html { redirect_to_index(alert: _('Feature flag was not removed.')) }
        format.json { render_error_json(result[:message]) }
      end
    end
  end

  protected

  def feature_flag
    @feature_flag ||= @noteable = project.operations_feature_flags.find_by_iid!(params[:iid])
  end

  def create_params
    params.require(:operations_feature_flag).permit(
      :name,
      :description,
      :active,
      :version,
      scopes_attributes: [
        :environment_scope, :active,
        { strategies: [:name, { parameters: [:groupId, :percentage, :userIds] }] }
      ],
      strategies_attributes: [
        :name,
        :user_list_id,
        { parameters: [:groupId, :percentage, :userIds, :rollout, :stickiness],
          scopes_attributes: [:environment_scope] }
      ]
    )
  end

  def update_params
    params.require(:operations_feature_flag).permit(
      :name,
      :description,
      :active,
      scopes_attributes: [
        :id,
        :environment_scope,
        :active,
        :_destroy,
        { strategies: [:name, { parameters: [:groupId, :percentage, :userIds] }] }
      ],
      strategies_attributes: [
        :id,
        :name,
        :user_list_id,
        :_destroy,
        { parameters: [:groupId, :percentage, :userIds, :rollout, :stickiness],
          scopes_attributes: [:id, :environment_scope, :_destroy] }
      ]
    )
  end

  def feature_flag_json(feature_flag)
    FeatureFlagSerializer
      .new(project: @project, current_user: @current_user)
      .represent(feature_flag)
  end

  def feature_flags_json
    FeatureFlagSerializer
      .new(project: @project, current_user: @current_user)
      .with_pagination(request, response)
      .represent(@feature_flags)
  end

  def summary_json
    FeatureFlagSummarySerializer
      .new(project: @project, current_user: @current_user)
      .represent(@project)
  end

  def redirect_to_index(**args)
    redirect_to project_feature_flags_path(@project), status: :found, **args
  end

  def render_success_json(feature_flag)
    render json: feature_flag_json(feature_flag), status: :ok
  end

  def render_error_json(messages, status = :bad_request)
    render json: { message: messages }, status: status
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    #ruleid: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => "LOCK #{params[:lock]}")
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'open3'

def test_params()
  user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  commands = "ls -lah /raz/dva"
# ok: ruby_injection_rule-DangerousExec
  system(commands)

  cmd_name = "sh"
# ok: ruby_injection_rule-DangerousExec
  Process.exec([cmd_name, "ls", "-la"])
# ok: ruby_injection_rule-DangerousExec
  Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
# ok: ruby_injection_rule-DangerousExec
  system("ls -lah /tmp")
# ok: ruby_injection_rule-DangerousExec
  exec(["ls", "-lah", "/tmp"])
end

def test_calls(user_input)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end

  def test_params()
    user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
  # ruleid: ruby_injection_rule-DangerousExec
    pid = spawn(["bash", user_input])
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end
  
  def test_cookies()
    user_input = cookies['some_cookie']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
      commands = "ls -lah /raz/dva"
    # ok: ruby_injection_rule-DangerousExec
      system(commands)
    
      cmd_name = "sh"
    # ok: ruby_injection_rule-DangerousExec
      Process.exec([cmd_name, "ls", "-la"])
    # ok: ruby_injection_rule-DangerousExec
      Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
    # ok: ruby_injection_rule-DangerousExec
      system("ls -lah /tmp")
    # ok: ruby_injection_rule-DangerousExec
      exec(["ls", "-lah", "/tmp"])
    end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

  def test_render
    @some_variable = params[:unsafe_input]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :index
  end

  def test_dynamic_render
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_modern_param
    page = params[:page]
    #ruleid: ruby_file_rule-CheckRenderLocalFileInclude
    render file: "/some/path/#{page}"
  end

  def test_render_with_modern_param
    page = params[:page]
    #ok: ruby_file_rule-CheckRenderLocalFileInclude
    render file: File.basename("/some/path/#{page}")
  end

  def test_render_with_modern_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_old_param_second_param
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_render_with_first_positional_argument_and_keyword
    page = params[:page]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_param_ok
    map = make_map
    thing = map[params.id]
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :file => "/some/path/#{thing}"
  end
    


  def test_render_static_template_name
    # ok: ruby_file_rule-CheckRenderLocalFileInclude
    render :update, locals: { username: params[:username] }
  end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class BaseController < ActionController::Base
  def test_redirect
    params[:action] = :index
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def redirect_to_strong_params
    # ruleid: ruby_redirect_rule-CheckRedirectTo
    redirect_to params.permit(:domain) # should warn
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to params.permit(:page, :sort) # should not warn
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to [params.permit(:domain)]
  end

  def test_only_path_wrong
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end
  def test_only_path_correct
    params.merge! :only_path => true
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to params
  end

  def wrong_redirect_only_path
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def redirect_only_path_with_unsafe_hash
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to(params.to_unsafe_hash.merge(:only_path => true, :display => nil))
  end

  def redirect_only_path_with_unsafe_h
    # ok: ruby_redirect_rule-CheckRedirectTo
    redirect_to(params.to_unsafe_h.merge(:only_path => true, :display => nil))
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'erb'

class FaxHelper

  def to_fax
    html = File.open(path_to_template).read
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    template.result
  end

end


x = 42
# ruleid: ruby_xss_rule-ManualTemplateCreation
template = ERB.new <<-EOF
  The value of x is: <%= x %>
EOF
puts template.result(binding)
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: ruby_crypto_rule-WeakHashesMD5
        md5 = Digest::MD5.digest 'abc'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
    o = Klass.new("hello\n")
    data = params['data']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    o = Klass.new("hello\n")
    data = YAML.dump(o)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = YAML.load(data)

    o = Klass.new("hello\n")
    data = cookies['some_field']
    # ruleid: ruby_deserialization_rule-BadDeserialization
    obj = Oj.object_load(data)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(data,options=some_safe_options)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserialization
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Account < ActiveRecord::Base
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  serialize :cc_info #safe from CVE-2013-0277
  attr_accessible :blah_admin_blah
end

class Account < ActiveRecord::Base
  #ruleid: ruby_regex_rule-CheckValidationRegex
  validates_format_of :name, :with => /^[a-zA-Z]+$/
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  #ok: ruby_regex_rule-CheckValidationRegex
  validates_format_of :good_valid, :with => /\A[a-zA-Z]\z/ #No warning
  #ok: ruby_regex_rule-CheckValidationRegex
  validates_format_of :not_bad, :with => /\A[a-zA-Z]\Z/ #No warning

  def mass_assign_it
    Account.new(params[:account_info]).some_other_method
  end

  def test_class_eval
    #Should not raise a warning
    User.class_eval do
      attr_reader :some_private_thing
    end
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :age, :role)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'open3'

def test_params()
  user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: ruby_injection_rule-DangerousExec
  output = exec(["sh", "-c", user_input])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  commands = "ls -lah /raz/dva"
# ok: ruby_injection_rule-DangerousExec
  system(commands)

  cmd_name = "sh"
# ok: ruby_injection_rule-DangerousExec
  Process.exec([cmd_name, "ls", "-la"])
# ok: ruby_injection_rule-DangerousExec
  Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
# ok: ruby_injection_rule-DangerousExec
  system("ls -lah /tmp")
# ok: ruby_injection_rule-DangerousExec
  exec(["ls", "-lah", "/tmp"])
end

def test_calls(user_input)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end

  def test_params()
    user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end
  
  def test_cookies()
    user_input = cookies['some_cookie']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
      commands = "ls -lah /raz/dva"
    # ok: ruby_injection_rule-DangerousExec
      system(commands)
    
      cmd_name = "sh"
    # ok: ruby_injection_rule-DangerousExec
      Process.exec([cmd_name, "ls", "-la"])
    # ok: ruby_injection_rule-DangerousExec
      Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
    # ok: ruby_injection_rule-DangerousExec
      system("ls -lah /tmp")
    # ok: ruby_injection_rule-DangerousExec
      exec(["ls", "-lah", "/tmp"])
    end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

    #ruleid: ruby_sql_rule-CheckSQL
    Product.select params[:columns]
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
require 'net/http'

def foo

  url = params[:url]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:url])
  # ruleid: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.post(uri)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:server])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get("example.com", "/index.html")

  uri = URI("example.com/index.html")
  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(uri)

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
class GroupsController < ApplicationController

  def dynamic_method_invocations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ruleid: ruby_reflection_rule-CheckUnsafeReflectionMethods
    Kernel.tap(&params[:method].to_sym)
    User.method("#{User.first.some_method_thing}_stuff")
    user_input_value = params[:my_user_input]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def dynamic_method_invocations_ok
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    "SomeClass".to_sym.to_proc.call(Kernel)
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    SomeClass.method("some_method").("some_argument")
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    Kernel.tap("SomeClass".to_sym)
    user_input_value = params[:my_user_input]
    # ok: ruby_reflection_rule-CheckUnsafeReflectionMethods
    user_input_value.tap("some_method")
  end

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def foo

    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad chdir combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chdir("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chdir("/tmp/usr/bin")

    #
    # Test bad chroot combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.chroot("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.chroot("/tmp/usr/bin")

    #
    # Test bad delete combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.delete("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.delete("/tmp/usr/bin")

    #
    # Test bad lchmod combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.lchmod("/tmp/usr/bin")

    # ruleid: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/#{params[:name]}")
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.lchmod("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.lchmod("/tmp/usr/bin")

    #
    # Test bad open combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.open("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.open("/tmp/usr/bin")

    #
    # Test bad readlines combinations

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Dir.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    IO.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Kernel.readlines("/tmp/usr/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    PStore.readlines("/tmp/#{anything}/bin")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    # ok: ruby_file_rule-AvoidTaintedFileAccess
    Pathname.readlines("/tmp/#{anything}/bin")


    #
    # Test ok tainted calls

    # ok: ruby_file_rule-AvoidTaintedFileAccess
    File.basename("/tmp/#{params[:name]}")

end
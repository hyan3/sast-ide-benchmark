@SuppressWarnings("LineLength")
public class WifiStateChangeService extends Worker {
    private static final String TAG = "WifiStateChangeService";

    public static final String BROADCAST = "org.fdroid.fdroid.action.WIFI_CHANGE";
    public static final String EXTRA_STATUS = "wifiStateChangeStatus";

    private WifiManager wifiManager;
    private static WifiInfoThread wifiInfoThread;
    private static int previousWifiState = Integer.MIN_VALUE;
    private volatile static int wifiState;
    private static final int NETWORK_INFO_STATE_NOT_SET = -1;

    public WifiStateChangeService(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    public static void registerReceiver(Context context) {
        ContextCompat.registerReceiver(
                context,
                new WifiStateChangeReceiver(),
                new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION),
                ContextCompat.RECEIVER_NOT_EXPORTED);
    }

    public static void start(Context context, @Nullable Intent intent) {
        int networkInfoStateInt = NETWORK_INFO_STATE_NOT_SET;
        if (intent != null) {
            NetworkInfo ni = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            networkInfoStateInt = ni.getState().ordinal();
        }

        WorkRequest workRequest = new OneTimeWorkRequest.Builder(WifiStateChangeService.class)
                .setConstraints(new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build())
                .setInputData(new Data.Builder()
                        .putInt(WifiManager.EXTRA_NETWORK_INFO, networkInfoStateInt)
                        .build()
                )
                .build();
        WorkManager.getInstance(context).enqueue(workRequest);
    }

    @NonNull
    @Override
    public Result doWork() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
        int networkInfoStateInt = getInputData().getInt(WifiManager.EXTRA_NETWORK_INFO, NETWORK_INFO_STATE_NOT_SET);
        NetworkInfo.State networkInfoState = null;
        if (networkInfoStateInt != NETWORK_INFO_STATE_NOT_SET) {
            networkInfoState = NetworkInfo.State.values()[networkInfoStateInt];
        }
        Utils.debugLog(TAG, "WiFi change service started.");
        wifiManager = ContextCompat.getSystemService(getApplicationContext(), WifiManager.class);
        if (wifiManager == null) {
            return Result.failure();
        }
        wifiState = wifiManager.getWifiState();
        Utils.debugLog(TAG, "networkInfoStateInt == " + networkInfoStateInt
                + "  wifiState == " + printWifiState(wifiState));
        if (networkInfoState == null
                || networkInfoState == NetworkInfo.State.CONNECTED
                || networkInfoState == NetworkInfo.State.DISCONNECTED) {
            if (previousWifiState != wifiState &&
                    (wifiState == WifiManager.WIFI_STATE_ENABLED
                            || wifiState == WifiManager.WIFI_STATE_DISABLING  // might be switching to hotspot
                            || wifiState == WifiManager.WIFI_STATE_DISABLED   // might be hotspot
                            || wifiState == WifiManager.WIFI_STATE_UNKNOWN)) { // might be hotspot
                if (wifiInfoThread != null) {
                    wifiInfoThread.interrupt();
                }
                wifiInfoThread = new WifiInfoThread();
                wifiInfoThread.start();
            }
        }
        return Result.success();
    }

    public class WifiInfoThread extends Thread {

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
            try {
                FDroidApp.initWifiSettings();
                Utils.debugLog(TAG, "Checking wifi state (in background thread).");
                WifiInfo wifiInfo = null;

                int wifiState = wifiManager.getWifiState();
                int retryCount = 0;
                while (FDroidApp.ipAddressString == null) {
                    if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                        return;
                    }
                    if (wifiState == WifiManager.WIFI_STATE_ENABLED) {
                        wifiInfo = wifiManager.getConnectionInfo();
                        FDroidApp.ipAddressString = formatIpAddress(wifiInfo.getIpAddress());
                        setSsid(wifiInfo);
                        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
                        if (dhcpInfo != null) {
                            String netmask = formatIpAddress(dhcpInfo.netmask);
                            if (!TextUtils.isEmpty(FDroidApp.ipAddressString) && netmask != null) {
                                try {
                                    FDroidApp.subnetInfo = new SubnetUtils(FDroidApp.ipAddressString, netmask).getInfo();
                                } catch (IllegalArgumentException e) {
                                    // catch mystery: "java.lang.IllegalArgumentException: Could not parse [null/24]"
                                    e.printStackTrace();
                                }
                            }
                        }
                        if (FDroidApp.ipAddressString == null
                                || FDroidApp.subnetInfo == FDroidApp.UNSET_SUBNET_INFO) {
                            setIpInfoFromNetworkInterface();
                        }
                    } else if (wifiState == WifiManager.WIFI_STATE_DISABLED
                            || wifiState == WifiManager.WIFI_STATE_DISABLING
                            || wifiState == WifiManager.WIFI_STATE_UNKNOWN) {
                        // try once to see if its a hotspot
                        setIpInfoFromNetworkInterface();
                        if (FDroidApp.ipAddressString == null) {
                            return;
                        }
                    }

                    if (retryCount > 120) {
                        return;
                    }
                    retryCount++;

                    if (FDroidApp.ipAddressString == null) {
                        Thread.sleep(1000);
                        Utils.debugLog(TAG, "waiting for an IP address...");
                    }
                }
                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                setSsid(wifiInfo);

                String scheme;
                if (Preferences.get().isLocalRepoHttpsEnabled()) {
                    scheme = "https";
                } else {
                    scheme = "http";
                }
                Context context = WifiStateChangeService.this.getApplicationContext();
                String address = String.format(Locale.ENGLISH, "%s://%s:%d/fdroid/repo",
                        scheme, FDroidApp.ipAddressString, FDroidApp.port);
                // the fingerprint for the local repo's signing key
                LocalRepoKeyStore localRepoKeyStore = LocalRepoKeyStore.get(context);
                Certificate localCert = localRepoKeyStore.getCertificate();
                String cert = localCert == null ?
                        null : Hasher.hex(localCert).toLowerCase(Locale.US);
                Repository repo = FDroidApp.createSwapRepo(address, cert);

                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                LocalRepoManager lrm = LocalRepoManager.get(context);
                lrm.writeIndexPage(Utils.getSharingUri(FDroidApp.repo).toString());

                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                FDroidApp.repo = repo;

                /*
                 * Once the IP address is known we need to generate a self
                 * signed certificate to use for HTTPS that has a CN field set
                 * to the ipAddressString. This must be run in the background
                 * because if this is the first time the singleton is run, it
                 * can take a while to instantiate.
                 */
                if (Preferences.get().isLocalRepoHttpsEnabled()) {
                    localRepoKeyStore.setupHTTPSCertificate();
                }

            } catch (LocalRepoKeyStore.InitException e) {
                Log.e(TAG, "Unable to configure a fingerprint or HTTPS for the local repo", e);
            } catch (InterruptedException e) {
                Utils.debugLog(TAG, "interrupted");
                return;
            }
            Intent intent = new Intent(BROADCAST);
            intent.putExtra(EXTRA_STATUS, wifiState);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }

    private void setSsid(WifiInfo wifiInfo) {
        Context context = getApplicationContext();
        if (wifiInfo != null && wifiInfo.getBSSID() != null) {
            String ssid = wifiInfo.getSSID();
            Utils.debugLog(TAG, "Have wifi info, connected to " + ssid);
            if (ssid == null) {
                FDroidApp.ssid = context.getString(R.string.swap_blank_wifi_ssid);
            } else {
                FDroidApp.ssid = ssid.replaceAll("^\"(.*)\"$", "$1");
            }
            FDroidApp.bssid = wifiInfo.getBSSID();
        } else {
            WifiApControl wifiApControl = WifiApControl.getInstance(context);
            Utils.debugLog(TAG, "WifiApControl: " + wifiApControl);
            if (wifiApControl == null && FDroidApp.ipAddressString != null) {
                wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null && wifiInfo.getBSSID() != null) {
                    setSsid(wifiInfo);
                } else {
                    FDroidApp.ssid = context.getString(R.string.swap_active_hotspot, "");
                }
            } else if (wifiApControl != null && wifiApControl.isEnabled()) {
                WifiConfiguration wifiConfiguration = wifiApControl.getConfiguration();
                Utils.debugLog(TAG, "WifiConfiguration: " + wifiConfiguration);
                if (wifiConfiguration == null) {
                    FDroidApp.ssid = context.getString(R.string.swap_active_hotspot, "");
                    FDroidApp.bssid = "";
                    return;
                }

                if (wifiConfiguration.hiddenSSID) {
                    FDroidApp.ssid = context.getString(R.string.swap_hidden_wifi_ssid);
                } else {
                    FDroidApp.ssid = wifiConfiguration.SSID;
                }
                FDroidApp.bssid = wifiConfiguration.BSSID;
            }
        }
    }

    /**
     * Search for known Wi-Fi, Hotspot, and local network interfaces and get
     * the IP Address info from it.  This is necessary because network
     * interfaces in Hotspot/AP mode do not show up in the regular
     * {@link WifiManager} queries, and also on
     * {@link android.os.Build.VERSION_CODES#LOLLIPOP Android 5.0} and newer,
     * {@link WifiManager#getDhcpInfo()} returns an invalid netmask.
     *
     * @see <a href="https://issuetracker.google.com/issues/37015180">netmask of WifiManager.getDhcpInfo() is always zero on Android 5.0</a>
     */
    private void setIpInfoFromNetworkInterface() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            if (networkInterfaces == null) {
                return;
            }
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface netIf = networkInterfaces.nextElement();

                for (Enumeration<InetAddress> inetAddresses = netIf.getInetAddresses(); inetAddresses.hasMoreElements(); ) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress.isLoopbackAddress() || inetAddress instanceof Inet6Address) {
                        continue;
                    }
                    if (netIf.getDisplayName().contains("wlan0")
                            || netIf.getDisplayName().contains("eth0")
                            || netIf.getDisplayName().contains("ap0")) {
                        FDroidApp.ipAddressString = inetAddress.getHostAddress();
                        for (InterfaceAddress address : netIf.getInterfaceAddresses()) {
                            short networkPrefixLength = address.getNetworkPrefixLength();
                            if (networkPrefixLength > 32) {
                                // something is giving a "/64" netmask, IPv6?
                                // java.lang.IllegalArgumentException: Value [64] not in range [0,32]
                                continue;
                            }
                            try {
                                String cidr = String.format(Locale.ENGLISH, "%s/%d",
                                        FDroidApp.ipAddressString, networkPrefixLength);
                                FDroidApp.subnetInfo = new SubnetUtils(cidr).getInfo();
                                break;
                            } catch (IllegalArgumentException e) {
                                if (BuildConfig.DEBUG) {
                                    e.printStackTrace();
                                } else {
                                    Log.i(TAG, "Getting subnet failed: " + e.getLocalizedMessage());
                                }
                            }
                        }
                    }
                }
            }
        } catch (NullPointerException | SocketException e) {
            // NetworkInterface.getNetworkInterfaces() can throw a NullPointerException internally
            Log.e(TAG, "Could not get ip address", e);
        }
    }

    static String formatIpAddress(int ipAddress) {
        if (ipAddress == 0) {
            return null;
        }
        return String.format(Locale.ENGLISH, "%d.%d.%d.%d",
                ipAddress & 0xff,
                ipAddress >> 8 & 0xff,
                ipAddress >> 16 & 0xff,
                ipAddress >> 24 & 0xff);
    }

    private String printWifiState(int wifiState) {
        switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLED:
                return "WIFI_STATE_DISABLED";
            case WifiManager.WIFI_STATE_DISABLING:
                return "WIFI_STATE_DISABLING";
            case WifiManager.WIFI_STATE_ENABLING:
                return "WIFI_STATE_ENABLING";
            case WifiManager.WIFI_STATE_ENABLED:
                return "WIFI_STATE_ENABLED";
            case WifiManager.WIFI_STATE_UNKNOWN:
                return "WIFI_STATE_UNKNOWN";
            case Integer.MIN_VALUE:
                return "previous value unset";
            default:
                return "~not mapped~";
        }
    }
}

public class AppStatusListItemController extends AppListItemController {
    AppStatusListItemController(AppCompatActivity activity, View itemView) {
        super(activity, itemView);
    }

    @NonNull
    @Override
    protected AppListItemState getCurrentViewState(@NonNull App app, @Nullable AppUpdateStatus appStatus) {

        return super.getCurrentViewState(app, appStatus)
                .setStatusText(getStatusText(appStatus));
    }

    @Nullable
    private CharSequence getStatusText(@Nullable AppUpdateStatus appStatus) {
        if (appStatus != null) {
            switch (appStatus.status) {
                case ReadyToInstall:
                    return activity.getString(R.string.app_list_download_ready);

                case Installed:
                    return activity.getString(R.string.notification_content_single_installed);
            }
        }

        return null;
    }

    @Override
    public boolean canDismiss() {
        return true;
    }

    @Override
    protected void onDismissApp(@NonNull final App app, final UpdatesAdapter adapter) {
        AppUpdateStatus status = getCurrentStatus();
        if (status != null) {
            final AppUpdateStatusManager manager = AppUpdateStatusManager.getInstance(activity);
            final AppUpdateStatus appUpdateStatus = manager.get(status.getCanonicalUrl());
            manager.removeApk(status.getCanonicalUrl());

            switch (status.status) {
                case Downloading:
                    cancelDownload();
                    Snackbar.make(itemView, R.string.app_list__dismiss_downloading_app, Snackbar.LENGTH_SHORT).show();
                    break;

                case ReadyToInstall:
                    if (appUpdateStatus != null) {
                        Snackbar.make(
                                itemView,
                                R.string.app_list__dismiss_installing_app,
                                Snackbar.LENGTH_LONG
                        ).setAction(R.string.undo, view -> {
                            manager.addApk(appUpdateStatus.app, appUpdateStatus.apk, appUpdateStatus.status,
                                    appUpdateStatus.intent);
                            adapter.refreshItems();
                        }).show();
                        break;
                    }
            }
        }

        adapter.refreshItems();
    }
}

public final class QRCodeEncoder {
    private static final int WHITE = 0xFFFFFFFF;
    private static final int BLACK = 0xFF000000;

    private final int dimension;
    private String contents;
    private String displayContents;
    private String title;
    private BarcodeFormat format;
    private final boolean encoded;

    public QRCodeEncoder(String data, Bundle bundle, String type, String format, int dimension) {
        this.dimension = dimension;
        encoded = encodeContents(data, bundle, type, format);
    }

    public String getContents() {
        return contents;
    }

    public String getDisplayContents() {
        return displayContents;
    }

    public String getTitle() {
        return title;
    }

    private boolean encodeContents(String data, Bundle bundle, String type, String formatString) {
        // Default to QR_CODE if no format given.
        format = null;
        if (formatString != null) {
            try {
                format = BarcodeFormat.valueOf(formatString);
            } catch (IllegalArgumentException iae) {
                // Ignore it then
            }
        }
        if (format == null || format == BarcodeFormat.QR_CODE) {
            this.format = BarcodeFormat.QR_CODE;
            encodeQRCodeContents(data, bundle, type);
        } else if (data != null && data.length() > 0) {
            contents = data;
            displayContents = data;
            title = "Text";
        }
        return contents != null && contents.length() > 0;
    }

    private void encodeQRCodeContents(String data, Bundle bundle, String type) {
        switch (type) {
            case Contents.Type.TEXT:
                if (data != null && data.length() > 0) {
                    contents = data;
                    displayContents = data;
                    title = "Text";
                }
                break;
            case Contents.Type.EMAIL:
                data = trim(data);
                if (data != null) {
                    contents = "mailto:" + data;
                    displayContents = data;
                    title = "E-Mail";
                }
                break;
            case Contents.Type.PHONE:
                data = trim(data);
                if (data != null) {
                    contents = "tel:" + data;
                    displayContents = PhoneNumberUtils.formatNumber(data);
                    title = "Phone";
                }
                break;
            case Contents.Type.SMS:
                data = trim(data);
                if (data != null) {
                    contents = "sms:" + data;
                    displayContents = PhoneNumberUtils.formatNumber(data);
                    title = "SMS";
                }
                break;
            case Contents.Type.CONTACT:
                if (bundle != null) {
                    StringBuilder newContents = new StringBuilder(100);
                    StringBuilder newDisplayContents = new StringBuilder(100);

                    newContents.append("MECARD:");

                    String name = trim(bundle.getString(ContactsContract.Intents.Insert.NAME));
                    if (name != null) {
                        newContents.append("N:").append(escapeMECARD(name)).append(';');
                        newDisplayContents.append(name);
                    }

                    String address = trim(bundle.getString(ContactsContract.Intents.Insert.POSTAL));
                    if (address != null) {
                        newContents.append("ADR:").append(escapeMECARD(address)).append(';');
                        newDisplayContents.append('\n').append(address);
                    }

                    Collection<String> uniquePhones = new HashSet<>(Contents.PHONE_KEYS.length);
                    for (int x = 0; x < Contents.PHONE_KEYS.length; x++) {
                        String phone = trim(bundle.getString(Contents.PHONE_KEYS[x]));
                        if (phone != null) {
                            uniquePhones.add(phone);
                        }
                    }
                    for (String phone : uniquePhones) {
                        newContents.append("TEL:").append(escapeMECARD(phone)).append(';');
                        newDisplayContents.append('\n').append(PhoneNumberUtils.formatNumber(phone));
                    }

                    Collection<String> uniqueEmails = new HashSet<>(Contents.EMAIL_KEYS.length);
                    for (int x = 0; x < Contents.EMAIL_KEYS.length; x++) {
                        String email = trim(bundle.getString(Contents.EMAIL_KEYS[x]));
                        if (email != null) {
                            uniqueEmails.add(email);
                        }
                    }
                    for (String email : uniqueEmails) {
                        newContents.append("EMAIL:").append(escapeMECARD(email)).append(';');
                        newDisplayContents.append('\n').append(email);
                    }

                    String url = trim(bundle.getString(Contents.URL_KEY));
                    if (url != null) {
                        // escapeMECARD(url) -> wrong escape e.g. http\://zxing.google.com
                        newContents.append("URL:").append(url).append(';');
                        newDisplayContents.append('\n').append(url);
                    }

                    String note = trim(bundle.getString(Contents.NOTE_KEY));
                    if (note != null) {
                        newContents.append("NOTE:").append(escapeMECARD(note)).append(';');
                        newDisplayContents.append('\n').append(note);
                    }

                    // Make sure we've encoded at least one field.
                    if (newDisplayContents.length() > 0) {
                        newContents.append(';');
                        contents = newContents.toString();
                        displayContents = newDisplayContents.toString();
                        title = "Contact";
                    } else {
                        contents = null;
                        displayContents = null;
                    }
                }
                break;
            case Contents.Type.LOCATION:
                if (bundle != null) {
                    // These must use Bundle.getFloat(), not getDouble(), it's part of the API.
                    float latitude = bundle.getFloat("LAT", Float.MAX_VALUE);
                    float longitude = bundle.getFloat("LONG", Float.MAX_VALUE);
                    if (latitude != Float.MAX_VALUE && longitude != Float.MAX_VALUE) {
                        contents = "geo:" + latitude + ',' + longitude;
                        displayContents = latitude + "," + longitude;
                        title = "Location";
                    }
                }
                break;
        }
    }

    public Bitmap encodeAsBitmap() throws WriterException {
        if (!encoded) return null;

        Map<EncodeHintType, Object> hints = null;
        String encoding = guessAppropriateEncoding(contents);
        if (encoding != null) {
            hints = new EnumMap<>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, encoding);
        }
        MultiFormatWriter writer = new MultiFormatWriter();
        BitMatrix result = writer.encode(contents, format, dimension, dimension, hints);
        int width = result.getWidth();
        int height = result.getHeight();
        int[] pixels = new int[width * height];
        // All are 0, or black, by default
        for (int y = 0; y < height; y++) {
            int offset = y * width;
            for (int x = 0; x < width; x++) {
                pixels[offset + x] = result.get(x, y) ? BLACK : WHITE;
            }
        }

        Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
        return bitmap;
    }

    private static String guessAppropriateEncoding(CharSequence contents) {
        // Very crude at the moment
        for (int i = 0; i < contents.length(); i++) {
            if (contents.charAt(i) > 0xFF) {
                return "UTF-8";
            }
        }
        return null;
    }

    private static String trim(String s) {
        if (s == null) {
            return null;
        }
        String result = s.trim();
        return result.length() == 0 ? null : result;
    }

    private static String escapeMECARD(String input) {
        if (input == null || (input.indexOf(':') < 0 && input.indexOf(';') < 0)) {
            return input;
        }
        int length = input.length();
        StringBuilder result = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            char c = input.charAt(i);
            if (c == ':' || c == ';') {
                result.append('\\');
            }
            result.append(c);
        }
        return result.toString();
    }
}

@SuppressWarnings("LineLength")
public class WifiStateChangeService extends Worker {
    private static final String TAG = "WifiStateChangeService";

    public static final String BROADCAST = "org.fdroid.fdroid.action.WIFI_CHANGE";
    public static final String EXTRA_STATUS = "wifiStateChangeStatus";

    private WifiManager wifiManager;
    private static WifiInfoThread wifiInfoThread;
    private static int previousWifiState = Integer.MIN_VALUE;
    private volatile static int wifiState;
    private static final int NETWORK_INFO_STATE_NOT_SET = -1;

    public WifiStateChangeService(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    public static void registerReceiver(Context context) {
        ContextCompat.registerReceiver(
                context,
                new WifiStateChangeReceiver(),
                new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION),
                ContextCompat.RECEIVER_NOT_EXPORTED);
    }

    public static void start(Context context, @Nullable Intent intent) {
        int networkInfoStateInt = NETWORK_INFO_STATE_NOT_SET;
        if (intent != null) {
            NetworkInfo ni = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            networkInfoStateInt = ni.getState().ordinal();
        }

        WorkRequest workRequest = new OneTimeWorkRequest.Builder(WifiStateChangeService.class)
                .setConstraints(new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build())
                .setInputData(new Data.Builder()
                        .putInt(WifiManager.EXTRA_NETWORK_INFO, networkInfoStateInt)
                        .build()
                )
                .build();
        WorkManager.getInstance(context).enqueue(workRequest);
    }

    @NonNull
    @Override
    public Result doWork() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
        int networkInfoStateInt = getInputData().getInt(WifiManager.EXTRA_NETWORK_INFO, NETWORK_INFO_STATE_NOT_SET);
        NetworkInfo.State networkInfoState = null;
        if (networkInfoStateInt != NETWORK_INFO_STATE_NOT_SET) {
            networkInfoState = NetworkInfo.State.values()[networkInfoStateInt];
        }
        Utils.debugLog(TAG, "WiFi change service started.");
        wifiManager = ContextCompat.getSystemService(getApplicationContext(), WifiManager.class);
        if (wifiManager == null) {
            return Result.failure();
        }
        wifiState = wifiManager.getWifiState();
        Utils.debugLog(TAG, "networkInfoStateInt == " + networkInfoStateInt
                + "  wifiState == " + printWifiState(wifiState));
        if (networkInfoState == null
                || networkInfoState == NetworkInfo.State.CONNECTED
                || networkInfoState == NetworkInfo.State.DISCONNECTED) {
            if (previousWifiState != wifiState &&
                    (wifiState == WifiManager.WIFI_STATE_ENABLED
                            || wifiState == WifiManager.WIFI_STATE_DISABLING  // might be switching to hotspot
                            || wifiState == WifiManager.WIFI_STATE_DISABLED   // might be hotspot
                            || wifiState == WifiManager.WIFI_STATE_UNKNOWN)) { // might be hotspot
                if (wifiInfoThread != null) {
                    wifiInfoThread.interrupt();
                }
                wifiInfoThread = new WifiInfoThread();
                wifiInfoThread.start();
            }
        }
        return Result.success();
    }

    public class WifiInfoThread extends Thread {

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
            try {
                FDroidApp.initWifiSettings();
                Utils.debugLog(TAG, "Checking wifi state (in background thread).");
                WifiInfo wifiInfo = null;

                int wifiState = wifiManager.getWifiState();
                int retryCount = 0;
                while (FDroidApp.ipAddressString == null) {
                    if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                        return;
                    }
                    if (wifiState == WifiManager.WIFI_STATE_ENABLED) {
                        wifiInfo = wifiManager.getConnectionInfo();
                        FDroidApp.ipAddressString = formatIpAddress(wifiInfo.getIpAddress());
                        setSsid(wifiInfo);
                        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
                        if (dhcpInfo != null) {
                            String netmask = formatIpAddress(dhcpInfo.netmask);
                            if (!TextUtils.isEmpty(FDroidApp.ipAddressString) && netmask != null) {
                                try {
                                    FDroidApp.subnetInfo = new SubnetUtils(FDroidApp.ipAddressString, netmask).getInfo();
                                } catch (IllegalArgumentException e) {
                                    // catch mystery: "java.lang.IllegalArgumentException: Could not parse [null/24]"
                                    e.printStackTrace();
                                }
                            }
                        }
                        if (FDroidApp.ipAddressString == null
                                || FDroidApp.subnetInfo == FDroidApp.UNSET_SUBNET_INFO) {
                            setIpInfoFromNetworkInterface();
                        }
                    } else if (wifiState == WifiManager.WIFI_STATE_DISABLED
                            || wifiState == WifiManager.WIFI_STATE_DISABLING
                            || wifiState == WifiManager.WIFI_STATE_UNKNOWN) {
                        // try once to see if its a hotspot
                        setIpInfoFromNetworkInterface();
                        if (FDroidApp.ipAddressString == null) {
                            return;
                        }
                    }

                    if (retryCount > 120) {
                        return;
                    }
                    retryCount++;

                    if (FDroidApp.ipAddressString == null) {
                        Thread.sleep(1000);
                        Utils.debugLog(TAG, "waiting for an IP address...");
                    }
                }
                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                setSsid(wifiInfo);

                String scheme;
                if (Preferences.get().isLocalRepoHttpsEnabled()) {
                    scheme = "https";
                } else {
                    scheme = "http";
                }
                Context context = WifiStateChangeService.this.getApplicationContext();
                String address = String.format(Locale.ENGLISH, "%s://%s:%d/fdroid/repo",
                        scheme, FDroidApp.ipAddressString, FDroidApp.port);
                // the fingerprint for the local repo's signing key
                LocalRepoKeyStore localRepoKeyStore = LocalRepoKeyStore.get(context);
                Certificate localCert = localRepoKeyStore.getCertificate();
                String cert = localCert == null ?
                        null : Hasher.hex(localCert).toLowerCase(Locale.US);
                Repository repo = FDroidApp.createSwapRepo(address, cert);

                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                LocalRepoManager lrm = LocalRepoManager.get(context);
                lrm.writeIndexPage(Utils.getSharingUri(FDroidApp.repo).toString());

                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                FDroidApp.repo = repo;

                /*
                 * Once the IP address is known we need to generate a self
                 * signed certificate to use for HTTPS that has a CN field set
                 * to the ipAddressString. This must be run in the background
                 * because if this is the first time the singleton is run, it
                 * can take a while to instantiate.
                 */
                if (Preferences.get().isLocalRepoHttpsEnabled()) {
                    localRepoKeyStore.setupHTTPSCertificate();
                }

            } catch (LocalRepoKeyStore.InitException e) {
                Log.e(TAG, "Unable to configure a fingerprint or HTTPS for the local repo", e);
            } catch (InterruptedException e) {
                Utils.debugLog(TAG, "interrupted");
                return;
            }
            Intent intent = new Intent(BROADCAST);
            intent.putExtra(EXTRA_STATUS, wifiState);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }

    private void setSsid(WifiInfo wifiInfo) {
        Context context = getApplicationContext();
        if (wifiInfo != null && wifiInfo.getBSSID() != null) {
            String ssid = wifiInfo.getSSID();
            Utils.debugLog(TAG, "Have wifi info, connected to " + ssid);
            if (ssid == null) {
                FDroidApp.ssid = context.getString(R.string.swap_blank_wifi_ssid);
            } else {
                FDroidApp.ssid = ssid.replaceAll("^\"(.*)\"$", "$1");
            }
            FDroidApp.bssid = wifiInfo.getBSSID();
        } else {
            WifiApControl wifiApControl = WifiApControl.getInstance(context);
            Utils.debugLog(TAG, "WifiApControl: " + wifiApControl);
            if (wifiApControl == null && FDroidApp.ipAddressString != null) {
                wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null && wifiInfo.getBSSID() != null) {
                    setSsid(wifiInfo);
                } else {
                    FDroidApp.ssid = context.getString(R.string.swap_active_hotspot, "");
                }
            } else if (wifiApControl != null && wifiApControl.isEnabled()) {
                WifiConfiguration wifiConfiguration = wifiApControl.getConfiguration();
                Utils.debugLog(TAG, "WifiConfiguration: " + wifiConfiguration);
                if (wifiConfiguration == null) {
                    FDroidApp.ssid = context.getString(R.string.swap_active_hotspot, "");
                    FDroidApp.bssid = "";
                    return;
                }

                if (wifiConfiguration.hiddenSSID) {
                    FDroidApp.ssid = context.getString(R.string.swap_hidden_wifi_ssid);
                } else {
                    FDroidApp.ssid = wifiConfiguration.SSID;
                }
                FDroidApp.bssid = wifiConfiguration.BSSID;
            }
        }
    }

    /**
     * Search for known Wi-Fi, Hotspot, and local network interfaces and get
     * the IP Address info from it.  This is necessary because network
     * interfaces in Hotspot/AP mode do not show up in the regular
     * {@link WifiManager} queries, and also on
     * {@link android.os.Build.VERSION_CODES#LOLLIPOP Android 5.0} and newer,
     * {@link WifiManager#getDhcpInfo()} returns an invalid netmask.
     *
     * @see <a href="https://issuetracker.google.com/issues/37015180">netmask of WifiManager.getDhcpInfo() is always zero on Android 5.0</a>
     */
    private void setIpInfoFromNetworkInterface() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            if (networkInterfaces == null) {
                return;
            }
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface netIf = networkInterfaces.nextElement();

                for (Enumeration<InetAddress> inetAddresses = netIf.getInetAddresses(); inetAddresses.hasMoreElements(); ) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress.isLoopbackAddress() || inetAddress instanceof Inet6Address) {
                        continue;
                    }
                    if (netIf.getDisplayName().contains("wlan0")
                            || netIf.getDisplayName().contains("eth0")
                            || netIf.getDisplayName().contains("ap0")) {
                        FDroidApp.ipAddressString = inetAddress.getHostAddress();
                        for (InterfaceAddress address : netIf.getInterfaceAddresses()) {
                            short networkPrefixLength = address.getNetworkPrefixLength();
                            if (networkPrefixLength > 32) {
                                // something is giving a "/64" netmask, IPv6?
                                // java.lang.IllegalArgumentException: Value [64] not in range [0,32]
                                continue;
                            }
                            try {
                                String cidr = String.format(Locale.ENGLISH, "%s/%d",
                                        FDroidApp.ipAddressString, networkPrefixLength);
                                FDroidApp.subnetInfo = new SubnetUtils(cidr).getInfo();
                                break;
                            } catch (IllegalArgumentException e) {
                                if (BuildConfig.DEBUG) {
                                    e.printStackTrace();
                                } else {
                                    Log.i(TAG, "Getting subnet failed: " + e.getLocalizedMessage());
                                }
                            }
                        }
                    }
                }
            }
        } catch (NullPointerException | SocketException e) {
            // NetworkInterface.getNetworkInterfaces() can throw a NullPointerException internally
            Log.e(TAG, "Could not get ip address", e);
        }
    }

    static String formatIpAddress(int ipAddress) {
        if (ipAddress == 0) {
            return null;
        }
        return String.format(Locale.ENGLISH, "%d.%d.%d.%d",
                ipAddress & 0xff,
                ipAddress >> 8 & 0xff,
                ipAddress >> 16 & 0xff,
                ipAddress >> 24 & 0xff);
    }

    private String printWifiState(int wifiState) {
        switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLED:
                return "WIFI_STATE_DISABLED";
            case WifiManager.WIFI_STATE_DISABLING:
                return "WIFI_STATE_DISABLING";
            case WifiManager.WIFI_STATE_ENABLING:
                return "WIFI_STATE_ENABLING";
            case WifiManager.WIFI_STATE_ENABLED:
                return "WIFI_STATE_ENABLED";
            case WifiManager.WIFI_STATE_UNKNOWN:
                return "WIFI_STATE_UNKNOWN";
            case Integer.MIN_VALUE:
                return "previous value unset";
            default:
                return "~not mapped~";
        }
    }
}


// License: MIT (c) GitLab Inc.
package cookie;

import org.apache.commons.text.StringEscapeUtils;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class RequestParamToHeader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        //ruleid: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramNames = req.getParameterNames().nextElement();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramValues = req.getParameterValues("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramMap = req.getParameterMap().get("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = req.getParameter("input");
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", input);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", input);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", header);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", header);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", contextPath);

    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", searchControls, mapper, dirContextProcessor);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// cf. https://find-sec-bugs.github.io/bugs.htm#SPRING_CSRF_UNRESTRICTED_REQUEST_MAPPING

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

class Controller {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    // ruleid: java_csrf_rule-UnrestrictedRequestMapping
    @RequestMapping(value = "/path")
    public void writeData2() {
        // State-changing operations performed within this method.
    }

    /**
     * For methods without side-effects use either
     * RequestMethod.GET, RequestMethod.HEAD, RequestMethod.TRACE, or
     * RequestMethod.OPTIONS.
     */
    // ok: java_csrf_rule-UnrestrictedRequestMapping
    @RequestMapping(value = "/path", method = RequestMethod.GET)
    public String readData() {
        // No state-changing operations performed within this method.
        return "";
    }

    /**
     * For state-changing methods use either
     * RequestMethod.POST, RequestMethod.PUT, RequestMethod.DELETE, or
     * RequestMethod.PATCH.
     */
    // ok: java_csrf_rule-UnrestrictedRequestMapping
    @RequestMapping(value = "/path", method = RequestMethod.POST)
    public void writeData3() {
        // State-changing operations performed within this method.
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/disallow-old-tls-versions2.java

class InsecureTLS {
    public void InsecureTLS1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void InsecureTLS2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void InsecureSSL() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void InsecureTLS3() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void InsecureTLS4() {
        // ruleid: java_crypto_rule-DisallowOldTLSVersion
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1");
    }

}

class SecureTLS {
    public void SecureTLS1() {
        // ok: java_crypto_rule-DisallowOldTLSVersion
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.2,TLSv1.3");
    }

    public void SecureTLS2() {
        // ok: java_crypto_rule-DisallowOldTLSVersion
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.2");
    }

    public void SecureTLS3() {
        // ok: java_crypto_rule-DisallowOldTLSVersion
        java.lang.System.setProperty("jdk.tls.client.protocols", "TLSv1.3");
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class X509TrustManagerTest {

    class TrustAllManager implements X509TrustManager {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_endpoint_rule-X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            return null;
        }
    }

    class LoggedTrustAllManager implements X509TrustManager {

        Logger logger = LogManager.getLogger(LoggedTrustAllManager.class);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            if(issuerRequestsCount < 5){
                issuerRequestsCount++;
                return null;
            }
            return certificates;
        }
    }

    class FaultyLimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            X509Certificate[] a = certificates;
            return a;
        }
    }

    class LimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;
        
        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return certificates;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return certificates;
        }
    }

    class FaultyLimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class FaultyLimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


    class FaultyLimitedManager4 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: MIT (c) GitLab Inc.
package cookie;

import org.apache.commons.text.StringEscapeUtils;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class RequestParamToHeader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramNames = req.getParameterNames().nextElement();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramValues = req.getParameterValues("input")[0];
        //ruleid: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", paramValues);

        String paramMap = req.getParameterMap().get("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = req.getParameter("input");
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", input);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", input);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", header);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", header);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", contextPath);

    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", new LdapEntryIdentificationContextMapper());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.io.FilenameUtils.*;

@WebServlet(name = "FilenameUtils", value = "/FilenameUtils")
public class FilenameUtils extends HttpServlet {

    final static String basePath = "/usr/local/lib";

    //Method where input is not sanitized with normalize:

    //Normal Input (opens article as intended):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (returns restricted file):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Input from From Servlet Param
        String input = req.getHeader("input");

        String method = req.getHeader("method");

        String fullPath = null;

        if(method != null && !method.isEmpty()) {
            switch (method) {
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPath'
                case "getFullPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPathNoEndSeparator'
                case "getFullPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPath'
                case "getPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPathNoEndSeparator'
                case "getPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalize'
                case "normalize": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalizeNoEndSeparator'
                case "normalizeNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: removeExtension'
                case "removeExtension": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToSystem'
                case "separatorsToSystem": {
                    //ruleid: java_file_rule-FilenameUtils
                    fullPath = separatorsToSystem(input);
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToUnix'
                case "separatorsToUnix": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                case "concat": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                default: {
                    input = "article.txt";
                    // ok: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }

            }
        }

        // Remove whitespaces
        fullPath = fullPath.replace(" ","");

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method not using user input
    //curl --location --request POST 'http://localhost:8080/ServletSample/FilenameUtils'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String filepath = "article.txt";

        //ok: java_file_rule-FilenameUtils
        String fullPath = concat(basePath, filepath);

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method where input is sanitized with getName:

    //Normal Input (opens article as intended):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (causes exception):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String input = req.getHeader("input");

        // Securely combine the user input with the base directory
        input = getName(input); // Extracts just the filename, no paths
        // Construct the safe, absolute path
        //ok: java_file_rule-FilenameUtils
        String safePath = concat(basePath, input);

        System.out.println(safePath);

        // Read the contents of the file
        File file = new File(safePath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
 
    protected void safe(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        
        String input = req.getHeader("input");

        String base = "/var/app/restricted";
        Path basePath = Paths.get(base);
        // ok: java_file_rule-FilenameUtils
        String inputPath = getPath(input);
        // Resolve the full path, but only use our random generated filename
        Path fullPath = basePath.resolve(inputPath);
        // verify the path is contained within our basePath
        if (!fullPath.startsWith(base)) {
            throw new Exception("Invalid path specified!");
        }
        
        // Read the contents of the file
        File file = new File(fullPath.toString());
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
        // ruleid: java_inject_rule-SqlInjection
        stmt.execute("select * from users where email = " + input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs

import org.apache.http.impl.client.CloseableHttpClient;

class Bad {
    public void bad1() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("http://targethost/homepage");
        // ruleid: java_crypto_rule-HttpComponentsRequest
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
    }

    public void bad2() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class Ok {
    public void ok1() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet("https://targethost/homepage");
        // ok: java_crypto_rule-HttpComponentsRequest
        CloseableHttpResponse response1 = httpclient.execute(httpGet);
    }

    public void ok2() {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // ok: java_crypto_rule-HttpComponentsRequest
        CloseableHttpResponse response1 = httpclient.execute(new HttpPost("https://example.com"));
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        Files.createTempFile(input,"");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/r?q=java.jjwt.security.jwt-none-alg.jjwt-none-alg
package crypto;

class JWTNoneAlgorithm {

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        System.out.println("Lib1 bad1() JWT: " + token);
    }

    public void ok1() {
        javax.crypto.SecretKey key = io.jsonwebtoken.Jwts.SIG.HS256.key().build();
        // ok: java_crypto_rule_JwtNoneAlgorithm
        String token = io.jsonwebtoken.Jwts.builder()
                .subject("Bob")
                .signWith(key)
                .compact();

        System.out.println("Lib1 ok() JWT: " + token);
    }

    public void bad2() throws Exception {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            System.out.println("Lib2 bad2() JWT: " + token);
        } catch (Exception e) {
            System.out.println("Exception : " + e.getLocalizedMessage());
        }
    }

    public void bad3() {
        try {
            // PlainHeaders are set with None Algo.
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\", \"exp\":1300819111}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad3() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bad4() {
        try {
            // PlainHeaders are set with None Algo.
            // ruleid: java_crypto_rule_JwtNoneAlgorithm
            com.nimbusds.jose.PlainHeader header = new com.nimbusds.jose.PlainHeader.Builder().contentType("text/plain")
                    .customParam("exp", "1300819111")
                    .build();

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\"}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad4() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.client.methods.HttpGet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

import static com.google.common.net.UrlEscapers.urlPathSegmentEscaper;

public class HttpParameterPollution extends HttpServlet {
    @SuppressWarnings("deprecation") //URLEncoder.encode is deprecated but use to specially test this API.
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String item = request.getParameter("item");

            //in HttpClient 4.x, there is no GetMethod anymore. Instead there is HttpGet
            HttpGet httpget = new HttpGet("http://host.com?param=" + URLEncoder.encode(item)); //OK
            // ruleid: java_inject_rule-HttpParameterPollution
            HttpGet httpget2 = new HttpGet("http://host.com?param=" + item); //BAD
            HttpGet httpget3 = new HttpGet("http://host.com?param=" + urlPathSegmentEscaper().escape(item)); //OK

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            //get.execute();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-OgnlInjection
        reflectionProvider.setProperties(propsInput, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

// License: MIT (c) GitLab Inc.
package cookie;

import org.apache.commons.text.StringEscapeUtils;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class RequestParamToHeader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramNames = req.getParameterNames().nextElement();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramValues = req.getParameterValues("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramMap = req.getParameterMap().get("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
        //ruleid: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", contextPath);

        String tainted = req.getParameter("input");
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", input);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", input);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", header);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", header);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", contextPath);

    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/xxe/documentbuilderfactory-disallow-doctype-decl-missing.java

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

class GoodDocumentBuilderFactory {
    public void GoodDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

    public void GoodDocumentBuilderFactory2() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

    public void GoodDocumentBuilderFactory3() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

    public void GoodDocumentBuilderFactory4() throws ParserConfigurationException {
        DocumentBuilderFactory factory = XmlUtils.getSecureDocumentBuilderFactory();
        // Deep semgrep could find issues like this
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        DocumentBuilder documentBuilder = factory.newDocumentBuilder();
    }
}

class BadDocumentBuilderFactory {
    public void BadDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ruleid:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

    public void BadDocumentBuilderFactory2() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature("somethingElse", true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodDocumentBuilderFactoryStatic {

    private static DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    static {
        try {
            dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
            dbf.newDocumentBuilder();
        } catch (ParserConfigurationException e){
            System.out.println(e.getMessage());
        }
    }

    public void doSomething() throws ParserConfigurationException {
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

}

class BadDocumentBuilderFactoryStatic {

    private static DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

    static {
        try {
            dbf.setFeature("not-a-secure-feature", true);
        } catch (ParserConfigurationException e) {
            System.out.println(e.getMessage());
        }
    }

    public void doSomething() throws ParserConfigurationException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

class OneMoreGoodDocumentBuilderFactory {

    public void GoodDocumentBuilderFactory(boolean condition) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = null;

        if (condition) {
            dbf = DocumentBuilderFactory.newInstance();
        } else {
            dbf = newFactory();
        }
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

    private DocumentBuilderFactory newFactory() {
        return DocumentBuilderFactory.newInstance();
    }

}

class OneMoreBadDocumentBuilderFactory {

    public void GoodDocumentBuilderFactory(boolean condition) throws ParserConfigurationException {
        DocumentBuilderFactory dbf = null;

        if (condition) {
            dbf = DocumentBuilderFactory.newInstance();
        } else {
            dbf = newFactory();
        }
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    private DocumentBuilderFactory newFactory() {
        return DocumentBuilderFactory.newInstance();
    }

}

class GoodDocumentBuilderFactoryCtr {

    private final DocumentBuilderFactory dbf;

    public GoodDocumentBuilderFactoryCtr() throws Exception {
        dbf = DocumentBuilderFactory.newInstance();
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }
}

class GoodDocumentBuilderFactoryCtr2 {
    public void somemethod() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        setFeatures(dbf);
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

    private void setFeatures(DocumentBuilderFactory dbf) throws Exception {
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    }

}

class GoodDocumentBuilderFactoryCtr3 {
    public void somemethod() throws Exception {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        setFeatures(dbf);
        // ok:java_xxe_rule-DocumentBuilderFactoryDisallowDoctypeDeclMissing
        dbf.newDocumentBuilder();
    }

    private void setFeatures(DocumentBuilderFactory dbf) throws Exception {
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        File.createTempFile("safe", input, new File("safeDir")); // BAD, DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package templateinjection;

import com.mitchellbosecke.pebble.error.PebbleException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.VelocityContext;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

import java.io.*;
import java.util.*;

public class TemplateInjection {

    public void usage1(String inputFile) throws FileNotFoundException {
        Velocity.init();

        VelocityContext context = new VelocityContext();

        context.put("author", "Elliot A.");
        context.put("address", "217 E Broadway");
        context.put("phone", "555-1337");

        FileInputStream file = new FileInputStream(inputFile);

        //Evaluate
        StringWriter swOut = new StringWriter();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String result =  swOut.getBuffer().toString();
        System.out.println(result);
    }

    public void allSignatures(InputStream inputStream, Reader fileReader, String template) throws FileNotFoundException {
        VelocityContext context = new VelocityContext();
        StringWriter swOut = new StringWriter();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_templateinjection_rule-TemplateInjection
        Velocity.evaluate(context, swOut, "test", template);
    }

    public void falsePositive() throws FileNotFoundException {
        VelocityContext context = new VelocityContext();
        StringWriter swOut = new StringWriter();

        Velocity.evaluate(context, swOut, "test", "Hello $user !");
    }

    public void simple1(String inputFile) {

        //Freemarker configuration object
        Configuration cfg = new Configuration();
        try {
            //Load template from source folder
            Template template = cfg.getTemplate(inputFile);

            // Build the data-model
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("message", "Hello World!");

            //List parsing
            List<String> countries = new ArrayList<String>();
            countries.add("India");
            countries.add("United States");
            countries.add("Germany");
            countries.add("France");

            data.put("countries", countries);

            // Console output
            Writer out = new OutputStreamWriter(System.out);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    public void allSignatures1(String inputFile) throws IOException, TemplateException {
        Configuration cfg = new Configuration();
        Template template = cfg.getTemplate(inputFile);
        Map<String, Object> data = new HashMap<String, Object>();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void simple(String inputFile) throws IOException {

        PebbleEngine engine = new PebbleEngine.Builder().build();
        PebbleTemplate compiledTemplate = null;
        try {
            compiledTemplate = engine.getTemplate(inputFile);
        } catch (PebbleException e) {
            e.printStackTrace();
        }

        Map<String, Object> context = new HashMap<>();
        context.put("name", "Shivam");

        Writer writer = new StringWriter();
        try {
            compiledTemplate.evaluate(writer, context);
        } catch (PebbleException e) {
            e.printStackTrace();
        }

        String output = writer.toString();
    }

    public void allSignatures(String inputFile) throws IOException, PebbleException {
        PebbleEngine engine = new PebbleEngine.Builder().build();
        PebbleTemplate compiledTemplate = null;

        compiledTemplate = engine.getTemplate(inputFile);

        Map<String, Object> data = new HashMap<String, Object>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/file/FileDisclosure.java
// hash: a7694d0

package injection;

import java.io.IOException;
import org.apache.struts.action.ActionForward;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

// REQUESTDISPATCHER_FILE_DISCLOSURE
public class FileDisclosureSpringFramework extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String returnURL = request.getParameter("returnURL");

            /******Spring ModelAndView vulnerable code tests******/
            // ruleid: java_inject_rule-FileDisclosureSpringFramework
            ModelAndView mv = new ModelAndView(returnURL); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            ModelAndView mv4 = new ModelAndView();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            // ok: java_inject_rule-FileDisclosureSpringFramework
            ModelAndView mv5 = new ModelAndView("viewName", returnURL, new Object()); //OK

            // Create a look up table or pull from a data source
            HashMap<String, String> lookupTable = new HashMap<>();
            lookupTable.put("key1", "/ServletSample/simpleForm.jsp");
            // Look up resource to redirect to from the user input
            String redirectValue = lookupTable.getOrDefault(returnURL, "/ServletSample/index.html");
            ModelAndView mv6 = new ModelAndView();
            // ok: java_inject_rule-FileDisclosureSpringFramework
            mv6.setViewName(redirectValue);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/xxe/transformerfactory-dtds-not-disabled.java

import javax.xml.transform.TransformerFactory;

class TransformerFactory {
    public void GoodTransformerFactory(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void GoodTransformerFactory2(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void GoodTransformerFactory3(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void GoodTransformerFactory4(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void BadTransformerFactory(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        // ruleid:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

    public void BadTransformerFactory2(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void BadTransformerFactory3(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void BadTransformerFactory4(String xmlContent) {
        TransformerFactory factory = TransformerFactory.newInstance();
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

class TransformerFactory2 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
    }

    public TransformerFactory2(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory3 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    }

    public TransformerFactory3(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory4 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
    }

    public TransformerFactory4(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory5 {
    static TransformerFactory factory = TransformerFactory.newInstance();

    static {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
    }

    public TransformerFactory5(String xmlContent) {
        goodTransformerFactory(xmlContent);
    }

    public void goodTransformerFactory(String xmlContent) {

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory6 {

    TransformerFactory factory = null;

    public void parserFactory(boolean condition, String xmlContent) throws ParserConfigurationException {

        if (condition) {
            factory = TransformerFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }

        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");

        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t2 = factory.newTransformer(new StreamSource(xmlContent));
    }

}

class TransformerFactory7 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
    }

}

class TransformerFactory8 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
        factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
    }

}

class TransformerFactory9 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
    }

}

class TransformerFactory10 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
        // ok:java_xxe_rule-TransformerfactoryDTDNotDisabled
        Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        factory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    }

}

class TransformerFactory11 {
    public void somemethod(String xmlContent) throws Exception {
        TransformerFactory factory = TransformerFactory.newInstance();
        setFeatures(factory);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    private void setFeatures(TransformerFactory factory) throws Exception {
        factory.setAttribute("Random", "");
    }

}

// This works only with pro-engine. I was hoping it'll work with
// `pattern-sources: - by-side-effect: true`, but it doesn't.
// todo rules
class TransformerFactory12 {

    TransformerFactory factory = null;

    public void parserFactory(boolean condition, String xmlContent) throws ParserConfigurationException {

        if (condition) {
            factory = newFactory();
            // todoruleid: java_xxe_rule-TransformerfactoryDTDNotDisabled
            Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
        } else {
            factory = newFactory();
            factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalStylesheet", "");
            factory.setAttribute("http://javax.xml.XMLConstants/property/accessExternalDTD", "");
            // todook: java_xxe_rule-TransformerfactoryDTDNotDisabled
            Transformer t1 = factory.newTransformer(new StreamSource(xmlContent));
        }
    }

    private TransformerFactory newFactory() {
        TransformerFactory factory = TransformerFactory.newInstance();
        return factory;
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
        // ruleid: java_inject_rule-OgnlInjection
        ognlUtil.setValue(input, null, null, "12345");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package templateinjection;

import com.mitchellbosecke.pebble.error.PebbleException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.VelocityContext;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

import java.io.*;
import java.util.*;

public class TemplateInjection {

    public void usage1(String inputFile) throws FileNotFoundException {
        Velocity.init();

        VelocityContext context = new VelocityContext();

        context.put("author", "Elliot A.");
        context.put("address", "217 E Broadway");
        context.put("phone", "555-1337");

        FileInputStream file = new FileInputStream(inputFile);

        //Evaluate
        StringWriter swOut = new StringWriter();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String result =  swOut.getBuffer().toString();
        System.out.println(result);
    }

    public void allSignatures(InputStream inputStream, Reader fileReader, String template) throws FileNotFoundException {
        VelocityContext context = new VelocityContext();
        StringWriter swOut = new StringWriter();

        // ruleid: java_templateinjection_rule-TemplateInjection
        Velocity.evaluate(context, swOut, "test", inputStream.toString());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void falsePositive() throws FileNotFoundException {
        VelocityContext context = new VelocityContext();
        StringWriter swOut = new StringWriter();

        Velocity.evaluate(context, swOut, "test", "Hello $user !");
    }

    public void simple1(String inputFile) {

        //Freemarker configuration object
        Configuration cfg = new Configuration();
        try {
            //Load template from source folder
            Template template = cfg.getTemplate(inputFile);

            // Build the data-model
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("message", "Hello World!");

            //List parsing
            List<String> countries = new ArrayList<String>();
            countries.add("India");
            countries.add("United States");
            countries.add("Germany");
            countries.add("France");

            data.put("countries", countries);

            // Console output
            Writer out = new OutputStreamWriter(System.out);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    public void allSignatures1(String inputFile) throws IOException, TemplateException {
        Configuration cfg = new Configuration();
        Template template = cfg.getTemplate(inputFile);
        Map<String, Object> data = new HashMap<String, Object>();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void simple(String inputFile) throws IOException {

        PebbleEngine engine = new PebbleEngine.Builder().build();
        PebbleTemplate compiledTemplate = null;
        try {
            compiledTemplate = engine.getTemplate(inputFile);
        } catch (PebbleException e) {
            e.printStackTrace();
        }

        Map<String, Object> context = new HashMap<>();
        context.put("name", "Shivam");

        Writer writer = new StringWriter();
        try {
            compiledTemplate.evaluate(writer, context);
        } catch (PebbleException e) {
            e.printStackTrace();
        }

        String output = writer.toString();
    }

    public void allSignatures(String inputFile) throws IOException, PebbleException {
        PebbleEngine engine = new PebbleEngine.Builder().build();
        PebbleTemplate compiledTemplate = null;

        compiledTemplate = engine.getTemplate(inputFile);

        Map<String, Object> data = new HashMap<String, Object>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

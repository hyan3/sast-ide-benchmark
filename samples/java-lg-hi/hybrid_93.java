public final class BitmapUtils {
    public static ActivityManager activityManager;

    private BitmapUtils() {
    }

    public static void initialize(Context context) {
        activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
    }

    public static long availableMemory() {
        final Runtime runtime = Runtime.getRuntime();
        final long used = runtime.totalMemory() - runtime.freeMemory();

        final long total = activityManager.getMemoryClass() * 1024 * 1024;

        return total - used;
    }

    public static Bitmap decodeStream(@NonNull InputStreamPipe isp, int maxWidth, int maxHeight,
                                      int pixels, boolean checkMemory, boolean justCalc, int[] sampleSize) {
        try {
            isp.obtain();

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(isp.open(), null, options);
            isp.close();

            int width = options.outWidth;
            int height = options.outHeight;
            if (width <= 0 || height <= 0) {
                if (sampleSize != null && sampleSize.length >= 1) {
                    sampleSize[0] = -1;
                }
                return null;
            }

            int scaleW = 1;
            int scaleH = 1;
            int scaleP = 1;
            int scaleM = 1;
            if (maxWidth > 0 && width > maxWidth) {
                scaleW = MathUtils.ceilDivide(width, maxWidth);
            }
            if (maxHeight > 0 && height > maxHeight) {
                scaleH = MathUtils.ceilDivide(height, maxHeight);
            }
            if (pixels > 0 && width * height > pixels) {
                scaleP = (int) Math.ceil(Math.sqrt(width * height / (float) pixels));
            }
            if (checkMemory) {
                long m = availableMemory() - 5 * 1024 * 1024; // Leave 5m
                if (m < 0) {
                    if (sampleSize != null && sampleSize.length >= 1) {
                        sampleSize[0] = -1;
                    }
                    return null;
                }
                if (width * height * 3 > m) {
                    scaleM = (int) Math.ceil(Math.sqrt(width * height * 3 / (float) m));
                }
            }
            options.inSampleSize = MathUtils.nextPowerOf2(MathUtils.max(scaleW, scaleH, scaleP, scaleM, 1));
            if (sampleSize != null && sampleSize.length >= 1) {
                sampleSize[0] = options.inSampleSize;
            }

            options.inJustDecodeBounds = false;

            if (justCalc) {
                return null;
            } else {
                try {
                    return BitmapFactory.decodeStream(isp.open(), null, options);
                } catch (OutOfMemoryError e) {
                    if (sampleSize != null && sampleSize.length >= 1) {
                        sampleSize[0] = -1;
                    }
                    return null;
                }
            }
        } catch (IOException e) {
            if (sampleSize != null && sampleSize.length >= 1) {
                sampleSize[0] = -1;
            }
            return null;
        } finally {
            isp.close();
            isp.release();
        }
    }

    /**
     * @return null or the bitmap
     */
    public static Bitmap decodeStream(@NonNull InputStreamPipe isp, int maxWidth, int maxHeight) {
        return decodeStream(isp, maxWidth, maxHeight, -1, false, false, null);
    }
}

public final class Utils {

    private static final String TAG = "Utils";

    private static final int BUFFER_SIZE = 4096;

    private static final String[] FRIENDLY_SIZE_FORMAT = {
        "%.0f B", "%.0f KiB", "%.1f MiB", "%.2f GiB",
    };

    private static RequestOptions iconRequestOptions;
    private static RequestOptions alwaysShowIconRequestOptions;

    private static Pattern safePackageNamePattern;

    private static Handler toastHandler;

    @NonNull
    public static Uri getUri(String repoAddress, String... pathElements) {
        /*
         * Storage Access Framework URLs have this wacky URL-encoded path within the URL path.
         *
         * i.e.
         * content://authority/tree/313E-1F1C%3A/document/313E-1F1C%3Aguardianproject.info%2Ffdroid%2Frepo
         *
         * Currently don't know a better way to identify these than by content:// prefix,
         * seems the Android SDK expects apps to consider them as opaque identifiers.
         *
         * Note: This hack works for the external storage documents provider for now,
         *       but will most likely fail for other providers.
         *       Using DocumentFile off the UiThread can be used to build path Uris reliably.
         */
        if (repoAddress.startsWith("content://")) {
            StringBuilder result = new StringBuilder(repoAddress);
            for (String element : pathElements) {
                result.append(TreeUriDownloader.ESCAPED_SLASH);
                result.append(element);
            }
            return Uri.parse(result.toString());
        } else { // Normal URL
            Uri.Builder result = Uri.parse(repoAddress).buildUpon();
            for (String element : pathElements) {
                result.appendPath(element);
            }
            return result.build();
        }
    }

    /**
     * Returns the repository address. Usually this is {@link Repository#getAddress()},
     * but in case of a content:// repo, we need to take its local Uri instead,
     * so we can know that we need to use different downloaders for non-HTTP locations.
     */
    public static String getRepoAddress(Repository repository) {
        List<Mirror> mirrors = repository.getMirrors();
        // check if we need to account for non-HTTP mirrors
        String nonHttpUri = null;
        for (Mirror m : mirrors) {
            if (ContentResolver.SCHEME_CONTENT.equals(m.getUrl().getProtocol().getName())
                    || ContentResolver.SCHEME_FILE.equals(m.getUrl().getProtocol().getName())) {
                nonHttpUri = m.getBaseUrl();
                break;
            }
        }
        // return normal canonical URL, if this is a pure HTTP repo
        if (nonHttpUri == null) {
            String address = repository.getAddress();
            if (address.endsWith("/")) return address.substring(0, address.length() - 1);
            return address;
        } else {
            return nonHttpUri;
        }
    }

    /**
     * @return the directory where cached icons/feature graphics/screenshots are stored
     */
    public static File getImageCacheDir(Context context) {
        File cacheDir = Glide.getPhotoCacheDir(context.getApplicationContext());
        return new File(cacheDir, "icons");
    }

    public static long getImageCacheDirAvailableMemory(Context context) {
        File statDir = getImageCacheDir(context);
        while (statDir != null && !statDir.exists()) {
            statDir = statDir.getParentFile();
        }
        if (statDir == null) {
            return 50 * 1024 * 1024; // just return a minimal amount
        }
        StatFs stat = new StatFs(statDir.getPath());
        return stat.getAvailableBlocksLong() * stat.getBlockSizeLong();
    }

    public static long getImageCacheDirTotalMemory(Context context) {
        File statDir = getImageCacheDir(context);
        while (statDir != null && !statDir.exists()) {
            statDir = statDir.getParentFile();
        }
        if (statDir == null) {
            return 100 * 1024 * 1024; // just return a minimal amount
        }
        StatFs stat = new StatFs(statDir.getPath());
        return stat.getBlockCountLong() * stat.getBlockSizeLong();
    }

    public static void copy(InputStream input, OutputStream output) throws IOException {
        byte[] buffer = new byte[BUFFER_SIZE];
        while (true) {
            int count = input.read(buffer);
            if (count == -1) {
                break;
            }
            output.write(buffer, 0, count);
        }
        output.flush();
    }

    /**
     * Attempt to symlink, but if that fails, it will make a copy of the file.
     */
    public static boolean symlinkOrCopyFileQuietly(SanitizedFile inFile, SanitizedFile outFile) {
        return FileCompat.symlink(inFile, outFile) || copyQuietly(inFile, outFile);
    }

    /**
     * Read the input stream until it reaches the end, ignoring any exceptions.
     */
    public static void consumeStream(InputStream stream) {
        final byte[] buffer = new byte[256];
        try {
            int read;
            do {
                read = stream.read(buffer);
            } while (read != -1);
        } catch (IOException e) {
            // Ignore...
        }
    }

    private static boolean copyQuietly(File inFile, File outFile) {
        InputStream input = null;
        OutputStream output = null;
        try {
            input = new FileInputStream(inFile);
            output = new FileOutputStream(outFile);
            Utils.copy(input, output);
            return true;
        } catch (IOException e) {
            Log.e(TAG, "I/O error when copying a file", e);
            return false;
        } finally {
            closeQuietly(output);
            closeQuietly(input);
        }
    }

    public static void closeQuietly(Closeable closeable) {
        if (closeable == null) {
            return;
        }
        try {
            closeable.close();
        } catch (IOException ioe) {
            // ignore
        }
    }

    public static String getFriendlySize(long size) {
        double s = size;
        int i = 0;
        while (i < FRIENDLY_SIZE_FORMAT.length - 1 && s >= 1024) {
            s = (100 * s / 1024) / 100.0;
            i++;
        }
        return String.format(FRIENDLY_SIZE_FORMAT[i], s);
    }

    private static final String[] ANDROID_VERSION_NAMES = {
        "?", // 0, undefined
        "1.0", // 1
        "1.1", // 2
        "1.5", // 3
        "1.6", // 4
        "2.0", // 5
        "2.0.1", // 6
        "2.1", // 7
        "2.2", // 8
        "2.3", // 9
        "2.3.3", // 10
        "3.0", // 11
        "3.1", // 12
        "3.2", // 13
        "4.0", // 14
        "4.0.3", // 15
        "4.1", // 16
        "4.2", // 17
        "4.3", // 18
        "4.4", // 19
        "4.4W", // 20
        "5.0", // 21
        "5.1", // 22
        "6.0", // 23
        "7.0", // 24
        "7.1", // 25
        "8.0", // 26
        "8.1", // 27
        "9", // 28
        "10", // 29
        "11", // 30
        "12", // 31
        "12.1", // 32
        "13", // 33
        "14", // 34
    };

    public static String getAndroidVersionName(int sdkLevel) {
        if (sdkLevel < 0) {
            return ANDROID_VERSION_NAMES[0];
        }
        if (sdkLevel >= ANDROID_VERSION_NAMES.length) {
            return String.format(Locale.ENGLISH, "v%d", sdkLevel);
        }
        return ANDROID_VERSION_NAMES[sdkLevel];
    }

    // return a fingerprint formatted for display
    public static String formatFingerprint(Context context, String fingerprint) {
        if (TextUtils.isEmpty(fingerprint)
                || fingerprint.length() != 64 // SHA-256 is 64 hex chars
                || fingerprint.matches(".*[^0-9a-fA-F].*")) { // its a hex string
            return context.getString(R.string.bad_fingerprint);
        }
        StringBuilder displayFP = new StringBuilder(fingerprint.substring(0, 2));
        for (int i = 2; i < fingerprint.length(); i = i + 2) {
            displayFP.append(" ").append(fingerprint.substring(i, i + 2));
        }
        return displayFP.toString().toUpperCase(Locale.US);
    }

    @NonNull
    public static Uri getLocalRepoUri(Repository repo) {
        if (TextUtils.isEmpty(repo.getAddress())) {
            return Uri.parse("http://wifi-not-enabled");
        }
        Uri uri = Uri.parse(repo.getAddress());
        Uri.Builder b = uri.buildUpon();
        if (!TextUtils.isEmpty(repo.getCertificate())) {
            String fingerprint = DigestUtils.sha256Hex(repo.getCertificate());
            b.appendQueryParameter("fingerprint", fingerprint);
        }
        String scheme = Preferences.get().isLocalRepoHttpsEnabled() ? "https" : "http";
        b.scheme(scheme);
        return b.build();
    }

    public static Uri getSharingUri(Repository repo) {
        if (repo == null || TextUtils.isEmpty(repo.getAddress())) {
            return Uri.parse("http://wifi-not-enabled");
        }
        Uri localRepoUri = getLocalRepoUri(repo);
        Uri.Builder b = localRepoUri.buildUpon();
        b.scheme(localRepoUri.getScheme().replaceFirst("http", "fdroidrepo"));
        b.appendQueryParameter("swap", "1");
        if (!TextUtils.isEmpty(FDroidApp.bssid)) {
            b.appendQueryParameter("bssid", FDroidApp.bssid);
            if (!TextUtils.isEmpty(FDroidApp.ssid)) {
                b.appendQueryParameter("ssid", FDroidApp.ssid);
            }
        }
        return b.build();
    }

    public static String calcFingerprint(Certificate cert) {
        if (cert == null) {
            return null;
        }
        try {
            return calcFingerprint(cert.getEncoded());
        } catch (CertificateEncodingException e) {
            return null;
        }
    }

    private static String calcFingerprint(byte[] key) {
        if (key == null) {
            return null;
        }
        if (key.length < 256) {
            Log.e(TAG, "key was shorter than 256 bytes (" + key.length + "), cannot be valid!");
            return null;
        }
        String ret = null;
        try {
            // keytool -list -v gives you the SHA-256 fingerprint
            MessageDigest digest = MessageDigest.getInstance("sha256");
            digest.update(key);
            byte[] fingerprint = digest.digest();
            Formatter formatter = new Formatter(new StringBuilder());
            for (byte aFingerprint : fingerprint) {
                formatter.format("%02X", aFingerprint);
            }
            ret = formatter.toString();
            formatter.close();
        } catch (Throwable e) { // NOPMD
            Log.w(TAG, "Unable to get certificate fingerprint", e);
        }
        return ret;
    }

    /**
     * Checks the file against the provided hash, returning whether it is a match.
     */
    public static boolean isFileMatchingHash(File file, String hash, String hashType) {
        if (file == null || !file.exists() || TextUtils.isEmpty(hash)) {
            return false;
        }
        return hash.equals(getFileHexDigest(file, hashType));
    }

    /**
     * Get the standard, lowercase SHA-256 fingerprint used to represent an
     * APK or JAR signing key. <b>NOTE</b>: this does not handle signers that
     * have multiple X.509 signing certificates.
     * <p>
     * Calling the X.509 signing certificate the "signature" is incorrect, e.g.
     * {@link PackageInfo#signatures} or {@link android.content.pm.Signature}.
     * The Android docs about APK signatures call this the "signer".
     *
     * @see org.fdroid.fdroid.data.Apk#signer
     * @see PackageInfo#signatures
     * @see <a href="https://source.android.com/docs/security/features/apksigning/v2">APK Signature Scheme v2</a>
     */
    @Nullable
    public static String getPackageSigner(PackageInfo info) {
        if (info == null || info.signatures == null || info.signatures.length < 1) {
            return null;
        }
        return DigestUtils.sha256Hex(info.signatures[0].toByteArray());
    }

    /**
     * Gets the {@link RequestOptions} instance used to configure
     * {@link Glide} instances used to display app icons that should always be
     * downloaded.  It lazy loads a reusable static instance.
     */
    public static RequestOptions getAlwaysShowIconRequestOptions() {
        if (alwaysShowIconRequestOptions == null) {
            alwaysShowIconRequestOptions = new RequestOptions()
                    .onlyRetrieveFromCache(false)
                    .error(R.drawable.ic_repo_app_default)
                    .fallback(R.drawable.ic_repo_app_default);
        }
        return alwaysShowIconRequestOptions;
    }

    /**
     * Write app icon into the view, downloading it as necessary and if the
     * settings allow it.  Fall back to the placeholder icon otherwise.
     *
     * @see Preferences#isBackgroundDownloadAllowed()
     */
    public static void setIconFromRepoOrPM(@NonNull App app, ImageView iv, Context context) {
        loadWithGlide(context, app.repoId, app.iconFile, iv);
    }

    @Deprecated
    public static void setIconFromRepoOrPM(@NonNull AppOverviewItem app, ImageView iv, Context context) {
        long repoId = app.getRepoId();
        IndexFile iconFile = app.getIcon(App.getLocales());
        loadWithGlide(context, repoId, iconFile, iv);
    }

    public static void loadWithGlide(Context context, long repoId, @Nullable IndexFile file, ImageView iv) {
        if (file == null) {
            Glide.with(context).clear(iv);
            iv.setImageResource(R.drawable.ic_repo_app_default);
            return;
        }
        if (iconRequestOptions == null) {
            iconRequestOptions = new RequestOptions()
                    .error(R.drawable.ic_repo_app_default)
                    .fallback(R.drawable.ic_repo_app_default);
        }
        RequestOptions options = iconRequestOptions.onlyRetrieveFromCache(
                !Preferences.get().isBackgroundDownloadAllowed());

        Repository repo = FDroidApp.getRepoManager(context).getRepository(repoId);
        if (repo == null) {
            Glide.with(context).clear(iv);
            return;
        }
        Object model = getGlideModel(repo, file);
        if (model == null) {
            Glide.with(context).clear(iv);
            return;
        }
        Glide.with(context).load(model).apply(options).into(iv);
    }

    @Nullable
    public static Object getGlideModel(@NonNull Repository repo, @Nullable IndexFile file) {
        if (file == null) return null;

        String address = getRepoAddress(repo);
        if (address.startsWith("content://") || address.startsWith("file://")) {
            return getUri(address, file.getName().split("/")).toString();
        }
        return getDownloadRequest(repo, file);
    }

    @Nullable
    private static DownloadRequest getDownloadRequest(@NonNull Repository repo, @Nullable IndexFile file) {
        if (file == null) return null;
        List<Mirror> mirrors = repo.getMirrors();
        Proxy proxy = NetCipher.getProxy();
        return new DownloadRequest(file, mirrors, proxy, repo.getUsername(), repo.getPassword());
    }

    /**
     * Get the checksum hash of the file {@code file} using the algorithm in {@code hashAlgo}.
     * {@code file} must exist on the filesystem and {@code hashAlgo} must be supported
     * by this device, otherwise an {@link IllegalArgumentException} is thrown.  This
     * method must be very defensive about checking whether the file exists, since APKs
     * can be uninstalled/deleted in background at any time, even if this is in the
     * middle of running.
     * <p>
     * This also will run into filesystem corruption if the device is having trouble.
     * So hide those so F-Droid does not pop up crash reports about that. As such this
     * exception-message-parsing-and-throwing-a-new-ignorable-exception-hackery is
     * probably warranted. See https://www.gitlab.com/fdroid/fdroidclient/issues/855
     * for more detail.
     *
     * @see
     * <a href="https://gitlab.com/fdroid/fdroidclient/-/merge_requests/1089#note_822501322">forced to vendor Apache Commons Codec</a>
     */
    @Nullable
    static String getFileHexDigest(File file, String hashAlgo) {
        try {
            return Hex.encodeHexString(DigestUtils.digest(DigestUtils.getDigest(hashAlgo), file));
        } catch (IOException e) {
            String message = e.getMessage();
            if (message.contains("read failed: EIO (I/O error)")) {
                Utils.debugLog(TAG, "potential filesystem corruption while accessing " + file + ": " + message);
            } else if (message.contains(" ENOENT ")) {
                Utils.debugLog(TAG, file + " vanished: " + message);
            }
        }
        return null;
    }

    /**
     * Formats the app name using "sans-serif" and then appends the summary after a space with
     * "sans-serif-light". Doesn't mandate any font sizes or any other styles, that is up to the
     * {@link android.widget.TextView} which it ends up being displayed in.
     */
    public static CharSequence formatAppNameAndSummary(String appName, @Nullable String summary) {
        String toFormat = appName;
        if (summary != null) toFormat += ' ' + summary;
        CharacterStyle normal = new TypefaceSpan("sans-serif");
        CharacterStyle light = new TypefaceSpan("sans-serif-light");

        SpannableStringBuilder sb = new SpannableStringBuilder(toFormat);
        sb.setSpan(normal, 0, appName.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        sb.setSpan(light, appName.length(), toFormat.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        return sb;
    }

    /**
     * This is not strict validation of the package name, this is just to make
     * sure that the package name is not used as an attack vector, e.g. SQL
     * Injection.
     */
    public static boolean isSafePackageName(@Nullable String packageName) {
        if (TextUtils.isEmpty(packageName)) {
            return false;
        }
        if (safePackageNamePattern == null) {
            safePackageNamePattern = Pattern.compile("[a-zA-Z0-9._]+");
        }
        return safePackageNamePattern.matcher(packageName).matches();
    }

    /**
     * Calculate the number of days since the given date.
     */
    public static int daysSince(long ms) {
        long msDiff = Calendar.getInstance().getTimeInMillis() - ms;
        return (int) TimeUnit.MILLISECONDS.toDays(msDiff);
    }

    public static String formatLastUpdated(@NonNull Resources res, @NonNull Date date) {
        return formatLastUpdated(res, date.getTime());
    }

    public static String formatLastUpdated(@NonNull Resources res, long date) {
        double msDiff = System.currentTimeMillis() - date;
        long days = Math.round(msDiff / DateUtils.DAY_IN_MILLIS);
        long weeks = Math.round(msDiff / (DateUtils.WEEK_IN_MILLIS));
        long months = Math.round(msDiff / (DateUtils.DAY_IN_MILLIS * 30));
        long years = Math.round(msDiff / (DateUtils.YEAR_IN_MILLIS));

        if (days < 1) {
            return res.getString(R.string.details_last_updated_today);
        } else if (weeks < 3) {
            return res.getQuantityString(R.plurals.details_last_update_days, (int) days, days);
        } else if (months < 2) {
            return res.getQuantityString(R.plurals.details_last_update_weeks, (int) weeks, weeks);
        } else if (years < 2) {
            return res.getQuantityString(R.plurals.details_last_update_months, (int) months, months);
        } else {
            return res.getQuantityString(R.plurals.details_last_update_years, (int) years, years);
        }
    }

    /**
     * Need this to add the unimplemented support for ordered and unordered
     * lists to Html.fromHtml().
     */
    public static class HtmlTagHandler implements Html.TagHandler {
        int listNum;

        @Override
        public void handleTag(boolean opening, String tag, Editable output,
                              XMLReader reader) {
            switch (tag) {
                case "ul":
                    if (opening) {
                        listNum = -1;
                    } else {
                        output.append('\n');
                    }
                    break;
                case "ol":
                    if (opening) {
                        listNum = 1;
                    } else {
                        output.append('\n');
                    }
                    break;
                case "li":
                    if (opening) {
                        if (listNum == -1) {
                            output.append("\t• ");
                        } else {
                            output.append("\t").append(Integer.toString(listNum)).append(". ");
                            listNum++;
                        }
                    } else {
                        output.append('\n');
                    }
                    break;
            }
        }
    }

    public static void debugLog(String tag, String msg) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg);
        }
    }

    public static void debugLog(String tag, String msg, Throwable tr) {
        if (BuildConfig.DEBUG) {
            Log.d(tag, msg, tr);
        }
    }

    /**
     * Try to get the {@link PackageInfo#versionName} of the
     * client.
     *
     * @return null on failure
     */
    public static String getVersionName(Context context) {
        String versionName = null;
        try {
            versionName = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Could not get client version name", e);
        }
        return versionName;
    }

    public static String getApplicationLabel(Context context, String packageName) {
        PackageManager pm = context.getPackageManager();
        ApplicationInfo appInfo;
        try {
            appInfo = pm.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            return appInfo.loadLabel(pm).toString();
        } catch (PackageManager.NameNotFoundException | Resources.NotFoundException e) {
            Utils.debugLog(TAG, "Could not get application label: " + e.getMessage());
        }
        return packageName; // all else fails, return packageName
    }

    public static String getUserAgent() {
        return "F-Droid " + BuildConfig.VERSION_NAME;
    }

    /**
     * Try to get the {@link PackageInfo} for the {@code packageName} provided.
     *
     * @return null on failure
     */
    public static PackageInfo getPackageInfo(Context context, String packageName) {
        try {
            return context.getPackageManager().getPackageInfo(packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
            debugLog(TAG, "Could not get PackageInfo: ", e);
        }
        return null;
    }

    /**
     * Converts a {@code long} bytes value, like from {@link File#length()}, to
     * an {@code int} value that is kilobytes, suitable for things like
     * {@link android.widget.ProgressBar#setMax(int)} or
     * {@link androidx.core.app.NotificationCompat.Builder#setProgress(int, int, boolean)}
     */
    public static int bytesToKb(long bytes) {
        return (int) (bytes / 1024);
    }

    /**
     * Converts two {@code long} bytes values, like from {@link File#length()}, to
     * an {@code int} value that is a percentage, suitable for things like
     * {@link android.widget.ProgressBar#setMax(int)} or
     * {@link androidx.core.app.NotificationCompat.Builder#setProgress(int, int, boolean)}.
     * @param current should be smaller than {@link Long#MAX_VALUE} / 100
     * @param total must never be zero!
     */
    public static int getPercent(long current, long total) {
        return (int) (100L * current / total);
    }

    @SuppressWarnings("unused")
    public static class Profiler {
        public final long startTime = System.currentTimeMillis();
        public final String logTag;

        public Profiler(String logTag) {
            this.logTag = logTag;
        }

        public void log(String message) {
            long duration = System.currentTimeMillis() - startTime;
            Utils.debugLog(logTag, "[" + duration + "ms] " + message);
        }
    }

    /**
     * In order to send a {@link Toast} from a {@link android.app.Service}, we
     * have to do these tricks.
     */
    public static void showToastFromService(final Context context, final String msg, final int length) {
        if (toastHandler == null) {
            toastHandler = new Handler(Looper.getMainLooper());
        }
        toastHandler.post(() -> Toast.makeText(context.getApplicationContext(), msg, length).show());
    }

    public static void applySwipeLayoutColors(SwipeRefreshLayout swipeLayout) {
        TypedValue typedValue = new TypedValue();
        Resources.Theme theme = swipeLayout.getContext().getTheme();
        theme.resolveAttribute(android.R.attr.colorPrimary, typedValue, true);
        swipeLayout.setColorSchemeColors(typedValue.data);
    }

    public static boolean canConnectToSocket(String host, int port) {
        try {
            Socket socket = new Socket();
            socket.connect(new InetSocketAddress(host, port), 5);
            socket.close();
            return true;
        } catch (IOException e) {
            // Could not connect.
            return false;
        }
    }

    public static boolean isServerSocketInUse(int port) {
        try {
            (new ServerSocket(port)).close();
            return false;
        } catch (IOException e) {
            // Could not connect.
            return true;
        }
    }

    @NonNull
    public static Single<Bitmap> generateQrBitmap(@NonNull final AppCompatActivity activity,
                                                  @NonNull final String qrData) {
        return Single.fromCallable(() -> {
            final DisplayCompat.ModeCompat displayMode = DisplayCompat.getMode(activity,
                    activity.getWindowManager().getDefaultDisplay());
            final int qrCodeDimension = Math.min(displayMode.getPhysicalWidth(),
                    displayMode.getPhysicalHeight());
            debugLog(TAG, "generating QRCode Bitmap of " + qrCodeDimension + "x" + qrCodeDimension);

            return new QRCodeEncoder(qrData, null, Contents.Type.TEXT,
                    BarcodeFormat.QR_CODE.toString(), qrCodeDimension).encodeAsBitmap();
        })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorReturnItem(Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888))
                .doOnError(throwable -> Log.e(TAG, "Could not encode QR as bitmap", throwable));
    }

    public static <T> Disposable runOffUiThread(Supplier<T> supplier, Consumer<T> consumer) {
        return Single.fromCallable(supplier::get)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> Log.e(TAG, "Error running off UiThread", throwable))
                .subscribe(consumer::accept, e -> {
                    throw e; // pass this through to ACRA
                });
    }

    public static Disposable runOffUiThread(Runnable runnable) {
        return Single.fromCallable(() -> {
            runnable.run();
            return true;
        })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnError(throwable -> Log.e(TAG, "Error running off UiThread", throwable))
                .subscribe(Functions.emptyConsumer(), e -> {
                    throw e; // pass this through to ACRA
                });
    }

    public static <T> void observeOnce(LiveData<T> liveData, LifecycleOwner lifecycleOwner, Consumer<T> consumer) {
        liveData.observe(lifecycleOwner, new Observer<T>() {
            @Override
            public void onChanged(T t) {
                consumer.accept(t);
                liveData.removeObserver(this);
            }
        });
    }

    public static ArrayList<String> toString(@Nullable List<FileV2> files) {
        if (files == null) return new ArrayList<>(0);
        ArrayList<String> list = new ArrayList<>(files.size());
        for (FileV2 file : files) {
            list.add(file.serialize());
        }
        return list;
    }

    public static List<FileV2> fileV2FromStrings(List<String> list) {
        ArrayList<FileV2> files = new ArrayList<>(list.size());
        for (String s : list) {
            files.add(FileV2.deserialize(s));
        }
        return files;
    }

    /**
     * Keep an instance of this class as an field in an AppCompatActivity for figuring out whether the on
     * screen keyboard is currently visible or not.
     */
    public static class KeyboardStateMonitor {

        private boolean visible = false;

        /**
         * @param contentView this must be the top most Container of the layout used by the AppCompatActivity
         */
        public KeyboardStateMonitor(final View contentView) {
            contentView.getViewTreeObserver().addOnGlobalLayoutListener(() -> {
                        int screenHeight = contentView.getRootView().getHeight();
                        Rect rect = new Rect();
                        contentView.getWindowVisibleDisplayFrame(rect);
                        int keypadHeight = screenHeight - rect.bottom;
                        visible = keypadHeight >= screenHeight * 0.15;
                    }
            );
        }

        public boolean isKeyboardVisible() {
            return visible;
        }
    }

    public static boolean isPortInUse(String host, int port) {
        boolean result = false;

        try {
            (new Socket(host, port)).close();
            result = true;
        } catch (IOException e) {
            // Could not connect.
            e.printStackTrace();
        }
        return result;
    }

    /**
     * Copy text to the clipboard and show a toast informing the user that something has been copied.
     * @param context the context to use
     * @param label the label used in the clipboard
     * @param text the text to copy
     */
    public static void copyToClipboard(@NonNull Context context, @Nullable String label,
                                       @NonNull String text) {
        copyToClipboard(context, label, text, R.string.copied_to_clipboard);
    }

    /**
     * Copy text to the clipboard and show a toast informing the user that the text has been copied.
     * @param context the context to use
     * @param label the label used in the clipboard
     * @param text the text to copy
     * @param message the message to show in the toast
     */
    public static void copyToClipboard(@NonNull Context context, @Nullable String label,
                                       @NonNull String text, @StringRes int message) {
        ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
        if (clipboard == null) {
            // permission denied
            return;
        }
        try {
            clipboard.setPrimaryClip(ClipData.newPlainText(label, text));
            if (Build.VERSION.SDK_INT < 33) {
                // Starting with Android 13 (SDK 33) there is a system dialog with more clipboard actions
                // shown automatically so there is no need to inform the user about the copy action.
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            // should not happen, something went wrong internally
            debugLog(TAG, "Could not copy to clipboard: " + e.getMessage());
        }

    }

    public static String toJsonStringArray(List<String> list) {
        JSONArray jsonArray = new JSONArray();
        for (String str : list) {
            jsonArray.put(str);
        }
        return jsonArray.toString();
    }

    public static List<String> parseJsonStringArray(String json) {
        try {
            JSONArray jsonArray = new JSONArray(json);
            List<String> l = new ArrayList<>(jsonArray.length());
            for (int i = 0; i < jsonArray.length(); i++) {
                l.add(jsonArray.getString(i));
            }
            return l;
        } catch (JSONException e) {
            return Collections.emptyList();
        }
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class X509TrustManagerTest {

    class TrustAllManager implements X509TrustManager {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LoggedTrustAllManager implements X509TrustManager {

        Logger logger = LogManager.getLogger(LoggedTrustAllManager.class);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            if(issuerRequestsCount < 5){
                issuerRequestsCount++;
                return null;
            }
            return certificates;
        }
    }

    class FaultyLimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            X509Certificate[] a = certificates;
            return a;
        }
    }

    class LimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;
        
        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return certificates;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return certificates;
        }
    }

    class FaultyLimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class FaultyLimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ruleid: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            return null;
        }
    }


    class FaultyLimitedManager4 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package strings;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

// ref: java_strings_rule-BadHexConversion
public class BadHexConversion {

    public String danger(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return stringBuilder.toString();
    }


    public String danger2(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        // ruleid: java_strings_rule-BadHexConversion
        for (int i = 0, resultBytesLength = resultBytes.length; i < resultBytesLength; i++) {
            byte b = resultBytes[i];
            stringBuilder.append(Integer.toHexString(b));
        }
        return stringBuilder.toString();
    }

    public String danger3(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return stringBuilder.toString();
    }

    public String danger4(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return stringBuilder.toString();
    }

    public String danger5(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return stringBuilder.toString();
    }

    public String danger6(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return stringBuilder.toString();
    }

    public String danger7(String text) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] resultBytes = md.digest(text.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        int i = 0;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return stringBuilder.toString();
    }

    public static String safeOne(String password) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-1");
        byte[] resultBytes = md.digest(password.getBytes("UTF-8"));

        StringBuilder stringBuilder = new StringBuilder();
        // ok: java_strings_rule-BadHexConversion
        for (byte b : resultBytes) {
            stringBuilder.append(String.format("%02X", b));
        }

        return stringBuilder.toString();
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.HttpClient;

class Bad {
    public static void bad1() throws IOException {
        // ruleid: java_crypto_rule-HttpGetHTTPRequest
        HttpGet httpGet = new HttpGet("http://example.com");
        HttpClients.createDefault().execute(httpGet);
    }

    public static void bad2() throws IOException {
        String url = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        HttpClients.createDefault().execute(httpGet);
    }

    public static void bad3() throws IOException {
        HttpClient client = new DefaultHttpClient();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        client.execute(request);
    }
}

class Ok {
    public static void ok1() throws IOException {
        // ok: java_crypto_rule-HttpGetHTTPRequest
        HttpGet httpGet = new HttpGet("https://example.com");
        HttpClients.createDefault().execute(httpGet);
    }

    public static void ok2() throws IOException {
        String url = "https://example.com";
        // ok: java_crypto_rule-HttpGetHTTPRequest
        HttpGet httpGet = new HttpGet(url);
        HttpClients.createDefault().execute(httpGet);
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-spring/spring-http-request.java

import java.net.URI;
import custom.Foo;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

class Bad {
    public void bad1() {
        RestTemplate restTemplate = new RestTemplate();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("http://example.com");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad4(Object obj) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad5(Object object) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "http://example.com";
        // ruleid: java_crypto_rule-SpringHTTPRequestRestTemplate
        restTemplate.put(url, object);
    }

    public void bad6(Object object) {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("http://example.com");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad7() {
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl = "http://localhost:8080/spring-rest/foos";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad8() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
        String fooResourceUrl = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad9(String headers) {
        RestTemplate template = new RestTemplate();
        Foo updatedInstance = new Foo("newName");
        updatedInstance.setId("1");
        String resourceUrl = "http://example.com";
        HttpEntity<Foo> requestUpdate = new HttpEntity<>(updatedInstance, headers);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class Safe {
    public void safe1() {
        RestTemplate restTemplate = new RestTemplate();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safe2() {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://example.com";
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        restTemplate.delete(url);
    }

    public void safe3() {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("https://example.com");
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        restTemplate.delete(url);
    }

    public void safe4(Object obj) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        restTemplate.put(URI.create("https://example.com"), obj);
    }

    public void safe5(Object object) {
        RestTemplate restTemplate = new RestTemplate();
        String url = "https://example.com";
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        restTemplate.put(url, object);
    }

    public void safe6(Object object) {
        RestTemplate restTemplate = new RestTemplate();
        URI url = new URI("https://example.com");
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        restTemplate.put(url, object);
    }

    public void safe7() {
        RestTemplate restTemplate = new RestTemplate();
        String fooResourceUrl = "https://localhost:8080/spring-rest/foos";
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        ResponseEntity<String> response = restTemplate.getForEntity(fooResourceUrl, String.class);
    }

    public void safe8() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Foo> request = new HttpEntity<>(new Foo("bar"));
        String fooResourceUrl = "https://example.com";
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        Foo foo = restTemplate.postForObject(fooResourceUrl, request, Foo.class);
    }

    public void safe9(String headers) {
        RestTemplate template = new RestTemplate();
        Foo updatedInstance = new Foo("newName");
        updatedInstance.setId("1");
        String resourceUrl = "https://example.com";
        HttpEntity<Foo> requestUpdate = new HttpEntity<>(updatedInstance, headers);
        // ok: java_crypto_rule-SpringHTTPRequestRestTemplate
        template.exchange(resourceUrl, HttpMethod.PUT, requestUpdate, Void.class);
    }
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package cookie;

public class CookieHTTPOnly {

    public void dangerJavax(javax.servlet.http.HttpServletResponse res) {
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("key", "value");
        cookie.setSecure(true);
        cookie.setMaxAge(60);
        // ruleid: java_cookie_rule-CookieHTTPOnly
        cookie.setHttpOnly(false); // danger
        res.addCookie(cookie);
    }

    // cookie.setHttpOnly(true) is missing
    public void danger2Javax(javax.servlet.http.HttpServletResponse res) {
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("key", "value");
        cookie.setSecure(true);
        cookie.setMaxAge(60);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void safeJavax(javax.servlet.http.HttpServletResponse response) {
        javax.servlet.http.Cookie myCookie = new javax.servlet.http.Cookie("key", "value");
        myCookie.setHttpOnly(true);
        myCookie.setMaxAge(60);
        // rule ok: java_cookie_rule-CookieHTTPOnly
        response.addCookie(myCookie);
    }

    protected void dangerJakarta(jakarta.servlet.http.HttpServletResponse response) {
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }

    protected void danger2Jakarta(jakarta.servlet.http.HttpServletResponse response) {
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
        myCookie.setMaxAge(60);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void safeJakarta(jakarta.servlet.http.HttpServletResponse response) {
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
        myCookie.setHttpOnly(true);
        myCookie.setMaxAge(60);
        // rule ok: java_cookie_rule-CookieHTTPOnly
        response.addCookie(myCookie);
    }
}

// License: The GitLab Enterprise Edition (EE) license (the “EE License”)
package deserialization;

import java.io.File;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.json.JsonMapper;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes;

class App {
    public static void main(String[] args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        ExecuteVulns executeVulns = new ExecuteVulns(objectMapper);

        executeVulns.enableDefaultTyping();
        executeVulns.defaultTypeResolverBuilder();
        executeVulns.defaultTypeResolverBuilder2();

        executeVulns.safeTypingWithUnsafeBaseAllowed();
        executeVulns.safeTypingWithLaissezFaireSubTypeValidator();
        executeVulns.blockUnsafeBaseType();
        executeVulns.blockUnsafeBaseType2();

    }

}

class ExecuteVulns {

    ObjectMapper objectMapper;

    ExecuteVulns(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    void enableDefaultTyping() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./car_jdbc.json";
        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // No Annotations
    void defaultTypeResolverBuilder() throws Exception {

        DefaultTypeResolverBuilder resolverBuilder = new DefaultTypeResolverBuilder(
                ObjectMapper.DefaultTyping.NON_FINAL);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        resolverBuilder.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        resolverBuilder.typeProperty("_type");

        objectMapper.setDefaultTyping(resolverBuilder);

    }

    void defaultTypeResolverBuilder2() throws Exception {

        DefaultTypeResolverBuilder rb = new DefaultTypeResolverBuilder(ObjectMapper.DefaultTyping.NON_FINAL);
        // ruleid: java_deserialization_rule-JacksonUnsafeDeserialization
        rb.init(JsonTypeInfo.Id.MINIMAL_CLASS, null);
        rb.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        rb.typeProperty("_type");

        objectMapper.setDefaultTyping(rb);

    }

    void safeTypingWithUnsafeBaseAllowed() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./mygadgetload.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void safeTypingWithLaissezFaireSubTypeValidator() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./mygadgetload_array.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // denies resolution of all subtypes of java.lang.Object
    void blockUnsafeBaseType() throws Exception {

        objectMapper.enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES);

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        objectMapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = objectMapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void blockUnsafeBaseType2() throws Exception {

        ObjectMapper mapper = JsonMapper.builder()
                .enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES)
                .build();

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        mapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = mapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        mapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }
}

class Car {

    public Object engine;
    public Object secEngine;
    public String color;
    public String model;

    public Car() {
    }

}

class CarWithCLASSAnnotations {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    public String color;
    public String model;

    public CarWithCLASSAnnotations() {
    }

}

class CarWithMinimalCLASSAnnotations {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    public String color;
    public String model;

    public CarWithMinimalCLASSAnnotations() {
    }

}

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.WRAPPER_ARRAY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ElectricEngine.class, name = "ElectricEngine"),
        @JsonSubTypes.Type(value = FuelEngine.class, name = "FuelEngine")
})
abstract class Engine {
    public int engineModel;
    public int cc;
}

class ElectricEngine extends Engine {
    public int maxEnergy;

    public ElectricEngine() {
    }
}

class FuelEngine extends Engine {
    public int fuel;

    public FuelEngine() {
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/crypto/use-of-rc4.java

import javax.crypto.Cipher;

class RC4 {
    String algo2 = "RC4";

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() throws Exception {
        String algo = "RC4";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad4() throws Exception {
        // ruleid: java_crypto_rule-UseOfRC4
        javax.crypto.Cipher.getInstance("RC4");
    }

    public void ok() {
        // ok: java_crypto_rule-UseOfRC4
        Cipher.getInstance("AES/CBC/PKCS7PADDING");
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package com.test.servlet.xpath;

import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

//import static com.test.servlet.xpath.XpathUtils.getFileFromClasspath;

class XPassInjection extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String param = "";
        if (req.getHeader("ID") != null) {
            param = req.getHeader("ID");
        }
        // URL Decode the header value since req.getHeader() doesn't. Unlike req.getParameter().
        param = java.net.URLDecoder.decode(param, "UTF-8");
        String bar = "";
        if (param != null) {
            bar = new String(org.apache.commons.codec.binary.Base64.decodeBase64(org.apache.commons.codec.binary.Base64.encodeBase64(param.getBytes())));
        }
       try {
          
            java.io.FileInputStream file = new java.io.FileInputStream(getFileFromClasspath("xpath/employee.xml", this.getClass().getClassLoader()));
            javax.xml.parsers.DocumentBuilderFactory builderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            // Prevent XXE
            builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            javax.xml.parsers.DocumentBuilder builder = builderFactory.newDocumentBuilder();
            org.w3c.dom.Document xmlDocument = builder.parse(file);
            javax.xml.xpath.XPathFactory xpf = javax.xml.xpath.XPathFactory.newInstance();
            javax.xml.xpath.XPath xp = xpf.newXPath();

            final String expression = "/Employees/Employee[@ID='" + bar + "']";
            final String expression_safe = "/Employees/Employee[@ID='1']";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            String result2 = evaluate(expression);
            // ok: java_xpathi_rule-XpathInjection
            String result3 = xp.evaluate(expression_safe, xmlDocument);
            resp.getWriter().println("Your query results 1 are: " + result1 + "<br/>");
            resp.getWriter().println("Your query results 2 are: " + result2 + "<br/>");
            resp.getWriter().println("Your query results 3 are: " + result3 + "<br/>");
            org.w3c.dom.NodeList nodeList1 = compileEvaluate(expression);
            resp.getWriter().println("Your query results 4 are: <br/>");
            for (int i = 0; i < nodeList1.getLength(); i++) {
                org.w3c.dom.Element value = (org.w3c.dom.Element) nodeList1.item(i);
                resp.getWriter().println(value.getTextContent() + "<br/>");
            }


            private final Map<String, String> xpathVariableMap = new HashMap<String, String>();
            xpathVariableMap.put("1", "1");
			xpathVariableMap.put("default", "1");
			xpathVariableMap.put("2", "2");
            String newbar = xpathVariableMap.getOrDefault(bar,"");

            AllowList = Arrays.asList("1","2");
            final String expr = "/Employees/Employee[@ID='" + bar + "']";
            if(AllowList.contains(bar)){
                // ok: java_xpathi_rule-XpathInjection
                xp.evaluate(expr);
            }
            boolean validation = AllowList.contains(bar);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            if(validation){
                // ok: java_xpathi_rule-XpathInjection
                xp.evaluate(expr);
            }
            
            for(String s:new String[]{"1", "2"}) 
                if(s.equals(bar))
                    // ok: java_xpathi_rule-XpathInjection
                    xp.evaluate(expr);
                    
            final String expr = "/Employees/Employee[@ID='" + newbar + "']";
            // ok: java_xpathi_rule-XpathInjection
            String result1 = xp.evaluate(expr);

            String newbar2 = xpathVariableMap.getOrDefault(bar, bar);
            final String expr = "/Employees/Employee[@ID='" + newbar2 + "']";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        } catch (javax.xml.xpath.XPathExpressionException | javax.xml.parsers.ParserConfigurationException |
                 org.xml.sax.SAXException e) {
            resp.getWriter().println("Error parsing XPath input: '" + bar + "'");
            throw new ServletException(e);
        }
    }

    private String evaluate(String expression) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        java.io.FileInputStream file = new java.io.FileInputStream(getFileFromClasspath("xpath/employee.xml", this.getClass().getClassLoader()));
        javax.xml.parsers.DocumentBuilderFactory builderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
        javax.xml.parsers.DocumentBuilder builder = builderFactory.newDocumentBuilder();
        org.w3c.dom.Document xmlDocument = builder.parse(file);
        javax.xml.xpath.XPathFactory xpf = javax.xml.xpath.XPathFactory.newInstance();
        // Prevent XXE
        builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        javax.xml.xpath.XPath xp = xpf.newXPath();
        // ruleid: java_xpathi_rule-XpathInjection
        String result = xp.evaluate(expression, xmlDocument);
        return result;
    }

    private org.w3c.dom.NodeList compileEvaluate(String expression) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        java.io.FileInputStream file = new java.io.FileInputStream(getFileFromClasspath("xpath/employee.xml", this.getClass().getClassLoader()));
        javax.xml.parsers.DocumentBuilderFactory builderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
        // Prevent XXE
        builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        javax.xml.parsers.DocumentBuilder builder = builderFactory.newDocumentBuilder();
        org.w3c.dom.Document xmlDocument = builder.parse(file);
        javax.xml.xpath.XPathFactory xpf = javax.xml.xpath.XPathFactory.newInstance();
        javax.xml.xpath.XPath xp = xpf.newXPath();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static File getFileFromClasspath(String fileName, ClassLoader classLoader) {
        URL url = classLoader.getResource(fileName);
        try {
            return new File(url.toURI().getPath());
        } catch (URISyntaxException e) {
            System.out.println("The file form the classpath cannot be loaded.");
        }
        return null;
    }
    
    private void safe_1(javax.xml.xpath.XPath xp, String bar) throws XPathExpressionException, Exception {
        SimpleXPathVariableResolver resolver = new SimpleXPathVariableResolver();
        // ok: java_xpathi_rule-XpathInjection
        resolver.setVariable(new QName("author"), bar);
        xp.setXPathVariableResolver(resolver);
        final String expression_res = "/Employees/Employee[@ID=$id]";
        String result = xp.evaluate(expression_res);
    }

    private void unsafe_1(javax.xml.xpath.XPath xp, String bar) throws XPathExpressionException, Exception {
        SimpleXPathVariableResolver resolver = new SimpleXPathVariableResolver();
        resolver.setVariable(new QName("author"), bar);
        xp.setXPathVariableResolver(resolver);
        final String expression_res = "/Employees/Employee[@ID='" + bar + "']";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class SimpleXPathVariableResolver implements XPathVariableResolver {
    // Use a map or lookup table to store variables for resolution
    private HashMap<QName, String> variables = new HashMap<>();
    // Allow caller to set variables
    public void setVariable(QName name, String value) {
        variables.put(name, value);
    }
    // Implement the resolveVariable to return the value
    @Override
    public Object resolveVariable(QName name) {
        return variables.getOrDefault(name, "");
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/crypto/RsaNoPadding.java
// hash: a7694d0

package crypto;

import javax.crypto.Cipher;

/**
 * Code sample taken from : http://cwe.mitre.org/data/definitions/780.html
 */
public class RsaNoPadding {

    public void rsaCipherOk() throws Exception {
        // ok: java_crypto_rule-RsaNoPadding
        Cipher.getInstance("RSA/ECB/OAEPWithMD5AndMGF1Padding");
        // ok: java_crypto_rule-RsaNoPadding
        Cipher.getInstance("RSA");
        // ok: java_crypto_rule-RsaNoPadding
        Cipher.getInstance("RSA/ECB/OAEPWithMD5AndMGF1Padding", "BC");
        // ok: java_crypto_rule-RsaNoPadding
        Cipher.getInstance("AES/GCM/NoPadding");
    }

    public void rsaCipherWeak() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void dataflowCipherWeak() throws Exception {
        String cipher1 = null;
        // ok: java_crypto_rule-RsaNoPadding
        Cipher.getInstance(cipher1);

        String cipher2 = "RSA/NONE/NoPadding";
        // ruleid: java_crypto_rule-RsaNoPadding
        Cipher.getInstance(cipher2);

        String cipher3 = "RSA/ECB/NoPadding";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: MIT (c) GitLab Inc.

package com.test.servlet.cors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

// ref: java_cors_rule-PermissiveCORSInjection
// sample get req: http://localhost:8080/ServletSample/PermissiveCORSInjection/*?tainted=*f&URL=*&URL=*&url=*
public class PermissiveCORSInjection extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("attributeName",request.getParameter("tainted"));
        request.getSession().setAttribute("attributeName",request.getParameter("tainted"));
        request.getServletContext().setAttribute("attributeName",request.getParameter("tainted"));

        String paramValue = request.getParameter("tainted");
        String header = request.getHeader("tainted");
        String requestAttribute = (String) request.getAttribute("attributeName");
        String sessionAttribute = (String) request.getSession().getAttribute("attributeName");
        String contextAttribute = (String) request.getServletContext().getAttribute("attributeName");
        String pathInfo = request.getPathInfo();
        String modifiedPath = pathInfo.replaceFirst("/","");
        String queryString = request.getQueryString();

        String[] parameterValues = request.getParameterValues("URL");
        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, String[]> parameterMap = request.getParameterMap();

        String valueFromPV = parameterValues[0];
        String valueFromPN = parameterNames.nextElement();
        String valueFromPM = parameterMap.get("URL")[0];

        String[] keyValuePairs = queryString.split("=");
        String lastPair = keyValuePairs[keyValuePairs.length - 1];

        if (paramValue != null) {
          // ok:java_cors_rule-PermissiveCORSInjection
          response.setHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPV);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("Some-Example-Header", valueFromPN);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPM);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.addHeader("access-control-allow-origin", contextAttribute);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          String headerName = "ACCESS-CONTROL-ALLOW-ORIGIN";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          return;
        }

        // ok:java_cors_rule-PermissiveCORSInjection
        response.setHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", getFromList("key"));

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "*");
    }

    public String getFromList(String key){
        HashMap<String, String> corsList = new HashMap<>();
        corsList.put("key", "https://example.com");

        return corsList.get(key);
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package cookie;

import org.apache.commons.text.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class HttpResponseSplitting extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        Cookie c = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String paramNames = req.getParameterNames().nextElement();
        Cookie cParamNames = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamNames.setHttpOnly(true);
        cParamNames.setSecure(true);
        resp.addCookie(cParamNames);

        String paramValues = req.getParameterValues("input")[0];
        Cookie cParamValues = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamValues.setHttpOnly(true);
        cParamValues.setSecure(true);
        resp.addCookie(cParamValues);

        String paramMap = req.getParameterMap().get("input")[0];
        Cookie cParamMap = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamMap.setHttpOnly(true);
        cParamMap.setSecure(true);
        resp.addCookie(cParamMap);

        String header = req.getHeader("input");
        Cookie cHeader = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\r", "");
        //ruleid: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = StringEscapeUtils.escapeJava(data);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = StringEscapeUtils.escapeJava(header);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = StringEscapeUtils.escapeJava(contextPath);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // BAD
        String tainted = req.getParameter("input");
        resp.setHeader("test", tainted);

        // OK: False negative but reported by spotbugs
        String data = req.getParameter("input");
        String normalized = data.replaceAll("\n", "\n");
        resp.setHeader("test", normalized);

        // OK: False negative but reported by spotbugs
        String normalized2 = data.replaceAll("\n", req.getParameter("test"));
        resp.setHeader("test2", normalized2);

        // OK
        String normalized3 = org.apache.commons.text.StringEscapeUtils.unescapeJava(tainted);
        resp.setHeader("test3", normalized3);

        // BAD
        String normalized4 = getString(tainted);
        resp.setHeader("test4", normalized4);

        // BAD
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
        wrapper.addHeader("test", tainted);
        wrapper.setHeader("test2", tainted);

    }

    private String getString(String s) {
        return s;
    }
}

// License: MIT (c) GitLab Inc.
package xml;

import java.io.*;
import java.beans.XMLDecoder;
import java.util.ArrayList;
import java.util.List;

class TestXmlDecoder {

    String safeVariable = "";

    // This will just create a file in your /tmp/ folder named Hacked1.txt
    void decodeObjectUnsafe1() throws IOException {
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce.xml");
        XMLDecoder decoder = new XMLDecoder(inputStream);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked1.txt file");
    }

    // This will just create a file in your /tmp/ folder named Hacked2.txt
    void decodeObjectUnsafe2() throws IOException {
        ClassLoader customClassLoader = null;
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked2.txt file");
    }

    void decodeObjectUnsafe3() throws IOException {
        System.out.println("Running Unsafe3(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ruleid: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe4() throws IOException {
        System.out.println("Running Unsafe4(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {

                    return super.loadClass(name, resolve);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe5() throws IOException {
        System.out.println("Running Unsafe5(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            return super.loadClass(name, resolve);
                        }
                    });
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe6() throws IOException {
        System.out.println("Running Unsafe6(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!name.equals(
                                    TestXmlDecoder.class.getName()) &&
                                    !name.equals(XMLDecoder.class.getName())) {

                                return super.loadClass(name, resolve);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe1() throws IOException {
        System.out.println("Running Safe1(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe2() throws IOException {
        System.out
                .println("Running Safe2(): (This should run normally as xml file does not contain malicious payload.)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-safe.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                new ClassLoader() {
                    @Override
                    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                        if (!name.equals(
                                TestXmlDecoder.class.getName()) &&
                                !name.equals(XMLDecoder.class.getName())) {
                            throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                        }
                        return super.loadClass(name, resolve);
                    }
                });
        // ok: java_xml_rule-XmlDecoder
        Object o = decoder.readObject();
        decoder.close();
    }

    void decodeObjectSafe3() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe3(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!allowedClasses.contains(name)) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe4() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe4(): (Exceptions will be thrown)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!allowedClasses.contains(name)) {
                                throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    public void setSafeVariable(String str) {
        safeVariable = str;
        System.out.println("SafeVariable set: " + safeVariable);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-OgnlInjection
        reflectionProvider.setProperties(propsInput, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        Files.createTempDirectory(p,input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-1", new ExampleProvider());
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
        // ruleid: java_inject_rule-SqlInjection
        jdbcTemplate.update("test" + clientDetails + "test", clientDetails);
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid:java_inject_rule-DangerousGroovyShell
        shell.parse(new URI(uri));

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SpelView.java
// hash: a7694d0
package script;

import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.PropertyPlaceholderHelper;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class SpringSpelExpressionParser implements View {
    private String template;

    private final SpelExpressionParser parser = new SpelExpressionParser();
    private final ExpressionParser parserVariant = new SpelExpressionParser();

    private final StandardEvaluationContext context = new StandardEvaluationContext();

    private PropertyPlaceholderHelper.PlaceholderResolver resolver;

    public void SpelView(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                String tainted = name + "blah";
                // ruleid: java_script_rule-SpringSpelExpressionParser
                Expression expression3 = this.parser.parseExpression(tainted);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public void SpelViewParseRaw(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                String tainted = name + "blah";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public String getContentType() {
        return "text/html";
    }

    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> map = new HashMap<>(model);
        String path = ServletUriComponentsBuilder.fromContextPath(request).build()
                .getPath();
        map.put("path", path==null ? "" : path);
        context.setRootObject(map);
        PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper("${", "}");
        String result = helper.replacePlaceholders(template, resolver);
        response.setContentType(getContentType());
        response.getWriter().append(result);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs

package crypto;

import java.security.*;
import java.security.spec.RSAKeyGenParameterSpec;

/**
 * The key size might need to be adjusted in the future.
 * http://en.wikipedia.org/wiki/Key_size#Asymmetric_algorithm_key_lengths
 */
public class InsufficientKeySizeRsa {

    public KeyPair weakKeySize1() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySize2() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }


    public KeyPair weakKeySize3ParameterSpec() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        KeyPair key = keyGen.generateKeyPair();
        return key;
    }

    public KeyPair weakKeySize4ParameterSpec() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        KeyPair key = keyGen.generateKeyPair();
        return key;
    }

    public KeyPair weakKeySize5Recommended() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair okKeySizeParameterSpec() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(new RSAKeyGenParameterSpec(2048,RSAKeyGenParameterSpec.F4)); //Different signature

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySizeWithProviderString() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySizeWithProviderObject1() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", new ExampleProvider());
        // ruleid: java_crypto_rule-InsufficientKeySizeRsa
        keyGen.initialize(1024); //BAD with lower priority

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySizeWithProviderObject2() throws NoSuchAlgorithmException {
        Provider p = new ExampleProvider("info");
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", p);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair strongKeySizeWithProviderString() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
        keyGen.initialize(2048); // OK: n >= 2048

        return keyGen.generateKeyPair();
    }


    private class ExampleProvider extends Provider {
        ExampleProvider() {
            this("example");
        }

        ExampleProvider(String info) {
            super("example", 0.0, info);
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
        // ruleid:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
        // ruleid: java_inject_rule-SqlInjection
        stmt.execute(str);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SmtpClient.java
// hash: a7694d0
package smtp;

import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Properties;
import org.apache.commons.text.StringEscapeUtils;

public class SmtpClient {
    public static void main(String[] args) throws Exception {
        //Example of payload that would prove the injection
        sendEmail("Testing Subject\nX-Test: Override", "test\nDEF:2", "SVW:2\nXYZ", "ou\nlalala:22\n\rlili:22", "123;\n456=889","../../../etc/passwd");
    }

    public static void sendEmail(String input1, String input2, String input3, String input4, String input5,String input6) throws Exception {

        final String username = "email@gmail.com"; //Replace by test account
        final String password = ""; //Replace by app password

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("source@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,new InternetAddress[] {new InternetAddress("destination@gmail.com")});
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_smtp_rule-SmtpClient
        message.setDisposition(input5); //Injectable API
        message.setText("This is just a test 2.");
        Transport.send(message);
        new FileDataSource("/path/traversal/here/"+input6);

        System.out.println("Done");
    }

    public static void sendEmailTainted(Session session,HttpServletRequest req) throws MessagingException {
        Message message = new MimeMessage(session);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static void sendEmailOK(Session session,String input) throws MessagingException {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
        message.addHeader("abc", URLEncoder.encode(input));
    }

    public static void fixedVersions(Session session, String input) throws Exception {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
       message.setSubject(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.addHeader("lol", StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDescription(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDisposition(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setText(StringEscapeUtils.escapeJava(input));
       // ok: java_smtp_rule-SmtpClient
       message.setText("" + 42);
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/r?q=java.jjwt.security.jwt-none-alg.jjwt-none-alg
package crypto;

class JWTNoneAlgorithm {

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        System.out.println("Lib1 bad1() JWT: " + token);
    }

    public void ok1() {
        javax.crypto.SecretKey key = io.jsonwebtoken.Jwts.SIG.HS256.key().build();
        // ok: java_crypto_rule_JwtNoneAlgorithm
        String token = io.jsonwebtoken.Jwts.builder()
                .subject("Bob")
                .signWith(key)
                .compact();

        System.out.println("Lib1 ok() JWT: " + token);
    }

    public void bad2() throws Exception {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            System.out.println("Lib2 bad2() JWT: " + token);
        } catch (Exception e) {
            System.out.println("Exception : " + e.getLocalizedMessage());
        }
    }

    public void bad3() {
        try {
            // PlainHeaders are set with None Algo.
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\", \"exp\":1300819111}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad3() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bad4() {
        try {
            // PlainHeaders are set with None Algo.
            // ruleid: java_crypto_rule_JwtNoneAlgorithm
            com.nimbusds.jose.PlainHeader header = new com.nimbusds.jose.PlainHeader.Builder().contentType("text/plain")
                    .customParam("exp", "1300819111")
                    .build();

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\"}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad4() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/webview/webview_debugging.java
// hash: e29e85c3

package com.myapp;

import com.facebook.react.ReactActivity;
import android.webkit.WebView;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "myapp";
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        //added this line with necessary imports at the top.
        // ruleid:rules_lgpl_java_webview_rule-webview-debugging
        WebView.setWebContentsDebuggingEnabled(true);
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        URL resourceInput = getClass().getResource(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

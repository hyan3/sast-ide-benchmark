@RunWith(ParameterizedRobolectricTestRunner.class)
public class GalleryListParserTest {

  private static final String E_MINIMAL = "GalleryListParserTestEMinimal.html";
  private static final String E_MINIMAL_PLUS = "GalleryListParserTestEMinimalPlus.html";
  private static final String E_COMPAT = "GalleryListParserTestECompat.html";
  private static final String E_EXTENDED = "GalleryListParserTestEExtended.html";
  private static final String E_THUMBNAIL = "GalleryListParserTestEThumbnail.html";

  private static final String EX_MINIMAL = "GalleryListParserTestExMinimal.html";
  private static final String EX_MINIMAL_PLUS = "GalleryListParserTestExMinimalPlus.html";
  private static final String EX_COMPAT = "GalleryListParserTestExCompat.html";
  private static final String EX_EXTENDED = "GalleryListParserTestExExtended.html";
  private static final String EX_THUMBNAIL = "GalleryListParserTestExThumbnail.html";

  @ParameterizedRobolectricTestRunner.Parameters(name = "{index}-{0}")
  public static List data() {
    return Arrays.asList(new Object[][] {
        { E_MINIMAL },
        { E_MINIMAL_PLUS },
        { E_COMPAT },
        { E_EXTENDED },
        { E_THUMBNAIL },
        { EX_MINIMAL },
        { EX_MINIMAL_PLUS },
        { EX_COMPAT },
        { EX_EXTENDED },
        { EX_THUMBNAIL },
    });
  }

  private String file;

  public GalleryListParserTest(String file) {
    this.file = file;
  }

  @Test
  public void testParse() throws Exception {
    InputStream resource = GalleryPageApiParserTest.class.getResourceAsStream(file);
    BufferedSource source = Okio.buffer(Okio.source(resource));
    String body = source.readUtf8();

    GalleryListParser.Result result = GalleryListParser.parse(body);

    assertEquals(25, result.galleryInfoList.size());

    result.galleryInfoList.forEach(gi -> {
      assertNotEquals(0, gi.gid);
      assertNotEquals(0, gi.token);
      assertNotNull(gi.title);

      //assertNotNull(gi.simpleTags);

      assertNotEquals(0, gi.category);
      assertNotEquals(EhUtils.UNKNOWN, gi.category);
      assertNotEquals(0, gi.thumbWidth);
      assertNotEquals(0, gi.thumbHeight);
      assertNotNull(gi.thumb);
      assertNotNull(gi.posted);
      assertNotEquals(0.0, gi.rating);
      if (E_MINIMAL.equals(file) ||
          E_MINIMAL_PLUS.equals(file) ||
          E_COMPAT.equals(file) ||
          E_EXTENDED.equals(file) ||
          EX_MINIMAL.equals(file) ||
          EX_MINIMAL_PLUS.equals(file) ||
          EX_COMPAT.equals(file) ||
          EX_EXTENDED.equals(file)) {
        assertNotNull(gi.uploader);
      } else {
        assertNull(gi.uploader);
      }
      assertNotEquals(0, gi.pages);
    });
  }
}

public class AdvancedFragment extends BasePreferenceFragment {

    private static final String KEY_DUMP_LOGCAT = "dump_logcat";
    private static final String KEY_CLEAR_MEMORY_CACHE = "clear_memory_cache";
    private static final String KEY_APP_LANGUAGE = "app_language";
    private static final String KEY_IMPORT_DATA = "import_data";
    private static final String KEY_EXPORT_DATA = "export_data";
    private static final String KEY_OPEN_BY_DEFAULT = "open_by_default";

    ActivityResultLauncher<String> exportLauncher = registerForActivityResult(
            new ActivityResultContracts.CreateDocument(),
            uri -> {
                if (uri != null) {
                    try {
                        // grantUriPermission might throw RemoteException on MIUI
                        requireActivity().grantUriPermission(BuildConfig.APPLICATION_ID, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    } catch (Exception e) {
                        ExceptionUtils.throwIfFatal(e);
                        e.printStackTrace();
                    }
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(requireActivity())
                                .setCancelable(false)
                                .setView(R.layout.preference_dialog_task)
                                .show();
                        IoThreadPoolExecutor.getInstance().execute(() -> {
                            boolean success = EhDB.exportDB(requireActivity(), uri);
                            Activity activity = getActivity();
                            if (activity != null) {
                                activity.runOnUiThread(() -> {
                                    if (alertDialog.isShowing()) {
                                        alertDialog.dismiss();
                                    }
                                    showTip(
                                            (success)
                                                    ? GetText.getString(R.string.settings_advanced_export_data_to, uri.toString())
                                                    : GetText.getString(R.string.settings_advanced_export_data_failed),
                                            BaseScene.LENGTH_SHORT);
                                });
                            }
                        });
                    } catch (Exception e) {
                        showTip(R.string.settings_advanced_export_data_failed, BaseScene.LENGTH_SHORT);
                    }
                }
            });
    ActivityResultLauncher<String> dumpLogcatLauncher = registerForActivityResult(
            new ActivityResultContracts.CreateDocument(),
            uri -> {
                if (uri != null) {
                    try {
                        // grantUriPermission might throw RemoteException on MIUI
                        requireActivity().grantUriPermission(BuildConfig.APPLICATION_ID, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    } catch (Exception e) {
                        ExceptionUtils.throwIfFatal(e);
                        e.printStackTrace();
                    }
                    try {
                        File zipFile = new File(AppConfig.getExternalTempDir(), "logs.zip");
                        if (zipFile.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            zipFile.delete();
                        }

                        ArrayList<File> files = new ArrayList<>();
                        files.addAll(Arrays.asList(AppConfig.getExternalParseErrorDir().listFiles()));
                        files.addAll(Arrays.asList(AppConfig.getExternalCrashDir().listFiles()));

                        boolean finished = false;

                        BufferedInputStream origin = null;
                        ZipOutputStream out = null;
                        try {
                            FileOutputStream dest = new FileOutputStream(zipFile);
                            out = new ZipOutputStream(new BufferedOutputStream(dest));
                            byte[] bytes = new byte[1024 * 64];

                            for (File file : files) {
                                if (!file.isFile()) {
                                    continue;
                                }
                                try {
                                    FileInputStream fi = new FileInputStream(file);
                                    origin = new BufferedInputStream(fi, bytes.length);

                                    ZipEntry entry = new ZipEntry(file.getName());
                                    out.putNextEntry(entry);
                                    int count;
                                    while ((count = origin.read(bytes, 0, bytes.length)) != -1) {
                                        out.write(bytes, 0, count);
                                    }
                                    origin.close();
                                    origin = null;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            ZipEntry entry = new ZipEntry("logcat-" + ReadableTime.getFilenamableTime(System.currentTimeMillis()) + ".txt");
                            out.putNextEntry(entry);
                            LogCat.save(out);
                            out.closeEntry();
                            out.close();
                            IOUtils.copy(new FileInputStream(zipFile), requireActivity().getContentResolver().openOutputStream(uri));
                            finished = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (origin != null) {
                                origin.close();
                            }
                            if (out != null) {
                                out.close();
                            }
                        }
                        if (!finished) {
                            finished = LogCat.save(requireActivity().getContentResolver().openOutputStream(uri));
                        }
                        showTip(
                                finished ? getString(R.string.settings_advanced_dump_logcat_to, uri.toString()) :
                                        getString(R.string.settings_advanced_dump_logcat_failed), BaseScene.LENGTH_SHORT);
                    } catch (Exception e) {
                        showTip(getString(R.string.settings_advanced_dump_logcat_failed), BaseScene.LENGTH_SHORT);
                    }
                }
            });
    ActivityResultLauncher<String[]> importDataLauncher = registerForActivityResult(
            new ActivityResultContracts.OpenDocument(),
            uri -> {
                if (uri != null) {
                    try {
                        // grantUriPermission might throw RemoteException on MIUI
                        requireActivity().grantUriPermission(BuildConfig.APPLICATION_ID, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    } catch (Exception e) {
                        ExceptionUtils.throwIfFatal(e);
                        e.printStackTrace();
                    }
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(requireActivity())
                                .setCancelable(false)
                                .setView(R.layout.preference_dialog_task)
                                .show();
                        IoThreadPoolExecutor.getInstance().execute(() -> {
                            final String error = EhDB.importDB(requireActivity(), uri);
                            Activity activity = getActivity();
                            if (activity != null) {
                                activity.runOnUiThread(() -> {
                                    if (alertDialog.isShowing()) {
                                        alertDialog.dismiss();
                                    }
                                    if (null == error) {
                                        showTip(getString(R.string.settings_advanced_import_data_successfully), BaseScene.LENGTH_SHORT);
                                    } else {
                                        showTip(error, BaseScene.LENGTH_SHORT);
                                    }
                                });
                            }

                        });
                    } catch (Exception e) {
                        showTip(e.getLocalizedMessage(), BaseScene.LENGTH_SHORT);
                    }
                }
            });

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.advanced_settings);

        Preference dumpLogcat = findPreference(KEY_DUMP_LOGCAT);
        Preference clearMemoryCache = findPreference(KEY_CLEAR_MEMORY_CACHE);
        Preference appLanguage = findPreference(KEY_APP_LANGUAGE);
        Preference importData = findPreference(KEY_IMPORT_DATA);
        Preference exportData = findPreference(KEY_EXPORT_DATA);
        Preference openByDefault = findPreference(KEY_OPEN_BY_DEFAULT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            openByDefault.setVisible(false);
        } else {
            openByDefault.setOnPreferenceClickListener(this);
        }

        dumpLogcat.setOnPreferenceClickListener(this);
        clearMemoryCache.setOnPreferenceClickListener(this);
        importData.setOnPreferenceClickListener(this);
        exportData.setOnPreferenceClickListener(this);

        appLanguage.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();
        if (KEY_DUMP_LOGCAT.equals(key)) {
            try {
                dumpLogcatLauncher.launch("log-" + ReadableTime.getFilenamableTime(System.currentTimeMillis()) + ".zip");
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                showTip(R.string.error_cant_find_activity, BaseScene.LENGTH_SHORT);
            }
            return true;
        } else if (KEY_CLEAR_MEMORY_CACHE.equals(key)) {
            ((EhApplication) requireContext().getApplicationContext()).clearMemoryCache();
            Runtime.getRuntime().gc();
            showTip(R.string.settings_advanced_clear_memory_cache_done, BaseScene.LENGTH_SHORT);
        } else if (KEY_IMPORT_DATA.equals(key)) {
            try {
                importDataLauncher.launch(new String[]{"*/*"});
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                showTip(R.string.error_cant_find_activity, BaseScene.LENGTH_SHORT);
            }
            return true;
        } else if (KEY_EXPORT_DATA.equals(key)) {
            try {
                exportLauncher.launch(ReadableTime.getFilenamableTime(System.currentTimeMillis()) + ".db");
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                showTip(R.string.error_cant_find_activity, BaseScene.LENGTH_SHORT);
            }
            return true;
        } else if (KEY_OPEN_BY_DEFAULT.equals(key)) {
            try {
                Intent intent = new Intent(android.provider.Settings.ACTION_APP_OPEN_BY_DEFAULT_SETTINGS,
                        Uri.parse("package:" + requireContext().getPackageName()));
                startActivity(intent);
            } catch (Throwable t) {
                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + requireContext().getPackageName()));
                startActivity(intent);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        if (KEY_APP_LANGUAGE.equals(key)) {
            if ("system".equals(newValue)) {
                LocaleDelegate.setDefaultLocale(LocaleDelegate.getSystemLocale());
            } else {
                LocaleDelegate.setDefaultLocale(Locale.forLanguageTag((String) newValue));
            }
            requireActivity().recreate();
            return true;
        }
        return false;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.settings_advanced;
    }
}

public class DownloaderFactory extends org.fdroid.download.DownloaderFactory {

    private static final String TAG = "DownloaderFactory";
    // TODO move to application object or inject where needed
    public static final DownloaderFactory INSTANCE = new DownloaderFactory();
    public static final HttpManager HTTP_MANAGER =
            new HttpManager(Utils.getUserAgent(), FDroidApp.queryString, NetCipher.getProxy(), new DnsWithCache());

    @NonNull
    @Override
    public Downloader create(Repository repo, @NonNull Uri uri, @NonNull IndexFile indexFile,
                             @NonNull File destFile) throws IOException {
        List<Mirror> mirrors = repo.getMirrors();
        return create(repo, mirrors, uri, indexFile, destFile, null);
    }

    @NonNull
    @Override
    @SuppressLint("MissingPermission") // we'd need to ask for Bluetooth permission, but code unmaintained
    protected Downloader create(@NonNull Repository repo, @NonNull List<Mirror> mirrors, @NonNull Uri uri,
                                @NonNull IndexFile indexFile, @NonNull File destFile,
                                @Nullable Mirror tryFirst) throws IOException {
        Downloader downloader;

        String scheme = uri.getScheme();
        if (BluetoothDownloader.SCHEME.equals(scheme)) {
            downloader = new BluetoothDownloader(uri, indexFile, destFile);
        } else if (ContentResolver.SCHEME_CONTENT.equals(scheme)) {
            downloader = new TreeUriDownloader(uri, indexFile, destFile);
        } else if (ContentResolver.SCHEME_FILE.equals(scheme)) {
            downloader = new LocalFileDownloader(uri, indexFile, destFile);
        } else {
            String repoAddress = Utils.getRepoAddress(repo);
            String path = uri.toString().replace(repoAddress, "");
            Utils.debugLog(TAG, "Using suffix " + path + " with mirrors " + mirrors);
            Proxy proxy = NetCipher.getProxy();
            DownloadRequest request = new DownloadRequest(indexFile, mirrors, proxy,
                    repo.getUsername(), repo.getPassword(), tryFirst);
            Preferences prefs = Preferences.get();
            boolean oldIndex = prefs.isForceOldIndexEnabled();
            boolean v1OrUnknown = repo.getFormatVersion() == null ||
                    repo.getFormatVersion() == IndexFormatVersion.ONE;
            if (oldIndex || v1OrUnknown) {
                //noinspection deprecation
                downloader = new HttpDownloader(HTTP_MANAGER, request, destFile);
            } else {
                DownloadRequest r;
                if (request.getIndexFile().getIpfsCidV1() == null || !prefs.isIpfsEnabled()) {
                    r = request;
                } else {
                    // add IPFS gateways to mirrors, because have have a CIDv1 and IPFS is enabled in preferences
                    List<Mirror> m = new ArrayList<>(mirrors);
                    m.addAll(loadIpfsGateways(prefs));
                    r = new DownloadRequest(request.getIndexFile(), m, proxy, repo.getUsername(),
                            repo.getPassword(), tryFirst);
                }
                downloader = new HttpDownloaderV2(HTTP_MANAGER, r, destFile);
            }
        }
        return downloader;
    }

    private static List<Mirror> loadIpfsGateways(Preferences prefs) {
        List<Mirror> mirrorList = new ArrayList<>();
        for (String gatewayUrl : prefs.getActiveIpfsGateways()) {
            mirrorList.add(new Mirror(gatewayUrl, null, true));
        }
        return mirrorList;
    }

}

private class SpiderWorker implements Runnable {

        private final long mGid;

        public SpiderWorker() {
            mGid = mGalleryInfo.gid;
        }

        private String getPageUrl(long gid, int index, String pToken,
                                  String oldPageUrl, String skipHathKey) {
            String pageUrl;
            if (oldPageUrl != null) {
                pageUrl = oldPageUrl;
            } else {
                pageUrl = EhUrl.getPageUrl(gid, index, pToken);
            }
            // Add skipHathKey
            if (skipHathKey != null) {
                if (pageUrl.contains("?")) {
                    pageUrl += "&nl=" + skipHathKey;
                } else {
                    pageUrl += "?nl=" + skipHathKey;
                }
            }
            return pageUrl;
        }

        private GalleryPageParser.Result fetchPageResultFromHtml(int index, String pageUrl) throws Throwable {
            GalleryPageParser.Result result = EhEngine.getGalleryPage(null, mHttpClient, pageUrl, mGalleryInfo.gid, mGalleryInfo.token);
            if (StringUtils.endsWith(result.imageUrl, URL_509_SUFFIX_ARRAY)) {
                // Get 509
                // Notify listeners
                notifyGet509(index);
                throw new Image509Exception();
            }

            return result;
        }

        private GalleryPageApiParser.Result fetchPageResultFromApi(long gid, int index, String pToken, String showKey, String previousPToken) throws Throwable {
            GalleryPageApiParser.Result result = EhEngine.getGalleryPageApi(null, mHttpClient, gid, index, pToken, showKey, previousPToken);
            if (StringUtils.endsWith(result.imageUrl, URL_509_SUFFIX_ARRAY)) {
                // Get 509
                // Notify listeners
                notifyGet509(index);
                throw new Image509Exception();
            }

            return result;
        }

        // false for stop
        private boolean downloadImage(long gid, int index, String pToken, String previousPToken, boolean force) {
            String skipHathKey = null;
            List<String> skipHathKeys = new ArrayList<>(5);
            String originImageUrl = null;
            String pageUrl = null;
            String error = null;
            boolean forceHtml = false;
            boolean interrupt = false;
            boolean leakSkipHathKey = false;

            for (int i = 0; i < 5; i++) {
                String imageUrl = null;
                String localShowKey;

                // Check show key
                synchronized (showKeyLock) {
                    localShowKey = showKey.get();
                    if (localShowKey == null || forceHtml) {
                        if (leakSkipHathKey) {
                            break;
                        }

                        // Try to get show key
                        pageUrl = getPageUrl(gid, index, pToken, pageUrl, skipHathKey);
                        try {
                            GalleryPageParser.Result result = fetchPageResultFromHtml(index, pageUrl);
                            imageUrl = result.imageUrl;
                            skipHathKey = result.skipHathKey;
                            originImageUrl = result.originImageUrl;
                            localShowKey = result.showKey;

                            if (!TextUtils.isEmpty(skipHathKey)) {
                                if (skipHathKeys.contains(skipHathKey)) {
                                    // Duplicate skip hath key
                                    leakSkipHathKey = true;
                                } else {
                                    skipHathKeys.add(skipHathKey);
                                }
                            } else {
                                leakSkipHathKey = true;
                            }

                            showKey.lazySet(result.showKey);
                        } catch (Image509Exception e) {
                            error = GetText.getString(R.string.error_509);
                            break;
                        } catch (Throwable e) {
                            ExceptionUtils.throwIfFatal(e);
                            error = ExceptionUtils.getReadableString(e);
                            break;
                        }

                        // Check interrupted
                        if (Thread.currentThread().isInterrupted()) {
                            error = "Interrupted";
                            interrupt = true;
                            break;
                        }
                    }
                }

                if (imageUrl == null) {
                    if (localShowKey == null) {
                        error = "ShowKey error";
                        break;
                    }

                    try {
                        GalleryPageApiParser.Result result = fetchPageResultFromApi(gid, index, pToken, localShowKey, previousPToken);
                        imageUrl = result.imageUrl;
                        skipHathKey = result.skipHathKey;
                        originImageUrl = result.originImageUrl;
                    } catch (Image509Exception e) {
                        error = GetText.getString(R.string.error_509);
                        break;
                    } catch (Throwable e) {
                        if (e instanceof ParseException && "Key mismatch".equals(e.getMessage())) {
                            // Show key is wrong, enter a new loop to get the new show key
                            showKey.compareAndSet(localShowKey, null);
                            continue;
                        } else {
                            ExceptionUtils.throwIfFatal(e);
                            error = ExceptionUtils.getReadableString(e);
                            break;
                        }
                    }

                    // Check interrupted
                    if (Thread.currentThread().isInterrupted()) {
                        error = "Interrupted";
                        interrupt = true;
                        break;
                    }
                }

                String targetImageUrl;
                String referer;
                if (Settings.getDownloadOriginImage() && !TextUtils.isEmpty(originImageUrl)) {
                    targetImageUrl = originImageUrl;
                    referer = EhUrl.getPageUrl(gid, index, pToken);
                } else {
                    targetImageUrl = imageUrl;
                    referer = null;
                }
                if (targetImageUrl == null) {
                    error = "TargetImageUrl error";
                    break;
                }
                if (DEBUG_LOG) {
                    Log.d(TAG, targetImageUrl);
                }

                // Download image
                InputStream is = null;
                try {
                    if (DEBUG_LOG) {
                        Log.d(TAG, "Start download image " + index);
                    }

                    // disable Call Timeout for image-downloading requests
                    Call call = mHttpClient.newBuilder()
                            .callTimeout(0, TimeUnit.SECONDS).build()
                            .newCall(new EhRequestBuilder(targetImageUrl, referer).build());
                    Response response = call.execute();
                    ResponseBody responseBody = response.body();

                    if (response.code() >= 400) {
                        // Maybe 404
                        response.close();
                        error = "Bad code: " + response.code();
                        forceHtml = true;
                        continue;
                    }

                    if (responseBody == null) {
                        error = "Empty response body";
                        forceHtml = true;
                        continue;
                    }

                    // Get extension
                    String extension = null;
                    MediaType mediaType = responseBody.contentType();
                    if (mediaType != null) {
                        extension = MimeTypeMap.getSingleton().getExtensionFromMimeType(mediaType.toString());
                    }
                    // Ensure extension
                    if (!Utilities.contain(GalleryProvider2.SUPPORT_IMAGE_EXTENSIONS, extension)) {
                        extension = GalleryProvider2.SUPPORT_IMAGE_EXTENSIONS[0];
                    }

                    OutputStreamPipe osPipe = null;
                    try {
                        // Get out put pipe
                        osPipe = mSpiderDen.openOutputStreamPipe(index, extension);
                        if (osPipe == null) {
                            // Can't get pipe
                            error = GetText.getString(R.string.error_write_failed);
                            response.close();
                            break;
                        }

                        long contentLength = responseBody.contentLength();
                        is = responseBody.byteStream();
                        osPipe.obtain();
                        OutputStream os = osPipe.open();

                        final byte[] data = new byte[1024 * 4];
                        long receivedSize = 0;

                        while (!Thread.currentThread().isInterrupted()) {
                            int bytesRead = is.read(data);
                            if (bytesRead == -1) {
                                response.close();
                                break;
                            }
                            os.write(data, 0, bytesRead);
                            receivedSize += bytesRead;
                            // Update page percent
                            if (contentLength > 0) {
                                mPagePercentMap.put(index, (float) receivedSize / contentLength);
                            }
                            // Notify listener
                            notifyPageDownload(index, contentLength, receivedSize, bytesRead);
                        }
                        os.flush();

                        // check download size
                        if (contentLength >= 0) {
                            if (receivedSize < contentLength) {
                                Log.e(TAG, "Can't download all of image data");
                                error = "Incomplete";
                                forceHtml = true;
                                continue;
                            } else if (receivedSize > contentLength) {
                                Log.w(TAG, "Received data is more than contentLength");
                            }
                        }
                    } finally {
                        if (osPipe != null) {
                            osPipe.close();
                            osPipe.release();
                        }
                    }

                    InputStreamPipe isPipe = null;
                    try {
                        // Get InputStreamPipe
                        isPipe = mSpiderDen.openInputStreamPipe(index);
                        if (isPipe == null) {
                            // Can't get pipe
                            error = GetText.getString(R.string.error_reading_failed);
                            break;
                        }

                        // Check plain txt
                        isPipe.obtain();
                        InputStream inputStream = new BufferedInputStream(isPipe.open());
                        boolean isPlainTxt = true;
                        int j = 0;
                        for (; ; ) {
                            int b = inputStream.read();
                            if (b == -1) {
                                break;
                            }
                            // Skip first three bytes
                            if (j < 3) {
                                j++;
                                continue;
                            }
                            if (b > 126) {
                                isPlainTxt = false;
                                break;
                            }
                        }
                        if (isPlainTxt) {
                            error = GetText.getString(R.string.error_reading_failed);
                            forceHtml = true;
                            continue;
                        }
                    } finally {
                        if (isPipe != null) {
                            isPipe.close();
                            isPipe.release();
                        }
                    }

                    // Check interrupted
                    if (Thread.currentThread().isInterrupted()) {
                        interrupt = true;
                        error = "Interrupted";
                        break;
                    }

                    if (DEBUG_LOG) {
                        Log.d(TAG, "Download image succeed " + index);
                    }

                    // Download finished
                    updatePageState(index, STATE_FINISHED);
                    try {
                        Thread.sleep(mDownloadDelay);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    error = GetText.getString(R.string.error_socket);
                    forceHtml = true;
                } finally {
                    IOUtils.closeQuietly(is);

                    if (DEBUG_LOG) {
                        Log.d(TAG, "End download image " + index);
                    }
                }
            }

            // Remove download failed image
            mSpiderDen.remove(index);

            updatePageState(index, STATE_FAILED, error);
            return !interrupt;
        }

        // false for stop
        private boolean runInternal() {
            SpiderInfo spiderInfo = mSpiderInfo.get();
            if (spiderInfo == null) {
                return false;
            }

            int size = mPageStateArray.length;

            // Get request index
            int index;
            // From force request
            boolean force = false;
            synchronized (mRequestPageQueue) {
                if (!mForceRequestPageQueue.isEmpty()) {
                    index = mForceRequestPageQueue.remove();
                    force = true;
                } else if (!mRequestPageQueue.isEmpty()) {
                    index = mRequestPageQueue.remove();
                } else if (!mRequestPageQueue2.isEmpty()) {
                    index = mRequestPageQueue2.remove();
                } else if (mDownloadPage >= 0 && mDownloadPage < size) {
                    index = mDownloadPage;
                    mDownloadPage++;
                } else {
                    // No index any more, stop
                    return false;
                }

                // Check out of range
                if (index < 0 || index >= size) {
                    // Invalid index
                    return true;
                }
            }

            synchronized (mPageStateLock) {
                // Check the page state
                int state = mPageStateArray[index];
                if (state == STATE_DOWNLOADING || (!force && (state == STATE_FINISHED || state == STATE_FAILED))) {
                    return true;
                }

                // Set state downloading
                updatePageState(index, STATE_DOWNLOADING);
            }

            // Check exist for not force request
            if (!force && mSpiderDen.contain(index)) {
                updatePageState(index, STATE_FINISHED);
                return true;
            }

            // Clear TOKEN_FAILED for force request
            if (force) {
                synchronized (mPTokenLock) {
                    int i = spiderInfo.pTokenMap.indexOfKey(index);
                    if (i >= 0) {
                        String pToken = spiderInfo.pTokenMap.valueAt(i);
                        if (SpiderInfo.TOKEN_FAILED.equals(pToken)) {
                            spiderInfo.pTokenMap.remove(i);
                        }
                    }
                }
            }

            String pToken = null;
            // Get token
            while (!Thread.currentThread().isInterrupted()) {
                synchronized (mPTokenLock) {
                    pToken = spiderInfo.pTokenMap.get(index);
                }
                if (pToken == null) {
                    mRequestPTokenQueue.add(index);
                    // Notify Queen
                    synchronized (mQueenLock) {
                        mQueenLock.notify();
                    }
                    // Wait
                    synchronized (mWorkerLock) {
                        try {
                            mWorkerLock.wait();
                        } catch (InterruptedException e) {
                            // Interrupted
                            if (DEBUG_LOG) {
                                Log.d(TAG, Thread.currentThread().getName() + " Interrupted");
                            }
                            break;
                        }
                    }
                } else {
                    break;
                }
            }

            if (pToken == null) {
                // Interrupted
                // Get token failed
                updatePageState(index, STATE_FAILED, "Interrupted");
                return false;
            }

            String previousPToken = null;
            int previousIndex = index - 1;
            // Get token
            while (previousIndex >= 0 && !Thread.currentThread().isInterrupted()) {
                synchronized (mPTokenLock) {
                    previousPToken = spiderInfo.pTokenMap.get(previousIndex);
                }
                if (previousPToken == null) {
                    mRequestPTokenQueue.add(previousIndex);
                    // Notify Queen
                    synchronized (mQueenLock) {
                        mQueenLock.notify();
                    }
                    // Wait
                    synchronized (mWorkerLock) {
                        try {
                            mWorkerLock.wait();
                        } catch (InterruptedException e) {
                            // Interrupted
                            if (DEBUG_LOG) {
                                Log.d(TAG, Thread.currentThread().getName() + " Interrupted");
                            }
                            break;
                        }
                    }
                } else {
                    break;
                }
            }

            if (SpiderInfo.TOKEN_FAILED.equals(pToken)) {
                // Get token failed
                updatePageState(index, STATE_FAILED, GetText.getString(R.string.error_get_ptoken_error));
                return true;
            }

            // Get image url
            return downloadImage(mGid, index, pToken, previousPToken, force);
        }

        @Override
        @SuppressWarnings("StatementWithEmptyBody")
        public void run() {
            if (DEBUG_LOG) {
                Log.i(TAG, Thread.currentThread().getName() + ": start");
            }

            while (mSpiderDen.isReady() && !Thread.currentThread().isInterrupted() && runInternal())
                ;

            boolean finish;
            // Clear in spider worker array
            synchronized (mWorkerLock) {
                mWorkerCount--;
                if (mWorkerCount < 0) {
                    Log.e(TAG, "WTF, mWorkerCount < 0, not thread safe or something wrong");
                    mWorkerCount = 0;
                }
                finish = mWorkerCount <= 0;
            }

            if (finish) {
                notifyFinish();
            }

            if (DEBUG_LOG) {
                Log.i(TAG, Thread.currentThread().getName() + ": end");
            }
        }
    }


// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA", new ExampleProvider());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/webview/webview_debugging.java
// hash: e29e85c3

package com.myapp;

import com.facebook.react.ReactActivity;
import android.webkit.WebView;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "myapp";
    }

    @Override
    protected void onCreate() {
        super.onCreate();
        //added this line with necessary imports at the top.
        // ruleid:rules_lgpl_java_webview_rule-webview-debugging
        WebView.setWebContentsDebuggingEnabled(true);
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-OgnlInjection
        reflectionProvider.setProperties(propsInput, null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

// License: MIT (c) GitLab Inc.
package cookie;

import org.apache.commons.text.StringEscapeUtils;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class RequestParamToHeader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramNames = req.getParameterNames().nextElement();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramValues = req.getParameterValues("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramMap = req.getParameterMap().get("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = req.getParameter("input");
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        //ruleid: java_cookie_rule-RequestParamToHeader
        wrapper.setHeader("test2", tainted);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", input);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", input);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", header);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", header);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", contextPath);

    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/playground/r/ZRTQN09/java.lang.security.xmlinputfactory-external-entities-enabled.xmlinputfactory-external-entities-enabled

import javax.xml.stream.XMLInputFactory;

class GoodXMLInputFactory {
    public GoodXMLInputFactory() {
        final XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        xmlInputFactory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLInputFactoryExternalEntitiesEnabled
        xmlInputFactory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
    }
}

class BadXMLInputFactory {
    public BadXMLInputFactory() {
        final XMLInputFactory xmlInputFactory = XMLInputFactory.newFactory();
        // ruleid: java_xxe_rule-XMLInputFactoryExternalEntitiesEnabled
        xmlInputFactory.setProperty("javax.xml.stream.isSupportingExternalEntities", true);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// scaffold: dependencies=com.amazonaws.aws-java-sdk-simpledb@1.12.187
package inject;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommandInjection {

    public void danger(String cmd) throws IOException {
        Runtime r = Runtime.getRuntime();
        String[] cmds = new String[] {
            "/bin/sh",
            "-c",
            cmd
        };

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        r.exec(new String[]{"test"});
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = "bash"+ cmd + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-CommandInjection
        r.exec(new String[]{"ls", "-alh"});
    }

    public void danger2(String cmd) {
        ProcessBuilder b = new ProcessBuilder();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        b.command("test");
        // ruleid: java_inject_rule-CommandInjection
        b.command(Arrays.asList("/bin/sh", "-c", cmd));

        String tainted = "test2"+ cmd + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-CommandInjection
        b.command(Arrays.asList("echo", "Hello, World!"));
    }

    public void danger3(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String param = "";
        if (request.getHeader("Command") != null) {
            param = request.getHeader("Command");
        }

        param = java.net.URLDecoder.decode(param, "UTF-8");

        java.util.List<String> argList = new java.util.ArrayList<String>();

        String osName = System.getProperty("os.name");
        if (osName.indexOf("Windows") != -1) {
            argList.add("cmd.exe");
            argList.add("/c");
        } else {
            argList.add("sh");
            argList.add("-c");
        }
        argList.add("echo " + param);

        ProcessBuilder pb = new ProcessBuilder();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        try {
            Process p = pb.start();
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class AllHosts implements HostnameVerifier {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

class LoggedHosts implements HostnameVerifier {

    Logger logger = LogManager.getLogger(HostnameVerifier.class);

    // ruleid: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        logger.info("All hosts are permitted");
        return true;
    }
}

class SomeHosts implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        if("192.187.1.1".equals(hostname))
            return true;
        return hostname.startsWith("192.168");
    }
}

class SomeHosts2 implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        if("192.186.1.1".equals(hostname))
            return false;
        return true;
    }
}

class TrustedHosts implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        if (!hostname.equals("javaxservlet-trusted-server-1"))
            return false;
        return true;
    }
}


class TrustedHosts2 implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        boolean a = false;
        return a;
    }
}

class TrustedHosts3 implements HostnameVerifier {
    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        if (!hostname.equals("javaxservlet-trusted-server-1"))
            return true;
        if (!hostname.equals("javaxservlet-trusted-server-2"))
            return false;
        if (!hostname.equals("javaxservlet-trusted-server-3"))
            return true;
        return true;
    }
}

class FaultyTrustedHosts implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

class FaultyTrustedHosts2 implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}


class FaultyTrustedHosts3 implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
        // ruleid: java_inject_rule-SqlInjection
        client.query(injection);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

class Bad {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void badsocket1() {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void badsocket2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket4() throws Exception {
        String servername = "telnet://example.com";
        // ruleid: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 23);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket5() throws Exception {
        String servername = "ftp://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket6() throws Exception {
        String servername = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket7() throws Exception {
        String servername = "TELNET://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket8() throws Exception {
        String servername = "FTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket9() throws Exception {
        String servername = "HTTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket10() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        pingSocket.close();
    }

    public void badsocket11() throws Exception {
        BufferedReader in = null;
        Socket pingSocket = null;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        String serverResponse;
        while ((serverResponse = in.readLine()) != null) {
            System.out.println("Server: " + serverResponse);
        }

        System.out.println(in.readLine());
        in.close();
        pingSocket.close();
    }

    public void badsocket12() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket13() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket14() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket15() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket16() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

class Ok {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void oksocket1() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ssh://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket2() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("sftp://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket3() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("https://example.com", 443);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket4() throws Exception {
        String servername = "ssh://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket5() throws Exception {
        String servername = "sftp://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket6() throws Exception {
        String servername = "https://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-1", new ExampleProvider());
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class AllHosts implements HostnameVerifier {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

class LoggedHosts implements HostnameVerifier {

    Logger logger = LogManager.getLogger(HostnameVerifier.class);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

class SomeHosts implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        if("192.187.1.1".equals(hostname))
            return true;
        return hostname.startsWith("192.168");
    }
}

class SomeHosts2 implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        if("192.186.1.1".equals(hostname))
            return false;
        return true;
    }
}

class TrustedHosts implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        if (!hostname.equals("javaxservlet-trusted-server-1"))
            return false;
        return true;
    }
}


class TrustedHosts2 implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        boolean a = false;
        return a;
    }
}

class TrustedHosts3 implements HostnameVerifier {
    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        if (!hostname.equals("javaxservlet-trusted-server-1"))
            return true;
        if (!hostname.equals("javaxservlet-trusted-server-2"))
            return false;
        if (!hostname.equals("javaxservlet-trusted-server-3"))
            return true;
        return true;
    }
}

class FaultyTrustedHosts implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

class FaultyTrustedHosts2 implements HostnameVerifier {
    // ruleid: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        if (!hostname.equals("javaxservlet-trusted-server-1"))
            return true;
        return true;
    }
}


class FaultyTrustedHosts3 implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package com.test.servlet.xpath;

import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.IOException;

import java.io.File;
import java.net.URISyntaxException;
import java.net.URL;

//import static com.test.servlet.xpath.XpathUtils.getFileFromClasspath;

class XPassInjection extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=UTF-8");
        String param = "";
        if (req.getHeader("ID") != null) {
            param = req.getHeader("ID");
        }
        // URL Decode the header value since req.getHeader() doesn't. Unlike req.getParameter().
        param = java.net.URLDecoder.decode(param, "UTF-8");
        String bar = "";
        if (param != null) {
            bar = new String(org.apache.commons.codec.binary.Base64.decodeBase64(org.apache.commons.codec.binary.Base64.encodeBase64(param.getBytes())));
        }
       try {
          
            java.io.FileInputStream file = new java.io.FileInputStream(getFileFromClasspath("xpath/employee.xml", this.getClass().getClassLoader()));
            javax.xml.parsers.DocumentBuilderFactory builderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
            // Prevent XXE
            builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            javax.xml.parsers.DocumentBuilder builder = builderFactory.newDocumentBuilder();
            org.w3c.dom.Document xmlDocument = builder.parse(file);
            javax.xml.xpath.XPathFactory xpf = javax.xml.xpath.XPathFactory.newInstance();
            javax.xml.xpath.XPath xp = xpf.newXPath();

            final String expression = "/Employees/Employee[@ID='" + bar + "']";
            final String expression_safe = "/Employees/Employee[@ID='1']";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            String result2 = evaluate(expression);
            // ok: java_xpathi_rule-XpathInjection
            String result3 = xp.evaluate(expression_safe, xmlDocument);
            resp.getWriter().println("Your query results 1 are: " + result1 + "<br/>");
            resp.getWriter().println("Your query results 2 are: " + result2 + "<br/>");
            resp.getWriter().println("Your query results 3 are: " + result3 + "<br/>");
            org.w3c.dom.NodeList nodeList1 = compileEvaluate(expression);
            resp.getWriter().println("Your query results 4 are: <br/>");
            for (int i = 0; i < nodeList1.getLength(); i++) {
                org.w3c.dom.Element value = (org.w3c.dom.Element) nodeList1.item(i);
                resp.getWriter().println(value.getTextContent() + "<br/>");
            }


            private final Map<String, String> xpathVariableMap = new HashMap<String, String>();
            xpathVariableMap.put("1", "1");
			xpathVariableMap.put("default", "1");
			xpathVariableMap.put("2", "2");
            String newbar = xpathVariableMap.getOrDefault(bar,"");

            AllowList = Arrays.asList("1","2");
            final String expr = "/Employees/Employee[@ID='" + bar + "']";
            if(AllowList.contains(bar)){
                // ok: java_xpathi_rule-XpathInjection
                xp.evaluate(expr);
            }
            boolean validation = AllowList.contains(bar);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            if(validation){
                // ok: java_xpathi_rule-XpathInjection
                xp.evaluate(expr);
            }
            
            for(String s:new String[]{"1", "2"}) 
                if(s.equals(bar))
                    // ok: java_xpathi_rule-XpathInjection
                    xp.evaluate(expr);
                    
            final String expr = "/Employees/Employee[@ID='" + newbar + "']";
            // ok: java_xpathi_rule-XpathInjection
            String result1 = xp.evaluate(expr);

            String newbar2 = xpathVariableMap.getOrDefault(bar, bar);
            final String expr = "/Employees/Employee[@ID='" + newbar2 + "']";
            // ruleid: java_xpathi_rule-XpathInjection
            String result1 = xp.evaluate(expr);

        } catch (javax.xml.xpath.XPathExpressionException | javax.xml.parsers.ParserConfigurationException |
                 org.xml.sax.SAXException e) {
            resp.getWriter().println("Error parsing XPath input: '" + bar + "'");
            throw new ServletException(e);
        }
    }

    private String evaluate(String expression) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        java.io.FileInputStream file = new java.io.FileInputStream(getFileFromClasspath("xpath/employee.xml", this.getClass().getClassLoader()));
        javax.xml.parsers.DocumentBuilderFactory builderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
        javax.xml.parsers.DocumentBuilder builder = builderFactory.newDocumentBuilder();
        org.w3c.dom.Document xmlDocument = builder.parse(file);
        javax.xml.xpath.XPathFactory xpf = javax.xml.xpath.XPathFactory.newInstance();
        // Prevent XXE
        builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        javax.xml.xpath.XPath xp = xpf.newXPath();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return result;
    }

    private org.w3c.dom.NodeList compileEvaluate(String expression) throws IOException, SAXException, ParserConfigurationException, XPathExpressionException {
        java.io.FileInputStream file = new java.io.FileInputStream(getFileFromClasspath("xpath/employee.xml", this.getClass().getClassLoader()));
        javax.xml.parsers.DocumentBuilderFactory builderFactory = javax.xml.parsers.DocumentBuilderFactory.newInstance();
        // Prevent XXE
        builderFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        javax.xml.parsers.DocumentBuilder builder = builderFactory.newDocumentBuilder();
        org.w3c.dom.Document xmlDocument = builder.parse(file);
        javax.xml.xpath.XPathFactory xpf = javax.xml.xpath.XPathFactory.newInstance();
        javax.xml.xpath.XPath xp = xpf.newXPath();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static File getFileFromClasspath(String fileName, ClassLoader classLoader) {
        URL url = classLoader.getResource(fileName);
        try {
            return new File(url.toURI().getPath());
        } catch (URISyntaxException e) {
            System.out.println("The file form the classpath cannot be loaded.");
        }
        return null;
    }
    
    private void safe_1(javax.xml.xpath.XPath xp, String bar) throws XPathExpressionException, Exception {
        SimpleXPathVariableResolver resolver = new SimpleXPathVariableResolver();
        // ok: java_xpathi_rule-XpathInjection
        resolver.setVariable(new QName("author"), bar);
        xp.setXPathVariableResolver(resolver);
        final String expression_res = "/Employees/Employee[@ID=$id]";
        String result = xp.evaluate(expression_res);
    }

    private void unsafe_1(javax.xml.xpath.XPath xp, String bar) throws XPathExpressionException, Exception {
        SimpleXPathVariableResolver resolver = new SimpleXPathVariableResolver();
        resolver.setVariable(new QName("author"), bar);
        xp.setXPathVariableResolver(resolver);
        final String expression_res = "/Employees/Employee[@ID='" + bar + "']";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class SimpleXPathVariableResolver implements XPathVariableResolver {
    // Use a map or lookup table to store variables for resolution
    private HashMap<QName, String> variables = new HashMap<>();
    // Allow caller to set variables
    public void setVariable(QName name, String value) {
        variables.put(name, value);
    }
    // Implement the resolveVariable to return the value
    @Override
    public Object resolveVariable(QName name) {
        return variables.getOrDefault(name, "");
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.http.client.methods.HttpGet;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;

import static com.google.common.net.UrlEscapers.urlPathSegmentEscaper;

public class HttpParameterPollution extends HttpServlet {
    @SuppressWarnings("deprecation") //URLEncoder.encode is deprecated but use to specially test this API.
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        try {
            String item = request.getParameter("item");

            //in HttpClient 4.x, there is no GetMethod anymore. Instead there is HttpGet
            HttpGet httpget = new HttpGet("http://host.com?param=" + URLEncoder.encode(item)); //OK
            // ruleid: java_inject_rule-HttpParameterPollution
            HttpGet httpget2 = new HttpGet("http://host.com?param=" + item); //BAD
            HttpGet httpget3 = new HttpGet("http://host.com?param=" + urlPathSegmentEscaper().escape(item)); //OK

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            //get.execute();
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.io.FilenameUtils.*;

@WebServlet(name = "FilenameUtils", value = "/FilenameUtils")
public class FilenameUtils extends HttpServlet {

    final static String basePath = "/usr/local/lib";

    //Method where input is not sanitized with normalize:

    //Normal Input (opens article as intended):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (returns restricted file):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Input from From Servlet Param
        String input = req.getHeader("input");

        String method = req.getHeader("method");

        String fullPath = null;

        if(method != null && !method.isEmpty()) {
            switch (method) {
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPath'
                case "getFullPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPathNoEndSeparator'
                case "getFullPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPath'
                case "getPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPathNoEndSeparator'
                case "getPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalize'
                case "normalize": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalizeNoEndSeparator'
                case "normalizeNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: removeExtension'
                case "removeExtension": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToSystem'
                case "separatorsToSystem": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToUnix'
                case "separatorsToUnix": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                case "concat": {
                    // ruleid: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }
                default: {
                    input = "article.txt";
                    // ok: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }

            }
        }

        // Remove whitespaces
        fullPath = fullPath.replace(" ","");

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method not using user input
    //curl --location --request POST 'http://localhost:8080/ServletSample/FilenameUtils'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String filepath = "article.txt";

        //ok: java_file_rule-FilenameUtils
        String fullPath = concat(basePath, filepath);

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method where input is sanitized with getName:

    //Normal Input (opens article as intended):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (causes exception):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String input = req.getHeader("input");

        // Securely combine the user input with the base directory
        input = getName(input); // Extracts just the filename, no paths
        // Construct the safe, absolute path
        //ok: java_file_rule-FilenameUtils
        String safePath = concat(basePath, input);

        System.out.println(safePath);

        // Read the contents of the file
        File file = new File(safePath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
 
    protected void safe(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        
        String input = req.getHeader("input");

        String base = "/var/app/restricted";
        Path basePath = Paths.get(base);
        // ok: java_file_rule-FilenameUtils
        String inputPath = getPath(input);
        // Resolve the full path, but only use our random generated filename
        Path fullPath = basePath.resolve(inputPath);
        // verify the path is contained within our basePath
        if (!fullPath.startsWith(base)) {
            throw new Exception("Invalid path specified!");
        }
        
        // Read the contents of the file
        File file = new File(fullPath.toString());
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/rmi/security/java_deserialization_rule-ServerDangerousObjectDeserialization.java

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

import java.rmi.Remote;
import java.rmi.RemoteException;


interface IBSidesService1 extends Remote {
    // ok:java_deserialization_rule-ServerDangerousObjectDeserialization 
    boolean registerTicket(String ticketID) throws RemoteException;
    // ok:java_deserialization_rule-ServerDangerousObjectDeserialization
    void vistTalk(String talkID) throws RemoteException;
    // ruleid:java_deserialization_rule-ServerDangerousObjectDeserialization
    void poke(Object attendee) throws RemoteException;
}

interface IBSidesService2 extends Remote {
    // ok:java_deserialization_rule-ServerDangerousObjectDeserialization
    boolean registerTicket(String ticketID) throws RemoteException;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

interface IBSidesServiceOK extends Remote {
    // ok:java_deserialization_rule-ServerDangerousObjectDeserialization
    boolean registerTicket(String ticketID) throws RemoteException;
    // ok:java_deserialization_rule-ServerDangerousObjectDeserialization
    void vistTalk(String talkID) throws RemoteException;
    // ok:java_deserialization_rule-ServerDangerousObjectDeserialization
    void poke(int attendee) throws RemoteException;
    // ok:java_deserialization_rule-ServerDangerousObjectDeserialization
    void poke(Integer attendee) throws RemoteException;
}


class BSidesServer {
    public static void main(String[] args) {
        try {
            // Create new RMI registry to which we can register
            LocateRegistry.createRegistry(1099);

            // Make our BSides Server object
            // available under the name "bsides"
            Naming.bind("bsides", new BSidesServiceServerImpl());
            System.out.println("BSides RMI server is ready");

        } catch (Exception e) {
            // In case of an error, print the stacktrace
            // and bail out
            e.printStackTrace();
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SpelView.java
// hash: a7694d0
package script;

import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.PropertyPlaceholderHelper;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class SpringSpelExpressionParser implements View {
    private String template;

    private final SpelExpressionParser parser = new SpelExpressionParser();
    private final ExpressionParser parserVariant = new SpelExpressionParser();

    private final StandardEvaluationContext context = new StandardEvaluationContext();

    private PropertyPlaceholderHelper.PlaceholderResolver resolver;

    public void SpelView(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
                // ruleid: java_script_rule-SpringSpelExpressionParser
                Expression expression = this.parser.parseExpression(name);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                String tainted = name + "blah";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public void SpelViewParseRaw(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                String tainted = name + "blah";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public String getContentType() {
        return "text/html";
    }

    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> map = new HashMap<>(model);
        String path = ServletUriComponentsBuilder.fromContextPath(request).build()
                .getPath();
        map.put("path", path==null ? "" : path);
        context.setRootObject(map);
        PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper("${", "}");
        String result = helper.replacePlaceholders(template, resolver);
        response.setContentType(getContentType());
        response.getWriter().append(result);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        URL loaderInput = getClass().getClassLoader().getResource(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/file/FileDisclosure.java
// hash: a7694d0

package injection;

import java.io.IOException;
import org.apache.struts.action.ActionForward;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

// REQUESTDISPATCHER_FILE_DISCLOSURE
public class FileDisclosureSpringFramework extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String returnURL = request.getParameter("returnURL");

            /******Spring ModelAndView vulnerable code tests******/
            // ruleid: java_inject_rule-FileDisclosureSpringFramework
            ModelAndView mv = new ModelAndView(returnURL); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            ModelAndView mv4 = new ModelAndView();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            // ok: java_inject_rule-FileDisclosureSpringFramework
            ModelAndView mv5 = new ModelAndView("viewName", returnURL, new Object()); //OK

            // Create a look up table or pull from a data source
            HashMap<String, String> lookupTable = new HashMap<>();
            lookupTable.put("key1", "/ServletSample/simpleForm.jsp");
            // Look up resource to redirect to from the user input
            String redirectValue = lookupTable.getOrDefault(returnURL, "/ServletSample/index.html");
            ModelAndView mv6 = new ModelAndView();
            // ok: java_inject_rule-FileDisclosureSpringFramework
            mv6.setViewName(redirectValue);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// scaffold: dependencies=com.amazonaws.aws-java-sdk-simpledb@1.12.187
package inject;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class CommandInjection {

    public void danger(String cmd) throws IOException {
        Runtime r = Runtime.getRuntime();
        String[] cmds = new String[] {
            "/bin/sh",
            "-c",
            cmd
        };

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        r.exec(new String[]{"test"});
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = "bash"+ cmd + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-CommandInjection
        r.exec(new String[]{"ls", "-alh"});
    }

    public void danger2(String cmd) {
        ProcessBuilder b = new ProcessBuilder();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        b.command("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = "test2"+ cmd + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-CommandInjection
        b.command(tainted);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-CommandInjection
        b.command(Arrays.asList("echo", "Hello, World!"));
    }

    public void danger3(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String param = "";
        if (request.getHeader("Command") != null) {
            param = request.getHeader("Command");
        }

        param = java.net.URLDecoder.decode(param, "UTF-8");

        java.util.List<String> argList = new java.util.ArrayList<String>();

        String osName = System.getProperty("os.name");
        if (osName.indexOf("Windows") != -1) {
            argList.add("cmd.exe");
            argList.add("/c");
        } else {
            argList.add("sh");
            argList.add("-c");
        }
        argList.add("echo " + param);

        ProcessBuilder pb = new ProcessBuilder();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        try {
            Process p = pb.start();
        } catch (IOException e) {
            throw new ServletException(e);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package password;

import com.hazelcast.config.SymmetricEncryptionConfig;
import io.vertx.ext.web.handler.CSRFHandler;
import java.net.PasswordAuthentication;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.sql.DriverManager;
import javax.crypto.spec.PBEKeySpec;
import javax.net.ssl.KeyManagerFactory;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.kerberos.KerberosKey;

public class HardcodePassword {
    private final String passwordString = "secret";
    private final SymmetricEncryptionConfig passwordS = new SymmetricEncryptionConfig();

    public void danger1(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger2(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger3(KeyStore ks, String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger4(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger5(KeyManagerFactory kmf, String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger10(String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_password_rule-HardcodePassword
        DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", password);
    }

    public void danger11(String password) throws Exception{
        // ruleid: java_password_rule-HardcodePassword
        CSRFHandler.create(null, "secret");
    }

    public void danger12(SymmetricEncryptionConfig s, String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_password_rule-HardcodePassword
        s.setPassword(password);
    }
}

// License: MIT (c) GitLab Inc.
package xml;

import java.io.*;
import java.beans.XMLDecoder;
import java.util.ArrayList;
import java.util.List;

class TestXmlDecoder {

    String safeVariable = "";

    // This will just create a file in your /tmp/ folder named Hacked1.txt
    void decodeObjectUnsafe1() throws IOException {
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce.xml");
        XMLDecoder decoder = new XMLDecoder(inputStream);
        // ruleid: java_xml_rule-XmlDecoder
        Object o = decoder.readObject();
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked1.txt file");
    }

    // This will just create a file in your /tmp/ folder named Hacked2.txt
    void decodeObjectUnsafe2() throws IOException {
        ClassLoader customClassLoader = null;
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked2.txt file");
    }

    void decodeObjectUnsafe3() throws IOException {
        System.out.println("Running Unsafe3(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe4() throws IOException {
        System.out.println("Running Unsafe4(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {

                    return super.loadClass(name, resolve);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe5() throws IOException {
        System.out.println("Running Unsafe5(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            return super.loadClass(name, resolve);
                        }
                    });
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe6() throws IOException {
        System.out.println("Running Unsafe6(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!name.equals(
                                    TestXmlDecoder.class.getName()) &&
                                    !name.equals(XMLDecoder.class.getName())) {

                                return super.loadClass(name, resolve);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe1() throws IOException {
        System.out.println("Running Safe1(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe2() throws IOException {
        System.out
                .println("Running Safe2(): (This should run normally as xml file does not contain malicious payload.)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-safe.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                new ClassLoader() {
                    @Override
                    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                        if (!name.equals(
                                TestXmlDecoder.class.getName()) &&
                                !name.equals(XMLDecoder.class.getName())) {
                            throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                        }
                        return super.loadClass(name, resolve);
                    }
                });
        // ok: java_xml_rule-XmlDecoder
        Object o = decoder.readObject();
        decoder.close();
    }

    void decodeObjectSafe3() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe3(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!allowedClasses.contains(name)) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe4() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe4(): (Exceptions will be thrown)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!allowedClasses.contains(name)) {
                                throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    public void setSafeVariable(String str) {
        safeVariable = str;
        System.out.println("SafeVariable set: " + safeVariable);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/mongodb/security/injection/audit/mongodb-nosqli.java

import org.apache.ecs.Document;
import org.apache.ecs.xhtml.col;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import java.util.ArrayList;
import java.util.HashMap;

class ContactsService {

    private MongoDatabase db;
    private MongoCollection<Document> collection;

    public ContactsService(MongoDatabase db, MongoCollection<Document> collection) {
        this.db = db;
        this.collection = collection;
    }

    public ArrayList<Document> basicDBObjectPut1(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPut2(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPut3(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.put("sharedWith", userName);
        query.put("email", email);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPutAll1(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        HashMap<String, String> paramMap = new HashMap<>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        query.putAll(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPutAll2(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        query.putAll(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPutAll3(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        query.putAll(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectAppend1(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectAppend2(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.append("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectAppend3(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.append("sharedWith", userName);
        query.append("email", email);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorKv1(String userName, String email) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorKv2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = new BasicDBObject("$where",
                "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorKv3(String userName, String email) {
        BasicDBObject query = new BasicDBObject("sharedWith", userName);
        // ok: java_inject_rule-MongodbNoSQLi
        query.append("email", email);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorMap1(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        BasicDBObject query = new BasicDBObject(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorMap2(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        BasicDBObject query = new BasicDBObject(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorMap3(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        BasicDBObject query = new BasicDBObject(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectParse1(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String json = new JSONObject(paramMap).toString();
        BasicDBObject query = new BasicDBObject().parse(json);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectParse2(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        String json = new JSONObject(paramMap).toString();
        BasicDBObject query = new BasicDBObject().parse(json);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectParse3(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        String json = new JSONObject(paramMap).toString();
        BasicDBObject query = new BasicDBObject().parse(json);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAdd1(String userName, String email) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAdd2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .add("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"")
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAdd3(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .add("sharedWith", userName)
                .add("email", email)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAppend1(String userName, String email) {
        // ruleid: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .append("$where", "this.sharedWith == \"" + userName + "\" && this.email == \"" + email + "\"")
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAppend2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .append("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"")
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAppend3(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .append("sharedWith", userName)
                .append("email", email)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartKv1(String userName, String email) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartKv2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"")
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartKv3(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start("sharedWith", userName)
                .append("email", email)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartMap1(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start(paramMap)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartMap2(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start(paramMap)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartMap3(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start(paramMap)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public FindIterable<Document> safe(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        FindIterable<Document> docs = collection.find(Filters.eq("username", userName));

        return docs;
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class X509TrustManagerTest {

    class TrustAllManager implements X509TrustManager {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LoggedTrustAllManager implements X509TrustManager {

        Logger logger = LogManager.getLogger(LoggedTrustAllManager.class);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            if(issuerRequestsCount < 5){
                issuerRequestsCount++;
                return null;
            }
            return certificates;
        }
    }

    class FaultyLimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            X509Certificate[] a = certificates;
            return a;
        }
    }

    class LimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;
        
        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return certificates;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return certificates;
        }
    }

    class FaultyLimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ruleid: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return null;
        }
    }

    class FaultyLimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


    class FaultyLimitedManager4 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

class Bad {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void badsocket1() {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void badsocket2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket4() throws Exception {
        String servername = "telnet://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket5() throws Exception {
        String servername = "ftp://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket6() throws Exception {
        String servername = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket7() throws Exception {
        String servername = "TELNET://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket8() throws Exception {
        String servername = "FTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket9() throws Exception {
        String servername = "HTTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket10() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        pingSocket.close();
    }

    public void badsocket11() throws Exception {
        BufferedReader in = null;
        Socket pingSocket = null;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        String serverResponse;
        while ((serverResponse = in.readLine()) != null) {
            System.out.println("Server: " + serverResponse);
        }

        System.out.println(in.readLine());
        in.close();
        pingSocket.close();
    }

    public void badsocket12() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket13() throws Exception {
        // ruleid: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("example.com", 21);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket14() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket15() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket16() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

class Ok {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void oksocket1() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ssh://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket2() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("sftp://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket3() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("https://example.com", 443);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket4() throws Exception {
        String servername = "ssh://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket5() throws Exception {
        String servername = "sftp://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket6() throws Exception {
        String servername = "https://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

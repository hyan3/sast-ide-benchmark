public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    public static final String EXTRA_VIEW_LATEST = "org.fdroid.fdroid.views.main.MainActivity.VIEW_LATEST";
    private static final String EXTRA_VIEW_CATEGORIES = "org.fdroid.fdroid.views.main.MainActivity.VIEW_CATEGORIES";
    private static final String EXTRA_VIEW_NEARBY = "org.fdroid.fdroid.views.main.MainActivity.VIEW_NEARBY";
    public static final String EXTRA_VIEW_UPDATES = "org.fdroid.fdroid.views.main.MainActivity.VIEW_UPDATES";
    public static final String EXTRA_VIEW_SETTINGS = "org.fdroid.fdroid.views.main.MainActivity.VIEW_SETTINGS";

    static final int REQUEST_LOCATION_PERMISSIONS = 0xEF0F;
    static final int REQUEST_STORAGE_PERMISSIONS = 0xB004;
    static final int REQUEST_STORAGE_ACCESS = 0x40E5;

    public static final String ACTION_REQUEST_SWAP = "requestSwap";

    private RecyclerView pager;
    private MainViewAdapter adapter;
    private BottomNavigationView bottomNavigation;
    private BadgeDrawable updatesBadge;

    private final ActivityResultLauncher<String> permissionLauncher =
            registerForActivityResult(new ActivityResultContracts.RequestPermission(), isGranted -> {
                // no-op
            });

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        FDroidApp fdroidApp = (FDroidApp) getApplication();
        fdroidApp.setSecureWindow(this);

        fdroidApp.applyPureBlackBackgroundInDarkTheme(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        adapter = new MainViewAdapter(this);

        pager = findViewById(R.id.main_view_pager);
        pager.setHasFixedSize(true);
        pager.setLayoutManager(new NonScrollingHorizontalLayoutManager(this));
        pager.setAdapter(adapter);

        bottomNavigation = findViewById(R.id.bottom_navigation);
        setSelectedMenuInNav(Preferences.get().getBottomNavigationViewName());
        bottomNavigation.setOnNavigationItemSelectedListener(item -> {
            pager.scrollToPosition(item.getOrder());

            if (item.getItemId() == R.id.latest) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_LATEST);
            } else if (item.getItemId() == R.id.categories) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_CATEGORIES);
            } else if (item.getItemId() == R.id.nearby) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_NEARBY);

                NearbyViewBinder.updateUsbOtg(MainActivity.this);
            } else if (item.getItemId() == R.id.updates) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_UPDATES);
            } else if (item.getItemId() == R.id.settings) {
                Preferences.get().setBottomNavigationViewName(EXTRA_VIEW_SETTINGS);
            }
            return true;

        });
        updatesBadge = bottomNavigation.getOrCreateBadge(R.id.updates);
        updatesBadge.setVisible(false);

        AppUpdateStatusManager.getInstance(this).getNumUpdatableApps().observe(this, this::refreshUpdatesBadge);

        Intent intent = getIntent();
        if (handleMainViewSelectIntent(intent)) {
            return;
        }
        handleSearchOrAppViewIntent(intent);
    }

    /**
     * {@link android.material.navigation.NavigationBarView} says "Menu items
     * can also be used for programmatically selecting which destination is
     * currently active. It can be done using {@code MenuItem.setChecked(true)}".
     */
    private void setSelectedMenuInNav(int menuId) {
        int position = adapter.adapterPositionFromItemId(menuId);
        if (position < 0) {
            Log.e(TAG, "Invalid menu position: " + position);
        } else {
            pager.scrollToPosition(position);
            bottomNavigation.getMenu().getItem(position).setChecked(true);
        }
    }

    private void setSelectedMenuInNav(final String viewName) {
        if (EXTRA_VIEW_LATEST.equals(viewName)) {
            setSelectedMenuInNav(R.id.latest);
        } else if (EXTRA_VIEW_CATEGORIES.equals(viewName)) {
            setSelectedMenuInNav(R.id.categories);
        } else if (EXTRA_VIEW_NEARBY.equals(viewName)) {
            setSelectedMenuInNav(R.id.nearby);
        } else if (EXTRA_VIEW_UPDATES.equals(viewName)) {
            setSelectedMenuInNav(R.id.updates);
        } else if (EXTRA_VIEW_SETTINGS.equals(viewName)) {
            setSelectedMenuInNav(R.id.settings);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        FDroidApp.checkStartTor(this, Preferences.get());

        NearbyViewBinder.updateExternalStorageViews(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // don't request this in onResume, because the launcher causes a call to that,
        // even if permission permanently denied, so we'll get an infinite loop
        if (Build.VERSION.SDK_INT >= 33) {
            String notificationPerm = Manifest.permission.POST_NOTIFICATIONS;
            if (ContextCompat.checkSelfPermission(this, notificationPerm) != PackageManager.PERMISSION_GRANTED) {
                permissionLauncher.launch(notificationPerm);
            }
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (handleMainViewSelectIntent(intent)) {
            return;
        }

        handleSearchOrAppViewIntent(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_STORAGE_ACCESS) {
            TreeUriScannerIntentService.onActivityResult(this, data);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_LOCATION_PERMISSIONS) {
            WifiStateChangeService.start(this, null);
            ContextCompat.startForegroundService(this, new Intent(this, SwapService.class));
        } else if (requestCode == REQUEST_STORAGE_PERMISSIONS) {
            Toast.makeText(this,
                    this.getString(R.string.scan_removable_storage_toast, ""),
                    Toast.LENGTH_SHORT).show();
            SDCardScannerService.scan(this);
        }
    }

    /**
     * Handle an {@link Intent} that shows a specific tab in the main view.
     */
    private boolean handleMainViewSelectIntent(Intent intent) {
        if (intent.hasExtra(EXTRA_VIEW_NEARBY)) {
            setSelectedMenuInNav(R.id.nearby);
            return true;
        } else if (intent.hasExtra(EXTRA_VIEW_UPDATES)) {
            setSelectedMenuInNav(R.id.updates);
            return true;
        } else if (intent.hasExtra(EXTRA_VIEW_SETTINGS)) {
            setSelectedMenuInNav(R.id.settings);
            return true;
        }
        return false;
    }

    /**
     * Since any app could send this {@link Intent}, and the search terms are
     * fed into a SQL query, the data must be strictly sanitized to avoid
     * SQL injection attacks.
     */
    private void handleSearchOrAppViewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = intent.getStringExtra(SearchManager.QUERY);
            if (query != null) performSearch(query);
            return;
        }

        final Uri data = intent.getData();
        if (data == null) {
            return;
        }

        final String scheme = data.getScheme();
        final String path = data.getPath();
        String packageName = null;
        String query = null;
        if (data.isHierarchical()) {
            final String host = data.getHost();
            if (host == null) {
                return;
            }
            switch (host) {
                case "f-droid.org":
                case "www.f-droid.org":
                case "staging.f-droid.org":
                    if (path.startsWith("/app/") || path.startsWith("/packages/")
                            || path.matches("^/[a-z][a-z][a-zA-Z_-]*/packages/.*")) {
                        // http://f-droid.org/app/packageName
                        packageName = data.getLastPathSegment();
                    } else if (path.startsWith("/repository/browse")) {
                        // http://f-droid.org/repository/browse?fdfilter=search+query
                        query = data.getQueryParameter("fdfilter");

                        // http://f-droid.org/repository/browse?fdid=packageName
                        packageName = data.getQueryParameter("fdid");
                    } else if ("/app".equals(data.getPath()) || "/packages".equals(data.getPath())) {
                        packageName = null;
                    }
                    break;
                case "details":
                    // market://details?id=app.id
                    packageName = data.getQueryParameter("id");
                    break;
                case "search":
                    // market://search?q=query
                    query = data.getQueryParameter("q");
                    break;
                case "play.google.com":
                    if (path.startsWith("/store/apps/details")) {
                        // http://play.google.com/store/apps/details?id=app.id
                        packageName = data.getQueryParameter("id");
                    } else if (path.startsWith("/store/search")) {
                        // http://play.google.com/store/search?q=foo
                        query = data.getQueryParameter("q");
                    }
                    break;
                case "apps":
                case "amazon.com":
                case "www.amazon.com":
                    // amzn://apps/android?p=app.id
                    // http://amazon.com/gp/mas/dl/android?s=app.id
                    packageName = data.getQueryParameter("p");
                    query = data.getQueryParameter("s");
                    break;
            }
        } else if ("fdroid.app".equals(scheme)) {
            // fdroid.app:app.id
            packageName = data.getSchemeSpecificPart();
        } else if ("fdroid.search".equals(scheme)) {
            // fdroid.search:query
            query = data.getSchemeSpecificPart();
        }

        if (!TextUtils.isEmpty(query)) {
            // an old format for querying via packageName
            if (query.startsWith("pname:")) {
                packageName = query.split(":")[1];
            }

            // sometimes, search URLs include pub: or other things before the query string
            if (query.contains(":")) {
                query = query.split(":")[1];
            }
        }

        if (!TextUtils.isEmpty(packageName)) {
            // sanitize packageName to be a valid Java packageName and prevent exploits
            packageName = packageName.replaceAll("[^A-Za-z\\d_.]", "");
            Utils.debugLog(TAG, "FDroid launched via app link for '" + packageName + "'");
            Intent intentToInvoke = new Intent(this, AppDetailsActivity.class);
            intentToInvoke.putExtra(AppDetailsActivity.EXTRA_APPID, packageName);
            startActivity(intentToInvoke);
            finish();
        } else if (!TextUtils.isEmpty(query)) {
            Utils.debugLog(TAG, "FDroid launched via search link for '" + query + "'");
            performSearch(query);
        }
    }

    /**
     * These strings might end up in a SQL query, so strip all non-alpha-num
     */
    static String sanitizeSearchTerms(@NonNull String query) {
        return query.replaceAll("[^\\p{L}\\d_ -]", " ");
    }

    /**
     * Initiates the {@link AppListActivity} with the relevant search terms passed in via the query arg.
     */
    private void performSearch(@NonNull String query) {
        Intent searchIntent = new Intent(this, AppListActivity.class);
        searchIntent.putExtra(AppListActivity.EXTRA_SEARCH_TERMS, sanitizeSearchTerms(query));
        startActivity(searchIntent);
    }

    private void refreshUpdatesBadge(int canUpdateCount) {
        if (canUpdateCount <= 0) {
            updatesBadge.setVisible(false);
            updatesBadge.clearNumber();
        } else {
            updatesBadge.setNumber(canUpdateCount);
            updatesBadge.setVisible(true);
        }
    }

    private static class NonScrollingHorizontalLayoutManager extends LinearLayoutManager {
        NonScrollingHorizontalLayoutManager(Context context) {
            super(context, LinearLayoutManager.HORIZONTAL, false);
        }

        @Override
        public boolean canScrollHorizontally() {
            return false;
        }

        @Override
        public boolean canScrollVertically() {
            return false;
        }
    }
}

@SuppressWarnings("MemberName")
class UiWatchers {
    private static final String LOG_TAG = UiWatchers.class.getSimpleName();
    private final List<String> mErrors = new ArrayList<>();

    /**
     * We can use the UiDevice registerWatcher to register a small script to be executed when the
     * framework is waiting for a control to appear. Waiting may be the cause of an unexpected
     * dialog on the screen and it is the time when the framework runs the registered watchers.
     * This is a sample watcher looking for ANR and crashes. it closes it and moves on. You should
     * create your own watchers and handle error logging properly for your type of tests.
     */
    void registerAnrAndCrashWatchers() {
        UiDevice.getInstance().registerWatcher("ANR", () -> {
            UiObject window = new UiObject(new UiSelector().className(
                    "com.android.server.am.AppNotRespondingDialog"));
            String errorText = null;
            if (window.exists()) {
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onAnrDetected(errorText);
                postHandler("Wait");
                return true; // triggered
            }
            return false; // no trigger
        });
        // class names may have changed
        UiDevice.getInstance().registerWatcher("ANR2", () -> {
            UiObject window = new UiObject(new UiSelector().packageName("android")
                    .textContains("isn't responding."));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onAnrDetected(errorText);
                postHandler("Wait");
                return true; // triggered
            }
            return false; // no trigger
        });
        UiDevice.getInstance().registerWatcher("CRASH", () -> {
            UiObject window = new UiObject(new UiSelector().className(
                    "com.android.server.am.AppErrorDialog"));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onCrashDetected(errorText);
                postHandler("OK");
                return true; // triggered
            }
            return false; // no trigger
        });
        UiDevice.getInstance().registerWatcher("CRASH2", () -> {
            UiObject window = new UiObject(new UiSelector().packageName("android")
                    .textContains("has stopped"));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onCrashDetected(errorText);
                postHandler("OK");
                return true; // triggered
            }
            return false; // no trigger
        });
        Log.i(LOG_TAG, "Registered GUI Exception watchers");
    }

    private void onAnrDetected(String errorText) {
        mErrors.add(errorText);
    }

    private void onCrashDetected(String errorText) {
        mErrors.add(errorText);
    }

    /**
     * Current implementation ignores the exception and continues.
     */
    private void postHandler(String buttonText) {
        // TODO: Add custom error logging here
        String formatedOutput = String.format("UI Exception Message: %-20s\n", UiDevice
                .getInstance().getCurrentPackageName());
        Log.e(LOG_TAG, formatedOutput);
        UiObject buttonOK = new UiObject(new UiSelector().text(buttonText).enabled(true));
        // sometimes it takes a while for the OK button to become enabled
        buttonOK.waitForExists(5000);
        try {
            buttonOK.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }
    }
}

public class AntiFeaturesListingView extends RecyclerView {

    private static final String TAG = "AntiFeaturesListingView";

    public AntiFeaturesListingView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public void setApp(final App app) {

        setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        setLayoutManager(layoutManager);

        swapAdapter(new RecyclerView.Adapter<AntiFeatureItemViewHolder>() {

            @NonNull
            @Override
            public AntiFeatureItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                View view = inflater.inflate(R.layout.listitem_antifeaturelisting, parent, false);
                return new AntiFeatureItemViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull AntiFeatureItemViewHolder holder, int position) {
                final String antiFeatureId = app.antiFeatures[position];
                Repository repo = FDroidApp.getRepoManager(getContext()).getRepository(app.repoId);
                if (repo == null) return;
                LocaleListCompat localeList = LocaleListCompat.getDefault();
                AntiFeature antiFeature = repo.getAntiFeatures().get(antiFeatureId);
                if (antiFeature == null) {
                    Log.w(TAG, "Anti-feature not defined in repo: " + antiFeatureId);
                    holder.antiFeatureText.setText(getAntiFeatureDescriptionText(getContext(), antiFeatureId));
                    Glide.with(getContext()).clear(holder.antiFeatureIcon);
                    holder.antiFeatureIcon.setImageResource(antiFeatureIcon(getContext(), antiFeatureId));
                } else {
                    // text
                    String desc = antiFeature.getDescription(localeList);
                    holder.antiFeatureText.setText(desc == null ?
                            getAntiFeatureDescriptionText(getContext(), antiFeatureId) : desc);
                    // icon
                    int fallbackIcon = antiFeatureIcon(getContext(), antiFeatureId);
                    app.loadWithGlide(getContext(), antiFeature.getIcon(localeList))
                            .fallback(fallbackIcon)
                            .error(fallbackIcon)
                            .into(holder.antiFeatureIcon);
                }
                // reason
                String reason = app.antiFeatureReasons.get(antiFeatureId);
                if (reason == null) {
                    holder.antiFeatureReason.setVisibility(View.GONE);
                } else {
                    holder.antiFeatureReason.setText(reason);
                    holder.antiFeatureReason.setVisibility(View.VISIBLE);
                }
                // click
                holder.entireView.setOnClickListener(v -> {
                    Intent i = new Intent(Intent.ACTION_VIEW);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_DOCUMENT);
                    i.setData(Uri.parse("https://f-droid.org/docs/Anti-Features#" + antiFeatureId));
                    getContext().startActivity(i);
                });
            }

            @Override
            public int getItemCount() {
                return app == null || app.antiFeatures == null ? 0 : app.antiFeatures.length;
            }
        }, false);
    }

    static class AntiFeatureItemViewHolder extends RecyclerView.ViewHolder {

        private final View entireView;
        private final ImageView antiFeatureIcon;
        private final TextView antiFeatureText;
        private final TextView antiFeatureReason;

        AntiFeatureItemViewHolder(View itemView) {
            super(itemView);
            entireView = itemView;
            antiFeatureIcon = itemView.findViewById(R.id.anti_feature_icon);
            antiFeatureText = itemView.findViewById(R.id.anti_feature_text);
            antiFeatureReason = itemView.findViewById(R.id.anti_feature_reason);
        }
    }

    private static String getAntiFeatureDescriptionText(Context context, String antiFeatureName) {
        if (antiFeatureName.equals(context.getString(R.string.antiads_key))) {
            return context.getString(R.string.antiadslist);
        } else if (antiFeatureName.equals(context.getString(R.string.antitrack_key))) {
            return context.getString(R.string.antitracklist);
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreenet_key))) {
            return context.getString(R.string.antinonfreenetlist);
        } else if (antiFeatureName.equals(context.getString(R.string.antitetherednet_key))) {
            return context.getString(R.string.antitetherednetlist);
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreead_key))) {
            return context.getString(R.string.antinonfreeadlist);
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreedep_key))) {
            return context.getString(R.string.antinonfreedeplist);
        } else if (antiFeatureName.equals(context.getString(R.string.antiupstreamnonfree_key))) {
            return context.getString(R.string.antiupstreamnonfreelist);
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreeassets_key))) {
            return context.getString(R.string.antinonfreeassetslist);
        } else if (antiFeatureName.equals(context.getString(R.string.antidisabledalgorithm_key))) {
            return context.getString(R.string.antidisabledalgorithmlist);
        } else if (antiFeatureName.equals(context.getString(R.string.antiknownvuln_key))) {
            return context.getString(R.string.antiknownvulnlist);
        } else if (antiFeatureName.equals(context.getString(R.string.antinosource_key))) {
            return context.getString(R.string.antinosourcesince);
        } else if (antiFeatureName.equals(context.getString(R.string.antinsfw_key))) {
            return context.getString(R.string.antinsfwlist);
        } else {
            return antiFeatureName;
        }
    }

    private static @DrawableRes int antiFeatureIcon(Context context, String antiFeatureName) {
        if (antiFeatureName.equals(context.getString(R.string.antiads_key))) {
            return R.drawable.ic_antifeature_ads;
        } else if (antiFeatureName.equals(context.getString(R.string.antitrack_key))) {
            return R.drawable.ic_antifeature_tracking;
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreenet_key))) {
            return R.drawable.ic_antifeature_nonfreenet;
        } else if (antiFeatureName.equals(context.getString(R.string.antitetherednet_key))) {
            return R.drawable.ic_antifeature_tetherednet;
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreead_key))) {
            return R.drawable.ic_antifeature_nonfreeadd;
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreedep_key))) {
            return R.drawable.ic_antifeature_nonfreedep;
        } else if (antiFeatureName.equals(context.getString(R.string.antiupstreamnonfree_key))) {
            return R.drawable.ic_antifeature_upstreamnonfree;
        } else if (antiFeatureName.equals(context.getString(R.string.antinonfreeassets_key))) {
            return R.drawable.ic_antifeature_nonfreeassets;
        } else if (antiFeatureName.equals(context.getString(R.string.antidisabledalgorithm_key))) {
            return R.drawable.ic_antifeature_disabledalgorithm;
        } else if (antiFeatureName.equals(context.getString(R.string.antiknownvuln_key))) {
            return R.drawable.ic_antifeature_knownvuln;
        } else if (antiFeatureName.equals(context.getString(R.string.antinosource_key))) {
            return R.drawable.ic_antifeature_nosourcesince;
        } else if (antiFeatureName.equals(context.getString(R.string.antinsfw_key))) {
            return R.drawable.ic_antifeature_nsfw;
        } else {
            return R.drawable.ic_cancel;
        }
    }
}

public class DownloadFragment extends BasePreferenceFragment {

    public static final String KEY_DOWNLOAD_LOCATION = "download_location";

    @Nullable
    private Preference mDownloadLocation;

    ActivityResultLauncher<Uri> pickImageDirLauncher = registerForActivityResult(
            new ActivityResultContracts.OpenDocumentTree(),
            treeUri -> {
                if (treeUri != null) {
                    requireActivity().getContentResolver().takePersistableUriPermission(
                            treeUri, Intent.FLAG_GRANT_READ_URI_PERMISSION | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    UniFile uniFile = UniFile.fromTreeUri(getActivity(), treeUri);
                    if (uniFile != null) {
                        Settings.putDownloadLocation(uniFile);
                        onUpdateDownloadLocation();
                    } else {
                        showTip(R.string.settings_download_cant_get_download_location,
                                BaseScene.LENGTH_SHORT);
                    }
                }
            });

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.download_settings);

        Preference mediaScan = findPreference(Settings.KEY_MEDIA_SCAN);
        Preference imageResolution = findPreference(Settings.KEY_IMAGE_RESOLUTION);
        mDownloadLocation = findPreference(KEY_DOWNLOAD_LOCATION);

        onUpdateDownloadLocation();

        mediaScan.setOnPreferenceChangeListener(this);
        imageResolution.setOnPreferenceChangeListener(this);

        if (mDownloadLocation != null) {
            mDownloadLocation.setOnPreferenceClickListener(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mDownloadLocation = null;
    }

    public void onUpdateDownloadLocation() {
        UniFile file = Settings.getDownloadLocation();
        if (mDownloadLocation != null) {
            if (file != null) {
                mDownloadLocation.setSummary(file.getUri().toString());
            } else {
                mDownloadLocation.setSummary(R.string.settings_download_invalid_download_location);
            }
        }
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();
        if (KEY_DOWNLOAD_LOCATION.equals(key)) {
            UniFile file = Settings.getDownloadLocation();
            if (file != null && !UniFile.isFileUri(Settings.getDownloadLocation().getUri())) {
                new AlertDialog.Builder(requireContext())
                        .setTitle(R.string.settings_download_download_location)
                        .setMessage(file.getUri().toString())
                        .setPositiveButton(R.string.pick_new_download_location, (dialogInterface, i) -> openDirPickerL())
                        .setNeutralButton(R.string.reset_download_location, (dialogInterface, i) -> {
                            UniFile uniFile = UniFile.fromFile(AppConfig.getDefaultDownloadDir());
                            if (uniFile != null) {
                                Settings.putDownloadLocation(uniFile);
                                onUpdateDownloadLocation();
                            } else {
                                showTip(R.string.settings_download_cant_get_download_location,
                                        BaseScene.LENGTH_SHORT);
                            }
                        })
                        .show();
            } else {
                openDirPickerL();
            }
            return true;
        }
        return false;
    }

    private void openDirPickerL() {
        try {
            pickImageDirLauncher.launch(null);
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            showTip(R.string.error_cant_find_activity, BaseScene.LENGTH_SHORT);
        }
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        if (Settings.KEY_MEDIA_SCAN.equals(key)) {
            if (newValue instanceof Boolean) {
                UniFile downloadLocation = Settings.getDownloadLocation();
                if ((Boolean) newValue) {
                    CommonOperations.removeNoMediaFile(downloadLocation);
                } else {
                    CommonOperations.ensureNoMediaFile(downloadLocation);
                }
            }
            return true;
        } else if (Settings.KEY_IMAGE_RESOLUTION.equals(key)) {
            if (newValue instanceof String) {
                Settings.putImageResolution((String) newValue);
            }
            return true;
        }
        return false;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.settings_download;
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
            // ruleid:java_inject_rule-SqlInjection
            BasePeer.executeQuery(new StringBuilder(input).toString());
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SqlInjection
        stmt.executeUpdate("update from users set email = '" + input +"' where name != NULL");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class X509TrustManagerTest {

    class TrustAllManager implements X509TrustManager {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LoggedTrustAllManager implements X509TrustManager {

        Logger logger = LogManager.getLogger(LoggedTrustAllManager.class);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            if(issuerRequestsCount < 5){
                issuerRequestsCount++;
                return null;
            }
            return certificates;
        }
    }

    class FaultyLimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            X509Certificate[] a = certificates;
            return a;
        }
    }

    class LimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;
        
        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return certificates;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return certificates;
        }
    }

    class FaultyLimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ruleid: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return null;
        }
    }

    class FaultyLimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


    class FaultyLimitedManager4 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/crypto/use-of-rc4.java

import javax.crypto.Cipher;

class RC4 {
    String algo2 = "RC4";

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() throws Exception {
        String algo = "RC4";
        // ruleid: java_crypto_rule-UseOfRC4
        Cipher.getInstance(algo);
    }

    public void bad3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad4() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void ok() {
        // ok: java_crypto_rule-UseOfRC4
        Cipher.getInstance("AES/CBC/PKCS7PADDING");
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/xxe/saxparserfactory-disallow-doctype-decl-missing.java

import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.ParserConfigurationException;

class GoodSAXParserFactory {
    public void GoodSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

    public void GoodSAXParserFactory2() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

    public void GoodSAXParserFactory3() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

    public void GoodSAXParserFactory4() throws ParserConfigurationException {
        SAXParserFactory factory = XmlUtils.getSecureSAXParserFactory();
        // Deep semgrep could find issues like this
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        saxparser = factory.newSAXParser();
    }
}

class BadSAXParserFactory {
    public void BadSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void BadSAXParserFactory2() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        spf.setFeature("somethingElse", true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXParserFactoryStatic {

    private static SAXParserFactory spf = SAXParserFactory.newInstance();

    static {
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

}

class BadSAXParserFactoryStatic {

    private static SAXParserFactory spf = SAXParserFactory.newInstance();

    static {
        spf.setFeature("not-a-secure-feature", true);
    }

    public void doSomething() {
        // ruleid:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

}

class OneMoreGoodSAXParserFactory {

    public void GoodSAXParserFactory(boolean condition) throws ParserConfigurationException {
        SAXParserFactory spf = null;

        if (condition) {
            spf = SAXParserFactor.newInstance();
        } else {
            spf = newFactory();
        }
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

    private SAXParserFactory newFactory() {
        return SAXParserFactory.newInstance();
    }

}

class OneMoreBadSAXParserFactory {

    public void GoodSAXParserFactory(boolean condition) throws ParserConfigurationException {
        SAXParserFactory spf = null;

        if (condition) {
            spf = SAXParserFactory.newInstance();
        } else {
            spf = newFactory();
        }
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    private SAXParserFactory newFactory() {
        return SAXParserFactory.newInstance();
    }

}

class GoodSAXParserFactoryCtr {

    private final SAXParserFactory spf;

    public GoodSAXParserFactoryCtr() throws Exception {
        spf = SAXParserFactory.newInstance();
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }
}

class GoodSAXParserFactoryCtr2 {
    public void somemethod() throws Exception {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        setFeatures(spf);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

    private void setFeatures(SAXParserFactory spf) throws Exception {
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    }

}

class GoodSAXParserFactoryCtr3 {
    public void somemethod() throws Exception {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        setFeatures(spf);
        // ok:java_xxe_rule-SAXParserFactoryDisallowDoctypeDeclMissing
        spf.newSAXParser();
    }

    private void setFeatures(SAXParserFactory spf) throws Exception {
        spf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class AllHosts implements HostnameVerifier {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

class LoggedHosts implements HostnameVerifier {

    Logger logger = LogManager.getLogger(HostnameVerifier.class);

    // ruleid: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        logger.info("All hosts are permitted");
        return true;
    }
}

class SomeHosts implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        if("192.187.1.1".equals(hostname))
            return true;
        return hostname.startsWith("192.168");
    }
}

class SomeHosts2 implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    public boolean verify(final String hostname, final SSLSession session) {
        if("192.186.1.1".equals(hostname))
            return false;
        return true;
    }
}

class TrustedHosts implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        if (!hostname.equals("javaxservlet-trusted-server-1"))
            return false;
        return true;
    }
}


class TrustedHosts2 implements HostnameVerifier {

    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        boolean a = false;
        return a;
    }
}

class TrustedHosts3 implements HostnameVerifier {
    // ok: java_endpoint_rule-HostnameVerifier
    @Override
    public boolean verify(final String hostname, final SSLSession session) {
        if (!hostname.equals("javaxservlet-trusted-server-1"))
            return true;
        if (!hostname.equals("javaxservlet-trusted-server-2"))
            return false;
        if (!hostname.equals("javaxservlet-trusted-server-3"))
            return true;
        return true;
    }
}

class FaultyTrustedHosts implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

class FaultyTrustedHosts2 implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}


class FaultyTrustedHosts3 implements HostnameVerifier {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

class Bad {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void badsocket1() {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void badsocket2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket4() throws Exception {
        String servername = "telnet://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket5() throws Exception {
        String servername = "ftp://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket6() throws Exception {
        String servername = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket7() throws Exception {
        String servername = "TELNET://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket8() throws Exception {
        String servername = "FTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket9() throws Exception {
        String servername = "HTTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket10() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        pingSocket.close();
    }

    public void badsocket11() throws Exception {
        BufferedReader in = null;
        Socket pingSocket = null;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        String serverResponse;
        while ((serverResponse = in.readLine()) != null) {
            System.out.println("Server: " + serverResponse);
        }

        System.out.println(in.readLine());
        in.close();
        pingSocket.close();
    }

    public void badsocket12() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket13() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket14() throws Exception {
        // ruleid: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("example.com", 80);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket15() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket16() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

class Ok {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void oksocket1() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ssh://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket2() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("sftp://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket3() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("https://example.com", 443);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket4() throws Exception {
        String servername = "ssh://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket5() throws Exception {
        String servername = "sftp://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket6() throws Exception {
        String servername = "https://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/file/FileDisclosure.java
// hash: a7694d0

package injection;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

// java_inject_rule-FileDisclosureRequestDispatcher
public class FileDisclosureRequestDispatcher extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String jspFile = request.getParameter("jspFile");

            RequestDispatcher requestDispatcher = request.getRequestDispatcher(jspFile);

            // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
            requestDispatcher.include(request, response);

            requestDispatcher = request.getSession().getServletContext().getRequestDispatcher(jspFile);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            // Create a look up table or pull from a data source
            HashMap<String, String> lookupTable = new HashMap<>();
            lookupTable.put("key1", "/ServletSample/simpleForm.jsp");
            // Get user input
            String userInput = request.getParameter("key");
            // Look up resource to redirect to from the user input
            String redirectValue = lookupTable.getOrDefault(userInput, "/ServletSample/index.html");
            // Redirect the user
            // ok: java_inject_rule-FileDisclosureRequestDispatcher
            response.sendRedirect(redirectValue);

            RequestDispatcher requestDispatcherTwo = request.getSession().getServletContext().getRequestDispatcher(redirectValue);
            // ok: java_inject_rule-FileDisclosureRequestDispatcher
            requestDispatcherTwo.forward(request, response);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

class XMLStreamRdr {

    public void parseXMLSafe1(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe2(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe3(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe4(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe5(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe6(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe7(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe1(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        // ruleid: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe2(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe3(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe4(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

class Bad {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void badsocket1() {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void badsocket2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket4() throws Exception {
        String servername = "telnet://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket5() throws Exception {
        String servername = "ftp://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket6() throws Exception {
        String servername = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket7() throws Exception {
        String servername = "TELNET://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket8() throws Exception {
        String servername = "FTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket9() throws Exception {
        String servername = "HTTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket10() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        pingSocket.close();
    }

    public void badsocket11() throws Exception {
        BufferedReader in = null;
        Socket pingSocket = null;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        String serverResponse;
        while ((serverResponse = in.readLine()) != null) {
            System.out.println("Server: " + serverResponse);
        }

        System.out.println(in.readLine());
        in.close();
        pingSocket.close();
    }

    public void badsocket12() throws Exception {

        // ruleid: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("example.com", 23);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket13() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket14() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket15() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket16() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

class Ok {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void oksocket1() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ssh://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket2() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("sftp://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket3() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("https://example.com", 443);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket4() throws Exception {
        String servername = "ssh://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket5() throws Exception {
        String servername = "sftp://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket6() throws Exception {
        String servername = "https://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import java.util.List;
import java.io.*;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class FileUploadFileName {

    public void handleFileCommon(javax.servlet.http.HttpServletRequest req) throws FileUploadException {
        ServletFileUpload upload = new ServletFileUpload(new DiskFileItemFactory());
        List<FileItem> fileItems = upload.parseRequest(req);

        for (FileItem item : fileItems) {
            // ruleid: java_file_rule-FileUploadFileName
            System.out.println("Saving " + item.getName() + "...");
        }
    }

}

@javax.servlet.annotation.WebServlet("/upload")
@javax.servlet.annotation.MultipartConfig
class SimpleFileUploadServlet extends javax.servlet.http.HttpServlet {

    protected void doPost(
            javax.servlet.http.HttpServletRequest request,
            javax.servlet.http.HttpServletResponse response)
            throws ServletException, IOException {

        javax.servlet.http.Part filePart = request.getPart("file");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Define the path where you want to save the file
        String uploadPath = getServletContext().getRealPath("") + File.separator + "uploads";
        File uploadDir = new File(uploadPath);
        if (!uploadDir.exists())
            uploadDir.mkdir();

        // Write the file to the server
        try (InputStream fileContent = filePart.getInputStream();
                FileOutputStream fos = new FileOutputStream(uploadPath + File.separator + fileName)) {
            byte[] buffer = new byte[1024];
            int bytesRead;
            while ((bytesRead = fileContent.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
            }
        }
    }
}

@jakarta.servlet.annotation.WebServlet("/upload2")
@jakarta.servlet.annotation.MultipartConfig
class FileUploadServlet extends jakarta.servlet.http.HttpServlet {

    protected void doPost(
            jakarta.servlet.http.HttpServletRequest request,
            jakarta.servlet.http.HttpServletResponse response) throws IOException, jakarta.servlet.ServletException {
        jakarta.servlet.http.Part filePart = request.getPart("file");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        File file = new File("/path/to/uploads", fileName);
        try (InputStream fileContent = filePart.getInputStream()) {
            Files.copy(fileContent, file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        Files.createTempDirectory(p,input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import javax.net.ssl.SSLContext;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.NoSuchProviderException;

// ref: java_crypto_rule-WeakTLSProtocol-SSLContext
public class WeakTLSProtocolSSLContext {

    public static void main(String[] args) {
        try {
            // ruleid: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("SSL"); // BAD
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("TLS"); // BAD
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("TLSv1.2"); // OK
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("TLSv1.3"); // OK

            // Testing getInstance(String protocol, String provider)
            try{
                // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
                SSLContext sslContext2 = SSLContext.getInstance("TLSv1.3", "SunJSSE");
            }catch (NoSuchProviderException e) {
                e.printStackTrace();
            }

            // Testing getInstance(String protocol, Provider provider)
            Provider provider = Security.getProvider("SunJSSE");
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext sslContext3 = SSLContext.getInstance("TLSv1.3", provider);

            try{
                // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
                SSLContext sslContext4 = SSLContext.getInstance("TLS", "SunJSSE");
            }catch (NoSuchProviderException e) {
                e.printStackTrace();
            }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid:java_inject_rule-DangerousGroovyShell
        shell.evaluate(new URI(uri));
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.io.FilenameUtils.*;

@WebServlet(name = "FilenameUtils", value = "/FilenameUtils")
public class FilenameUtils extends HttpServlet {

    final static String basePath = "/usr/local/lib";

    //Method where input is not sanitized with normalize:

    //Normal Input (opens article as intended):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (returns restricted file):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Input from From Servlet Param
        String input = req.getHeader("input");

        String method = req.getHeader("method");

        String fullPath = null;

        if(method != null && !method.isEmpty()) {
            switch (method) {
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPath'
                case "getFullPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPathNoEndSeparator'
                case "getFullPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPath'
                case "getPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPathNoEndSeparator'
                case "getPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalize'
                case "normalize": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalizeNoEndSeparator'
                case "normalizeNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: removeExtension'
                case "removeExtension": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToSystem'
                case "separatorsToSystem": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToUnix'
                case "separatorsToUnix": {
                    //ruleid: java_file_rule-FilenameUtils
                    fullPath = separatorsToUnix(input);
                    break;
                }
                case "concat": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                default: {
                    input = "article.txt";
                    // ok: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }

            }
        }

        // Remove whitespaces
        fullPath = fullPath.replace(" ","");

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method not using user input
    //curl --location --request POST 'http://localhost:8080/ServletSample/FilenameUtils'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String filepath = "article.txt";

        //ok: java_file_rule-FilenameUtils
        String fullPath = concat(basePath, filepath);

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method where input is sanitized with getName:

    //Normal Input (opens article as intended):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (causes exception):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String input = req.getHeader("input");

        // Securely combine the user input with the base directory
        input = getName(input); // Extracts just the filename, no paths
        // Construct the safe, absolute path
        //ok: java_file_rule-FilenameUtils
        String safePath = concat(basePath, input);

        System.out.println(safePath);

        // Read the contents of the file
        File file = new File(safePath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
 
    protected void safe(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        
        String input = req.getHeader("input");

        String base = "/var/app/restricted";
        Path basePath = Paths.get(base);
        // ok: java_file_rule-FilenameUtils
        String inputPath = getPath(input);
        // Resolve the full path, but only use our random generated filename
        Path fullPath = basePath.resolve(inputPath);
        // verify the path is contained within our basePath
        if (!fullPath.startsWith(base)) {
            throw new Exception("Invalid path specified!");
        }
        
        // Read the contents of the file
        File file = new File(fullPath.toString());
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package strings;
import java.util.Formatter;
import java.util.Locale;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;

public class FormatStringManipulation extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

        // create a new formatter
        StringBuffer buffer = new StringBuffer();
        Formatter formatter = new Formatter(buffer, Locale.US);
        String input = request.getParameter("suffix");
        String format = "The customer: %s %s" + input;

        //test cases
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_strings_rule-FormatStringManipulation
        formatter.format(Locale.US, format, "John", "Smith"); //BAD
        //false positive test
        formatter.format("The customer: %s %s", "John", request.getParameter("testParam")); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String format2 = "The customer: %s %s" + request.getParameter("suffix");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/file/FileDisclosure.java
// hash: a7694d0

package injection;

import java.io.IOException;
import org.apache.struts.action.ActionForward;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

// REQUESTDISPATCHER_FILE_DISCLOSURE
public class FileDisclosureSpringFramework extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String returnURL = request.getParameter("returnURL");

            /******Spring ModelAndView vulnerable code tests******/
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            // ruleid: java_inject_rule-FileDisclosureSpringFramework
            ModelAndView mv3 = new ModelAndView(returnURL, "modelName", new Object()); //BAD

            ModelAndView mv4 = new ModelAndView();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            // ok: java_inject_rule-FileDisclosureSpringFramework
            ModelAndView mv5 = new ModelAndView("viewName", returnURL, new Object()); //OK

            // Create a look up table or pull from a data source
            HashMap<String, String> lookupTable = new HashMap<>();
            lookupTable.put("key1", "/ServletSample/simpleForm.jsp");
            // Look up resource to redirect to from the user input
            String redirectValue = lookupTable.getOrDefault(returnURL, "/ServletSample/index.html");
            ModelAndView mv6 = new ModelAndView();
            // ok: java_inject_rule-FileDisclosureSpringFramework
            mv6.setViewName(redirectValue);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package xss;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Radio;
//import org.apache.wicket.extensions.markup.html.form.select.SelectOptions.SimpleSelectOption;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.model.util.ListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;

class WicketXSS extends WebPage {
    public void XssWicketExamplePage(PageParameters pageParameters) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // http://www.java2s.com/example/java-src/pkg/org/jabylon/rest/ui/wicket/components/customfeedbackpanel-edea7.html
    protected Component newMessageDisplayComponent(String id, String message) {
        MultiLineLabel label = new MultiLineLabel(id, message);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return label;
    }

    public void setText(String txt, Component c) {

        if (txt == null || txt.trim().length() == 0) {
            c.setVisible(false);
        } else {
            c.setDefaultModelObject(txt);
            c.setVisible(true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
    }

    protected void newLabel(Label lab) {
        final Component label = lab;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // https://github.com/apache/wicket/blob/e9461b0d115a7dbf4992596823521f6e038817d9/wicket-user-guide/src/main/asciidoc/forms2/forms2_3.adoc#L134
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void advancedSearchPage(String input) {

        Form<?> form = new Form<>("form");
        // Options for the dropdown
        List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

        // Model for the selected value
        Model<String> selectedOption = new Model<>("Option 1 " + input);

        // Create a DropDownChoice and add it to the form
        DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        form.add(dropdown);

        add(form);

    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Radio/setEscapeModelStrings
    private String getFormComponentValue(final Radio<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Check/setEscapeModelStrings
    private String getFormComponentValue2(final Check<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
        // ruleid: java_xss_rule-WicketXSS
        formComponent.setEscapeModelStrings(false);
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }
}

class MyPages extends WebPage {

    private ModalWindow modalWindow;

    public MyPages() {
        // Create a ModalWindow and set its title
        modalWindow = new ModalWindow("modalWindow");
        modalWindow.setTitle("Potentially Unsafe Title");

        // Explicitly disable escaping of the model strings (title)
        // for CVE-2015-5347/
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        add(modalWindow);
    }
}

class MyPage extends WebPage {

    public MyPage(List<String> userGeneratedOptions, String input) {

        Form<Void> form = new Form<>("form");

        Model<String> selectedOption = Model.of("");

        RadioGroup<String> radioGroup = new RadioGroup<>("radioGroup", selectedOption);
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        for (String option : userGeneratedOptions) {
            radioGroup.add(new Radio<>("radio" + option, Model.of(option)));
        }

        form.add(radioGroup);

        // https://nightlies.apache.org/wicket/apidocs/8.x/org/apache/wicket/markup/html/form/CheckBoxMultipleChoice.html
        List SITES = Arrays.asList(new String[] { "The Server Side", "Java Lobby", input });
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URL;

class Bad {
    public static void sendbad1() throws IOException {
        // ruleid: java_crypto_rule-HTTPUrlConnectionHTTPRequest
        URL obj = new URL("http://example.com");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        con.connect();
    }

    public static void sendbad2() throws IOException {
        String url = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        con.connect();
    }

    public static void sendbad3() throws IOException {
        String url = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        URLConnection urlCon = urlObj.openConnection();
        InputStream inputStream = urlCon.getInputStream();
    }

    public static void sendbad4() throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        URLConnection urlCon = urlObj.openConnection();
        InputStream inputStream = urlCon.getInputStream();
    }
}

class Ok {
    public static void sendok1() throws IOException {
        // ok: java_crypto_rule-HTTPUrlConnectionHTTPRequest
        URL obj = new URL("https://example.com");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        con.connect();
    }

    public static void sendok2() throws IOException {
        String url = "https://example.com";
        // ok: java_crypto_rule-HTTPUrlConnectionHTTPRequest
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        int responseCode = con.getResponseCode();
        con.connect();
    }

    public static void sendok3() throws IOException {
        String url = "https://example.com";
        // ok: java_crypto_rule-HTTPUrlConnectionHTTPRequest
        URL urlObj = new URL(url);
        URLConnection urlCon = urlObj.openConnection();
        InputStream inputStream = urlCon.getInputStream();
    }

    public static void sendok4() throws IOException {
        // ok: java_crypto_rule-HTTPUrlConnectionHTTPRequest
        URL urlObj = new URL("https://example.com");
        URLConnection urlCon = urlObj.openConnection();
        InputStream inputStream = urlCon.getInputStream();
    }
}

// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/webview/webview_file_access.java
// hash: e29e85c3

class Webview extends WebViewClient {
        public void vulnerableMethodSetAllowFileAccess() {
                WebView web = (WebView) findViewById(R.id.webview);
        }
        public void vulnerableMethodSetAllowFileAccess2() {
                WebView web = (WebView) findViewById(R.id.webview);
                WebSettings webSettings = web.getSettings();
                webSettings.setWebContentsDebuggingEnabled(true);
                // ruleid:rules_lgpl_java_webview_rule-webview-set-allow-file-access
                webSettings.setAllowFileAccess(true);
        }

         public void vulnerableMethodSetAllowFileAccess1() {
                boolean fileAccess = true;
                WebView web = (WebView) findViewById(R.id.webview);
                WebSettings webSettings = web.getSettings();
                webSettings.setWebContentsDebuggingEnabled(true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }

          public void vulnerableMethodSetAllowFileAccessok() {
                boolean fileAccess = false;
                WebView web = (WebView) findViewById(R.id.webview);
                WebSettings webSettings = web.getSettings();
                webSettings.setWebContentsDebuggingEnabled(true);
                // ok:rules_lgpl_java_webview_rule-webview-set-allow-file-access
                webSettings.setAllowFileAccess(fileAccess);
        }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/tls-renegotiation.java

class Bad {
    public void bad1() {
        // ruleid: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", true);
    }

    public void bad2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class Ok {
    public void ok1() {
        // ok: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", false);
    }

    public void ok2() {
        // ok: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "false");
    }

    public void ok3() {
        // ok: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", Boolean.FALSE);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

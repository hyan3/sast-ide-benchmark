public final class GalleryCommentsScene extends ToolbarScene
        implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    public static final String TAG = GalleryCommentsScene.class.getSimpleName();

    public static final String KEY_API_UID = "api_uid";
    public static final String KEY_API_KEY = "api_key";
    public static final String KEY_GID = "gid";
    public static final String KEY_TOKEN = "token";
    public static final String KEY_COMMENT_LIST = "comment_list";
    public static final String KEY_GALLERY_DETAIL = "gallery_detail";
    private static final int TYPE_COMMENT = 0;
    private static final int TYPE_MORE = 1;
    private static final int TYPE_PROGRESS = 2;
    private GalleryDetail mGalleryDetail;
    @Nullable
    private EasyRecyclerView mRecyclerView;
    @Nullable
    private FabLayout mFabLayout;
    @Nullable
    private FloatingActionButton mFab;
    @Nullable
    private View mEditPanel;
    @Nullable
    private ImageView mSendImage;
    @Nullable
    private EditText mEditText;
    @Nullable
    private CommentAdapter mAdapter;
    @Nullable
    private ViewTransition mViewTransition;
    private SwipeRefreshLayout mRefreshLayout;
    private Drawable mSendDrawable;
    private Drawable mPencilDrawable;
    private long mCommentId;
    private boolean mInAnimation = false;
    private boolean mShowAllComments = false;
    private boolean mRefreshingComments = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState == null) {
            onInit();
        } else {
            onRestore(savedInstanceState);
        }
    }

    private void handleArgs(Bundle args) {
        if (args == null) {
            return;
        }

        mGalleryDetail = args.getParcelable(KEY_GALLERY_DETAIL);
        mShowAllComments = mGalleryDetail != null && mGalleryDetail.comments != null && !mGalleryDetail.comments.hasMore;
    }

    private void onInit() {
        handleArgs(getArguments());
    }

    private void onRestore(@NonNull Bundle savedInstanceState) {
        mGalleryDetail = savedInstanceState.getParcelable(KEY_GALLERY_DETAIL);
        mShowAllComments = mGalleryDetail != null && mGalleryDetail.comments != null && !mGalleryDetail.comments.hasMore;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelable(KEY_GALLERY_DETAIL, mGalleryDetail);
    }

    @NonNull
    @Override
    public View onCreateViewWithToolbar(LayoutInflater inflater,
                                        @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.scene_gallery_comments, container, false);
        mRecyclerView = (EasyRecyclerView) ViewUtils.$$(view, R.id.recycler_view);
        RecyclerViewKt.fixEdgeEffect(mRecyclerView, false, true);
        TextView tip = (TextView) ViewUtils.$$(view, R.id.tip);
        mEditPanel = ViewUtils.$$(view, R.id.edit_panel);
        mSendImage = (ImageView) ViewUtils.$$(mEditPanel, R.id.send);
        mEditText = (EditText) ViewUtils.$$(mEditPanel, R.id.edit_text);
        mFabLayout = (FabLayout) ViewUtils.$$(view, R.id.fab_layout);
        mFab = (FloatingActionButton) ViewUtils.$$(view, R.id.fab);
        mRefreshLayout = (SwipeRefreshLayout) ViewUtils.$$(view, R.id.refresh_layout);
        ViewCompat.setWindowInsetsAnimationCallback(view, new WindowInsetsAnimationHelper(
                WindowInsetsAnimationCompat.Callback.DISPATCH_MODE_STOP,
                mEditPanel,
                mFabLayout
        ));

        mRefreshLayout.setColorSchemeResources(
                R.color.loading_indicator_red,
                R.color.loading_indicator_purple,
                R.color.loading_indicator_blue,
                R.color.loading_indicator_cyan,
                R.color.loading_indicator_green,
                R.color.loading_indicator_yellow);
        mRefreshLayout.setOnRefreshListener(this);

        Context context = requireContext();

        Drawable drawable = ContextCompat.getDrawable(context, R.drawable.big_sad_pandroid);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        tip.setCompoundDrawables(null, drawable, null, null);

        mSendDrawable = ContextCompat.getDrawable(context, R.drawable.v_send_dark_x24);
        mPencilDrawable = ContextCompat.getDrawable(context, R.drawable.v_pencil_dark_x24);

        mAdapter = new CommentAdapter();
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context,
                RecyclerView.VERTICAL, false));
        LinearDividerItemDecoration decoration = new LinearDividerItemDecoration(
                LinearDividerItemDecoration.VERTICAL, ResourcesKt.resolveColor(getTheme(), R.attr.dividerColor),
                LayoutUtils.dp2pix(context, 1));
        decoration.setShowLastDivider(true);
        mRecyclerView.addItemDecoration(decoration);
        mRecyclerView.setHasFixedSize(true);
        // Cancel change animator
        RecyclerView.ItemAnimator itemAnimator = mRecyclerView.getItemAnimator();
        if (itemAnimator instanceof DefaultItemAnimator) {
            ((DefaultItemAnimator) itemAnimator).setSupportsChangeAnimations(false);
        }
        RecyclerViewKt.addEdgeSpacing(mRecyclerView, 0,0,0,80, TypedValue.COMPLEX_UNIT_DIP);

        mSendImage.setOnClickListener(this);
        mFab.setOnClickListener(this);

        addAboveSnackView(mEditPanel);
        addAboveSnackView(mFabLayout);

        mViewTransition = new ViewTransition(mRecyclerView, tip);

        updateView(false);

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        if (null != mRecyclerView) {
            mRecyclerView.stopScroll();
            mRecyclerView = null;
        }
        if (null != mEditPanel) {
            removeAboveSnackView(mEditPanel);
            mEditPanel = null;
        }
        if (null != mFabLayout) {
            removeAboveSnackView(mFabLayout);
            mFabLayout = null;
        }

        mFab = null;
        mSendImage = null;
        mEditText = null;
        mAdapter = null;
        mViewTransition = null;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setTitle(R.string.gallery_comments);
        setNavigationIcon(R.drawable.v_arrow_left_dark_x24);
    }

    @Override
    public void onNavigationClick() {
        onBackPressed();
    }

    private void voteComment(long id, int vote) {
        Context context = getContext();
        MainActivity activity = getMainActivity();
        if (null == context || null == activity) {
            return;
        }

        EhRequest request = new EhRequest()
                .setMethod(EhClient.METHOD_VOTE_COMMENT)
                .setArgs(mGalleryDetail.apiUid, mGalleryDetail.apiKey, mGalleryDetail.gid, mGalleryDetail.token, id, vote)
                .setCallback(new VoteCommentListener(context,
                        activity.getStageId(), getTag()));
        EhApplication.getEhClient(context).execute(request);
    }

    @SuppressLint("InflateParams")
    public void showVoteStatusDialog(Context context, String voteStatus) {
        String[] temp = StringUtils.split(voteStatus, ',');
        final int length = temp.length;
        final String[] userArray = new String[length];
        final String[] voteArray = new String[length];
        for (int i = 0; i < length; i++) {
            String str = StringUtils.trim(temp[i]);
            int index = str.lastIndexOf(' ');
            if (index < 0) {
                Log.d(TAG, "Something wrong happened about vote state");
                userArray[i] = str;
                voteArray[i] = "";
            } else {
                userArray[i] = StringUtils.trim(str.substring(0, index));
                voteArray[i] = StringUtils.trim(str.substring(index + 1));
            }
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        context = builder.getContext();
        final LayoutInflater inflater = LayoutInflater.from(context);
        EasyRecyclerView rv = (EasyRecyclerView) inflater.inflate(R.layout.dialog_recycler_view, null);
        rv.setAdapter(new RecyclerView.Adapter<InfoHolder>() {
            @NonNull
            @Override
            public InfoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                return new InfoHolder(inflater.inflate(R.layout.item_drawer_favorites, parent, false));
            }

            @Override
            public void onBindViewHolder(@NonNull InfoHolder holder, int position) {
                holder.key.setText(userArray[position]);
                holder.value.setText(voteArray[position]);
            }

            @Override
            public int getItemCount() {
                return length;
            }
        });
        rv.setLayoutManager(new LinearLayoutManager(context));
        LinearDividerItemDecoration decoration = new LinearDividerItemDecoration(
                LinearDividerItemDecoration.VERTICAL, ResourcesKt.resolveColor(getTheme(), R.attr.dividerColor),
                LayoutUtils.dp2pix(context, 1));
        decoration.setPadding(ResourcesUtils.getAttrDimensionPixelOffset(context, androidx.appcompat.R.attr.dialogPreferredPadding));
        rv.addItemDecoration(decoration);
        rv.setClipToPadding(false);
        builder.setView(rv).show();
    }

    private void showCommentDialog(int position) {
        final Context context = getContext();
        if (context == null || mGalleryDetail == null || mGalleryDetail.comments == null || mGalleryDetail.comments.comments == null || position >= mGalleryDetail.comments.comments.length || position < 0) {
            return;
        }

        final GalleryComment comment = mGalleryDetail.comments.comments[position];
        List<String> menu = new ArrayList<>();
        final IntList menuId = new IntList();
        Resources resources = context.getResources();

        menu.add(resources.getString(R.string.copy_comment_text));
        menuId.add(R.id.copy);
        if (comment.editable) {
            menu.add(resources.getString(R.string.edit_comment));
            menuId.add(R.id.edit_comment);
        }
        if (comment.voteUpAble) {
            menu.add(resources.getString(comment.voteUpEd ? R.string.cancel_vote_up : R.string.vote_up));
            menuId.add(R.id.vote_up);
        }
        if (comment.voteDownAble) {
            menu.add(resources.getString(comment.voteDownEd ? R.string.cancel_vote_down : R.string.vote_down));
            menuId.add(R.id.vote_down);
        }
        if (!TextUtils.isEmpty(comment.voteState)) {
            menu.add(resources.getString(R.string.check_vote_status));
            menuId.add(R.id.check_vote_status);
        }

        new AlertDialog.Builder(context)
                .setItems(menu.toArray(new String[0]), (dialog, which) -> {
                    if (which < 0 || which >= menuId.size()) {
                        return;
                    }
                    int id = menuId.get(which);
                    if (id == R.id.copy) {
                        ClipboardUtil.addTextToClipboard(comment.comment);
                        showTip(R.string.copied_to_clipboard, LENGTH_SHORT);
                    } else if (id == R.id.vote_up) {
                        voteComment(comment.id, 1);
                    } else if (id == R.id.vote_down) {
                        voteComment(comment.id, -1);
                    } else if (id == R.id.check_vote_status) {
                        showVoteStatusDialog(context, comment.voteState);
                    } else if (id == R.id.edit_comment) {
                        prepareEditComment(comment.id);
                        if (!mInAnimation && mEditPanel != null && mEditPanel.getVisibility() != View.VISIBLE) {
                            showEditPanel(true);
                        }
                    }
                }).show();
    }

    public boolean onItemClick(EasyRecyclerView parent, View view, int position) {
        MainActivity activity = getMainActivity();
        if (null == activity) {
            return false;
        }

        RecyclerView.ViewHolder holder = parent.getChildViewHolder(view);
        if (holder instanceof ActualCommentHolder) {
            ActualCommentHolder commentHolder = (ActualCommentHolder) holder;
            ClickableSpan span = commentHolder.comment.getCurrentSpan();
            commentHolder.comment.clearCurrentSpan();

            if (span instanceof URLSpan) {
                UrlOpener.openUrl(activity, ((URLSpan) span).getURL(), true, mGalleryDetail);
            } else {
                showCommentDialog(position);
            }
        } else if (holder instanceof MoreCommentHolder && !mRefreshingComments && mAdapter != null) {
            mRefreshingComments = true;
            mShowAllComments = true;
            mAdapter.notifyItemChanged(position);

            String url = getGalleryDetailUrl();
            if (url != null) {
                // Request
                EhRequest request = new EhRequest()
                        .setMethod(EhClient.METHOD_GET_GALLERY_DETAIL)
                        .setArgs(url)
                        .setCallback(new RefreshCommentListener(activity, activity.getStageId(), getTag()));
                EhApplication.getEhClient(activity).execute(request);
            }
        }

        return true;
    }

    private void updateView(boolean animation) {
        if (null == mViewTransition) {
            return;
        }

        if (mGalleryDetail == null || mGalleryDetail.comments == null || mGalleryDetail.comments.comments == null || mGalleryDetail.comments.comments.length <= 0) {
            mViewTransition.showView(1, animation);
        } else {
            mViewTransition.showView(0, animation);
        }
    }

    private void prepareNewComment() {
        mCommentId = 0;
        if (mSendImage != null) {
            mSendImage.setImageDrawable(mSendDrawable);
        }
    }

    private void prepareEditComment(long commentId) {
        mCommentId = commentId;
        if (mSendImage != null) {
            mSendImage.setImageDrawable(mPencilDrawable);
        }
    }

    private void showEditPanelWithAnimation() {
        if (null == mFab || null == mEditPanel) {
            return;
        }

        mInAnimation = true;
        mFab.setTranslationX(0.0f);
        mFab.setTranslationY(0.0f);
        mFab.setScaleX(1.0f);
        mFab.setScaleY(1.0f);
        int fabEndX = mEditPanel.getLeft() + (mEditPanel.getWidth() / 2) - (mFab.getWidth() / 2);
        int fabEndY = mEditPanel.getTop() + (mEditPanel.getHeight() / 2) - (mFab.getHeight() / 2);
        mFab.animate().x(fabEndX).y(fabEndY).scaleX(0.0f).scaleY(0.0f)
                .setInterpolator(AnimationUtils.SLOW_FAST_SLOW_INTERPOLATOR)
                .setDuration(300L).setListener(new SimpleAnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (null == mFab || null == mEditPanel) {
                    return;
                }

                ((View) mFab).setVisibility(View.INVISIBLE);
                mEditPanel.setVisibility(View.VISIBLE);
                int halfW = mEditPanel.getWidth() / 2;
                int halfH = mEditPanel.getHeight() / 2;
                Animator animator = ViewAnimationUtils.createCircularReveal(mEditPanel, halfW, halfH, 0,
                        (float) Math.hypot(halfW, halfH)).setDuration(300L);
                animator.addListener(new SimpleAnimatorListener() {
                    @Override
                    public void onAnimationEnd(Animator a) {
                        mInAnimation = false;
                    }
                });
                animator.start();
            }
        }).start();
    }

    private void showEditPanel(boolean animation) {
        if (animation) {
            showEditPanelWithAnimation();
        } else {
            if (null == mFab || null == mEditPanel) {
                return;
            }

            ((View) mFab).setVisibility(View.INVISIBLE);
            mEditPanel.setVisibility(View.VISIBLE);
        }
    }

    private void hideEditPanelWithAnimation() {
        if (null == mFab || null == mEditPanel) {
            return;
        }

        mInAnimation = true;
        int halfW = mEditPanel.getWidth() / 2;
        int halfH = mEditPanel.getHeight() / 2;
        Animator animator = ViewAnimationUtils.createCircularReveal(mEditPanel, halfW, halfH,
                (float) Math.hypot(halfW, halfH), 0.0f).setDuration(300L);
        animator.addListener(new SimpleAnimatorListener() {
            @Override
            public void onAnimationEnd(Animator a) {
                if (null == mFab || null == mEditPanel) {
                    return;
                }

                if (Looper.myLooper() != Looper.getMainLooper()) {
                    // Some devices may run this block in non-UI thread.
                    // It might be a bug of Android OS.
                    // Check it here to avoid crash.
                    return;
                }

                mEditPanel.setVisibility(View.GONE);
                ((View) mFab).setVisibility(View.VISIBLE);
                int fabStartX = mEditPanel.getLeft() + (mEditPanel.getWidth() / 2) - (mFab.getWidth() / 2);
                int fabStartY = mEditPanel.getTop() + (mEditPanel.getHeight() / 2) - (mFab.getHeight() / 2);
                mFab.setX(fabStartX);
                mFab.setY(fabStartY);
                mFab.setScaleX(0.0f);
                mFab.setScaleY(0.0f);
                mFab.setRotation(-45.0f);
                mFab.animate().translationX(0.0f).translationY(0.0f).scaleX(1.0f).scaleY(1.0f).rotation(0.0f)
                        .setInterpolator(AnimationUtils.SLOW_FAST_SLOW_INTERPOLATOR)
                        .setDuration(300L).setListener(new SimpleAnimatorListener() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        mInAnimation = false;
                    }
                }).start();
            }
        });
        animator.start();
    }

    private void hideEditPanel(boolean animation) {
        hideSoftInput();
        if (animation) {
            hideEditPanelWithAnimation();
        } else {
            if (null == mFab || null == mEditPanel) {
                return;
            }

            ((View) mFab).setVisibility(View.VISIBLE);
            mEditPanel.setVisibility(View.INVISIBLE);
        }
    }

    @Nullable
    private String getGalleryDetailUrl() {
        if (mGalleryDetail != null && mGalleryDetail.gid != -1 && mGalleryDetail.token != null) {
            return EhUrl.getGalleryDetailUrl(mGalleryDetail.gid, mGalleryDetail.token, 0, mShowAllComments);
        } else {
            return null;
        }
    }

    @Override
    public void onClick(View v) {
        Context context = getContext();
        MainActivity activity = getMainActivity();
        if (null == context || null == activity || null == mEditText) {
            return;
        }

        if (mFab == v) {
            if (!mInAnimation) {
                prepareNewComment();
                showEditPanel(true);
            }
        } else if (mSendImage == v) {
            if (!mInAnimation) {
                String comment = mEditText.getText().toString();
                if (TextUtils.isEmpty(comment)) {
                    // Comment is empty
                    return;
                }
                String url = getGalleryDetailUrl();
                if (url == null) {
                    return;
                }
                // Request
                EhRequest request = new EhRequest()
                        .setMethod(EhClient.METHOD_GET_COMMENT_GALLERY)
                        .setArgs(url, comment, mCommentId != 0 ? Long.toString(mCommentId) : null)
                        .setCallback(new CommentGalleryListener(context,
                                activity.getStageId(), getTag(), mCommentId));
                EhApplication.getEhClient(context).execute(request);
                hideSoftInput();
                hideEditPanel(true);
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mInAnimation) {
            return;
        }
        if (null != mEditPanel && mEditPanel.getVisibility() == View.VISIBLE) {
            hideEditPanel(true);
        } else {
            finish();
        }
    }

    private void onRefreshGallerySuccess(GalleryCommentList result) {
        if (mGalleryDetail == null || mAdapter == null) {
            return;
        }

        mRefreshLayout.setRefreshing(false);
        mRefreshingComments = false;
        mGalleryDetail.comments = result;
        mAdapter.notifyDataSetChanged();

        updateView(true);

        Bundle re = new Bundle();
        re.putParcelable(KEY_COMMENT_LIST, result);
        setResult(SceneFragment.RESULT_OK, re);
    }

    private void onRefreshGalleryFailure() {
        if (mAdapter == null) {
            return;
        }

        mRefreshLayout.setRefreshing(false);
        mRefreshingComments = false;
        int position = mAdapter.getItemCount() - 1;
        if (position >= 0) {
            mAdapter.notifyItemChanged(position);
        }
    }

    private void onCommentGallerySuccess(GalleryCommentList result) {
        if (mGalleryDetail == null || mAdapter == null) {
            return;
        }

        mGalleryDetail.comments = result;
        mAdapter.notifyDataSetChanged();
        Bundle re = new Bundle();
        re.putParcelable(KEY_COMMENT_LIST, result);
        setResult(SceneFragment.RESULT_OK, re);

        // Remove text
        if (mEditText != null) {
            mEditText.setText("");
        }

        updateView(true);
    }

    private void onVoteCommentSuccess(VoteCommentParser.Result result) {
        if (mAdapter == null || mGalleryDetail.comments == null || mGalleryDetail.comments.comments == null) {
            return;
        }

        int position = -1;
        for (int i = 0, n = mGalleryDetail.comments.comments.length; i < n; i++) {
            GalleryComment comment = mGalleryDetail.comments.comments[i];
            if (comment.id == result.id) {
                position = i;
                break;
            }
        }

        if (-1 == position) {
            Log.d(TAG, "Can't find comment with id " + result.id);
            return;
        }

        // Update comment
        GalleryComment comment = mGalleryDetail.comments.comments[position];
        comment.score = result.score;
        if (result.expectVote > 0) {
            comment.voteUpEd = 0 != result.vote;
            comment.voteDownEd = false;
        } else {
            comment.voteDownEd = 0 != result.vote;
            comment.voteUpEd = false;
        }

        mAdapter.notifyItemChanged(position);

        Bundle re = new Bundle();
        re.putParcelable(KEY_COMMENT_LIST, mGalleryDetail.comments);
        setResult(SceneFragment.RESULT_OK, re);
    }

    @Override
    public void onRefresh() {
        if (!mRefreshingComments && mAdapter != null) {
            MainActivity activity = (MainActivity) requireActivity();
            mRefreshingComments = true;

            String url = getGalleryDetailUrl();
            if (url != null) {
                // Request
                EhRequest request = new EhRequest()
                        .setMethod(EhClient.METHOD_GET_GALLERY_DETAIL)
                        .setArgs(url)
                        .setCallback(new RefreshCommentListener(activity, activity.getStageId(), getTag()));
                EhApplication.getEhClient(activity).execute(request);
            }
        }
    }

    private static class RefreshCommentListener extends EhCallback<GalleryCommentsScene, GalleryDetail> {

        public RefreshCommentListener(Context context, int stageId, String sceneTag) {
            super(context, stageId, sceneTag);
        }

        @Override
        public void onSuccess(GalleryDetail result) {
            GalleryCommentsScene scene = getScene();
            if (scene != null) {
                scene.onRefreshGallerySuccess(result.comments);
            }
        }

        @Override
        public void onFailure(Exception e) {
            GalleryCommentsScene scene = getScene();
            if (scene != null) {
                scene.onRefreshGalleryFailure();
            }
        }

        @Override
        public void onCancel() {
        }

        @Override
        public boolean isInstance(SceneFragment scene) {
            return scene instanceof GalleryCommentsScene;
        }
    }

    private static class CommentGalleryListener extends EhCallback<GalleryCommentsScene, GalleryCommentList> {

        private final long mCommentId;

        public CommentGalleryListener(Context context, int stageId, String sceneTag, long commentId) {
            super(context, stageId, sceneTag);
            mCommentId = commentId;
        }

        @Override
        public void onSuccess(GalleryCommentList result) {
            showTip(mCommentId != 0 ? R.string.edit_comment_successfully : R.string.comment_successfully, LENGTH_SHORT);

            GalleryCommentsScene scene = getScene();
            if (scene != null) {
                scene.onCommentGallerySuccess(result);
            }
        }

        @Override
        public void onFailure(Exception e) {
            showTip(getContent().getString(mCommentId != 0 ? R.string.edit_comment_failed : R.string.comment_failed) + "\n" + ExceptionUtils.getReadableString(e), LENGTH_LONG);
        }

        @Override
        public void onCancel() {
        }

        @Override
        public boolean isInstance(SceneFragment scene) {
            return scene instanceof GalleryCommentsScene;
        }
    }

    private static class VoteCommentListener extends EhCallback<GalleryCommentsScene, VoteCommentParser.Result> {

        public VoteCommentListener(Context context, int stageId, String sceneTag) {
            super(context, stageId, sceneTag);
        }

        @Override
        public void onSuccess(VoteCommentParser.Result result) {
            showTip(result.expectVote > 0 ?
                            (0 != result.vote ? R.string.vote_up_successfully : R.string.cancel_vote_up_successfully) :
                            (0 != result.vote ? R.string.vote_down_successfully : R.string.cancel_vote_down_successfully),
                    LENGTH_SHORT);

            GalleryCommentsScene scene = getScene();
            if (scene != null) {
                scene.onVoteCommentSuccess(result);
            }
        }

        @Override
        public void onFailure(Exception e) {
            showTip(R.string.vote_failed, LENGTH_LONG);
        }

        @Override
        public void onCancel() {
        }

        @Override
        public boolean isInstance(SceneFragment scene) {
            return scene instanceof GalleryCommentsScene;
        }
    }

    private static class InfoHolder extends RecyclerView.ViewHolder {

        private final TextView key;
        private final TextView value;

        public InfoHolder(View itemView) {
            super(itemView);
            key = (TextView) ViewUtils.$$(itemView, R.id.key);
            value = (TextView) ViewUtils.$$(itemView, R.id.value);
        }
    }

    private abstract static class CommentHolder extends RecyclerView.ViewHolder {
        public CommentHolder(LayoutInflater inflater, int resId, ViewGroup parent) {
            super(inflater.inflate(resId, parent, false));
        }
    }

    private class ActualCommentHolder extends CommentHolder {

        private final TextView user;
        private final TextView time;
        private final LinkifyTextView comment;

        public ActualCommentHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater, R.layout.item_gallery_comment, parent);
            user = itemView.findViewById(R.id.user);
            time = itemView.findViewById(R.id.time);
            comment = itemView.findViewById(R.id.comment);
        }

        private CharSequence generateComment(Context context, ObservedTextView textView, GalleryComment comment) {
            SpannableStringBuilder ssb = Html.fromHtml(comment.comment, new URLImageGetter(textView,
                    EhApplication.getConaco(context)), null);

            if (0 != comment.id && 0 != comment.score) {
                int score = comment.score;
                String scoreString = score > 0 ? "+" + score : Integer.toString(score);
                SpannableString ss = new SpannableString(scoreString);
                ss.setSpan(new RelativeSizeSpan(0.8f), 0, scoreString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new StyleSpan(Typeface.BOLD), 0, scoreString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(ResourcesKt.resolveColor(getTheme(), android.R.attr.textColorSecondary))
                        , 0, scoreString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssb.append("  ").append(ss);
            }

            if (comment.lastEdited != 0) {
                String str = context.getString(R.string.last_edited, ReadableTime.getTimeAgo(comment.lastEdited));
                SpannableString ss = new SpannableString(str);
                ss.setSpan(new RelativeSizeSpan(0.8f), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new StyleSpan(Typeface.BOLD), 0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ss.setSpan(new ForegroundColorSpan(ResourcesKt.resolveColor(getTheme(), android.R.attr.textColorSecondary)),
                        0, str.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                ssb.append("\n\n").append(ss);
            }

            return TextUrl.handleTextUrl(ssb);
        }

        public void bind(GalleryComment value) {
            user.setText(value.uploader ? getString(R.string.comment_user_uploader, value.user) : value.user);
            user.setOnClickListener(v -> {
                ListUrlBuilder lub = new ListUrlBuilder();
                lub.setMode(ListUrlBuilder.MODE_UPLOADER);
                lub.setKeyword(value.user);
                GalleryListScene.startScene(GalleryCommentsScene.this, lub);
            });
            time.setText(ReadableTime.getTimeAgo(value.time));
            comment.setText(generateComment(comment.getContext(), comment, value));
        }
    }

    private static class MoreCommentHolder extends CommentHolder {
        public MoreCommentHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater, R.layout.item_gallery_comment_more, parent);
        }
    }

    private static class ProgressCommentHolder extends CommentHolder {
        public ProgressCommentHolder(LayoutInflater inflater, ViewGroup parent) {
            super(inflater, R.layout.item_gallery_comment_progress, parent);
        }
    }

    private class CommentAdapter extends RecyclerView.Adapter<CommentHolder> {

        private final LayoutInflater mInflater;

        public CommentAdapter() {
            mInflater = getLayoutInflater();
            AssertUtils.assertNotNull(mInflater);
        }

        @NonNull
        @Override
        public CommentHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case TYPE_COMMENT:
                    return new ActualCommentHolder(mInflater, parent);
                case TYPE_MORE:
                    return new MoreCommentHolder(mInflater, parent);
                case TYPE_PROGRESS:
                    return new ProgressCommentHolder(mInflater, parent);
                default:
                    throw new IllegalStateException("Invalid view type: " + viewType);
            }
        }

        @Override
        public void onBindViewHolder(@NonNull CommentHolder holder, int position) {
            Context context = getContext();
            if (context == null || mGalleryDetail == null || mGalleryDetail.comments == null) {
                return;
            }

            holder.itemView.setOnClickListener(v -> onItemClick(mRecyclerView, holder.itemView, position));
            holder.itemView.setClickable(true);
            holder.itemView.setFocusable(true);


            if (holder instanceof ActualCommentHolder) {
                ((ActualCommentHolder) holder).bind(mGalleryDetail.comments.comments[position]);
            }
        }

        @Override
        public int getItemCount() {
            if (mGalleryDetail == null || mGalleryDetail.comments == null || mGalleryDetail.comments.comments == null) {
                return 0;
            } else if (mGalleryDetail.comments.hasMore) {
                return mGalleryDetail.comments.comments.length + 1;
            } else {
                return mGalleryDetail.comments.comments.length;
            }
        }

        @Override
        public int getItemViewType(int position) {
            if (position >= mGalleryDetail.comments.comments.length) {
                return mRefreshingComments ? TYPE_PROGRESS : TYPE_MORE;
            } else {
                return TYPE_COMMENT;
            }
        }
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/mongodb/security/injection/audit/mongodb-nosqli.java

import org.apache.ecs.Document;
import org.apache.ecs.xhtml.col;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;

import java.util.ArrayList;
import java.util.HashMap;

class ContactsService {

    private MongoDatabase db;
    private MongoCollection<Document> collection;

    public ContactsService(MongoDatabase db, MongoCollection<Document> collection) {
        this.db = db;
        this.collection = collection;
    }

    public ArrayList<Document> basicDBObjectPut1(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPut2(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPut3(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.put("sharedWith", userName);
        query.put("email", email);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPutAll1(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        HashMap<String, String> paramMap = new HashMap<>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        query.putAll(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPutAll2(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        query.putAll(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectPutAll3(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        query.putAll(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectAppend1(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectAppend2(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.append("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectAppend3(String userName, String email) {
        BasicDBObject query = new BasicDBObject();
        // ok: java_inject_rule-MongodbNoSQLi
        query.append("sharedWith", userName);
        query.append("email", email);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorKv1(String userName, String email) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorKv2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = new BasicDBObject("$where",
                "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorKv3(String userName, String email) {
        BasicDBObject query = new BasicDBObject("sharedWith", userName);
        // ok: java_inject_rule-MongodbNoSQLi
        query.append("email", email);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorMap1(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ruleid: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"" + userName + "\" && this.email == \"" + email + "\"");
        BasicDBObject query = new BasicDBObject(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorMap2(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        BasicDBObject query = new BasicDBObject(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectConstructorMap3(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        BasicDBObject query = new BasicDBObject(paramMap);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectParse1(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String json = new JSONObject(paramMap).toString();
        BasicDBObject query = new BasicDBObject().parse(json);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectParse2(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        String json = new JSONObject(paramMap).toString();
        BasicDBObject query = new BasicDBObject().parse(json);

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectParse3(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        String json = new JSONObject(paramMap).toString();
        BasicDBObject query = new BasicDBObject().parse(json);

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAdd1(String userName, String email) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAdd2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .add("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"")
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAdd3(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .add("sharedWith", userName)
                .add("email", email)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAppend1(String userName, String email) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAppend2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .append("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"")
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderAppend3(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start()
                .append("sharedWith", userName)
                .append("email", email)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartKv1(String userName, String email) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartKv2(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"")
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartKv3(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start("sharedWith", userName)
                .append("email", email)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartMap1(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start(paramMap)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartMap2(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("$where", "this.sharedWith == \"CONSTANT\" && this.email == \"CONSTANT\"");
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start(paramMap)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();

        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public ArrayList<Document> basicDBObjectBuilderStartMap3(String userName, String email) {
        HashMap<String, String> paramMap = new HashMap<>();
        // ok: java_inject_rule-MongodbNoSQLi
        paramMap.put("sharedWith", userName);
        paramMap.put("email", email);
        BasicDBObject query = (BasicDBObject) BasicDBObjectBuilder
                .start(paramMap)
                .get();

        MongoCursor<Document> cursor = collection.find(query).iterator();
        ArrayList<Document> results = new ArrayList<>();
        while (cursor.hasNext()) {
            Document doc = cursor.next();
            results.add(doc);
        }

        return results;
    }

    public FindIterable<Document> safe(String userName, String email) {
        // ok: java_inject_rule-MongodbNoSQLi
        FindIterable<Document> docs = collection.find(Filters.eq("username", userName));

        return docs;
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("MD4", new ExampleProvider());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/insecure-jms-deserialization.java

import java.util.Date;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import org.apache.log4j.Logger;
import com.rands.couponproject.jpa.Income;

/**
 * Message-Driven Bean implementation class for: IncomeConsumerBean
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MyQueue")
})
class IncomeConsumerBean implements MessageListener {

    static Logger logger = Logger.getLogger(IncomeConsumerBean.class);

    @EJB
    IncomeServiceBean isb;

    public IncomeConsumerBean() {
        // your implementation
    }

    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.info("onMessage received a TextMessage at " + new Date());
                TextMessage msg = (TextMessage) message;
                logger.warn("onMessage ignoring TextMessage : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                logger.info("onMessage received an ObjectMessage at " + new Date());

                ObjectMessage msg = (ObjectMessage) message;

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                logger.info("o=" + o);

                // ruleid: java_deserialization_rule-InsecureJmsDeserialization
                Income income = (Income) msg.getObject(); // variant 2 : calling getObject method and casting to a custom class
                logger.info("Message is : " + income);

                isb.StoreIncome(income);
            } else {
                logger.error("onMessage received an invalid message type");
            }

        } catch (JMSException e) {
            logger.error("onMessage failed : " + e.toString());
        }
    }

}

/**
 * Safe implementation
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MyQueue")
})
class SafeIncomeConsumerBean implements MessageListener {

    static Logger logger = Logger.getLogger(IncomeConsumerBean.class);

    @EJB
    IncomeServiceBean isb;

    public SafeIncomeConsumerBean() {
        // your implementation
    }

    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.info("onMessage received a TextMessage at " + new Date());
                TextMessage msg = (TextMessage) message;
                logger.warn("onMessage ignoring TextMessage : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                logger.info("onMessage received an ObjectMessage at " + new Date());

                ObjectMessage msg = (ObjectMessage) message;

                // Use custom LookAheadObjectInputStream to safely deserialize the object
                InputStream is = msg.getObjectStream();
                LookAheadObjectInputStream laois = new LookAheadObjectInputStream(is);
                // ok: java_deserialization_rule-InsecureJmsDeserialization
                Object o = laois.readObject();
                logger.info("Deserialized object: " + o);

                // Ensure that the deserialized object is of type Income
                if (o instanceof Income) {
                    Income income = (Income) o;
                    logger.info("Message is : " + income);
                    isb.StoreIncome(income);
                } else {
                    logger.error("Received object is not of type Income");
                }

                isb.StoreIncome(income);
            } else {
                logger.error("onMessage received an invalid message type");
            }

        } catch (JMSException e) {
            logger.error("onMessage failed : " + e.toString());
        }
    }

}

class LookAheadObjectInputStream extends ObjectInputStream {
    public LookAheadObjectInputStream(InputStream inputStream) throws IOException {
        super(inputStream);
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        if (!desc.getName().equals(Income.class.getName())) {
            throw new InvalidClassException("Unauthorized deserialization attempt", desc.getName());
        }
        return super.resolveClass(desc);
    }
}
// License: MIT (c) GitLab Inc.
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConstantDBPassword {
    private static final String URL = "jdbc:mysql://localhost:3306/your_database_name";
    private static final String USERNAME = "your_username";

    public static Connection getConnection() {
        Connection connection = null;
        try {
            // ruleid: java_password_rule-ConstantDBPassword
            connection = DriverManager.getConnection(URL, USERNAME, "somesuperstrongthing");
            System.out.println("Connected to the database successfully!");
        } catch (SQLException e) {
            System.out.println("Error connecting to the database: " + e.getMessage());
            e.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) {
        getConnection();
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package templateinjection;

import com.mitchellbosecke.pebble.error.PebbleException;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.VelocityContext;
import com.mitchellbosecke.pebble.PebbleEngine;
import com.mitchellbosecke.pebble.template.PebbleTemplate;

import java.io.*;
import java.util.*;

public class TemplateInjection {

    public void usage1(String inputFile) throws FileNotFoundException {
        Velocity.init();

        VelocityContext context = new VelocityContext();

        context.put("author", "Elliot A.");
        context.put("address", "217 E Broadway");
        context.put("phone", "555-1337");

        FileInputStream file = new FileInputStream(inputFile);

        //Evaluate
        StringWriter swOut = new StringWriter();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String result =  swOut.getBuffer().toString();
        System.out.println(result);
    }

    public void allSignatures(InputStream inputStream, Reader fileReader, String template) throws FileNotFoundException {
        VelocityContext context = new VelocityContext();
        StringWriter swOut = new StringWriter();

        // ruleid: java_templateinjection_rule-TemplateInjection
        Velocity.evaluate(context, swOut, "test", inputStream.toString());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void falsePositive() throws FileNotFoundException {
        VelocityContext context = new VelocityContext();
        StringWriter swOut = new StringWriter();

        Velocity.evaluate(context, swOut, "test", "Hello $user !");
    }

    public void simple1(String inputFile) {

        //Freemarker configuration object
        Configuration cfg = new Configuration();
        try {
            //Load template from source folder
            Template template = cfg.getTemplate(inputFile);

            // Build the data-model
            Map<String, Object> data = new HashMap<String, Object>();
            data.put("message", "Hello World!");

            //List parsing
            List<String> countries = new ArrayList<String>();
            countries.add("India");
            countries.add("United States");
            countries.add("Germany");
            countries.add("France");

            data.put("countries", countries);

            // Console output
            Writer out = new OutputStreamWriter(System.out);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out.flush();

        } catch (IOException e) {
            e.printStackTrace();
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }

    public void allSignatures1(String inputFile) throws IOException, TemplateException {
        Configuration cfg = new Configuration();
        Template template = cfg.getTemplate(inputFile);
        Map<String, Object> data = new HashMap<String, Object>();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void simple(String inputFile) throws IOException {

        PebbleEngine engine = new PebbleEngine.Builder().build();
        PebbleTemplate compiledTemplate = null;
        try {
            compiledTemplate = engine.getTemplate(inputFile);
        } catch (PebbleException e) {
            e.printStackTrace();
        }

        Map<String, Object> context = new HashMap<>();
        context.put("name", "Shivam");

        Writer writer = new StringWriter();
        try {
            compiledTemplate.evaluate(writer, context);
        } catch (PebbleException e) {
            e.printStackTrace();
        }

        String output = writer.toString();
    }

    public void allSignatures(String inputFile) throws IOException, PebbleException {
        PebbleEngine engine = new PebbleEngine.Builder().build();
        PebbleTemplate compiledTemplate = null;

        compiledTemplate = engine.getTemplate(inputFile);

        Map<String, Object> data = new HashMap<String, Object>();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class X509TrustManagerTest {

    class TrustAllManager implements X509TrustManager {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LoggedTrustAllManager implements X509TrustManager {

        Logger logger = LogManager.getLogger(LoggedTrustAllManager.class);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            if(issuerRequestsCount < 5){
                issuerRequestsCount++;
                return null;
            }
            return certificates;
        }
    }

    class FaultyLimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            X509Certificate[] a = certificates;
            return a;
        }
    }

    class LimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;
        
        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return certificates;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return certificates;
        }
    }

    class FaultyLimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class FaultyLimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ruleid: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            return null;
        }
    }


    class FaultyLimitedManager4 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package crypto;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

public class GCMNonceReuse
{
    public static final int GCM_TAG_LENGTH = 16;
    public static final String IV1String = "ab0123456789";
    public static final byte[] IV1 = IV1String.getBytes();
    public static final byte[] IV2 = new byte[]{97,98,48,49,50,51,52,53,54,55,56,57};

    private static byte[] IV3;

    private static SecretKey secretKey;

    GCMParameterSpec gcmParameterSpec;


    public GCMNonceReuse() {

        IV3 = IV1String.getBytes();

        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(256);
            secretKey = keyGenerator.generateKey();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
        //ruleid: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV1);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt2&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt2(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt3&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt3(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encryptSecure&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encryptSecure(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

        // Generate a new, random IV for each encryption operation
        SecureRandom secureRandom = new SecureRandom();
        byte[] iv = new byte[12];
        // GCM standard recommends a 12-byte (96-bit) IV
        secureRandom.nextBytes(iv);

        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        // Prepend the IV to the ciphertext to ensure it's available for decryption
        byte[] cipherTextWithIv = new byte[iv.length + cipherText.length];
        System.arraycopy(iv, 0, cipherTextWithIv, 0, iv.length);
        System.arraycopy(cipherText, 0, cipherTextWithIv, iv.length, cipherText.length);

        return Base64.getEncoder().encodeToString(cipherTextWithIv);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decrypt&input=ciphertext>'
    public String decrypt(String cipherText) throws Exception {
        cipherText = URLDecoder.decode(cipherText, StandardCharsets.UTF_8.toString()).replace(" ", "+");;

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] decoded = Base64.getDecoder().decode(cipherText);
        byte[] decryptedText = cipher.doFinal(decoded);

        return new String(decryptedText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decryptSecure&input=ciphertext>'
    public static String decryptSecure(String encryptedText) throws Exception {
        encryptedText = URLDecoder.decode(encryptedText, StandardCharsets.UTF_8.toString()).replace(" ", "+");

        // Decode the base64-encoded string
        byte[] decodedCipherText = Base64.getDecoder().decode(encryptedText);

        // Extract the IV from the beginning of the ciphertext
        byte[] iv = new byte[12];

        // Extract the actual ciphertext
        byte[] cipherText = new byte[decodedCipherText.length - 12];
        System.arraycopy(decodedCipherText, 12, cipherText, 0, cipherText.length);

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameterSpec);

        // Decrypt the ciphertext
        byte[] clearTextBytes = cipher.doFinal(cipherText);

        return new String(clearTextBytes);
    }

}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
        // ruleid: java_inject_rule-SqlInjection
        stmt.execute("select * from users where email = " + sb2.toString());

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/unirest-http-request.java
import kong.unirest.core.HttpResponse;
import kong.unirest.core.JsonNode;
import kong.unirest.core.Unirest;

class UnirestHTTPRequest {

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String url = "http://httpbin.org";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String url2 = "http://httpbin.org";
        // ruleid: java_crypto_rule-UnirestHTTPRequest
        Unirest.patch(url2);
    }

    public void ok1() {
        // ok: java_crypto_rule-UnirestHTTPRequest
        HttpResponse<JsonNode> response = Unirest.post("https://httpbin.org/post")
                .header("accept", "application/json")
                .queryString("apiKey", "123")
                .field("parameter", "value")
                .field("firstname", "Gary")
                .asJson();
    }

    public void ok2() {
        // ok: java_crypto_rule-UnirestHTTPRequest
        Unirest.get("https://httpbin.org")
                .queryString("fruit", "apple")
                .queryString("droid", "R2D2")
                .asString();
    }

    public void ok3() {
        // ok: java_crypto_rule-UnirestHTTPRequest
        HttpResponse response = Unirest.delete("https://httpbin.org").asEmpty();

        // ok: java_crypto_rule-UnirestHTTPRequest
        Unirest.patch("https://httpbin.org");
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-1", "SUN");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/unirest-http-request.java
import kong.unirest.core.HttpResponse;
import kong.unirest.core.JsonNode;
import kong.unirest.core.Unirest;

class UnirestHTTPRequest {

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String url = "http://httpbin.org";
        // ruleid: java_crypto_rule-UnirestHTTPRequest
        HttpResponse response = Unirest.delete(url).asEmpty();

        String url2 = "http://httpbin.org";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void ok1() {
        // ok: java_crypto_rule-UnirestHTTPRequest
        HttpResponse<JsonNode> response = Unirest.post("https://httpbin.org/post")
                .header("accept", "application/json")
                .queryString("apiKey", "123")
                .field("parameter", "value")
                .field("firstname", "Gary")
                .asJson();
    }

    public void ok2() {
        // ok: java_crypto_rule-UnirestHTTPRequest
        Unirest.get("https://httpbin.org")
                .queryString("fruit", "apple")
                .queryString("droid", "R2D2")
                .asString();
    }

    public void ok3() {
        // ok: java_crypto_rule-UnirestHTTPRequest
        HttpResponse response = Unirest.delete("https://httpbin.org").asEmpty();

        // ok: java_crypto_rule-UnirestHTTPRequest
        Unirest.patch("https://httpbin.org");
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-OgnlInjection
        reflectionProvider.setValue(input, null, null, null);
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/r?q=java.lang.security.audit.xxe.documentbuilderfactory-external-parameter-entities-true.documentbuilderfactory-external-parameter-entities-true

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.jdom2.input.SAXBuilder;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

class GoodDocumentBuilderFactory {
    public void GoodDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadDocumentBuilderFactory {
    public void BadDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXParserFactory {
    public void GoodSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadSAXParserFactory {
    public void BadSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXBuilder {
    public void GoodSAXBuilder1() throws ParserConfigurationException {
        SAXBuilder saxBuilder = new SAXBuilder();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        saxBuilder.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadSAXBuilder {
    public void BadSAXBuilder1() throws ParserConfigurationException {
        SAXBuilder saxBuilder = new SAXBuilder();
        // ruleid:java_xxe_rule-ExternalParameterEntitiesTrue
        saxBuilder.setFeature("http://xml.org/sax/features/external-parameter-entities", true);
    }
}

class GoodSAXReader {
    public void GoodSAXReader1() throws ParserConfigurationException {
        SAXReader saxReader = new SAXReader();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        saxReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadSAXReader {
    public void BadSAXReader1() throws ParserConfigurationException {
        SAXReader saxReader = new SAXReader();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodXMLReader {
    public void GoodXMLReader1() throws SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        SAXParser saxParser = factory.newSAXParser();
        // Obtain an XMLReader from the SAXParser
        XMLReader xmlReader = saxParser.getXMLReader();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        xmlReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadXMLReader {
    public void BadXMLReader1() throws ParserConfigurationException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        SAXParser saxParser = factory.newSAXParser();
        // Obtain an XMLReader from the SAXParser
        XMLReader xmlReader = saxParser.getXMLReader();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            // ruleid: java_inject_rule-SqlInjection
            ResultSet resultSet = statement.executeQuery("select * from Users where name = " + String.format("%s", input));
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package xml;

import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.ParserPool;
import org.springframework.context.annotation.Bean;

public class SAMLIgnoreComments {
  @Bean
  ParserPool parserPool() {
    BasicParserPool pool = new BasicParserPool();
    // ruleid: java_xml_rule-SAMLIgnoreComments
    pool.setIgnoreComments(false);
    return pool;
  }

  @Bean
  void parserPool2() {
    boolean shouldIgnore = false;
    BasicParserPool pool = new BasicParserPool();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
  }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
        // ruleid: java_inject_rule-OgnlInjection
        ognlUtil.setValue(input, null, null, "12345");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SmtpClient.java
// hash: a7694d0
package smtp;

import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Properties;
import org.apache.commons.text.StringEscapeUtils;

public class SmtpClient {
    public static void main(String[] args) throws Exception {
        //Example of payload that would prove the injection
        sendEmail("Testing Subject\nX-Test: Override", "test\nDEF:2", "SVW:2\nXYZ", "ou\nlalala:22\n\rlili:22", "123;\n456=889","../../../etc/passwd");
    }

    public static void sendEmail(String input1, String input2, String input3, String input4, String input5,String input6) throws Exception {

        final String username = "email@gmail.com"; //Replace by test account
        final String password = ""; //Replace by app password

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("source@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,new InternetAddress[] {new InternetAddress("destination@gmail.com")});
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_smtp_rule-SmtpClient
        message.addHeader(input3,"aa"); //Injectable API (key parameter)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        message.setText("This is just a test 2.");
        Transport.send(message);
        new FileDataSource("/path/traversal/here/"+input6);

        System.out.println("Done");
    }

    public static void sendEmailTainted(Session session,HttpServletRequest req) throws MessagingException {
        Message message = new MimeMessage(session);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static void sendEmailOK(Session session,String input) throws MessagingException {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
        message.addHeader("abc", URLEncoder.encode(input));
    }

    public static void fixedVersions(Session session, String input) throws Exception {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
       message.setSubject(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.addHeader("lol", StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDescription(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDisposition(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setText(StringEscapeUtils.escapeJava(input));
       // ok: java_smtp_rule-SmtpClient
       message.setText("" + 42);
    }
}

// License: MIT (c) GitLab Inc.
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConstantDBPassword {
    private static final String URL = "jdbc:mysql://localhost:3306/your_database_name";
    private static final String USERNAME = "your_username";

    public static Connection getConnection() {
        Connection connection = null;
        try {
            // ruleid: java_password_rule-ConstantDBPassword
            connection = DriverManager.getConnection(URL, USERNAME, "somesuperstrongthing");
            System.out.println("Connected to the database successfully!");
        } catch (SQLException e) {
            System.out.println("Error connecting to the database: " + e.getMessage());
            e.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) {
        getConnection();
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
        // ruleid:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script, "script1.groovy", "test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/r?q=java.jjwt.security.jwt-none-alg.jjwt-none-alg
package crypto;

class JWTNoneAlgorithm {

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        System.out.println("Lib1 bad1() JWT: " + token);
    }

    public void ok1() {
        javax.crypto.SecretKey key = io.jsonwebtoken.Jwts.SIG.HS256.key().build();
        // ok: java_crypto_rule_JwtNoneAlgorithm
        String token = io.jsonwebtoken.Jwts.builder()
                .subject("Bob")
                .signWith(key)
                .compact();

        System.out.println("Lib1 ok() JWT: " + token);
    }

    public void bad2() throws Exception {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            System.out.println("Lib2 bad2() JWT: " + token);
        } catch (Exception e) {
            System.out.println("Exception : " + e.getLocalizedMessage());
        }
    }

    public void bad3() {
        try {
            // PlainHeaders are set with None Algo.
            // ruleid: java_crypto_rule_JwtNoneAlgorithm
            com.nimbusds.jose.PlainHeader header = new com.nimbusds.jose.PlainHeader();

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\", \"exp\":1300819111}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad3() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bad4() {
        try {
            // PlainHeaders are set with None Algo.
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\"}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad4() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
        // ruleid: java_inject_rule-SqlInjection
        jdbcTemplate.update("test" + clientDetails + "test", clientDetails);
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package xss;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Radio;
//import org.apache.wicket.extensions.markup.html.form.select.SelectOptions.SimpleSelectOption;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.model.util.ListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;

class WicketXSS extends WebPage {
    public void XssWicketExamplePage(PageParameters pageParameters) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // http://www.java2s.com/example/java-src/pkg/org/jabylon/rest/ui/wicket/components/customfeedbackpanel-edea7.html
    protected Component newMessageDisplayComponent(String id, String message) {
        MultiLineLabel label = new MultiLineLabel(id, message);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return label;
    }

    public void setText(String txt, Component c) {

        if (txt == null || txt.trim().length() == 0) {
            c.setVisible(false);
        } else {
            c.setDefaultModelObject(txt);
            c.setVisible(true);
            // ruleid: java_xss_rule-WicketXSS
            c.setEscapeModelStrings(false);
        }
    }

    protected void newLabel(Label lab) {
        final Component label = lab;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // https://github.com/apache/wicket/blob/e9461b0d115a7dbf4992596823521f6e038817d9/wicket-user-guide/src/main/asciidoc/forms2/forms2_3.adoc#L134
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void advancedSearchPage(String input) {

        Form<?> form = new Form<>("form");
        // Options for the dropdown
        List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

        // Model for the selected value
        Model<String> selectedOption = new Model<>("Option 1 " + input);

        // Create a DropDownChoice and add it to the form
        DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        form.add(dropdown);

        add(form);

    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Radio/setEscapeModelStrings
    private String getFormComponentValue(final Radio<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Check/setEscapeModelStrings
    private String getFormComponentValue2(final Check<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }
}

class MyPages extends WebPage {

    private ModalWindow modalWindow;

    public MyPages() {
        // Create a ModalWindow and set its title
        modalWindow = new ModalWindow("modalWindow");
        modalWindow.setTitle("Potentially Unsafe Title");

        // Explicitly disable escaping of the model strings (title)
        // for CVE-2015-5347/
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        add(modalWindow);
    }
}

class MyPage extends WebPage {

    public MyPage(List<String> userGeneratedOptions, String input) {

        Form<Void> form = new Form<>("form");

        Model<String> selectedOption = Model.of("");

        RadioGroup<String> radioGroup = new RadioGroup<>("radioGroup", selectedOption);
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        for (String option : userGeneratedOptions) {
            radioGroup.add(new Radio<>("radio" + option, Model.of(option)));
        }

        form.add(radioGroup);

        // https://nightlies.apache.org/wicket/apidocs/8.x/org/apache/wicket/markup/html/form/CheckBoxMultipleChoice.html
        List SITES = Arrays.asList(new String[] { "The Server Side", "Java Lobby", input });
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package xxe;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLReaderFactory;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

public class XMLRdr {
    private static void receiveXMLStream(final InputStream inStream,
                                         final DefaultHandler defHandler)
            throws ParserConfigurationException, SAXException, IOException {
        // ...
        XMLReader reader = XMLReaderFactory.createXMLReader();
        // ruleid: java_xxe_rule-XMLRdr
        reader.parse(new InputSource(inStream));
    }


    public static void main(String[] args) throws ParserConfigurationException,
            SAXException, IOException {

        String xmlString = "<?xml version=\"1.0\"?>" +
                "<!DOCTYPE test [ <!ENTITY foo SYSTEM \"C:/Code/public.txt\"> ]><test>&foo;</test>"; // Tainted input

        InputStream is = new ByteArrayInputStream(xmlString.getBytes());
        receiveXMLStream(is, new DefaultHandler());
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
// scaffold: dependencies=org.owasp.encoder.encoder@1.2.3
package xss;

import org.owasp.encoder.Encode;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

// Also contains vulnerabilities found under ids: XSS_SERVLET,SERVLET_PARAMETER
public class XSSReqParamToServletWriter extends HttpServlet {

    protected void danger(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
        // ruleid: java_xss_rule-XSSReqParamToServletWriter
        resp.getWriter().write(input1);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1"); // BAD
        String sessionId = req.getRequestedSessionId(); // BAD
        String queryString = req.getQueryString(); // BAD

        String referrer = req.getHeader("Referer"); //Should have a higher priority
        if (referrer != null && referrer.startsWith("http://company.ca")) {
            // Header access
            String host = req.getHeader("Host"); // BAD
            String referer = req.getHeader("Referer"); // BAD
            String userAgent = req.getHeader("User-Agent"); // BAD
        }

        PrintWriter writer = resp.getWriter();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void ok2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
        PrintWriter writer = resp.getWriter();
        // ok: java_xss_rule-XSSReqParamToServletWriter
        writer.write(Encode.forHtml(input1));
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package password;

import com.hazelcast.config.SymmetricEncryptionConfig;
import io.vertx.ext.web.handler.CSRFHandler;
import java.net.PasswordAuthentication;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.sql.DriverManager;
import javax.crypto.spec.PBEKeySpec;
import javax.net.ssl.KeyManagerFactory;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.kerberos.KerberosKey;

public class HardcodePassword {
    private final String passwordString = "secret";
    private final SymmetricEncryptionConfig passwordS = new SymmetricEncryptionConfig();

    public void danger1(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger2(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger3(KeyStore ks, String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger4(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger5(KeyManagerFactory kmf, String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger10(String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_password_rule-HardcodePassword
        DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", password);
    }

    public void danger11(String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger12(SymmetricEncryptionConfig s, String password) throws Exception{
        // ruleid: java_password_rule-HardcodePassword
        s.setPassword("secret");

        // ok: java_password_rule-HardcodePassword
        s.setPassword(password);
    }
}

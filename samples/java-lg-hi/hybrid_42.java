class Pipe {

    private final int capacity;
    private final byte[] buffer;

    private int head = 0;
    private int tail = 0;
    private boolean full = false;

    private boolean inClosed = false;
    private boolean outClosed = false;

    private final InputStream inputStream = new InputStream() {
        @Override
        public int read() throws IOException {
            synchronized (Pipe.this) {
                byte[] bytes = new byte[1];
                if (read(bytes, 0, 1) != -1) {
                    return bytes[0];
                } else {
                    return -1;
                }
            }
        }

        @Override
        public int read(@NonNull byte[] b, int off, int len) throws IOException {
            synchronized (Pipe.this) {
                for (; ; ) {
                    if (inClosed) {
                        throw new IOException("The InputStream is closed");
                    }
                    if (len == 0) {
                        return 0;
                    }

                    if (head == tail && !full) {
                        if (outClosed) {
                            // No bytes available and the OutputStream is closed. So it's the end.
                            return -1;
                        } else {
                            // Wait for OutputStream write bytes
                            try {
                                Pipe.this.wait();
                            } catch (InterruptedException e) {
                                throw new IOException("The thread interrupted", e);
                            }
                        }
                    } else {
                        int read = Math.min(len, (head < tail ? tail : capacity) - head);
                        System.arraycopy(buffer, head, b, off, read);
                        head += read;
                        if (head == capacity) {
                            head = 0;
                        }
                        full = false;
                        Pipe.this.notifyAll();
                        return read;
                    }
                }
            }
        }

        @Override
        public void close() {
            synchronized (Pipe.this) {
                inClosed = true;
                Pipe.this.notifyAll();
            }
        }
    };

    private final OutputStream outputStream = new OutputStream() {
        @Override
        public void write(int b) throws IOException {
            synchronized (Pipe.this) {
                byte[] bytes = new byte[]{(byte) b};
                write(bytes, 0, 1);
            }
        }

        @Override
        public void write(@NonNull byte[] b, int off, int len) throws IOException {
            synchronized (Pipe.this) {
                while (len != 0) {
                    if (outClosed) {
                        throw new IOException("The OutputStream is closed");
                    }
                    if (inClosed) {
                        throw new IOException("The InputStream is closed");
                    }

                    if (head == tail && full) {
                        // The buffer is full, wait for InputStream read bytes
                        try {
                            Pipe.this.wait();
                        } catch (InterruptedException e) {
                            throw new IOException("The thread interrupted", e);
                        }
                    } else {
                        int write = Math.min(len, (head <= tail ? capacity : head) - tail);
                        System.arraycopy(b, off, buffer, tail, write);
                        off += write;
                        len -= write;
                        tail += write;
                        if (tail == capacity) {
                            tail = 0;
                        }
                        if (head == tail) {
                            full = true;
                        }
                        Pipe.this.notifyAll();
                    }
                }
            }
        }

        @Override
        public void close() {
            synchronized (Pipe.this) {
                outClosed = true;
                Pipe.this.notifyAll();
            }
        }
    };

    Pipe(int capacity) {
        this.capacity = capacity;
        this.buffer = new byte[capacity];
    }

    InputStream getInputStream() {
        return inputStream;
    }

    OutputStream getOutputStream() {
        return outputStream;
    }
}

public class EhStageLayout extends StageLayout implements CoordinatorLayout.AttachedBehavior {

    private List<View> mAboveSnackViewList;

    public EhStageLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EhStageLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void addAboveSnackView(View view) {
        if (null == mAboveSnackViewList) {
            mAboveSnackViewList = new ArrayList<>();
        }
        mAboveSnackViewList.add(view);
    }

    public void removeAboveSnackView(View view) {
        if (null == mAboveSnackViewList) {
            return;
        }
        mAboveSnackViewList.remove(view);
    }

    public int getAboveSnackViewCount() {
        return null == mAboveSnackViewList ? 0 : mAboveSnackViewList.size();
    }

    @Nullable
    public View getAboveSnackViewAt(int index) {
        if (null == mAboveSnackViewList || index < 0 || index >= mAboveSnackViewList.size()) {
            return null;
        } else {
            return mAboveSnackViewList.get(index);
        }
    }

    @NonNull
    @Override
    public EhStageLayout.Behavior getBehavior() {
        return new EhStageLayout.Behavior();
    }

    public static class Behavior extends CoordinatorLayout.Behavior<EhStageLayout> {

        @Override
        public boolean layoutDependsOn(@NonNull CoordinatorLayout parent, @NonNull EhStageLayout child, @NonNull View dependency) {
            return dependency instanceof Snackbar.SnackbarLayout;
        }

        @Override
        public boolean onDependentViewChanged(@NonNull CoordinatorLayout parent, EhStageLayout child, @NonNull View dependency) {
            for (int i = 0, n = child.getAboveSnackViewCount(); i < n; i++) {
                View view = child.getAboveSnackViewAt(i);
                if (view != null) {
                    float translationY = Math.min(0, dependency.getTranslationY() - dependency.getHeight() - LayoutUtils.dp2pix(view.getContext(), 8));
                    ViewCompat.animate(view).setInterpolator(new FastOutSlowInInterpolator()).translationY(translationY).setDuration(150).start();
                }
            }
            return false;
        }

        @Override
        public void onDependentViewRemoved(@NonNull CoordinatorLayout parent, @NonNull EhStageLayout child, @NonNull View dependency) {
            for (int i = 0, n = child.getAboveSnackViewCount(); i < n; i++) {
                View view = child.getAboveSnackViewAt(i);
                if (view != null) {
                    ViewCompat.animate(view).setInterpolator(new FastOutSlowInInterpolator()).translationY(0).setDuration(75).start();
                }
            }
        }
    }
}

public class CookieSignInScene extends SolidScene implements EditText.OnEditorActionListener,
        View.OnClickListener {

    /*---------------
     View life cycle
     ---------------*/
    @Nullable
    private TextInputLayout mIpbMemberIdLayout;
    @Nullable
    private TextInputLayout mIpbPassHashLayout;
    @Nullable
    private TextInputLayout mIgneousLayout;
    @Nullable
    private EditText mIpbMemberId;
    @Nullable
    private EditText mIpbPassHash;
    @Nullable
    private EditText mIgneous;
    @Nullable
    private View mOk;
    @Nullable
    private TextView mFromClipboard;

    // false for error
    private static boolean checkIpbMemberId(String id) {
        for (int i = 0, n = id.length(); i < n; i++) {
            char ch = id.charAt(i);
            if (!(ch >= '0' && ch <= '9')) {
                return false;
            }
        }
        return true;
    }

    private static boolean checkIpbPassHash(String hash) {
        if (32 != hash.length()) {
            return false;
        }

        for (int i = 0, n = hash.length(); i < n; i++) {
            char ch = hash.charAt(i);
            if (!(ch >= '0' && ch <= '9') && !(ch >= 'a' && ch <= 'z')) {
                return false;
            }
        }
        return true;
    }

    private static Cookie newCookie(String name, String value, String domain) {
        return new Cookie.Builder().name(name).value(value)
                .domain(domain).expiresAt(Long.MAX_VALUE).build();
    }

    @Override
    public boolean needShowLeftDrawer() {
        return false;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.scene_cookie_sign_in, container, false);
        mIpbMemberIdLayout = (TextInputLayout) ViewUtils.$$(view, R.id.ipb_member_id_layout);
        mIpbMemberId = mIpbMemberIdLayout.getEditText();
        AssertUtils.assertNotNull(mIpbMemberId);
        mIpbPassHashLayout = (TextInputLayout) ViewUtils.$$(view, R.id.ipb_pass_hash_layout);
        mIpbPassHash = mIpbPassHashLayout.getEditText();
        AssertUtils.assertNotNull(mIpbPassHash);
        mIgneousLayout = (TextInputLayout) ViewUtils.$$(view, R.id.igneous_layout);
        mIgneous = mIgneousLayout.getEditText();
        AssertUtils.assertNotNull(mIgneous);
        mOk = ViewUtils.$$(view, R.id.ok);
        mFromClipboard = (TextView) ViewUtils.$$(view, R.id.from_clipboard);

        mFromClipboard.setPaintFlags(mFromClipboard.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG | Paint.ANTI_ALIAS_FLAG);

        mIpbPassHash.setOnEditorActionListener(this);

        mOk.setOnClickListener(this);
        mFromClipboard.setOnClickListener(this);

        // Try to get old version cookie info
        Context context = getContext();
        AssertUtils.assertNotNull(context);
        SharedPreferences sharedPreferences = context.getSharedPreferences("eh_info", 0);
        String ipbMemberId = sharedPreferences.getString("ipb_member_id", null);
        String ipbPassHash = sharedPreferences.getString("ipb_pass_hash", null);
        String igneous = sharedPreferences.getString("igneous", null);
        boolean getIt = false;
        if (!TextUtils.isEmpty(ipbMemberId)) {
            mIpbMemberId.setText(ipbMemberId);
            getIt = true;
        }
        if (!TextUtils.isEmpty(ipbPassHash)) {
            mIpbPassHash.setText(ipbPassHash);
            getIt = true;
        }
        if (!TextUtils.isEmpty(igneous)) {
            mIgneous.setText(igneous);
            getIt = true;
        }
        if (getIt) {
            showTip(R.string.found_cookies, LENGTH_SHORT);
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        showSoftInput(mIpbMemberId);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mIpbMemberIdLayout = null;
        mIpbPassHashLayout = null;
        mIgneousLayout = null;
        mIpbMemberId = null;
        mIpbPassHash = null;
        mIgneous = null;
    }

    @Override
    public void onClick(View v) {
        if (mOk == v) {
            enter();
        } else if (mFromClipboard == v) {
            fillCookiesFromClipboard();
        }
    }

    @Override
    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
        if (mIpbPassHash == v) {
            enter();
        }
        return true;
    }

    public void enter() {
        Context context = getContext();
        if (null == context || null == mIpbMemberIdLayout || null == mIpbPassHashLayout ||
                null == mIgneousLayout || null == mIpbMemberId || null == mIpbPassHash ||
                null == mIgneous) {
            return;
        }

        final String ipbMemberId = mIpbMemberId.getText().toString().trim();
        final String ipbPassHash = mIpbPassHash.getText().toString().trim();
        final String igneous = mIgneous.getText().toString().trim();

        if (TextUtils.isEmpty(ipbMemberId)) {
            mIpbMemberIdLayout.setError(getString(R.string.text_is_empty));
            return;
        } else {
            mIpbMemberIdLayout.setError(null);
        }
        if (TextUtils.isEmpty(ipbPassHash)) {
            mIpbPassHashLayout.setError(getString(R.string.text_is_empty));
            return;
        } else {
            mIpbPassHashLayout.setError(null);
        }

        hideSoftInput();

        if (!checkIpbMemberId(ipbMemberId) || !(checkIpbPassHash(ipbPassHash))) {
            new AlertDialog.Builder(context).setTitle(R.string.waring)
                    .setMessage(R.string.wrong_cookie_warning)
                    .setPositiveButton(R.string.i_will_check_it, null)
                    .setNegativeButton(R.string.i_dont_think_so, (dialog, which) -> {
                        storeCookie(ipbMemberId, ipbPassHash, igneous);
                        setResult(RESULT_OK, null);
                        finish();
                    }).show();
        } else {
            storeCookie(ipbMemberId, ipbPassHash, igneous);
            setResult(RESULT_OK, null);
            finish();
        }
    }

    private void storeCookie(String id, String hash, String igneous) {
        Context context = getContext();
        if (null == context) {
            return;
        }

        EhUtils.signOut(context);

        EhCookieStore store = EhApplication.getEhCookieStore(context);
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_MEMBER_ID, id, EhUrl.DOMAIN_E));
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_MEMBER_ID, id, EhUrl.DOMAIN_EX));
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_PASS_HASH, hash, EhUrl.DOMAIN_E));
        store.addCookie(newCookie(EhCookieStore.KEY_IPD_PASS_HASH, hash, EhUrl.DOMAIN_EX));
        if (!igneous.isEmpty()) {
            store.addCookie(newCookie(EhCookieStore.KEY_IGNEOUS, igneous, EhUrl.DOMAIN_E));
            store.addCookie(newCookie(EhCookieStore.KEY_IGNEOUS, igneous, EhUrl.DOMAIN_EX));
        }
    }

    private void fillCookiesFromClipboard() {
        hideSoftInput();
        String text = ClipboardUtil.getTextFromClipboard();
        if (text == null) {
            showTip(R.string.from_clipboard_error, LENGTH_SHORT);
            return;
        }
        try {
            String[] kvs;
            if (text.contains(";")) {
                kvs = text.split(";");
            } else if (text.contains("\n")) {
                kvs = text.split("\n");
            } else {
                showTip(R.string.from_clipboard_error, LENGTH_SHORT);
                return;
            }
            if (kvs.length < 3) {
                showTip(R.string.from_clipboard_error, LENGTH_SHORT);
                return;
            }
            for (String s : kvs) {
                String[] kv;
                if (s.contains("=")) {
                    kv = s.split("=");
                } else if (s.contains(":")) {
                    kv = s.split(":");
                } else {
                    continue;
                }
                if (kv.length != 2) {
                    continue;
                }
                switch (kv[0].trim().toLowerCase()) {
                    case "ipb_member_id":
                        if (mIpbMemberId != null) {
                            mIpbMemberId.setText(kv[1].trim());
                        }
                        break;
                    case "ipb_pass_hash":
                        if (mIpbPassHash != null) {
                            mIpbPassHash.setText(kv[1].trim());
                        }
                        break;
                    case "igneous":
                        if (mIgneous != null) {
                            mIgneous.setText(kv[1].trim());
                        }
                        break;
                }
            }
            enter();
        } catch (Exception e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            showTip(R.string.from_clipboard_error, LENGTH_SHORT);
        }
    }
}

public class FilterFragment extends BaseFragment {

    @Nullable
    private ViewTransition mViewTransition;
    @Nullable
    private FilterAdapter mAdapter;
    @Nullable
    private FilterList mFilterList;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_filter, container, false);

        mFilterList = new FilterList();

        RecyclerView recyclerView = (EasyRecyclerView) ViewUtils.$$(view, R.id.recycler_view);
        TextView tip = (TextView) ViewUtils.$$(view, R.id.tip);
        mViewTransition = new ViewTransition(recyclerView, tip);
        FloatingActionButton fab = view.findViewById(R.id.fab);

        Drawable drawable = ContextCompat.getDrawable(requireActivity(), R.drawable.big_filter);
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
        tip.setCompoundDrawables(null, drawable, null, null);

        mAdapter = new FilterAdapter();
        mAdapter.setHasStableIds(true);
        recyclerView.setAdapter(mAdapter);
        recyclerView.setClipToPadding(false);
        recyclerView.setClipChildren(false);
        LinearDividerItemDecoration decoration = new LinearDividerItemDecoration(
                LinearDividerItemDecoration.VERTICAL,
                ResourcesKt.resolveColor(requireActivity().getTheme(), R.attr.dividerColor),
                LayoutUtils.dp2pix(requireActivity(), 1));
        decoration.setShowLastDivider(true);
        recyclerView.addItemDecoration(decoration);
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        recyclerView.setHasFixedSize(true);
        DefaultItemAnimator defaultItemAnimator = (DefaultItemAnimator) recyclerView.getItemAnimator();
        if (defaultItemAnimator != null) {
            defaultItemAnimator.setSupportsChangeAnimations(false);
        }

        fab.setOnClickListener(v -> showAddFilterDialog());

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        updateView(false);
    }

    private void updateView(boolean animation) {
        if (null == mViewTransition) {
            return;
        }

        if (null == mFilterList || 0 == mFilterList.size()) {
            mViewTransition.showView(1, animation);
        } else {
            mViewTransition.showView(0, animation);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mViewTransition = null;
        mAdapter = null;
        mFilterList = null;
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.activity_filter, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.action_tip) {
            showTipDialog();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showTipDialog() {
        new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.filter)
                .setMessage(R.string.filter_tip)
                .setPositiveButton(android.R.string.ok, null)
                .show();
    }

    private void showAddFilterDialog() {
        AlertDialog dialog = new AlertDialog.Builder(requireActivity())
                .setTitle(R.string.add_filter)
                .setView(R.layout.dialog_add_filter)
                .setPositiveButton(R.string.add, null)
                .setNegativeButton(android.R.string.cancel, null)
                .show();
        AddFilterDialogHelper helper = new AddFilterDialogHelper();
        helper.setDialog(dialog);
    }

    private void showDeleteFilterDialog(final Filter filter) {
        String message = getString(R.string.delete_filter, filter.text);
        new AlertDialog.Builder(requireActivity())
                .setMessage(message)
                .setPositiveButton(R.string.delete, (dialog, which) -> {
                    if (DialogInterface.BUTTON_POSITIVE != which || null == mFilterList) {
                        return;
                    }
                    mFilterList.delete(filter);
                    if (null != mAdapter) {
                        mAdapter.notifyDataSetChanged();
                    }
                    updateView(true);
                })
                .setNegativeButton(android.R.string.cancel, null)
                .show();
    }

    private class AddFilterDialogHelper implements View.OnClickListener {

        @Nullable
        private AlertDialog mDialog;
        @Nullable
        private Spinner mSpinner;
        @Nullable
        private TextInputLayout mInputLayout;
        @Nullable
        private EditText mEditText;

        public void setDialog(AlertDialog dialog) {
            mDialog = dialog;
            mSpinner = (Spinner) ViewUtils.$$(dialog, R.id.spinner);
            mInputLayout = (TextInputLayout) ViewUtils.$$(dialog, R.id.text_input_layout);
            mEditText = mInputLayout.getEditText();
            View button = dialog.getButton(DialogInterface.BUTTON_POSITIVE);
            if (null != button) {
                button.setOnClickListener(this);
            }
        }

        @Override
        public void onClick(View v) {
            if (null == mFilterList || null == mDialog || null == mSpinner ||
                    null == mInputLayout || null == mEditText) {
                return;
            }

            String text = mEditText.getText().toString().trim();
            if (TextUtils.isEmpty(text)) {
                mInputLayout.setError(getString(R.string.text_is_empty));
                return;
            } else {
                mInputLayout.setError(null);
            }
            int mode = mSpinner.getSelectedItemPosition();

            Filter filter = new Filter();
            filter.mode = mode;
            filter.text = text;
            if (!mFilterList.add(filter)) {
                mInputLayout.setError(getString(R.string.label_text_exist));
                return;
            } else {
                mInputLayout.setError(null);
            }

            if (null != mAdapter) {
                mAdapter.notifyDataSetChanged();
            }
            updateView(true);

            mDialog.dismiss();
            mDialog = null;
            mSpinner = null;
            mInputLayout = null;
            mEditText = null;
        }
    }

    private static class FilterHolder extends RecyclerView.ViewHolder {

        private final MaterialCheckBox checkbox;
        private final TextView text;
        private final ImageView delete;

        public FilterHolder(View itemView) {
            super(itemView);
            checkbox = itemView.findViewById(R.id.checkbox);
            text = itemView.findViewById(R.id.text);
            delete = itemView.findViewById(R.id.delete);
        }
    }

    private class FilterAdapter extends RecyclerView.Adapter<FilterHolder> {

        private static final int TYPE_ITEM = 0;
        private static final int TYPE_HEADER = 1;

        @Override
        public int getItemViewType(int position) {
            if (null == mFilterList) {
                return TYPE_ITEM;
            }

            if (mFilterList.get(position).mode == FilterList.MODE_HEADER) {
                return TYPE_HEADER;
            } else {
                return TYPE_ITEM;
            }
        }

        @NonNull
        @Override
        public FilterHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            int layoutId;
            switch (viewType) {
                default:
                case TYPE_ITEM:
                    layoutId = R.layout.item_filter;
                    break;
                case TYPE_HEADER:
                    layoutId = R.layout.item_filter_header;
                    break;
            }

            return new FilterHolder(getLayoutInflater().inflate(layoutId, parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull FilterHolder holder, int position) {
            if (null == mFilterList) {
                return;
            }

            Filter filter = mFilterList.get(position);
            if (FilterList.MODE_HEADER == filter.mode) {
                holder.text.setText(filter.text);
            } else {
                holder.checkbox.setText(filter.text);
                holder.checkbox.setChecked(filter.enable);
                holder.itemView.setOnClickListener(v -> {
                    mFilterList.trigger(filter);

                    //for updating delete line on filter text
                    if (mAdapter != null) {
                        mAdapter.notifyItemChanged(position);
                    }
                });
                holder.delete.setOnClickListener(v -> showDeleteFilterDialog(filter));
            }
        }

        @Override
        public int getItemCount() {
            return null != mFilterList ? mFilterList.size() : 0;
        }

        @Override
        public long getItemId(int position) {
            if (mFilterList == null) {
                return 0;
            } else {
                Filter filter = mFilterList.get(position);
                if (filter.getId() != null) {
                    return (filter.text.hashCode() >> filter.mode) + filter.getId();
                }
                return filter.text.hashCode() >> filter.mode;
            }
        }
    }

    private class FilterList {

        public static final int MODE_HEADER = -1;

        private final EhFilter mEhFilter;
        private final List<Filter> mTitleFilterList;
        private final List<Filter> mUploaderFilterList;
        private final List<Filter> mTagFilterList;
        private final List<Filter> mTagNamespaceFilterList;

        private Filter mTitleHeader;
        private Filter mUploaderHeader;
        private Filter mTagHeader;
        private Filter mTagNamespaceHeader;

        public FilterList() {
            mEhFilter = EhFilter.getInstance();
            mTitleFilterList = mEhFilter.getTitleFilterList();
            mUploaderFilterList = mEhFilter.getUploaderFilterList();
            mTagFilterList = mEhFilter.getTagFilterList();
            mTagNamespaceFilterList = mEhFilter.getTagNamespaceFilterList();
        }

        public int size() {
            int count = 0;
            int size = mTitleFilterList.size();
            count += 0 == size ? 0 : size + 1;
            size = mUploaderFilterList.size();
            count += 0 == size ? 0 : size + 1;
            size = mTagFilterList.size();
            count += 0 == size ? 0 : size + 1;
            size = mTagNamespaceFilterList.size();
            count += 0 == size ? 0 : size + 1;
            return count;
        }

        private Filter getTitleHeader() {
            if (null == mTitleHeader) {
                mTitleHeader = new Filter();
                mTitleHeader.mode = MODE_HEADER;
                mTitleHeader.text = getString(R.string.filter_title);
            }
            return mTitleHeader;
        }

        private Filter getUploaderHeader() {
            if (null == mUploaderHeader) {
                mUploaderHeader = new Filter();
                mUploaderHeader.mode = MODE_HEADER;
                mUploaderHeader.text = getString(R.string.filter_uploader);
            }
            return mUploaderHeader;
        }

        private Filter getTagHeader() {
            if (null == mTagHeader) {
                mTagHeader = new Filter();
                mTagHeader.mode = MODE_HEADER;
                mTagHeader.text = getString(R.string.filter_tag);
            }
            return mTagHeader;
        }

        private Filter getTagNamespaceHeader() {
            if (null == mTagNamespaceHeader) {
                mTagNamespaceHeader = new Filter();
                mTagNamespaceHeader.mode = MODE_HEADER;
                mTagNamespaceHeader.text = getString(R.string.filter_tag_namespace);
            }
            return mTagNamespaceHeader;
        }

        public Filter get(int index) {
            int size = mTitleFilterList.size();
            if (0 != size) {
                if (index == 0) {
                    return getTitleHeader();
                } else if (index <= size) {
                    return mTitleFilterList.get(index - 1);
                } else {
                    index -= size + 1;
                }
            }

            size = mUploaderFilterList.size();
            if (0 != size) {
                if (index == 0) {
                    return getUploaderHeader();
                } else if (index <= size) {
                    return mUploaderFilterList.get(index - 1);
                } else {
                    index -= size + 1;
                }
            }

            size = mTagFilterList.size();
            if (0 != size) {
                if (index == 0) {
                    return getTagHeader();
                } else if (index <= size) {
                    return mTagFilterList.get(index - 1);
                } else {
                    index -= size + 1;
                }
            }

            size = mTagNamespaceFilterList.size();
            if (0 != size) {
                if (index == 0) {
                    return getTagNamespaceHeader();
                } else if (index <= size) {
                    return mTagNamespaceFilterList.get(index - 1);
                }
            }

            throw new IndexOutOfBoundsException();
        }

        public boolean add(Filter filter) {
            return mEhFilter.addFilter(filter);
        }

        public void delete(Filter filter) {
            mEhFilter.deleteFilter(filter);
        }

        public void trigger(Filter filter) {
            mEhFilter.triggerFilter(filter);
        }
    }

    @Override
    public int getFragmentTitle() {
        return R.string.filter;
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package cookie;

public class CookieHTTPOnly {

    public void dangerJavax(javax.servlet.http.HttpServletResponse res) {
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("key", "value");
        cookie.setSecure(true);
        cookie.setMaxAge(60);
        // ruleid: java_cookie_rule-CookieHTTPOnly
        cookie.setHttpOnly(false); // danger
        res.addCookie(cookie);
    }

    // cookie.setHttpOnly(true) is missing
    public void danger2Javax(javax.servlet.http.HttpServletResponse res) {
        javax.servlet.http.Cookie cookie = new javax.servlet.http.Cookie("key", "value");
        cookie.setSecure(true);
        cookie.setMaxAge(60);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void safeJavax(javax.servlet.http.HttpServletResponse response) {
        javax.servlet.http.Cookie myCookie = new javax.servlet.http.Cookie("key", "value");
        myCookie.setHttpOnly(true);
        myCookie.setMaxAge(60);
        // rule ok: java_cookie_rule-CookieHTTPOnly
        response.addCookie(myCookie);
    }

    protected void dangerJakarta(jakarta.servlet.http.HttpServletResponse response) {
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }

    protected void danger2Jakarta(jakarta.servlet.http.HttpServletResponse response) {
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
        myCookie.setMaxAge(60);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void safeJakarta(jakarta.servlet.http.HttpServletResponse response) {
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
        myCookie.setHttpOnly(true);
        myCookie.setMaxAge(60);
        // rule ok: java_cookie_rule-CookieHTTPOnly
        response.addCookie(myCookie);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package ssrf;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.OutputStream;
import java.net.*;

/**
 * @author Tomas Polesovsky
 */
public class SSRF {


    private static final int TIMEOUT_IN_SECONDS = 20;

    public static void testURL(String url) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_ssrf_rule-SSRF
        new URL(url).getContent();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static void testURI(String url) throws IOException, URISyntaxException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static void connect(URI url, SSLContext ctx) throws IOException {
        int port = url.getPort();
        port = port > 0 ? port : 443;
        try (Socket s = ctx.getSocketFactory().createSocket()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            s.connect(socketAddress, TIMEOUT_IN_SECONDS * 1000);
            try (OutputStream os = s.getOutputStream()) {
                os.write("GET / HTTP/1.1\n\n".getBytes());
            }
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-OgnlInjection
        ognlUtil.getValue(input, null, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/crypto/use-of-rc2.java
import javax.crypto.Cipher;

class RC2 {

    String algo2 = "RC2";

    public void bad1() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() throws Exception {
        String algo = "RC2";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad4() throws Exception {
        // ruleid: java_crypto_rule-UseOfRC2
        Cipher.getInstance(algo2);
    }
    public void bad5() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
    public void ok() throws Exception {
        // ok: java_crypto_rule-UseOfRC2
        Cipher.getInstance("AES/CBC/PKCS7PADDING");
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package com.gitlab.csrfservlet;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;

@Configuration
@EnableWebSecurity
@Profile("csrf-configurer")
public class SpringCSRFDisabled extends WebSecurityConfigurerAdapter {

    //Starting from Spring Security 4.x, the CSRF protection is enabled by default.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CsrfConfigurer<HttpSecurity> csrf = http.csrf();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_csrf_rule-SpringCSRFDisabled
        http.csrf().requireCsrfProtectionMatcher(new RegexRequestMatcher("/csrf/submit", "POST"));
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs

package crypto;

import java.security.*;
import java.security.spec.RSAKeyGenParameterSpec;

/**
 * The key size might need to be adjusted in the future.
 * http://en.wikipedia.org/wiki/Key_size#Asymmetric_algorithm_key_lengths
 */
public class InsufficientKeySizeRsa {

    public KeyPair weakKeySize1() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        // ruleid: java_crypto_rule-InsufficientKeySizeRsa
        keyGen.initialize(512); //BAD

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySize2() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }


    public KeyPair weakKeySize3ParameterSpec() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        KeyPair key = keyGen.generateKeyPair();
        return key;
    }

    public KeyPair weakKeySize4ParameterSpec() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        KeyPair key = keyGen.generateKeyPair();
        return key;
    }

    public KeyPair weakKeySize5Recommended() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair okKeySizeParameterSpec() throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA");
        keyGen.initialize(new RSAKeyGenParameterSpec(2048,RSAKeyGenParameterSpec.F4)); //Different signature

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySizeWithProviderString() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySizeWithProviderObject1() throws NoSuchAlgorithmException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", new ExampleProvider());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair weakKeySizeWithProviderObject2() throws NoSuchAlgorithmException {
        Provider p = new ExampleProvider("info");
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", p);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        return keyGen.generateKeyPair();
    }

    public KeyPair strongKeySizeWithProviderString() throws NoSuchAlgorithmException, NoSuchProviderException {
        KeyPairGenerator keyGen = KeyPairGenerator.getInstance("RSA", "BC");
        keyGen.initialize(2048); // OK: n >= 2048

        return keyGen.generateKeyPair();
    }


    private class ExampleProvider extends Provider {
        ExampleProvider() {
            this("example");
        }

        ExampleProvider(String info) {
            super("example", 0.0, info);
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo", path);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package xss;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Radio;
//import org.apache.wicket.extensions.markup.html.form.select.SelectOptions.SimpleSelectOption;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.model.util.ListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;

class WicketXSS extends WebPage {
    public void XssWicketExamplePage(PageParameters pageParameters) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // http://www.java2s.com/example/java-src/pkg/org/jabylon/rest/ui/wicket/components/customfeedbackpanel-edea7.html
    protected Component newMessageDisplayComponent(String id, String message) {
        MultiLineLabel label = new MultiLineLabel(id, message);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return label;
    }

    public void setText(String txt, Component c) {

        if (txt == null || txt.trim().length() == 0) {
            c.setVisible(false);
        } else {
            c.setDefaultModelObject(txt);
            c.setVisible(true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
    }

    protected void newLabel(Label lab) {
        final Component label = lab;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // https://github.com/apache/wicket/blob/e9461b0d115a7dbf4992596823521f6e038817d9/wicket-user-guide/src/main/asciidoc/forms2/forms2_3.adoc#L134
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void advancedSearchPage(String input) {

        Form<?> form = new Form<>("form");
        // Options for the dropdown
        List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

        // Model for the selected value
        Model<String> selectedOption = new Model<>("Option 1 " + input);

        // Create a DropDownChoice and add it to the form
        DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        form.add(dropdown);

        add(form);

    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Radio/setEscapeModelStrings
    private String getFormComponentValue(final Radio<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Check/setEscapeModelStrings
    private String getFormComponentValue2(final Check<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }
}

class MyPages extends WebPage {

    private ModalWindow modalWindow;

    public MyPages() {
        // Create a ModalWindow and set its title
        modalWindow = new ModalWindow("modalWindow");
        modalWindow.setTitle("Potentially Unsafe Title");

        // Explicitly disable escaping of the model strings (title)
        // for CVE-2015-5347/
        // ruleid: java_xss_rule-WicketXSS
        modalWindow.setEscapeModelStrings(false);
        add(modalWindow);
    }
}

class MyPage extends WebPage {

    public MyPage(List<String> userGeneratedOptions, String input) {

        Form<Void> form = new Form<>("form");

        Model<String> selectedOption = Model.of("");

        RadioGroup<String> radioGroup = new RadioGroup<>("radioGroup", selectedOption);
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        for (String option : userGeneratedOptions) {
            radioGroup.add(new Radio<>("radio" + option, Model.of(option)));
        }

        form.add(radioGroup);

        // https://nightlies.apache.org/wicket/apidocs/8.x/org/apache/wicket/markup/html/form/CheckBoxMultipleChoice.html
        List SITES = Arrays.asList(new String[] { "The Server Side", "Java Lobby", input });
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid:java_inject_rule-DangerousGroovyShell
        shell.evaluate(new URI(uri));
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

class Bad {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void badsocket1() {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void badsocket2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket4() throws Exception {
        String servername = "telnet://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket5() throws Exception {
        String servername = "ftp://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket6() throws Exception {
        String servername = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket7() throws Exception {
        String servername = "TELNET://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket8() throws Exception {
        String servername = "FTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket9() throws Exception {
        String servername = "HTTP://example.com";
        // ruleid: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 80);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket10() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        pingSocket.close();
    }

    public void badsocket11() throws Exception {
        BufferedReader in = null;
        Socket pingSocket = null;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        String serverResponse;
        while ((serverResponse = in.readLine()) != null) {
            System.out.println("Server: " + serverResponse);
        }

        System.out.println(in.readLine());
        in.close();
        pingSocket.close();
    }

    public void badsocket12() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket13() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket14() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket15() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket16() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

class Ok {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void oksocket1() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ssh://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket2() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("sftp://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket3() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("https://example.com", 443);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket4() throws Exception {
        String servername = "ssh://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket5() throws Exception {
        String servername = "sftp://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket6() throws Exception {
        String servername = "https://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_inject_rule-LDAPInjection
        template.lookup(jndiInjectMe);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(userInput);

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", new LdapEntryIdentificationContextMapper());
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package cookie;

import org.apache.commons.text.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class HttpResponseSplitting extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        Cookie c = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String paramNames = req.getParameterNames().nextElement();
        Cookie cParamNames = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamNames.setHttpOnly(true);
        cParamNames.setSecure(true);
        resp.addCookie(cParamNames);

        String paramValues = req.getParameterValues("input")[0];
        Cookie cParamValues = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamValues.setHttpOnly(true);
        cParamValues.setSecure(true);
        resp.addCookie(cParamValues);

        String paramMap = req.getParameterMap().get("input")[0];
        Cookie cParamMap = new Cookie("name", null);
        //ruleid: java_cookie_rule-HttpResponseSplitting
        cParamMap.setValue(paramMap);
        cParamMap.setHttpOnly(true);
        cParamMap.setSecure(true);
        resp.addCookie(cParamMap);

        String header = req.getHeader("input");
        Cookie cHeader = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = StringEscapeUtils.escapeJava(data);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = StringEscapeUtils.escapeJava(header);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = StringEscapeUtils.escapeJava(contextPath);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // BAD
        String tainted = req.getParameter("input");
        resp.setHeader("test", tainted);

        // OK: False negative but reported by spotbugs
        String data = req.getParameter("input");
        String normalized = data.replaceAll("\n", "\n");
        resp.setHeader("test", normalized);

        // OK: False negative but reported by spotbugs
        String normalized2 = data.replaceAll("\n", req.getParameter("test"));
        resp.setHeader("test2", normalized2);

        // OK
        String normalized3 = org.apache.commons.text.StringEscapeUtils.unescapeJava(tainted);
        resp.setHeader("test3", normalized3);

        // BAD
        String normalized4 = getString(tainted);
        resp.setHeader("test4", normalized4);

        // BAD
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
        wrapper.addHeader("test", tainted);
        wrapper.setHeader("test2", tainted);

    }

    private String getString(String s) {
        return s;
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SqlInjection
        conn.prepare(injection);

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/xxe/documentbuilderfactory-disallow-doctype-decl-false.java

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;

class GoodDocumentBuilderFactory {
    public void GoodXMLInputFactory() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    }

    public void GoodXMLInputFactory2() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }

    public void GoodXMLInputFactory2a() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }

    public void GoodXMLInputFactory3() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
        dbf.setFeature("http://xml.org/sax/features/external-general-entities", false);
    }

    public void GoodXMLInputFactory4() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    }

    public void GoodXMLInputFactory4a() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false);
        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
    }

    public void GoodXMLInputFactory5() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        dbf.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
    }

    public void GoodSAXParserFactory() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        // ok:java_xxe_rule-DisallowDoctypeDeclFalse
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    }

}

class BadDocumentBuilderFactory {
    public void BadXMLInputFactory() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // fix:java_xxe_rule-DisallowDoctypeDeclFalse
        // dbf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    }
}

class BadSAXParserFactory {
    public void BadSAXParserFactoryfn() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        // ruleid:java_xxe_rule-DisallowDoctypeDeclFalse
        spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", false);
        // fix:java_xxe_rule-DisallowDoctypeDeclFalse
        // spf.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    }
}

// License: The GitLab Enterprise Edition (EE) license (the “EE License”)
package deserialization;

import java.io.File;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.json.JsonMapper;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes;

class App {
    public static void main(String[] args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        ExecuteVulns executeVulns = new ExecuteVulns(objectMapper);

        executeVulns.enableDefaultTyping();
        executeVulns.defaultTypeResolverBuilder();
        executeVulns.defaultTypeResolverBuilder2();

        executeVulns.safeTypingWithUnsafeBaseAllowed();
        executeVulns.safeTypingWithLaissezFaireSubTypeValidator();
        executeVulns.blockUnsafeBaseType();
        executeVulns.blockUnsafeBaseType2();

    }

}

class ExecuteVulns {

    ObjectMapper objectMapper;

    ExecuteVulns(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    void enableDefaultTyping() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./car_jdbc.json";
        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // No Annotations
    void defaultTypeResolverBuilder() throws Exception {

        DefaultTypeResolverBuilder resolverBuilder = new DefaultTypeResolverBuilder(
                ObjectMapper.DefaultTyping.NON_FINAL);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        resolverBuilder.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        resolverBuilder.typeProperty("_type");

        objectMapper.setDefaultTyping(resolverBuilder);

    }

    void defaultTypeResolverBuilder2() throws Exception {

        DefaultTypeResolverBuilder rb = new DefaultTypeResolverBuilder(ObjectMapper.DefaultTyping.NON_FINAL);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        rb.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        rb.typeProperty("_type");

        objectMapper.setDefaultTyping(rb);

    }

    void safeTypingWithUnsafeBaseAllowed() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./mygadgetload.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void safeTypingWithLaissezFaireSubTypeValidator() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./mygadgetload_array.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // denies resolution of all subtypes of java.lang.Object
    void blockUnsafeBaseType() throws Exception {

        objectMapper.enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES);

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        objectMapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = objectMapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void blockUnsafeBaseType2() throws Exception {

        ObjectMapper mapper = JsonMapper.builder()
                .enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES)
                .build();

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        mapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = mapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        mapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }
}

class Car {

    public Object engine;
    public Object secEngine;
    public String color;
    public String model;

    public Car() {
    }

}

class CarWithCLASSAnnotations {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    public String color;
    public String model;

    public CarWithCLASSAnnotations() {
    }

}

class CarWithMinimalCLASSAnnotations {

    // ruleid: java_deserialization_rule-JacksonUnsafeDeserialization
    @JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS)
    public Object engine;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    public String color;
    public String model;

    public CarWithMinimalCLASSAnnotations() {
    }

}

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.WRAPPER_ARRAY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ElectricEngine.class, name = "ElectricEngine"),
        @JsonSubTypes.Type(value = FuelEngine.class, name = "FuelEngine")
})
abstract class Engine {
    public int engineModel;
    public int cc;
}

class ElectricEngine extends Engine {
    public int maxEnergy;

    public ElectricEngine() {
    }
}

class FuelEngine extends Engine {
    public int fuel;

    public FuelEngine() {
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package perm;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

public class OverlyPermissiveFilePermissionInline {

  public void dangerInline(Path path) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
  }

  public void dangerInline2(Path path) throws IOException {
    // ruleid: java_perm_rule-OverlyPermissiveFilePermissionInline
    Set<PosixFilePermission> perms = PosixFilePermissions.fromString("rw-rw-rw-");
    Files.setPosixFilePermissions(path, perms);
  }

  public void okInline(Path path) throws IOException {
    Files.setPosixFilePermissions(path, PosixFilePermissions.fromString("rw-rw----"));
  }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.KeyException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class NullCipher {

    public static void main(String[] args) throws Exception {

        byte[] pt = "AAAAAAAAAAAAAAAA".getBytes("UTF-8");

        Cipher expectedCihper = Cipher.getInstance("AES/CBC/NoPadding");
        // ruleid: java_crypto_rule-NullCipher
        Cipher doNothingCihper = new javax.crypto.NullCipher();


        printHex(encryptWithCipher(expectedCihper, pt));
        printHex(encryptWithCipher(doNothingCihper, pt));
    }

    public static byte[] encryptWithCipher(Cipher cipher, byte[] value) throws KeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException,
            UnsupportedEncodingException {

        //Key generation
        byte[] passkey = "BBBBBBBBBBBBBBBB".getBytes("UTF-8");
        Cipher expectedCihper = cipher;
        SecretKeySpec key = new SecretKeySpec(passkey, "AES");
        //Setting the key
        expectedCihper.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(new byte[expectedCihper.getBlockSize()]));

        return cipher.doFinal(value);
    }

    private static void printHex(byte[] resultBytes) {
        for (byte b : resultBytes) {
            System.out.print(Integer.toHexString(b & 0xFF));
        }
        System.out.println();
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

class Bad {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void badsocket1() {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void badsocket2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket3() throws Exception {
        // ruleid: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("http://example.com", 80);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket4() throws Exception {
        String servername = "telnet://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket5() throws Exception {
        String servername = "ftp://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket6() throws Exception {
        String servername = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket7() throws Exception {
        String servername = "TELNET://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket8() throws Exception {
        String servername = "FTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket9() throws Exception {
        String servername = "HTTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket10() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        pingSocket.close();
    }

    public void badsocket11() throws Exception {
        BufferedReader in = null;
        Socket pingSocket = null;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        String serverResponse;
        while ((serverResponse = in.readLine()) != null) {
            System.out.println("Server: " + serverResponse);
        }

        System.out.println(in.readLine());
        in.close();
        pingSocket.close();
    }

    public void badsocket12() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket13() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket14() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket15() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket16() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

class Ok {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void oksocket1() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ssh://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket2() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("sftp://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket3() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("https://example.com", 443);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket4() throws Exception {
        String servername = "ssh://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket5() throws Exception {
        String servername = "sftp://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket6() throws Exception {
        String servername = "https://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

class XMLStreamRdr {

    public void parseXMLSafe1(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe2(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe3(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe4(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe5(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe6(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe7(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe1(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe2(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe3(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe4(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, true);
        // ruleid: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

}

// License: MIT (c) GitLab Inc.

package com.test.servlet.cors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

// ref: java_cors_rule-PermissiveCORSInjection
// sample get req: http://localhost:8080/ServletSample/PermissiveCORSInjection/*?tainted=*f&URL=*&URL=*&url=*
public class PermissiveCORSInjection extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("attributeName",request.getParameter("tainted"));
        request.getSession().setAttribute("attributeName",request.getParameter("tainted"));
        request.getServletContext().setAttribute("attributeName",request.getParameter("tainted"));

        String paramValue = request.getParameter("tainted");
        String header = request.getHeader("tainted");
        String requestAttribute = (String) request.getAttribute("attributeName");
        String sessionAttribute = (String) request.getSession().getAttribute("attributeName");
        String contextAttribute = (String) request.getServletContext().getAttribute("attributeName");
        String pathInfo = request.getPathInfo();
        String modifiedPath = pathInfo.replaceFirst("/","");
        String queryString = request.getQueryString();

        String[] parameterValues = request.getParameterValues("URL");
        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, String[]> parameterMap = request.getParameterMap();

        String valueFromPV = parameterValues[0];
        String valueFromPN = parameterNames.nextElement();
        String valueFromPM = parameterMap.get("URL")[0];

        String[] keyValuePairs = queryString.split("=");
        String lastPair = keyValuePairs[keyValuePairs.length - 1];

        if (paramValue != null) {
          // ok:java_cors_rule-PermissiveCORSInjection
          response.setHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPV);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("Some-Example-Header", valueFromPN);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPM);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.addHeader("access-control-allow-origin", sessionAttribute);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          String headerName = "ACCESS-CONTROL-ALLOW-ORIGIN";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          return;
        }

        // ok:java_cors_rule-PermissiveCORSInjection
        response.setHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", getFromList("key"));

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "*");
    }

    public String getFromList(String key){
        HashMap<String, String> corsList = new HashMap<>();
        corsList.put("key", "https://example.com");

        return corsList.get(key);
    }
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SqlInjection
        new org.jdbi.v3.core.statement.PreparedBatch(handle, input);
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package com.gitlab.csrfservlet;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;

@Configuration
@EnableWebSecurity
@Profile("csrf-configurer")
public class SpringCSRFDisabled extends WebSecurityConfigurerAdapter {

    //Starting from Spring Security 4.x, the CSRF protection is enabled by default.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CsrfConfigurer<HttpSecurity> csrf = http.csrf();
        // ruleid: java_csrf_rule-SpringCSRFDisabled
        csrf.disable();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package perm;
import java.lang.reflect.ReflectPermission;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Policy;
import java.util.ArrayList;
import java.util.List;

public class DangerousPermissions extends Policy {
    public void danger(CodeSource cs) {
        PermissionCollection pc = super.getPermissions(cs);
        // ruleid: java_perm_rule-DangerousPermissions
        pc.add(new ReflectPermission("suppressAccessChecks"));
    }

    public void danger2(PermissionCollection pc) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger3(PermissionCollection pc) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        pc.add(perm);
    }

    public void danger4(PermissionCollection pc) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        pc.add(perm);
    }

    public void ok(CodeSource cs) {
        ReflectPermission perm = new ReflectPermission("suppressAccessChecks");
        List<ReflectPermission> list = new ArrayList<>();
        list.add(perm);
    }
}

public class App implements Comparable<App>, Parcelable {

    private static final String TAG = "App";

    /**
     * {@link LocaleListCompat} for finding the right app description material.
     * It is set globally static to a) cache this value, since there are thousands
     * of {@link App} entries, and b) make it easy to test}
     */
    public static LocaleListCompat systemLocaleList;

    public static LocaleListCompat getLocales() {
        LocaleListCompat cached = systemLocaleList;
        if (cached == null) {
            // Tries to get the device locales list set by the user in system settings.
            // The official docs are less than apparent in this regard, but empirically,
            // `ConfigurationCompat.getLocales(Resources.getSystem().getConfiguration())`
            // seems to push back languages marked "May not be available in some apps"
            // in Settings UI while `LocaleListCompat.getDefault()` appears to preserve
            // the user-preferred order so we prefer the latter here
            cached = LocaleListCompat.getDefault();
            systemLocaleList = cached;
        }
        return cached;
    }

    // these properties are not from the index metadata, but represent the state on the device
    /**
     * True if compatible with the device (i.e. if at least one apk is)
     */
    public boolean compatible;

    public Apk installedApk; // might be null if not installed
    public String installedSigner;
    public long installedVersionCode;
    public String installedVersionName;
    public org.fdroid.database.AppPrefs prefs;
    public String preferredSigner;
    public boolean isApk;

    /**
     * This is primarily for the purpose of saving app metadata when parsing an index.xml file.
     * At most other times, we don't particularly care which repo an {@link App} object came from.
     * It is pretty much transparent, because the metadata will be populated from the repo with
     * the highest priority. The UI doesn't care normally _which_ repo provided the metadata.
     * This is required for getting the full URL to the various graphics and screenshots.
     */
    public long repoId;

    // the remaining properties are set directly from the index metadata
    public String packageName = "unknown";
    public String name = "Unknown";

    public String summary = "Unknown application";

    public String description;

    /**
     * A descriptive text for what has changed in this version.
     */
    public String whatsNew;

    public FileV2 featureGraphic;
    private FileV2 promoGraphic;
    private FileV2 tvBanner;

    public List<FileV2> phoneScreenshots = Collections.emptyList();
    private List<FileV2> sevenInchScreenshots = Collections.emptyList();
    private List<FileV2> tenInchScreenshots = Collections.emptyList();
    private List<FileV2> tvScreenshots = Collections.emptyList();
    private List<FileV2> wearScreenshots = Collections.emptyList();

    public String license;

    public String authorName;
    public String authorEmail;

    public String webSite;

    public String issueTracker;

    public String sourceCode;

    public String translation;

    public String video;

    public String changelog;

    public String donate;

    public String bitcoin;

    public String litecoin;

    private String flattrID;

    public String liberapay;

    private String openCollective;

    /**
     * This matches {@code CurrentVersion} in build metadata files.
     *
     * @see <a href="https://f-droid.org/docs/Build_Metadata_Reference/#CurrentVersion">CurrentVersion</a>
     */
    @Deprecated
    public String suggestedVersionName;

    /**
     * This matches {@code CurrentVersionCode} in build metadata files. Java
     * inits {@code int}s to 0.  Since it is valid to have a negative Version
     * Code, this is inited to {@link Integer#MIN_VALUE};
     *
     * @see <a href="https://f-droid.org/docs/Build_Metadata_Reference/#CurrentVersionCode">CurrentVersionCode</a>
     */
    @Deprecated
    public int suggestedVersionCode = Integer.MIN_VALUE;

    /**
     * Unlike other public fields, this is only accessible via a getter, to
     * emphasise that setting it won't do anything.
     */
    private String autoInstallVersionName;

    /**
     * The version that will be automatically installed if the user does not
     * choose a specific version.
     * TODO this should probably be converted to init to {@link Integer#MIN_VALUE} like {@link #suggestedVersionCode}
     */
    public long autoInstallVersionCode;

    public Date added;
    public Date lastUpdated;

    /**
     * List of categories (as defined in the metadata documentation) or null if there aren't any.
     * This is only populated when parsing a repository. If you need to know about the categories
     * an app is in any other part of F-Droid, use the database.
     */
    public String[] categories;

    /**
     * List of anti-features (as defined in the metadata documentation) or null if there aren't any.
     */
    @Nullable
    public String[] antiFeatures;
    public Map<String, String> antiFeatureReasons = new HashMap<>();

    public FileV2 iconFile;

    @Override
    public int compareTo(@NonNull App app) {
        return name.compareToIgnoreCase(app.name);
    }

    public App() {
    }

    public App(final UpdatableApp app) {
        repoId = app.getUpdate().getRepoId();
        setPackageName(app.getPackageName());
        name = app.getName() == null ? "" : app.getName();
        summary = app.getSummary() == null ? "" : app.getSummary();
        installedVersionCode = app.getInstalledVersionCode();
        autoInstallVersionCode = app.getUpdate().getManifest().getVersionCode();
        iconFile = app.getIcon(getLocales());
    }

    public App(final org.fdroid.database.App app, @Nullable PackageInfo packageInfo) {
        repoId = app.getRepoId();
        compatible = app.getMetadata().isCompatible();
        setPackageName(app.getPackageName());
        name = app.getName() == null ? "" : app.getName();
        summary = app.getSummary() == null ? "" : app.getSummary();
        String desc = app.getDescription(getLocales());
        setDescription(desc == null ? "" : desc);
        license = app.getMetadata().getLicense();
        authorName = app.getMetadata().getAuthorName();
        authorEmail = app.getMetadata().getAuthorEmail();
        webSite = app.getMetadata().getWebSite();
        issueTracker = app.getMetadata().getIssueTracker();
        sourceCode = app.getMetadata().getSourceCode();
        translation = app.getMetadata().getTranslation();
        video = app.getVideo(getLocales());
        changelog = app.getMetadata().getChangelog();
        List<String> donateList = app.getMetadata().getDonate();
        if (donateList != null && !donateList.isEmpty()) {
            donate = donateList.get(0);
        }
        bitcoin = app.getMetadata().getBitcoin();
        litecoin = app.getMetadata().getLitecoin();
        flattrID = app.getMetadata().getFlattrID();
        liberapay = app.getMetadata().getLiberapay();
        openCollective = app.getMetadata().getOpenCollective();
        preferredSigner = app.getMetadata().getPreferredSigner();
        added = new Date(app.getMetadata().getAdded());
        lastUpdated = new Date(app.getMetadata().getLastUpdated());
        iconFile = app.getIcon(getLocales());
        featureGraphic = app.getFeatureGraphic(getLocales());
        promoGraphic = app.getPromoGraphic(getLocales());
        tvBanner = app.getTvBanner(getLocales());
        phoneScreenshots = app.getPhoneScreenshots(getLocales());
        sevenInchScreenshots = app.getSevenInchScreenshots(getLocales());
        tenInchScreenshots = app.getTenInchScreenshots(getLocales());
        tvScreenshots = app.getTvScreenshots(getLocales());
        wearScreenshots = app.getWearScreenshots(getLocales());
        setInstalled(packageInfo);
    }

    public App(AppListItem item) {
        repoId = item.getRepoId();
        setPackageName(item.getPackageName());
        name = item.getName() == null ? "" : item.getName();
        summary = item.getSummary() == null ? "" : item.getSummary();
        iconFile = item.getIcon(getLocales());
        installedVersionCode = item.getInstalledVersionCode() == null ? 0 : item.getInstalledVersionCode();
        installedVersionName = item.getInstalledVersionName();
        antiFeatures = item.getAntiFeatureKeys().toArray(new String[0]);
        compatible = item.isCompatible();
        preferredSigner = item.getPreferredSigner();
    }

    public void setInstalled(@Nullable PackageInfo packageInfo) {
        installedVersionCode = packageInfo == null ? 0 : PackageInfoCompat.getLongVersionCode(packageInfo);
        installedVersionName = packageInfo == null ? null : packageInfo.versionName;
        installedSigner = packageInfo == null ? null : Utils.getPackageSigner(packageInfo);
    }

    /**
     * Updates this App instance with information from the APKs.
     *
     * @param apks The APKs available for this app.
     */
    public void update(Context context, List<Apk> apks, org.fdroid.database.AppPrefs appPrefs) {
        this.prefs = appPrefs;
        for (Apk apk : apks) {
            boolean apkIsInstalled = (apk.versionCode == installedVersionCode &&
                    TextUtils.equals(apk.signer, installedSigner)) || (!apk.isApk() && apk.isMediaInstalled(context));
            if (apkIsInstalled) {
                installedApk = apk;
                installedVersionCode = apk.versionCode;
                installedVersionName = apk.versionName;
                break;
            }
        }
        Apk apk = findSuggestedApk(apks, appPrefs);
        if (apk == null) return;
        // update the autoInstallVersionCode, if needed
        if (autoInstallVersionCode <= 0 && installedVersionCode < apk.versionCode) {
            autoInstallVersionCode = apk.versionCode;
            autoInstallVersionName = apk.versionName;
        }
        antiFeatures = apk.antiFeatures;
        antiFeatureReasons = apk.antiFeatureReasons;
        whatsNew = apk.whatsNew;
        isApk = apk.isApk();
    }

    /**
     * In order to format all in coming descriptions before they are written
     * out to the database and used elsewhere, this is needed to intercept
     * the setting of {@link App#description} to insert the format method.
     */
    private void setDescription(String description) { // NOPMD
        this.description = formatDescription(description);
    }

    /**
     * Set the Package Name property while ensuring it is sanitized.
     */
    private void setPackageName(String packageName) {
        if (Utils.isSafePackageName(packageName)) {
            this.packageName = packageName;
        } else {
            throw new IllegalArgumentException("Repo index app entry includes unsafe packageName: '"
                    + packageName + "'");
        }
    }

    /**
     * Returns the app description text with all newlines replaced by {@code <br>}
     */
    private static String formatDescription(String description) {
        return description.replace("\n", "<br>");
    }

    /**
     * Get the URL with the standard path for displaying in a browser.
     */
    @Nullable
    public Uri getShareUri(Context context) {
        Repository repo = FDroidApp.getRepoManager(context).getRepository(repoId);
        if (repo == null || repo.getWebBaseUrl() == null) return null;
        return Uri.parse(repo.getWebBaseUrl()).buildUpon()
                .appendPath(packageName)
                .build();
    }

    public RequestBuilder<Drawable> loadWithGlide(Context context, IndexFile file) {
        return loadWithGlide(context, repoId, file);
    }

    public static RequestBuilder<Drawable> loadWithGlide(Context context, long repoId, IndexFile file) {
        Repository repo = FDroidApp.getRepoManager(context).getRepository(repoId);
        if (repo == null || file == null) { // This is also used for apps that do not have a repo
            return Glide.with(context).load(R.drawable.ic_repo_app_default);
        }
        return Glide.with(context).load(Utils.getGlideModel(repo, file));
    }

    public static RequestBuilder<Bitmap> loadBitmapWithGlide(Context context, long repoId,
                                                             IndexFile file) {
        Repository repo = FDroidApp.getRepoManager(context).getRepository(repoId);
        if (repo == null) {
            Log.e(TAG, "Repo not found: " + repoId);
            return Glide.with(context).asBitmap().load(R.drawable.ic_repo_app_default);
        }
        return Glide.with(context).asBitmap().load(Utils.getGlideModel(repo, file));
    }

    /**
     * Gets the path relative to the repo root.
     * Can be used to create URLs for use with mirrors.
     * Attention: This does NOT encode for use in URLs.
     */
    public static String getPath(String... pathElements) {
        /* Each String in pathElements might contain a /, should keep these as path elements */
        ArrayList<String> elements = new ArrayList<>();
        for (String element : pathElements) {
            Collections.addAll(elements, element.split("/"));
        }
        // build up path WITHOUT encoding the segments, this will happen later when turned into URL
        StringBuilder sb = new StringBuilder();
        for (String element : elements) {
            sb.append(element).append("/");
        }
        sb.deleteCharAt(sb.length() - 1); // remove trailing slash
        return sb.toString();
    }

    public List<FileV2> getAllScreenshots() {
        ArrayList<FileV2> list = new ArrayList<>(phoneScreenshots);
        list.addAll(sevenInchScreenshots);
        list.addAll(tenInchScreenshots);
        list.addAll(tvScreenshots);
        list.addAll(wearScreenshots);
        return list;
    }

    /**
     * Get the directory where APK Expansion Files aka OBB files are stored for the app as
     * specified by {@code packageName}.
     *
     * @see <a href="https://developer.android.com/google/play/expansion-files.html">APK Expansion Files</a>
     */
    static File getObbDir(String packageName) {
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                + "/Android/obb/" + packageName);
    }

    /**
     * Attempts to find the installed {@link Apk} in the given list of APKs. If not found, will lookup the
     * the details of the installed app and use that to instantiate an {@link Apk} to be returned.
     * <p>
     * Cases where an {@link Apk} will not be found in the database and for which we fall back to
     * the {@link PackageInfo} include:
     * <li>System apps which are provided by a repository, but for which the version code bundled
     * with the system is not included in the repository.</li>
     * <li>Regular apps from a repository, where the installed version is old enough that it is no
     * longer available in the repository.</li>
     */
    @Nullable
    public Apk getInstalledApk(Context context, List<Apk> apks) {
        try {
            PackageInfo pi = context.getPackageManager().getPackageInfo(packageName, 0);
            // If we are here, the package is actually installed, so we better find something
            Apk foundApk = null;
            for (Apk apk : apks) {
                if (apk.versionCode == PackageInfoCompat.getLongVersionCode(pi)) {
                    foundApk = apk;
                    break;
                }
            }
            if (foundApk == null) foundApk = new Apk(pi);
            return foundApk;
        } catch (PackageManager.NameNotFoundException e) {
            return null;
        }
    }

    public boolean isInstalled(Context context) {
        // First check isApk() before isMediaInstalled() because the latter is quite expensive,
        // hitting the database for each apk version, then the disk to check for installed media.
        return installedVersionCode > 0 || (!isApk() && isMediaInstalled(context));
    }

    private boolean isApk() {
        return isApk;
    }

    private boolean isMediaInstalled(Context context) {
        return getMediaApkifInstalled(context) != null;
    }

    /**
     * Gets the installed media apk from all the apks of this {@link App}, if any.
     *
     * @return The installed media {@link Apk} if it exists, null otherwise.
     */
    public Apk getMediaApkifInstalled(Context context) {
        if (this.installedApk != null && !this.installedApk.isApk() && this.installedApk.isMediaInstalled(context)) {
            return this.installedApk;
        }
        return null;
    }

    /**
     * True if there are new versions (apks) available
     */
    @Deprecated
    public boolean hasUpdates() {
        boolean updates = false;
        if (autoInstallVersionCode > 0) {
            updates = installedVersionCode > 0 && installedVersionCode < autoInstallVersionCode;
        }
        return updates;
    }

    /**
     * True if there are new versions (apks) available
     */
    public boolean hasUpdates(List<Apk> sortedApks, org.fdroid.database.AppPrefs appPrefs) {
        Apk suggestedApk = findSuggestedApk(sortedApks, appPrefs);
        boolean updates = false;
        if (suggestedApk != null) {
            updates = installedVersionCode > 0 && installedVersionCode < suggestedApk.versionCode;
        }
        return updates;
    }

    @Nullable
    public Apk findSuggestedApk(List<Apk> apks, org.fdroid.database.AppPrefs appPrefs) {
        String releaseChannel;
        if (appPrefs.getReleaseChannels().contains(Apk.RELEASE_CHANNEL_BETA)) {
            releaseChannel = Apk.RELEASE_CHANNEL_BETA;
        } else {
            releaseChannel = Preferences.get().getReleaseChannel();
        }
        return findSuggestedApk(apks, releaseChannel);
    }

    /**
     * Finds the APK we suggest to install.
     *
     * @param apks           a list of APKs sorted by version code (highest first).
     * @param releaseChannel the key of the release channel to be considered.
     * @return The Apk we suggest to install or null, if we didn't find any.
     */
    @Nullable
    public Apk findSuggestedApk(List<Apk> apks, String releaseChannel) {
        final String mostAppropriateSigner = getMostAppropriateSigner();
        Apk apk = null;
        for (Apk a : apks) {
            // only consider compatible APKs
            if (!a.compatible) continue;
            // if we have a signer, but it doesn't match, don't use this APK
            if (mostAppropriateSigner != null && !mostAppropriateSigner.equals(a.signer)) continue;
            // stable release channel is always allowed, otherwise must include given channel
            final String stable = Apk.RELEASE_CHANNEL_STABLE;
            boolean isReleaseChannelAllowed = stable.equals(releaseChannel) ?
                    a.releaseChannels.contains(stable) :
                    a.releaseChannels.contains(stable) || a.releaseChannels.contains(releaseChannel);
            // if signer matches and we want the highest version code, take this as list is sorted
            if (isReleaseChannelAllowed) {
                apk = a;
                break;
            }
        }
        // use the first of the list, before we don't choose anything
        // TODO does this still make sense? Maybe better to not suggest anything in this case?
        if (apk == null && apks.size() > 0) {
            apk = apks.get(0);
        }
        return apk;
    }

    /**
     * True if there are new versions (apks) available and the user wants to be notified about them
     */
    public boolean canAndWantToUpdate(@Nullable Apk suggestedApk) {
        if (suggestedApk == null) return false;
        if (installedVersionCode >= suggestedApk.versionCode) return false;
        final org.fdroid.database.AppPrefs prefs = this.prefs;
        return prefs == null || !prefs.shouldIgnoreUpdate(autoInstallVersionCode);
    }

    /**
     * @return if the given app should be filtered out based on the
     * {@link Preferences#PREF_SHOW_ANTI_FEATURES Show Anti-Features Setting}
     */
    public boolean isDisabledByAntiFeatures(Context context) {
        if (this.antiFeatures == null) {
            return false;
        }

        List<String> chooseableAntiFeatures = Arrays.asList(
                context.getResources().getStringArray(R.array.antifeaturesValues)
        );

        Set<String> shownAntiFeatures = Preferences.get().showAppsWithAntiFeatures();

        for (String antiFeature : this.antiFeatures) {
            if (chooseableAntiFeatures.contains(antiFeature)) {
                if (!shownAntiFeatures.contains(antiFeature)) {
                    return true;
                }
            } else {
                if (!shownAntiFeatures.contains(context.getResources().getString(R.string.antiothers_key))) {
                    return true;
                }
            }
        }

        return false;
    }

    @Nullable
    public String getBitcoinUri() {
        return TextUtils.isEmpty(bitcoin) ? null : "bitcoin:" + bitcoin;
    }

    @Nullable
    public String getLitecoinUri() {
        return TextUtils.isEmpty(litecoin) ? null : "litecoin:" + litecoin;
    }

    @Nullable
    public String getOpenCollectiveUri() {
        return TextUtils.isEmpty(openCollective) ? null : "https://opencollective.com/"
                + openCollective + "/donate/";
    }

    @Nullable
    public String getFlattrUri() {
        return TextUtils.isEmpty(flattrID) ? null : "https://flattr.com/thing/" + flattrID;
    }

    @Nullable
    public String getLiberapayUri() {
        return TextUtils.isEmpty(liberapay) ? null : "https://liberapay.com/" + liberapay;
    }

    /**
     * {@link PackageManager} doesn't give us {@code minSdkVersion}, {@code targetSdkVersion},
     * and {@code maxSdkVersion}, so we have to parse it straight from {@code <uses-sdk>} in
     * {@code AndroidManifest.xml}.  If {@code targetSdkVersion} is not set, then it is
     * equal to {@code minSdkVersion}
     *
     * @see <a href="https://developer.android.com/guide/topics/manifest/uses-sdk-element.html">&lt;uses-sdk&gt;</a>
     */
    @SuppressWarnings("unused") // TODO port to lib
    private static int[] getMinTargetMaxSdkVersions(Context context, String packageName) {
        int minSdkVersion = Apk.SDK_VERSION_MIN_VALUE;
        int targetSdkVersion = Apk.SDK_VERSION_MIN_VALUE;
        int maxSdkVersion = Apk.SDK_VERSION_MAX_VALUE;
        try {
            AssetManager am = context.createPackageContext(packageName, 0).getAssets();
            XmlResourceParser xml = am.openXmlResourceParser("AndroidManifest.xml");
            int eventType = xml.getEventType();
            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG && "uses-sdk".equals(xml.getName())) {
                    for (int j = 0; j < xml.getAttributeCount(); j++) {
                        if (xml.getAttributeName(j).equals("minSdkVersion")) {
                            minSdkVersion = Integer.parseInt(xml.getAttributeValue(j));
                        } else if (xml.getAttributeName(j).equals("targetSdkVersion")) {
                            targetSdkVersion = Integer.parseInt(xml.getAttributeValue(j));
                        } else if (xml.getAttributeName(j).equals("maxSdkVersion")) {
                            maxSdkVersion = Integer.parseInt(xml.getAttributeValue(j));
                        }
                    }
                    break;
                }
                eventType = xml.nextToken();
            }
        } catch (PackageManager.NameNotFoundException
                 | IOException
                 | XmlPullParserException
                 | NumberFormatException e) {
            Log.e(TAG, "Could not get min/max sdk version", e);
        }
        if (targetSdkVersion < minSdkVersion) {
            targetSdkVersion = minSdkVersion;
        }
        return new int[]{minSdkVersion, targetSdkVersion, maxSdkVersion};
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public boolean isUninstallable(Context context) {
        if (this.isMediaInstalled(context)) {
            return true;
        } else if (this.isInstalled(context)) {
            PackageManager pm = context.getPackageManager();
            ApplicationInfo appInfo;
            try {
                appInfo = pm.getApplicationInfo(this.packageName,
                        PackageManager.GET_UNINSTALLED_PACKAGES);
            } catch (PackageManager.NameNotFoundException e) {
                return false;
            }

            // System apps aren't uninstallable.
            final boolean isSystem = (appInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0;
            return !isSystem && this.isInstalled(context);
        } else {
            return false;
        }
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeByte(this.compatible ? (byte) 1 : (byte) 0);
        dest.writeString(this.packageName);
        dest.writeString(this.name);
        dest.writeLong(this.repoId);
        dest.writeString(this.summary);
        dest.writeString(this.iconFile == null ? null : this.iconFile.serialize());
        dest.writeString(this.description);
        dest.writeString(this.whatsNew);
        dest.writeString(this.license);
        dest.writeString(this.authorName);
        dest.writeString(this.authorEmail);
        dest.writeString(this.webSite);
        dest.writeString(this.issueTracker);
        dest.writeString(this.sourceCode);
        dest.writeString(this.translation);
        dest.writeString(this.video);
        dest.writeString(this.changelog);
        dest.writeString(this.donate);
        dest.writeString(this.bitcoin);
        dest.writeString(this.litecoin);
        dest.writeString(this.flattrID);
        dest.writeString(this.liberapay);
        dest.writeString(this.openCollective);
        dest.writeString(this.preferredSigner);
        dest.writeString(this.suggestedVersionName);
        dest.writeInt(this.suggestedVersionCode);
        dest.writeString(this.autoInstallVersionName);
        dest.writeLong(this.autoInstallVersionCode);
        dest.writeLong(this.added != null ? this.added.getTime() : -1);
        dest.writeLong(this.lastUpdated != null ? this.lastUpdated.getTime() : -1);
        dest.writeStringArray(this.categories);
        dest.writeStringArray(this.antiFeatures);
        dest.writeString(this.featureGraphic == null ? null : this.featureGraphic.serialize());
        dest.writeString(this.promoGraphic == null ? null : this.promoGraphic.serialize());
        dest.writeString(this.tvBanner == null ? null : this.tvBanner.serialize());
        dest.writeStringList(Utils.toString(this.phoneScreenshots));
        dest.writeStringList(Utils.toString(this.sevenInchScreenshots));
        dest.writeStringList(Utils.toString(this.tenInchScreenshots));
        dest.writeStringList(Utils.toString(this.tvScreenshots));
        dest.writeStringList(Utils.toString(this.wearScreenshots));
        dest.writeByte(this.isApk ? (byte) 1 : (byte) 0);
        dest.writeString(this.installedVersionName);
        dest.writeLong(this.installedVersionCode);
        dest.writeParcelable(this.installedApk, flags);
        dest.writeString(this.installedSigner);
    }

    protected App(Parcel in) {
        this.compatible = in.readByte() != 0;
        this.packageName = in.readString();
        this.name = in.readString();
        this.repoId = in.readLong();
        this.summary = in.readString();
        this.iconFile = FileV2.deserialize(in.readString());
        this.description = in.readString();
        this.whatsNew = in.readString();
        this.license = in.readString();
        this.authorName = in.readString();
        this.authorEmail = in.readString();
        this.webSite = in.readString();
        this.issueTracker = in.readString();
        this.sourceCode = in.readString();
        this.translation = in.readString();
        this.video = in.readString();
        this.changelog = in.readString();
        this.donate = in.readString();
        this.bitcoin = in.readString();
        this.litecoin = in.readString();
        this.flattrID = in.readString();
        this.liberapay = in.readString();
        this.openCollective = in.readString();
        this.preferredSigner = in.readString();
        this.suggestedVersionName = in.readString();
        this.suggestedVersionCode = in.readInt();
        this.autoInstallVersionName = in.readString();
        this.autoInstallVersionCode = in.readLong();
        long tmpAdded = in.readLong();
        this.added = tmpAdded == -1 ? null : new Date(tmpAdded);
        long tmpLastUpdated = in.readLong();
        this.lastUpdated = tmpLastUpdated == -1 ? null : new Date(tmpLastUpdated);
        this.categories = in.createStringArray();
        this.antiFeatures = in.createStringArray();
        this.featureGraphic = FileV2.deserialize(in.readString());
        this.promoGraphic = FileV2.deserialize(in.readString());
        this.tvBanner = FileV2.deserialize(in.readString());
        this.phoneScreenshots = Utils.fileV2FromStrings(in.createStringArrayList());
        this.sevenInchScreenshots = Utils.fileV2FromStrings(in.createStringArrayList());
        this.tenInchScreenshots = Utils.fileV2FromStrings(in.createStringArrayList());
        this.tvScreenshots = Utils.fileV2FromStrings(in.createStringArrayList());
        this.wearScreenshots = Utils.fileV2FromStrings(in.createStringArrayList());
        this.isApk = in.readByte() != 0;
        this.installedVersionName = in.readString();
        this.installedVersionCode = in.readLong();
        this.installedApk = in.readParcelable(Apk.class.getClassLoader());
        this.installedSigner = in.readString();
    }

    public static final Parcelable.Creator<App> CREATOR = new Parcelable.Creator<App>() {
        @Override
        public App createFromParcel(Parcel source) {
            return new App(source);
        }

        @Override
        public App[] newArray(int size) {
            return new App[size];
        }
    };

    /**
     * Choose the APK Signing Certificate aka "signer" which we should
     * encourage the user to install. Usually, we want the
     * {@link #preferredSigner} rather than any random signer. However, if the
     * package is installed, then we override this and instead want to only
     * encourage the user to try and install versions with that signer (because
     * that is all the OS will let them do).
     * <p>
     * Will return null for any {@link App} which represents media (instead of an apk) and thus
     * doesn't have a signer.
     */
    @Nullable
    private String getMostAppropriateSigner() {
        if (!TextUtils.isEmpty(installedSigner)) {
            return installedSigner;
        } else if (!TextUtils.isEmpty(preferredSigner)) {
            return preferredSigner;
        }

        return null;
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package xml;

import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.ParserPool;
import org.springframework.context.annotation.Bean;

public class SAMLIgnoreComments {
  @Bean
  ParserPool parserPool() {
    BasicParserPool pool = new BasicParserPool();
    // ruleid: java_xml_rule-SAMLIgnoreComments
    pool.setIgnoreComments(false);
    return pool;
  }

  @Bean
  void parserPool2() {
    boolean shouldIgnore = false;
    BasicParserPool pool = new BasicParserPool();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
  }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package strings;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ModifyAfterValidation {

    public String modifyDangerReplaceAll(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        s = s.replaceAll("[\\p{Cn}]", ""); // modified after validation
        return s;
    }

    public String modifyDangerConcat(String str, String stringVal) {
        Boolean isTrue = false;
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

        // ruleid: java_strings_rule-ModifyAfterValidation
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        if(isTrue){
            someMethod(s.concat(stringVal));
        }
        return s;
    }

    public String modifyDangerReplace(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        s = s.replace("[\\p{Cn}]", ""); // modified after validation
        return s;
    }

    public String modifyDangerReplaceFirst(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);
        Pattern pattern = Pattern.compile("<script>");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        s = s.replaceFirst("[\\p{Cn}]", ""); // modified after validation
        return s;
    }

    public String modifySafeReplaceAll(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.replaceAll("[\\p{Cn}]", "");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }

    public String modifySafeReplace(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.replace("[\\p{Cn}]", "");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }

    public String modifySafeReplaceFirst(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.replaceFirst("[\\p{Cn}]", "");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);

        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }

    public String modifySafeConcat(String str) {
        String s = Normalizer.normalize(str, Normalizer.Form.NFKC);

        // ok: java_strings_rule-ModifyAfterValidation
        s = s.concat("test");

        Pattern pattern = Pattern.compile("<script>");
        Matcher matcher = pattern.matcher(s);
        
        if (matcher.find()) {
            throw new IllegalArgumentException("Invalid input");
        }
        return s;
    }


    public String someMethod(String input){
        return input;
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// source (original): https://github.com/semgrep/semgrep-rules/blob/release/java/spring/security/injection/tainted-file-path.java
package traversal;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import java.nio.file.*;
import javax.xml.transform.*;

@RestController
public class PreflightController {
    static final String CONTENT_DISPOSITION_STATIC_FILE_LOCATION = "contentDispositionUpload";
    private UnrestrictedFileUpload unrestrictedFileUpload;

    public PreflightController(UnrestrictedFileUpload unrestrictedFileUpload) {
        this.unrestrictedFileUpload = unrestrictedFileUpload;
    }

    @RequestMapping(
            CONTENT_DISPOSITION_STATIC_FILE_LOCATION + "/" + "{fileName}")
    public ResponseEntity<byte[]> fetchFile(@PathVariable("fileName") String fileName)
            throws IOException {
        InputStream inputStream =
                // ruleid: java_traversal_rule-RelativePathTraversal
                new FileInputStream(
                        unrestrictedFileUpload.getContentDispositionRoot().toFile()
                                + "/"
                                + fileName);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment");
        return new ResponseEntity<byte[]>(
                IOUtils.toByteArray(inputStream), httpHeaders, HttpStatus.OK);
    }

    public static void bad(@RequestParam String user)
    {
        Socket sock;
        BufferedReader filenameReader = new BufferedReader(
                new InputStreamReader(sock.getInputStream(), "UTF-8"));
        String filename = filenameReader.readLine();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String fileLine = fileReader.readLine();
        while(fileLine != null) {
            sock.getOutputStream().write(fileLine.getBytes());
            fileLine = fileReader.readLine();
        }
    }

    public static void bad2(@RequestParam String filename)
    {
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext(new String[] {"If-you-have-any.xml"});

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        try {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();

        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void ok(@RequestParam String filename)
    {
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext(new String[] {"If-you-have-any.xml"});

        // ok: java_traversal_rule-RelativePathTraversal
        Resource resource =
                appContext.getResource("classpath:com/" + org.apache.commons.io.FilenameUtils.getName(filename));

        try {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();

        } catch(IOException e){
            e.printStackTrace();
        }
    }

    @Test
    public void whenResourceAsFile_thenReadSuccessful(@RequestParam String filename) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String employees = new String(Files.readAllBytes(resource.toPath()));
        assertEquals("Joe Employee,Jan Employee,James T. Employee", employees);
    }

    public String bad3(@RequestParam String filename) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad4(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            BufferedReader reader = new BufferedReader(fileReader);
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad5(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            BufferedReader reader = Files.newBufferedReader(path);
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad6(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad7(@RequestParam String fileName, @RequestParam String content) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            fos.write(content.getBytes());
            fos.close();
            return "File written successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error writing to the file";
        }
    }

    public String bad8(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(source, new StreamResult(writer));
            String result = writer.toString();
            return "File content: " + result;
        } catch (Exception e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad9(@RequestParam String fileName, @RequestParam String content) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            os.write(content.getBytes());
            os.close();
            return "File written successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error writing to the file";
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-1", new ExampleProvider());
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: MIT (c) GitLab Inc.

package crypto;

class Cipher {

    javax.crypto.Cipher cipher;

    public void config1() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-CipherECBMode
        cipher = javax.crypto.Cipher.getInstance("DESede/ECB/NoPadding");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_crypto_rule-CipherECBMode
        cipher = javax.crypto.Cipher.getInstance("AES/GCM/PKCS1Padding");
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SqlInjection
        handle.createUpdate(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.io.FilenameUtils.*;

@WebServlet(name = "FilenameUtils", value = "/FilenameUtils")
public class FilenameUtils extends HttpServlet {

    final static String basePath = "/usr/local/lib";

    //Method where input is not sanitized with normalize:

    //Normal Input (opens article as intended):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (returns restricted file):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Input from From Servlet Param
        String input = req.getHeader("input");

        String method = req.getHeader("method");

        String fullPath = null;

        if(method != null && !method.isEmpty()) {
            switch (method) {
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPath'
                case "getFullPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPathNoEndSeparator'
                case "getFullPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPath'
                case "getPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPathNoEndSeparator'
                case "getPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalize'
                case "normalize": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalizeNoEndSeparator'
                case "normalizeNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: removeExtension'
                case "removeExtension": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToSystem'
                case "separatorsToSystem": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToUnix'
                case "separatorsToUnix": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                case "concat": {
                    // ruleid: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }
                default: {
                    input = "article.txt";
                    // ok: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }

            }
        }

        // Remove whitespaces
        fullPath = fullPath.replace(" ","");

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method not using user input
    //curl --location --request POST 'http://localhost:8080/ServletSample/FilenameUtils'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String filepath = "article.txt";

        //ok: java_file_rule-FilenameUtils
        String fullPath = concat(basePath, filepath);

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method where input is sanitized with getName:

    //Normal Input (opens article as intended):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (causes exception):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String input = req.getHeader("input");

        // Securely combine the user input with the base directory
        input = getName(input); // Extracts just the filename, no paths
        // Construct the safe, absolute path
        //ok: java_file_rule-FilenameUtils
        String safePath = concat(basePath, input);

        System.out.println(safePath);

        // Read the contents of the file
        File file = new File(safePath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
 
    protected void safe(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        
        String input = req.getHeader("input");

        String base = "/var/app/restricted";
        Path basePath = Paths.get(base);
        // ok: java_file_rule-FilenameUtils
        String inputPath = getPath(input);
        // Resolve the full path, but only use our random generated filename
        Path fullPath = basePath.resolve(inputPath);
        // verify the path is contained within our basePath
        if (!fullPath.startsWith(base)) {
            throw new Exception("Invalid path specified!");
        }
        
        // Read the contents of the file
        File file = new File(fullPath.toString());
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        File.createTempFile("safe", input); // BAD, DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package cookie;

import org.apache.commons.text.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class HttpResponseSplitting extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        Cookie c = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String paramNames = req.getParameterNames().nextElement();
        Cookie cParamNames = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamNames.setHttpOnly(true);
        cParamNames.setSecure(true);
        resp.addCookie(cParamNames);

        String paramValues = req.getParameterValues("input")[0];
        Cookie cParamValues = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamValues.setHttpOnly(true);
        cParamValues.setSecure(true);
        resp.addCookie(cParamValues);

        String paramMap = req.getParameterMap().get("input")[0];
        Cookie cParamMap = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamMap.setHttpOnly(true);
        cParamMap.setSecure(true);
        resp.addCookie(cParamMap);

        String header = req.getHeader("input");
        Cookie cHeader = new Cookie("name", null);
        //ruleid: java_cookie_rule-HttpResponseSplitting
        c.setValue(header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = StringEscapeUtils.escapeJava(data);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = StringEscapeUtils.escapeJava(header);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = StringEscapeUtils.escapeJava(contextPath);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // BAD
        String tainted = req.getParameter("input");
        resp.setHeader("test", tainted);

        // OK: False negative but reported by spotbugs
        String data = req.getParameter("input");
        String normalized = data.replaceAll("\n", "\n");
        resp.setHeader("test", normalized);

        // OK: False negative but reported by spotbugs
        String normalized2 = data.replaceAll("\n", req.getParameter("test"));
        resp.setHeader("test2", normalized2);

        // OK
        String normalized3 = org.apache.commons.text.StringEscapeUtils.unescapeJava(tainted);
        resp.setHeader("test3", normalized3);

        // BAD
        String normalized4 = getString(tainted);
        resp.setHeader("test4", normalized4);

        // BAD
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
        wrapper.addHeader("test", tainted);
        wrapper.setHeader("test2", tainted);

    }

    private String getString(String s) {
        return s;
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("MD4", "SUN");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package password;

import com.hazelcast.config.SymmetricEncryptionConfig;
import io.vertx.ext.web.handler.CSRFHandler;
import java.net.PasswordAuthentication;
import java.security.KeyStore;
import java.security.KeyStore.PasswordProtection;
import java.sql.DriverManager;
import javax.crypto.spec.PBEKeySpec;
import javax.net.ssl.KeyManagerFactory;
import javax.security.auth.callback.PasswordCallback;
import javax.security.auth.kerberos.KerberosKey;

public class HardcodePassword {
    private final String passwordString = "secret";
    private final SymmetricEncryptionConfig passwordS = new SymmetricEncryptionConfig();

    public void danger1(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
	// ruleid: java_password_rule-HardcodePassword
        new KeyStore.PasswordProtection(passwordString.toCharArray());
    }

    public void danger2(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger3(KeyStore ks, String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger4(String password) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger5(KeyManagerFactory kmf, String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger10(String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_password_rule-HardcodePassword
        DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", password);
    }

    public void danger11(String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger12(SymmetricEncryptionConfig s, String password) throws Exception{
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_password_rule-HardcodePassword
        s.setPassword(password);
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/crypto/use-of-rc4.java

import javax.crypto.Cipher;

class RC4 {
    String algo2 = "RC4";

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() throws Exception {
        String algo = "RC4";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() throws Exception {
        // ruleid: java_crypto_rule-UseOfRC4
        Cipher.getInstance(algo2);
    }

    public void bad4() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void ok() {
        // ok: java_crypto_rule-UseOfRC4
        Cipher.getInstance("AES/CBC/PKCS7PADDING");
    }
}

// License: GNU Lesser General Public License v3.0
// source (original): https://github.com/mobsf/mobsfscan/blob/main/tests/assets/rules/semgrep/webview/webview_ignore_ssl_errors.java
// hash: e29e85c3

package com.example;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.webkit.SslErrorHandler;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class MyActivity extends Activity {
    // here you can put your url address
    private static final String URL = "http://www.google.ro";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        WebView myWebView = (WebView)findViewById(R.id.webview);

        WebSettings settings = myWebView.getSettings();

        settings.setJavaScriptEnabled(true);
        // By using this method together with the overridden method onReceivedSslError()
        // you will avoid the "WebView Blank Page" problem to appear. This might happen if you
        // use a "https" url!
        settings.setDomStorageEnabled(true);

        myWebView.loadUrl(URL);

        myWebView.setWebViewClient(new MyWebViewClient());

    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            if (Uri.parse(url).getHost().equals(URL)) {
                // This is your web site, so do not override; let the WebView to load the page
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
            startActivity(intent);
            return true;
        }

        // ruleid:rules_lgpl_java_webview_rule-ignore-ssl-certificate-errors
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);

            // this will ignore the Ssl error and will go forward to your site
            handler.proceed();
        }
    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Provider;
import java.security.Signature;

public class WeakMessageDigest {

    public static void weakDigestMoreSig() throws NoSuchProviderException, NoSuchAlgorithmException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("MD5");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("sha-384","SUN"); //OK!
        // ok: java_crypto_rule-WeakMessageDigest
        MessageDigest.getInstance("SHA-512", "SUN"); //OK!

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok: java_crypto_rule-WeakMessageDigest
        Signature.getInstance("SHA256withRSA"); //OK
        Signature.getInstance("uncommon name", ""); //OK
    }

    static class ExampleProvider extends Provider {
        protected ExampleProvider() {
            super("example", 1.0, "");
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
        // ruleid: java_inject_rule-LDAPInjection
        NamingEnumeration<SearchResult> results = ctx.search(base, filter,filters, sc);
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileInputStream(input); // BAD, DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        Files.createTempDirectory(p,input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package xss;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Radio;
//import org.apache.wicket.extensions.markup.html.form.select.SelectOptions.SimpleSelectOption;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.model.util.ListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;

class WicketXSS extends WebPage {
    public void XssWicketExamplePage(PageParameters pageParameters) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // http://www.java2s.com/example/java-src/pkg/org/jabylon/rest/ui/wicket/components/customfeedbackpanel-edea7.html
    protected Component newMessageDisplayComponent(String id, String message) {
        MultiLineLabel label = new MultiLineLabel(id, message);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return label;
    }

    public void setText(String txt, Component c) {

        if (txt == null || txt.trim().length() == 0) {
            c.setVisible(false);
        } else {
            c.setDefaultModelObject(txt);
            c.setVisible(true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
    }

    protected void newLabel(Label lab) {
        final Component label = lab;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // https://github.com/apache/wicket/blob/e9461b0d115a7dbf4992596823521f6e038817d9/wicket-user-guide/src/main/asciidoc/forms2/forms2_3.adoc#L134
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void advancedSearchPage(String input) {

        Form<?> form = new Form<>("form");
        // Options for the dropdown
        List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

        // Model for the selected value
        Model<String> selectedOption = new Model<>("Option 1 " + input);

        // Create a DropDownChoice and add it to the form
        DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        form.add(dropdown);

        add(form);

    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Radio/setEscapeModelStrings
    private String getFormComponentValue(final Radio<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
        // ruleid: java_xss_rule-WicketXSS
        formComponent.setEscapeModelStrings(false);
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Check/setEscapeModelStrings
    private String getFormComponentValue2(final Check<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }
}

class MyPages extends WebPage {

    private ModalWindow modalWindow;

    public MyPages() {
        // Create a ModalWindow and set its title
        modalWindow = new ModalWindow("modalWindow");
        modalWindow.setTitle("Potentially Unsafe Title");

        // Explicitly disable escaping of the model strings (title)
        // for CVE-2015-5347/
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        add(modalWindow);
    }
}

class MyPage extends WebPage {

    public MyPage(List<String> userGeneratedOptions, String input) {

        Form<Void> form = new Form<>("form");

        Model<String> selectedOption = Model.of("");

        RadioGroup<String> radioGroup = new RadioGroup<>("radioGroup", selectedOption);
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        for (String option : userGeneratedOptions) {
            radioGroup.add(new Radio<>("radio" + option, Model.of(option)));
        }

        form.add(radioGroup);

        // https://nightlies.apache.org/wicket/apidocs/8.x/org/apache/wicket/markup/html/form/CheckBoxMultipleChoice.html
        List SITES = Arrays.asList(new String[] { "The Server Side", "Java Lobby", input });
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ruleid: java_inject_rule-LDAPInjection
        template.lookup(jndiInjectMe, mapper);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/file/FileDisclosure.java
// hash: a7694d0

package injection;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

// java_inject_rule-FileDisclosureRequestDispatcher
public class FileDisclosureRequestDispatcher extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String jspFile = request.getParameter("jspFile");

            RequestDispatcher requestDispatcher = request.getRequestDispatcher(jspFile);

            // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
            requestDispatcher.include(request, response);

            requestDispatcher = request.getSession().getServletContext().getRequestDispatcher(jspFile);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            // Create a look up table or pull from a data source
            HashMap<String, String> lookupTable = new HashMap<>();
            lookupTable.put("key1", "/ServletSample/simpleForm.jsp");
            // Get user input
            String userInput = request.getParameter("key");
            // Look up resource to redirect to from the user input
            String redirectValue = lookupTable.getOrDefault(userInput, "/ServletSample/index.html");
            // Redirect the user
            // ok: java_inject_rule-FileDisclosureRequestDispatcher
            response.sendRedirect(redirectValue);

            RequestDispatcher requestDispatcherTwo = request.getSession().getServletContext().getRequestDispatcher(redirectValue);
            // ok: java_inject_rule-FileDisclosureRequestDispatcher
            requestDispatcherTwo.forward(request, response);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.net.Socket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;

class Bad {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void badsocket1() {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            out = new PrintWriter(pingSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

            out.println("ping");
            System.out.println(in.readLine());
            out.close();
            in.close();
            pingSocket.close();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void badsocket2() throws Exception {
        // ruleid: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ftp://example.com", 21);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket3() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket4() throws Exception {
        String servername = "telnet://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket5() throws Exception {
        String servername = "ftp://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket6() throws Exception {
        String servername = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket7() throws Exception {
        String servername = "TELNET://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket8() throws Exception {
        String servername = "FTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket9() throws Exception {
        String servername = "HTTP://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket10() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        out = new PrintWriter(pingSocket.getOutputStream(), true);
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        pingSocket.close();
    }

    public void badsocket11() throws Exception {
        BufferedReader in = null;
        Socket pingSocket = null;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        String serverResponse;
        while ((serverResponse = in.readLine()) != null) {
            System.out.println("Server: " + serverResponse);
        }

        System.out.println(in.readLine());
        in.close();
        pingSocket.close();
    }

    public void badsocket12() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket13() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket14() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket15() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket16() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

class Ok {

    BufferedReader in = null;
    PrintWriter out = null;
    Socket pingSocket = null;

    public void oksocket1() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("ssh://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket2() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("sftp://example.com", 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket3() throws Exception {
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket("https://example.com", 443);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket4() throws Exception {
        String servername = "ssh://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket5() throws Exception {
        String servername = "sftp://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 22);

        out = new PrintWriter(pingSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void oksocket6() throws Exception {
        String servername = "https://example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));

        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }

    public void badsocket17() throws Exception {
        String servername = "example.com";
        // ok: java_crypto_rule-SocketRequestUnsafeProtocols
        pingSocket = new Socket(servername, 443);
        out = new PrintWriter(pingSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(pingSocket.getInputStream()));
        out.println("ping");
        System.out.println(in.readLine());
        out.close();
        in.close();
        pingSocket.close();
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package strings;
import java.util.Formatter;
import java.util.Locale;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;

public class FormatStringManipulation extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

        // create a new formatter
        StringBuffer buffer = new StringBuffer();
        Formatter formatter = new Formatter(buffer, Locale.US);
        String input = request.getParameter("suffix");
        String format = "The customer: %s %s" + input;

        //test cases
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        //false positive test
        formatter.format("The customer: %s %s", "John", request.getParameter("testParam")); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_strings_rule-FormatStringManipulation
        System.out.printf(Locale.US, format, "John", "Smith"); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String format2 = "The customer: %s %s" + request.getParameter("suffix");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import org.apache.commons.net.ftp.FTPClient;

class FTPInsecureTransport {

    public void run() throws Exception {
        System.out.println("The connections will time out after 1 second each as server addresses does not exist.");
        unsafe1();
        unsafe2();
        safe1();
    }

    public static void unsafe1() {
        System.out.println("Running Unsafe1()");
        String server = "www.yourserver.net";
        int port = 21;

        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient.setConnectTimeout(1000);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public static void unsafe2() {
        System.out.println("Running Unsafe2()");
        try {
            // ruleid: java_ftp_rule-FTPInsecureTransport
            URL url = new URL("ftp://user01:pass1234@ftp.foo.com/README.txt;type=i");
            URLConnection urlc = url.openConnection();
            urlc.setConnectTimeout(1000);
            InputStream is = urlc.getInputStream();
        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }

    public static void safe1() {
        System.out.println("Running Safe1()");
        try {
            // ok: java_ftp_rule-FTPInsecureTransport
            URL url = new URL("http://somerandomurl23432.com");
            URLConnection urlc = url.openConnection();

        } catch (Exception e) {
            System.out.println(e.getLocalizedMessage());
        }
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package xss;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Radio;
//import org.apache.wicket.extensions.markup.html.form.select.SelectOptions.SimpleSelectOption;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.model.util.ListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;

class WicketXSS extends WebPage {
    public void XssWicketExamplePage(PageParameters pageParameters) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // http://www.java2s.com/example/java-src/pkg/org/jabylon/rest/ui/wicket/components/customfeedbackpanel-edea7.html
    protected Component newMessageDisplayComponent(String id, String message) {
        MultiLineLabel label = new MultiLineLabel(id, message);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return label;
    }

    public void setText(String txt, Component c) {

        if (txt == null || txt.trim().length() == 0) {
            c.setVisible(false);
        } else {
            c.setDefaultModelObject(txt);
            c.setVisible(true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
    }

    protected void newLabel(Label lab) {
        final Component label = lab;
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // https://github.com/apache/wicket/blob/e9461b0d115a7dbf4992596823521f6e038817d9/wicket-user-guide/src/main/asciidoc/forms2/forms2_3.adoc#L134
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void advancedSearchPage(String input) {

        Form<?> form = new Form<>("form");
        // Options for the dropdown
        List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

        // Model for the selected value
        Model<String> selectedOption = new Model<>("Option 1 " + input);

        // Create a DropDownChoice and add it to the form
        DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        form.add(dropdown);

        add(form);

    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Radio/setEscapeModelStrings
    private String getFormComponentValue(final Radio<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Check/setEscapeModelStrings
    private String getFormComponentValue2(final Check<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }
}

class MyPages extends WebPage {

    private ModalWindow modalWindow;

    public MyPages() {
        // Create a ModalWindow and set its title
        modalWindow = new ModalWindow("modalWindow");
        modalWindow.setTitle("Potentially Unsafe Title");

        // Explicitly disable escaping of the model strings (title)
        // for CVE-2015-5347/
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        add(modalWindow);
    }
}

class MyPage extends WebPage {

    public MyPage(List<String> userGeneratedOptions, String input) {

        Form<Void> form = new Form<>("form");

        Model<String> selectedOption = Model.of("");

        RadioGroup<String> radioGroup = new RadioGroup<>("radioGroup", selectedOption);
        // for CVE-2015-7520
        // ruleid: java_xss_rule-WicketXSS
        radioGroup.setEscapeModelStrings(false);

        for (String option : userGeneratedOptions) {
            radioGroup.add(new Radio<>("radio" + option, Model.of(option)));
        }

        form.add(radioGroup);

        // https://nightlies.apache.org/wicket/apidocs/8.x/org/apache/wicket/markup/html/form/CheckBoxMultipleChoice.html
        List SITES = Arrays.asList(new String[] { "The Server Side", "Java Lobby", input });
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

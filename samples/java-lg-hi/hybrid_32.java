public class EhDB {

    private static final String TAG = EhDB.class.getSimpleName();

    private static final int MAX_HISTORY_COUNT = 200;

    private static DaoSession sDaoSession;

    private static boolean sHasOldDB;
    private static boolean sNewDB;

    private static void upgradeDB(SQLiteDatabase db, int oldVersion) {
        switch (oldVersion) {
            case 1: // 1 to 2, add FILTER
                db.execSQL("CREATE TABLE IF NOT EXISTS \"FILTER\" (" + //
                        "\"_id\" INTEGER PRIMARY KEY ," + // 0: id
                        "\"MODE\" INTEGER NOT NULL ," + // 1: mode
                        "\"TEXT\" TEXT," + // 2: text
                        "\"ENABLE\" INTEGER);"); // 3: enable
            case 2: // 2 to 3, add ENABLE column to table FILTER
                db.execSQL("CREATE TABLE " + "\"FILTER2\" (" +
                        "\"_id\" INTEGER PRIMARY KEY ," +
                        "\"MODE\" INTEGER NOT NULL ," +
                        "\"TEXT\" TEXT," +
                        "\"ENABLE\" INTEGER);");
                db.execSQL("INSERT INTO \"FILTER2\" (" +
                        "_id, MODE, TEXT, ENABLE)" +
                        "SELECT _id, MODE, TEXT, 1 FROM FILTER;");
                db.execSQL("DROP TABLE FILTER");
                db.execSQL("ALTER TABLE FILTER2 RENAME TO FILTER");
            case 3: // 3 to 4, add PAGE_FROM and PAGE_TO column to QUICK_SEARCH
                db.execSQL("CREATE TABLE " + "\"QUICK_SEARCH2\" (" +
                        "\"_id\" INTEGER PRIMARY KEY ," +
                        "\"NAME\" TEXT," +
                        "\"MODE\" INTEGER NOT NULL ," +
                        "\"CATEGORY\" INTEGER NOT NULL ," +
                        "\"KEYWORD\" TEXT," +
                        "\"ADVANCE_SEARCH\" INTEGER NOT NULL ," +
                        "\"MIN_RATING\" INTEGER NOT NULL ," +
                        "\"PAGE_FROM\" INTEGER NOT NULL ," +
                        "\"PAGE_TO\" INTEGER NOT NULL ," +
                        "\"TIME\" INTEGER NOT NULL );");
                db.execSQL("INSERT INTO \"QUICK_SEARCH2\" (" +
                        "_id, NAME, MODE, CATEGORY, KEYWORD, ADVANCE_SEARCH, MIN_RATING, PAGE_FROM, PAGE_TO, TIME)" +
                        "SELECT _id, NAME, MODE, CATEGORY, KEYWORD, ADVANCE_SEARCH, MIN_RATING, -1, -1, TIME FROM QUICK_SEARCH;");
                db.execSQL("DROP TABLE QUICK_SEARCH");
                db.execSQL("ALTER TABLE QUICK_SEARCH2 RENAME TO QUICK_SEARCH");
        }
    }

    public static void initialize(Context context) {
        sHasOldDB = context.getDatabasePath("data").exists();

        DBOpenHelper helper = new DBOpenHelper(
                context.getApplicationContext(), "eh.db", null);

        SQLiteDatabase db = helper.getWritableDatabase();
        DaoMaster daoMaster = new DaoMaster(db);

        sDaoSession = daoMaster.newSession();
    }

    public static boolean needMerge() {
        return sNewDB && sHasOldDB;
    }

    public static void mergeOldDB(Context context) {
        sNewDB = false;

        OldDBHelper oldDBHelper = new OldDBHelper(context);
        SQLiteDatabase oldDB;
        try {
            oldDB = oldDBHelper.getReadableDatabase();
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            return;
        }

        // Get GalleryInfo list
        SparseJLArray<GalleryInfo> map = new SparseJLArray<>();
        try {
            Cursor cursor = oldDB.rawQuery("select * from " + OldDBHelper.TABLE_GALLERY, null);
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        GalleryInfo gi = new GalleryInfo();
                        gi.gid = cursor.getInt(0);
                        gi.token = cursor.getString(1);
                        gi.title = cursor.getString(2);
                        gi.posted = cursor.getString(3);
                        gi.category = cursor.getInt(4);
                        gi.thumb = cursor.getString(5);
                        gi.uploader = cursor.getString(6);
                        try {
                            // In 0.6.x version, NaN is stored
                            gi.rating = cursor.getFloat(7);
                        } catch (Throwable e) {
                            ExceptionUtils.throwIfFatal(e);
                            gi.rating = -1.0f;
                        }

                        map.put(gi.gid, gi);

                        cursor.moveToNext();
                    }
                }
                cursor.close();
            }
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            // Ignore
        }

        // Merge local favorites
        try {
            Cursor cursor = oldDB.rawQuery("select * from " + OldDBHelper.TABLE_LOCAL_FAVOURITE, null);
            if (cursor != null) {
                LocalFavoritesDao dao = sDaoSession.getLocalFavoritesDao();
                if (cursor.moveToFirst()) {
                    long i = 0L;
                    while (!cursor.isAfterLast()) {
                        // Get GalleryInfo first
                        long gid = cursor.getInt(0);
                        GalleryInfo gi = map.get(gid);
                        if (gi == null) {
                            Log.e(TAG, "Can't get GalleryInfo with gid: " + gid);
                            cursor.moveToNext();
                            continue;
                        }

                        LocalFavoriteInfo info = new LocalFavoriteInfo(gi);
                        info.setTime(i);
                        dao.insert(info);
                        cursor.moveToNext();
                        i++;
                    }
                }
                cursor.close();
            }
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            // Ignore
        }


        // Merge quick search
        try {
            Cursor cursor = oldDB.rawQuery("select * from " + OldDBHelper.TABLE_TAG, null);
            if (cursor != null) {
                QuickSearchDao dao = sDaoSession.getQuickSearchDao();
                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        QuickSearch quickSearch = new QuickSearch();

                        int mode = cursor.getInt(2);
                        String search = cursor.getString(4);
                        String tag = cursor.getString(7);
                        if (mode == ListUrlBuilder.MODE_UPLOADER && search != null &&
                                search.startsWith("uploader:")) {
                            search = search.substring("uploader:".length());
                        }

                        quickSearch.setTime(cursor.getInt(0));
                        quickSearch.setName(cursor.getString(1));
                        quickSearch.setMode(mode);
                        quickSearch.setCategory(cursor.getInt(3));
                        quickSearch.setKeyword(mode == ListUrlBuilder.MODE_TAG ? tag : search);
                        quickSearch.setAdvanceSearch(cursor.getInt(5));
                        quickSearch.setMinRating(cursor.getInt(6));

                        dao.insert(quickSearch);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
            }
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            // Ignore
        }

        // Merge download info
        try {
            Cursor cursor = oldDB.rawQuery("select * from " + OldDBHelper.TABLE_DOWNLOAD, null);
            if (cursor != null) {
                DownloadsDao dao = sDaoSession.getDownloadsDao();
                if (cursor.moveToFirst()) {
                    long i = 0L;
                    while (!cursor.isAfterLast()) {
                        // Get GalleryInfo first
                        long gid = cursor.getInt(0);
                        GalleryInfo gi = map.get(gid);
                        if (gi == null) {
                            Log.e(TAG, "Can't get GalleryInfo with gid: " + gid);
                            cursor.moveToNext();
                            continue;
                        }

                        DownloadInfo info = new DownloadInfo(gi);
                        int state = cursor.getInt(2);
                        int legacy = cursor.getInt(3);
                        if (state == DownloadInfo.STATE_FINISH && legacy > 0) {
                            state = DownloadInfo.STATE_FAILED;
                        }
                        info.setState(state);
                        info.setLegacy(legacy);
                        if (cursor.getColumnCount() == 5) {
                            info.setTime(cursor.getLong(4));
                        } else {
                            info.setTime(i);
                        }
                        dao.insert(info);
                        cursor.moveToNext();
                        i++;
                    }
                }
                cursor.close();
            }
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            // Ignore
        }

        try {
            // Merge history info
            Cursor cursor = oldDB.rawQuery("select * from " + OldDBHelper.TABLE_HISTORY, null);
            if (cursor != null) {
                HistoryDao dao = sDaoSession.getHistoryDao();
                if (cursor.moveToFirst()) {
                    while (!cursor.isAfterLast()) {
                        // Get GalleryInfo first
                        long gid = cursor.getInt(0);
                        GalleryInfo gi = map.get(gid);
                        if (gi == null) {
                            Log.e(TAG, "Can't get GalleryInfo with gid: " + gid);
                            cursor.moveToNext();
                            continue;
                        }

                        HistoryInfo info = new HistoryInfo(gi);
                        info.setMode(cursor.getInt(1));
                        info.setTime(cursor.getLong(2));
                        dao.insert(info);
                        cursor.moveToNext();
                    }
                }
                cursor.close();
            }
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            // Ignore
        }

        try {
            oldDBHelper.close();
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            // Ignore
        }
    }

    public static synchronized List<DownloadInfo> getAllDownloadInfo() {
        DownloadsDao dao = sDaoSession.getDownloadsDao();
        List<DownloadInfo> list = dao.queryBuilder().orderDesc(DownloadsDao.Properties.Time).list();
        // Fix state
        for (DownloadInfo info : list) {
            if (info.state == DownloadInfo.STATE_WAIT || info.state == DownloadInfo.STATE_DOWNLOAD) {
                info.state = DownloadInfo.STATE_NONE;
            }
        }
        return list;
    }

    // Insert or update
    public static synchronized void putDownloadInfo(DownloadInfo downloadInfo) {
        DownloadsDao dao = sDaoSession.getDownloadsDao();
        if (null != dao.load(downloadInfo.gid)) {
            // Update
            dao.update(downloadInfo);
        } else {
            // Insert
            dao.insert(downloadInfo);
        }
    }

    public static synchronized void removeDownloadInfo(long gid) {
        sDaoSession.getDownloadsDao().deleteByKey(gid);
    }

    @Nullable
    public static synchronized String getDownloadDirname(long gid) {
        DownloadDirnameDao dao = sDaoSession.getDownloadDirnameDao();
        DownloadDirname raw = dao.load(gid);
        if (raw != null) {
            return raw.getDirname();
        } else {
            return null;
        }
    }

    /**
     * Insert or update
     */
    public static synchronized void putDownloadDirname(long gid, String dirname) {
        DownloadDirnameDao dao = sDaoSession.getDownloadDirnameDao();
        DownloadDirname raw = dao.load(gid);
        if (raw != null) { // Update
            raw.setDirname(dirname);
            dao.update(raw);
        } else { // Insert
            raw = new DownloadDirname();
            raw.setGid(gid);
            raw.setDirname(dirname);
            dao.insert(raw);
        }
    }

    public static synchronized void removeDownloadDirname(long gid) {
        DownloadDirnameDao dao = sDaoSession.getDownloadDirnameDao();
        dao.deleteByKey(gid);
    }

    public static synchronized void clearDownloadDirname() {
        DownloadDirnameDao dao = sDaoSession.getDownloadDirnameDao();
        dao.deleteAll();
    }

    @NonNull
    public static synchronized List<DownloadLabel> getAllDownloadLabelList() {
        DownloadLabelDao dao = sDaoSession.getDownloadLabelDao();
        return dao.queryBuilder().orderAsc(DownloadLabelDao.Properties.Time).list();
    }

    public static synchronized DownloadLabel addDownloadLabel(String label) {
        DownloadLabelDao dao = sDaoSession.getDownloadLabelDao();
        DownloadLabel raw = new DownloadLabel();
        raw.setLabel(label);
        raw.setTime(System.currentTimeMillis());
        raw.setId(dao.insert(raw));
        return raw;
    }

    public static synchronized DownloadLabel addDownloadLabel(DownloadLabel raw) {
        // Reset id
        raw.setId(null);
        DownloadLabelDao dao = sDaoSession.getDownloadLabelDao();
        raw.setId(dao.insert(raw));
        return raw;
    }

    public static synchronized void updateDownloadLabel(DownloadLabel raw) {
        DownloadLabelDao dao = sDaoSession.getDownloadLabelDao();
        dao.update(raw);
    }

    public static synchronized void moveDownloadLabel(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        boolean reverse = fromPosition > toPosition;
        int offset = reverse ? toPosition : fromPosition;
        int limit = reverse ? fromPosition - toPosition + 1 : toPosition - fromPosition + 1;

        DownloadLabelDao dao = sDaoSession.getDownloadLabelDao();
        List<DownloadLabel> list = dao.queryBuilder().orderAsc(DownloadLabelDao.Properties.Time)
                .offset(offset).limit(limit).list();

        int step = reverse ? 1 : -1;
        int start = reverse ? limit - 1 : 0;
        int end = reverse ? 0 : limit - 1;
        long toTime = list.get(end).getTime();
        for (int i = end; reverse ? i < start : i > start; i += step) {
            list.get(i).setTime(list.get(i + step).getTime());
        }
        list.get(start).setTime(toTime);

        dao.updateInTx(list);
    }

    public static synchronized void removeDownloadLabel(DownloadLabel raw) {
        DownloadLabelDao dao = sDaoSession.getDownloadLabelDao();
        dao.delete(raw);
    }

    public static synchronized List<GalleryInfo> getAllLocalFavorites() {
        LocalFavoritesDao dao = sDaoSession.getLocalFavoritesDao();
        List<LocalFavoriteInfo> list = dao.queryBuilder().orderDesc(LocalFavoritesDao.Properties.Time).list();
        List<GalleryInfo> result = new ArrayList<>();
        result.addAll(list);
        return result;
    }

    public static synchronized List<GalleryInfo> searchLocalFavorites(String query) {
        query = SqlUtils.sqlEscapeString("%" + query + "%");
        LocalFavoritesDao dao = sDaoSession.getLocalFavoritesDao();
        List<LocalFavoriteInfo> list = dao.queryBuilder().orderDesc(LocalFavoritesDao.Properties.Time)
                .where(LocalFavoritesDao.Properties.Title.like(query)).list();
        List<GalleryInfo> result = new ArrayList<>();
        result.addAll(list);
        return result;
    }

    public static synchronized void removeLocalFavorites(long gid) {
        sDaoSession.getLocalFavoritesDao().deleteByKey(gid);
    }

    public static synchronized void removeLocalFavorites(long[] gidArray) {
        LocalFavoritesDao dao = sDaoSession.getLocalFavoritesDao();
        for (long gid : gidArray) {
            dao.deleteByKey(gid);
        }
    }

    public static synchronized boolean containLocalFavorites(long gid) {
        LocalFavoritesDao dao = sDaoSession.getLocalFavoritesDao();
        return null != dao.load(gid);
    }

    public static synchronized void putLocalFavorites(GalleryInfo galleryInfo) {
        LocalFavoritesDao dao = sDaoSession.getLocalFavoritesDao();
        if (null == dao.load(galleryInfo.gid)) {
            LocalFavoriteInfo info;
            if (galleryInfo instanceof LocalFavoriteInfo) {
                info = (LocalFavoriteInfo) galleryInfo;
            } else {
                info = new LocalFavoriteInfo(galleryInfo);
                info.time = System.currentTimeMillis();
            }
            dao.insert(info);
        }
    }

    public static synchronized void putLocalFavorites(List<GalleryInfo> galleryInfoList) {
        for (GalleryInfo gi : galleryInfoList) {
            putLocalFavorites(gi);
        }
    }

    public static synchronized List<QuickSearch> getAllQuickSearch() {
        QuickSearchDao dao = sDaoSession.getQuickSearchDao();
        return dao.queryBuilder().orderAsc(QuickSearchDao.Properties.Time).list();
    }

    public static synchronized void insertQuickSearch(QuickSearch quickSearch) {
        QuickSearchDao dao = sDaoSession.getQuickSearchDao();
        quickSearch.id = null;
        quickSearch.time = System.currentTimeMillis();
        quickSearch.id = dao.insert(quickSearch);
    }

    public static synchronized void importQuickSearch(List<QuickSearch> quickSearchList) {
        QuickSearchDao dao = sDaoSession.getQuickSearchDao();
        for (QuickSearch quickSearch : quickSearchList) {
            dao.insert(quickSearch);
        }
    }

    public static synchronized void updateQuickSearch(QuickSearch quickSearch) {
        QuickSearchDao dao = sDaoSession.getQuickSearchDao();
        dao.update(quickSearch);
    }

    public static synchronized void deleteQuickSearch(QuickSearch quickSearch) {
        QuickSearchDao dao = sDaoSession.getQuickSearchDao();
        dao.delete(quickSearch);
    }

    public static synchronized void moveQuickSearch(int fromPosition, int toPosition) {
        if (fromPosition == toPosition) {
            return;
        }

        boolean reverse = fromPosition > toPosition;
        int offset = reverse ? toPosition : fromPosition;
        int limit = reverse ? fromPosition - toPosition + 1 : toPosition - fromPosition + 1;

        QuickSearchDao dao = sDaoSession.getQuickSearchDao();
        List<QuickSearch> list = dao.queryBuilder().orderAsc(QuickSearchDao.Properties.Time)
                .offset(offset).limit(limit).list();

        int step = reverse ? 1 : -1;
        int start = reverse ? limit - 1 : 0;
        int end = reverse ? 0 : limit - 1;
        long toTime = list.get(end).getTime();
        for (int i = end; reverse ? i < start : i > start; i += step) {
            list.get(i).setTime(list.get(i + step).getTime());
        }
        list.get(start).setTime(toTime);

        dao.updateInTx(list);
    }

    public static synchronized LazyList<HistoryInfo> getHistoryLazyList() {
        return sDaoSession.getHistoryDao().queryBuilder().orderDesc(HistoryDao.Properties.Time).listLazy();
    }

    public static synchronized void putHistoryInfo(GalleryInfo galleryInfo) {
        HistoryDao dao = sDaoSession.getHistoryDao();
        HistoryInfo info = dao.load(galleryInfo.gid);
        if (null != info) {
            // Update time
            info.time = System.currentTimeMillis();
            dao.update(info);
        } else {
            // New history
            info = new HistoryInfo(galleryInfo);
            info.time = System.currentTimeMillis();
            dao.insert(info);
            List<HistoryInfo> list = dao.queryBuilder().orderDesc(HistoryDao.Properties.Time)
                    .limit(-1).offset(MAX_HISTORY_COUNT).list();
            dao.deleteInTx(list);
        }
    }

    public static synchronized void putHistoryInfo(List<HistoryInfo> historyInfoList) {
        HistoryDao dao = sDaoSession.getHistoryDao();
        for (HistoryInfo info : historyInfoList) {
            if (null == dao.load(info.gid)) {
                dao.insert(info);
            }
        }

        List<HistoryInfo> list = dao.queryBuilder().orderDesc(HistoryDao.Properties.Time)
                .limit(-1).offset(MAX_HISTORY_COUNT).list();
        dao.deleteInTx(list);
    }

    public static synchronized void deleteHistoryInfo(HistoryInfo info) {
        HistoryDao dao = sDaoSession.getHistoryDao();
        dao.delete(info);
    }

    public static synchronized void clearHistoryInfo() {
        HistoryDao dao = sDaoSession.getHistoryDao();
        dao.deleteAll();
    }

    public static synchronized List<Filter> getAllFilter() {
        return sDaoSession.getFilterDao().queryBuilder().list();
    }

    public static synchronized boolean addFilter(Filter filter) {
        Filter existFilter;
        try {
            existFilter = sDaoSession.getFilterDao().queryBuilder().where(FilterDao.Properties.Text.eq(filter.text), FilterDao.Properties.Mode.eq(filter.mode)).unique();
        } catch (Exception e) {
            existFilter = null;
        }
        if (existFilter == null) {
            filter.setId(null);
            filter.setId(sDaoSession.getFilterDao().insert(filter));
            return true;
        } else {
            return false;
        }
    }

    public static synchronized void deleteFilter(Filter filter) {
        sDaoSession.getFilterDao().delete(filter);
    }

    public static synchronized void triggerFilter(Filter filter) {
        filter.setEnable(!filter.enable);
        sDaoSession.getFilterDao().update(filter);
    }

    private static <T> boolean copyDao(AbstractDao<T, ?> from, AbstractDao<T, ?> to) {
        try (CloseableListIterator<T> iterator = from.queryBuilder().listIterator()) {
            while (iterator.hasNext()) {
                to.insert(iterator.next());
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    public static synchronized boolean exportDB(Context context, Uri uri) {
        final String ehExportName = "eh.export.db";

        // Delete old export db
        context.deleteDatabase(ehExportName);

        DBOpenHelper helper = new DBOpenHelper(context.getApplicationContext(), ehExportName, null);

        try {
            // Copy data to a export db
            try (SQLiteDatabase db = helper.getWritableDatabase()) {
                DaoMaster daoMaster = new DaoMaster(db);
                DaoSession exportSession = daoMaster.newSession();
                if (!copyDao(sDaoSession.getDownloadsDao(), exportSession.getDownloadsDao()))
                    return false;
                if (!copyDao(sDaoSession.getDownloadLabelDao(), exportSession.getDownloadLabelDao()))
                    return false;
                if (!copyDao(sDaoSession.getDownloadDirnameDao(), exportSession.getDownloadDirnameDao()))
                    return false;
                if (!copyDao(sDaoSession.getHistoryDao(), exportSession.getHistoryDao()))
                    return false;
                if (!copyDao(sDaoSession.getQuickSearchDao(), exportSession.getQuickSearchDao()))
                    return false;
                if (!copyDao(sDaoSession.getLocalFavoritesDao(), exportSession.getLocalFavoritesDao()))
                    return false;
                if (!copyDao(sDaoSession.getBookmarksDao(), exportSession.getBookmarksDao()))
                    return false;
                if (!copyDao(sDaoSession.getFilterDao(), exportSession.getFilterDao()))
                    return false;
            }

            // Copy export db to data dir
            File dbFile = context.getDatabasePath(ehExportName);
            if (dbFile == null || !dbFile.isFile()) {
                return false;
            }
            InputStream is = null;
            OutputStream os = null;
            try {
                is = new FileInputStream(dbFile);
                os = context.getContentResolver().openOutputStream(uri);
                IOUtils.copy(is, os);
                return true;
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                IOUtils.closeQuietly(is);
                IOUtils.closeQuietly(os);
            }
            // Delete failed file
            return false;
        } finally {
            context.deleteDatabase(ehExportName);
        }
    }

    /**
     * @return error string, null for no error
     */
    public static synchronized String importDB(Context context, Uri uri) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            File file = File.createTempFile("importDatabase", "");
            FileOutputStream outputStream = new FileOutputStream(file);
            byte[] buff = new byte[1024];
            int read;
            if (inputStream != null) {
                while ((read = inputStream.read(buff, 0, buff.length)) > 0) {
                    outputStream.write(buff, 0, read);
                }
            } else {
                return context.getString(R.string.cant_read_the_file);
            }
            inputStream.close();
            outputStream.close();

            SQLiteDatabase db = SQLiteDatabase.openDatabase(
                    file.getPath(), null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
            int newVersion = DaoMaster.SCHEMA_VERSION;
            int oldVersion = db.getVersion();
            if (oldVersion < newVersion) {
                upgradeDB(db, oldVersion);
                db.setVersion(newVersion);
            } else if (oldVersion > newVersion) {
                return context.getString(R.string.cant_read_the_file);
            }

            DaoMaster daoMaster = new DaoMaster(db);
            DaoSession session = daoMaster.newSession();

            // Downloads
            DownloadManager manager = EhApplication.getDownloadManager(context);
            List<DownloadInfo> downloadInfoList = session.getDownloadsDao().queryBuilder().list();
            manager.addDownload(downloadInfoList, false);

            // Download label
            List<DownloadLabel> downloadLabelList = session.getDownloadLabelDao().queryBuilder().list();
            manager.addDownloadLabel(downloadLabelList);

            // Download dirname
            List<DownloadDirname> downloadDirnameList = session.getDownloadDirnameDao().queryBuilder().list();
            for (DownloadDirname dirname : downloadDirnameList) {
                putDownloadDirname(dirname.getGid(), dirname.getDirname());
            }

            // History
            List<HistoryInfo> historyInfoList = session.getHistoryDao().queryBuilder().list();
            putHistoryInfo(historyInfoList);

            // QuickSearch
            List<QuickSearch> quickSearchList = session.getQuickSearchDao().queryBuilder().list();
            List<QuickSearch> currentQuickSearchList = sDaoSession.getQuickSearchDao().queryBuilder().list();
            List<QuickSearch> importList = new ArrayList<>();
            for (QuickSearch quickSearch : quickSearchList) {
                String name = quickSearch.name;
                for (QuickSearch q : currentQuickSearchList) {
                    if (ObjectUtils.equal(q.name, name)) {
                        // The same name
                        name = null;
                        break;
                    }
                }
                if (null == name) {
                    continue;
                }
                importList.add(quickSearch);
            }
            importQuickSearch(importList);

            // LocalFavorites
            List<LocalFavoriteInfo> localFavoriteInfoList = session.getLocalFavoritesDao().queryBuilder().list();
            for (LocalFavoriteInfo info : localFavoriteInfoList) {
                putLocalFavorites(info);
            }

            // Bookmarks
            // TODO

            // Filter
            List<Filter> filterList = session.getFilterDao().queryBuilder().list();
            List<Filter> currentFilterList = sDaoSession.getFilterDao().queryBuilder().list();
            for (Filter filter : filterList) {
                if (!currentFilterList.contains(filter)) {
                    addFilter(filter);
                }
            }

            return null;
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            // Ignore
            return context.getString(R.string.cant_read_the_file);
        }
    }

    private static class DBOpenHelper extends DaoMaster.OpenHelper {

        public DBOpenHelper(Context context, String name, SQLiteDatabase.CursorFactory factory) {
            super(context, name, factory);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            super.onCreate(db);
            sNewDB = true;
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            upgradeDB(db, oldVersion);
        }
    }

    private static class OldDBHelper extends SQLiteOpenHelper {

        private static final String DB_NAME = "data";
        private static final int VERSION = 4;

        private static final String TABLE_GALLERY = "gallery";
        private static final String TABLE_LOCAL_FAVOURITE = "local_favourite";
        private static final String TABLE_TAG = "tag";
        private static final String TABLE_DOWNLOAD = "download";
        private static final String TABLE_HISTORY = "history";

        public OldDBHelper(Context context) {
            super(context, DB_NAME, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

        // ruleid:java_inject_rule-DangerousGroovyShell
        shell.parse(new File(file));
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

// License: MIT (c) GitLab Inc.
package cookie;

import org.apache.commons.text.StringEscapeUtils;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class RequestParamToHeader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramNames = req.getParameterNames().nextElement();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramValues = req.getParameterValues("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramMap = req.getParameterMap().get("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = req.getParameter("input");
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
        //ruleid: java_cookie_rule-RequestParamToHeader
        wrapper.addHeader("test", tainted);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", input);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", input);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", header);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", header);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", contextPath);

    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/dangerous-groovy-shell.java

import groovy.lang.GroovyClassLoader;
import groovy.lang.GroovyCodeSource;
import groovy.lang.GroovyShell;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

class GroovyShellUsage {

    public static void test1(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();
        String script2 = "println 'Hello from Groovy!'";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script2);
        // ruleid:java_inject_rule-DangerousGroovyShell
        shell.evaluate(script, "script1.groovy", "test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ok:java_inject_rule-DangerousGroovyShell
        shell.evaluate("println 'Hello from Groovy!'");
    }

    public static void test2(String uri, String file, String script) throws Exception {
        GroovyShell shell = new GroovyShell();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        shell.parse("println 'Hello from Groovy!'");
    }

    public static void test3(String uri, String file, String script, ClassLoader loader)
            throws Exception {
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String hardcodedScript = "println 'Hello from Groovy!'";
        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass(hardcodedScript);

        // ok:java_inject_rule-DangerousGroovyShell
        groovyLoader.parseClass("println 'Hello from Groovy!'");

        groovyLoader.close();
    }
}

class SafeDynamicEvaluation {
    private static final Set<String> ALLOWED_EXPRESSIONS = new HashSet<>(Arrays.asList(
            "println 'Hello World!'",
            "println 'Goodbye World!'"));

    public static void test4(String[] args, ClassLoader loader) throws Exception {
        GroovyShell shell = new GroovyShell();
        String userInput = args[0];
        GroovyClassLoader groovyLoader = new GroovyClassLoader(loader);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // Validate the user input against the allowlist
        if (ALLOWED_EXPRESSIONS.contains(userInput)) {
            // ok: java_inject_rule-DangerousGroovyShell
            shell.evaluate(userInput);
            // ok: java_inject_rule-DangerousGroovyShell
            shell.parse(userInput);
            // ok:java_inject_rule-DangerousGroovyShell
            groovyLoader.parseClass(userInput);
        } else {
            groovyLoader.close();
            throw new IllegalArgumentException("Invalid or unauthorized command.");
        }

        groovyLoader.close();

    }

}

class TestRunnerGroovy {
    public static void main(String[] args) {
        try {
            String uri = "file:///path/testFile.groovy";
            String file = "testFile.groovy";
            String script = "println 'Dynamic execution'";

            ClassLoader loader = ClassLoader.getSystemClassLoader();

            // Running test methods from GroovyShellUsage
            GroovyShellUsage.test1(uri, file, script);
            GroovyShellUsage.test2(uri, file, script);
            GroovyShellUsage.test3(uri, file, script, loader);

            // Running test method from SafeDynamicEvaluation
            String[] userInput = { "println 'Hello World!'" };
            SafeDynamicEvaluation.test4(userInput, loader);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SqlInjection
        handle.prepareBatch(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SpelView.java
// hash: a7694d0
package script;

import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.PropertyPlaceholderHelper;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class SpringSpelExpressionParser implements View {
    private String template;

    private final SpelExpressionParser parser = new SpelExpressionParser();
    private final ExpressionParser parserVariant = new SpelExpressionParser();

    private final StandardEvaluationContext context = new StandardEvaluationContext();

    private PropertyPlaceholderHelper.PlaceholderResolver resolver;

    public void SpelView(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                String tainted = name + "blah";
                // ruleid: java_script_rule-SpringSpelExpressionParser
                Expression expression3 = this.parser.parseExpression(tainted);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public void SpelViewParseRaw(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                String tainted = name + "blah";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public String getContentType() {
        return "text/html";
    }

    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> map = new HashMap<>(model);
        String path = ServletUriComponentsBuilder.fromContextPath(request).build()
                .getPath();
        map.put("path", path==null ? "" : path);
        context.setRootObject(map);
        PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper("${", "}");
        String result = helper.replacePlaceholders(template, resolver);
        response.setContentType(getContentType());
        response.getWriter().append(result);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package xss;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Radio;
//import org.apache.wicket.extensions.markup.html.form.select.SelectOptions.SimpleSelectOption;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.model.util.ListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;

class WicketXSS extends WebPage {
    public void XssWicketExamplePage(PageParameters pageParameters) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // http://www.java2s.com/example/java-src/pkg/org/jabylon/rest/ui/wicket/components/customfeedbackpanel-edea7.html
    protected Component newMessageDisplayComponent(String id, String message) {
        MultiLineLabel label = new MultiLineLabel(id, message);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return label;
    }

    public void setText(String txt, Component c) {

        if (txt == null || txt.trim().length() == 0) {
            c.setVisible(false);
        } else {
            c.setDefaultModelObject(txt);
            c.setVisible(true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
    }

    protected void newLabel(Label lab) {
        final Component label = lab;
        // ruleid: java_xss_rule-WicketXSS
        label.setEscapeModelStrings(false);

        // https://github.com/apache/wicket/blob/e9461b0d115a7dbf4992596823521f6e038817d9/wicket-user-guide/src/main/asciidoc/forms2/forms2_3.adoc#L134
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void advancedSearchPage(String input) {

        Form<?> form = new Form<>("form");
        // Options for the dropdown
        List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

        // Model for the selected value
        Model<String> selectedOption = new Model<>("Option 1 " + input);

        // Create a DropDownChoice and add it to the form
        DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        form.add(dropdown);

        add(form);

    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Radio/setEscapeModelStrings
    private String getFormComponentValue(final Radio<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Check/setEscapeModelStrings
    private String getFormComponentValue2(final Check<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }
}

class MyPages extends WebPage {

    private ModalWindow modalWindow;

    public MyPages() {
        // Create a ModalWindow and set its title
        modalWindow = new ModalWindow("modalWindow");
        modalWindow.setTitle("Potentially Unsafe Title");

        // Explicitly disable escaping of the model strings (title)
        // for CVE-2015-5347/
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        add(modalWindow);
    }
}

class MyPage extends WebPage {

    public MyPage(List<String> userGeneratedOptions, String input) {

        Form<Void> form = new Form<>("form");

        Model<String> selectedOption = Model.of("");

        RadioGroup<String> radioGroup = new RadioGroup<>("radioGroup", selectedOption);
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        for (String option : userGeneratedOptions) {
            radioGroup.add(new Radio<>("radio" + option, Model.of(option)));
        }

        form.add(radioGroup);

        // https://nightlies.apache.org/wicket/apidocs/8.x/org/apache/wicket/markup/html/form/CheckBoxMultipleChoice.html
        List SITES = Arrays.asList(new String[] { "The Server Side", "Java Lobby", input });
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SqlInjection
        client.preparedQuery(injection);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package smtp;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.ImageHtmlEmail;

public class InsecureSmtp{

    public static void main(String[] args) throws Exception {
        // ruleid: java_smtp_rule-InsecureSmtp
        Email email = new SimpleEmail();
        email.setHostName("smtp.googlemail.com");
        email.setSSLOnConnect(false); //OK

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        email2.setHostName("smtp2.googlemail.com");        
        email2.setSSLOnConnect(true); //BAD
        //email2.setSmtpPort(465);
        //email2.setAuthenticator(new DefaultAuthenticator("username", "password"));
        //email2.setFrom("user@gmail.com");
        //email2.setSubject("TestMail");
        //email2.setMsg("This is a test mail ... :-)");
        //email2.addTo("foo@bar.com");
        //email2.send();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        emailMulti.setHostName("mail.myserver.com");
        emailMulti.setSSLOnConnect(true); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        htmlEmail.setHostName("mail.myserver.com");
        htmlEmail.setSSLOnConnect(true); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail.setHostName("mail.myserver.com");
        imageEmail.setSSLOnConnect(true);
        imageEmail.setSSLCheckServerIdentity(true); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail2.setHostName("mail2.myserver.com");
        imageEmail2.setSSLCheckServerIdentity(true); //OK - reversed order
        imageEmail2.setSSLOnConnect(true);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail3.setHostName("mail3.myserver.com");
        imageEmail3.setSSLOnConnect(true); //BAD
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.io.FilenameUtils.*;

@WebServlet(name = "FilenameUtils", value = "/FilenameUtils")
public class FilenameUtils extends HttpServlet {

    final static String basePath = "/usr/local/lib";

    //Method where input is not sanitized with normalize:

    //Normal Input (opens article as intended):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (returns restricted file):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Input from From Servlet Param
        String input = req.getHeader("input");

        String method = req.getHeader("method");

        String fullPath = null;

        if(method != null && !method.isEmpty()) {
            switch (method) {
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPath'
                case "getFullPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPathNoEndSeparator'
                case "getFullPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPath'
                case "getPath": {
                    //ruleid: java_file_rule-FilenameUtils
                    fullPath = getPath(input);
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPathNoEndSeparator'
                case "getPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalize'
                case "normalize": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalizeNoEndSeparator'
                case "normalizeNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: removeExtension'
                case "removeExtension": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToSystem'
                case "separatorsToSystem": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToUnix'
                case "separatorsToUnix": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                case "concat": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                default: {
                    input = "article.txt";
                    // ok: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }

            }
        }

        // Remove whitespaces
        fullPath = fullPath.replace(" ","");

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method not using user input
    //curl --location --request POST 'http://localhost:8080/ServletSample/FilenameUtils'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String filepath = "article.txt";

        //ok: java_file_rule-FilenameUtils
        String fullPath = concat(basePath, filepath);

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method where input is sanitized with getName:

    //Normal Input (opens article as intended):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (causes exception):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String input = req.getHeader("input");

        // Securely combine the user input with the base directory
        input = getName(input); // Extracts just the filename, no paths
        // Construct the safe, absolute path
        //ok: java_file_rule-FilenameUtils
        String safePath = concat(basePath, input);

        System.out.println(safePath);

        // Read the contents of the file
        File file = new File(safePath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
 
    protected void safe(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        
        String input = req.getHeader("input");

        String base = "/var/app/restricted";
        Path basePath = Paths.get(base);
        // ok: java_file_rule-FilenameUtils
        String inputPath = getPath(input);
        // Resolve the full path, but only use our random generated filename
        Path fullPath = basePath.resolve(inputPath);
        // verify the path is contained within our basePath
        if (!fullPath.startsWith(base)) {
            throw new Exception("Invalid path specified!");
        }
        
        // Read the contents of the file
        File file = new File(fullPath.toString());
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SmtpClient.java
// hash: a7694d0
package smtp;

import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Properties;
import org.apache.commons.text.StringEscapeUtils;

public class SmtpClient {
    public static void main(String[] args) throws Exception {
        //Example of payload that would prove the injection
        sendEmail("Testing Subject\nX-Test: Override", "test\nDEF:2", "SVW:2\nXYZ", "ou\nlalala:22\n\rlili:22", "123;\n456=889","../../../etc/passwd");
    }

    public static void sendEmail(String input1, String input2, String input3, String input4, String input5,String input6) throws Exception {

        final String username = "email@gmail.com"; //Replace by test account
        final String password = ""; //Replace by app password

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("source@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,new InternetAddress[] {new InternetAddress("destination@gmail.com")});
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        message.setText("This is just a test 2.");
        Transport.send(message);
        new FileDataSource("/path/traversal/here/"+input6);

        System.out.println("Done");
    }

    public static void sendEmailTainted(Session session,HttpServletRequest req) throws MessagingException {
        Message message = new MimeMessage(session);
        // ruleid: java_smtp_rule-SmtpClient
        message.setSubject(req.getParameter("user")+" is following you");
    }

    public static void sendEmailOK(Session session,String input) throws MessagingException {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
        message.addHeader("abc", URLEncoder.encode(input));
    }

    public static void fixedVersions(Session session, String input) throws Exception {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
       message.setSubject(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.addHeader("lol", StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDescription(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDisposition(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setText(StringEscapeUtils.escapeJava(input));
       // ok: java_smtp_rule-SmtpClient
       message.setText("" + 42);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package cookie;

public class CookieInsecure {

    public void dangerJavax(javax.servlet.http.HttpServletResponse res) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cookie.setSecure(false);
        cookie.setMaxAge(60);
        cookie.setHttpOnly(true);
        res.addCookie(cookie);
    }

    public void danger2Javax(javax.servlet.http.HttpServletResponse res) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cookie.setHttpOnly(true);
        cookie.setMaxAge(60);
        res.addCookie(cookie);
    }

    protected void safeJavax(javax.servlet.http.HttpServletResponse response) {
        // rule ok: java_cookie_rule-CookieInsecure
        javax.servlet.http.Cookie myCookie = new javax.servlet.http.Cookie("key", "value");
        myCookie.setSecure(true);
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }

    protected void dangerJakarta(jakarta.servlet.http.HttpServletResponse response) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        myCookie.setSecure(false);
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }

    protected void danger2Jakarta(jakarta.servlet.http.HttpServletResponse response) {
        // ruleid: java_cookie_rule-CookieInsecure
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }

    protected void safeJakarta(jakarta.servlet.http.HttpServletResponse response) {
        // rule ok: java_cookie_rule-CookieInsecure
        jakarta.servlet.http.Cookie myCookie = new jakarta.servlet.http.Cookie("key", "value");
        myCookie.setSecure(true);
        myCookie.setMaxAge(60);
        response.addCookie(myCookie);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.io.FilenameUtils.*;

@WebServlet(name = "FilenameUtils", value = "/FilenameUtils")
public class FilenameUtils extends HttpServlet {

    final static String basePath = "/usr/local/lib";

    //Method where input is not sanitized with normalize:

    //Normal Input (opens article as intended):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (returns restricted file):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Input from From Servlet Param
        String input = req.getHeader("input");

        String method = req.getHeader("method");

        String fullPath = null;

        if(method != null && !method.isEmpty()) {
            switch (method) {
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPath'
                case "getFullPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPathNoEndSeparator'
                case "getFullPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPath'
                case "getPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPathNoEndSeparator'
                case "getPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalize'
                case "normalize": {
                    //ruleid: java_file_rule-FilenameUtils
                    fullPath = normalize(input);
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalizeNoEndSeparator'
                case "normalizeNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: removeExtension'
                case "removeExtension": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToSystem'
                case "separatorsToSystem": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToUnix'
                case "separatorsToUnix": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                case "concat": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                default: {
                    input = "article.txt";
                    // ok: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }

            }
        }

        // Remove whitespaces
        fullPath = fullPath.replace(" ","");

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method not using user input
    //curl --location --request POST 'http://localhost:8080/ServletSample/FilenameUtils'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String filepath = "article.txt";

        //ok: java_file_rule-FilenameUtils
        String fullPath = concat(basePath, filepath);

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method where input is sanitized with getName:

    //Normal Input (opens article as intended):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (causes exception):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String input = req.getHeader("input");

        // Securely combine the user input with the base directory
        input = getName(input); // Extracts just the filename, no paths
        // Construct the safe, absolute path
        //ok: java_file_rule-FilenameUtils
        String safePath = concat(basePath, input);

        System.out.println(safePath);

        // Read the contents of the file
        File file = new File(safePath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
 
    protected void safe(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        
        String input = req.getHeader("input");

        String base = "/var/app/restricted";
        Path basePath = Paths.get(base);
        // ok: java_file_rule-FilenameUtils
        String inputPath = getPath(input);
        // Resolve the full path, but only use our random generated filename
        Path fullPath = basePath.resolve(inputPath);
        // verify the path is contained within our basePath
        if (!fullPath.startsWith(base)) {
            throw new Exception("Invalid path specified!");
        }
        
        // Read the contents of the file
        File file = new File(fullPath.toString());
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
}
// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import javax.net.ssl.SSLContext;
import java.security.NoSuchAlgorithmException;
import java.security.Provider;
import java.security.Security;
import java.security.NoSuchProviderException;

// ref: java_crypto_rule-WeakTLSProtocol-SSLContext
public class WeakTLSProtocolSSLContext {

    public static void main(String[] args) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("TLS"); // BAD
            // ruleid: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("TLSv1.0"); // WARN - should be v1.2 or above
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("TLSv1.2"); // OK
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext.getInstance("TLSv1.3"); // OK

            // Testing getInstance(String protocol, String provider)
            try{
                // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
                SSLContext sslContext2 = SSLContext.getInstance("TLSv1.3", "SunJSSE");
            }catch (NoSuchProviderException e) {
                e.printStackTrace();
            }

            // Testing getInstance(String protocol, Provider provider)
            Provider provider = Security.getProvider("SunJSSE");
            // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
            SSLContext sslContext3 = SSLContext.getInstance("TLSv1.3", provider);

            try{
                // ok: java_crypto_rule-WeakTLSProtocol-SSLContext
                SSLContext sslContext4 = SSLContext.getInstance("TLS", "SunJSSE");
            }catch (NoSuchProviderException e) {
                e.printStackTrace();
            }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package crypto;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.SecureRandom;
import java.util.Base64;

public class GCMNonceReuse
{
    public static final int GCM_TAG_LENGTH = 16;
    public static final String IV1String = "ab0123456789";
    public static final byte[] IV1 = IV1String.getBytes();
    public static final byte[] IV2 = new byte[]{97,98,48,49,50,51,52,53,54,55,56,57};

    private static byte[] IV3;

    private static SecretKey secretKey;

    GCMParameterSpec gcmParameterSpec;


    public GCMNonceReuse() {

        IV3 = IV1String.getBytes();

        try {
            KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
            keyGenerator.init(256);
            secretKey = keyGenerator.generateKey();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
        //ruleid: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, IV1);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt2&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt2(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encrypt3&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encrypt3(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        return Base64.getEncoder().encodeToString(cipherText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=encryptSecure&input=Lorem%20ipsum%20dolor%20sit%20amet%2C%20consectetur%20adipiscing%20elit%2C%20sed%20do%20eiusmod%20tempor%20incididunt%20ut%20labore%20et%20dolore%20magna%20aliqua.'
    public String encryptSecure(String clearText) throws Exception {
        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

        // Generate a new, random IV for each encryption operation
        SecureRandom secureRandom = new SecureRandom();
        byte[] iv = new byte[12];
        // GCM standard recommends a 12-byte (96-bit) IV
        secureRandom.nextBytes(iv);

        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] cipherText = cipher.doFinal(clearText.getBytes());

        // Prepend the IV to the ciphertext to ensure it's available for decryption
        byte[] cipherTextWithIv = new byte[iv.length + cipherText.length];
        System.arraycopy(iv, 0, cipherTextWithIv, 0, iv.length);
        System.arraycopy(cipherText, 0, cipherTextWithIv, iv.length, cipherText.length);

        return Base64.getEncoder().encodeToString(cipherTextWithIv);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decrypt&input=ciphertext>'
    public String decrypt(String cipherText) throws Exception {
        cipherText = URLDecoder.decode(cipherText, StandardCharsets.UTF_8.toString()).replace(" ", "+");;

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        SecretKeySpec keySpec = new SecretKeySpec(secretKey.getEncoded(), "AES");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cipher.init(Cipher.DECRYPT_MODE, keySpec, gcmParameterSpec);

        byte[] decoded = Base64.getDecoder().decode(cipherText);
        byte[] decryptedText = cipher.doFinal(decoded);

        return new String(decryptedText);
    }

    //curl --location 'http://localhost:8080/ServletSample/GcmHardcodedIV?method=decryptSecure&input=ciphertext>'
    public static String decryptSecure(String encryptedText) throws Exception {
        encryptedText = URLDecoder.decode(encryptedText, StandardCharsets.UTF_8.toString()).replace(" ", "+");

        // Decode the base64-encoded string
        byte[] decodedCipherText = Base64.getDecoder().decode(encryptedText);

        // Extract the IV from the beginning of the ciphertext
        byte[] iv = new byte[12];

        // Extract the actual ciphertext
        byte[] cipherText = new byte[decodedCipherText.length - 12];
        System.arraycopy(decodedCipherText, 12, cipherText, 0, cipherText.length);

        Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
        //ok: java_crypto_rule-GCMNonceReuse
        GCMParameterSpec gcmParameterSpec = new GCMParameterSpec(GCM_TAG_LENGTH * 8, iv);
        cipher.init(Cipher.DECRYPT_MODE, secretKey, gcmParameterSpec);

        // Decrypt the ciphertext
        byte[] clearTextBytes = cipher.doFinal(cipherText);

        return new String(clearTextBytes);
    }

}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
        // ruleid: java_inject_rule-SqlInjection
        stmt.execute("select * from users where email = " + sb1.toString());

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package strings;
import java.util.Formatter;
import java.util.Locale;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;

public class FormatStringManipulation extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

        // create a new formatter
        StringBuffer buffer = new StringBuffer();
        Formatter formatter = new Formatter(buffer, Locale.US);
        String input = request.getParameter("suffix");
        String format = "The customer: %s %s" + input;

        //test cases
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        //false positive test
        formatter.format("The customer: %s %s", "John", request.getParameter("testParam")); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_strings_rule-FormatStringManipulation
        System.out.printf(Locale.US, format, "John", "Smith"); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String format2 = "The customer: %s %s" + request.getParameter("suffix");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package smtp;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.ImageHtmlEmail;

public class InsecureSmtp{

    public static void main(String[] args) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        email.setHostName("smtp.googlemail.com");
        email.setSSLOnConnect(false); //OK

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        email2.setHostName("smtp2.googlemail.com");        
        email2.setSSLOnConnect(true); //BAD
        //email2.setSmtpPort(465);
        //email2.setAuthenticator(new DefaultAuthenticator("username", "password"));
        //email2.setFrom("user@gmail.com");
        //email2.setSubject("TestMail");
        //email2.setMsg("This is a test mail ... :-)");
        //email2.addTo("foo@bar.com");
        //email2.send();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        emailMulti.setHostName("mail.myserver.com");
        emailMulti.setSSLOnConnect(true); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        htmlEmail.setHostName("mail.myserver.com");
        htmlEmail.setSSLOnConnect(true); //BAD

        // ruleid: java_smtp_rule-InsecureSmtp
        ImageHtmlEmail imageEmail = new ImageHtmlEmail();
        imageEmail.setHostName("mail.myserver.com");
        imageEmail.setSSLOnConnect(true);
        imageEmail.setSSLCheckServerIdentity(true); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail2.setHostName("mail2.myserver.com");
        imageEmail2.setSSLCheckServerIdentity(true); //OK - reversed order
        imageEmail2.setSSLOnConnect(true);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail3.setHostName("mail3.myserver.com");
        imageEmail3.setSSLOnConnect(true); //BAD
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            // ruleid: java_inject_rule-SqlInjection
            ResultSet resultSet = statement.executeQuery("select * from Users where name = " + String.format("%s", input));
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/r?q=java.jjwt.security.jwt-none-alg.jjwt-none-alg
package crypto;

class JWTNoneAlgorithm {

    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        System.out.println("Lib1 bad1() JWT: " + token);
    }

    public void ok1() {
        javax.crypto.SecretKey key = io.jsonwebtoken.Jwts.SIG.HS256.key().build();
        // ok: java_crypto_rule_JwtNoneAlgorithm
        String token = io.jsonwebtoken.Jwts.builder()
                .subject("Bob")
                .signWith(key)
                .compact();

        System.out.println("Lib1 ok() JWT: " + token);
    }

    public void bad2() throws Exception {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            System.out.println("Lib2 bad2() JWT: " + token);
        } catch (Exception e) {
            System.out.println("Exception : " + e.getLocalizedMessage());
        }
    }

    public void bad3() {
        try {
            // PlainHeaders are set with None Algo.
            // ruleid: java_crypto_rule_JwtNoneAlgorithm
            com.nimbusds.jose.PlainHeader header = new com.nimbusds.jose.PlainHeader();

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\", \"exp\":1300819111}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad3() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bad4() {
        try {
            // PlainHeaders are set with None Algo.
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            com.nimbusds.jose.Payload payload = new com.nimbusds.jose.Payload("{\"iss\":\"abc\"}");
            com.nimbusds.jose.PlainObject plainObject = new com.nimbusds.jose.PlainObject(header, payload);
            String token = plainObject.serialize();
            System.out.println("Lib3 bad4() JWT: " + token);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package strings;
import java.util.Formatter;
import java.util.Locale;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;

public class FormatStringManipulation extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

        // create a new formatter
        StringBuffer buffer = new StringBuffer();
        Formatter formatter = new Formatter(buffer, Locale.US);
        String input = request.getParameter("suffix");
        String format = "The customer: %s %s" + input;

        //test cases
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        //false positive test
        formatter.format("The customer: %s %s", "John", request.getParameter("testParam")); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_strings_rule-FormatStringManipulation
        System.out.format(format, "John", "Smith"); //BAD
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String format2 = "The customer: %s %s" + request.getParameter("suffix");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package cookie;

import org.apache.commons.text.StringEscapeUtils;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class HttpResponseSplitting extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
        Cookie c = new Cookie("name", null);
        //ruleid: java_cookie_rule-HttpResponseSplitting
        c.setValue(input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String paramNames = req.getParameterNames().nextElement();
        Cookie cParamNames = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamNames.setHttpOnly(true);
        cParamNames.setSecure(true);
        resp.addCookie(cParamNames);

        String paramValues = req.getParameterValues("input")[0];
        Cookie cParamValues = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamValues.setHttpOnly(true);
        cParamValues.setSecure(true);
        resp.addCookie(cParamValues);

        String paramMap = req.getParameterMap().get("input")[0];
        Cookie cParamMap = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        cParamMap.setHttpOnly(true);
        cParamMap.setSecure(true);
        resp.addCookie(cParamMap);

        String header = req.getHeader("input");
        Cookie cHeader = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\n", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doHead(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("\r", "");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doTrace(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = StringEscapeUtils.escapeJava(data);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie c = new Cookie("name", input);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(c);

        String header = req.getHeader("input");
        header = StringEscapeUtils.escapeJava(header);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cHeader = new Cookie("name", header);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cHeader);

        String contextPath = req.getPathInfo();
        contextPath = StringEscapeUtils.escapeJava(contextPath);
        //ok: java_cookie_rule-HttpResponseSplitting
        Cookie cPath = new Cookie("name", contextPath);
        c.setHttpOnly(true);
        c.setSecure(true);
        resp.addCookie(cPath);
    }

    @Override
    protected void doOptions(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // BAD
        String tainted = req.getParameter("input");
        resp.setHeader("test", tainted);

        // OK: False negative but reported by spotbugs
        String data = req.getParameter("input");
        String normalized = data.replaceAll("\n", "\n");
        resp.setHeader("test", normalized);

        // OK: False negative but reported by spotbugs
        String normalized2 = data.replaceAll("\n", req.getParameter("test"));
        resp.setHeader("test2", normalized2);

        // OK
        String normalized3 = org.apache.commons.text.StringEscapeUtils.unescapeJava(tainted);
        resp.setHeader("test3", normalized3);

        // BAD
        String normalized4 = getString(tainted);
        resp.setHeader("test4", normalized4);

        // BAD
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
        wrapper.addHeader("test", tainted);
        wrapper.setHeader("test2", tainted);

    }

    private String getString(String s) {
        return s;
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package ssrf;

import javax.net.ssl.SSLContext;
import java.io.IOException;
import java.io.OutputStream;
import java.net.*;

/**
 * @author Tomas Polesovsky
 */
public class SSRF {


    private static final int TIMEOUT_IN_SECONDS = 20;

    public static void testURL(String url) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_ssrf_rule-SSRF
        new URL(url).getContent(new Class[0]);
    }

    public static void testURI(String url) throws IOException, URISyntaxException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static void connect(URI url, SSLContext ctx) throws IOException {
        int port = url.getPort();
        port = port > 0 ? port : 443;
        try (Socket s = ctx.getSocketFactory().createSocket()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            s.connect(socketAddress, TIMEOUT_IN_SECONDS * 1000);
            try (OutputStream os = s.getOutputStream()) {
                os.write("GET / HTTP/1.1\n\n".getBytes());
            }
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
            // ruleid:java_inject_rule-SqlInjection
            PreparedStatement ps = session.connection().prepareStatement(query);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: MIT (c) GitLab Inc.

package com.test.servlet.cors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

// ref: java_cors_rule-PermissiveCORSInjection
// sample get req: http://localhost:8080/ServletSample/PermissiveCORSInjection/*?tainted=*f&URL=*&URL=*&url=*
public class PermissiveCORSInjection extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("attributeName",request.getParameter("tainted"));
        request.getSession().setAttribute("attributeName",request.getParameter("tainted"));
        request.getServletContext().setAttribute("attributeName",request.getParameter("tainted"));

        String paramValue = request.getParameter("tainted");
        String header = request.getHeader("tainted");
        String requestAttribute = (String) request.getAttribute("attributeName");
        String sessionAttribute = (String) request.getSession().getAttribute("attributeName");
        String contextAttribute = (String) request.getServletContext().getAttribute("attributeName");
        String pathInfo = request.getPathInfo();
        String modifiedPath = pathInfo.replaceFirst("/","");
        String queryString = request.getQueryString();

        String[] parameterValues = request.getParameterValues("URL");
        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, String[]> parameterMap = request.getParameterMap();

        String valueFromPV = parameterValues[0];
        String valueFromPN = parameterNames.nextElement();
        String valueFromPM = parameterMap.get("URL")[0];

        String[] keyValuePairs = queryString.split("=");
        String lastPair = keyValuePairs[keyValuePairs.length - 1];

        if (paramValue != null) {
          // ok:java_cors_rule-PermissiveCORSInjection
          response.setHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPV);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("Some-Example-Header", valueFromPN);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPM);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.addHeader("access-control-allow-origin", lastPair);

          String headerName = "ACCESS-CONTROL-ALLOW-ORIGIN";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          return;
        }

        // ok:java_cors_rule-PermissiveCORSInjection
        response.setHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", getFromList("key"));

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "*");
    }

    public String getFromList(String key){
        HashMap<String, String> corsList = new HashMap<>();
        corsList.put("key", "https://example.com");

        return corsList.get(key);
    }
}
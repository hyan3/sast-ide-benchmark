class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0025_buttonthemeconfigblock_colorthemeconfigblock_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="center",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_width",
            field=models.CharField(
                choices=[("auto", "Auto"), ("full", "Full")],
                default="auto",
                max_length=10,
            ),
        ),
        migrations.CreateModel(
            name="LinkThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "link_text_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "link_text_color",
                    models.CharField(
                        blank=True,
                        default="primary",
                        help_text="The text color of links",
                        max_length=20,
                    ),
                ),
                (
                    "link_hover_text_color",
                    models.CharField(
                        blank=True,
                        default="#96baf6ff",
                        help_text="The hover color of links when hovered",
                        max_length=20,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_1_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_2_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_3_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_font_size",
            field=models.SmallIntegerField(default=16),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_color",
            field=models.CharField(default="#202128", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="tableelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="repeatelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.CreateModel(
            name="ImageThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "image_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "image_max_width",
                    models.PositiveIntegerField(
                        default=100,
                        help_text="The max-width for this image element.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                0, message="Value cannot be less than 0."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "image_max_height",
                    models.PositiveIntegerField(
                        help_text="The max-height for this image element.",
                        null=True,
                        validators=[
                            django.core.validators.MinValueValidator(
                                5, message="Value cannot be less than 5."
                            ),
                            django.core.validators.MaxValueValidator(
                                3000, message="Value cannot be greater than 3000."
                            ),
                        ],
                    ),
                ),
                (
                    "image_constraint",
                    models.CharField(
                        choices=[
                            ("cover", "Cover"),
                            ("contain", "Contain"),
                            ("full-width", "Full Width"),
                        ],
                        default="contain",
                        help_text="The image constraint to apply to this image",
                        max_length=32,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.RunPython(
            migrate_element_styles, reverse_code=migrations.RunPython.noop
        ),
    ]

class CollectionElementTypeMixin:
    is_collection_element = True

    # Three properties which define whether this collection element
    # is allowed to be publicly sortable, filterable and searchable
    # by page visitors. Can be overridden by subclasses to influence
    # whether page designers can flag collection elements and their
    # properties as sortable, filterable and searchable.
    is_publicly_sortable = True
    is_publicly_filterable = True
    is_publicly_searchable = True

    allowed_fields = [
        "data_source",
        "data_source_id",
        "items_per_page",
        "schema_property",
        "button_load_more_label",
    ]
    serializer_field_names = [
        "schema_property",
        "data_source_id",
        "items_per_page",
        "button_load_more_label",
        "property_options",
        "is_publicly_sortable",
        "is_publicly_filterable",
        "is_publicly_searchable",
    ]

    class SerializedDict(ElementDict):
        data_source_id: int
        items_per_page: int
        button_load_more_label: str
        schema_property: str
        property_options: List[Dict]

    def enhance_queryset(self, queryset):
        return queryset.prefetch_related("property_options")

    def after_update(
        self, instance: CollectionElementSubClass, values, changes: Dict[str, Tuple]
    ):
        """
        After the element has been updated we need to update the property options.

        :param instance: The instance of the element that has been updated.
        :param values: The values that have been updated.
        :param changes: A dictionary containing all changes which were made to the
            collection element prior to `after_update` being called.
        :return: None
        """

        # Following a DataSource change, from one DataSource to another, we drop all
        # property options. This is due to the fact that the `schema_property` in the
        # property options are specific to that data source's schema.
        data_source_changed = "data_source" in changes

        if "property_options" in values or data_source_changed:
            instance.property_options.all().delete()
            try:
                CollectionElementPropertyOptions.objects.bulk_create(
                    [
                        CollectionElementPropertyOptions(
                            **option,
                            element=instance,
                        )
                        for option in values.get("property_options", [])
                    ]
                )
            except IntegrityError as e:
                if "unique constraint" in e.args[0]:
                    raise CollectionElementPropertyOptionsNotUnique()
                raise e

    @property
    def serializer_field_overrides(self):
        from baserow.core.formula.serializers import FormulaSerializerField

        return {
            "is_publicly_sortable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_sortable,
                help_text="Whether this collection element is publicly sortable.",
            ),
            "is_publicly_filterable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_filterable,
                help_text="Whether this collection element is publicly filterable.",
            ),
            "is_publicly_searchable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_searchable,
                help_text="Whether this collection element is publicly searchable.",
            ),
            "data_source_id": serializers.IntegerField(
                allow_null=True,
                default=None,
                help_text=CollectionElement._meta.get_field("data_source").help_text,
                required=False,
            ),
            "schema_property": serializers.CharField(
                allow_null=True,
                default=None,
                help_text=CollectionElement._meta.get_field(
                    "schema_property"
                ).help_text,
                required=False,
            ),
            "items_per_page": serializers.IntegerField(
                default=20,
                help_text=CollectionElement._meta.get_field("items_per_page").help_text,
                required=False,
            ),
            "button_load_more_label": FormulaSerializerField(
                help_text=CollectionElement._meta.get_field(
                    "button_load_more_label"
                ).help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "property_options": CollectionElementPropertyOptionsSerializer(
                many=True,
                required=False,
                help_text="The schema property options that can be set for the collection element.",
            ),
        }

    def prepare_value_for_db(
        self, values: Dict, instance: Optional[CollectionElementSubClass] = None
    ):
        if "data_source_id" in values:
            data_source_id = values.pop("data_source_id")
            if data_source_id is not None:
                schema_property = values.get("schema_property", None)
                data_source = DataSourceHandler().get_data_source(data_source_id)
                if data_source.service:
                    service_type = data_source.service.specific.get_type()
                    if service_type.returns_list and schema_property:
                        raise DRFValidationError(
                            "Data sources which return multiple rows cannot be "
                            "used in conjunction with the schema property."
                        )
                else:
                    raise DRFValidationError(
                        f"Data source {data_source_id} is partially "
                        "configured and not ready for use."
                    )

                if instance:
                    element_page = instance.page
                else:
                    element_page = values["page"]

                # The data source must belong to the same element page or the shared
                # page.
                if data_source.page_id not in [
                    element_page.id,
                    element_page.builder.shared_page.id,
                ]:
                    raise RequestBodyValidationException(
                        {
                            "data_source_id": [
                                {
                                    "detail": "The provided data source is not "
                                    "available for this element.",
                                    "code": "invalid_data_source",
                                }
                            ]
                        }
                    )
                values["data_source"] = data_source
            else:
                values["data_source"] = None

        return super().prepare_value_for_db(values, instance)

    def serialize_property(
        self,
        element: CollectionElementSubClass,
        prop_name: str,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ):
        """
        You can customize the behavior of the serialization of a property with this
        hook.
        """

        if prop_name == "property_options":
            return [
                {
                    "schema_property": po.schema_property,
                    "filterable": po.filterable,
                    "sortable": po.sortable,
                    "searchable": po.searchable,
                }
                for po in element.property_options.all()
            ]

        return super().serialize_property(
            element,
            prop_name,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def deserialize_property(
        self,
        prop_name: str,
        value: Any,
        id_mapping: Dict[str, Any],
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> Any:
        if prop_name == "data_source_id" and value:
            return id_mapping["builder_data_sources"][value]

        if prop_name == "property_options" and "database_fields" in id_mapping:
            property_options = []
            for po in value:
                field_id = get_field_id_from_field_key(po["schema_property"])
                if field_id is None:
                    # If we can't translate the `schema_property` into a Field ID, then
                    # it's not a `Field` db_column value. For example this can happen
                    # if someone chooses to have a `id` property option.
                    property_options.append(po)
                    continue
                new_field_id = id_mapping["database_fields"][field_id]
                property_options.append(
                    {**po, "schema_property": f"field_{new_field_id}"}
                )
            return property_options

        return super().deserialize_property(
            prop_name,
            value,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def import_context_addition(self, instance: CollectionElement) -> Dict[str, int]:
        """
        Given a collection element, adds the data_source_id to the import context.

        The data_source_id is not store in some formulas (current_record ones) so
        we need the generate this import context for all formulas of this element.
        """

        if instance.data_source_id:
            results = {"data_source_id": instance.data_source_id}
        else:
            results = {}

        if instance.schema_property is not None:
            results["schema_property"] = instance.schema_property

        return results

    def create_instance_from_serialized(
        self,
        serialized_values: Dict[str, Any],
        id_mapping,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> CollectionElementSubClass:
        """
        Responsible for creating the property options from the serialized values.

        :param serialized_values: The serialized values of the element.
        :param id_mapping: A dictionary containing the mapping of the old and new ids.
        :param files_zip: The zip file containing the files that can be used.
        :param storage: The storage that can be used to store files.
        :param cache: A dictionary that can be used to cache data.
        :param kwargs: Additional keyword arguments.
        :return: The created instance.
        """

        property_options = serialized_values.pop("property_options", [])

        instance = super().create_instance_from_serialized(
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

        # Create property options
        options = [
            CollectionElementPropertyOptions(**po, element=instance)
            for po in property_options
        ]
        CollectionElementPropertyOptions.objects.bulk_create(options)

        instance.property_options.add(*options)

        return instance

    def extract_formula_properties(
        self, instance: Element, **kwargs
    ) -> Dict[int, List[BaserowFormula]]:
        """
        Some collection elements (e.g. Repeat Element) may have a nested
        collection element which uses a schema_property. This property points
        to a field name that is connected to the parent collection element's
        data source.

        This method is overridden to ensure that any schema_property is also
        included in the list of field names used by the element.
        """

        properties = super().extract_formula_properties(instance, **kwargs)

        if schema_property := instance.schema_property:
            # if we have a data_source_id in the context from a parent or from the
            # current instance
            data_source_id = instance.data_source_id or kwargs.get(
                "data_source_id", None
            )
            if data_source_id:
                data_source = DataSourceHandler().get_data_source(data_source_id)
                properties[data_source.service_id] = [schema_property]

        return properties


#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# ruleid: python_urlopen_rule-urllib-urlopen
opener.open(url_from_user)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/mako_templating.py
# hash:  8eee173

from mako.template import Template
from mako.lookup import TemplateLookup
import mako

from mako import template

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# XXX(fletcher): for some reason, bandit is missing the one below. keeping it
# in for now so that if it gets fixed inadvertently we know.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Disable default filters so no filters are applied.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# rule ok: python_escaping_rule-use-of-mako-templates
template = Template("hern", default_filters=['h'])  # 'h' is the HTML escape filter

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# rule ok: python_escaping_rule-use-of-mako-templates
TemplateLookup(directories=['/tmp'], default_filters=['h'])  # 'h' is the HTML escape filter

# Disable default filters so no filters are applied.
# ruleid: python_escaping_rule-use-of-mako-templates
TemplateLookup(directories=['/tmp'], default_filters=[])
# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# ruleid: python_requests_rule-request-without-timeout
requests.put('https://gmail.com')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data5 = UsmUserData(username, authKey=None, privKey=None)
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

from django.http import HttpResponse
from urllib.parse import parse_qs, urlparse
from myapp.models import User, Person
from django.shortcuts import render
from django.views import View



import logging
logger = logging.getLogger(__name__)

def index(request):
    return render(request, 'index.html')

def debug(input):
    logger.debug('[DEBUG] %s' % input)
    return input

def test_sql_injection1(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with % operator 
def test_sql_injection2(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with F-Strings
def test_sql_injection3(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object inline with query
# request object format - request.$W.get(...)
# string concatenation with + operator
def test_sql_injection4(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")

#-------------------------------------------------------------

# request object not inline
# string interpolation with str.format() method
def test_sql_injection5(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object not inline
# string interpolation with % operator 
def test_sql_injection6(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '%s'" % uname
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object not inline
# string interpolation with F-Strings
def test_sql_injection7(request):

    uname = request.GET.get('username', 'testuser2')
    query = f'SELECT * FROM myapp_user WHERE username = "{uname}"'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object not inline
# string concatenation with + operator
def test_sql_injection8(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '" + uname + "'"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
#-------------------------------------------------
        

# request object format - request.$W(...)
# string interpolation with str.format() method
def test_sql_injection9(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W(...)
# string interpolation with % operator 
def test_sql_injection10(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W(...)
# string interpolation with F-Strings
def test_sql_injection11(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W(...)
# string concatenation with + operator
def test_sql_injection12(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")  

     
#-------------------------------------------------
    
# request object format - request.$W[...]
# string interpolation with str.format() method
def test_sql_injection13(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W[...]
# string interpolation with % operator 
def test_sql_injection14(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W[...]
# string interpolation with F-Strings
def test_sql_injection15(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W[...]
# string concatenation with + operator
def test_sql_injection16(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  

#----------------------

# Using Parameterized query - Safe
# True negative case
def test_sql_injection17(request):

    uname = request.GET["username"] 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw('SELECT * FROM myapp_user WHERE username = %s', (uname,))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 


# Using translations map
# Not using Parameterized queries
def test_sql_injection18(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 



# Using translations map
# No user input
# True negative
def test_sql_injection19(request):

    name_map = {'username': 'person_name', 'email':'person_email'}

    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user", translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  
        

# Using translations map
# Using Parameterized queries - Safe
# True negative
def test_sql_injection20(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user WHERE username = %s LIMIT 1", [request.GET["username"]], translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")


# With empty parameters as tuple
# Not using Parameterized queries
def test_sql_injection21(request):
    uname = request.GET["username"] 
    res = User.objects.raw(
                # ruleid: python_django_rule-django-raw-used
                "SELECT * FROM myapp_user WHERE username = '%s'" % uname,
                params=(),
                )
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# With empty parameters as list
# No User input: String interpolation % operator
# Not using Parameterized queries
def test_sql_injection22(request):
    uname = request.GET["username"]     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# With empty parameters as list
# No User input
# True Negative
def test_sql_injection23(request):    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % "testuser2", [])
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

#----------------------------------------------------

# No User input
# String interpolation % operator 
# True negative case
def test_sql_injection24(request):

    val = "testuser1" 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % val)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# String concatenation + operator 
# True negative case
def test_sql_injection25(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '" + val +"'"     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with F-Strings
# True negative case
def test_sql_injection26(request):
    val = "testuser2"
    query = f"SELECT * FROM myapp_user WHERE username = '{val}'"    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with str.format() method
# True negative case
def test_sql_injection27(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(val)     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
#----------

# Testing different parameter name
def test_sql_injection27(myrequest):
    uname = myrequest.GET["username"]
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# Testing multiple view arguments
def test_sql_injection28(request, uname):
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing inter-procedural tainted query returned
def intra_file_query_builder(myrequest=None):
    if myrequest is None:
        uname = myrequest.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
    return query

# Testing intra-file inter-procedural vulnerable calls
def test_sql_injection29(myrequest):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing vulnerable calls in class-based views 
class TestSQLInjection30(View):

    # Testing inter-procedural tainted query returned 
    def intra_class_query_builder(self):
        uname = self.request.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
        return query

    # Testing intra-class and inter-procedural vulnerable calls
    def get(self, myrequest):
        # todoruleid: python_django_rule-django-raw-used
        res = User.objects.raw(self.intra_class_query_builder())
        try:
            user = next(iter(res))  
            return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
        except StopIteration:
            return HttpResponse("No such user found.")
        except Exception as e:
            return HttpResponse(f"An error occurred: {str(e)}")     

from .helpers import inter_file_query_builder

# Testing inter-file inter-procedural vulnerable calls with parameters
def test_sql_injection31(myrequest):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing inter-file inter-procedural vulnerable calls without parameters
def test_sql_injection32(myrequest):
    # todoruleid: python_django_rule-django-raw-used
    res = User.objects.raw(inter_file_query_builder())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing safe inter-procedural and inter-file
def intra_file_safe_function(param=None):
    retval = "SELECT * FROM myapp_user WHERE username = 'testuser1'"
    return retval

# Testing safe inter-procedural and intra-file
def test_sql_injection33(myrequest):
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(intra_file_safe_function())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

from .helpers import inter_file_safe_function

# Testing safe inter-procedural and inter-file
def test_sql_injection34(myrequest):
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(inter_file_safe_function())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# ruleid: python_requests_rule-request-without-timeout
requests.get('https://gmail.com', timeout=None, headers={'authorization': f'token 8675309'})
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: MIT (c) GitLab Inc.
# source: https://bandit.readthedocs.io/en/latest/_modules/bandit/plugins/snmp_security_check.html#snmp_insecure_version_check

from pysnmp.hlapi import CommunityData

# SHOULD FAIL
# ruleid: python_snmp_rule-insecure-snmp-version
a = CommunityData('public', mpModel=0)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
    # ruleid: python_requests_rule-request-without-timeout
    return requests.get(url)


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/pycrypto.py
# hash:  8eee173

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_crypto_rule-import-pycrypto
import Crypto.Protocol
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

from . import CryptoMaterialsCacheEntry

def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()


def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()

import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
        #ruleid: python_flask_rule-tainted-sql-string
        result = db.session.execute(text(f'{query_prefix} WHERE firstname = \"{name}\"'))

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/jinja2_templating.py
# hash:  8eee173

import jinja2
from jinja2 import Environment, select_autoescape
templateLoader = jinja2.FileSystemLoader( searchpath="/" )
something = ''

Environment(loader=templateLoader, load=templateLoader, autoescape=True)
templateEnv = jinja2.Environment(autoescape=True,
        loader=templateLoader )
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_escaping_rule-jinja2-autoescape-false
Environment(loader=templateLoader,
            load=templateLoader,
            autoescape=False)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

Environment(loader=templateLoader, autoescape=select_autoescape())

Environment(loader=templateLoader,
            autoescape=select_autoescape(['html', 'htm', 'xml']))


def fake_func():
    return 'foobar'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
    # ruleid: python_requests_rule-request-without-timeout
    return requests.get(url)


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data16 = UsmUserData(username, None, None)
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import sys
import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# ok: python_exec_rule-subprocess-popen-shell-true
pop('/bin/gcc --version', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('/bin/ls *', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % ('this is hardcoded'), shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % 'this is hardcoded', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo {}'.format('this is hardcoded'), shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + 'hi', shell=True)

name = 'David ' + 'Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + name, shell=True)

cmdd = 'echo ' + 'Alex Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmdd, shell=True)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hello!'])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(["/bin/ls",
                 "-l"
                 ])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell='')


command = "/bin/ls" + sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('pwd && echo Hi!', shell=True)


command2 = 'pwd'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='True')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='False')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='None')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell=1)


command6 = sys.argv[2]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command10 = "random"

def Random():
    return 2

cmd = sys.argv[2]
# Popen() creates a new process, which is an object. Objects are truthy. So this is equivalent to shell=True
# todoruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=Random())
# [True] is a non-empty list. In Python, non-empty lists are always truthy. Therefore, shell=[True] is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# {'IS': 'True'} is a non-empty dictionary. Non-empty dictionaries are truthy in Python. Thus, shell={'IS': 'True'} is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# command10 is a string ('random'). Non-empty strings are truthy in Python. Therefore, shell=command is truthy.
# ruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=command10)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=0)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=[])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell={})
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=None)

input = "/" 
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['ls', '-l', input])

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen("echo 'hello'", shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


name = sys.argv[1]
cmd = "echo %s" % name
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

address = "hello there"
cmd = "echo %s" % address
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % "hello"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % ("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)


user = sys.argv[1]
argument = sys.argv[2]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"echo {user} {argument}"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(f"pwd", shell=True)

thedir = sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"pwd"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

command11 = "echo {}".format(sys.argv[1])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def foo(v):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command12 = "echo {}".format("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command12, shell=True)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_etree_celementtree.py
# hash:  8eee173

import xml.etree.cElementTree as badET
import defusedxml.cElementTree as goodET

xmlString = "<note>\n<to>Tove</to>\n<from>Jani</from>\n<heading>Reminder</heading>\n<body>Don't forget me this weekend!</body>\n</note>"

# unsafe
tree = badET.fromstring(xmlString)
print(tree)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_xml_rule-celement
badET.iterparse('filethatdoesntexist.xml')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# safe
tree = goodET.fromstring(xmlString)
print(tree)
# ok: python_xml_rule-celement
goodET.parse('filethatdoesntexist.xml')
# ok: python_xml_rule-celement
goodET.iterparse('filethatdoesntexist.xml')
# ok: python_xml_rule-celement
a = goodET.XMLParser()


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
os.popen("tar cvf archive.tar *")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_requests_rule-request-without-timeout
requests.delete('https://gmail.com', timeout=None)
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: MIT (c) GitLab Inc.

import subprocess
import os
from subprocess import Popen as pop

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-os-popen2
popen2.Popen3("")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


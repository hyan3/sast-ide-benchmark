class PostgresqlLenientDatabaseSchemaEditor:
    """
    Class changes the behavior of the postgres database schema editor slightly. Normally
    when the field type is altered and one of the data columns doesn't have a value that
    can be casted to the new type, it fails. This class introduces the possibility to
    run a custom function, like REGEXP_REPLACE, to convert the data to the correct
    format. If the casting still fails the value will be set to null.
    """

    sql_alter_column_type = (
        "ALTER COLUMN %(column)s TYPE %(type)s%(collation)s "
        "USING pg_temp.try_cast(%(column)s::text)"
    )

    def __init__(
        self,
        *args,
        alter_column_prepare_old_value="",
        alter_column_prepare_new_value="",
        force_alter_column=False,
        **kwargs,
    ):
        self.alter_column_prepare_old_value = alter_column_prepare_old_value
        self.alter_column_prepare_new_value = alter_column_prepare_new_value
        self.force_alter_column = force_alter_column
        super().__init__(*args, **kwargs)

    def _alter_field(
        self,
        model,
        old_field,
        new_field,
        old_type,
        new_type,
        old_db_params,
        new_db_params,
        strict=False,
    ):
        if self.force_alter_column:
            old_type = f"{old_type}_forced"

        if old_type != new_type:
            variables = {}
            if isinstance(self.alter_column_prepare_old_value, tuple):
                alter_column_prepare_old_value, v = self.alter_column_prepare_old_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_old_value = self.alter_column_prepare_old_value

            if isinstance(self.alter_column_prepare_new_value, tuple):
                alter_column_prepare_new_value, v = self.alter_column_prepare_new_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_new_value = self.alter_column_prepare_new_value

            quoted_column_name = self.quote_name(new_field.column)
            for key, value in variables.items():
                variables[key] = value.replace("$FUNCTION$", "")
            self.execute(sql_drop_try_cast)
            self.execute(
                sql_create_try_cast
                % {
                    "column": quoted_column_name,
                    "type": new_type,
                    "alter_column_prepare_old_value": alter_column_prepare_old_value,
                    "alter_column_prepare_new_value": alter_column_prepare_new_value,
                },
                variables,
            )

        return super()._alter_field(
            model,
            old_field,
            new_field,
            old_type,
            new_type,
            old_db_params,
            new_db_params,
            strict,
        )

    def _alter_column_type_sql(
        self, model, old_field, new_field, new_type, old_collation, new_collation
    ):
        # Cast when data type changed.
        # Make ALTER TYPE with SERIAL make sense.
        table = strip_quotes(model._meta.db_table)
        serial_fields_map = {
            "bigserial": "bigint",
            "serial": "integer",
            "smallserial": "smallint",
        }
        if collate_sql := self._collate_sql(
            new_collation, old_collation, model._meta.db_table
        ):
            collate_sql = f" {collate_sql}"
        else:
            collate_sql = ""
        if new_type.lower() in serial_fields_map:
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            return (
                (
                    self.sql_alter_column_type
                    % {
                        "column": self.quote_name(column),
                        "type": serial_fields_map[new_type.lower()],
                        "collation": collate_sql,
                    },
                    [],
                ),
                [
                    (
                        self.sql_delete_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_create_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_alter_column
                        % {
                            "table": self.quote_name(table),
                            "changes": self.sql_alter_column_default
                            % {
                                "column": self.quote_name(column),
                                "default": "nextval('%s')"
                                % self.quote_name(sequence_name),
                            },
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_max
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_owner
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                ],
            )
        elif (
            old_field.db_parameters(connection=self.connection)["type"]
            in serial_fields_map
        ):
            # Drop the sequence if migrating away from AutoField.
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            fragment, _ = BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )
            return fragment, [
                (
                    self.sql_delete_sequence
                    % {
                        "sequence": self.quote_name(sequence_name),
                    },
                    [],
                ),
            ]
        else:
            return BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )

    def _field_should_be_altered(self, old_field, new_field):
        return self.force_alter_column or super()._field_should_be_altered(
            old_field, new_field
        )

class AllApplicationsView(APIView):
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        tags=["Applications"],
        operation_id="list_all_applications",
        description=(
            "Lists all the applications that the authorized user has access to. The "
            "properties that belong to the application can differ per type. An "
            "application always belongs to a single workspace. All the applications of the "
            "workspaces that the user has access to are going to be listed here."
        ),
        responses={
            200: PolymorphicApplicationResponseSerializer(many=True),
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
        },
    )
    @map_exceptions({UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP})
    def get(self, request):
        """
        Responds with a list of serialized applications that belong to the user. If a
        workspace id is provided only the applications of that workspace are going to be
        returned.
        """

        workspaces = Workspace.objects.filter(users=request.user)

        # Compute list of readable application ids
        applications_ids = []
        for workspace in workspaces:
            applications = Application.objects.filter(
                workspace=workspace, workspace__trashed=False
            )
            applications = CoreHandler().filter_queryset(
                request.user,
                ListApplicationsWorkspaceOperationType.type,
                applications,
                workspace=workspace,
            )
            applications_ids += applications.values_list("id", flat=True)

        # Then filter with these ids
        applications = specific_iterator(
            Application.objects.select_related("content_type", "workspace")
            .prefetch_related("workspace__template_set")
            .filter(id__in=applications_ids)
            .order_by("workspace_id", "order"),
            per_content_type_queryset_hook=(
                lambda model, queryset: application_type_registry.get_by_model(
                    model
                ).enhance_queryset(queryset)
            ),
        )

        data = [
            PolymorphicApplicationResponseSerializer(
                application, context={"request": request}
            ).data
            for application in applications
        ]

        return Response(data)

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0025_buttonthemeconfigblock_colorthemeconfigblock_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="center",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_width",
            field=models.CharField(
                choices=[("auto", "Auto"), ("full", "Full")],
                default="auto",
                max_length=10,
            ),
        ),
        migrations.CreateModel(
            name="LinkThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "link_text_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "link_text_color",
                    models.CharField(
                        blank=True,
                        default="primary",
                        help_text="The text color of links",
                        max_length=20,
                    ),
                ),
                (
                    "link_hover_text_color",
                    models.CharField(
                        blank=True,
                        default="#96baf6ff",
                        help_text="The hover color of links when hovered",
                        max_length=20,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_1_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_2_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_3_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_font_size",
            field=models.SmallIntegerField(default=16),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_color",
            field=models.CharField(default="#202128", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="tableelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="repeatelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.CreateModel(
            name="ImageThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "image_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "image_max_width",
                    models.PositiveIntegerField(
                        default=100,
                        help_text="The max-width for this image element.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                0, message="Value cannot be less than 0."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "image_max_height",
                    models.PositiveIntegerField(
                        help_text="The max-height for this image element.",
                        null=True,
                        validators=[
                            django.core.validators.MinValueValidator(
                                5, message="Value cannot be less than 5."
                            ),
                            django.core.validators.MaxValueValidator(
                                3000, message="Value cannot be greater than 3000."
                            ),
                        ],
                    ),
                ),
                (
                    "image_constraint",
                    models.CharField(
                        choices=[
                            ("cover", "Cover"),
                            ("contain", "Contain"),
                            ("full-width", "Full Width"),
                        ],
                        default="contain",
                        help_text="The image constraint to apply to this image",
                        max_length=32,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.RunPython(
            migrate_element_styles, reverse_code=migrations.RunPython.noop
        ),
    ]


# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data18 = UsmUserData(username, securityEngineId=None, securityName=None)
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
# License: MIT (c) GitLab Inc.
import sys
import tarfile
import tempfile


def unsafe_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def managed_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def list_members_archive_handler(filename):
    tar = tarfile.open(filename)
    # ruleid: python_files_rule-tarfile-unsafe-members
    tar.extractall(path=tempfile.mkdtemp(), members=[])
    tar.close()


def provided_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def members_filter(tarfile):
    result = []
    for member in tarfile.getmembers():
        if '../' in member.name:
            print('Member name container directory traversal sequence')
            continue
        elif (member.issym() or member.islnk()) and ('../' in member.linkname):
            print('Symlink to external resource')
            continue
        result.append(member)
    return result


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        unsafe_archive_handler(filename)
        managed_members_archive_handler(filename)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/pycrypto.py
# hash:  8eee173

# ruleid: python_crypto_rule-import-pycrypto
import Crypto.Cipher
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

from . import CryptoMaterialsCacheEntry

def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()


def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data14 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,1))
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("rm *.tmp", shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
os.system('grep "some_search_term" *.py')
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def test_function():
    # ruleid: python_assert_rule-assert-used
    assert 1 == 1

def test_function2():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/requests-ssl-verify-disabled.py
# hash:  8eee173

import requests

requests.get('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', verify=True)
# ruleid: python_ssl_rule-req-no-certvalid
requests.head('https://gmail.com', verify=False)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import sys
import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# ok: python_exec_rule-subprocess-popen-shell-true
pop('/bin/gcc --version', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('/bin/ls *', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % ('this is hardcoded'), shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % 'this is hardcoded', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo {}'.format('this is hardcoded'), shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + 'hi', shell=True)

name = 'David ' + 'Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + name, shell=True)

cmdd = 'echo ' + 'Alex Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmdd, shell=True)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hello!'])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(["/bin/ls",
                 "-l"
                 ])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell='')


command = "/bin/ls" + sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('pwd && echo Hi!', shell=True)


command2 = 'pwd'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='True')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='False')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='None')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell=1)


command6 = sys.argv[2]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command10 = "random"

def Random():
    return 2

cmd = sys.argv[2]
# Popen() creates a new process, which is an object. Objects are truthy. So this is equivalent to shell=True
# todoruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=Random())
# [True] is a non-empty list. In Python, non-empty lists are always truthy. Therefore, shell=[True] is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# {'IS': 'True'} is a non-empty dictionary. Non-empty dictionaries are truthy in Python. Thus, shell={'IS': 'True'} is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# command10 is a string ('random'). Non-empty strings are truthy in Python. Therefore, shell=command is truthy.
# ruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=command10)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=0)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=[])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell={})
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=None)

input = "/" 
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['ls', '-l', input])

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen("echo 'hello'", shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


name = sys.argv[1]
cmd = "echo %s" % name
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

address = "hello there"
cmd = "echo %s" % address
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % "hello"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % ("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)


user = sys.argv[1]
argument = sys.argv[2]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"echo {user} {argument}"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(f"pwd", shell=True)

thedir = sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"pwd"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

command11 = "echo {}".format(sys.argv[1])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def foo(v):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command12 = "echo {}".format("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command12, shell=True)

#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

import os

# ruleid: python_file-permissions_rule-general-bad-permission
os.chmod(key_file, 0o777)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_minidom.py
# hash:  8eee173

from xml.dom.pulldom import parseString as badParseString
from defusedxml.pulldom import parseString as goodParseString

from xml.dom.pulldom import parse as badParse
from defusedxml.pulldom import parse as goodParse

def foo(str):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  print(a)
  b = goodParseString(str)
  print(b)

  # ruleid: python_xml_rule-pulldom
  a = badParse(str)
  print(a)
  # ok: python_xml_rule-pulldom
  b = goodParse(str)
  print(b)

# License: MIT (c) GitLab Inc.
import sys
import tarfile
import tempfile


def unsafe_archive_handler(filename):
    tar = tarfile.open(filename)
    # ruleid: python_files_rule-tarfile-unsafe-members
    tar.extractall(path=tempfile.mkdtemp())
    tar.close()


def managed_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def list_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def provided_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def members_filter(tarfile):
    result = []
    for member in tarfile.getmembers():
        if '../' in member.name:
            print('Member name container directory traversal sequence')
            continue
        elif (member.issym() or member.islnk()) and ('../' in member.linkname):
            print('Symlink to external resource')
            continue
        result.append(member)
    return result


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        unsafe_archive_handler(filename)
        managed_members_archive_handler(filename)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

from Crypto.Hash import MD2 as pycrypto_md2
from Cryptodome.Hash import MD2 as pycryptodomex_md2

# ruleid: python_crypto_rule-hash-md2
pycrypto_md2.new()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/mako_templating.py
# hash:  8eee173

from mako.template import Template
from mako.lookup import TemplateLookup
import mako

from mako import template

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# XXX(fletcher): for some reason, bandit is missing the one below. keeping it
# in for now so that if it gets fixed inadvertently we know.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Disable default filters so no filters are applied.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# rule ok: python_escaping_rule-use-of-mako-templates
template = Template("hern", default_filters=['h'])  # 'h' is the HTML escape filter

# ruleid: python_escaping_rule-use-of-mako-templates
TemplateLookup(directories=['/tmp'])

# rule ok: python_escaping_rule-use-of-mako-templates
TemplateLookup(directories=['/tmp'], default_filters=['h'])  # 'h' is the HTML escape filter

# Disable default filters so no filters are applied.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import sys
import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# ok: python_exec_rule-subprocess-popen-shell-true
pop('/bin/gcc --version', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('/bin/ls *', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % ('this is hardcoded'), shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % 'this is hardcoded', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo {}'.format('this is hardcoded'), shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + 'hi', shell=True)

name = 'David ' + 'Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + name, shell=True)

cmdd = 'echo ' + 'Alex Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmdd, shell=True)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hello!'])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(["/bin/ls",
                 "-l"
                 ])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell='')


command = "/bin/ls" + sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('pwd && echo Hi!', shell=True)


command2 = 'pwd'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='True')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='False')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='None')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell=1)


command6 = sys.argv[2]
# ruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command6, shell='True')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command10 = "random"

def Random():
    return 2

cmd = sys.argv[2]
# Popen() creates a new process, which is an object. Objects are truthy. So this is equivalent to shell=True
# todoruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=Random())
# [True] is a non-empty list. In Python, non-empty lists are always truthy. Therefore, shell=[True] is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# {'IS': 'True'} is a non-empty dictionary. Non-empty dictionaries are truthy in Python. Thus, shell={'IS': 'True'} is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# command10 is a string ('random'). Non-empty strings are truthy in Python. Therefore, shell=command is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=0)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=[])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell={})
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=None)

input = "/" 
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['ls', '-l', input])

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen("echo 'hello'", shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


name = sys.argv[1]
cmd = "echo %s" % name
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

address = "hello there"
cmd = "echo %s" % address
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % "hello"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % ("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)


user = sys.argv[1]
argument = sys.argv[2]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"echo {user} {argument}"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(f"pwd", shell=True)

thedir = sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"pwd"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

command11 = "echo {}".format(sys.argv[1])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def foo(v):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command12 = "echo {}".format("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command12, shell=True)

import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
    #ruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % request.args.get('param')))
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/random_module.py
# hash:  8eee173

import random
import os
import somelib

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_random_rule-random
bad = random.uniform()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

good = os.urandom()
good = random.SystemRandom()

unknown = random()
unknown = somelib.a.random()

# License: MIT (c) GitLab Inc.
import sys
import tarfile
import tempfile


def unsafe_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def managed_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def list_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def provided_members_archive_handler(filename):
    tar = tarfile.open(filename)
    # ruleid: python_files_rule-tarfile-unsafe-members
    tarfile.extractall(path=tempfile.mkdtemp(), members=tar)
    tar.close()


def members_filter(tarfile):
    result = []
    for member in tarfile.getmembers():
        if '../' in member.name:
            print('Member name container directory traversal sequence')
            continue
        elif (member.issym() or member.islnk()) and ('../' in member.linkname):
            print('Symlink to external resource')
            continue
        result.append(member)
    return result


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        unsafe_archive_handler(filename)
        managed_members_archive_handler(filename)

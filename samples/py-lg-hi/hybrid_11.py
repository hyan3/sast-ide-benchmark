class ImageElementWorkspaceStorageUsageItem(WorkspaceStorageUsageItemType):
    type = "image_element"

    def calculate_storage_usage(self, workspace_id: int) -> UsageInMB:
        image_elements = ImageElement.objects.filter(
            page__builder__workspace_id=workspace_id,
            page__trashed=False,
            page__builder__trashed=False,
        )

        usage_in_mb = (
            UserFile.objects.filter(Q(id__in=image_elements.values("image_file")))
            .values("size")
            .aggregate(sum=Coalesce(Sum("size") / USAGE_UNIT_MB, 0))["sum"]
        )

        return usage_in_mb

class AirtableHandler:
    @staticmethod
    def fetch_publicly_shared_base(share_id: str) -> Tuple[str, dict, dict]:
        """
        Fetches the initial page of the publicly shared page. It will parse the content
        and extract and return the initial data needed for future requests.

        :param share_id: The Airtable share id of the page that must be fetched. Note
            that the base must be shared publicly. The id stars with `shr`.
        :raises AirtableShareIsNotABase: When the URL doesn't point to a shared base.
        :return: The request ID, initial data and the cookies of the response.
        """

        url = f"https://airtable.com/{share_id}"
        response = requests.get(url, headers=BASE_HEADERS)  # nosec B113

        if not response.ok:
            raise AirtableBaseNotPublic(
                f"The base with share id {share_id} is not public."
            )

        decoded_content = remove_invalid_surrogate_characters(response.content)

        request_id_re = re.search('requestId: "(.*)",', decoded_content)
        if request_id_re is None:
            raise AirtableShareIsNotABase("The `shared_id` is not a valid base link.")
        request_id = request_id_re.group(1)
        raw_init_data = re.search("window.initData = (.*);\n", decoded_content).group(1)
        init_data = json.loads(raw_init_data)
        cookies = response.cookies.get_dict()

        if "sharedApplicationId" not in raw_init_data:
            raise AirtableShareIsNotABase("The `shared_id` is not a base.")

        return request_id, init_data, cookies

    @staticmethod
    def fetch_table_data(
        table_id: str,
        init_data: dict,
        request_id: str,
        cookies: dict,
        fetch_application_structure: bool,
        stream=True,
    ) -> Response:
        """
        Fetches the data or application structure of a publicly shared Airtable table.

        :param table_id: The Airtable table id that must be fetched. The id starts with
            `tbl`.
        :param init_data: The init_data returned by the initially requested shared base.
        :param request_id: The request_id returned by the initially requested shared
            base.
        :param cookies: The cookies dict returned by the initially requested shared
            base.
        :param fetch_application_structure: Indicates whether the application structure
            must also be fetched. If True, the schema of all the tables and views will
            be included in the response. Note that the structure of the response is
            different because it will wrap application/table schema around the table
            data. The table data will be available at the path `data.tableDatas.0.rows`.
            If False, the only the table data will be included in the response JSON,
            which will be available at the path `data.rows`.
        :param stream: Indicates whether the request should be streamed. This could be
            useful if we want to show a progress bar. It will directly be passed into
            the `requests` request.
        :return: The `requests` response containing the result.
        """

        application_id = list(init_data["rawApplications"].keys())[0]
        client_code_version = init_data["codeVersion"]
        page_load_id = init_data["pageLoadId"]

        stringified_object_params = {
            "includeDataForViewIds": None,
            "shouldIncludeSchemaChecksum": True,
            "mayOnlyIncludeRowAndCellDataForIncludedViews": False,
        }
        access_policy = json.loads(init_data["accessPolicy"])

        if fetch_application_structure:
            stringified_object_params["includeDataForTableIds"] = [table_id]
            url = f"https://airtable.com/v0.3/application/{application_id}/read"
        else:
            url = f"https://airtable.com/v0.3/table/{table_id}/readData"

        response = requests.get(
            url=url,
            stream=stream,
            params={
                "stringifiedObjectParams": json.dumps(stringified_object_params),
                "requestId": request_id,
                "accessPolicy": json.dumps(access_policy),
            },
            headers={
                "x-airtable-application-id": application_id,
                "x-airtable-client-queue-time": "45",
                "x-airtable-inter-service-client": "webClient",
                "x-airtable-inter-service-client-code-version": client_code_version,
                "x-airtable-page-load-id": page_load_id,
                "X-Requested-With": "XMLHttpRequest",
                "x-time-zone": "Europe/Amsterdam",
                "x-user-locale": "en",
                **BASE_HEADERS,
            },
            cookies=cookies,
        )  # nosec B113
        return response

    @staticmethod
    def extract_schema(exports: List[dict]) -> Tuple[dict, dict]:
        """
        Loops over the provided exports and finds the export containing the application
        schema. That will be extracted and the rest of the table data will be moved
        into a dict where the key is the table id.

        :param exports: A list containing all the exports as dicts.
        :return: The database schema and a dict containing the table data.
        """

        schema = None
        tables = {}

        for export in exports:
            if "appBlanket" in export["data"]:
                table_data = export["data"].pop("tableDatas")[0]
                schema = export["data"]
            else:
                table_data = export["data"]

            tables[table_data["id"]] = table_data

        if schema is None:
            raise ValueError("None of the provided exports contains the schema.")

        return schema, tables

    @staticmethod
    def to_baserow_field(
        table: dict,
        column: dict,
    ) -> Union[Tuple[None, None, None], Tuple[Field, FieldType, AirtableColumnType]]:
        """
        Converts the provided Airtable column dict to the right Baserow field object.

        :param table: The Airtable table dict. This is needed to figure out whether the
            field is the primary field.
        :param column: The Airtable column dict. These values will be converted to
            Baserow format.
        :return: The converted Baserow field, field type and the Airtable column type.
        """

        (
            baserow_field,
            airtable_column_type,
        ) = airtable_column_type_registry.from_airtable_column_to_serialized(
            table, column
        )

        if baserow_field is None:
            return None, None, None

        baserow_field_type = field_type_registry.get_by_model(baserow_field)

        try:
            order = next(
                index
                for index, value in enumerate(table["meaningfulColumnOrder"])
                if value["columnId"] == column["id"]
            )
        except StopIteration:
            order = 32767

        baserow_field.id = column["id"]
        baserow_field.pk = 0
        baserow_field.name = column["name"]
        baserow_field.order = order
        baserow_field.primary = (
            baserow_field_type.can_be_primary_field(baserow_field)
            and table["primaryColumnId"] == column["id"]
        )

        return baserow_field, baserow_field_type, airtable_column_type

    @staticmethod
    def to_baserow_row_export(
        row_id_mapping: Dict[str, Dict[str, int]],
        column_mapping: Dict[str, dict],
        row: dict,
        index: int,
        files_to_download: Dict[str, str],
    ) -> dict:
        """
        Converts the provided Airtable record to a Baserow row by looping over the field
        types and executing the `from_airtable_column_value_to_serialized` method.

        :param row_id_mapping: A mapping containing the table as key as the value is
            another mapping where the Airtable row id maps the Baserow row id.
        :param column_mapping: A mapping where the Airtable column id is the value and
            the value containing another mapping with the Airtable column dict and
            Baserow field dict.
        :param row: The Airtable row that must be converted a Baserow row.
        :param index: The index the row has in the table.
        :param files_to_download: A dict that contains all the user file URLs that must
            be downloaded. The key is the file name and the value the URL. Additional
            files can be added to this dict.
        :return: The converted row in Baserow export format.
        """

        created_on = row.get("createdTime")

        if created_on:
            created_on = (
                datetime.strptime(created_on, "%Y-%m-%dT%H:%M:%S.%fZ")
                .replace(tzinfo=timezone.utc)
                .isoformat()
            )

        exported_row = DatabaseExportSerializedStructure.row(
            id=row["id"],
            order=f"{index + 1}.00000000000000000000",
            created_on=created_on,
            updated_on=None,
        )

        # Some empty rows don't have the `cellValuesByColumnId` property because it
        # doesn't contain values, hence the fallback to prevent failing hard.
        cell_values = row.get("cellValuesByColumnId", {})
        for column_id, column_value in cell_values.items():
            if column_id not in column_mapping:
                continue

            mapping_values = column_mapping[column_id]
            baserow_serialized_value = mapping_values[
                "airtable_column_type"
            ].to_baserow_export_serialized_value(
                row_id_mapping,
                mapping_values["raw_airtable_column"],
                mapping_values["baserow_field"],
                column_value,
                files_to_download,
            )
            exported_row[f"field_{column_id}"] = baserow_serialized_value

        return exported_row

    @staticmethod
    def download_files_as_zip(
        files_to_download: Dict[str, str],
        progress_builder: Optional[ChildProgressBuilder] = None,
        files_buffer: Union[None, IOBase] = None,
    ) -> BytesIO:
        """
        Downloads all the user files in the provided dict and adds them to a zip file.
        The key of the dict will be the file name in the zip file.

        :param files_to_download: A dict that contains all the user file URLs that must
            be downloaded. The key is the file name and the value the URL. Additional
            files can be added to this dict.
        :param progress_builder: If provided will be used to build a child progress bar
            and report on this methods progress to the parent of the progress_builder.
        :param files_buffer: Optionally a file buffer can be provided to store the
            downloaded files in. They will be stored in memory if not provided.
        :return: An in memory buffer as zip file containing all the user files.
        """

        if files_buffer is None:
            files_buffer = BytesIO()

        progress = ChildProgressBuilder.build(
            progress_builder, child_total=len(files_to_download.keys())
        )

        with ZipFile(files_buffer, "a", ZIP_DEFLATED, False) as files_zip:
            for index, (file_name, url) in enumerate(files_to_download.items()):
                response = requests.get(url, headers=BASE_HEADERS)  # nosec B113
                files_zip.writestr(file_name, response.content)
                progress.increment(state=AIRTABLE_EXPORT_JOB_DOWNLOADING_FILES)

        return files_buffer

    @classmethod
    def to_baserow_database_export(
        cls,
        init_data: dict,
        schema: dict,
        tables: list,
        progress_builder: Optional[ChildProgressBuilder] = None,
        download_files_buffer: Union[None, IOBase] = None,
    ) -> Tuple[dict, IOBase]:
        """
        Converts the provided raw Airtable database dict to a Baserow export format and
        an in memory zip file containing all the downloaded user files.

        @TODO add the views.
        @TODO preserve the order of least one view.

        :param init_data: The init_data, extracted from the initial page related to the
            shared base.
        :param schema: An object containing the schema of the Airtable base.
        :param tables: a list containing the table data.
        :param progress_builder: If provided will be used to build a child progress bar
            and report on this methods progress to the parent of the progress_builder.
        :param download_files_buffer: Optionally a file buffer can be provided to store
            the downloaded files in. They will be stored in memory if not provided.
        :return: The converted Airtable base in Baserow export format and a zip file
            containing the user files.
        """

        progress = ChildProgressBuilder.build(progress_builder, child_total=1000)
        converting_progress = progress.create_child(
            represents_progress=500,
            total=sum(
                [
                    # Mapping progress
                    len(tables[table["id"]]["rows"])
                    # Table column progress
                    + len(table["columns"])
                    # Table rows progress
                    + len(tables[table["id"]]["rows"])
                    # The table itself.
                    + 1
                    for table in schema["tableSchemas"]
                ]
            ),
        )

        # A list containing all the exported table in Baserow format.
        exported_tables = []

        # A dict containing all the user files that must be downloaded and added to a
        # zip file.
        files_to_download = {}

        # A mapping containing the Airtable table id as key and as value another mapping
        # containing with the key as Airtable row id and the value as new Baserow row
        # id. This mapping is created because Airtable has string row id that look like
        # "recAjnk3nkj5", but Baserow doesn't support string row id, so we need to
        # replace them with a unique int. We need a mapping because there could be
        # references to the row.
        row_id_mapping = defaultdict(dict)
        for index, table in enumerate(schema["tableSchemas"]):
            for row_index, row in enumerate(tables[table["id"]]["rows"]):
                new_id = row_index + 1
                row_id_mapping[table["id"]][row["id"]] = new_id
                row["id"] = new_id
                converting_progress.increment(state=AIRTABLE_EXPORT_JOB_CONVERTING)

        view_id = 0
        for table_index, table in enumerate(schema["tableSchemas"]):
            field_mapping = {}
            files_to_download_for_table = {}

            # Loop over all the columns in the table and try to convert them to Baserow
            # format.
            primary = None
            for column in table["columns"]:
                (
                    baserow_field,
                    baserow_field_type,
                    airtable_column_type,
                ) = cls.to_baserow_field(table, column)
                converting_progress.increment(state=AIRTABLE_EXPORT_JOB_CONVERTING)

                # None means that none of the field types know how to parse this field,
                # so we must ignore it.
                if baserow_field is None:
                    continue

                # Construct a mapping where the Airtable column id is the key and the
                # value contains the raw Airtable column values, Baserow field and
                # the Baserow field type object for later use.
                field_mapping[column["id"]] = {
                    "baserow_field": baserow_field,
                    "baserow_field_type": baserow_field_type,
                    "raw_airtable_column": column,
                    "airtable_column_type": airtable_column_type,
                }
                if baserow_field.primary:
                    primary = baserow_field

            if primary is None:
                # First check if another field can act as the primary field type.
                found_existing_field = False
                for value in field_mapping.values():
                    if field_type_registry.get_by_model(
                        value["baserow_field"]
                    ).can_be_primary_field(value["baserow_field"]):
                        value["baserow_field"].primary = True
                        found_existing_field = True
                        break

                # If none of the existing fields can be primary, we will add a new
                # text field.
                if not found_existing_field:
                    airtable_column = {
                        "id": "primary_field",
                        "name": "Primary field (auto created)",
                        "type": "text",
                    }
                    (
                        baserow_field,
                        baserow_field_type,
                        airtable_column_type,
                    ) = cls.to_baserow_field(table, airtable_column)
                    baserow_field.primary = True
                    field_mapping["primary_id"] = {
                        "baserow_field": baserow_field,
                        "baserow_field_type": baserow_field_type,
                        "raw_airtable_column": airtable_column,
                        "airtable_column_type": airtable_column_type,
                    }

            # Loop over all the fields and convert them to Baserow serialized format.
            exported_fields = [
                value["baserow_field_type"].export_serialized(value["baserow_field"])
                for value in field_mapping.values()
            ]

            # Loop over all the rows in the table and convert them to Baserow format. We
            # need to provide the `row_id_mapping` and `field_mapping` because there
            # could be references to other rows and fields. the
            # `files_to_download_for_table` is needed because every value could be
            # depending on additional files that must later be downloaded.
            exported_rows = []
            for row_index, row in enumerate(tables[table["id"]]["rows"]):
                exported_rows.append(
                    cls.to_baserow_row_export(
                        row_id_mapping,
                        field_mapping,
                        row,
                        row_index,
                        files_to_download_for_table,
                    )
                )
                converting_progress.increment(state=AIRTABLE_EXPORT_JOB_CONVERTING)

            # Create an empty grid view because the importing of views doesn't work
            # yet. It's a bit quick and dirty, but it will be replaced soon.
            grid_view = GridView(pk=0, id=None, name="Grid", order=1)
            grid_view.get_field_options = lambda *args, **kwargs: []
            grid_view_type = view_type_registry.get_by_model(grid_view)
            empty_serialized_grid_view = grid_view_type.export_serialized(
                grid_view, None, None, None
            )
            view_id += 1
            empty_serialized_grid_view["id"] = view_id
            exported_views = [empty_serialized_grid_view]

            exported_table = DatabaseExportSerializedStructure.table(
                id=table["id"],
                name=table["name"],
                order=table_index,
                fields=exported_fields,
                views=exported_views,
                rows=exported_rows,
                data_sync=None,
            )
            exported_tables.append(exported_table)
            converting_progress.increment(state=AIRTABLE_EXPORT_JOB_CONVERTING)

            # Airtable has a mapping of signed URLs for the uploaded files. The
            # mapping is provided in the table payload, and if it exists, we need
            # that URL for download instead of the one originally provided.
            signed_user_content_urls = tables[table["id"]]["signedUserContentUrls"]
            for file_name, url in files_to_download_for_table.items():
                if url in signed_user_content_urls:
                    url = signed_user_content_urls[url]
                files_to_download[file_name] = url

        exported_database = CoreExportSerializedStructure.application(
            id=1,
            name=init_data["rawApplications"][init_data["sharedApplicationId"]]["name"],
            order=1,
            type=DatabaseApplicationType.type,
        )
        exported_database.update(
            **DatabaseExportSerializedStructure.database(tables=exported_tables)
        )

        # After all the tables have been converted to Baserow format, we must
        # download all the user files. Because we first want to the whole conversion to
        # be completed and because we want this to be added to the progress bar, this is
        # done last.
        user_files_zip = cls.download_files_as_zip(
            files_to_download,
            progress.create_child_builder(represents_progress=500),
            download_files_buffer,
        )

        return exported_database, user_files_zip

    @classmethod
    def import_from_airtable_to_workspace(
        cls,
        workspace: Workspace,
        share_id: str,
        storage: Optional[Storage] = None,
        progress_builder: Optional[ChildProgressBuilder] = None,
        download_files_buffer: Union[None, IOBase] = None,
    ) -> Database:
        """
        Downloads all the data of the provided publicly shared Airtable base, converts
        it into Baserow export format, downloads the related files and imports that
        converted base into the provided workspace.

        :param workspace: The workspace where the copy of the Airtable must be added to.
        :param share_id: The shared Airtable ID that must be imported.
        :param storage: The storage where the user files must be saved to.
        :param progress_builder: If provided will be used to build a child progress bar
            and report on this methods progress to the parent of the progress_builder.
        :param download_files_buffer: Optionally a file buffer can be provided to store
            the downloaded files in. They will be stored in memory if not provided.
        :return: The imported database application representing the Airtable base.
        """

        progress = ChildProgressBuilder.build(progress_builder, child_total=1000)

        # Execute the initial request to obtain the initial data that's needed to
        # make the request.
        request_id, init_data, cookies = cls.fetch_publicly_shared_base(share_id)
        progress.increment(state=AIRTABLE_EXPORT_JOB_DOWNLOADING_BASE)

        # Loop over all the tables and make a request for each table to obtain the raw
        # Airtable table data.
        tables = []
        raw_tables = list(
            init_data["singleApplicationScaffoldingData"]["tableById"].keys()
        )
        for index, table_id in enumerate(
            progress.track(
                represents_progress=99,
                state=AIRTABLE_EXPORT_JOB_DOWNLOADING_BASE,
                iterable=raw_tables,
            )
        ):
            response = cls.fetch_table_data(
                table_id=table_id,
                init_data=init_data,
                request_id=request_id,
                cookies=cookies,
                # At least one request must also fetch the application structure that
                # contains the schema of all the tables, so we do this for the first
                # table.
                fetch_application_structure=index == 0,
                stream=False,
            )
            try:
                decoded_content = remove_invalid_surrogate_characters(
                    response.content, response.encoding
                )
                json_decoded_content = json.loads(decoded_content)
            except json.decoder.JSONDecodeError:
                # In some cases, the `remove_invalid_surrogate_characters` results in
                # invalid JSON. It's not completely clear why that is, but this
                # fallback can still produce valid JSON to import in most cases if
                # the original json didn't contain invalid surrogate characters.
                json_decoded_content = response.json()

            tables.append(json_decoded_content)

        # Split database schema from the tables because we need this to be separated
        # later on.
        schema, tables = cls.extract_schema(tables)

        # Convert the raw Airtable data to Baserow export format so we can import that
        # later.
        baserow_database_export, files_buffer = cls.to_baserow_database_export(
            init_data,
            schema,
            tables,
            progress.create_child_builder(represents_progress=300),
            download_files_buffer,
        )

        import_export_config = ImportExportConfig(
            # We are not yet downloading any role/permission data from airtable so
            # nothing to import
            include_permission_data=False,
            reduce_disk_space_usage=False,
        )
        # Import the converted data using the existing method to avoid duplicate code.
        databases, _ = CoreHandler().import_applications_to_workspace(
            workspace,
            [baserow_database_export],
            files_buffer,
            import_export_config,
            storage=storage,
            progress_builder=progress.create_child_builder(represents_progress=600),
        )

        return databases[0].specific


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

from django.http import HttpResponse
from urllib.parse import parse_qs, urlparse
from myapp.models import User, Person
from django.shortcuts import render
from django.views import View



import logging
logger = logging.getLogger(__name__)

def index(request):
    return render(request, 'index.html')

def debug(input):
    logger.debug('[DEBUG] %s' % input)
    return input

def test_sql_injection1(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with % operator 
def test_sql_injection2(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with F-Strings
def test_sql_injection3(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object inline with query
# request object format - request.$W.get(...)
# string concatenation with + operator
def test_sql_injection4(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")

#-------------------------------------------------------------

# request object not inline
# string interpolation with str.format() method
def test_sql_injection5(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object not inline
# string interpolation with % operator 
def test_sql_injection6(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '%s'" % uname
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object not inline
# string interpolation with F-Strings
def test_sql_injection7(request):

    uname = request.GET.get('username', 'testuser2')
    query = f'SELECT * FROM myapp_user WHERE username = "{uname}"'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object not inline
# string concatenation with + operator
def test_sql_injection8(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '" + uname + "'"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
#-------------------------------------------------
        

# request object format - request.$W(...)
# string interpolation with str.format() method
def test_sql_injection9(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W(...)
# string interpolation with % operator 
def test_sql_injection10(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W(...)
# string interpolation with F-Strings
def test_sql_injection11(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W(...)
# string concatenation with + operator
def test_sql_injection12(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")  

     
#-------------------------------------------------
    
# request object format - request.$W[...]
# string interpolation with str.format() method
def test_sql_injection13(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W[...]
# string interpolation with % operator 
def test_sql_injection14(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W[...]
# string interpolation with F-Strings
def test_sql_injection15(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W[...]
# string concatenation with + operator
def test_sql_injection16(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  

#----------------------

# Using Parameterized query - Safe
# True negative case
def test_sql_injection17(request):

    uname = request.GET["username"] 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw('SELECT * FROM myapp_user WHERE username = %s', (uname,))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 


# Using translations map
# Not using Parameterized queries
def test_sql_injection18(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 



# Using translations map
# No user input
# True negative
def test_sql_injection19(request):

    name_map = {'username': 'person_name', 'email':'person_email'}

    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user", translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  
        

# Using translations map
# Using Parameterized queries - Safe
# True negative
def test_sql_injection20(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user WHERE username = %s LIMIT 1", [request.GET["username"]], translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")


# With empty parameters as tuple
# Not using Parameterized queries
def test_sql_injection21(request):
    uname = request.GET["username"] 
    res = User.objects.raw(
                # ruleid: python_django_rule-django-raw-used
                "SELECT * FROM myapp_user WHERE username = '%s'" % uname,
                params=(),
                )
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# With empty parameters as list
# No User input: String interpolation % operator
# Not using Parameterized queries
def test_sql_injection22(request):
    uname = request.GET["username"]     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# With empty parameters as list
# No User input
# True Negative
def test_sql_injection23(request):    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % "testuser2", [])
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

#----------------------------------------------------

# No User input
# String interpolation % operator 
# True negative case
def test_sql_injection24(request):

    val = "testuser1" 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % val)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# String concatenation + operator 
# True negative case
def test_sql_injection25(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '" + val +"'"     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with F-Strings
# True negative case
def test_sql_injection26(request):
    val = "testuser2"
    query = f"SELECT * FROM myapp_user WHERE username = '{val}'"    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with str.format() method
# True negative case
def test_sql_injection27(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(val)     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
#----------

# Testing different parameter name
def test_sql_injection27(myrequest):
    uname = myrequest.GET["username"]
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# Testing multiple view arguments
def test_sql_injection28(request, uname):
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing inter-procedural tainted query returned
def intra_file_query_builder(myrequest=None):
    if myrequest is None:
        uname = myrequest.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
    return query

# Testing intra-file inter-procedural vulnerable calls
def test_sql_injection29(myrequest):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing vulnerable calls in class-based views 
class TestSQLInjection30(View):

    # Testing inter-procedural tainted query returned 
    def intra_class_query_builder(self):
        uname = self.request.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
        return query

    # Testing intra-class and inter-procedural vulnerable calls
    def get(self, myrequest):
        # todoruleid: python_django_rule-django-raw-used
        res = User.objects.raw(self.intra_class_query_builder())
        try:
            user = next(iter(res))  
            return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
        except StopIteration:
            return HttpResponse("No such user found.")
        except Exception as e:
            return HttpResponse(f"An error occurred: {str(e)}")     

from .helpers import inter_file_query_builder

# Testing inter-file inter-procedural vulnerable calls with parameters
def test_sql_injection31(myrequest):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing inter-file inter-procedural vulnerable calls without parameters
def test_sql_injection32(myrequest):
    # todoruleid: python_django_rule-django-raw-used
    res = User.objects.raw(inter_file_query_builder())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing safe inter-procedural and inter-file
def intra_file_safe_function(param=None):
    retval = "SELECT * FROM myapp_user WHERE username = 'testuser1'"
    return retval

# Testing safe inter-procedural and intra-file
def test_sql_injection33(myrequest):
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(intra_file_safe_function())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

from .helpers import inter_file_safe_function

# Testing safe inter-procedural and inter-file
def test_sql_injection34(myrequest):
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(inter_file_safe_function())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_requests_rule-request-without-timeout
requests.head('https://gmail.com', timeout=None)
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: MIT (c) GitLab Inc.
# source: https://github.com/PyCQA/bandit/blob/main/examples/logging_config_insecure_listen.py (incl modifications)

import logging
from logging import config as aliased_cfg

# ruleid: python_log_rule-logging-config-insecure-listen
logging.config.listen(9999) # FAIL
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/ciphers.py
# hash:  8eee173

from Crypto.Cipher import DES as pycrypto_des
from Cryptodome.Cipher import DES as pycryptodomex_des
from Crypto import Random
from Crypto.Util import Counter

key = b'-8B key-'
plaintext = b'We are no longer the knights who say ni!'
nonce = Random.new().read(pycrypto_des.block_size/2)
ctr = Counter.new(pycrypto_des.block_size*8/2, prefix=nonce)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
msg = nonce + cipher.encrypt(plaintext)
nonce = Random.new().read(pycryptodomex_des.block_size/2)
ctr = Counter.new(pycryptodomex_des.block_size*8/2, prefix=nonce)
# ruleid: python_crypto_rule-crypto-cipher-des
cipher = pycryptodomex_des.new(key, pycryptodomex_des.MODE_CTR, counter=ctr)
msg = nonce + cipher.encrypt(plaintext)


#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/ciphers.py
# hash:  8eee173

from Crypto.Cipher import Blowfish as pycrypto_blowfish
from Cryptodome.Cipher import Blowfish as pycryptodomex_blowfish


iv = Random.new().read(bs)
key = b'An arbitrarily long key'
plaintext = b'docendo discimus '
plen = bs - divmod(len(plaintext),bs)[1]
padding = [plen]*plen
padding = pack('b'*plen, *padding)
bs = pycrypto_blowfish.block_size
# ruleid: python_crypto_rule-crypto-cipher-blowfish
cipher = pycrypto_blowfish.new(key, pycrypto_blowfish.MODE_CBC, iv)
msg = iv + cipher.encrypt(plaintext + padding)
bs = pycryptodomex_blowfish.block_size
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
msg = iv + cipher.encrypt(plaintext + padding)



# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import sys
import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# ok: python_exec_rule-subprocess-popen-shell-true
pop('/bin/gcc --version', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('/bin/ls *', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % ('this is hardcoded'), shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % 'this is hardcoded', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo {}'.format('this is hardcoded'), shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + 'hi', shell=True)

name = 'David ' + 'Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + name, shell=True)

cmdd = 'echo ' + 'Alex Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmdd, shell=True)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hello!'])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(["/bin/ls",
                 "-l"
                 ])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell='')


command = "/bin/ls" + sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('pwd && echo Hi!', shell=True)


command2 = 'pwd'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='True')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='False')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='None')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell=1)


command6 = sys.argv[2]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command10 = "random"

def Random():
    return 2

cmd = sys.argv[2]
# Popen() creates a new process, which is an object. Objects are truthy. So this is equivalent to shell=True
# todoruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=Random())
# [True] is a non-empty list. In Python, non-empty lists are always truthy. Therefore, shell=[True] is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# {'IS': 'True'} is a non-empty dictionary. Non-empty dictionaries are truthy in Python. Thus, shell={'IS': 'True'} is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# command10 is a string ('random'). Non-empty strings are truthy in Python. Therefore, shell=command is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=0)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=[])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell={})
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=None)

input = "/" 
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['ls', '-l', input])

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen("echo 'hello'", shell=True)

# ruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen("ls {} ".format(sys.argv[1]), shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


name = sys.argv[1]
cmd = "echo %s" % name
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

address = "hello there"
cmd = "echo %s" % address
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % "hello"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % ("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)


user = sys.argv[1]
argument = sys.argv[2]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"echo {user} {argument}"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(f"pwd", shell=True)

thedir = sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"pwd"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

command11 = "echo {}".format(sys.argv[1])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def foo(v):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command12 = "echo {}".format("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command12, shell=True)

import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

    #ruleid: python_flask_rule-tainted-sql-string
    query="INSERT INTO user(firstname, lastname, email, age) VALUES ( '%s','todo','todo@todo.net',99);" % name
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
# License: MIT (c) GitLab Inc.

import subprocess
import os
from subprocess import Popen as pop

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-os-popen2
popen2.Popen3("")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

import hashlib

# ruleid: python_crypto_rule-hash-sha1
hashlib.sha1(1)


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/no_host_key_verification.py
# hash:  8eee173

from paramiko import client

ssh_client = client.SSHClient()
# ruleid: python_ssh_rule-ssh-nohost-key-verification
ssh_client.set_missing_host_key_policy(client.AutoAddPolicy)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/marshal_deserialize.py
# hash:  8eee173

import marshal
import tempfile

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_deserialization_rule-marshal
print(marshal.loads(serialized))

file_obj = tempfile.TemporaryFile()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
file_obj.seek(0)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
file_obj.close()

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_requests_rule-request-without-timeout
requests.get('https://gmail.com', timeout=None)
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bar)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data10 = UsmUserData(username, privKey=None, privProtocol=usmNoPrivProtocol)
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def test_function():
    # ruleid: python_assert_rule-assert-used
    assert 1 == 1

def test_function2():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: MIT (c) GitLab Inc.
import sys
import tarfile
import tempfile


def unsafe_archive_handler(filename):
    tar = tarfile.open(filename)
    # ruleid: python_files_rule-tarfile-unsafe-members
    tar.extractall(path=tempfile.mkdtemp())
    tar.close()


def managed_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def list_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def provided_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def members_filter(tarfile):
    result = []
    for member in tarfile.getmembers():
        if '../' in member.name:
            print('Member name container directory traversal sequence')
            continue
        elif (member.issym() or member.islnk()) and ('../' in member.linkname):
            print('Symlink to external resource')
            continue
        result.append(member)
    return result


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        unsafe_archive_handler(filename)
        managed_members_archive_handler(filename)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/yaml_load.py
# hash:  8eee173

import json
import yaml

def test_yaml_load():
    ystr = yaml.dump({'a': 1, 'b': 2, 'c': 3})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    yaml.dump(y)
    try:
        yaml.load(ystr, Loader=yaml.CSafeLoader)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: python_deserialization_rule-yaml-load
        yaml.load_all(ystr, Loader=yaml.CLoader)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    except AttributeError:
        # CSafeLoader only exists if you build yaml with LibYAML
        y = yaml.load(ystr, Loader=yaml.SafeLoader)

def test_json_load():
    # no issue should be found
    j = json.load("{}")

#!/usr/bin/env python

# SETUP

import os

content = """
__import__('datetime').datetime.now()
"""
with open("eval.input","w+") as f:
	f.write(content)

# NOTES
#   String Literals f, """ """, r'', b'', fr"", etc should all be detected by "...", but still some tests are added as assurance. 

# single string argument
#ok: python_eval_rule-eval
eval("1+1")

# literal string concatenation 
#ok: python_eval_rule-eval
print(eval("1"+"+"+"1"))

# implicit string concatenation
#ok: python_eval_rule-eval
eval("1+" "1")

plus="+"
one= "1"

# variable string concatenation
#ok: python_eval_rule-eval
eval("1"+plus+"1")

# format string with single argument
#ok: python_eval_rule-eval
eval(f"1{plus}1")

# raw format string with single argument
#ok: python_eval_rule-eval
eval(rf"1{plus}1")

# format string with multiple arguments
#ok: python_eval_rule-eval
eval(f"1{plus}{one}")

# single string interpolation
#known false positive, semgrep limitations in understanding of Python string interpolations
#ok: python_eval_rule-eval
eval("1%s"%"+1")

# multiple string interpolation
#known false positive, semgrep limitations in understanding of Python string interpolations
#ok: python_eval_rule-eval
eval( "1 %s %s" % ("+", "1") )


# string concatenation with casted integer constant
#known false positive, semgrep limitations in understanding of Python string constants
#ok: python_eval_rule-eval
#eval("1+"+str(1))
#TODO: enable once SemGrep's String Constant Propagation improves.

# string literal and standard library call
#ok: python_eval_rule-eval
print(eval("os.getcwd()"))

# format string literal and standard library call
#ok: python_eval_rule-eval
print(eval(f"os.getcwd()"))

# format string literal and standard library call
#ok: python_eval_rule-eval
print(eval(r"os.getcwd()"))

# triple-quote string literal and standard library call
#ok: python_eval_rule-eval
print(eval("""os.getcwd()"""))

#following call evaluates a hard-coded string which results in a predetermined behavior and a harmless output
#ok: python_eval_rule-eval
content = eval("__builtins__.open('eval.input','r').read()")
print(content)

print(
    #following call, as opposed to the previous one, evaluates a string with known procedence but unknown content and thus exhibits a non-deterministic, externally-controlled behavior
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
)

print(
    #this call evaluates a hard-coded expression which is parametrized using str.format and externally provided input 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
)

print(
    #this call evaluates a hard-coded expression which is parametrized using string interpolation and externally provided input 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
)

#ok: python_eval_rule-eval
eval("os.chmod('%s', 0o666)" % 'eval.input')

# User-defined functions and methods named "eval" should not trigger a finding.

class Foo(object):

    #ok: python_eval_rule-eval 
    def eval(self):
        print("User-defined eval method")

    def foo(self):
        #ok: python_eval_rule-eval 
        self.eval()

class Bar(object):
    def bar(self,input):
        #ruleid: python_eval_rule-eval
        eval(input)

        def eval(input):
             print("User-defined eval method")
        #ok: python_eval_rule-eval
        eval(input)

#ok: python_eval_rule-eval 
Foo().eval()

class MyClass:
    def __init__():
          return
    def eval(input):
          print('harmless')
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# call without arguments, superfluous but not dangerous.
#ok: python_eval_rule-eval 
eval()

#ok: python_eval_rule-eval 
def eval(input):
	print("Harmless user-defined eval function")

#known false positive, requires reflection or ability to compare eval.__module__=='builtins'
#ok: python_eval_rule-eval 
eval(content)


# TEARDOWN

os.remove("eval.input")

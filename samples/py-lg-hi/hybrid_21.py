class PostgresqlLenientDatabaseSchemaEditor:
    """
    Class changes the behavior of the postgres database schema editor slightly. Normally
    when the field type is altered and one of the data columns doesn't have a value that
    can be casted to the new type, it fails. This class introduces the possibility to
    run a custom function, like REGEXP_REPLACE, to convert the data to the correct
    format. If the casting still fails the value will be set to null.
    """

    sql_alter_column_type = (
        "ALTER COLUMN %(column)s TYPE %(type)s%(collation)s "
        "USING pg_temp.try_cast(%(column)s::text)"
    )

    def __init__(
        self,
        *args,
        alter_column_prepare_old_value="",
        alter_column_prepare_new_value="",
        force_alter_column=False,
        **kwargs,
    ):
        self.alter_column_prepare_old_value = alter_column_prepare_old_value
        self.alter_column_prepare_new_value = alter_column_prepare_new_value
        self.force_alter_column = force_alter_column
        super().__init__(*args, **kwargs)

    def _alter_field(
        self,
        model,
        old_field,
        new_field,
        old_type,
        new_type,
        old_db_params,
        new_db_params,
        strict=False,
    ):
        if self.force_alter_column:
            old_type = f"{old_type}_forced"

        if old_type != new_type:
            variables = {}
            if isinstance(self.alter_column_prepare_old_value, tuple):
                alter_column_prepare_old_value, v = self.alter_column_prepare_old_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_old_value = self.alter_column_prepare_old_value

            if isinstance(self.alter_column_prepare_new_value, tuple):
                alter_column_prepare_new_value, v = self.alter_column_prepare_new_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_new_value = self.alter_column_prepare_new_value

            quoted_column_name = self.quote_name(new_field.column)
            for key, value in variables.items():
                variables[key] = value.replace("$FUNCTION$", "")
            self.execute(sql_drop_try_cast)
            self.execute(
                sql_create_try_cast
                % {
                    "column": quoted_column_name,
                    "type": new_type,
                    "alter_column_prepare_old_value": alter_column_prepare_old_value,
                    "alter_column_prepare_new_value": alter_column_prepare_new_value,
                },
                variables,
            )

        return super()._alter_field(
            model,
            old_field,
            new_field,
            old_type,
            new_type,
            old_db_params,
            new_db_params,
            strict,
        )

    def _alter_column_type_sql(
        self, model, old_field, new_field, new_type, old_collation, new_collation
    ):
        # Cast when data type changed.
        # Make ALTER TYPE with SERIAL make sense.
        table = strip_quotes(model._meta.db_table)
        serial_fields_map = {
            "bigserial": "bigint",
            "serial": "integer",
            "smallserial": "smallint",
        }
        if collate_sql := self._collate_sql(
            new_collation, old_collation, model._meta.db_table
        ):
            collate_sql = f" {collate_sql}"
        else:
            collate_sql = ""
        if new_type.lower() in serial_fields_map:
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            return (
                (
                    self.sql_alter_column_type
                    % {
                        "column": self.quote_name(column),
                        "type": serial_fields_map[new_type.lower()],
                        "collation": collate_sql,
                    },
                    [],
                ),
                [
                    (
                        self.sql_delete_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_create_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_alter_column
                        % {
                            "table": self.quote_name(table),
                            "changes": self.sql_alter_column_default
                            % {
                                "column": self.quote_name(column),
                                "default": "nextval('%s')"
                                % self.quote_name(sequence_name),
                            },
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_max
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_owner
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                ],
            )
        elif (
            old_field.db_parameters(connection=self.connection)["type"]
            in serial_fields_map
        ):
            # Drop the sequence if migrating away from AutoField.
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            fragment, _ = BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )
            return fragment, [
                (
                    self.sql_delete_sequence
                    % {
                        "sequence": self.quote_name(sequence_name),
                    },
                    [],
                ),
            ]
        else:
            return BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )

    def _field_should_be_altered(self, old_field, new_field):
        return self.force_alter_column or super()._field_should_be_altered(
            old_field, new_field
        )

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0025_buttonthemeconfigblock_colorthemeconfigblock_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="center",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_width",
            field=models.CharField(
                choices=[("auto", "Auto"), ("full", "Full")],
                default="auto",
                max_length=10,
            ),
        ),
        migrations.CreateModel(
            name="LinkThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "link_text_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "link_text_color",
                    models.CharField(
                        blank=True,
                        default="primary",
                        help_text="The text color of links",
                        max_length=20,
                    ),
                ),
                (
                    "link_hover_text_color",
                    models.CharField(
                        blank=True,
                        default="#96baf6ff",
                        help_text="The hover color of links when hovered",
                        max_length=20,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_1_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_2_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_3_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_font_size",
            field=models.SmallIntegerField(default=16),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_color",
            field=models.CharField(default="#202128", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="tableelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="repeatelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.CreateModel(
            name="ImageThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "image_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "image_max_width",
                    models.PositiveIntegerField(
                        default=100,
                        help_text="The max-width for this image element.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                0, message="Value cannot be less than 0."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "image_max_height",
                    models.PositiveIntegerField(
                        help_text="The max-height for this image element.",
                        null=True,
                        validators=[
                            django.core.validators.MinValueValidator(
                                5, message="Value cannot be less than 5."
                            ),
                            django.core.validators.MaxValueValidator(
                                3000, message="Value cannot be greater than 3000."
                            ),
                        ],
                    ),
                ),
                (
                    "image_constraint",
                    models.CharField(
                        choices=[
                            ("cover", "Cover"),
                            ("contain", "Contain"),
                            ("full-width", "Full Width"),
                        ],
                        default="contain",
                        help_text="The image constraint to apply to this image",
                        max_length=32,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.RunPython(
            migrate_element_styles, reverse_code=migrations.RunPython.noop
        ),
    ]

class ImageElementWorkspaceStorageUsageItem(WorkspaceStorageUsageItemType):
    type = "image_element"

    def calculate_storage_usage(self, workspace_id: int) -> UsageInMB:
        image_elements = ImageElement.objects.filter(
            page__builder__workspace_id=workspace_id,
            page__trashed=False,
            page__builder__trashed=False,
        )

        usage_in_mb = (
            UserFile.objects.filter(Q(id__in=image_elements.values("image_file")))
            .values("size")
            .aggregate(sum=Coalesce(Sum("size") / USAGE_UNIT_MB, 0))["sum"]
        )

        return usage_in_mb

class ImageElementWorkspaceStorageUsageItem(WorkspaceStorageUsageItemType):
    type = "image_element"

    def calculate_storage_usage(self, workspace_id: int) -> UsageInMB:
        image_elements = ImageElement.objects.filter(
            page__builder__workspace_id=workspace_id,
            page__trashed=False,
            page__builder__trashed=False,
        )

        usage_in_mb = (
            UserFile.objects.filter(Q(id__in=image_elements.values("image_file")))
            .values("size")
            .aggregate(sum=Coalesce(Sum("size") / USAGE_UNIT_MB, 0))["sum"]
        )

        return usage_in_mb

class UserSourceJSONWebTokenAuthentication(JWTAuthentication):
    """
    Authentication middleware that allow user source users to authenticate. All the
    authentication logic is delegated to the user source used to generate the token.
    """

    def __init__(
        self, use_user_source_authentication_header: bool = False, *args, **kwargs
    ):
        """
        :param use_user_source_authentication_header: Set to True if you want to
          authentication using a special `settings.USER_SOURCE_AUTHENTICATION_HEADER`
          header. This is useful when you want to keep the main authentication in the
          `Authorization` header but still want a "double" authentication with a user
          source user.
        """

        self.use_user_source_authentication_header = (
            use_user_source_authentication_header
        )
        super().__init__(*args, **kwargs)

    def get_header(self, request):
        if self.use_user_source_authentication_header:
            # Instead of reading the usual `Authorization` header we use a custom
            # header to authenticate the user source user.
            header = request.headers.get(
                settings.USER_SOURCE_AUTHENTICATION_HEADER, None
            )
            if isinstance(header, str):
                # Work around django test client oddness
                header = header.encode(HTTP_HEADER_ENCODING)

            return header
        else:
            return super().get_header(request)

    def authenticate(self, request: Request) -> Optional[Tuple[AuthUser, Token]]:
        """
        This method authenticate a user source user if the token has been generated
        by a user source.

        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication. Otherwise returns `None`.
        """

        header = self.get_header(request)
        if header is None:
            return None

        raw_token = self.get_raw_token(header)
        if raw_token is None:
            return None

        try:
            validated_token = UserSourceAccessToken(raw_token)
            user_source_uid = validated_token[USER_SOURCE_CLAIM]
            user_id = validated_token[jwt_settings.USER_ID_CLAIM]
        except (TokenError, KeyError):
            # Probably not a user source token let's skip this auth backend
            return None

        try:
            if self.use_user_source_authentication_header:
                # Using service here due to the "double authentication" situation.
                # This call has been triggered by the user source middleware
                # following initial DRF authentication.
                # The service ensures the authenticated user with the right
                # permissions regarding the user_source.
                user_source = UserSourceService().get_user_source_by_uid(
                    getattr(request, "user", AnonymousUser()),
                    user_source_uid,
                    for_authentication=True,
                )
            else:
                user_source = UserSourceHandler().get_user_source_by_uid(
                    user_source_uid,
                )
        except UserSourceDoesNotExist as exc:
            raise exceptions.AuthenticationFailed(
                detail={
                    "detail": "The user source does not exist",
                    "error": "user_source_does_not_exist",
                },
                code="user_source_does_not_exist",
            ) from exc
        except (PermissionDenied, UserNotInWorkspace) as exc:
            raise exceptions.AuthenticationFailed(
                detail={
                    "detail": (
                        "You are not allowed to use this user source to authenticate"
                    ),
                    "error": "user_source_not_allowed",
                },
                code="user_source_not_allowed",
            ) from exc

        try:
            user = user_source.get_type().get_user(user_source, user_id=user_id)
        except UserNotFound as exc:
            raise exceptions.AuthenticationFailed(
                detail={"detail": "User not found", "error": "user_not_found"},
                code="user_not_found",
            ) from exc

        if not self.use_user_source_authentication_header:
            try:
                CoreHandler().check_permissions(
                    user,
                    AuthenticateUserSourceOperationType.type,
                    workspace=user_source.application.workspace,
                    context=user_source,
                )
            except (PermissionDenied, UserNotInWorkspace) as exc:
                raise exceptions.AuthenticationFailed(
                    detail={
                        "detail": (
                            "You are not allowed to use this user source to "
                            "authenticate"
                        ),
                        "error": "user_source_not_allowed",
                    },
                    code="user_source_not_allowed",
                ) from exc

        return (
            user,
            validated_token,
        )


# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_requests_rule-request-without-timeout
requests.head('https://gmail.com', timeout=None)
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import sys
import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# ok: python_exec_rule-subprocess-popen-shell-true
pop('/bin/gcc --version', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('/bin/ls *', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % ('this is hardcoded'), shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % 'this is hardcoded', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo {}'.format('this is hardcoded'), shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + 'hi', shell=True)

name = 'David ' + 'Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + name, shell=True)

cmdd = 'echo ' + 'Alex Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmdd, shell=True)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hello!'])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(["/bin/ls",
                 "-l"
                 ])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell='')


command = "/bin/ls" + sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('pwd && echo Hi!', shell=True)


command2 = 'pwd'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='True')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='False')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='None')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell=1)


command6 = sys.argv[2]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command10 = "random"

def Random():
    return 2

cmd = sys.argv[2]
# Popen() creates a new process, which is an object. Objects are truthy. So this is equivalent to shell=True
# todoruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=Random())
# [True] is a non-empty list. In Python, non-empty lists are always truthy. Therefore, shell=[True] is truthy.
# ruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=[True])
# {'IS': 'True'} is a non-empty dictionary. Non-empty dictionaries are truthy in Python. Thus, shell={'IS': 'True'} is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# command10 is a string ('random'). Non-empty strings are truthy in Python. Therefore, shell=command is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=0)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=[])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell={})
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=None)

input = "/" 
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['ls', '-l', input])

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen("echo 'hello'", shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


name = sys.argv[1]
cmd = "echo %s" % name
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

address = "hello there"
cmd = "echo %s" % address
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % "hello"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % ("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)


user = sys.argv[1]
argument = sys.argv[2]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"echo {user} {argument}"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(f"pwd", shell=True)

thedir = sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"pwd"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

command11 = "echo {}".format(sys.argv[1])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def foo(v):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command12 = "echo {}".format("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command12, shell=True)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import sys
import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# ok: python_exec_rule-subprocess-popen-shell-true
pop('/bin/gcc --version', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('/bin/ls *', shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % ('this is hardcoded'), shell=True)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo %s' % 'this is hardcoded', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo {}'.format('this is hardcoded'), shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + 'hi', shell=True)

name = 'David ' + 'Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('echo ' + name, shell=True)

cmdd = 'echo ' + 'Alex Rose'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmdd, shell=True)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hello!'])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(["/bin/ls",
                 "-l"
                 ])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['echo', 'Hi!'], shell='')


command = "/bin/ls" + sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen('pwd && echo Hi!', shell=True)


command2 = 'pwd'
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='True')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='False')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell='None')
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command2, shell=1)


command6 = sys.argv[2]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command10 = "random"

def Random():
    return 2

cmd = sys.argv[2]
# Popen() creates a new process, which is an object. Objects are truthy. So this is equivalent to shell=True
# todoruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=Random())
# [True] is a non-empty list. In Python, non-empty lists are always truthy. Therefore, shell=[True] is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# {'IS': 'True'} is a non-empty dictionary. Non-empty dictionaries are truthy in Python. Thus, shell={'IS': 'True'} is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# command10 is a string ('random'). Non-empty strings are truthy in Python. Therefore, shell=command is truthy.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=False)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=0)
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=[])
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell={})
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=None)

input = "/" 
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(['ls', '-l', input])

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen("echo 'hello'", shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


name = sys.argv[1]
cmd = "echo %s" % name
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

address = "hello there"
cmd = "echo %s" % address
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % "hello"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

cmd = "echo %s" % ("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)


user = sys.argv[1]
argument = sys.argv[2]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"echo {user} {argument}"
# ruleid: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(f"pwd", shell=True)

thedir = sys.argv[1]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

cmd = f"pwd"
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(cmd, shell=True)

command11 = "echo {}".format(sys.argv[1])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def foo(v):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

command12 = "echo {}".format("hello")
# ok: python_exec_rule-subprocess-popen-shell-true
subprocess.Popen(command12, shell=True)

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# ruleid: python_requests_rule-request-without-timeout
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_urlopen_rule-urllib-urlopen
opener.retrieve(url_from_user)
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record']))
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: MIT (c) GitLab Inc.

import subprocess
import os
from subprocess import Popen as pop

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-os-popen2
commands.getstatusoutput("")


# License: MIT (c) GitLab Inc.
import requests

# ruleid: python_requests_rule-request-without-timeout
requests.get('https://gmail.com')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
os.system('grep "some_search_term" *.py')
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/pycrypto.py
# hash:  8eee173

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_crypto_rule-import-pycrypto
import Crypto.IO
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

from . import CryptoMaterialsCacheEntry

def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()


def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# ruleid: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen(url_from_user)
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

#!/usr/bin/env python
# License: GNU Lesser General Public License v2.1
# source: https://github.com/semgrep/semgrep-rules/blob/release/python/jwt/security/jwt-none-alg.py

import jwt


def bad1():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return encoded


def bad2(encoded):
    # ruleid: python_jwt_rule-jwt-none-alg
    jwt.decode(encoded, None, algorithms=['none'])
    return encoded


def ok(secret_key):
    # ok: python_jwt_rule-jwt-none-alg
    encoded = jwt.encode({'some': 'payload'}, secret_key, algorithm='HS256')
    return encoded

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data9 = UsmUserData(username, authKey=auth_key, authProtocol=usmNoAuthProtocol)
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
    #ruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable04_helper(request.args)))
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

from Crypto.Hash import SHA as pycrypto_sha
from Cryptodome.Hash import SHA as pycryptodomex_sha

# ruleid: python_crypto_rule-crypto-hash-sha1
pycrypto_sha.new()

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
    #ruleid: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(f"""SELECT * FROM main.user 
    WHERE firstname LIKE '%{name}%';""")
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
os.popen("chmod 777 *")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# ruleid: python_requests_rule-request-without-timeout
requests.get('https://gmail.com', timeout=None, headers={'authorization': f'token 8675309'})
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

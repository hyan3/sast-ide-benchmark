class ChoiceElementType(FormElementTypeMixin, ElementType):
    type = "choice"
    model_class = ChoiceElement
    allowed_fields = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    request_serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    simple_formula_fields = [
        "label",
        "default_value",
        "placeholder",
        "formula_value",
        "formula_name",
    ]

    class SerializedDict(ElementDict):
        label: BaserowFormula
        required: bool
        placeholder: BaserowFormula
        default_value: BaserowFormula
        options: List
        multiple: bool
        show_as_dropdown: bool
        option_type: str
        formula_value: BaserowFormula
        formula_name: BaserowFormula

    @property
    def serializer_field_overrides(self):
        from baserow.contrib.builder.api.theme.serializers import (
            DynamicConfigBlockSerializer,
        )
        from baserow.contrib.builder.theme.theme_config_block_types import (
            InputThemeConfigBlockType,
        )
        from baserow.core.formula.serializers import FormulaSerializerField

        overrides = {
            "label": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("label").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "default_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("default_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "required": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("required").help_text,
                default=False,
                required=False,
            ),
            "placeholder": serializers.CharField(
                help_text=ChoiceElement._meta.get_field("placeholder").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "options": ChoiceOptionSerializer(
                source="choiceelementoption_set", many=True, required=False
            ),
            "multiple": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("multiple").help_text,
                default=False,
                required=False,
            ),
            "show_as_dropdown": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("show_as_dropdown").help_text,
                default=True,
                required=False,
            ),
            "option_type": serializers.ChoiceField(
                choices=ChoiceElement.OPTION_TYPE.choices,
                help_text=ChoiceElement._meta.get_field("option_type").help_text,
                required=False,
                default=ChoiceElement.OPTION_TYPE.MANUAL,
            ),
            "formula_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "formula_name": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_name").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "styles": DynamicConfigBlockSerializer(
                required=False,
                property_name="input",
                theme_config_block_type_name=InputThemeConfigBlockType.type,
                serializer_kwargs={"required": False},
            ),
        }

        return overrides

    @property
    def request_serializer_field_overrides(self):
        return {
            **self.serializer_field_overrides,
            "options": ChoiceOptionSerializer(many=True, required=False),
        }

    def serialize_property(
        self,
        element: ChoiceElement,
        prop_name: str,
        files_zip=None,
        storage=None,
        cache=None,
    ):
        if prop_name == "options":
            return [
                self.serialize_option(option)
                for option in element.choiceelementoption_set.all()
            ]

        return super().serialize_property(
            element, prop_name, files_zip=files_zip, storage=storage, cache=cache
        )

    def import_serialized(
        self,
        parent: Any,
        serialized_values: Dict[str, Any],
        id_mapping: Dict[str, Dict[int, int]],
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        choice_element = super().import_serialized(
            parent,
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

        options = []
        for option in serialized_values.get("options", []):
            option["choice_id"] = choice_element.id
            option_deserialized = self.deserialize_option(option)
            options.append(option_deserialized)

        ChoiceElementOption.objects.bulk_create(options)

        return choice_element

    def create_instance_from_serialized(
        self,
        serialized_values: Dict[str, Any],
        id_mapping,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        serialized_values.pop("options", None)
        return super().create_instance_from_serialized(
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def serialize_option(self, option: ChoiceElementOption) -> Dict:
        return {
            "value": option.value,
            "name": option.name,
            "choice_id": option.choice_id,
        }

    def deserialize_option(self, value: Dict):
        return ChoiceElementOption(**value)

    def get_pytest_params(self, pytest_data_fixture) -> Dict[str, Any]:
        return {
            "label": "'test'",
            "default_value": "'option 1'",
            "required": False,
            "placeholder": "'some placeholder'",
            "multiple": False,
            "show_as_dropdown": True,
            "option_type": ChoiceElement.OPTION_TYPE.MANUAL,
            "formula_value": "",
            "formula_name": "",
        }

    def after_create(self, instance: ChoiceElement, values: Dict):
        options = values.get("options", [])

        ChoiceElementOption.objects.bulk_create(
            [ChoiceElementOption(choice=instance, **option) for option in options]
        )

    def after_update(
        self, instance: ChoiceElement, values: Dict, changes: Dict[str, Tuple]
    ):
        options = values.get("options", None)

        if options is not None:
            ChoiceElementOption.objects.filter(choice=instance).delete()
            ChoiceElementOption.objects.bulk_create(
                [ChoiceElementOption(choice=instance, **option) for option in options]
            )

    def is_valid(
        self,
        element: ChoiceElement,
        value: Union[List, str],
        dispatch_context: DispatchContext,
    ) -> str:
        """
        Responsible for validating `ChoiceElement` form data. We handle
        this validation a little differently to ensure that if someone creates
        an option with a blank value, it's considered valid.

        :param element: The choice element.
        :param value: The choice value we want to validate.
        :return: The value if it is valid for this element.
        """

        options_tuple = set(
            element.choiceelementoption_set.values_list("value", "name")
        )
        options = [
            value if value is not None else name for (value, name) in options_tuple
        ]

        if element.option_type == ChoiceElement.OPTION_TYPE.FORMULAS:
            options = ensure_array(
                resolve_formula(
                    element.formula_value,
                    formula_runtime_function_registry,
                    dispatch_context,
                )
            )
            options = [ensure_string_or_integer(option) for option in options]

        if element.multiple:
            try:
                value = ensure_array(value)
            except ValidationError as exc:
                raise FormDataProviderChunkInvalidException(
                    "The value must be an array or convertible to an array."
                ) from exc

            if not value:
                if element.required:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            else:
                for v in value:
                    if v not in options:
                        raise FormDataProviderChunkInvalidException(
                            f"{value} is not a valid option."
                        )
        else:
            if not value:
                if element.required and value not in options:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            elif value not in options:
                raise FormDataProviderChunkInvalidException(
                    f"{value} is not a valid option."
                )

        return value

class TokenPermissionsField(serializers.Field):
    default_error_messages = {
        "invalid_key": "Only create, read, update and delete keys are allowed.",
        "invalid_value": (
            "The value must either be a bool, or a list containing database or table "
            'ids like [["database", 1], ["table", 1]].'
        ),
        "invalid_instance_type": "The instance type can only be a database or table.",
        "invalid_table_id": "The table id {instance_id} is not valid.",
        "invalid_database_id": "The database id {instance_id} is not valid.",
    }
    valid_types = ["create", "read", "update", "delete"]

    def __init__(self, **kwargs):
        kwargs["source"] = "*"
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        """
        Validates if the provided data structure is correct and replaces the database
        and table references with selected instances. The following data format is
        expected:

        {
            "create": True,
            "read": [['database', ID], ['table', ID]],
            "update": False,
            "delete": True
        }

        which will be converted to:

        {
            "create": True,
            "read": [Database(ID), Table(ID)],
            "update": False,
            "delete": True
        }

        :param data: The non validated permissions containing references to the
            database and tables.
        :type data: dict
        :return: The validated permissions with objects instead of references.
        :rtype: dict
        """

        tables = {}
        databases = {}

        if not isinstance(data, dict) or len(data) != len(self.valid_types):
            self.fail("invalid_key")

        for key, value in data.items():
            if key not in self.valid_types:
                self.fail("invalid_key")

            if not isinstance(value, bool) and not isinstance(value, list):
                self.fail("invalid_value")

            if isinstance(value, list):
                for instance in value:
                    if (
                        not isinstance(instance, list)
                        or not len(instance) == 2
                        or not isinstance(instance[0], str)
                        or not isinstance(instance[1], int)
                    ):
                        self.fail("invalid_value")

                    instance_type, instance_id = instance
                    if instance_type == "database":
                        databases[instance_id] = None
                    elif instance_type == "table":
                        tables[instance_id] = None
                    else:
                        self.fail("invalid_instance_type")

        if len(tables) > 0:
            tables = {
                table.id: table
                for table in Table.objects.filter(id__in=tables.keys()).select_related(
                    "database"
                )
            }

        if len(databases) > 0:
            databases = {
                database.id: database
                for database in Database.objects.filter(id__in=databases.keys())
            }

        for key, value in data.items():
            if isinstance(value, list):
                for index, (instance_type, instance_id) in enumerate(value):
                    if instance_type == "database":
                        if instance_id not in databases:
                            self.fail("invalid_database_id", instance_id=instance_id)
                        data[key][index] = databases[instance_id]
                    elif instance_type == "table":
                        if instance_id not in tables:
                            self.fail("invalid_table_id", instance_id=instance_id)
                        data[key][index] = tables[instance_id]

        return {
            "create": data["create"],
            "read": data["read"],
            "update": data["update"],
            "delete": data["delete"],
        }

    def to_representation(self, value):
        """
        If the provided value is a Token instance we need to fetch the permissions from
        the database and format them the correct way.

        If the provided value is a dict it means the permissions have already been
        provided and validated once, so we can just return that value. The variant is
        used when we want to validate the input.

        :param value: The prepared value that needs to be serialized.
        :type value: Token or dict
        :return: A dict containing the create, read, update and delete permissions
        :rtype: dict
        """

        if isinstance(value, Token):
            permissions = {
                "create": False,
                "read": False,
                "update": False,
                "delete": False,
            }

            for permission in value.tokenpermission_set.all():
                if permissions[permission.type] is True:
                    continue

                if permission.database_id is None and permission.table_id is None:
                    permissions[permission.type] = True
                else:
                    if not isinstance(permissions[permission.type], list):
                        permissions[permission.type] = []
                    if permission.database_id is not None:
                        permissions[permission.type].append(
                            ("database", permission.database_id)
                        )
                    elif permission.table_id is not None:
                        permissions[permission.type].append(
                            ("table", permission.table_id)
                        )

            return permissions
        else:
            permissions = {}
            for type_name in self.valid_types:
                if type_name not in value:
                    return None
                permissions[type_name] = value[type_name]
            return permissions

class ChoiceElementType(FormElementTypeMixin, ElementType):
    type = "choice"
    model_class = ChoiceElement
    allowed_fields = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    request_serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    simple_formula_fields = [
        "label",
        "default_value",
        "placeholder",
        "formula_value",
        "formula_name",
    ]

    class SerializedDict(ElementDict):
        label: BaserowFormula
        required: bool
        placeholder: BaserowFormula
        default_value: BaserowFormula
        options: List
        multiple: bool
        show_as_dropdown: bool
        option_type: str
        formula_value: BaserowFormula
        formula_name: BaserowFormula

    @property
    def serializer_field_overrides(self):
        from baserow.contrib.builder.api.theme.serializers import (
            DynamicConfigBlockSerializer,
        )
        from baserow.contrib.builder.theme.theme_config_block_types import (
            InputThemeConfigBlockType,
        )
        from baserow.core.formula.serializers import FormulaSerializerField

        overrides = {
            "label": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("label").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "default_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("default_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "required": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("required").help_text,
                default=False,
                required=False,
            ),
            "placeholder": serializers.CharField(
                help_text=ChoiceElement._meta.get_field("placeholder").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "options": ChoiceOptionSerializer(
                source="choiceelementoption_set", many=True, required=False
            ),
            "multiple": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("multiple").help_text,
                default=False,
                required=False,
            ),
            "show_as_dropdown": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("show_as_dropdown").help_text,
                default=True,
                required=False,
            ),
            "option_type": serializers.ChoiceField(
                choices=ChoiceElement.OPTION_TYPE.choices,
                help_text=ChoiceElement._meta.get_field("option_type").help_text,
                required=False,
                default=ChoiceElement.OPTION_TYPE.MANUAL,
            ),
            "formula_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "formula_name": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_name").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "styles": DynamicConfigBlockSerializer(
                required=False,
                property_name="input",
                theme_config_block_type_name=InputThemeConfigBlockType.type,
                serializer_kwargs={"required": False},
            ),
        }

        return overrides

    @property
    def request_serializer_field_overrides(self):
        return {
            **self.serializer_field_overrides,
            "options": ChoiceOptionSerializer(many=True, required=False),
        }

    def serialize_property(
        self,
        element: ChoiceElement,
        prop_name: str,
        files_zip=None,
        storage=None,
        cache=None,
    ):
        if prop_name == "options":
            return [
                self.serialize_option(option)
                for option in element.choiceelementoption_set.all()
            ]

        return super().serialize_property(
            element, prop_name, files_zip=files_zip, storage=storage, cache=cache
        )

    def import_serialized(
        self,
        parent: Any,
        serialized_values: Dict[str, Any],
        id_mapping: Dict[str, Dict[int, int]],
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        choice_element = super().import_serialized(
            parent,
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

        options = []
        for option in serialized_values.get("options", []):
            option["choice_id"] = choice_element.id
            option_deserialized = self.deserialize_option(option)
            options.append(option_deserialized)

        ChoiceElementOption.objects.bulk_create(options)

        return choice_element

    def create_instance_from_serialized(
        self,
        serialized_values: Dict[str, Any],
        id_mapping,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        serialized_values.pop("options", None)
        return super().create_instance_from_serialized(
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def serialize_option(self, option: ChoiceElementOption) -> Dict:
        return {
            "value": option.value,
            "name": option.name,
            "choice_id": option.choice_id,
        }

    def deserialize_option(self, value: Dict):
        return ChoiceElementOption(**value)

    def get_pytest_params(self, pytest_data_fixture) -> Dict[str, Any]:
        return {
            "label": "'test'",
            "default_value": "'option 1'",
            "required": False,
            "placeholder": "'some placeholder'",
            "multiple": False,
            "show_as_dropdown": True,
            "option_type": ChoiceElement.OPTION_TYPE.MANUAL,
            "formula_value": "",
            "formula_name": "",
        }

    def after_create(self, instance: ChoiceElement, values: Dict):
        options = values.get("options", [])

        ChoiceElementOption.objects.bulk_create(
            [ChoiceElementOption(choice=instance, **option) for option in options]
        )

    def after_update(
        self, instance: ChoiceElement, values: Dict, changes: Dict[str, Tuple]
    ):
        options = values.get("options", None)

        if options is not None:
            ChoiceElementOption.objects.filter(choice=instance).delete()
            ChoiceElementOption.objects.bulk_create(
                [ChoiceElementOption(choice=instance, **option) for option in options]
            )

    def is_valid(
        self,
        element: ChoiceElement,
        value: Union[List, str],
        dispatch_context: DispatchContext,
    ) -> str:
        """
        Responsible for validating `ChoiceElement` form data. We handle
        this validation a little differently to ensure that if someone creates
        an option with a blank value, it's considered valid.

        :param element: The choice element.
        :param value: The choice value we want to validate.
        :return: The value if it is valid for this element.
        """

        options_tuple = set(
            element.choiceelementoption_set.values_list("value", "name")
        )
        options = [
            value if value is not None else name for (value, name) in options_tuple
        ]

        if element.option_type == ChoiceElement.OPTION_TYPE.FORMULAS:
            options = ensure_array(
                resolve_formula(
                    element.formula_value,
                    formula_runtime_function_registry,
                    dispatch_context,
                )
            )
            options = [ensure_string_or_integer(option) for option in options]

        if element.multiple:
            try:
                value = ensure_array(value)
            except ValidationError as exc:
                raise FormDataProviderChunkInvalidException(
                    "The value must be an array or convertible to an array."
                ) from exc

            if not value:
                if element.required:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            else:
                for v in value:
                    if v not in options:
                        raise FormDataProviderChunkInvalidException(
                            f"{value} is not a valid option."
                        )
        else:
            if not value:
                if element.required and value not in options:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            elif value not in options:
                raise FormDataProviderChunkInvalidException(
                    f"{value} is not a valid option."
                )

        return value


#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_urlopen_rule-urllib-urlopen
opener.retrieve(url_from_user)
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

pop('/bin/gcc --version', shell=True)
# ruleid: python_exec_rule-subprocess-shell-TRUE
Popen('/bin/gcc --version', shell=True)

subprocess.Popen('/bin/gcc --version', shell=True)
subprocess.Popen(['/bin/gcc', '--version'], shell=False)
subprocess.Popen(['/bin/gcc', '--version'])

subprocess.call(["/bin/ls",
                 "-l"
                 ])
subprocess.call('/bin/ls -l', shell=True)

subprocess.check_call(['/bin/ls', '-l'], shell=False)
subprocess.check_call('/bin/ls -l', shell=True)

subprocess.check_output(['/bin/ls', '-l'])
subprocess.check_output('/bin/ls -l', shell=True)

subprocess.run(['/bin/ls', '-l'])
subprocess.run('/bin/ls -l', shell=True)

subprocess.Popen('/bin/ls *', shell=True)
subprocess.Popen('/bin/ls %s' % ('something',), shell=True)
subprocess.Popen('/bin/ls {}'.format('something'), shell=True)

command = "/bin/ls" + unknown_function()
subprocess.Popen(command, shell=True)

subprocess.Popen('/bin/ls && cat /etc/passwd', shell=True)

command = 'pwd'
subprocess.call(command, shell='True')
subprocess.call(command, shell='False')
subprocess.call(command, shell='None')
subprocess.call(command, shell=1)

subprocess.call(command, shell=Popen())
subprocess.call(command, shell=[True])
subprocess.call(command, shell={'IS': 'True'})
subprocess.call(command, shell=command)

subprocess.call(command, shell=False)
subprocess.call(command, shell=0)
subprocess.call(command, shell=[])
subprocess.call(command, shell={})
subprocess.call(command, shell=None)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/weak_cryptographic_key_sizes.py
# hash:  8eee173

from cryptography.hazmat import backends
from cryptography.hazmat.primitives.asymmetric import dsa
from cryptography.hazmat.primitives.asymmetric import rsa
from Crypto.PublicKey import DSA as pycrypto_dsa
from Crypto.PublicKey import RSA as pycrypto_rsa
from Cryptodome.PublicKey import DSA as pycryptodomex_dsa
from Cryptodome.PublicKey import RSA as pycryptodomex_rsa


# Correct
dsa.generate_private_key(key_size=2048,
                         backend=backends.default_backend())
rsa.generate_private_key(public_exponent=65537,
                         key_size=2048,
                         backend=backends.default_backend())
pycrypto_dsa.generate(bits=2048)
pycrypto_rsa.generate(bits=2048)
pycryptodomex_dsa.generate(bits=2048)
pycryptodomex_rsa.generate(bits=2048)

# Also correct: without keyword args
dsa.generate_private_key(4096,
                         backends.default_backend())
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
pycrypto_dsa.generate(4096)
pycrypto_rsa.generate(4096)
pycryptodomex_dsa.generate(4096)
pycryptodomex_rsa.generate(4096)

# Incorrect: weak key sizes
# ruleid: python_crypto_rule-crypto-encrypt-dsa-rsa
dsa.generate_private_key(key_size=1024,
                         backend=backends.default_backend())
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Also incorrect: without keyword args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Don't crash when the size is variable
rsa.generate_private_key(public_exponent=65537,
                         key_size=some_key_size,
                         backend=backends.default_backend())

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

import hashlib

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_crypto_rule-hash-md5
print(hashlib.md5("1"))


import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
    # ruleid: python_flask_rule-path-traversal-open
    return open(param, 'r').read()


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
    # ruleid: python_flask_rule-path-traversal-open
    return open("{}.csv".format(param)).read()


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data8 = UsmUserData(username, authKey=None, authProtocol=usmNoAuthProtocol, privKey=None, privProtocol=usmNoPrivProtocol)
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data6 = UsmUserData(username)
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/pycrypto.py
# hash:  8eee173

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_crypto_rule-import-pycrypto
import Crypto.Hash
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

from . import CryptoMaterialsCacheEntry

def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()


def test_pycrypto():
    key = b'Sixteen byte key'
    iv = Random.new().read(AES.block_size)
    cipher = pycrypto_arc2.new(key, AES.MODE_CFB, iv)
    factory = CryptoMaterialsCacheEntry()

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("rsync -avz secret?.txt destination/", shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
os.system("cat secret?.txt")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/os-spawn.py
# hash:  8eee173

import os

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_exec_rule-start-process-with-no-shell
os.execl(path, arg0, arg1)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/jinja2_templating.py
# hash:  8eee173

import jinja2
from jinja2 import Environment, select_autoescape
templateLoader = jinja2.FileSystemLoader( searchpath="/" )
something = ''

Environment(loader=templateLoader, load=templateLoader, autoescape=True)
templateEnv = jinja2.Environment(autoescape=True,
        loader=templateLoader )
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_escaping_rule-jinja2-autoescape-false
Environment(loader=templateLoader,
            load=templateLoader,
            autoescape=False)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

Environment(loader=templateLoader, autoescape=select_autoescape())

Environment(loader=templateLoader,
            autoescape=select_autoescape(['html', 'htm', 'xml']))


def fake_func():
    return 'foobar'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/os-spawn.py
# hash:  8eee173

import os

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-start-process-with-no-shell
os.startfile(file, args, env)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/ciphers.py
# hash:  8eee173

from Crypto.Cipher import ARC2 as pycrypto_arc2
from Cryptodome.Cipher import ARC2 as pycryptodomex_arc2
from Crypto import Random

key = b'Sixteen byte key'
iv = Random.new().read(pycrypto_arc2.block_size)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
msg = iv + cipher.encrypt(b'Attack at dawn')
# ruleid: python_crypto_rule-crypto-cipher-rc2
cipher = pycryptodomex_arc2.new(key, pycryptodomex_arc2.MODE_CFB, iv)
msg = iv + cipher.encrypt(b'Attack at dawn')


# License: MIT (c) GitLab Inc.
# source: https://bandit.readthedocs.io/en/latest/_modules/bandit/plugins/snmp_security_check.html#snmp_insecure_version_check

from pysnmp.hlapi import CommunityData

# SHOULD FAIL
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_snmp_rule-insecure-snmp-version
b = CommunityData('public', mpModel=1)

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# https://github.com/semgrep/semgrep-rules/blob/release/python/lang/security/audit/network/http-not-https-connection.py

import urllib3 as ur3
import urllib3 

# ruleid: python_crypto_rule-HTTPConnectionPool
pool = ur3.HTTPConnectionPool("example.com")
print(pool)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print(pool2)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print(pool3)

# ok: python_crypto_rule-HTTPConnectionPool
spool = ur3.connectionpool.HTTPSConnectionPool("example.com")
print(spool)

# ok: python_crypto_rule-HTTPConnectionPool
spool = urllib3.connectionpool.HTTPSConnectionPool("example.com")
print(spool)
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_lxml.py
# hash:  8eee173

import lxml.etree
import lxml
from lxml import etree
from defusedxml.lxml import fromstring
from defuxedxml import lxml as potato

xmlString = "<note>\n<to>Tove</to>\n<from>Jani</from>\n<heading>Reminder</heading>\n<body>Don't forget me this weekend!</body>\n</note>"
root = lxml.etree.fromstring(xmlString)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_xml_rule-etree
lxml.etree.check_docinfo()
root = fromstring(xmlString)

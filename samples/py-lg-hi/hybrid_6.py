class AllApplicationsView(APIView):
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        tags=["Applications"],
        operation_id="list_all_applications",
        description=(
            "Lists all the applications that the authorized user has access to. The "
            "properties that belong to the application can differ per type. An "
            "application always belongs to a single workspace. All the applications of the "
            "workspaces that the user has access to are going to be listed here."
        ),
        responses={
            200: PolymorphicApplicationResponseSerializer(many=True),
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
        },
    )
    @map_exceptions({UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP})
    def get(self, request):
        """
        Responds with a list of serialized applications that belong to the user. If a
        workspace id is provided only the applications of that workspace are going to be
        returned.
        """

        workspaces = Workspace.objects.filter(users=request.user)

        # Compute list of readable application ids
        applications_ids = []
        for workspace in workspaces:
            applications = Application.objects.filter(
                workspace=workspace, workspace__trashed=False
            )
            applications = CoreHandler().filter_queryset(
                request.user,
                ListApplicationsWorkspaceOperationType.type,
                applications,
                workspace=workspace,
            )
            applications_ids += applications.values_list("id", flat=True)

        # Then filter with these ids
        applications = specific_iterator(
            Application.objects.select_related("content_type", "workspace")
            .prefetch_related("workspace__template_set")
            .filter(id__in=applications_ids)
            .order_by("workspace_id", "order"),
            per_content_type_queryset_hook=(
                lambda model, queryset: application_type_registry.get_by_model(
                    model
                ).enhance_queryset(queryset)
            ),
        )

        data = [
            PolymorphicApplicationResponseSerializer(
                application, context={"request": request}
            ).data
            for application in applications
        ]

        return Response(data)

class PostgresqlLenientDatabaseSchemaEditor:
    """
    Class changes the behavior of the postgres database schema editor slightly. Normally
    when the field type is altered and one of the data columns doesn't have a value that
    can be casted to the new type, it fails. This class introduces the possibility to
    run a custom function, like REGEXP_REPLACE, to convert the data to the correct
    format. If the casting still fails the value will be set to null.
    """

    sql_alter_column_type = (
        "ALTER COLUMN %(column)s TYPE %(type)s%(collation)s "
        "USING pg_temp.try_cast(%(column)s::text)"
    )

    def __init__(
        self,
        *args,
        alter_column_prepare_old_value="",
        alter_column_prepare_new_value="",
        force_alter_column=False,
        **kwargs,
    ):
        self.alter_column_prepare_old_value = alter_column_prepare_old_value
        self.alter_column_prepare_new_value = alter_column_prepare_new_value
        self.force_alter_column = force_alter_column
        super().__init__(*args, **kwargs)

    def _alter_field(
        self,
        model,
        old_field,
        new_field,
        old_type,
        new_type,
        old_db_params,
        new_db_params,
        strict=False,
    ):
        if self.force_alter_column:
            old_type = f"{old_type}_forced"

        if old_type != new_type:
            variables = {}
            if isinstance(self.alter_column_prepare_old_value, tuple):
                alter_column_prepare_old_value, v = self.alter_column_prepare_old_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_old_value = self.alter_column_prepare_old_value

            if isinstance(self.alter_column_prepare_new_value, tuple):
                alter_column_prepare_new_value, v = self.alter_column_prepare_new_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_new_value = self.alter_column_prepare_new_value

            quoted_column_name = self.quote_name(new_field.column)
            for key, value in variables.items():
                variables[key] = value.replace("$FUNCTION$", "")
            self.execute(sql_drop_try_cast)
            self.execute(
                sql_create_try_cast
                % {
                    "column": quoted_column_name,
                    "type": new_type,
                    "alter_column_prepare_old_value": alter_column_prepare_old_value,
                    "alter_column_prepare_new_value": alter_column_prepare_new_value,
                },
                variables,
            )

        return super()._alter_field(
            model,
            old_field,
            new_field,
            old_type,
            new_type,
            old_db_params,
            new_db_params,
            strict,
        )

    def _alter_column_type_sql(
        self, model, old_field, new_field, new_type, old_collation, new_collation
    ):
        # Cast when data type changed.
        # Make ALTER TYPE with SERIAL make sense.
        table = strip_quotes(model._meta.db_table)
        serial_fields_map = {
            "bigserial": "bigint",
            "serial": "integer",
            "smallserial": "smallint",
        }
        if collate_sql := self._collate_sql(
            new_collation, old_collation, model._meta.db_table
        ):
            collate_sql = f" {collate_sql}"
        else:
            collate_sql = ""
        if new_type.lower() in serial_fields_map:
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            return (
                (
                    self.sql_alter_column_type
                    % {
                        "column": self.quote_name(column),
                        "type": serial_fields_map[new_type.lower()],
                        "collation": collate_sql,
                    },
                    [],
                ),
                [
                    (
                        self.sql_delete_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_create_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_alter_column
                        % {
                            "table": self.quote_name(table),
                            "changes": self.sql_alter_column_default
                            % {
                                "column": self.quote_name(column),
                                "default": "nextval('%s')"
                                % self.quote_name(sequence_name),
                            },
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_max
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_owner
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                ],
            )
        elif (
            old_field.db_parameters(connection=self.connection)["type"]
            in serial_fields_map
        ):
            # Drop the sequence if migrating away from AutoField.
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            fragment, _ = BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )
            return fragment, [
                (
                    self.sql_delete_sequence
                    % {
                        "sequence": self.quote_name(sequence_name),
                    },
                    [],
                ),
            ]
        else:
            return BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )

    def _field_should_be_altered(self, old_field, new_field):
        return self.force_alter_column or super()._field_should_be_altered(
            old_field, new_field
        )

class ImageElementWorkspaceStorageUsageItem(WorkspaceStorageUsageItemType):
    type = "image_element"

    def calculate_storage_usage(self, workspace_id: int) -> UsageInMB:
        image_elements = ImageElement.objects.filter(
            page__builder__workspace_id=workspace_id,
            page__trashed=False,
            page__builder__trashed=False,
        )

        usage_in_mb = (
            UserFile.objects.filter(Q(id__in=image_elements.values("image_file")))
            .values("size")
            .aggregate(sum=Coalesce(Sum("size") / USAGE_UNIT_MB, 0))["sum"]
        )

        return usage_in_mb

class CollectionElementTypeMixin:
    is_collection_element = True

    # Three properties which define whether this collection element
    # is allowed to be publicly sortable, filterable and searchable
    # by page visitors. Can be overridden by subclasses to influence
    # whether page designers can flag collection elements and their
    # properties as sortable, filterable and searchable.
    is_publicly_sortable = True
    is_publicly_filterable = True
    is_publicly_searchable = True

    allowed_fields = [
        "data_source",
        "data_source_id",
        "items_per_page",
        "schema_property",
        "button_load_more_label",
    ]
    serializer_field_names = [
        "schema_property",
        "data_source_id",
        "items_per_page",
        "button_load_more_label",
        "property_options",
        "is_publicly_sortable",
        "is_publicly_filterable",
        "is_publicly_searchable",
    ]

    class SerializedDict(ElementDict):
        data_source_id: int
        items_per_page: int
        button_load_more_label: str
        schema_property: str
        property_options: List[Dict]

    def enhance_queryset(self, queryset):
        return queryset.prefetch_related("property_options")

    def after_update(
        self, instance: CollectionElementSubClass, values, changes: Dict[str, Tuple]
    ):
        """
        After the element has been updated we need to update the property options.

        :param instance: The instance of the element that has been updated.
        :param values: The values that have been updated.
        :param changes: A dictionary containing all changes which were made to the
            collection element prior to `after_update` being called.
        :return: None
        """

        # Following a DataSource change, from one DataSource to another, we drop all
        # property options. This is due to the fact that the `schema_property` in the
        # property options are specific to that data source's schema.
        data_source_changed = "data_source" in changes

        if "property_options" in values or data_source_changed:
            instance.property_options.all().delete()
            try:
                CollectionElementPropertyOptions.objects.bulk_create(
                    [
                        CollectionElementPropertyOptions(
                            **option,
                            element=instance,
                        )
                        for option in values.get("property_options", [])
                    ]
                )
            except IntegrityError as e:
                if "unique constraint" in e.args[0]:
                    raise CollectionElementPropertyOptionsNotUnique()
                raise e

    @property
    def serializer_field_overrides(self):
        from baserow.core.formula.serializers import FormulaSerializerField

        return {
            "is_publicly_sortable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_sortable,
                help_text="Whether this collection element is publicly sortable.",
            ),
            "is_publicly_filterable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_filterable,
                help_text="Whether this collection element is publicly filterable.",
            ),
            "is_publicly_searchable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_searchable,
                help_text="Whether this collection element is publicly searchable.",
            ),
            "data_source_id": serializers.IntegerField(
                allow_null=True,
                default=None,
                help_text=CollectionElement._meta.get_field("data_source").help_text,
                required=False,
            ),
            "schema_property": serializers.CharField(
                allow_null=True,
                default=None,
                help_text=CollectionElement._meta.get_field(
                    "schema_property"
                ).help_text,
                required=False,
            ),
            "items_per_page": serializers.IntegerField(
                default=20,
                help_text=CollectionElement._meta.get_field("items_per_page").help_text,
                required=False,
            ),
            "button_load_more_label": FormulaSerializerField(
                help_text=CollectionElement._meta.get_field(
                    "button_load_more_label"
                ).help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "property_options": CollectionElementPropertyOptionsSerializer(
                many=True,
                required=False,
                help_text="The schema property options that can be set for the collection element.",
            ),
        }

    def prepare_value_for_db(
        self, values: Dict, instance: Optional[CollectionElementSubClass] = None
    ):
        if "data_source_id" in values:
            data_source_id = values.pop("data_source_id")
            if data_source_id is not None:
                schema_property = values.get("schema_property", None)
                data_source = DataSourceHandler().get_data_source(data_source_id)
                if data_source.service:
                    service_type = data_source.service.specific.get_type()
                    if service_type.returns_list and schema_property:
                        raise DRFValidationError(
                            "Data sources which return multiple rows cannot be "
                            "used in conjunction with the schema property."
                        )
                else:
                    raise DRFValidationError(
                        f"Data source {data_source_id} is partially "
                        "configured and not ready for use."
                    )

                if instance:
                    element_page = instance.page
                else:
                    element_page = values["page"]

                # The data source must belong to the same element page or the shared
                # page.
                if data_source.page_id not in [
                    element_page.id,
                    element_page.builder.shared_page.id,
                ]:
                    raise RequestBodyValidationException(
                        {
                            "data_source_id": [
                                {
                                    "detail": "The provided data source is not "
                                    "available for this element.",
                                    "code": "invalid_data_source",
                                }
                            ]
                        }
                    )
                values["data_source"] = data_source
            else:
                values["data_source"] = None

        return super().prepare_value_for_db(values, instance)

    def serialize_property(
        self,
        element: CollectionElementSubClass,
        prop_name: str,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ):
        """
        You can customize the behavior of the serialization of a property with this
        hook.
        """

        if prop_name == "property_options":
            return [
                {
                    "schema_property": po.schema_property,
                    "filterable": po.filterable,
                    "sortable": po.sortable,
                    "searchable": po.searchable,
                }
                for po in element.property_options.all()
            ]

        return super().serialize_property(
            element,
            prop_name,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def deserialize_property(
        self,
        prop_name: str,
        value: Any,
        id_mapping: Dict[str, Any],
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> Any:
        if prop_name == "data_source_id" and value:
            return id_mapping["builder_data_sources"][value]

        if prop_name == "property_options" and "database_fields" in id_mapping:
            property_options = []
            for po in value:
                field_id = get_field_id_from_field_key(po["schema_property"])
                if field_id is None:
                    # If we can't translate the `schema_property` into a Field ID, then
                    # it's not a `Field` db_column value. For example this can happen
                    # if someone chooses to have a `id` property option.
                    property_options.append(po)
                    continue
                new_field_id = id_mapping["database_fields"][field_id]
                property_options.append(
                    {**po, "schema_property": f"field_{new_field_id}"}
                )
            return property_options

        return super().deserialize_property(
            prop_name,
            value,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def import_context_addition(self, instance: CollectionElement) -> Dict[str, int]:
        """
        Given a collection element, adds the data_source_id to the import context.

        The data_source_id is not store in some formulas (current_record ones) so
        we need the generate this import context for all formulas of this element.
        """

        if instance.data_source_id:
            results = {"data_source_id": instance.data_source_id}
        else:
            results = {}

        if instance.schema_property is not None:
            results["schema_property"] = instance.schema_property

        return results

    def create_instance_from_serialized(
        self,
        serialized_values: Dict[str, Any],
        id_mapping,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> CollectionElementSubClass:
        """
        Responsible for creating the property options from the serialized values.

        :param serialized_values: The serialized values of the element.
        :param id_mapping: A dictionary containing the mapping of the old and new ids.
        :param files_zip: The zip file containing the files that can be used.
        :param storage: The storage that can be used to store files.
        :param cache: A dictionary that can be used to cache data.
        :param kwargs: Additional keyword arguments.
        :return: The created instance.
        """

        property_options = serialized_values.pop("property_options", [])

        instance = super().create_instance_from_serialized(
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

        # Create property options
        options = [
            CollectionElementPropertyOptions(**po, element=instance)
            for po in property_options
        ]
        CollectionElementPropertyOptions.objects.bulk_create(options)

        instance.property_options.add(*options)

        return instance

    def extract_formula_properties(
        self, instance: Element, **kwargs
    ) -> Dict[int, List[BaserowFormula]]:
        """
        Some collection elements (e.g. Repeat Element) may have a nested
        collection element which uses a schema_property. This property points
        to a field name that is connected to the parent collection element's
        data source.

        This method is overridden to ensure that any schema_property is also
        included in the list of field names used by the element.
        """

        properties = super().extract_formula_properties(instance, **kwargs)

        if schema_property := instance.schema_property:
            # if we have a data_source_id in the context from a parent or from the
            # current instance
            data_source_id = instance.data_source_id or kwargs.get(
                "data_source_id", None
            )
            if data_source_id:
                data_source = DataSourceHandler().get_data_source(data_source_id)
                properties[data_source.service_id] = [schema_property]

        return properties


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("chown root:root *", shell=True, stdin=subprocess.PIPE)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/yaml_load.py
# hash:  8eee173

import json
import yaml

def test_yaml_load():
    ystr = yaml.dump({'a': 1, 'b': 2, 'c': 3})
    # ruleid: python_deserialization_rule-yaml-load
    y = yaml.load(ystr, Loader=yaml.FullLoader)
    yaml.dump(y)
    try:
        yaml.load(ystr, Loader=yaml.CSafeLoader)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    except AttributeError:
        # CSafeLoader only exists if you build yaml with LibYAML
        y = yaml.load(ystr, Loader=yaml.SafeLoader)

def test_json_load():
    # no issue should be found
    j = json.load("{}")

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/jinja2_templating.py
# hash:  8eee173

import jinja2
from jinja2 import Environment, select_autoescape
templateLoader = jinja2.FileSystemLoader( searchpath="/" )
something = ''

Environment(loader=templateLoader, load=templateLoader, autoescape=True)
templateEnv = jinja2.Environment(autoescape=True,
        loader=templateLoader )
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

Environment(loader=templateLoader, autoescape=select_autoescape())

Environment(loader=templateLoader,
            autoescape=select_autoescape(['html', 'htm', 'xml']))


def fake_func():
    return 'foobar'
# ruleid: python_escaping_rule-jinja2-autoescape-false
Environment(loader=templateLoader, autoescape=fake_func())

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

from Crypto.Hash import SHA as pycrypto_sha
from Cryptodome.Hash import SHA as pycryptodomex_sha

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_crypto_rule-crypto-hash-sha1
pycryptodomex_sha.new()

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/marshal_deserialize.py
# hash:  8eee173

import marshal
import tempfile

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

file_obj = tempfile.TemporaryFile()
# ruleid: python_deserialization_rule-marshal
marshal.dump(range(5), file_obj)
file_obj.seek(0)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
file_obj.close()

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_etree_celementtree.py
# hash:  8eee173

import xml.etree.cElementTree as badET
import defusedxml.cElementTree as goodET

xmlString = "<note>\n<to>Tove</to>\n<from>Jani</from>\n<heading>Reminder</heading>\n<body>Don't forget me this weekend!</body>\n</note>"

# unsafe
tree = badET.fromstring(xmlString)
print(tree)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_xml_rule-celement
badET.iterparse('filethatdoesntexist.xml')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# safe
tree = goodET.fromstring(xmlString)
print(tree)
# ok: python_xml_rule-celement
goodET.parse('filethatdoesntexist.xml')
# ok: python_xml_rule-celement
goodET.iterparse('filethatdoesntexist.xml')
# ok: python_xml_rule-celement
a = goodET.XMLParser()


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id = '" + bar + "'" + " AND somethingElse = '" + foo + "'")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id = '" + bar + "'" + " AND somethingElse <= " + foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_lxml.py
# hash:  8eee173

import lxml.etree
import lxml
from lxml import etree
from defusedxml.lxml import fromstring
from defuxedxml import lxml as potato

xmlString = "<note>\n<to>Tove</to>\n<from>Jani</from>\n<heading>Reminder</heading>\n<body>Don't forget me this weekend!</body>\n</note>"
root = lxml.etree.fromstring(xmlString)
# ruleid: python_xml_rule-etree
lxml.etree.RestrictedElement()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
root = fromstring(xmlString)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/ciphers.py
# hash:  8eee173

from Crypto.Cipher import ARC2 as pycrypto_arc2
from Cryptodome.Cipher import ARC2 as pycryptodomex_arc2
from Crypto import Random

key = b'Sixteen byte key'
iv = Random.new().read(pycrypto_arc2.block_size)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
msg = iv + cipher.encrypt(b'Attack at dawn')
# ruleid: python_crypto_rule-crypto-cipher-rc2
cipher = pycryptodomex_arc2.new(key, pycryptodomex_arc2.MODE_CFB, iv)
msg = iv + cipher.encrypt(b'Attack at dawn')


import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
    #ruleid: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(f"""SELECT * FROM main.user 
    WHERE firstname LIKE '%{name}%';""")
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/requests-ssl-verify-disabled.py
# hash:  8eee173

import requests

requests.get('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', verify=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', verify=True)
# ruleid: python_ssl_rule-req-no-certvalid
requests.head('https://gmail.com', verify=False)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("rsync -avz secret?.txt destination/", shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

from flask import Flask, request

app = Flask(__name__)
# Literal strings are ok
com = '''
import os
os.system('ls -l')
'''


@app.route('/exec', methods=['POST'])
def exec_route():
    user_input = request.form['input']

    # ruleid: python_exec_rule-exec-used
    exec(f"{user_input}")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route('/safe', methods=['GET'])
def exec_safe_route():
    text = 'exec is evil'
    com = f"print('{text}')"

    # rule ok:
    exec(com)

    # rule ok:
    exec(com)

    # rule ok:
    exec("2 + 3")

    # rule ok:
    exec("print('hello world!')")

    return "no response"


if __name__ == '__main__':
    app.run(debug=True)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/django_sql_injection_extra.py
# hash:  8eee173

from django.contrib.auth.models import User

User.objects.filter(username='admin').extra(
    select={'test': 'secure'},
    where=['secure'],
    tables=['secure']
)
User.objects.filter(username='admin').extra({'test': 'secure'})
User.objects.filter(username='admin').extra(select={'test': 'secure'})
User.objects.filter(username='admin').extra(where=['secure'])

User.objects.filter(username='admin').extra(dict(could_be='insecure'))
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
query = '"username") AS "username", * FROM "auth_user" WHERE 1=1 OR "username"=? --'
User.objects.filter(username='admin').extra(select={'test': query})
User.objects.filter(username='admin').extra(select={'test': '%secure' % 'nos'})
User.objects.filter(username='admin').extra(select={'test': '{}secure'.format('nos')})

where_var = ['1=1) OR 1=1 AND (1=1']
User.objects.filter(username='admin').extra(where=where_var)
# ruleid: python_django_rule-django-extra-used
User.objects.filter(username='admin').extra(where=userinput)
where_str = '1=1) OR 1=1 AND (1=1'
User.objects.filter(username='admin').extra(where=[where_str])
User.objects.filter(username='admin').extra(where=['%secure' % 'nos'])
User.objects.filter(username='admin').extra(where=['{}secure'.format('no')])

tables_var = ['django_content_type" WHERE "auth_user"."username"="admin']
User.objects.all().extra(tables=tables_var).distinct()
tables_str = 'django_content_type" WHERE "auth_user"."username"="admin'
User.objects.all().extra(tables=[tables_str]).distinct()

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

from Crypto.Hash import MD5 as pycrypto_md5
from Cryptodome.Hash import MD5 as pycryptodomex_md5

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_crypto_rule-crypto-hash-md5
pycryptodomex_md5.new()

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/weak_cryptographic_key_sizes.py
# hash:  8eee173

from cryptography.hazmat import backends
from cryptography.hazmat.primitives.asymmetric import dsa
from cryptography.hazmat.primitives.asymmetric import rsa
from Crypto.PublicKey import DSA as pycrypto_dsa
from Crypto.PublicKey import RSA as pycrypto_rsa
from Cryptodome.PublicKey import DSA as pycryptodomex_dsa
from Cryptodome.PublicKey import RSA as pycryptodomex_rsa


# Correct
dsa.generate_private_key(key_size=2048,
                         backend=backends.default_backend())
rsa.generate_private_key(public_exponent=65537,
                         key_size=2048,
                         backend=backends.default_backend())
pycrypto_dsa.generate(bits=2048)
pycrypto_rsa.generate(bits=2048)
pycryptodomex_dsa.generate(bits=2048)
pycryptodomex_rsa.generate(bits=2048)

# Also correct: without keyword args
dsa.generate_private_key(4096,
                         backends.default_backend())
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
pycrypto_dsa.generate(4096)
pycrypto_rsa.generate(4096)
pycryptodomex_dsa.generate(4096)
pycryptodomex_rsa.generate(4096)

# Incorrect: weak key sizes
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_crypto_rule-crypto-encrypt-dsa-rsa
pycrypto_dsa.generate(bits=1024)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Also incorrect: without keyword args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Don't crash when the size is variable
rsa.generate_private_key(public_exponent=65537,
                         key_size=some_key_size,
                         backend=backends.default_backend())

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# safe
pop('/bin/gcc --version', shell=True)
Popen('/bin/gcc --version', shell=True)

subprocess.Popen('/bin/gcc --version', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.call('/bin/ls -l', shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.check_call('/bin/ls -l', shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.check_output('/bin/ls -l', shell=True)

# ruleid: python_exec_rule-subprocess-call
subprocess.run(['/bin/ls', '-l'])
subprocess.run('/bin/ls -l', shell=True)

subprocess.Popen('/bin/ls *', shell=True)
subprocess.Popen('/bin/ls %s' % ('something'), shell=True)
subprocess.Popen('/bin/ls {}'.format('something'), shell=True)

command = "/bin/ls" + unknown_function()
subprocess.Popen(command, shell=True)

subprocess.Popen('/bin/ls && cat /etc/passwd', shell=True)

constant_command = 'pwd'
subprocess.call(constant_command, shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# unsafe
user_command = input()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.run(user_command, shell=True)

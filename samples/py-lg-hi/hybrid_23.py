class PublicViewInfoView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="slug",
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                required=True,
                description="The slug of the view to get public information " "about.",
            )
        ],
        tags=["Database table views"],
        operation_id="get_public_view_info",
        description=(
            "Returns the required public information to display a single "
            "shared view."
        ),
        request=None,
        responses={
            200: PublicViewInfoSerializer,
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
            401: get_error_schema(["ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW"]),
            404: get_error_schema(["ERROR_VIEW_DOES_NOT_EXIST"]),
        },
    )
    @map_exceptions(
        {
            UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP,
            ViewDoesNotExist: ERROR_VIEW_DOES_NOT_EXIST,
            NoAuthorizationToPubliclySharedView: ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW,
        }
    )
    @transaction.atomic
    def get(self, request: Request, slug: str) -> Response:
        handler = ViewHandler()
        view = handler.get_public_view_by_slug(
            request.user,
            slug,
            authorization_token=get_public_view_authorization_token(request),
        )
        view_specific = view.specific
        view_type = view_type_registry.get_by_model(view_specific)

        if not view_type.has_public_info:
            raise ViewDoesNotExist()

        field_options = view_type.get_visible_field_options_in_order(view_specific)
        fields = specific_iterator(
            Field.objects.filter(id__in=field_options.values_list("field_id"))
            .select_related("content_type")
            .prefetch_related("select_options")
        )

        return Response(
            PublicViewInfoSerializer(
                view=view,
                fields=fields,
            ).data
        )

class CollectionElementTypeMixin:
    is_collection_element = True

    # Three properties which define whether this collection element
    # is allowed to be publicly sortable, filterable and searchable
    # by page visitors. Can be overridden by subclasses to influence
    # whether page designers can flag collection elements and their
    # properties as sortable, filterable and searchable.
    is_publicly_sortable = True
    is_publicly_filterable = True
    is_publicly_searchable = True

    allowed_fields = [
        "data_source",
        "data_source_id",
        "items_per_page",
        "schema_property",
        "button_load_more_label",
    ]
    serializer_field_names = [
        "schema_property",
        "data_source_id",
        "items_per_page",
        "button_load_more_label",
        "property_options",
        "is_publicly_sortable",
        "is_publicly_filterable",
        "is_publicly_searchable",
    ]

    class SerializedDict(ElementDict):
        data_source_id: int
        items_per_page: int
        button_load_more_label: str
        schema_property: str
        property_options: List[Dict]

    def enhance_queryset(self, queryset):
        return queryset.prefetch_related("property_options")

    def after_update(
        self, instance: CollectionElementSubClass, values, changes: Dict[str, Tuple]
    ):
        """
        After the element has been updated we need to update the property options.

        :param instance: The instance of the element that has been updated.
        :param values: The values that have been updated.
        :param changes: A dictionary containing all changes which were made to the
            collection element prior to `after_update` being called.
        :return: None
        """

        # Following a DataSource change, from one DataSource to another, we drop all
        # property options. This is due to the fact that the `schema_property` in the
        # property options are specific to that data source's schema.
        data_source_changed = "data_source" in changes

        if "property_options" in values or data_source_changed:
            instance.property_options.all().delete()
            try:
                CollectionElementPropertyOptions.objects.bulk_create(
                    [
                        CollectionElementPropertyOptions(
                            **option,
                            element=instance,
                        )
                        for option in values.get("property_options", [])
                    ]
                )
            except IntegrityError as e:
                if "unique constraint" in e.args[0]:
                    raise CollectionElementPropertyOptionsNotUnique()
                raise e

    @property
    def serializer_field_overrides(self):
        from baserow.core.formula.serializers import FormulaSerializerField

        return {
            "is_publicly_sortable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_sortable,
                help_text="Whether this collection element is publicly sortable.",
            ),
            "is_publicly_filterable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_filterable,
                help_text="Whether this collection element is publicly filterable.",
            ),
            "is_publicly_searchable": serializers.BooleanField(
                read_only=True,
                default=self.is_publicly_searchable,
                help_text="Whether this collection element is publicly searchable.",
            ),
            "data_source_id": serializers.IntegerField(
                allow_null=True,
                default=None,
                help_text=CollectionElement._meta.get_field("data_source").help_text,
                required=False,
            ),
            "schema_property": serializers.CharField(
                allow_null=True,
                default=None,
                help_text=CollectionElement._meta.get_field(
                    "schema_property"
                ).help_text,
                required=False,
            ),
            "items_per_page": serializers.IntegerField(
                default=20,
                help_text=CollectionElement._meta.get_field("items_per_page").help_text,
                required=False,
            ),
            "button_load_more_label": FormulaSerializerField(
                help_text=CollectionElement._meta.get_field(
                    "button_load_more_label"
                ).help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "property_options": CollectionElementPropertyOptionsSerializer(
                many=True,
                required=False,
                help_text="The schema property options that can be set for the collection element.",
            ),
        }

    def prepare_value_for_db(
        self, values: Dict, instance: Optional[CollectionElementSubClass] = None
    ):
        if "data_source_id" in values:
            data_source_id = values.pop("data_source_id")
            if data_source_id is not None:
                schema_property = values.get("schema_property", None)
                data_source = DataSourceHandler().get_data_source(data_source_id)
                if data_source.service:
                    service_type = data_source.service.specific.get_type()
                    if service_type.returns_list and schema_property:
                        raise DRFValidationError(
                            "Data sources which return multiple rows cannot be "
                            "used in conjunction with the schema property."
                        )
                else:
                    raise DRFValidationError(
                        f"Data source {data_source_id} is partially "
                        "configured and not ready for use."
                    )

                if instance:
                    element_page = instance.page
                else:
                    element_page = values["page"]

                # The data source must belong to the same element page or the shared
                # page.
                if data_source.page_id not in [
                    element_page.id,
                    element_page.builder.shared_page.id,
                ]:
                    raise RequestBodyValidationException(
                        {
                            "data_source_id": [
                                {
                                    "detail": "The provided data source is not "
                                    "available for this element.",
                                    "code": "invalid_data_source",
                                }
                            ]
                        }
                    )
                values["data_source"] = data_source
            else:
                values["data_source"] = None

        return super().prepare_value_for_db(values, instance)

    def serialize_property(
        self,
        element: CollectionElementSubClass,
        prop_name: str,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ):
        """
        You can customize the behavior of the serialization of a property with this
        hook.
        """

        if prop_name == "property_options":
            return [
                {
                    "schema_property": po.schema_property,
                    "filterable": po.filterable,
                    "sortable": po.sortable,
                    "searchable": po.searchable,
                }
                for po in element.property_options.all()
            ]

        return super().serialize_property(
            element,
            prop_name,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def deserialize_property(
        self,
        prop_name: str,
        value: Any,
        id_mapping: Dict[str, Any],
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> Any:
        if prop_name == "data_source_id" and value:
            return id_mapping["builder_data_sources"][value]

        if prop_name == "property_options" and "database_fields" in id_mapping:
            property_options = []
            for po in value:
                field_id = get_field_id_from_field_key(po["schema_property"])
                if field_id is None:
                    # If we can't translate the `schema_property` into a Field ID, then
                    # it's not a `Field` db_column value. For example this can happen
                    # if someone chooses to have a `id` property option.
                    property_options.append(po)
                    continue
                new_field_id = id_mapping["database_fields"][field_id]
                property_options.append(
                    {**po, "schema_property": f"field_{new_field_id}"}
                )
            return property_options

        return super().deserialize_property(
            prop_name,
            value,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def import_context_addition(self, instance: CollectionElement) -> Dict[str, int]:
        """
        Given a collection element, adds the data_source_id to the import context.

        The data_source_id is not store in some formulas (current_record ones) so
        we need the generate this import context for all formulas of this element.
        """

        if instance.data_source_id:
            results = {"data_source_id": instance.data_source_id}
        else:
            results = {}

        if instance.schema_property is not None:
            results["schema_property"] = instance.schema_property

        return results

    def create_instance_from_serialized(
        self,
        serialized_values: Dict[str, Any],
        id_mapping,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> CollectionElementSubClass:
        """
        Responsible for creating the property options from the serialized values.

        :param serialized_values: The serialized values of the element.
        :param id_mapping: A dictionary containing the mapping of the old and new ids.
        :param files_zip: The zip file containing the files that can be used.
        :param storage: The storage that can be used to store files.
        :param cache: A dictionary that can be used to cache data.
        :param kwargs: Additional keyword arguments.
        :return: The created instance.
        """

        property_options = serialized_values.pop("property_options", [])

        instance = super().create_instance_from_serialized(
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

        # Create property options
        options = [
            CollectionElementPropertyOptions(**po, element=instance)
            for po in property_options
        ]
        CollectionElementPropertyOptions.objects.bulk_create(options)

        instance.property_options.add(*options)

        return instance

    def extract_formula_properties(
        self, instance: Element, **kwargs
    ) -> Dict[int, List[BaserowFormula]]:
        """
        Some collection elements (e.g. Repeat Element) may have a nested
        collection element which uses a schema_property. This property points
        to a field name that is connected to the parent collection element's
        data source.

        This method is overridden to ensure that any schema_property is also
        included in the list of field names used by the element.
        """

        properties = super().extract_formula_properties(instance, **kwargs)

        if schema_property := instance.schema_property:
            # if we have a data_source_id in the context from a parent or from the
            # current instance
            data_source_id = instance.data_source_id or kwargs.get(
                "data_source_id", None
            )
            if data_source_id:
                data_source = DataSourceHandler().get_data_source(data_source_id)
                properties[data_source.service_id] = [schema_property]

        return properties

class ImageElementWorkspaceStorageUsageItem(WorkspaceStorageUsageItemType):
    type = "image_element"

    def calculate_storage_usage(self, workspace_id: int) -> UsageInMB:
        image_elements = ImageElement.objects.filter(
            page__builder__workspace_id=workspace_id,
            page__trashed=False,
            page__builder__trashed=False,
        )

        usage_in_mb = (
            UserFile.objects.filter(Q(id__in=image_elements.values("image_file")))
            .values("size")
            .aggregate(sum=Coalesce(Sum("size") / USAGE_UNIT_MB, 0))["sum"]
        )

        return usage_in_mb

class DomainHandler:
    allowed_fields_create = ["domain_name"]
    allowed_fields_update = ["domain_name", "last_published"]

    def get_domain(self, domain_id: int, base_queryset: QuerySet = None) -> Domain:
        """
        Gets a domain by ID

        :param domain_id: The ID of the domain
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :raises DomainDoesNotExist: If the domain doesn't exist
        :return: The model instance of the domain
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        try:
            return base_queryset.get(id=domain_id)
        except Domain.DoesNotExist:
            raise DomainDoesNotExist()

    def get_domains(
        self, builder: Builder, base_queryset: QuerySet = None
    ) -> Iterable[Domain]:
        """
        Gets all the domains of a builder.

        :param builder: The builder we are trying to get all domains for
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :return: An iterable of all the specific domains
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        return specific_iterator(base_queryset.filter(builder=builder))

    def get_public_builder_by_domain_name(self, domain_name: str) -> Builder:
        """
        Returns a builder given a domain name it's been published for.

        :param domain_name: The domain name we want the builder for.
        :raise BuilderDoesNotExist: When no builder is published with this domain name.
        :return: A public builder instance.
        """

        try:
            domain = (
                Domain.objects.exclude(published_to=None)
                .select_related("published_to", "builder")
                .only("published_to", "builder")
                .get(domain_name=domain_name)
            )
        except Domain.DoesNotExist:
            raise BuilderDoesNotExist()

        if TrashHandler.item_has_a_trashed_parent(domain, check_item_also=True):
            raise BuilderDoesNotExist()

        return domain.published_to

    def create_domain(
        self, domain_type: DomainType, builder: Builder, **kwargs
    ) -> Domain:
        """
        Creates a new domain

        :param domain_type: The type of domain that's being created
        :param builder: The builder the domain belongs to
        :param kwargs: Additional attributes of the domain
        :return: The newly created domain instance
        """

        last_order = Domain.get_last_order(builder)

        model_class = cast(Domain, domain_type.model_class)

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_create + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        domain = model_class(builder=builder, order=last_order, **prepared_values)
        domain.save()

        return domain

    def delete_domain(self, domain: Domain):
        """
        Deletes the domain provided

        :param domain: The domain that must be deleted
        """

        domain.delete()

    def update_domain(self, domain: Domain, **kwargs) -> Domain:
        """
        Updates fields of a domain

        :param domain: The domain that should be updated
        :param kwargs: The fields that should be updated with their corresponding value
        :return: The updated domain
        """

        domain_type = domain.get_type()

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_update + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        for key, value in prepared_values.items():
            setattr(domain, key, value)

        try:
            domain.save()
        except IntegrityError as error:
            if "unique" in str(error) and "domain_name" in prepared_values:
                raise DomainNameNotUniqueError(prepared_values["domain_name"])
            raise error

        return domain

    def order_domains(
        self, builder: Builder, order: List[int], base_qs=None
    ) -> List[int]:
        """
        Assigns a new order to the domains in a builder application.
        You can provide a base_qs for pre-filter the domains affected by this change.

        :param builder: The builder that the domains belong to
        :param order: The new order of the domains
        :param base_qs: A QS that can have filters already applied
        :raises DomainNotInBuilder: If the domain is not part of the provided builder
        :return: The new order of the domains
        """

        if base_qs is None:
            base_qs = Domain.objects.filter(builder=builder)

        try:
            full_order = Domain.order_objects(base_qs, order)
        except IdDoesNotExist as error:
            raise DomainNotInBuilder(error.not_existing_id)

        return full_order

    def publish(self, domain: Domain, progress: Progress):
        """
        Publishes a builder for the given domain object. If the builder was
        already published, the previous version is deleted and a new one is created.
        When a builder is published, a clone of the current version is created to avoid
        further modifications to the original builder affect the published version.

        :param domain: The object carrying the information for the publishing.
        :param progress: A progress object to track the publishing operation progress.
        """

        builder = domain.builder
        workspace = builder.workspace

        # Delete previously existing builder publication
        if domain.published_to:
            domain.published_to.delete()

        builder_application_type = application_type_registry.get("builder")

        import_export_config = ImportExportConfig(
            include_permission_data=True, reduce_disk_space_usage=False
        )

        default_storage = get_default_storage()

        exported_builder = builder_application_type.export_serialized(
            builder, import_export_config, None, default_storage
        )

        # Update user_source uid to have something stable per domain.
        # This is mainly because we want a token generated for one version of a site
        # to still be valid if we redeploy the website.
        exported_builder["user_sources"] = [
            {**user_source, "uid": f"domain_{domain.id}__{user_source['uid']}"}
            for user_source in exported_builder["user_sources"]
        ]

        progress.increment(by=50)

        id_mapping = {"import_workspace_id": workspace.id}
        duplicate_builder = builder_application_type.import_serialized(
            None,
            exported_builder,
            import_export_config,
            id_mapping,
            None,
            default_storage,
            progress_builder=progress.create_child_builder(represents_progress=50),
        )
        domain.published_to = duplicate_builder
        domain.last_published = datetime.now(tz=timezone.utc)
        domain.save()

        return domain


import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
    #ruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % request.args.get('param')))
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
    # ruleid: python_flask_rule-path-traversal-open
    return open("echo " + flask.request.args.get("param"), 'r').read()


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
    # ruleid: python_flask_rule-path-traversal-open
    with open(flask.request.form['param'], 'r') as fin:
        data = json.load(fin)
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
    # ruleid: python_flask_rule-path-traversal-open
    return open("{}.csv".format(param)).read()


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# ruleid: python_exec_rule-linux-command-wildcard-injection
o.system("/bin/tar xvzf *")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

# safe
pop('/bin/gcc --version', shell=True)
Popen('/bin/gcc --version', shell=True)

subprocess.Popen('/bin/gcc --version', shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.call('/bin/ls -l', shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.check_call('/bin/ls -l', shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.check_output('/bin/ls -l', shell=True)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
subprocess.run('/bin/ls -l', shell=True)

subprocess.Popen('/bin/ls *', shell=True)
subprocess.Popen('/bin/ls %s' % ('something'), shell=True)
subprocess.Popen('/bin/ls {}'.format('something'), shell=True)

command = "/bin/ls" + unknown_function()
subprocess.Popen(command, shell=True)

subprocess.Popen('/bin/ls && cat /etc/passwd', shell=True)

constant_command = 'pwd'
subprocess.call(constant_command, shell=True)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# unsafe
user_command = input()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-subprocess-call
subprocess.run(user_command)
subprocess.run(user_command, shell=True)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
os.popen("rsync -avz * destination/", 'r', -1)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# ruleid: python_urlopen_rule-urllib-urlopen
urllib.urlopen(url_from_user)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
    # ruleid: python_flask_rule-path-traversal-open
    with open(route_param, 'r') as fout:
        return fout.read()


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_urlopen_rule-urllib-urlopen
opener.retrieve(url_from_user)
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# source: https://github.com/semgrep/semgrep-rules/blob/release/python/flask/security/open-redirect.py
from urlparse import urlparse

from flask import request, redirect
from flask import Flask, redirect, request, url_for
from werkzeug.urls import url_parse

app = Flask(__name__)


@app.route("open_redirect/")
def open_redirect():
    # ruleid: python_flask_rule-flask-open-redirect
    url = request.args.get("url")
    print("something")
    return redirect(url)


@app.route("not_open_redirect/")
def not_open_redirect():
    page = request.args.get("page")
    if page == "about":
        # ok: python_flask_rule-flask-open-redirect
        url = "/about/"
        return redirect(url)
    elif page == "test":
        # ok: python_flask_rule-flask-open-redirect
        redirect(f"{request.path}/")
    else:
        # ok: python_flask_rule-flask-open-redirect
        redirect(request.path + "?failed")


@app.route("filter")
def filter():
    # ok: python_flask_rule-flask-open-redirect
    next_page = request.args.get('next')
    if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('main.index')
    return redirect(next_page)


# cf. https://github.com/mideind/Netskrafl/blob/2e1933ad0710a4425c319fde3b92b2a70729ed80/netskrafl.py#L1712


@app.route("/userprefs", methods=["GET", "POST"])
@auth_required()
def userprefs():
    """ Handler for the user preferences page """

    user = current_user()

    uf = UserForm()
    err = dict()

    # The URL to go back to, if not main.html
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    if request.method == "GET":
        # Entering the form for the first time: load the user data
        uf.init_from_user(user)
    elif request.method == "POST":
        # Attempting to submit modified data: retrieve it and validate
        uf.init_from_form(request.form)
        err = uf.validate()
        if not err:
            # All is fine: store the data back in the user entity
            uf.store(user)
            return redirect(from_url or url_for("main"))

    # Render the form with the current data and error messages, if any
    return render_template("userprefs.html", uf=uf, err=err, from_url=from_url)


# Positive Test Cases (Vulnerable - should trigger the rule)
@app.route("/open_redirect_brackets_positive/")
def open_redirect_brackets_positive():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return redirect(url)


@app.route("/open_redirect_function_call_positive/")
def open_redirect_function_call_negative():
    # ok: python_flask_rule-flask-open-redirect
    url = validate_url(request.args["redirect_url"])
    if url:
        return redirect(url)
    else:
        return url_for("home")


@app.route("/open_redirect_direct_assignment_positive/")
def open_redirect_direct_assignment_positive():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return redirect(url)


# Negative Test Cases (Safe - should not trigger the rule)
@app.route("/safe_redirect_brackets_negative/")
def safe_redirect_brackets_negative():
    # ok: python_flask_rule-flask-open-redirect
    url = validate_url(request.args.get("redirect_url", ""))
    if url:
        return redirect(url)
    else:
        return url_for("home")


@app.route("/safe_redirect_function_call_negative/")
def safe_redirect_function_call_negative():
    # ok: python_flask_rule-flask-open-redirect
    url = url_for("some_endpoint", param1="value1")
    return redirect(url)


@app.route("/safe_redirect_direct_assignment_negative/")
def safe_redirect_direct_assignment_negative():
    # ok: python_flask_rule-flask-open-redirect
    return redirect("/home")


@app.route("/open_redirect_positive/", methods=["POST"])
def open_redirect_positive():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/safe_redirect_positive/", methods=["POST"])
def safe_redirect_positive():
    # ok: python_flask_rule-flask-open-redirect
    url = request.get_data().decode('utf-8')
    if validate_url(url):  # Validate before redirect
        return redirect(url)
    else:
        return "Invalid URL"


# Placeholder Function for Validation
def validate_url(url):
    parsed_url = urlparse(url)
    if parsed_url.scheme not in ('http', 'https') or parsed_url.netloc != '':
        return None
    return url

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# ruleid: python_requests_rule-request-without-timeout
requests.post('https://gmail.com')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/yaml_load.py
# hash:  8eee173

import json
import yaml

def test_yaml_load():
    ystr = yaml.dump({'a': 1, 'b': 2, 'c': 3})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    yaml.dump(y)
    try:
        yaml.load(ystr, Loader=yaml.CSafeLoader)
        # ruleid: python_deserialization_rule-yaml-load
        yaml.load(ystr, Loader=yaml.Loader)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    except AttributeError:
        # CSafeLoader only exists if you build yaml with LibYAML
        y = yaml.load(ystr, Loader=yaml.SafeLoader)

def test_json_load():
    # no issue should be found
    j = json.load("{}")

import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
    # ruleid: python_flask_rule-path-traversal-open
    with open(flask.request.form['param'] + '.csv', 'r') as fin:
        data = json.load(fin)
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '" + identifier + "'")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# ruleid: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen(url_from_user)
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

from Crypto.Hash import SHA as pycrypto_sha
from Cryptodome.Hash import SHA as pycryptodomex_sha

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_crypto_rule-crypto-hash-sha1
pycryptodomex_sha.new()

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_exec_rule-linux-command-wildcard-injection
os.system("tar *")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
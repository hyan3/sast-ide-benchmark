class DomainHandler:
    allowed_fields_create = ["domain_name"]
    allowed_fields_update = ["domain_name", "last_published"]

    def get_domain(self, domain_id: int, base_queryset: QuerySet = None) -> Domain:
        """
        Gets a domain by ID

        :param domain_id: The ID of the domain
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :raises DomainDoesNotExist: If the domain doesn't exist
        :return: The model instance of the domain
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        try:
            return base_queryset.get(id=domain_id)
        except Domain.DoesNotExist:
            raise DomainDoesNotExist()

    def get_domains(
        self, builder: Builder, base_queryset: QuerySet = None
    ) -> Iterable[Domain]:
        """
        Gets all the domains of a builder.

        :param builder: The builder we are trying to get all domains for
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :return: An iterable of all the specific domains
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        return specific_iterator(base_queryset.filter(builder=builder))

    def get_public_builder_by_domain_name(self, domain_name: str) -> Builder:
        """
        Returns a builder given a domain name it's been published for.

        :param domain_name: The domain name we want the builder for.
        :raise BuilderDoesNotExist: When no builder is published with this domain name.
        :return: A public builder instance.
        """

        try:
            domain = (
                Domain.objects.exclude(published_to=None)
                .select_related("published_to", "builder")
                .only("published_to", "builder")
                .get(domain_name=domain_name)
            )
        except Domain.DoesNotExist:
            raise BuilderDoesNotExist()

        if TrashHandler.item_has_a_trashed_parent(domain, check_item_also=True):
            raise BuilderDoesNotExist()

        return domain.published_to

    def create_domain(
        self, domain_type: DomainType, builder: Builder, **kwargs
    ) -> Domain:
        """
        Creates a new domain

        :param domain_type: The type of domain that's being created
        :param builder: The builder the domain belongs to
        :param kwargs: Additional attributes of the domain
        :return: The newly created domain instance
        """

        last_order = Domain.get_last_order(builder)

        model_class = cast(Domain, domain_type.model_class)

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_create + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        domain = model_class(builder=builder, order=last_order, **prepared_values)
        domain.save()

        return domain

    def delete_domain(self, domain: Domain):
        """
        Deletes the domain provided

        :param domain: The domain that must be deleted
        """

        domain.delete()

    def update_domain(self, domain: Domain, **kwargs) -> Domain:
        """
        Updates fields of a domain

        :param domain: The domain that should be updated
        :param kwargs: The fields that should be updated with their corresponding value
        :return: The updated domain
        """

        domain_type = domain.get_type()

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_update + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        for key, value in prepared_values.items():
            setattr(domain, key, value)

        try:
            domain.save()
        except IntegrityError as error:
            if "unique" in str(error) and "domain_name" in prepared_values:
                raise DomainNameNotUniqueError(prepared_values["domain_name"])
            raise error

        return domain

    def order_domains(
        self, builder: Builder, order: List[int], base_qs=None
    ) -> List[int]:
        """
        Assigns a new order to the domains in a builder application.
        You can provide a base_qs for pre-filter the domains affected by this change.

        :param builder: The builder that the domains belong to
        :param order: The new order of the domains
        :param base_qs: A QS that can have filters already applied
        :raises DomainNotInBuilder: If the domain is not part of the provided builder
        :return: The new order of the domains
        """

        if base_qs is None:
            base_qs = Domain.objects.filter(builder=builder)

        try:
            full_order = Domain.order_objects(base_qs, order)
        except IdDoesNotExist as error:
            raise DomainNotInBuilder(error.not_existing_id)

        return full_order

    def publish(self, domain: Domain, progress: Progress):
        """
        Publishes a builder for the given domain object. If the builder was
        already published, the previous version is deleted and a new one is created.
        When a builder is published, a clone of the current version is created to avoid
        further modifications to the original builder affect the published version.

        :param domain: The object carrying the information for the publishing.
        :param progress: A progress object to track the publishing operation progress.
        """

        builder = domain.builder
        workspace = builder.workspace

        # Delete previously existing builder publication
        if domain.published_to:
            domain.published_to.delete()

        builder_application_type = application_type_registry.get("builder")

        import_export_config = ImportExportConfig(
            include_permission_data=True, reduce_disk_space_usage=False
        )

        default_storage = get_default_storage()

        exported_builder = builder_application_type.export_serialized(
            builder, import_export_config, None, default_storage
        )

        # Update user_source uid to have something stable per domain.
        # This is mainly because we want a token generated for one version of a site
        # to still be valid if we redeploy the website.
        exported_builder["user_sources"] = [
            {**user_source, "uid": f"domain_{domain.id}__{user_source['uid']}"}
            for user_source in exported_builder["user_sources"]
        ]

        progress.increment(by=50)

        id_mapping = {"import_workspace_id": workspace.id}
        duplicate_builder = builder_application_type.import_serialized(
            None,
            exported_builder,
            import_export_config,
            id_mapping,
            None,
            default_storage,
            progress_builder=progress.create_child_builder(represents_progress=50),
        )
        domain.published_to = duplicate_builder
        domain.last_published = datetime.now(tz=timezone.utc)
        domain.save()

        return domain

class AllApplicationsView(APIView):
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        tags=["Applications"],
        operation_id="list_all_applications",
        description=(
            "Lists all the applications that the authorized user has access to. The "
            "properties that belong to the application can differ per type. An "
            "application always belongs to a single workspace. All the applications of the "
            "workspaces that the user has access to are going to be listed here."
        ),
        responses={
            200: PolymorphicApplicationResponseSerializer(many=True),
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
        },
    )
    @map_exceptions({UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP})
    def get(self, request):
        """
        Responds with a list of serialized applications that belong to the user. If a
        workspace id is provided only the applications of that workspace are going to be
        returned.
        """

        workspaces = Workspace.objects.filter(users=request.user)

        # Compute list of readable application ids
        applications_ids = []
        for workspace in workspaces:
            applications = Application.objects.filter(
                workspace=workspace, workspace__trashed=False
            )
            applications = CoreHandler().filter_queryset(
                request.user,
                ListApplicationsWorkspaceOperationType.type,
                applications,
                workspace=workspace,
            )
            applications_ids += applications.values_list("id", flat=True)

        # Then filter with these ids
        applications = specific_iterator(
            Application.objects.select_related("content_type", "workspace")
            .prefetch_related("workspace__template_set")
            .filter(id__in=applications_ids)
            .order_by("workspace_id", "order"),
            per_content_type_queryset_hook=(
                lambda model, queryset: application_type_registry.get_by_model(
                    model
                ).enhance_queryset(queryset)
            ),
        )

        data = [
            PolymorphicApplicationResponseSerializer(
                application, context={"request": request}
            ).data
            for application in applications
        ]

        return Response(data)

class DomainHandler:
    allowed_fields_create = ["domain_name"]
    allowed_fields_update = ["domain_name", "last_published"]

    def get_domain(self, domain_id: int, base_queryset: QuerySet = None) -> Domain:
        """
        Gets a domain by ID

        :param domain_id: The ID of the domain
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :raises DomainDoesNotExist: If the domain doesn't exist
        :return: The model instance of the domain
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        try:
            return base_queryset.get(id=domain_id)
        except Domain.DoesNotExist:
            raise DomainDoesNotExist()

    def get_domains(
        self, builder: Builder, base_queryset: QuerySet = None
    ) -> Iterable[Domain]:
        """
        Gets all the domains of a builder.

        :param builder: The builder we are trying to get all domains for
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :return: An iterable of all the specific domains
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        return specific_iterator(base_queryset.filter(builder=builder))

    def get_public_builder_by_domain_name(self, domain_name: str) -> Builder:
        """
        Returns a builder given a domain name it's been published for.

        :param domain_name: The domain name we want the builder for.
        :raise BuilderDoesNotExist: When no builder is published with this domain name.
        :return: A public builder instance.
        """

        try:
            domain = (
                Domain.objects.exclude(published_to=None)
                .select_related("published_to", "builder")
                .only("published_to", "builder")
                .get(domain_name=domain_name)
            )
        except Domain.DoesNotExist:
            raise BuilderDoesNotExist()

        if TrashHandler.item_has_a_trashed_parent(domain, check_item_also=True):
            raise BuilderDoesNotExist()

        return domain.published_to

    def create_domain(
        self, domain_type: DomainType, builder: Builder, **kwargs
    ) -> Domain:
        """
        Creates a new domain

        :param domain_type: The type of domain that's being created
        :param builder: The builder the domain belongs to
        :param kwargs: Additional attributes of the domain
        :return: The newly created domain instance
        """

        last_order = Domain.get_last_order(builder)

        model_class = cast(Domain, domain_type.model_class)

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_create + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        domain = model_class(builder=builder, order=last_order, **prepared_values)
        domain.save()

        return domain

    def delete_domain(self, domain: Domain):
        """
        Deletes the domain provided

        :param domain: The domain that must be deleted
        """

        domain.delete()

    def update_domain(self, domain: Domain, **kwargs) -> Domain:
        """
        Updates fields of a domain

        :param domain: The domain that should be updated
        :param kwargs: The fields that should be updated with their corresponding value
        :return: The updated domain
        """

        domain_type = domain.get_type()

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_update + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        for key, value in prepared_values.items():
            setattr(domain, key, value)

        try:
            domain.save()
        except IntegrityError as error:
            if "unique" in str(error) and "domain_name" in prepared_values:
                raise DomainNameNotUniqueError(prepared_values["domain_name"])
            raise error

        return domain

    def order_domains(
        self, builder: Builder, order: List[int], base_qs=None
    ) -> List[int]:
        """
        Assigns a new order to the domains in a builder application.
        You can provide a base_qs for pre-filter the domains affected by this change.

        :param builder: The builder that the domains belong to
        :param order: The new order of the domains
        :param base_qs: A QS that can have filters already applied
        :raises DomainNotInBuilder: If the domain is not part of the provided builder
        :return: The new order of the domains
        """

        if base_qs is None:
            base_qs = Domain.objects.filter(builder=builder)

        try:
            full_order = Domain.order_objects(base_qs, order)
        except IdDoesNotExist as error:
            raise DomainNotInBuilder(error.not_existing_id)

        return full_order

    def publish(self, domain: Domain, progress: Progress):
        """
        Publishes a builder for the given domain object. If the builder was
        already published, the previous version is deleted and a new one is created.
        When a builder is published, a clone of the current version is created to avoid
        further modifications to the original builder affect the published version.

        :param domain: The object carrying the information for the publishing.
        :param progress: A progress object to track the publishing operation progress.
        """

        builder = domain.builder
        workspace = builder.workspace

        # Delete previously existing builder publication
        if domain.published_to:
            domain.published_to.delete()

        builder_application_type = application_type_registry.get("builder")

        import_export_config = ImportExportConfig(
            include_permission_data=True, reduce_disk_space_usage=False
        )

        default_storage = get_default_storage()

        exported_builder = builder_application_type.export_serialized(
            builder, import_export_config, None, default_storage
        )

        # Update user_source uid to have something stable per domain.
        # This is mainly because we want a token generated for one version of a site
        # to still be valid if we redeploy the website.
        exported_builder["user_sources"] = [
            {**user_source, "uid": f"domain_{domain.id}__{user_source['uid']}"}
            for user_source in exported_builder["user_sources"]
        ]

        progress.increment(by=50)

        id_mapping = {"import_workspace_id": workspace.id}
        duplicate_builder = builder_application_type.import_serialized(
            None,
            exported_builder,
            import_export_config,
            id_mapping,
            None,
            default_storage,
            progress_builder=progress.create_child_builder(represents_progress=50),
        )
        domain.published_to = duplicate_builder
        domain.last_published = datetime.now(tz=timezone.utc)
        domain.save()

        return domain

class PostgresqlLenientDatabaseSchemaEditor:
    """
    Class changes the behavior of the postgres database schema editor slightly. Normally
    when the field type is altered and one of the data columns doesn't have a value that
    can be casted to the new type, it fails. This class introduces the possibility to
    run a custom function, like REGEXP_REPLACE, to convert the data to the correct
    format. If the casting still fails the value will be set to null.
    """

    sql_alter_column_type = (
        "ALTER COLUMN %(column)s TYPE %(type)s%(collation)s "
        "USING pg_temp.try_cast(%(column)s::text)"
    )

    def __init__(
        self,
        *args,
        alter_column_prepare_old_value="",
        alter_column_prepare_new_value="",
        force_alter_column=False,
        **kwargs,
    ):
        self.alter_column_prepare_old_value = alter_column_prepare_old_value
        self.alter_column_prepare_new_value = alter_column_prepare_new_value
        self.force_alter_column = force_alter_column
        super().__init__(*args, **kwargs)

    def _alter_field(
        self,
        model,
        old_field,
        new_field,
        old_type,
        new_type,
        old_db_params,
        new_db_params,
        strict=False,
    ):
        if self.force_alter_column:
            old_type = f"{old_type}_forced"

        if old_type != new_type:
            variables = {}
            if isinstance(self.alter_column_prepare_old_value, tuple):
                alter_column_prepare_old_value, v = self.alter_column_prepare_old_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_old_value = self.alter_column_prepare_old_value

            if isinstance(self.alter_column_prepare_new_value, tuple):
                alter_column_prepare_new_value, v = self.alter_column_prepare_new_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_new_value = self.alter_column_prepare_new_value

            quoted_column_name = self.quote_name(new_field.column)
            for key, value in variables.items():
                variables[key] = value.replace("$FUNCTION$", "")
            self.execute(sql_drop_try_cast)
            self.execute(
                sql_create_try_cast
                % {
                    "column": quoted_column_name,
                    "type": new_type,
                    "alter_column_prepare_old_value": alter_column_prepare_old_value,
                    "alter_column_prepare_new_value": alter_column_prepare_new_value,
                },
                variables,
            )

        return super()._alter_field(
            model,
            old_field,
            new_field,
            old_type,
            new_type,
            old_db_params,
            new_db_params,
            strict,
        )

    def _alter_column_type_sql(
        self, model, old_field, new_field, new_type, old_collation, new_collation
    ):
        # Cast when data type changed.
        # Make ALTER TYPE with SERIAL make sense.
        table = strip_quotes(model._meta.db_table)
        serial_fields_map = {
            "bigserial": "bigint",
            "serial": "integer",
            "smallserial": "smallint",
        }
        if collate_sql := self._collate_sql(
            new_collation, old_collation, model._meta.db_table
        ):
            collate_sql = f" {collate_sql}"
        else:
            collate_sql = ""
        if new_type.lower() in serial_fields_map:
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            return (
                (
                    self.sql_alter_column_type
                    % {
                        "column": self.quote_name(column),
                        "type": serial_fields_map[new_type.lower()],
                        "collation": collate_sql,
                    },
                    [],
                ),
                [
                    (
                        self.sql_delete_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_create_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_alter_column
                        % {
                            "table": self.quote_name(table),
                            "changes": self.sql_alter_column_default
                            % {
                                "column": self.quote_name(column),
                                "default": "nextval('%s')"
                                % self.quote_name(sequence_name),
                            },
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_max
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_owner
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                ],
            )
        elif (
            old_field.db_parameters(connection=self.connection)["type"]
            in serial_fields_map
        ):
            # Drop the sequence if migrating away from AutoField.
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            fragment, _ = BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )
            return fragment, [
                (
                    self.sql_delete_sequence
                    % {
                        "sequence": self.quote_name(sequence_name),
                    },
                    [],
                ),
            ]
        else:
            return BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )

    def _field_should_be_altered(self, old_field, new_field):
        return self.force_alter_column or super()._field_should_be_altered(
            old_field, new_field
        )


# License: MIT (c) GitLab Inc.
import sys
import tarfile
import tempfile


def unsafe_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def managed_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def list_members_archive_handler(filename):
    tar = tarfile.open(filename)
    # ruleid: python_files_rule-tarfile-unsafe-members
    tar.extractall(path=tempfile.mkdtemp(), members=[])
    tar.close()


def provided_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def members_filter(tarfile):
    result = []
    for member in tarfile.getmembers():
        if '../' in member.name:
            print('Member name container directory traversal sequence')
            continue
        elif (member.issym() or member.islnk()) and ('../' in member.linkname):
            print('Symlink to external resource')
            continue
        result.append(member)
    return result


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        unsafe_archive_handler(filename)
        managed_members_archive_handler(filename)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/pickle_deserialize.py
# hash:  8eee173

import _pickle
import pickle
import os

# pickle
pick = pickle.dumps({'a': 'b', 'c': 'd'})

with open('serialized.pkl', 'wb') as file:
    pickle.dump([1, 2, '3'], file)

# Unpickle
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

with open('serialized.pkl', 'rb') as file:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

with open('serialized.pkl', 'rb') as file:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

with open('serialized.pkl', 'rb') as file:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

with open('serialized.pkl', 'rb') as file:
    # ruleid: python_deserialization_rule-pickle
    print(_pickle.Unpickler(file).load())

# Delete the file "serialized.pkl"
os.remove('serialized.pkl')

# This will run the following command and list all the directories: ls ~
# But this is okay because this string is not controlled by the attacker.
# And so hard-coded strings are okay.
# ok: python_deserialization_rule-pickle
pickle.loads(b"cos\nsystem\n(S\'ls ~\'\ntR.")

aa = ["a", 'b', 3]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
o.popen('/bin/chmod *')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data3 = UsmUserData(username, auth_key)
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

from django.http import HttpResponse
from urllib.parse import parse_qs, urlparse
from myapp.models import User, Person
from django.shortcuts import render
from django.views import View



import logging
logger = logging.getLogger(__name__)

def index(request):
    return render(request, 'index.html')

def debug(input):
    logger.debug('[DEBUG] %s' % input)
    return input

def test_sql_injection1(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with % operator 
def test_sql_injection2(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object inline with query
# request object format - request.$W.get(...)
# string interpolation with F-Strings
def test_sql_injection3(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object inline with query
# request object format - request.$W.get(...)
# string concatenation with + operator
def test_sql_injection4(request):

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")

#-------------------------------------------------------------

# request object not inline
# string interpolation with str.format() method
def test_sql_injection5(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object not inline
# string interpolation with % operator 
def test_sql_injection6(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '%s'" % uname
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object not inline
# string interpolation with F-Strings
def test_sql_injection7(request):

    uname = request.GET.get('username', 'testuser2')
    query = f'SELECT * FROM myapp_user WHERE username = "{uname}"'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object not inline
# string concatenation with + operator
def test_sql_injection8(request):

    uname = request.GET.get('username', 'testuser2')
    query = "SELECT * FROM myapp_user WHERE username = '" + uname + "'"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
#-------------------------------------------------
        

# request object format - request.$W(...)
# string interpolation with str.format() method
def test_sql_injection9(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W(...)
# string interpolation with % operator 
def test_sql_injection10(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W(...)
# string interpolation with F-Strings
def test_sql_injection11(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W(...)
# string concatenation with + operator
def test_sql_injection12(request):

    query_string = urlparse(request.get_full_path()).query
    params = parse_qs(query_string)
    uname = params.get('username', [''])[0]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")  

     
#-------------------------------------------------
    
# request object format - request.$W[...]
# string interpolation with str.format() method
def test_sql_injection13(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
    
# request object format - request.$W[...]
# string interpolation with % operator 
def test_sql_injection14(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        
        return HttpResponse(f"An error occurred: {str(e)}")
   
  
# request object format - request.$W[...]
# string interpolation with F-Strings
def test_sql_injection15(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")


# request object format - request.$W[...]
# string concatenation with + operator
def test_sql_injection16(request):

    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  

#----------------------

# Using Parameterized query - Safe
# True negative case
def test_sql_injection17(request):

    uname = request.GET["username"] 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw('SELECT * FROM myapp_user WHERE username = %s', (uname,))
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 


# Using translations map
# Not using Parameterized queries
def test_sql_injection18(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    uname = request.GET["username"] 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 



# Using translations map
# No user input
# True negative
def test_sql_injection19(request):

    name_map = {'username': 'person_name', 'email':'person_email'}

    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user", translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}")  
        

# Using translations map
# Using Parameterized queries - Safe
# True negative
def test_sql_injection20(request):

    name_map = {'username': 'person_name', 'email':'person_email'}
    # ok: python_django_rule-django-raw-used
    res = Person.objects.raw("SELECT * FROM myapp_user WHERE username = %s LIMIT 1", [request.GET["username"]], translations=name_map)
    try:
        person = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {person.person_name}<br>Email: {person.person_email}")
    except StopIteration:
        return HttpResponse("No such user found.")


# With empty parameters as tuple
# Not using Parameterized queries
def test_sql_injection21(request):
    uname = request.GET["username"] 
    res = User.objects.raw(
                # ruleid: python_django_rule-django-raw-used
                "SELECT * FROM myapp_user WHERE username = '%s'" % uname,
                params=(),
                )
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# With empty parameters as list
# No User input: String interpolation % operator
# Not using Parameterized queries
def test_sql_injection22(request):
    uname = request.GET["username"]     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name}: Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# With empty parameters as list
# No User input
# True Negative
def test_sql_injection23(request):    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % "testuser2", [])
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

#----------------------------------------------------

# No User input
# String interpolation % operator 
# True negative case
def test_sql_injection24(request):

    val = "testuser1" 
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw("SELECT * FROM myapp_user WHERE username = '%s'" % val)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# String concatenation + operator 
# True negative case
def test_sql_injection25(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '" + val +"'"     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with F-Strings
# True negative case
def test_sql_injection26(request):
    val = "testuser2"
    query = f"SELECT * FROM myapp_user WHERE username = '{val}'"    
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# No User input
# string interpolation with str.format() method
# True negative case
def test_sql_injection27(request):
    val = "testuser2"
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(val)     
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(query)
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} (Safe): Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
#----------

# Testing different parameter name
def test_sql_injection27(myrequest):
    uname = myrequest.GET["username"]
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 
    
# Testing multiple view arguments
def test_sql_injection28(request, uname):
    query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {request.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing inter-procedural tainted query returned
def intra_file_query_builder(myrequest=None):
    if myrequest is None:
        uname = myrequest.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)
    return query

# Testing intra-file inter-procedural vulnerable calls
def test_sql_injection29(myrequest):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing vulnerable calls in class-based views 
class TestSQLInjection30(View):

    # Testing inter-procedural tainted query returned 
    def intra_class_query_builder(self):
        uname = self.request.GET["username"]
        query = "SELECT * FROM myapp_user WHERE username = '{}'".format(uname)     
        return query

    # Testing intra-class and inter-procedural vulnerable calls
    def get(self, myrequest):
        # todoruleid: python_django_rule-django-raw-used
        res = User.objects.raw(self.intra_class_query_builder())
        try:
            user = next(iter(res))  
            return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
        except StopIteration:
            return HttpResponse("No such user found.")
        except Exception as e:
            return HttpResponse(f"An error occurred: {str(e)}")     

from .helpers import inter_file_query_builder

# Testing inter-file inter-procedural vulnerable calls with parameters
def test_sql_injection31(myrequest):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing inter-file inter-procedural vulnerable calls without parameters
def test_sql_injection32(myrequest):
    # todoruleid: python_django_rule-django-raw-used
    res = User.objects.raw(inter_file_query_builder())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

# Testing safe inter-procedural and inter-file
def intra_file_safe_function(param=None):
    retval = "SELECT * FROM myapp_user WHERE username = 'testuser1'"
    return retval

# Testing safe inter-procedural and intra-file
def test_sql_injection33(myrequest):
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(intra_file_safe_function())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

from .helpers import inter_file_safe_function

# Testing safe inter-procedural and inter-file
def test_sql_injection34(myrequest):
    # ok: python_django_rule-django-raw-used
    res = User.objects.raw(inter_file_safe_function())
    try:
        user = next(iter(res))  
        return HttpResponse(f"Test Case {myrequest.resolver_match.view_name} : Found user!<br>Username: {user.username}<br>Email: {user.email}")
    except StopIteration:
        return HttpResponse("No such user found.")
    except Exception as e:
        return HttpResponse(f"An error occurred: {str(e)}") 

#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

from flask import Flask, request

app = Flask(__name__)
# Literal strings are ok
com = '''
import os
os.system('ls -l')
'''


@app.route('/exec', methods=['POST'])
def exec_route():
    user_input = request.form['input']

    # ruleid: python_exec_rule-exec-used
    exec(f"{user_input}")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route('/safe', methods=['GET'])
def exec_safe_route():
    text = 'exec is evil'
    com = f"print('{text}')"

    # rule ok:
    exec(com)

    # rule ok:
    exec(com)

    # rule ok:
    exec("2 + 3")

    # rule ok:
    exec("print('hello world!')")

    return "no response"


if __name__ == '__main__':
    app.run(debug=True)

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_requests_rule-request-without-timeout
requests.post('https://gmail.com', timeout=None)
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
os.popen("chmod 777 *", 'r', -1)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
    # ruleid: python_flask_rule-path-traversal-open
    return open(route_param, 'r').read()


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

import os

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_file-permissions_rule-general-bad-permission
os.chmod('/tmp/oh_hai', 0o007)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

import hashlib

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_crypto_rule-hash-md5
abc = str.replace(hashlib.md5("1"), "###")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/os-spawn.py
# hash:  8eee173

import os

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-start-process-with-no-shell
os.execvp(file, args)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_etree_elementtree.py
# hash:  8eee173

import xml.etree.ElementTree as badET
import defusedxml.ElementTree as goodET

xmlString = "<note>\n<to>Tove</to>\n<from>Jani</from>\n<heading>Reminder</heading>\n<body>Don't forget me this weekend!</body>\n</note>"

# unsafe
tree = badET.fromstring(xmlString)
print(tree)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_xml_rule-element
badET.iterparse('filethatdoesntexist.xml')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# safe
tree = goodET.fromstring(xmlString)
print(tree)
# ok: python_xml_rule-element
goodET.parse('filethatdoesntexist.xml')
# ok: python_xml_rule-element
goodET.iterparse('filethatdoesntexist.xml')
# ok: python_xml_rule-element
a = goodET.XMLParser()

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id = '" + bar + "'" + " AND somethingElse <= " + foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_etree_celementtree.py
# hash:  8eee173

import xml.etree.cElementTree as badET
import defusedxml.cElementTree as goodET

xmlString = "<note>\n<to>Tove</to>\n<from>Jani</from>\n<heading>Reminder</heading>\n<body>Don't forget me this weekend!</body>\n</note>"

# unsafe
tree = badET.fromstring(xmlString)
print(tree)
# ruleid: python_xml_rule-celement
badET.parse('filethatdoesntexist.xml')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# safe
tree = goodET.fromstring(xmlString)
print(tree)
# ok: python_xml_rule-celement
goodET.parse('filethatdoesntexist.xml')
# ok: python_xml_rule-celement
goodET.iterparse('filethatdoesntexist.xml')
# ok: python_xml_rule-celement
a = goodET.XMLParser()


#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/mktemp.py
# hash:  8eee173

from tempfile import mktemp
import tempfile.mktemp as mt
import tempfile as tmp

foo = 'hi'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_tmpdir_rule-mktemp-q
tmp.mktemp(foo)

#!/usr/bin/env python

# SETUP

import os

content = """
__import__('datetime').datetime.now()
"""
with open("eval.input","w+") as f:
	f.write(content)

# NOTES
#   String Literals f, """ """, r'', b'', fr"", etc should all be detected by "...", but still some tests are added as assurance. 

# single string argument
#ok: python_eval_rule-eval
eval("1+1")

# literal string concatenation 
#ok: python_eval_rule-eval
print(eval("1"+"+"+"1"))

# implicit string concatenation
#ok: python_eval_rule-eval
eval("1+" "1")

plus="+"
one= "1"

# variable string concatenation
#ok: python_eval_rule-eval
eval("1"+plus+"1")

# format string with single argument
#ok: python_eval_rule-eval
eval(f"1{plus}1")

# raw format string with single argument
#ok: python_eval_rule-eval
eval(rf"1{plus}1")

# format string with multiple arguments
#ok: python_eval_rule-eval
eval(f"1{plus}{one}")

# single string interpolation
#known false positive, semgrep limitations in understanding of Python string interpolations
#ok: python_eval_rule-eval
eval("1%s"%"+1")

# multiple string interpolation
#known false positive, semgrep limitations in understanding of Python string interpolations
#ok: python_eval_rule-eval
eval( "1 %s %s" % ("+", "1") )


# string concatenation with casted integer constant
#known false positive, semgrep limitations in understanding of Python string constants
#ok: python_eval_rule-eval
#eval("1+"+str(1))
#TODO: enable once SemGrep's String Constant Propagation improves.

# string literal and standard library call
#ok: python_eval_rule-eval
print(eval("os.getcwd()"))

# format string literal and standard library call
#ok: python_eval_rule-eval
print(eval(f"os.getcwd()"))

# format string literal and standard library call
#ok: python_eval_rule-eval
print(eval(r"os.getcwd()"))

# triple-quote string literal and standard library call
#ok: python_eval_rule-eval
print(eval("""os.getcwd()"""))

#following call evaluates a hard-coded string which results in a predetermined behavior and a harmless output
#ok: python_eval_rule-eval
content = eval("__builtins__.open('eval.input','r').read()")
print(content)

print(
    #following call, as opposed to the previous one, evaluates a string with known procedence but unknown content and thus exhibits a non-deterministic, externally-controlled behavior
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
)

print(
    #this call evaluates a hard-coded expression which is parametrized using str.format and externally provided input 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
)

print(
    #this call evaluates a hard-coded expression which is parametrized using string interpolation and externally provided input 
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
)

#ok: python_eval_rule-eval
eval("os.chmod('%s', 0o666)" % 'eval.input')

# User-defined functions and methods named "eval" should not trigger a finding.

class Foo(object):

    #ok: python_eval_rule-eval 
    def eval(self):
        print("User-defined eval method")

    def foo(self):
        #ok: python_eval_rule-eval 
        self.eval()

class Bar(object):
    def bar(self,input):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

        def eval(input):
             print("User-defined eval method")
        #ok: python_eval_rule-eval
        eval(input)

#ok: python_eval_rule-eval 
Foo().eval()

class MyClass:
    def __init__():
          return
    def eval(input):
          print('harmless')
    
#ruleid: python_eval_rule-eval 
eval(content)

# call without arguments, superfluous but not dangerous.
#ok: python_eval_rule-eval 
eval()

#ok: python_eval_rule-eval 
def eval(input):
	print("Harmless user-defined eval function")

#known false positive, requires reflection or ability to compare eval.__module__=='builtins'
#ok: python_eval_rule-eval 
eval(content)


# TEARDOWN

os.remove("eval.input")

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/yaml_load.py
# hash:  8eee173

import json
import yaml

def test_yaml_load():
    ystr = yaml.dump({'a': 1, 'b': 2, 'c': 3})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    yaml.dump(y)
    try:
        yaml.load(ystr, Loader=yaml.CSafeLoader)
        # ruleid: python_deserialization_rule-yaml-load
        yaml.load(ystr, Loader=yaml.Loader)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    except AttributeError:
        # CSafeLoader only exists if you build yaml with LibYAML
        y = yaml.load(ystr, Loader=yaml.SafeLoader)

def test_json_load():
    # no issue should be found
    j = json.load("{}")

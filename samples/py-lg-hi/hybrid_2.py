class PostgresqlLenientDatabaseSchemaEditor:
    """
    Class changes the behavior of the postgres database schema editor slightly. Normally
    when the field type is altered and one of the data columns doesn't have a value that
    can be casted to the new type, it fails. This class introduces the possibility to
    run a custom function, like REGEXP_REPLACE, to convert the data to the correct
    format. If the casting still fails the value will be set to null.
    """

    sql_alter_column_type = (
        "ALTER COLUMN %(column)s TYPE %(type)s%(collation)s "
        "USING pg_temp.try_cast(%(column)s::text)"
    )

    def __init__(
        self,
        *args,
        alter_column_prepare_old_value="",
        alter_column_prepare_new_value="",
        force_alter_column=False,
        **kwargs,
    ):
        self.alter_column_prepare_old_value = alter_column_prepare_old_value
        self.alter_column_prepare_new_value = alter_column_prepare_new_value
        self.force_alter_column = force_alter_column
        super().__init__(*args, **kwargs)

    def _alter_field(
        self,
        model,
        old_field,
        new_field,
        old_type,
        new_type,
        old_db_params,
        new_db_params,
        strict=False,
    ):
        if self.force_alter_column:
            old_type = f"{old_type}_forced"

        if old_type != new_type:
            variables = {}
            if isinstance(self.alter_column_prepare_old_value, tuple):
                alter_column_prepare_old_value, v = self.alter_column_prepare_old_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_old_value = self.alter_column_prepare_old_value

            if isinstance(self.alter_column_prepare_new_value, tuple):
                alter_column_prepare_new_value, v = self.alter_column_prepare_new_value
                variables = {**variables, **v}
            else:
                alter_column_prepare_new_value = self.alter_column_prepare_new_value

            quoted_column_name = self.quote_name(new_field.column)
            for key, value in variables.items():
                variables[key] = value.replace("$FUNCTION$", "")
            self.execute(sql_drop_try_cast)
            self.execute(
                sql_create_try_cast
                % {
                    "column": quoted_column_name,
                    "type": new_type,
                    "alter_column_prepare_old_value": alter_column_prepare_old_value,
                    "alter_column_prepare_new_value": alter_column_prepare_new_value,
                },
                variables,
            )

        return super()._alter_field(
            model,
            old_field,
            new_field,
            old_type,
            new_type,
            old_db_params,
            new_db_params,
            strict,
        )

    def _alter_column_type_sql(
        self, model, old_field, new_field, new_type, old_collation, new_collation
    ):
        # Cast when data type changed.
        # Make ALTER TYPE with SERIAL make sense.
        table = strip_quotes(model._meta.db_table)
        serial_fields_map = {
            "bigserial": "bigint",
            "serial": "integer",
            "smallserial": "smallint",
        }
        if collate_sql := self._collate_sql(
            new_collation, old_collation, model._meta.db_table
        ):
            collate_sql = f" {collate_sql}"
        else:
            collate_sql = ""
        if new_type.lower() in serial_fields_map:
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            return (
                (
                    self.sql_alter_column_type
                    % {
                        "column": self.quote_name(column),
                        "type": serial_fields_map[new_type.lower()],
                        "collation": collate_sql,
                    },
                    [],
                ),
                [
                    (
                        self.sql_delete_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_create_sequence
                        % {
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_alter_column
                        % {
                            "table": self.quote_name(table),
                            "changes": self.sql_alter_column_default
                            % {
                                "column": self.quote_name(column),
                                "default": "nextval('%s')"
                                % self.quote_name(sequence_name),
                            },
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_max
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                    (
                        self.sql_set_sequence_owner
                        % {
                            "table": self.quote_name(table),
                            "column": self.quote_name(column),
                            "sequence": self.quote_name(sequence_name),
                        },
                        [],
                    ),
                ],
            )
        elif (
            old_field.db_parameters(connection=self.connection)["type"]
            in serial_fields_map
        ):
            # Drop the sequence if migrating away from AutoField.
            column = strip_quotes(new_field.column)
            sequence_name = "%s_%s_seq" % (table, column)
            fragment, _ = BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )
            return fragment, [
                (
                    self.sql_delete_sequence
                    % {
                        "sequence": self.quote_name(sequence_name),
                    },
                    [],
                ),
            ]
        else:
            return BaseDatabaseSchemaEditor._alter_column_type_sql(
                self,
                model,
                old_field,
                new_field,
                new_type,
                old_collation,
                new_collation,
            )

    def _field_should_be_altered(self, old_field, new_field):
        return self.force_alter_column or super()._field_should_be_altered(
            old_field, new_field
        )

class UserSourceJSONWebTokenAuthentication(JWTAuthentication):
    """
    Authentication middleware that allow user source users to authenticate. All the
    authentication logic is delegated to the user source used to generate the token.
    """

    def __init__(
        self, use_user_source_authentication_header: bool = False, *args, **kwargs
    ):
        """
        :param use_user_source_authentication_header: Set to True if you want to
          authentication using a special `settings.USER_SOURCE_AUTHENTICATION_HEADER`
          header. This is useful when you want to keep the main authentication in the
          `Authorization` header but still want a "double" authentication with a user
          source user.
        """

        self.use_user_source_authentication_header = (
            use_user_source_authentication_header
        )
        super().__init__(*args, **kwargs)

    def get_header(self, request):
        if self.use_user_source_authentication_header:
            # Instead of reading the usual `Authorization` header we use a custom
            # header to authenticate the user source user.
            header = request.headers.get(
                settings.USER_SOURCE_AUTHENTICATION_HEADER, None
            )
            if isinstance(header, str):
                # Work around django test client oddness
                header = header.encode(HTTP_HEADER_ENCODING)

            return header
        else:
            return super().get_header(request)

    def authenticate(self, request: Request) -> Optional[Tuple[AuthUser, Token]]:
        """
        This method authenticate a user source user if the token has been generated
        by a user source.

        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication. Otherwise returns `None`.
        """

        header = self.get_header(request)
        if header is None:
            return None

        raw_token = self.get_raw_token(header)
        if raw_token is None:
            return None

        try:
            validated_token = UserSourceAccessToken(raw_token)
            user_source_uid = validated_token[USER_SOURCE_CLAIM]
            user_id = validated_token[jwt_settings.USER_ID_CLAIM]
        except (TokenError, KeyError):
            # Probably not a user source token let's skip this auth backend
            return None

        try:
            if self.use_user_source_authentication_header:
                # Using service here due to the "double authentication" situation.
                # This call has been triggered by the user source middleware
                # following initial DRF authentication.
                # The service ensures the authenticated user with the right
                # permissions regarding the user_source.
                user_source = UserSourceService().get_user_source_by_uid(
                    getattr(request, "user", AnonymousUser()),
                    user_source_uid,
                    for_authentication=True,
                )
            else:
                user_source = UserSourceHandler().get_user_source_by_uid(
                    user_source_uid,
                )
        except UserSourceDoesNotExist as exc:
            raise exceptions.AuthenticationFailed(
                detail={
                    "detail": "The user source does not exist",
                    "error": "user_source_does_not_exist",
                },
                code="user_source_does_not_exist",
            ) from exc
        except (PermissionDenied, UserNotInWorkspace) as exc:
            raise exceptions.AuthenticationFailed(
                detail={
                    "detail": (
                        "You are not allowed to use this user source to authenticate"
                    ),
                    "error": "user_source_not_allowed",
                },
                code="user_source_not_allowed",
            ) from exc

        try:
            user = user_source.get_type().get_user(user_source, user_id=user_id)
        except UserNotFound as exc:
            raise exceptions.AuthenticationFailed(
                detail={"detail": "User not found", "error": "user_not_found"},
                code="user_not_found",
            ) from exc

        if not self.use_user_source_authentication_header:
            try:
                CoreHandler().check_permissions(
                    user,
                    AuthenticateUserSourceOperationType.type,
                    workspace=user_source.application.workspace,
                    context=user_source,
                )
            except (PermissionDenied, UserNotInWorkspace) as exc:
                raise exceptions.AuthenticationFailed(
                    detail={
                        "detail": (
                            "You are not allowed to use this user source to "
                            "authenticate"
                        ),
                        "error": "user_source_not_allowed",
                    },
                    code="user_source_not_allowed",
                ) from exc

        return (
            user,
            validated_token,
        )

class TokenPermissionsField(serializers.Field):
    default_error_messages = {
        "invalid_key": "Only create, read, update and delete keys are allowed.",
        "invalid_value": (
            "The value must either be a bool, or a list containing database or table "
            'ids like [["database", 1], ["table", 1]].'
        ),
        "invalid_instance_type": "The instance type can only be a database or table.",
        "invalid_table_id": "The table id {instance_id} is not valid.",
        "invalid_database_id": "The database id {instance_id} is not valid.",
    }
    valid_types = ["create", "read", "update", "delete"]

    def __init__(self, **kwargs):
        kwargs["source"] = "*"
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        """
        Validates if the provided data structure is correct and replaces the database
        and table references with selected instances. The following data format is
        expected:

        {
            "create": True,
            "read": [['database', ID], ['table', ID]],
            "update": False,
            "delete": True
        }

        which will be converted to:

        {
            "create": True,
            "read": [Database(ID), Table(ID)],
            "update": False,
            "delete": True
        }

        :param data: The non validated permissions containing references to the
            database and tables.
        :type data: dict
        :return: The validated permissions with objects instead of references.
        :rtype: dict
        """

        tables = {}
        databases = {}

        if not isinstance(data, dict) or len(data) != len(self.valid_types):
            self.fail("invalid_key")

        for key, value in data.items():
            if key not in self.valid_types:
                self.fail("invalid_key")

            if not isinstance(value, bool) and not isinstance(value, list):
                self.fail("invalid_value")

            if isinstance(value, list):
                for instance in value:
                    if (
                        not isinstance(instance, list)
                        or not len(instance) == 2
                        or not isinstance(instance[0], str)
                        or not isinstance(instance[1], int)
                    ):
                        self.fail("invalid_value")

                    instance_type, instance_id = instance
                    if instance_type == "database":
                        databases[instance_id] = None
                    elif instance_type == "table":
                        tables[instance_id] = None
                    else:
                        self.fail("invalid_instance_type")

        if len(tables) > 0:
            tables = {
                table.id: table
                for table in Table.objects.filter(id__in=tables.keys()).select_related(
                    "database"
                )
            }

        if len(databases) > 0:
            databases = {
                database.id: database
                for database in Database.objects.filter(id__in=databases.keys())
            }

        for key, value in data.items():
            if isinstance(value, list):
                for index, (instance_type, instance_id) in enumerate(value):
                    if instance_type == "database":
                        if instance_id not in databases:
                            self.fail("invalid_database_id", instance_id=instance_id)
                        data[key][index] = databases[instance_id]
                    elif instance_type == "table":
                        if instance_id not in tables:
                            self.fail("invalid_table_id", instance_id=instance_id)
                        data[key][index] = tables[instance_id]

        return {
            "create": data["create"],
            "read": data["read"],
            "update": data["update"],
            "delete": data["delete"],
        }

    def to_representation(self, value):
        """
        If the provided value is a Token instance we need to fetch the permissions from
        the database and format them the correct way.

        If the provided value is a dict it means the permissions have already been
        provided and validated once, so we can just return that value. The variant is
        used when we want to validate the input.

        :param value: The prepared value that needs to be serialized.
        :type value: Token or dict
        :return: A dict containing the create, read, update and delete permissions
        :rtype: dict
        """

        if isinstance(value, Token):
            permissions = {
                "create": False,
                "read": False,
                "update": False,
                "delete": False,
            }

            for permission in value.tokenpermission_set.all():
                if permissions[permission.type] is True:
                    continue

                if permission.database_id is None and permission.table_id is None:
                    permissions[permission.type] = True
                else:
                    if not isinstance(permissions[permission.type], list):
                        permissions[permission.type] = []
                    if permission.database_id is not None:
                        permissions[permission.type].append(
                            ("database", permission.database_id)
                        )
                    elif permission.table_id is not None:
                        permissions[permission.type].append(
                            ("table", permission.table_id)
                        )

            return permissions
        else:
            permissions = {}
            for type_name in self.valid_types:
                if type_name not in value:
                    return None
                permissions[type_name] = value[type_name]
            return permissions

class PublicViewInfoView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="slug",
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                required=True,
                description="The slug of the view to get public information " "about.",
            )
        ],
        tags=["Database table views"],
        operation_id="get_public_view_info",
        description=(
            "Returns the required public information to display a single "
            "shared view."
        ),
        request=None,
        responses={
            200: PublicViewInfoSerializer,
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
            401: get_error_schema(["ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW"]),
            404: get_error_schema(["ERROR_VIEW_DOES_NOT_EXIST"]),
        },
    )
    @map_exceptions(
        {
            UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP,
            ViewDoesNotExist: ERROR_VIEW_DOES_NOT_EXIST,
            NoAuthorizationToPubliclySharedView: ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW,
        }
    )
    @transaction.atomic
    def get(self, request: Request, slug: str) -> Response:
        handler = ViewHandler()
        view = handler.get_public_view_by_slug(
            request.user,
            slug,
            authorization_token=get_public_view_authorization_token(request),
        )
        view_specific = view.specific
        view_type = view_type_registry.get_by_model(view_specific)

        if not view_type.has_public_info:
            raise ViewDoesNotExist()

        field_options = view_type.get_visible_field_options_in_order(view_specific)
        fields = specific_iterator(
            Field.objects.filter(id__in=field_options.values_list("field_id"))
            .select_related("content_type")
            .prefetch_related("select_options")
        )

        return Response(
            PublicViewInfoSerializer(
                view=view,
                fields=fields,
            ).data
        )


import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
    # ruleid: python_flask_rule-path-traversal-open
    return open(param, 'r').read()


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# ruleid: python_sql_rule-hardcoded-sql-expression
db.execute(s1)

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# ruleid: python_sql_rule-hardcoded-sql-expression
db.execute(s2)

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/binding.py
# hash:  8eee173

import socket

# ruleid: python_bind-all-interfaces_rule-general-bindall-interfaces
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('0.0.0.0', 31137))
s.bind(('192.168.0.1', 8080))

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data12 = UsmUserData(username, authKey=auth_key, privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,1))
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data9 = UsmUserData(username, authKey=auth_key, authProtocol=usmNoAuthProtocol)
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
import flask
import json

app = flask.Flask(__name__)


@app.route("/route_param/<route_param>")
def route_param(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_ok/<route_param>")
def route_param_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    return open("this is safe", 'r').read()


@app.route("/route_param_with/<route_param>")
def route_param_with(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/route_param_with_ok/<route_param>")
def route_param_with_ok(route_param):
    print("blah")
    # ok: python_flask_rule-path-traversal-open
    with open("this is safe", 'r') as fout:
        return fout.read()


@app.route("/route_param_with_concat/<route_param>")
def route_param_with_concat(route_param):
    print("blah")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param", methods=["GET"])
def get_param():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    f.write("hello world")


@app.route("/get_param_inline_concat", methods=["GET"])
def get_param_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_concat", methods=["GET"])
def get_param_concat():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_format", methods=["GET"])
def get_param_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/get_param_percent_format", methods=["GET"])
def get_param_percent_format():
    param = flask.request.args.get("param")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/post_param", methods=["POST"])
def post_param():
    param = flask.request.form['param']
    if True:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline():
    # ruleid: python_flask_rule-path-traversal-open
    with open(flask.request.form['param'], 'r') as fin:
        data = json.load(fin)
    return data


@app.route("/post_param", methods=["POST"])
def post_param_with_inline_concat():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return data


@app.route("/subexpression", methods=["POST"])
def subexpression():
    param = "{}".format(flask.request.form['param'])
    print("do things")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/ok")
def ok():
    # ok: python_flask_rule-path-traversal-open
    open("static/path.txt", 'r')

# License: MIT (c) GitLab Inc.
import sys
import tarfile
import tempfile


def unsafe_archive_handler(filename):
    tar = tarfile.open(filename)
    # ruleid: python_files_rule-tarfile-unsafe-members
    tar.extractall(path=tempfile.mkdtemp())
    tar.close()


def managed_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def list_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def provided_members_archive_handler(filename):
    tar = tarfile.open(filename)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    tar.close()


def members_filter(tarfile):
    result = []
    for member in tarfile.getmembers():
        if '../' in member.name:
            print('Member name container directory traversal sequence')
            continue
        elif (member.issym() or member.islnk()) and ('../' in member.linkname):
            print('Symlink to external resource')
            continue
        result.append(member)
    return result


if __name__ == "__main__":
    if len(sys.argv) > 1:
        filename = sys.argv[1]
        unsafe_archive_handler(filename)
        managed_members_archive_handler(filename)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/yaml_load.py
# hash:  8eee173

import json
import yaml

def test_yaml_load():
    ystr = yaml.dump({'a': 1, 'b': 2, 'c': 3})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    yaml.dump(y)
    try:
        yaml.load(ystr, Loader=yaml.CSafeLoader)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ruleid: python_deserialization_rule-yaml-load
        yaml.load_all(ystr, Loader=yaml.UnsafeLoader)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    except AttributeError:
        # CSafeLoader only exists if you build yaml with LibYAML
        y = yaml.load(ystr, Loader=yaml.SafeLoader)

def test_json_load():
    # no issue should be found
    j = json.load("{}")

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_minidom.py
# hash:  8eee173

from xml.dom.minidom import parseString as badParseString
from defusedxml.minidom import parseString as goodParseString

from xml.dom.minidom import parse as badParse
from defusedxml.minidom import parse as goodParse

def foo(str):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  print(a)
  # ok: python_xml_rule-minidom
  b = goodParseString(str)
  print(b)

  # ruleid: python_xml_rule-minidom
  a = badParse(str)
  print(a)
  # ok: python_xml_rule-minidom
  b = goodParse(str)
  print(b)

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_requests_rule-request-without-timeout
requests.patch('https://gmail.com', timeout=None)
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# ruleid: python_requests_rule-request-without-timeout
requests.post('https://gmail.com')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/ciphers.py
# hash:  8eee173

from cryptography.hazmat.primitives.ciphers import Cipher
from cryptography.hazmat.primitives.ciphers import algorithms

# ruleid: python_crypto_rule-crypto-hazmat-cipher-arc4
cipher = Cipher(algorithms.ARC4(key), mode=None, backend=default_backend())
encryptor = cipher.encryptor()
ct = encryptor.update(b"a secret message")


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# ruleid: python_sql_rule-hardcoded-sql-expression
db.execute(s1)

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id = '" + bar + "'" + " AND somethingElse <= " + foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

from flask import Flask, request

app = Flask(__name__)
# Literal strings are ok
com = '''
import os
os.system('ls -l')
'''


@app.route('/exec', methods=['POST'])
def exec_route():
    user_input = request.form['input']

    # ruleid: python_exec_rule-exec-used
    exec(f"{user_input}")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route('/safe', methods=['GET'])
def exec_safe_route():
    text = 'exec is evil'
    com = f"print('{text}')"

    # rule ok:
    exec(com)

    # rule ok:
    exec(com)

    # rule ok:
    exec("2 + 3")

    # rule ok:
    exec("print('hello world!')")

    return "no response"


if __name__ == '__main__':
    app.run(debug=True)

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/django_sql_injection_extra.py
# hash:  8eee173

from django.contrib.auth.models import User

User.objects.filter(username='admin').extra(
    select={'test': 'secure'},
    where=['secure'],
    tables=['secure']
)
User.objects.filter(username='admin').extra({'test': 'secure'})
User.objects.filter(username='admin').extra(select={'test': 'secure'})
User.objects.filter(username='admin').extra(where=['secure'])

User.objects.filter(username='admin').extra(dict(could_be='insecure'))
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
query = '"username") AS "username", * FROM "auth_user" WHERE 1=1 OR "username"=? --'
User.objects.filter(username='admin').extra(select={'test': query})
User.objects.filter(username='admin').extra(select={'test': '%secure' % 'nos'})
User.objects.filter(username='admin').extra(select={'test': '{}secure'.format('nos')})

where_var = ['1=1) OR 1=1 AND (1=1']
User.objects.filter(username='admin').extra(where=where_var)
# ruleid: python_django_rule-django-extra-used
User.objects.filter(username='admin').extra(where=userinput)
where_str = '1=1) OR 1=1 AND (1=1'
User.objects.filter(username='admin').extra(where=[where_str])
User.objects.filter(username='admin').extra(where=['%secure' % 'nos'])
User.objects.filter(username='admin').extra(where=['{}secure'.format('no')])

tables_var = ['django_content_type" WHERE "auth_user"."username"="admin']
User.objects.all().extra(tables=tables_var).distinct()
tables_str = 'django_content_type" WHERE "auth_user"."username"="admin'
User.objects.all().extra(tables=[tables_str]).distinct()

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/crypto-md5.py
# hash:  8eee173

from Crypto.Hash import MD4 as pycrypto_md4
from Cryptodome.Hash import MD4 as pycryptodomex_md4

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: python_crypto_rule-hash-md4
pycryptodomex_md4.new()

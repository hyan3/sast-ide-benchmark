class ChoiceElementType(FormElementTypeMixin, ElementType):
    type = "choice"
    model_class = ChoiceElement
    allowed_fields = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    request_serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    simple_formula_fields = [
        "label",
        "default_value",
        "placeholder",
        "formula_value",
        "formula_name",
    ]

    class SerializedDict(ElementDict):
        label: BaserowFormula
        required: bool
        placeholder: BaserowFormula
        default_value: BaserowFormula
        options: List
        multiple: bool
        show_as_dropdown: bool
        option_type: str
        formula_value: BaserowFormula
        formula_name: BaserowFormula

    @property
    def serializer_field_overrides(self):
        from baserow.contrib.builder.api.theme.serializers import (
            DynamicConfigBlockSerializer,
        )
        from baserow.contrib.builder.theme.theme_config_block_types import (
            InputThemeConfigBlockType,
        )
        from baserow.core.formula.serializers import FormulaSerializerField

        overrides = {
            "label": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("label").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "default_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("default_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "required": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("required").help_text,
                default=False,
                required=False,
            ),
            "placeholder": serializers.CharField(
                help_text=ChoiceElement._meta.get_field("placeholder").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "options": ChoiceOptionSerializer(
                source="choiceelementoption_set", many=True, required=False
            ),
            "multiple": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("multiple").help_text,
                default=False,
                required=False,
            ),
            "show_as_dropdown": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("show_as_dropdown").help_text,
                default=True,
                required=False,
            ),
            "option_type": serializers.ChoiceField(
                choices=ChoiceElement.OPTION_TYPE.choices,
                help_text=ChoiceElement._meta.get_field("option_type").help_text,
                required=False,
                default=ChoiceElement.OPTION_TYPE.MANUAL,
            ),
            "formula_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "formula_name": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_name").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "styles": DynamicConfigBlockSerializer(
                required=False,
                property_name="input",
                theme_config_block_type_name=InputThemeConfigBlockType.type,
                serializer_kwargs={"required": False},
            ),
        }

        return overrides

    @property
    def request_serializer_field_overrides(self):
        return {
            **self.serializer_field_overrides,
            "options": ChoiceOptionSerializer(many=True, required=False),
        }

    def serialize_property(
        self,
        element: ChoiceElement,
        prop_name: str,
        files_zip=None,
        storage=None,
        cache=None,
    ):
        if prop_name == "options":
            return [
                self.serialize_option(option)
                for option in element.choiceelementoption_set.all()
            ]

        return super().serialize_property(
            element, prop_name, files_zip=files_zip, storage=storage, cache=cache
        )

    def import_serialized(
        self,
        parent: Any,
        serialized_values: Dict[str, Any],
        id_mapping: Dict[str, Dict[int, int]],
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        choice_element = super().import_serialized(
            parent,
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

        options = []
        for option in serialized_values.get("options", []):
            option["choice_id"] = choice_element.id
            option_deserialized = self.deserialize_option(option)
            options.append(option_deserialized)

        ChoiceElementOption.objects.bulk_create(options)

        return choice_element

    def create_instance_from_serialized(
        self,
        serialized_values: Dict[str, Any],
        id_mapping,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        serialized_values.pop("options", None)
        return super().create_instance_from_serialized(
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def serialize_option(self, option: ChoiceElementOption) -> Dict:
        return {
            "value": option.value,
            "name": option.name,
            "choice_id": option.choice_id,
        }

    def deserialize_option(self, value: Dict):
        return ChoiceElementOption(**value)

    def get_pytest_params(self, pytest_data_fixture) -> Dict[str, Any]:
        return {
            "label": "'test'",
            "default_value": "'option 1'",
            "required": False,
            "placeholder": "'some placeholder'",
            "multiple": False,
            "show_as_dropdown": True,
            "option_type": ChoiceElement.OPTION_TYPE.MANUAL,
            "formula_value": "",
            "formula_name": "",
        }

    def after_create(self, instance: ChoiceElement, values: Dict):
        options = values.get("options", [])

        ChoiceElementOption.objects.bulk_create(
            [ChoiceElementOption(choice=instance, **option) for option in options]
        )

    def after_update(
        self, instance: ChoiceElement, values: Dict, changes: Dict[str, Tuple]
    ):
        options = values.get("options", None)

        if options is not None:
            ChoiceElementOption.objects.filter(choice=instance).delete()
            ChoiceElementOption.objects.bulk_create(
                [ChoiceElementOption(choice=instance, **option) for option in options]
            )

    def is_valid(
        self,
        element: ChoiceElement,
        value: Union[List, str],
        dispatch_context: DispatchContext,
    ) -> str:
        """
        Responsible for validating `ChoiceElement` form data. We handle
        this validation a little differently to ensure that if someone creates
        an option with a blank value, it's considered valid.

        :param element: The choice element.
        :param value: The choice value we want to validate.
        :return: The value if it is valid for this element.
        """

        options_tuple = set(
            element.choiceelementoption_set.values_list("value", "name")
        )
        options = [
            value if value is not None else name for (value, name) in options_tuple
        ]

        if element.option_type == ChoiceElement.OPTION_TYPE.FORMULAS:
            options = ensure_array(
                resolve_formula(
                    element.formula_value,
                    formula_runtime_function_registry,
                    dispatch_context,
                )
            )
            options = [ensure_string_or_integer(option) for option in options]

        if element.multiple:
            try:
                value = ensure_array(value)
            except ValidationError as exc:
                raise FormDataProviderChunkInvalidException(
                    "The value must be an array or convertible to an array."
                ) from exc

            if not value:
                if element.required:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            else:
                for v in value:
                    if v not in options:
                        raise FormDataProviderChunkInvalidException(
                            f"{value} is not a valid option."
                        )
        else:
            if not value:
                if element.required and value not in options:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            elif value not in options:
                raise FormDataProviderChunkInvalidException(
                    f"{value} is not a valid option."
                )

        return value

class UserSourceJSONWebTokenAuthentication(JWTAuthentication):
    """
    Authentication middleware that allow user source users to authenticate. All the
    authentication logic is delegated to the user source used to generate the token.
    """

    def __init__(
        self, use_user_source_authentication_header: bool = False, *args, **kwargs
    ):
        """
        :param use_user_source_authentication_header: Set to True if you want to
          authentication using a special `settings.USER_SOURCE_AUTHENTICATION_HEADER`
          header. This is useful when you want to keep the main authentication in the
          `Authorization` header but still want a "double" authentication with a user
          source user.
        """

        self.use_user_source_authentication_header = (
            use_user_source_authentication_header
        )
        super().__init__(*args, **kwargs)

    def get_header(self, request):
        if self.use_user_source_authentication_header:
            # Instead of reading the usual `Authorization` header we use a custom
            # header to authenticate the user source user.
            header = request.headers.get(
                settings.USER_SOURCE_AUTHENTICATION_HEADER, None
            )
            if isinstance(header, str):
                # Work around django test client oddness
                header = header.encode(HTTP_HEADER_ENCODING)

            return header
        else:
            return super().get_header(request)

    def authenticate(self, request: Request) -> Optional[Tuple[AuthUser, Token]]:
        """
        This method authenticate a user source user if the token has been generated
        by a user source.

        Returns a two-tuple of `User` and token if a valid signature has been
        supplied using JWT-based authentication. Otherwise returns `None`.
        """

        header = self.get_header(request)
        if header is None:
            return None

        raw_token = self.get_raw_token(header)
        if raw_token is None:
            return None

        try:
            validated_token = UserSourceAccessToken(raw_token)
            user_source_uid = validated_token[USER_SOURCE_CLAIM]
            user_id = validated_token[jwt_settings.USER_ID_CLAIM]
        except (TokenError, KeyError):
            # Probably not a user source token let's skip this auth backend
            return None

        try:
            if self.use_user_source_authentication_header:
                # Using service here due to the "double authentication" situation.
                # This call has been triggered by the user source middleware
                # following initial DRF authentication.
                # The service ensures the authenticated user with the right
                # permissions regarding the user_source.
                user_source = UserSourceService().get_user_source_by_uid(
                    getattr(request, "user", AnonymousUser()),
                    user_source_uid,
                    for_authentication=True,
                )
            else:
                user_source = UserSourceHandler().get_user_source_by_uid(
                    user_source_uid,
                )
        except UserSourceDoesNotExist as exc:
            raise exceptions.AuthenticationFailed(
                detail={
                    "detail": "The user source does not exist",
                    "error": "user_source_does_not_exist",
                },
                code="user_source_does_not_exist",
            ) from exc
        except (PermissionDenied, UserNotInWorkspace) as exc:
            raise exceptions.AuthenticationFailed(
                detail={
                    "detail": (
                        "You are not allowed to use this user source to authenticate"
                    ),
                    "error": "user_source_not_allowed",
                },
                code="user_source_not_allowed",
            ) from exc

        try:
            user = user_source.get_type().get_user(user_source, user_id=user_id)
        except UserNotFound as exc:
            raise exceptions.AuthenticationFailed(
                detail={"detail": "User not found", "error": "user_not_found"},
                code="user_not_found",
            ) from exc

        if not self.use_user_source_authentication_header:
            try:
                CoreHandler().check_permissions(
                    user,
                    AuthenticateUserSourceOperationType.type,
                    workspace=user_source.application.workspace,
                    context=user_source,
                )
            except (PermissionDenied, UserNotInWorkspace) as exc:
                raise exceptions.AuthenticationFailed(
                    detail={
                        "detail": (
                            "You are not allowed to use this user source to "
                            "authenticate"
                        ),
                        "error": "user_source_not_allowed",
                    },
                    code="user_source_not_allowed",
                ) from exc

        return (
            user,
            validated_token,
        )

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0036_collectionfield_link_variant"),
    ]

    operations = [
        migrations.CreateModel(
            name="RecordSelectorElement",
            fields=[
                (
                    "element_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="builder.element",
                    ),
                ),
                (
                    "required",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this form element is a required field.",
                    ),
                ),
                (
                    "schema_property",
                    models.CharField(
                        help_text="A multiple valued schema property to use for the data source.",
                        max_length=225,
                        null=True,
                    ),
                ),
                (
                    "items_per_page",
                    models.PositiveIntegerField(
                        default=20,
                        help_text="The amount item loaded with each page.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                1, message="Value cannot be less than 1."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "button_load_more_label",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The label of the show more button",
                    ),
                ),
                (
                    "label",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="The text label for this record selector"
                    ),
                ),
                (
                    "default_value",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="This record selector default value."
                    ),
                ),
                (
                    "placeholder",
                    baserow.core.formula.field.FormulaField(
                        default="",
                        help_text="The placeholder text which should be applied to the element.",
                    ),
                ),
                (
                    "multiple",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this record selector allows users to choose multiple values.",
                    ),
                ),
                (
                    "option_name_suffix",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The formula to generate the displayed option name suffix",
                    ),
                ),
                (
                    "data_source",
                    models.ForeignKey(
                        help_text="The data source we want to show in the element for. Only data_sources that return list are allowed.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="builder.datasource",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("builder.element",),
        ),
    ]


import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
    #ruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % request.args.get('param')))
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'" % identifier)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/subprocess_shell.py
# hash:  8eee173

import subprocess
from subprocess import Popen as pop

def Popen(*args, **kwargs):
    print('hi')

    def __len__(self):
        return 0

pop('/bin/gcc --version', shell=True)
# ruleid: python_exec_rule-subprocess-shell-TRUE
Popen('/bin/gcc --version', shell=True)

subprocess.Popen('/bin/gcc --version', shell=True)
subprocess.Popen(['/bin/gcc', '--version'], shell=False)
subprocess.Popen(['/bin/gcc', '--version'])

subprocess.call(["/bin/ls",
                 "-l"
                 ])
subprocess.call('/bin/ls -l', shell=True)

subprocess.check_call(['/bin/ls', '-l'], shell=False)
subprocess.check_call('/bin/ls -l', shell=True)

subprocess.check_output(['/bin/ls', '-l'])
subprocess.check_output('/bin/ls -l', shell=True)

subprocess.run(['/bin/ls', '-l'])
subprocess.run('/bin/ls -l', shell=True)

subprocess.Popen('/bin/ls *', shell=True)
subprocess.Popen('/bin/ls %s' % ('something',), shell=True)
subprocess.Popen('/bin/ls {}'.format('something'), shell=True)

command = "/bin/ls" + unknown_function()
subprocess.Popen(command, shell=True)

subprocess.Popen('/bin/ls && cat /etc/passwd', shell=True)

command = 'pwd'
subprocess.call(command, shell='True')
subprocess.call(command, shell='False')
subprocess.call(command, shell='None')
subprocess.call(command, shell=1)

subprocess.call(command, shell=Popen())
subprocess.call(command, shell=[True])
subprocess.call(command, shell={'IS': 'True'})
subprocess.call(command, shell=command)

subprocess.call(command, shell=False)
subprocess.call(command, shell=0)
subprocess.call(command, shell=[])
subprocess.call(command, shell={})
subprocess.call(command, shell=None)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/xml_minidom.py
# hash:  8eee173

from xml.dom.minidom import parseString as badParseString
from defusedxml.minidom import parseString as goodParseString

from xml.dom.minidom import parse as badParse
from defusedxml.minidom import parse as goodParse

def foo(str):
  # ruleid: python_xml_rule-minidom
  a = badParseString(str)
  print(a)
  # ok: python_xml_rule-minidom
  b = goodParseString(str)
  print(b)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  print(a)
  # ok: python_xml_rule-minidom
  b = goodParse(str)
  print(b)

# License: MIT (c) GitLab Inc.

import subprocess
import os
from subprocess import Popen as pop

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-os-popen2
commands.getstatusoutput("")


#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/urlopen.py
# hash:  8eee173
import os
import ssl
import urllib
import urllib2

# Python 3
import urllib.request

# Six
import six

ssl._create_default_https_context = ssl._create_unverified_context

# Set up a sample file for tainted input
with open('a123.txt', 'w') as file1:
    file1.write("https://abc.com/")

with open('a123.txt', 'r') as file1:
    url_from_user = file1.read()
filename = 'abc.txt'
http_url = 'http://abc.com'

# urllib
url = urllib.quote('abc.com')
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen(http_url)
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlopen("http://abc.com")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.urlretrieve('http://abc.com', 'abc.txt')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

opener = urllib.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.FancyURLopener()
# ruleid: python_urlopen_rule-urllib-urlopen
opener.open(url_from_user)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# urllib2
handler = urllib2.HTTPBasicAuthHandler()
handler.add_password(realm='test', uri='http://mysite.com', user='bob', passwd='123')
opener = urllib2.build_opener(handler)
urllib2.install_opener(opener)
urllib2.urlopen('file:///bin/ls')
urllib2.urlopen(url_from_user)
urllib2.Request('file:///bin/ls')

# Python 3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlopen('file:///bin/ls')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
urllib.request.urlretrieve('file:///bin/ls',filename)

opener = urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Six
six.moves.urllib.request.urlopen('file:///bin/ls')
six.moves.urllib.request.urlretrieve('file:///bin/ls', 'abc3.txt')

opener = six.moves.urllib.request.URLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

opener = six.moves.urllib.request.FancyURLopener()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_urlopen_rule-urllib-urlopen
opener.open('file:///bin/ls')
# ok: python_urlopen_rule-urllib-urlopen
opener.retrieve('file:///bin/ls')

# Clean up
os.remove('a123.txt')
os.remove('abc.txt')
os.remove('abc2.txt')
os.remove('abc3.txt')

# License: MIT (c) GitLab Inc.

import subprocess
import os
from subprocess import Popen as pop

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-os-popen2
os.popen3("")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


# License: MIT (c) GitLab Inc.

import subprocess
import os
from subprocess import Popen as pop

# ruleid: python_exec_rule-os-popen2
os.system("")
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


# License: MIT (c) GitLab Inc.
# source: https://bandit.readthedocs.io/en/latest/_modules/bandit/plugins/snmp_security_check.html#snmp_insecure_version_check

from pysnmp.hlapi import CommunityData

# SHOULD FAIL
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_snmp_rule-insecure-snmp-version
b = CommunityData('public', mpModel=1)

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data8 = UsmUserData(username, authKey=None, authProtocol=usmNoAuthProtocol, privKey=None, privProtocol=usmNoPrivProtocol)
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/random_module.py
# hash:  8eee173

import random
import os
import somelib

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_random_rule-random
bad = random.uniform()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

good = os.urandom()
good = random.SystemRandom()

unknown = random()
unknown = somelib.a.random()

# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/sql_statements.py
# hash:  8eee173

import sqlalchemy

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("SELECT * FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("INSERT INTO foo VALUES ('a', 'b', '%s')", value)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM foo WHERE id = '%s'", identifier)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("UPDATE foo SET value = 'b' WHERE id = '%s'", identifier)

# real world false positives
choices=[('server_list', _("Select from active instances"))]
print("delete from the cache as the first argument")

def select_note_by_id(conn, id=None):
	query = "SELECT * FROM notes"
	if id:
		query = query + " WHERE id = '%s'" % id
	cur = conn.cursor()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
	rows = cur.fetchall()
	return rows

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("unrelated to select and sql" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
foo.execute("updated" % identifier)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('SELECT * FROM dual')

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute('CREATE TABLE %s' % tableName)

ctStm = 'CREATE TABLE '
ctStm = ctStm + ' %s' % tableName
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(ctStm)

upStm = 'UPDATE foo WHERE user = %s' % 'admin'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm)

upStm2 = f'UPDATE foo WHERE user = "admin"'
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(upStm2)

# ok: python_sql_rule-hardcoded-sql-expression
cur.execute(f'UPDATE foo WHERE user = "admin"')


s1 = "DELETE FROM tableX WHERE colY <= "
s1 += str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s2 = "DELETE FROM tableX WHERE colY <= " + str(response_data['delete_record'])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

s3 = "DELETE FROM tableX WHERE id <= %s" % "something constant"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s3)

s4 = "DELETE FROM tableX WHERE colY <= "
s4 += "delete_record"
# ok: python_sql_rule-hardcoded-sql-expression
db.execute(s4)

con = sqlite3.connect(filepath)
cur = con.cursor()
foo = getSomeVar()
bar = getAnotherVar()
bazConst = "this is a literal"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= " + "something constant")
# ruleid: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bar)
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % "something constant")
# ok: python_sql_rule-hardcoded-sql-expression
cur.execute("DELETE FROM tableX WHERE id <= %s" % bazConst)

# This is a false positive result since 3 is a constant.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data14 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,1))
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
        #ruleid: python_flask_rule-tainted-sql-string
        result = db.session.execute(text('SELECT * FROM user WHERE lastname=\"{}\"'.format(query)))

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data17 = UsmUserData(username, auth_key, authProtocol=usmHMACSHAAuthProtocol, securityEngineId=None, securityName=None)
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/mktemp.py
# hash:  8eee173

from tempfile import mktemp
import tempfile.mktemp as mt
import tempfile as tmp

foo = 'hi'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_tmpdir_rule-mktemp-q
tmp.mktemp(foo)

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# ruleid: python_requests_rule-request-without-timeout
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/mktemp.py
# hash:  8eee173

from tempfile import mktemp
import tempfile.mktemp as mt
import tempfile as tmp

foo = 'hi'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_tmpdir_rule-mktemp-q
tmp.mktemp(foo)

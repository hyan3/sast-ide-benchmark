@SuppressWarnings("MemberName")
class UiWatchers {
    private static final String LOG_TAG = UiWatchers.class.getSimpleName();
    private final List<String> mErrors = new ArrayList<>();

    /**
     * We can use the UiDevice registerWatcher to register a small script to be executed when the
     * framework is waiting for a control to appear. Waiting may be the cause of an unexpected
     * dialog on the screen and it is the time when the framework runs the registered watchers.
     * This is a sample watcher looking for ANR and crashes. it closes it and moves on. You should
     * create your own watchers and handle error logging properly for your type of tests.
     */
    void registerAnrAndCrashWatchers() {
        UiDevice.getInstance().registerWatcher("ANR", () -> {
            UiObject window = new UiObject(new UiSelector().className(
                    "com.android.server.am.AppNotRespondingDialog"));
            String errorText = null;
            if (window.exists()) {
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onAnrDetected(errorText);
                postHandler("Wait");
                return true; // triggered
            }
            return false; // no trigger
        });
        // class names may have changed
        UiDevice.getInstance().registerWatcher("ANR2", () -> {
            UiObject window = new UiObject(new UiSelector().packageName("android")
                    .textContains("isn't responding."));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onAnrDetected(errorText);
                postHandler("Wait");
                return true; // triggered
            }
            return false; // no trigger
        });
        UiDevice.getInstance().registerWatcher("CRASH", () -> {
            UiObject window = new UiObject(new UiSelector().className(
                    "com.android.server.am.AppErrorDialog"));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onCrashDetected(errorText);
                postHandler("OK");
                return true; // triggered
            }
            return false; // no trigger
        });
        UiDevice.getInstance().registerWatcher("CRASH2", () -> {
            UiObject window = new UiObject(new UiSelector().packageName("android")
                    .textContains("has stopped"));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onCrashDetected(errorText);
                postHandler("OK");
                return true; // triggered
            }
            return false; // no trigger
        });
        Log.i(LOG_TAG, "Registered GUI Exception watchers");
    }

    private void onAnrDetected(String errorText) {
        mErrors.add(errorText);
    }

    private void onCrashDetected(String errorText) {
        mErrors.add(errorText);
    }

    /**
     * Current implementation ignores the exception and continues.
     */
    private void postHandler(String buttonText) {
        // TODO: Add custom error logging here
        String formatedOutput = String.format("UI Exception Message: %-20s\n", UiDevice
                .getInstance().getCurrentPackageName());
        Log.e(LOG_TAG, formatedOutput);
        UiObject buttonOK = new UiObject(new UiSelector().text(buttonText).enabled(true));
        // sometimes it takes a while for the OK button to become enabled
        buttonOK.waitForExists(5000);
        try {
            buttonOK.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }
    }
}

public final class Languages {
    public static final String TAG = "Languages";

    public static final String USE_SYSTEM_DEFAULT = "";

    private static final Locale DEFAULT_LOCALE;
    private static final Locale TIBETAN = new Locale("bo");
    private static final Locale CHINESE_HONG_KONG = new Locale("zh", "HK");

    private static Locale locale;
    private static Languages singleton;
    private static final Map<String, String> TMP_MAP = new TreeMap<>();
    private static Map<String, String> nameMap;

    static {
        DEFAULT_LOCALE = Locale.getDefault();
    }

    private Languages(AppCompatActivity activity) {
        Set<Locale> localeSet = new LinkedHashSet<>(Arrays.asList(LOCALES_TO_TEST));

        for (Locale locale : localeSet) {
            if (locale.equals(TIBETAN)) {
                // include English name for devices without Tibetan font support
                TMP_MAP.put(TIBETAN.getLanguage(), "Tibetan བོད་སྐད།"); // Tibetan
            } else if (locale.equals(Locale.SIMPLIFIED_CHINESE)) {
                TMP_MAP.put(Locale.SIMPLIFIED_CHINESE.toString(), "中文 (中国)"); // Chinese (China)
            } else if (locale.equals(Locale.TRADITIONAL_CHINESE)) {
                TMP_MAP.put(Locale.TRADITIONAL_CHINESE.toString(), "中文 (台灣)"); // Chinese (Taiwan)
            } else if (locale.equals(CHINESE_HONG_KONG)) {
                TMP_MAP.put(CHINESE_HONG_KONG.toString(), "中文 (香港)"); // Chinese (Hong Kong)
            } else {
                TMP_MAP.put(locale.getLanguage(), capitalize(locale.getDisplayLanguage(locale)));
            }
        }

        // remove the current system language from the menu
        TMP_MAP.remove(Locale.getDefault().getLanguage());

        /* SYSTEM_DEFAULT is a fake one for displaying in a chooser menu. */
        TMP_MAP.put(USE_SYSTEM_DEFAULT, activity.getString(R.string.pref_language_default));
        nameMap = Collections.unmodifiableMap(TMP_MAP);
    }

    /**
     * @param activity the {@link AppCompatActivity} this is working as part of
     * @return the singleton to work with
     */
    public static Languages get(AppCompatActivity activity) {
        if (singleton == null) {
            singleton = new Languages(activity);
        }
        return singleton;
    }

    /**
     * Handles setting the language if it is different than the current language,
     * or different than the current system-wide locale.  The preference is cleared
     * if the language matches the system-wide locale or "System Default" is chosen.
     */
    public static void setLanguage(final ContextWrapper contextWrapper) {
        if (Build.VERSION.SDK_INT >= 24) {
            Utils.debugLog(TAG, "Languages.setLanguage() ignored on >= android-24");
            Preferences.get().clearLanguage();
            return;
        }
        String language = Preferences.get().getLanguage();
        if (TextUtils.equals(language, DEFAULT_LOCALE.getLanguage())) {
            Preferences.get().clearLanguage();
            locale = DEFAULT_LOCALE;
        } else if (locale != null && TextUtils.equals(locale.getLanguage(), language)) {
            return; // already configured
        } else if (language == null || language.equals(USE_SYSTEM_DEFAULT)) {
            Preferences.get().clearLanguage();
            locale = DEFAULT_LOCALE;
        } else {
            /* handle locales with the country in it, i.e. zh_CN, zh_TW, etc */
            String[] localeSplit = language.split("_");
            if (localeSplit.length > 1) {
                locale = new Locale(localeSplit[0], localeSplit[1]);
            } else {
                locale = new Locale(language);
            }
        }
        Locale.setDefault(locale);

        final Resources resources = contextWrapper.getBaseContext().getResources();
        Configuration config = resources.getConfiguration();
        config.setLocale(locale);
        resources.updateConfiguration(config, resources.getDisplayMetrics());
    }

    /**
     * Force reload the {@link AppCompatActivity to make language changes take effect.}
     *
     * @param activity the {@code AppCompatActivity} to force reload
     */
    public static void forceChangeLanguage(AppCompatActivity activity) {
        if (Build.VERSION.SDK_INT >= 24) {
            Utils.debugLog(TAG, "Languages.forceChangeLanguage() ignored on >= android-24");
            return;
        }
        Intent intent = activity.getIntent();
        if (intent == null) { // when launched as LAUNCHER
            return;
        }
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        activity.finish();
        activity.overridePendingTransition(0, 0);
        activity.startActivity(intent);
        activity.overridePendingTransition(0, 0);
    }

    /**
     * @return an array of the names of all the supported languages, sorted to
     * match what is returned by {@link Languages#getSupportedLocales()}.
     */
    public String[] getAllNames() {
        return nameMap.values().toArray(new String[0]);
    }

    /**
     * @return sorted list of supported locales.
     */
    public String[] getSupportedLocales() {
        Set<String> keys = nameMap.keySet();
        return keys.toArray(new String[0]);
    }

    private String capitalize(final String line) {
        return Character.toUpperCase(line.charAt(0)) + line.substring(1);
    }

    public static final Locale[] LOCALES_TO_TEST = {
            Locale.ENGLISH,
            Locale.FRENCH,
            Locale.GERMAN,
            Locale.ITALIAN,
            Locale.JAPANESE,
            Locale.KOREAN,
            Locale.SIMPLIFIED_CHINESE,
            Locale.TRADITIONAL_CHINESE,
            CHINESE_HONG_KONG,
            TIBETAN,
            new Locale("af"),
            new Locale("ar"),
            new Locale("be"),
            new Locale("bg"),
            new Locale("ca"),
            new Locale("cs"),
            new Locale("da"),
            new Locale("el"),
            new Locale("es"),
            new Locale("eo"),
            new Locale("et"),
            new Locale("eu"),
            new Locale("fa"),
            new Locale("fi"),
            new Locale("he"),
            new Locale("hi"),
            new Locale("hu"),
            new Locale("hy"),
            new Locale("id"),
            new Locale("is"),
            new Locale("it"),
            new Locale("ml"),
            new Locale("my"),
            new Locale("nb"),
            new Locale("nl"),
            new Locale("pl"),
            new Locale("pt"),
            new Locale("ro"),
            new Locale("ru"),
            new Locale("sc"),
            new Locale("sk"),
            new Locale("sn"),
            new Locale("sr"),
            new Locale("sv"),
            new Locale("th"),
            new Locale("tr"),
            new Locale("uk"),
            new Locale("vi"),
    };
}

public class AdvancedFragment extends BasePreferenceFragment {

    private static final String KEY_DUMP_LOGCAT = "dump_logcat";
    private static final String KEY_CLEAR_MEMORY_CACHE = "clear_memory_cache";
    private static final String KEY_APP_LANGUAGE = "app_language";
    private static final String KEY_IMPORT_DATA = "import_data";
    private static final String KEY_EXPORT_DATA = "export_data";
    private static final String KEY_OPEN_BY_DEFAULT = "open_by_default";

    ActivityResultLauncher<String> exportLauncher = registerForActivityResult(
            new ActivityResultContracts.CreateDocument(),
            uri -> {
                if (uri != null) {
                    try {
                        // grantUriPermission might throw RemoteException on MIUI
                        requireActivity().grantUriPermission(BuildConfig.APPLICATION_ID, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    } catch (Exception e) {
                        ExceptionUtils.throwIfFatal(e);
                        e.printStackTrace();
                    }
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(requireActivity())
                                .setCancelable(false)
                                .setView(R.layout.preference_dialog_task)
                                .show();
                        IoThreadPoolExecutor.getInstance().execute(() -> {
                            boolean success = EhDB.exportDB(requireActivity(), uri);
                            Activity activity = getActivity();
                            if (activity != null) {
                                activity.runOnUiThread(() -> {
                                    if (alertDialog.isShowing()) {
                                        alertDialog.dismiss();
                                    }
                                    showTip(
                                            (success)
                                                    ? GetText.getString(R.string.settings_advanced_export_data_to, uri.toString())
                                                    : GetText.getString(R.string.settings_advanced_export_data_failed),
                                            BaseScene.LENGTH_SHORT);
                                });
                            }
                        });
                    } catch (Exception e) {
                        showTip(R.string.settings_advanced_export_data_failed, BaseScene.LENGTH_SHORT);
                    }
                }
            });
    ActivityResultLauncher<String> dumpLogcatLauncher = registerForActivityResult(
            new ActivityResultContracts.CreateDocument(),
            uri -> {
                if (uri != null) {
                    try {
                        // grantUriPermission might throw RemoteException on MIUI
                        requireActivity().grantUriPermission(BuildConfig.APPLICATION_ID, uri, Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                    } catch (Exception e) {
                        ExceptionUtils.throwIfFatal(e);
                        e.printStackTrace();
                    }
                    try {
                        File zipFile = new File(AppConfig.getExternalTempDir(), "logs.zip");
                        if (zipFile.exists()) {
                            //noinspection ResultOfMethodCallIgnored
                            zipFile.delete();
                        }

                        ArrayList<File> files = new ArrayList<>();
                        files.addAll(Arrays.asList(AppConfig.getExternalParseErrorDir().listFiles()));
                        files.addAll(Arrays.asList(AppConfig.getExternalCrashDir().listFiles()));

                        boolean finished = false;

                        BufferedInputStream origin = null;
                        ZipOutputStream out = null;
                        try {
                            FileOutputStream dest = new FileOutputStream(zipFile);
                            out = new ZipOutputStream(new BufferedOutputStream(dest));
                            byte[] bytes = new byte[1024 * 64];

                            for (File file : files) {
                                if (!file.isFile()) {
                                    continue;
                                }
                                try {
                                    FileInputStream fi = new FileInputStream(file);
                                    origin = new BufferedInputStream(fi, bytes.length);

                                    ZipEntry entry = new ZipEntry(file.getName());
                                    out.putNextEntry(entry);
                                    int count;
                                    while ((count = origin.read(bytes, 0, bytes.length)) != -1) {
                                        out.write(bytes, 0, count);
                                    }
                                    origin.close();
                                    origin = null;
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            ZipEntry entry = new ZipEntry("logcat-" + ReadableTime.getFilenamableTime(System.currentTimeMillis()) + ".txt");
                            out.putNextEntry(entry);
                            LogCat.save(out);
                            out.closeEntry();
                            out.close();
                            IOUtils.copy(new FileInputStream(zipFile), requireActivity().getContentResolver().openOutputStream(uri));
                            finished = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            if (origin != null) {
                                origin.close();
                            }
                            if (out != null) {
                                out.close();
                            }
                        }
                        if (!finished) {
                            finished = LogCat.save(requireActivity().getContentResolver().openOutputStream(uri));
                        }
                        showTip(
                                finished ? getString(R.string.settings_advanced_dump_logcat_to, uri.toString()) :
                                        getString(R.string.settings_advanced_dump_logcat_failed), BaseScene.LENGTH_SHORT);
                    } catch (Exception e) {
                        showTip(getString(R.string.settings_advanced_dump_logcat_failed), BaseScene.LENGTH_SHORT);
                    }
                }
            });
    ActivityResultLauncher<String[]> importDataLauncher = registerForActivityResult(
            new ActivityResultContracts.OpenDocument(),
            uri -> {
                if (uri != null) {
                    try {
                        // grantUriPermission might throw RemoteException on MIUI
                        requireActivity().grantUriPermission(BuildConfig.APPLICATION_ID, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    } catch (Exception e) {
                        ExceptionUtils.throwIfFatal(e);
                        e.printStackTrace();
                    }
                    try {
                        AlertDialog alertDialog = new AlertDialog.Builder(requireActivity())
                                .setCancelable(false)
                                .setView(R.layout.preference_dialog_task)
                                .show();
                        IoThreadPoolExecutor.getInstance().execute(() -> {
                            final String error = EhDB.importDB(requireActivity(), uri);
                            Activity activity = getActivity();
                            if (activity != null) {
                                activity.runOnUiThread(() -> {
                                    if (alertDialog.isShowing()) {
                                        alertDialog.dismiss();
                                    }
                                    if (null == error) {
                                        showTip(getString(R.string.settings_advanced_import_data_successfully), BaseScene.LENGTH_SHORT);
                                    } else {
                                        showTip(error, BaseScene.LENGTH_SHORT);
                                    }
                                });
                            }

                        });
                    } catch (Exception e) {
                        showTip(e.getLocalizedMessage(), BaseScene.LENGTH_SHORT);
                    }
                }
            });

    @Override
    public void onCreatePreferencesFix(@Nullable Bundle savedInstanceState, String rootKey) {
        addPreferencesFromResource(R.xml.advanced_settings);

        Preference dumpLogcat = findPreference(KEY_DUMP_LOGCAT);
        Preference clearMemoryCache = findPreference(KEY_CLEAR_MEMORY_CACHE);
        Preference appLanguage = findPreference(KEY_APP_LANGUAGE);
        Preference importData = findPreference(KEY_IMPORT_DATA);
        Preference exportData = findPreference(KEY_EXPORT_DATA);
        Preference openByDefault = findPreference(KEY_OPEN_BY_DEFAULT);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.S) {
            openByDefault.setVisible(false);
        } else {
            openByDefault.setOnPreferenceClickListener(this);
        }

        dumpLogcat.setOnPreferenceClickListener(this);
        clearMemoryCache.setOnPreferenceClickListener(this);
        importData.setOnPreferenceClickListener(this);
        exportData.setOnPreferenceClickListener(this);

        appLanguage.setOnPreferenceChangeListener(this);
    }

    @Override
    public boolean onPreferenceClick(Preference preference) {
        String key = preference.getKey();
        if (KEY_DUMP_LOGCAT.equals(key)) {
            try {
                dumpLogcatLauncher.launch("log-" + ReadableTime.getFilenamableTime(System.currentTimeMillis()) + ".zip");
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                showTip(R.string.error_cant_find_activity, BaseScene.LENGTH_SHORT);
            }
            return true;
        } else if (KEY_CLEAR_MEMORY_CACHE.equals(key)) {
            ((EhApplication) requireContext().getApplicationContext()).clearMemoryCache();
            Runtime.getRuntime().gc();
            showTip(R.string.settings_advanced_clear_memory_cache_done, BaseScene.LENGTH_SHORT);
        } else if (KEY_IMPORT_DATA.equals(key)) {
            try {
                importDataLauncher.launch(new String[]{"*/*"});
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                showTip(R.string.error_cant_find_activity, BaseScene.LENGTH_SHORT);
            }
            return true;
        } else if (KEY_EXPORT_DATA.equals(key)) {
            try {
                exportLauncher.launch(ReadableTime.getFilenamableTime(System.currentTimeMillis()) + ".db");
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                showTip(R.string.error_cant_find_activity, BaseScene.LENGTH_SHORT);
            }
            return true;
        } else if (KEY_OPEN_BY_DEFAULT.equals(key)) {
            try {
                Intent intent = new Intent(android.provider.Settings.ACTION_APP_OPEN_BY_DEFAULT_SETTINGS,
                        Uri.parse("package:" + requireContext().getPackageName()));
                startActivity(intent);
            } catch (Throwable t) {
                Intent intent = new Intent(android.provider.Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                        Uri.parse("package:" + requireContext().getPackageName()));
                startActivity(intent);
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onPreferenceChange(Preference preference, Object newValue) {
        String key = preference.getKey();
        if (KEY_APP_LANGUAGE.equals(key)) {
            if ("system".equals(newValue)) {
                LocaleDelegate.setDefaultLocale(LocaleDelegate.getSystemLocale());
            } else {
                LocaleDelegate.setDefaultLocale(Locale.forLanguageTag((String) newValue));
            }
            requireActivity().recreate();
            return true;
        }
        return false;
    }

    @Override
    public int getFragmentTitle() {
        return R.string.settings_advanced;
    }
}

class Pipe {

    private final int capacity;
    private final byte[] buffer;

    private int head = 0;
    private int tail = 0;
    private boolean full = false;

    private boolean inClosed = false;
    private boolean outClosed = false;

    private final InputStream inputStream = new InputStream() {
        @Override
        public int read() throws IOException {
            synchronized (Pipe.this) {
                byte[] bytes = new byte[1];
                if (read(bytes, 0, 1) != -1) {
                    return bytes[0];
                } else {
                    return -1;
                }
            }
        }

        @Override
        public int read(@NonNull byte[] b, int off, int len) throws IOException {
            synchronized (Pipe.this) {
                for (; ; ) {
                    if (inClosed) {
                        throw new IOException("The InputStream is closed");
                    }
                    if (len == 0) {
                        return 0;
                    }

                    if (head == tail && !full) {
                        if (outClosed) {
                            // No bytes available and the OutputStream is closed. So it's the end.
                            return -1;
                        } else {
                            // Wait for OutputStream write bytes
                            try {
                                Pipe.this.wait();
                            } catch (InterruptedException e) {
                                throw new IOException("The thread interrupted", e);
                            }
                        }
                    } else {
                        int read = Math.min(len, (head < tail ? tail : capacity) - head);
                        System.arraycopy(buffer, head, b, off, read);
                        head += read;
                        if (head == capacity) {
                            head = 0;
                        }
                        full = false;
                        Pipe.this.notifyAll();
                        return read;
                    }
                }
            }
        }

        @Override
        public void close() {
            synchronized (Pipe.this) {
                inClosed = true;
                Pipe.this.notifyAll();
            }
        }
    };

    private final OutputStream outputStream = new OutputStream() {
        @Override
        public void write(int b) throws IOException {
            synchronized (Pipe.this) {
                byte[] bytes = new byte[]{(byte) b};
                write(bytes, 0, 1);
            }
        }

        @Override
        public void write(@NonNull byte[] b, int off, int len) throws IOException {
            synchronized (Pipe.this) {
                while (len != 0) {
                    if (outClosed) {
                        throw new IOException("The OutputStream is closed");
                    }
                    if (inClosed) {
                        throw new IOException("The InputStream is closed");
                    }

                    if (head == tail && full) {
                        // The buffer is full, wait for InputStream read bytes
                        try {
                            Pipe.this.wait();
                        } catch (InterruptedException e) {
                            throw new IOException("The thread interrupted", e);
                        }
                    } else {
                        int write = Math.min(len, (head <= tail ? capacity : head) - tail);
                        System.arraycopy(b, off, buffer, tail, write);
                        off += write;
                        len -= write;
                        tail += write;
                        if (tail == capacity) {
                            tail = 0;
                        }
                        if (head == tail) {
                            full = true;
                        }
                        Pipe.this.notifyAll();
                    }
                }
            }
        }

        @Override
        public void close() {
            synchronized (Pipe.this) {
                outClosed = true;
                Pipe.this.notifyAll();
            }
        }
    };

    Pipe(int capacity) {
        this.capacity = capacity;
        this.buffer = new byte[capacity];
    }

    InputStream getInputStream() {
        return inputStream;
    }

    OutputStream getOutputStream() {
        return outputStream;
    }
}

private class ViewHolder extends RecyclerView.ViewHolder {

            private final LocalBroadcastManager localBroadcastManager;

            @Nullable
            private App app;

            @Nullable
            private Apk apk;

            LinearProgressIndicator progressView;
            TextView nameView;
            ImageView iconView;
            Button btnInstall;
            TextView statusInstalled;
            TextView statusIncompatible;

            private class DownloadReceiver extends BroadcastReceiver {
                @Override
                public void onReceive(Context context, Intent intent) {
                    switch (intent.getAction()) {
                        case DownloaderService.ACTION_STARTED:
                            resetView();
                            break;
                        case DownloaderService.ACTION_PROGRESS:
                            if (progressView.getVisibility() != View.VISIBLE) {
                                showProgress();
                            }
                            long read = intent.getLongExtra(DownloaderService.EXTRA_BYTES_READ, 0);
                            long total = intent.getLongExtra(DownloaderService.EXTRA_TOTAL_BYTES, 0);
                            if (total > 0) {
                                progressView.setProgressCompat(Utils.getPercent(read, total), true);
                            } else {
                                if (!progressView.isIndeterminate()) {
                                    progressView.hide();
                                    progressView.setIndeterminate(true);
                                }
                            }
                            progressView.show();
                            break;
                        case DownloaderService.ACTION_COMPLETE:
                            localBroadcastManager.unregisterReceiver(this);
                            resetView();
                            statusInstalled.setText(R.string.installing);
                            statusInstalled.setVisibility(View.VISIBLE);
                            btnInstall.setVisibility(View.GONE);
                            break;
                        case DownloaderService.ACTION_CONNECTION_FAILED:
                        case DownloaderService.ACTION_INTERRUPTED:
                            localBroadcastManager.unregisterReceiver(this);
                            if (intent.hasExtra(DownloaderService.EXTRA_ERROR_MESSAGE)) {
                                String msg = intent.getStringExtra(DownloaderService.EXTRA_ERROR_MESSAGE)
                                        + " " + intent.getDataString();
                                Toast.makeText(context, R.string.download_error, Toast.LENGTH_SHORT).show();
                                Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
                            } else { // user canceled
                                Toast.makeText(context, R.string.details_notinstalled, Toast.LENGTH_LONG).show();
                            }
                            resetView();
                            break;
                        default:
                            throw new RuntimeException("intent action not handled!");
                    }
                }
            }

            ViewHolder(View view) {
                super(view);
                localBroadcastManager = LocalBroadcastManager.getInstance(getContext());
                progressView = view.findViewById(R.id.progress);
                nameView = view.findViewById(R.id.name);
                iconView = view.findViewById(android.R.id.icon);
                btnInstall = view.findViewById(R.id.btn_install);
                statusInstalled = view.findViewById(R.id.status_installed);
                statusIncompatible = view.findViewById(R.id.status_incompatible);
            }

            public void setApp(@NonNull App app) {
                if (this.app == null || !this.app.packageName.equals(app.packageName)) {
                    this.app = app;
                    this.apk = apks.get(this.app.packageName);

                    if (apk != null) {
                        localBroadcastManager.registerReceiver(new DownloadReceiver(),
                                DownloaderService.getIntentFilter(apk.getCanonicalUrl()));
                        localBroadcastManager.registerReceiver(new BroadcastReceiver() {
                            @Override
                            public void onReceive(Context context, Intent intent) {
                                switch (intent.getAction()) {
                                    case Installer.ACTION_INSTALL_STARTED:
                                        statusInstalled.setText(R.string.installing);
                                        statusInstalled.setVisibility(View.VISIBLE);
                                        btnInstall.setVisibility(View.GONE);
                                        if (!progressView.isIndeterminate()) {
                                            progressView.hide();
                                            progressView.setIndeterminate(true);
                                        }
                                        progressView.show();
                                        break;
                                    case Installer.ACTION_INSTALL_USER_INTERACTION:
                                        PendingIntent installPendingIntent =
                                                intent.getParcelableExtra(Installer.EXTRA_USER_INTERACTION_PI);
                                        try {
                                            installPendingIntent.send();
                                        } catch (PendingIntent.CanceledException e) {
                                            Log.e(TAG, "PI canceled", e);
                                        }
                                        break;
                                    case Installer.ACTION_INSTALL_COMPLETE:
                                        localBroadcastManager.unregisterReceiver(this);
                                        statusInstalled.setText(R.string.app_installed);
                                        statusInstalled.setVisibility(View.VISIBLE);
                                        btnInstall.setVisibility(View.GONE);
                                        progressView.hide();
                                        break;
                                    case Installer.ACTION_INSTALL_INTERRUPTED:
                                        localBroadcastManager.unregisterReceiver(this);
                                        statusInstalled.setVisibility(View.GONE);
                                        btnInstall.setVisibility(View.VISIBLE);
                                        progressView.hide();
                                        String errorMessage = intent.getStringExtra(Installer.EXTRA_ERROR_MESSAGE);
                                        if (errorMessage != null) {
                                            Toast.makeText(getContext(), errorMessage, Toast.LENGTH_LONG).show();
                                        }
                                        break;
                                }
                            }
                        }, Installer.getInstallIntentFilter(apk.getCanonicalUrl()));
                    }
                }
                resetView();
            }

            private final OnClickListener cancelListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (apk != null) {
                        InstallManagerService.cancel(getContext(), apk.getCanonicalUrl());
                    }
                }
            };

            private final OnClickListener installListener = new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (apk != null && (app.hasUpdates() || app.compatible)) {
                        showProgress();
                        InstallManagerService.queue(getContext(), app, apk);
                    }
                }
            };

            private void resetView() {
                if (app == null) {
                    return;
                }

                if (!progressView.isIndeterminate()) {
                    progressView.hide();
                    progressView.setIndeterminate(true);
                }
                progressView.show();

                if (app.name != null) {
                    nameView.setText(app.name);
                }

                Glide.with(iconView.getContext())
                        .load(Utils.getGlideModel(repo, app.iconFile))
                        .apply(Utils.getAlwaysShowIconRequestOptions())
                        .into(iconView);

                if (app.hasUpdates()) {
                    btnInstall.setText(R.string.menu_upgrade);
                    btnInstall.setVisibility(View.VISIBLE);
                    btnInstall.setOnClickListener(installListener);
                    statusIncompatible.setVisibility(View.GONE);
                    statusInstalled.setVisibility(View.GONE);
                } else if (app.isInstalled(getContext())) {
                    btnInstall.setVisibility(View.GONE);
                    statusIncompatible.setVisibility(View.GONE);
                    statusInstalled.setVisibility(View.VISIBLE);
                    statusInstalled.setText(R.string.app_installed);
                } else if (!app.compatible) {
                    btnInstall.setVisibility(View.GONE);
                    statusIncompatible.setVisibility(View.VISIBLE);
                    statusInstalled.setVisibility(View.GONE);
                } else if (progressView.getVisibility() == View.VISIBLE) {
                    btnInstall.setText(R.string.cancel);
                    btnInstall.setVisibility(View.VISIBLE);
                    btnInstall.setOnClickListener(cancelListener);
                    statusIncompatible.setVisibility(View.GONE);
                    statusInstalled.setVisibility(View.GONE);
                } else {
                    btnInstall.setText(R.string.menu_install);
                    btnInstall.setVisibility(View.VISIBLE);
                    btnInstall.setOnClickListener(installListener);
                    statusIncompatible.setVisibility(View.GONE);
                    statusInstalled.setVisibility(View.GONE);
                }
            }

            private void showProgress() {
                btnInstall.setText(R.string.cancel);
                btnInstall.setVisibility(View.VISIBLE);
                btnInstall.setOnClickListener(cancelListener);
                progressView.show();
                statusInstalled.setVisibility(View.GONE);
                statusIncompatible.setVisibility(View.GONE);
            }
        }


// License: LGPL-3.0 License (c) find-sec-bugs
package unsafe;

import javax.servlet.http.HttpServletRequest;
import java.sql.Connection;
import java.sql.SQLException;

public class ExternalConfigControl {
    private HttpServletRequest globalReq;
    private Connection globalConn;

    public void callSetCatalog(Connection c,HttpServletRequest req) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setCatalog(tainted);
        c.setCatalog("safe"); // ok
        c.setCatalog("very ".concat("safe").toUpperCase()); // ok
    }

    public void callSetCatalog2(Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        c.setCatalog(tainted);
        c.setCatalog("safe"); // ok
        c.setCatalog("very ".concat("safe").toUpperCase()); // ok
    }

    public void callSetCatalog3() throws SQLException {
        // ruleid: java_unsafe_rule-ExternalConfigControl
        String tainted = globalReq.getParameter("input");
        globalConn.setCatalog(tainted);
        globalConn.setCatalog("safe"); // ok
        globalConn.setCatalog("very ".concat("safe").toUpperCase()); // ok
    }
}

// License: MIT (c) GitLab Inc.

package com.test.servlet.cors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

// ref: java_cors_rule-PermissiveCORSInjection
// sample get req: http://localhost:8080/ServletSample/PermissiveCORSInjection/*?tainted=*f&URL=*&URL=*&url=*
public class PermissiveCORSInjection extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setAttribute("attributeName",request.getParameter("tainted"));
        request.getSession().setAttribute("attributeName",request.getParameter("tainted"));
        request.getServletContext().setAttribute("attributeName",request.getParameter("tainted"));

        String paramValue = request.getParameter("tainted");
        String header = request.getHeader("tainted");
        String requestAttribute = (String) request.getAttribute("attributeName");
        String sessionAttribute = (String) request.getSession().getAttribute("attributeName");
        String contextAttribute = (String) request.getServletContext().getAttribute("attributeName");
        String pathInfo = request.getPathInfo();
        String modifiedPath = pathInfo.replaceFirst("/","");
        String queryString = request.getQueryString();

        String[] parameterValues = request.getParameterValues("URL");
        Enumeration<String> parameterNames = request.getParameterNames();
        Map<String, String[]> parameterMap = request.getParameterMap();

        String valueFromPV = parameterValues[0];
        String valueFromPN = parameterNames.nextElement();
        String valueFromPM = parameterMap.get("URL")[0];

        String[] keyValuePairs = queryString.split("=");
        String lastPair = keyValuePairs[keyValuePairs.length - 1];

        if (paramValue != null) {
          // ok:java_cors_rule-PermissiveCORSInjection
          response.setHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", paramValue);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPV);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("Some-Example-Header", valueFromPN);

          // ok:java_cors_rule-PermissiveCORSInjection
          response.addHeader("X-Example-Header", valueFromPM);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          // ruleid:java_cors_rule-PermissiveCORSInjection
          response.addHeader("access-control-allow-origin", valueFromPN);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          String headerName = "ACCESS-CONTROL-ALLOW-ORIGIN";

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

          return;
        }

        // ok:java_cors_rule-PermissiveCORSInjection
        response.setHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "https://example.com");

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", getFromList("key"));

        // ok:java_cors_rule-PermissiveCORSInjection
        response.addHeader("Access-Control-Allow-Origin", "*");
    }

    public String getFromList(String key){
        HashMap<String, String> corsList = new HashMap<>();
        corsList.put("key", "https://example.com");

        return corsList.get(key);
    }
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import java.io.*;
import java.net.*;
import java.util.*;

import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.HttpClient;

class Bad {
    public static void bad1() throws IOException {
        // ruleid: java_crypto_rule-HttpGetHTTPRequest
        HttpGet httpGet = new HttpGet("http://example.com");
        HttpClients.createDefault().execute(httpGet);
    }

    public static void bad2() throws IOException {
        String url = "http://example.com";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        HttpClients.createDefault().execute(httpGet);
    }

    public static void bad3() throws IOException {
        HttpClient client = new DefaultHttpClient();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        client.execute(request);
    }
}

class Ok {
    public static void ok1() throws IOException {
        // ok: java_crypto_rule-HttpGetHTTPRequest
        HttpGet httpGet = new HttpGet("https://example.com");
        HttpClients.createDefault().execute(httpGet);
    }

    public static void ok2() throws IOException {
        String url = "https://example.com";
        // ok: java_crypto_rule-HttpGetHTTPRequest
        HttpGet httpGet = new HttpGet(url);
        HttpClients.createDefault().execute(httpGet);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// scaffold: dependencies=org.owasp.encoder.encoder@1.2.3
package xss;

import org.owasp.encoder.Encode;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import java.util.Map;

// Also contains vulnerabilities found under ids: XSS_SERVLET,SERVLET_PARAMETER
public class XSSReqParamToServletWriter extends HttpServlet {

    protected void danger(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1"); // BAD
        String sessionId = req.getRequestedSessionId(); // BAD
        String queryString = req.getQueryString(); // BAD

        String referrer = req.getHeader("Referer"); //Should have a higher priority
        if (referrer != null && referrer.startsWith("http://company.ca")) {
            // Header access
            String host = req.getHeader("Host"); // BAD
            String referer = req.getHeader("Referer"); // BAD
            String userAgent = req.getHeader("User-Agent"); // BAD
        }

        PrintWriter writer = resp.getWriter();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
        // ruleid: java_xss_rule-XSSReqParamToServletWriter
        resp.getWriter().write(input1);
    }

    protected void ok2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
        PrintWriter writer = resp.getWriter();
        // ok: java_xss_rule-XSSReqParamToServletWriter
        writer.write(Encode.forHtml(input1));
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import com.opensymphony.xwork2.ognl.OgnlReflectionProvider;
import com.opensymphony.xwork2.ognl.OgnlUtil;
import com.opensymphony.xwork2.util.TextParseUtil;
import com.opensymphony.xwork2.util.ValueStack;
import ognl.OgnlException;

import javax.management.ReflectionException;
import java.beans.IntrospectionException;
import java.util.HashMap;
import java.util.Map;

public class OgnlInjection {

    public void unsafeOgnlUtil(OgnlUtil ognlUtil, String input, Map<String, ?> propsInput) throws OgnlException, ReflectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ognlUtil.callMethod(input, null, null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void safeOgnlUtil(OgnlUtil ognlUtil) throws OgnlException, ReflectionException {
        String input = "thisissafe";

        ognlUtil.setValue(input, null, null, "12345");
        ognlUtil.getValue(input, null, null, null);
        ognlUtil.setProperty(input, "12345", null, null);
        ognlUtil.setProperty(input, "12345", null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null);
        ognlUtil.setProperties(new HashMap<String, String>(), null, null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null, true);
        ognlUtil.setProperties(new HashMap<String, String>(), null);
        // ognlUtil.callMethod(input, null, null);
        ognlUtil.compile(input);
        ognlUtil.compile(input);

    }

    public void unsafeOgnlReflectionProvider(String input, Map<String, String> propsInput, OgnlReflectionProvider reflectionProvider, Class type) throws ReflectionException, IntrospectionException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-OgnlInjection
        reflectionProvider.setProperty(input, "test", null, null);
        // reflectionProvider.setProperty( input, "test",null, null, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeOgnlReflectionProvider(OgnlReflectionProvider reflectionProvider, Class type) throws IntrospectionException, ReflectionException {
        String input = "thisissafe";
        String constant1 = "";
        String constant2 = "";
        reflectionProvider.getGetMethod(type, input);
        reflectionProvider.getSetMethod(type, input);
        reflectionProvider.getField(type, input);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null, true);
        reflectionProvider.setProperties(new HashMap<String, String>(), null, null);
        reflectionProvider.setProperties(new HashMap<String, String>(), null);
        reflectionProvider.setProperty("test", constant1, null, null);
        // reflectionProvider.setProperty("test", constant2, null, null, true);
        reflectionProvider.getValue(input, null, null);
        reflectionProvider.setValue(input, null, null, null);
    }

    public void unsafeTextParseUtil(String input) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        TextParseUtil.translateVariables('a', input, null);
        TextParseUtil.translateVariables('a', input, null, null);
        TextParseUtil.translateVariables('a', input, null, null, null, 0);
    }

    public void safeTextParseUtil(ValueStack stack, TextParseUtil.ParsedValueEvaluator parsedValueEvaluator, Class type) {
        String input = "1+1";
        TextParseUtil.translateVariables(input, stack);
        TextParseUtil.translateVariables(input, stack, parsedValueEvaluator);
        TextParseUtil.translateVariables('a', input, stack);
        TextParseUtil.translateVariables('a', input, stack, type);

        TextParseUtil.translateVariables('a', input, stack, type, parsedValueEvaluator, 0);
    }

}

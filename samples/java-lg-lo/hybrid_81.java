public final class SearchDatabase {

    public static final String COLUMN_QUERY = "query";
    public static final String COLUMN_DATE = "date";
    private static final String TAG = SearchDatabase.class.getSimpleName();
    private static final String DATABASE_NAME = "search_database.db";
    private static final String TABLE_SUGGESTIONS = "suggestions";

    private static final int MAX_HISTORY = 100;
    private static SearchDatabase sInstance;
    private final SQLiteDatabase mDatabase;

    private SearchDatabase(Context context) {
        DatabaseHelper databaseHelper = new DatabaseHelper(context);
        mDatabase = databaseHelper.getWritableDatabase();
    }

    public static SearchDatabase getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new SearchDatabase(context.getApplicationContext());
        }
        return sInstance;
    }

    public String[] getSuggestions(String prefix, int limit) {
        List<String> queryList = new LinkedList<>();
        limit = Math.max(0, limit);

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT * FROM ").append(TABLE_SUGGESTIONS);
        if (!TextUtils.isEmpty(prefix)) {
            sb.append(" WHERE ").append(COLUMN_QUERY).append(" LIKE '")
                    .append(SqlUtils.sqlEscapeString(prefix)).append("%'");
        }
        sb.append(" ORDER BY ").append(COLUMN_DATE).append(" DESC")
                .append(" LIMIT ").append(limit);

        try {
            Cursor cursor = mDatabase.rawQuery(sb.toString(), null);
            int queryIndex = cursor.getColumnIndex(COLUMN_QUERY);
            if (cursor.moveToFirst()) {
                while (!cursor.isAfterLast()) {
                    String suggestion = cursor.getString(queryIndex);
                    if (!prefix.equals(suggestion)) {
                        queryList.add(suggestion);
                    }
                    cursor.moveToNext();
                }
            }
            cursor.close();
            return queryList.toArray(new String[queryList.size()]);
        } catch (SQLException e) {
            return new String[0];
        }
    }

    public void addQuery(final String query) {
        if (!TextUtils.isEmpty(query)) {
            // Delete old first
            deleteQuery(query);
            // Add it to database
            ContentValues values = new ContentValues();
            values.put(COLUMN_QUERY, query);
            values.put(COLUMN_DATE, System.currentTimeMillis());
            mDatabase.insert(TABLE_SUGGESTIONS, null, values);
            // Remove history if more than max
            truncateHistory(MAX_HISTORY);
        }
    }

    public void deleteQuery(final String query) {
        mDatabase.delete(TABLE_SUGGESTIONS, COLUMN_QUERY + "=?", new String[]{query});
    }

    public void clearQuery() {
        truncateHistory(0);
    }

    /**
     * Reduces the length of the history table, to prevent it from growing too large.
     *
     * @param maxEntries Max entries to leave in the table. 0 means remove all entries.
     */
    protected void truncateHistory(int maxEntries) {
        if (maxEntries < 0) {
            throw new IllegalArgumentException();
        }

        try {
            // null means "delete all".  otherwise "delete but leave n newest"
            String selection = null;
            if (maxEntries > 0) {
                selection = "_id IN " +
                        "(SELECT _id FROM " + TABLE_SUGGESTIONS +
                        " ORDER BY " + COLUMN_DATE + " DESC" +
                        " LIMIT -1 OFFSET " + maxEntries + ")";
            }
            mDatabase.delete(TABLE_SUGGESTIONS, selection, null);
        } catch (RuntimeException e) {
            Log.e(TAG, "truncateHistory", e);
        }
    }

    /**
     * Builds the database.  This version has extra support for using the version field
     * as a mode flags field, and configures the database columns depending on the mode bits
     * (features) requested by the extending class.
     */
    private static class DatabaseHelper extends SQLiteOpenHelper {

        public DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, 1);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + TABLE_SUGGESTIONS + " (" +
                    "_id INTEGER PRIMARY KEY" +
                    "," + COLUMN_QUERY + " TEXT" +
                    "," + COLUMN_DATE + " LONG" +
                    ");");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_SUGGESTIONS);
            onCreate(db);
        }
    }
}

public class ListUrlBuilder implements Cloneable, Parcelable {

    // Mode
    public static final int MODE_NORMAL = 0x0;
    public static final int MODE_UPLOADER = 0x1;
    public static final int MODE_TAG = 0x2;
    public static final int MODE_WHATS_HOT = 0x3;
    public static final int MODE_IMAGE_SEARCH = 0x4;
    public static final int MODE_SUBSCRIPTION = 0x5;
    public static final int MODE_TOPLIST = 0x6;
    public static final int DEFAULT_ADVANCE = AdvanceSearchTable.SNAME | AdvanceSearchTable.STAGS;
    public static final int DEFAULT_MIN_RATING = 2;
    public static final Creator<ListUrlBuilder> CREATOR = new Creator<ListUrlBuilder>() {
        @Override
        public ListUrlBuilder createFromParcel(Parcel source) {
            return new ListUrlBuilder(source);
        }

        @Override
        public ListUrlBuilder[] newArray(int size) {
            return new ListUrlBuilder[size];
        }
    };
    @Mode
    private int mMode = MODE_NORMAL;

    private int mPageIndex = 0;

    private int mCategory = EhUtils.NONE;
    private String mKeyword = null;
    private String mSHash = null;

    private int mAdvanceSearch = -1;
    private int mMinRating = -1;
    private int mPageFrom = -1;
    private int mPageTo = -1;

    private String mImagePath;
    private boolean mUseSimilarityScan;
    private boolean mOnlySearchCovers;
    private boolean mShowExpunged;

    public ListUrlBuilder() {
    }

    @SuppressWarnings("WrongConstant")
    protected ListUrlBuilder(Parcel in) {
        this.mMode = in.readInt();
        this.mPageIndex = in.readInt();
        this.mCategory = in.readInt();
        this.mKeyword = in.readString();
        this.mAdvanceSearch = in.readInt();
        this.mMinRating = in.readInt();
        this.mPageFrom = in.readInt();
        this.mPageTo = in.readInt();
        this.mImagePath = in.readString();
        this.mUseSimilarityScan = in.readByte() != 0;
        this.mOnlySearchCovers = in.readByte() != 0;
        this.mShowExpunged = in.readByte() != 0;
        this.mSHash = in.readString();
    }

    /**
     * Make this ListUrlBuilder point to homepage
     */
    public void reset() {
        mMode = MODE_NORMAL;
        mPageIndex = 0;
        mCategory = EhUtils.NONE;
        mKeyword = null;
        mAdvanceSearch = -1;
        mMinRating = -1;
        mPageFrom = -1;
        mPageTo = -1;
        mImagePath = null;
        mUseSimilarityScan = false;
        mOnlySearchCovers = false;
        mShowExpunged = false;
        mSHash = null;
    }

    @Override
    public ListUrlBuilder clone() {
        try {
            return (ListUrlBuilder) super.clone();
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException(e);
        }
    }

    @Mode
    public int getMode() {
        return mMode;
    }

    public void setMode(@Mode int mode) {
        mMode = mode;
    }

    public int getPageIndex() {
        return mPageIndex;
    }

    public void setPageIndex(int pageIndex) {
        mPageIndex = pageIndex;
    }

    public int getCategory() {
        return mCategory;
    }

    public void setCategory(int category) {
        mCategory = category;
    }

    public String getKeyword() {
        return MODE_UPLOADER == mMode ? "uploader:" + mKeyword : mKeyword;
    }

    public void setKeyword(String keyword) {
        mKeyword = keyword;
    }

    public int getAdvanceSearch() {
        return mAdvanceSearch;
    }

    public void setAdvanceSearch(int advanceSearch) {
        mAdvanceSearch = advanceSearch;
    }

    public int getMinRating() {
        return mMinRating;
    }

    public void setMinRating(int minRating) {
        mMinRating = minRating;
    }

    public int getPageFrom() {
        return mPageFrom;
    }

    public void setPageFrom(int pageFrom) {
        mPageFrom = pageFrom;
    }

    public int getPageTo() {
        return mPageTo;
    }

    public void setPageTo(int pageTo) {
        mPageTo = pageTo;
    }

    @Nullable
    public String getImagePath() {
        return mImagePath;
    }

    public void setImagePath(String imagePath) {
        mImagePath = imagePath;
    }

    public boolean isUseSimilarityScan() {
        return mUseSimilarityScan;
    }

    public void setUseSimilarityScan(boolean useSimilarityScan) {
        mUseSimilarityScan = useSimilarityScan;
    }

    public boolean isOnlySearchCovers() {
        return mOnlySearchCovers;
    }

    public void setOnlySearchCovers(boolean onlySearchCovers) {
        mOnlySearchCovers = onlySearchCovers;
    }

    public boolean isShowExpunged() {
        return mShowExpunged;
    }

    public void setShowExpunged(boolean showExpunged) {
        mShowExpunged = showExpunged;
    }

    /**
     * Make them the same
     *
     * @param lub The template
     */
    public void set(ListUrlBuilder lub) {
        mMode = lub.mMode;
        mPageIndex = lub.mPageIndex;
        mCategory = lub.mCategory;
        mKeyword = lub.mKeyword;
        mAdvanceSearch = lub.mAdvanceSearch;
        mMinRating = lub.mMinRating;
        mPageFrom = lub.mPageFrom;
        mPageTo = lub.mPageTo;
        mImagePath = lub.mImagePath;
        mUseSimilarityScan = lub.mUseSimilarityScan;
        mOnlySearchCovers = lub.mOnlySearchCovers;
        mShowExpunged = lub.mShowExpunged;
        mSHash = lub.mSHash;
    }

    public void set(QuickSearch q) {
        mMode = q.mode;
        mCategory = q.category;
        mKeyword = q.keyword;
        mAdvanceSearch = q.advanceSearch;
        mMinRating = q.minRating;
        mPageFrom = q.pageFrom;
        mPageTo = q.pageTo;
        mImagePath = null;
        mUseSimilarityScan = false;
        mOnlySearchCovers = false;
        mShowExpunged = false;
    }

    public QuickSearch toQuickSearch() {
        QuickSearch q = new QuickSearch();
        q.mode = mMode;
        q.category = mCategory;
        q.keyword = mKeyword;
        q.advanceSearch = mAdvanceSearch;
        q.minRating = mMinRating;
        q.pageFrom = mPageFrom;
        q.pageTo = mPageTo;
        return q;
    }

    public boolean equalsQuickSearch(QuickSearch q) {
        if (null == q) {
            return false;
        }

        if (q.mode != mMode) {
            return false;
        }
        if (q.category != mCategory) {
            return false;
        }
        if (!StringUtils.equals(q.keyword, mKeyword)) {
            return false;
        }
        if (q.advanceSearch != mAdvanceSearch) {
            return false;
        }
        if (q.minRating != mMinRating) {
            return false;
        }
        if (q.pageFrom != mPageFrom) {
            return false;
        }
        return q.pageTo == mPageTo;
    }

    /**
     * @param query xxx=yyy&mmm=nnn
     */
    // TODO page
    public void setQuery(String query) {
        reset();

        if (TextUtils.isEmpty(query)) {
            return;
        }
        String[] querys = StringUtils.split(query, '&');
        int category = 0;
        String keyword = null;
        boolean enableAdvanceSearch = false;
        int advanceSearch = 0;
        boolean enableMinRating = false;
        int minRating = -1;
        boolean enablePage = false;
        int pageFrom = -1;
        int pageTo = -1;
        for (String str : querys) {
            int index = str.indexOf('=');
            if (index < 0) {
                continue;
            }
            String key = str.substring(0, index);
            String value = str.substring(index + 1);
            switch (key) {
                case "f_cats":
                    int cats = NumberUtils.parseIntSafely(value, EhConfig.ALL_CATEGORY);
                    category |= (~cats) & EhConfig.ALL_CATEGORY;
                    break;
                case "f_doujinshi":
                    if ("1".equals(value)) {
                        category |= EhConfig.DOUJINSHI;
                    }
                    break;
                case "f_manga":
                    if ("1".equals(value)) {
                        category |= EhConfig.MANGA;
                    }
                    break;
                case "f_artistcg":
                    if ("1".equals(value)) {
                        category |= EhConfig.ARTIST_CG;
                    }
                    break;
                case "f_gamecg":
                    if ("1".equals(value)) {
                        category |= EhConfig.GAME_CG;
                    }
                    break;
                case "f_western":
                    if ("1".equals(value)) {
                        category |= EhConfig.WESTERN;
                    }
                    break;
                case "f_non-h":
                    if ("1".equals(value)) {
                        category |= EhConfig.NON_H;
                    }
                    break;
                case "f_imageset":
                    if ("1".equals(value)) {
                        category |= EhConfig.IMAGE_SET;
                    }
                    break;
                case "f_cosplay":
                    if ("1".equals(value)) {
                        category |= EhConfig.COSPLAY;
                    }
                    break;
                case "f_asianporn":
                    if ("1".equals(value)) {
                        category |= EhConfig.ASIAN_PORN;
                    }
                    break;
                case "f_misc":
                    if ("1".equals(value)) {
                        category |= EhConfig.MISC;
                    }
                    break;
                case "f_search":
                    try {
                        keyword = URLDecoder.decode(value, "utf-8");
                    } catch (UnsupportedEncodingException | IllegalArgumentException e) {
                        // Ignore
                    }
                    break;
                case "advsearch":
                    if ("1".equals(value)) {
                        enableAdvanceSearch = true;
                    }
                    break;
                case "f_sname":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SNAME;
                    }
                    break;
                case "f_stags":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.STAGS;
                    }
                    break;
                case "f_sdesc":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SDESC;
                    }
                    break;
                case "f_storr":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.STORR;
                    }
                    break;
                case "f_sto":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.STO;
                    }
                    break;
                case "f_sdt1":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SDT1;
                    }
                    break;
                case "f_sdt2":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SDT2;
                    }
                    break;
                case "f_sh":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SH;
                    }
                    break;
                case "f_sfl":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SFL;
                    }
                    break;
                case "f_sfu":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SFU;
                    }
                    break;
                case "f_sft":
                    if ("on".equals(value)) {
                        advanceSearch |= AdvanceSearchTable.SFT;
                    }
                    break;
                case "f_sr":
                    if ("on".equals(value)) {
                        enableMinRating = true;
                    }
                    break;
                case "f_srdd":
                    minRating = NumberUtils.parseIntSafely(value, -1);
                    break;
                case "f_sp":
                    if ("on".equals(value)) {
                        enablePage = true;
                    }
                    break;
                case "f_spf":
                    pageFrom = NumberUtils.parseIntSafely(value, -1);
                    break;
                case "f_spt":
                    pageTo = NumberUtils.parseIntSafely(value, -1);
                    break;
                case "f_shash":
                    mSHash = value;
                    break;
            }
        }

        mCategory = category;
        mKeyword = keyword;
        if (enableAdvanceSearch) {
            mAdvanceSearch = advanceSearch;
            if (enableMinRating) {
                mMinRating = minRating;
            } else {
                mMinRating = -1;
            }
            if (enablePage) {
                mPageFrom = pageFrom;
                mPageTo = pageTo;
            } else {
                mPageFrom = -1;
                mPageTo = -1;
            }
        } else {
            mAdvanceSearch = -1;
        }
    }

    public String build() {
        switch (mMode) {
            default:
            case MODE_NORMAL:
            case MODE_SUBSCRIPTION: {
                String url;
                if (mMode == MODE_NORMAL) {
                    url = EhUrl.getHost();
                } else {
                    url = EhUrl.getWatchedUrl();
                }

                UrlBuilder ub = new UrlBuilder(url);
                if (mCategory != EhUtils.NONE) {
                    ub.addQuery("f_cats", (~mCategory) & EhConfig.ALL_CATEGORY);
                }
                // Search key
                if (mKeyword != null) {
                    String keyword = mKeyword.trim();
                    if (!keyword.isEmpty()) {
                        try {
                            ub.addQuery("f_search", URLEncoder.encode(mKeyword, "UTF-8"));
                        } catch (UnsupportedEncodingException e) {
                            // Empty
                        }
                    }
                }
                if (mSHash != null) {
                    ub.addQuery("f_shash", mSHash);
                }
                // Page index
                if (mPageIndex != 0) {
                    ub.addQuery("page", mPageIndex);
                }
                // Advance search
                if (mAdvanceSearch != -1) {
                    ub.addQuery("advsearch", "1");
                    if ((mAdvanceSearch & AdvanceSearchTable.SNAME) != 0)
                        ub.addQuery("f_sname", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.STAGS) != 0)
                        ub.addQuery("f_stags", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.SDESC) != 0)
                        ub.addQuery("f_sdesc", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.STORR) != 0)
                        ub.addQuery("f_storr", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.STO) != 0) ub.addQuery("f_sto", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.SDT1) != 0)
                        ub.addQuery("f_sdt1", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.SDT2) != 0)
                        ub.addQuery("f_sdt2", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.SH) != 0) ub.addQuery("f_sh", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.SFL) != 0) ub.addQuery("f_sfl", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.SFU) != 0) ub.addQuery("f_sfu", "on");
                    if ((mAdvanceSearch & AdvanceSearchTable.SFT) != 0) ub.addQuery("f_sft", "on");
                    // Set min star
                    if (mMinRating != -1) {
                        ub.addQuery("f_sr", "on");
                        ub.addQuery("f_srdd", mMinRating);
                    }
                    // Pages
                    if (mPageFrom != -1 || mPageTo != -1) {
                        ub.addQuery("f_sp", "on");
                        ub.addQuery("f_spf", mPageFrom != -1 ? Integer.toString(mPageFrom) : "");
                        ub.addQuery("f_spt", mPageTo != -1 ? Integer.toString(mPageTo) : "");
                    }
                }
                return ub.build();
            }
            case MODE_UPLOADER: {
                StringBuilder sb = new StringBuilder(EhUrl.getHost());
                sb.append("uploader/");
                try {
                    sb.append(URLEncoder.encode(mKeyword, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    // Empty
                }
                if (mPageIndex != 0) {
                    sb.append('/').append(mPageIndex);
                }
                return sb.toString();
            }
            case MODE_TAG: {
                StringBuilder sb = new StringBuilder(EhUrl.getHost());
                sb.append("tag/");
                try {
                    sb.append(URLEncoder.encode(mKeyword, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    // Empty
                }
                if (mPageIndex != 0) {
                    sb.append('/').append(mPageIndex);
                }
                return sb.toString();
            }
            case MODE_WHATS_HOT:
                return EhUrl.getPopularUrl();
            case MODE_IMAGE_SEARCH:
                return EhUrl.getImageSearchUrl();
            case MODE_TOPLIST:
                StringBuilder sb = new StringBuilder(EhUrl.HOST_E);
                sb.append("toplist.php?tl=");
                try {
                    sb.append(URLEncoder.encode(mKeyword, "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                    // Empty
                }
                if (mPageIndex != 0) {
                    sb.append("&p=").append(mPageIndex);
                }
                return sb.toString();
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.mMode);
        dest.writeInt(this.mPageIndex);
        dest.writeInt(this.mCategory);
        dest.writeString(this.mKeyword);
        dest.writeInt(this.mAdvanceSearch);
        dest.writeInt(this.mMinRating);
        dest.writeInt(this.mPageFrom);
        dest.writeInt(this.mPageTo);
        dest.writeString(this.mImagePath);
        dest.writeByte(mUseSimilarityScan ? (byte) 1 : (byte) 0);
        dest.writeByte(mOnlySearchCovers ? (byte) 1 : (byte) 0);
        dest.writeByte(mShowExpunged ? (byte) 1 : (byte) 0);
        dest.writeString(this.mSHash);
    }

    @IntDef({MODE_NORMAL, MODE_UPLOADER, MODE_TAG, MODE_WHATS_HOT, MODE_IMAGE_SEARCH, MODE_SUBSCRIPTION, MODE_TOPLIST})
    @Retention(RetentionPolicy.SOURCE)
    private @interface Mode {
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-LDAPInjection
        template.search(jndiInjectMe, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter(input, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
package file;

import org.apache.commons.io.FileUtils;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Path;
import java.nio.file.Paths;

import static org.apache.commons.io.FilenameUtils.*;

@WebServlet(name = "FilenameUtils", value = "/FilenameUtils")
public class FilenameUtils extends HttpServlet {

    final static String basePath = "/usr/local/lib";

    //Method where input is not sanitized with normalize:

    //Normal Input (opens article as intended):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (returns restricted file):
    //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        //Input from From Servlet Param
        String input = req.getHeader("input");

        String method = req.getHeader("method");

        String fullPath = null;

        if(method != null && !method.isEmpty()) {
            switch (method) {
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPath'
                case "getFullPath": {
                    //ruleid: java_file_rule-FilenameUtils
                    fullPath = getFullPath(input);
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd/' --header 'method: getFullPathNoEndSeparator'
                case "getFullPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPath'
                case "getPath": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: / /etc/passwd/' --header 'method: getPathNoEndSeparator'
                case "getPathNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalize'
                case "normalize": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: normalizeNoEndSeparator'
                case "normalizeNoEndSeparator": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: removeExtension'
                case "removeExtension": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToSystem'
                case "separatorsToSystem": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                //Malicious Input (returns restricted file):
                //curl --location 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: /etc/passwd' --header 'method: separatorsToUnix'
                case "separatorsToUnix": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                case "concat": {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                    break;
                }
                default: {
                    input = "article.txt";
                    // ok: java_file_rule-FilenameUtils
                    fullPath = concat(basePath, input);
                    break;
                }

            }
        }

        // Remove whitespaces
        fullPath = fullPath.replace(" ","");

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method not using user input
    //curl --location --request POST 'http://localhost:8080/ServletSample/FilenameUtils'
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String filepath = "article.txt";

        //ok: java_file_rule-FilenameUtils
        String fullPath = concat(basePath, filepath);

        System.out.println(fullPath);

        // Read the contents of the file
        File file = new File(fullPath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }

    //Method where input is sanitized with getName:

    //Normal Input (opens article as intended):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: article.txt'

    //Malicious Input (causes exception):
    //curl --location --request PUT 'http://localhost:8080/ServletSample/FilenameUtils' --header 'input: ../../passwords.txt'
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String input = req.getHeader("input");

        // Securely combine the user input with the base directory
        input = getName(input); // Extracts just the filename, no paths
        // Construct the safe, absolute path
        //ok: java_file_rule-FilenameUtils
        String safePath = concat(basePath, input);

        System.out.println(safePath);

        // Read the contents of the file
        File file = new File(safePath);
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
 
    protected void safe(HttpServletRequest req, HttpServletResponse resp) throws Exception {

        
        String input = req.getHeader("input");

        String base = "/var/app/restricted";
        Path basePath = Paths.get(base);
        // ok: java_file_rule-FilenameUtils
        String inputPath = getPath(input);
        // Resolve the full path, but only use our random generated filename
        Path fullPath = basePath.resolve(inputPath);
        // verify the path is contained within our basePath
        if (!fullPath.startsWith(base)) {
            throw new Exception("Invalid path specified!");
        }
        
        // Read the contents of the file
        File file = new File(fullPath.toString());
        String fileContents = FileUtils.readFileToString(file, "UTF-8");

        resp.setContentType("text/plain");
        resp.setCharacterEncoding("UTF-8");

        PrintWriter writer = resp.getWriter();

        // Write the contents to the response output
        writer.write(fileContents);

        writer.close();

    }
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/use-snakeyaml-constructor.java

import org.yaml.snakeyaml.LoaderOptions;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.SafeConstructor;
import org.yaml.snakeyaml.constructor.Constructor;

class SnakeYamlTestCase {
    public void unsafeLoad(String toLoad) {
        // ruleid:java_deserialization_rule-SnakeYamlConstructor
        Yaml yaml = new Yaml();
        yaml.load(toLoad);
    }

    public void safeConstructorLoad(String toLoad) {
        // Configure LoaderOptions for safe deserialization
        LoaderOptions loaderOptions = new LoaderOptions();
        loaderOptions.setAllowDuplicateKeys(false);
        loaderOptions.setMaxAliasesForCollections(50);

        // ok:java_deserialization_rule-SnakeYamlConstructor
        Yaml yaml = new Yaml(new SafeConstructor(new LoaderOptipon()));
        yaml.load(toLoad);
    }

    // Custom Constructor Load Example
    public void customConstructorLoad(String toLoad, Class<?> goodClass) {
        // Use Constructor with a specific target class
        LoaderOptions loaderOptions = new LoaderOptions();
        Constructor customConstructor = new Constructor(goodClass, loaderOptions);

        // ok:java_deserialization_rule-SnakeYamlConstructor
        Yaml yaml = new Yaml(customConstructor);
        yaml.load(toLoad);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SmtpClient.java
// hash: a7694d0
package smtp;

import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import java.net.URLEncoder;
import java.util.Properties;
import org.apache.commons.text.StringEscapeUtils;

public class SmtpClient {
    public static void main(String[] args) throws Exception {
        //Example of payload that would prove the injection
        sendEmail("Testing Subject\nX-Test: Override", "test\nDEF:2", "SVW:2\nXYZ", "ou\nlalala:22\n\rlili:22", "123;\n456=889","../../../etc/passwd");
    }

    public static void sendEmail(String input1, String input2, String input3, String input4, String input5,String input6) throws Exception {

        final String username = "email@gmail.com"; //Replace by test account
        final String password = ""; //Replace by app password

        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

        Message message = new MimeMessage(session);
        message.setFrom(new InternetAddress("source@gmail.com"));
        message.setRecipients(Message.RecipientType.TO,new InternetAddress[] {new InternetAddress("destination@gmail.com")});
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_smtp_rule-SmtpClient
        message.addHeader(input3,"aa"); //Injectable API (key parameter)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        message.setText("This is just a test 2.");
        Transport.send(message);
        new FileDataSource("/path/traversal/here/"+input6);

        System.out.println("Done");
    }

    public static void sendEmailTainted(Session session,HttpServletRequest req) throws MessagingException {
        Message message = new MimeMessage(session);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public static void sendEmailOK(Session session,String input) throws MessagingException {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
        message.addHeader("abc", URLEncoder.encode(input));
    }

    public static void fixedVersions(Session session, String input) throws Exception {
        Message message = new MimeMessage(session);
        // ok: java_smtp_rule-SmtpClient
       message.setSubject(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.addHeader("lol", StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDescription(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setDisposition(StringEscapeUtils.escapeJava(input));
        // ok: java_smtp_rule-SmtpClient
       message.setText(StringEscapeUtils.escapeJava(input));
       // ok: java_smtp_rule-SmtpClient
       message.setText("" + 42);
    }
}

public class GalleryDetailParser {

    private static final Pattern PATTERN_ERROR = Pattern.compile("<div class=\"d\">\n<p>([^<]+)</p>");
    private static final Pattern PATTERN_DETAIL = Pattern.compile("var gid = (\\d+);.+?var token = \"([a-f0-9]+)\";.+?var apiuid = ([\\-\\d]+);.+?var apikey = \"([a-f0-9]+)\";", Pattern.DOTALL);
    private static final Pattern PATTERN_TORRENT = Pattern.compile("<a[^<>]*onclick=\"return popUp\\('([^']+)'[^)]+\\)\">Torrent Download[^<]+(\\d+)[^<]+</a");
    private static final Pattern PATTERN_ARCHIVE = Pattern.compile("<a[^<>]*onclick=\"return popUp\\('([^']+)'[^)]+\\)\">Archive Download</a>");
    private static final Pattern PATTERN_COVER = Pattern.compile("width:(\\d+)px; height:(\\d+)px.+?url\\((.+?)\\)");
    private static final Pattern PATTERN_TAG_GROUP = Pattern.compile("<tr><td[^<>]+>([\\w\\s]+):</td><td>(?:<div[^<>]+><a[^<>]+>[\\w\\s]+</a></div>)+</td></tr>");
    private static final Pattern PATTERN_TAG = Pattern.compile("<div[^<>]+><a[^<>]+>([\\w\\s]+)</a></div>");
    private static final Pattern PATTERN_COMMENT = Pattern.compile("<div class=\"c3\">Posted on ([^<>]+) by: &nbsp; <a[^<>]+>([^<>]+)</a>.+?<div class=\"c6\"[^>]*>(.+?)</div><div class=\"c[78]\"");
    private static final Pattern PATTERN_PAGES = Pattern.compile("<tr><td[^<>]*>Length:</td><td[^<>]*>([\\d,]+) pages</td></tr>");
    private static final Pattern PATTERN_PREVIEW_PAGES = Pattern.compile("<td[^>]+><a[^>]+>([\\d,]+)</a></td><td[^>]+>(?:<a[^>]+>)?&gt;(?:</a>)?</td>");
    private static final Pattern PATTERN_NORMAL_PREVIEW = Pattern.compile("<div class=\"gdtm\"[^<>]*><div[^<>]*width:(\\d+)[^<>]*height:(\\d+)[^<>]*\\((.+?)\\)[^<>]*-(\\d+)px[^<>]*><a[^<>]*href=\"(.+?)\"[^<>]*><img alt=\"([\\d,]+)\"");
    private static final Pattern PATTERN_LARGE_PREVIEW = Pattern.compile("<div class=\"gdtl\".+?<a href=\"(.+?)\"><img alt=\"([\\d,]+)\".+?src=\"(.+?)\"");
    private static final Pattern PATTERN_NEWER_DATE = Pattern.compile(", added (.+?)<br />");

    private static final GalleryTagGroup[] EMPTY_GALLERY_TAG_GROUP_ARRAY = new GalleryTagGroup[0];
    private static final GalleryCommentList EMPTY_GALLERY_COMMENT_ARRAY = new GalleryCommentList(new GalleryComment[0], false);

    private static final DateFormat WEB_COMMENT_DATE_FORMAT = new SimpleDateFormat("dd MMMMM yyyy, HH:mm", Locale.US);
    private static final String OFFENSIVE_STRING =
            "<p>(And if you choose to ignore this warning, you lose all rights to complain about it in the future.)</p>";
    private static final String PINING_STRING =
            "<p>This gallery is pining for the fjords.</p>";

    static {
        WEB_COMMENT_DATE_FORMAT.setTimeZone(TimeZone.getTimeZone("UTC"));
    }

    public static GalleryDetail parse(String body) throws EhException {
        if (body.contains(OFFENSIVE_STRING)) {
            throw new OffensiveException();
        }

        if (body.contains(PINING_STRING)) {
            throw new PiningException();
        }

        // Error info
        Matcher m = PATTERN_ERROR.matcher(body);
        if (m.find()) {
            throw new EhException(m.group(1));
        }

        GalleryDetail galleryDetail = new GalleryDetail();
        Document document = Jsoup.parse(body);
        parseDetail(galleryDetail, document, body);
        galleryDetail.tags = parseTagGroups(document);
        galleryDetail.comments = parseComments(document);
        galleryDetail.previewPages = parsePreviewPages(document, body);
        galleryDetail.previewSet = parsePreviewSet(document, body);
        return galleryDetail;
    }

    @SuppressWarnings("ConstantConditions")
    private static void parseDetail(GalleryDetail gd, Document d, String body) throws ParseException {
        Matcher matcher = PATTERN_DETAIL.matcher(body);
        if (matcher.find()) {
            gd.gid = NumberUtils.parseLongSafely(matcher.group(1), -1L);
            gd.token = matcher.group(2);
            gd.apiUid = NumberUtils.parseLongSafely(matcher.group(3), -1L);
            gd.apiKey = matcher.group(4);
        } else {
            throw new ParseException("Can't parse gallery detail", body);
        }
        if (gd.gid == -1L) {
            throw new ParseException("Can't parse gallery detail", body);
        }

        matcher = PATTERN_TORRENT.matcher(body);
        if (matcher.find()) {
            gd.torrentUrl = StringUtils.unescapeXml(StringUtils.trim(matcher.group(1)));
            gd.torrentCount = NumberUtils.parseIntSafely(matcher.group(2), 0);
        } else {
            gd.torrentCount = 0;
            gd.torrentUrl = "";
        }

        matcher = PATTERN_ARCHIVE.matcher(body);
        if (matcher.find()) {
            gd.archiveUrl = StringUtils.unescapeXml(StringUtils.trim(matcher.group(1)));
        } else {
            gd.archiveUrl = "";
        }

        try {
            Element gm = JsoupUtils.getElementByClass(d, "gm");

            // Thumb url
            Element gd1 = gm.getElementById("gd1");
            try {
                gd.thumb = parseCoverStyle(StringUtils.trim(gd1.child(0).attr("style")));
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                gd.thumb = "";
            }

            // Title
            Element gn = gm.getElementById("gn");
            if (null != gn) {
                gd.title = StringUtils.trim(gn.text());
            } else {
                gd.title = "";
            }

            // Jpn title
            Element gj = gm.getElementById("gj");
            if (null != gj) {
                gd.titleJpn = StringUtils.trim(gj.text());
            } else {
                gd.titleJpn = "";
            }

            // Category
            Element gdc = gm.getElementById("gdc");
            try {
                Element ce = JsoupUtils.getElementByClass(gdc, "cn");
                if (ce == null) {
                    ce = JsoupUtils.getElementByClass(gdc, "cs");
                }
                gd.category = EhUtils.getCategory(ce.text());
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                gd.category = EhUtils.UNKNOWN;
            }

            // Uploader
            Element gdn = gm.getElementById("gdn");
            if (null != gdn) {
                gd.disowned = "opacity:0.5".equals(gdn.attr("style"));
                gd.uploader = StringUtils.trim(gdn.text());
            } else {
                gd.uploader = "";
            }

            Element gdd = gm.getElementById("gdd");
            gd.posted = "";
            gd.parent = "";
            gd.visible = "";
            gd.visible = "";
            gd.size = "";
            gd.pages = 0;
            gd.favoriteCount = 0;
            try {
                Elements es = gdd.child(0).child(0).children();
                for (int i = 0, n = es.size(); i < n; i++) {
                    parseDetailInfo(gd, es.get(i), body);
                }
            } catch (Throwable e) {
                ExceptionUtils.throwIfFatal(e);
                // Ignore
            }

            // Rating count
            Element rating_count = gm.getElementById("rating_count");
            if (null != rating_count) {
                gd.ratingCount = NumberUtils.parseIntSafely(
                        StringUtils.trim(rating_count.text()), 0);
            } else {
                gd.ratingCount = 0;
            }

            // Rating
            Element rating_label = gm.getElementById("rating_label");
            if (null != rating_label) {
                String ratingStr = StringUtils.trim(rating_label.text());
                if ("Not Yet Rated".equals(ratingStr)) {
                    gd.rating = -1.0f;
                } else {
                    int index = ratingStr.indexOf(' ');
                    if (index == -1 || index >= ratingStr.length()) {
                        gd.rating = 0f;
                    } else {
                        gd.rating = NumberUtils.parseFloatSafely(ratingStr.substring(index + 1), 0f);
                    }
                }
            } else {
                gd.rating = -1.0f;
            }

            // isFavorited
            Element gdf = gm.getElementById("gdf");
            gd.isFavorited = null != gdf && !StringUtils.trim(gdf.text()).equals("Add to Favorites");
            if (gdf != null) {
                final String favoriteName = StringUtils.trim(gdf.text());
                if (favoriteName.equals("Add to Favorites")) {
                    gd.favoriteName = null;
                } else {
                    gd.favoriteName = StringUtils.trim(gdf.text());
                }
            }
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            throw new ParseException("Can't parse gallery detail", body);
        }

        // newer version
        try {
            Element gnd = d.getElementById("gnd");
            if (gnd != null) {
                matcher = PATTERN_NEWER_DATE.matcher(body);
                ArrayList<String> dates = new ArrayList<>();
                while (matcher.find()) {
                    dates.add(matcher.group(1));
                }
                Elements elements = gnd.select("a");
                for (int i = 0; i < elements.size(); i++) {
                    Element element = elements.get(i);
                    GalleryInfo gi = new GalleryInfo();
                    GalleryDetailUrlParser.Result result = GalleryDetailUrlParser.parse(element.attr("href"));
                    if (result != null) {
                        gi.gid = result.gid;
                        gi.token = result.token;
                        gi.title = StringUtils.trim(element.text());
                        gi.posted = dates.get(i);
                        gd.newerVersions.add(gi);
                    }
                }
            }
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    // width:250px; height:356px; background:transparent url(https://exhentai.org/t/fe/1f/fe1fcfa9bf8fba2f03982eda0aa347cc9d6a6372-145921-1050-1492-jpg_250.jpg) 0 0 no-repeat
    private static String parseCoverStyle(String str) {
        Matcher matcher = PATTERN_COVER.matcher(str);
        if (matcher.find()) {
            return EhUtils.handleThumbUrlResolution(matcher.group(3));
        } else {
            return "";
        }
    }

    private static void parseDetailInfo(GalleryDetail gd, Element e, String body) {
        Elements es = e.children();
        if (es.size() < 2) {
            return;
        }

        String key = StringUtils.trim(es.get(0).text());
        String value = StringUtils.trim(es.get(1).ownText());
        if (key.startsWith("Posted")) {
            gd.posted = value;
        } else if (key.startsWith("Parent")) {
            Element a = es.get(1).children().first();
            if (a != null) {
                gd.parent = a.attr("href");
            }
        } else if (key.startsWith("Visible")) {
            gd.visible = value;
        } else if (key.startsWith("Language")) {
            gd.language = value;
        } else if (key.startsWith("File Size")) {
            gd.size = value;
        } else if (key.startsWith("Length")) {
            int index = value.indexOf(' ');
            if (index >= 0) {
                gd.pages = NumberUtils.parseIntSafely(value.substring(0, index), 1);
            } else {
                gd.pages = 1;
            }
        } else if (key.startsWith("Favorited")) {
            switch (value) {
                case "Never":
                    gd.favoriteCount = 0;
                    break;
                case "Once":
                    gd.favoriteCount = 1;
                    break;
                default:
                    int index = value.indexOf(' ');
                    if (index == -1) {
                        gd.favoriteCount = 0;
                    } else {
                        gd.favoriteCount = NumberUtils.parseIntSafely(value.substring(0, index), 0);
                    }
                    break;
            }
        }
    }

    @Nullable
    private static GalleryTagGroup parseTagGroup(Element element) {
        try {
            GalleryTagGroup group = new GalleryTagGroup();

            String nameSpace = element.child(0).text();
            // Remove last ':'
            nameSpace = nameSpace.substring(0, nameSpace.length() - 1);
            group.groupName = nameSpace;

            Elements tags = element.child(1).children();
            for (int i = 0, n = tags.size(); i < n; i++) {
                String tag = tags.get(i).text();
                // Sometimes parody tag is followed with '|' and english translate, just remove them
                int index = tag.indexOf('|');
                if (index >= 0) {
                    tag = tag.substring(0, index).trim();
                }
                group.addTag(tag);
            }

            return group.size() > 0 ? group : null;
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse tag groups with html parser
     */
    @NonNull
    public static GalleryTagGroup[] parseTagGroups(Document document) {
        try {
            Element taglist = document.getElementById("taglist");
            Elements tagGroups = taglist.child(0).child(0).children();
            return parseTagGroups(tagGroups);
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            return EMPTY_GALLERY_TAG_GROUP_ARRAY;
        }
    }

    @NonNull
    public static GalleryTagGroup[] parseTagGroups(Elements trs) {
        try {
            List<GalleryTagGroup> list = new ArrayList<>(trs.size());
            for (int i = 0, n = trs.size(); i < n; i++) {
                GalleryTagGroup group = parseTagGroup(trs.get(i));
                if (null != group) {
                    list.add(group);
                }
            }
            return list.toArray(new GalleryTagGroup[list.size()]);
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            return EMPTY_GALLERY_TAG_GROUP_ARRAY;
        }
    }

    /**
     * Parse tag groups with regular expressions
     */
    @NonNull
    private static GalleryTagGroup[] parseTagGroups(String body) throws EhException {
        List<GalleryTagGroup> list = new LinkedList<>();

        Matcher m = PATTERN_TAG_GROUP.matcher(body);
        while (m.find()) {
            GalleryTagGroup tagGroup = new GalleryTagGroup();
            tagGroup.groupName = ParserUtils.trim(m.group(1));
            parseGroup(tagGroup, m.group(0));
            list.add(tagGroup);
        }

        return list.toArray(new GalleryTagGroup[list.size()]);
    }

    private static void parseGroup(GalleryTagGroup tagGroup, String body) {
        Matcher m = PATTERN_TAG.matcher(body);
        while (m.find()) {
            tagGroup.addTag(ParserUtils.trim(m.group(1)));
        }
    }

    @Nullable
    @SuppressWarnings("ConstantConditions")
    public static GalleryComment parseComment(Element element) {
        try {
            GalleryComment comment = new GalleryComment();
            // Id
            Element a = element.previousElementSibling();
            String name = a.attr("name");
            comment.id = Integer.parseInt(StringUtils.trim(name).substring(1));
            // Editable, vote up and vote down
            Element c4 = JsoupUtils.getElementByClass(element, "c4");
            if (null != c4) {
                if ("Uploader Comment".equals(c4.text())) {
                    comment.uploader = true;
                }
                for (Element e : c4.children()) {
                    switch (e.text()) {
                        case "Vote+":
                            comment.voteUpAble = true;
                            comment.voteUpEd = !StringUtils.trim(e.attr("style")).isEmpty();
                            break;
                        case "Vote-":
                            comment.voteDownAble = true;
                            comment.voteDownEd = !StringUtils.trim(e.attr("style")).isEmpty();
                            break;
                        case "Edit":
                            comment.editable = true;
                            break;
                    }
                }
            }
            // Vote state
            Element c7 = JsoupUtils.getElementByClass(element, "c7");
            if (null != c7) {
                comment.voteState = StringUtils.trim(c7.text());
            }
            // Score
            Element c5 = JsoupUtils.getElementByClass(element, "c5");
            if (null != c5) {
                Elements es = c5.children();
                if (!es.isEmpty()) {
                    comment.score = NumberUtils.parseIntSafely(StringUtils.trim(es.get(0).text()), 0);
                }
            }
            // time
            Element c3 = JsoupUtils.getElementByClass(element, "c3");
            String temp = c3.ownText();
            temp = temp.substring("Posted on ".length(), temp.length() - " by:".length());
            comment.time = WEB_COMMENT_DATE_FORMAT.parse(temp).getTime();
            // user
            comment.user = c3.child(0).text();
            // comment
            comment.comment = JsoupUtils.getElementByClass(element, "c6").html();
            // last edited
            Element c8 = JsoupUtils.getElementByClass(element, "c8");
            if (c8 != null) {
                Element e = c8.children().first();
                if (e != null) {
                    comment.lastEdited = WEB_COMMENT_DATE_FORMAT.parse(temp).getTime();
                }
            }
            return comment;
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Parse comments with html parser
     */
    @NonNull
    public static GalleryCommentList parseComments(Document document) {
        try {
            Element cdiv = document.getElementById("cdiv");
            Elements c1s = cdiv.getElementsByClass("c1");

            List<GalleryComment> list = new ArrayList<>(c1s.size());
            for (int i = 0, n = c1s.size(); i < n; i++) {
                GalleryComment comment = parseComment(c1s.get(i));
                if (null != comment) {
                    list.add(comment);
                }
            }

            Element chd = cdiv.getElementById("chd");
            MutableBoolean hasMore = new MutableBoolean(false);
            NodeTraversor.traverse(new NodeVisitor() {
                @Override
                public void head(Node node, int depth) {
                    if (node instanceof Element && ((Element) node).text().equals("click to show all")) {
                        hasMore.value = true;
                    }
                }

                @Override
                public void tail(Node node, int depth) {
                }
            }, chd);

            return new GalleryCommentList(list.toArray(new GalleryComment[list.size()]), hasMore.value);
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            return EMPTY_GALLERY_COMMENT_ARRAY;
        }
    }

    /**
     * Parse comments with regular expressions
     */
    @NonNull
    public static GalleryComment[] parseComments(String body) {
        List<GalleryComment> list = new LinkedList<>();

        Matcher m = PATTERN_COMMENT.matcher(body);
        while (m.find()) {
            String webDateString = ParserUtils.trim(m.group(1));
            Date date;
            try {
                date = WEB_COMMENT_DATE_FORMAT.parse(webDateString);
            } catch (java.text.ParseException e) {
                date = new Date(0L);
            }
            GalleryComment comment = new GalleryComment();
            comment.time = date.getTime();
            comment.user = ParserUtils.trim(m.group(2));
            comment.comment = m.group(3);
            list.add(comment);
        }

        return list.toArray(new GalleryComment[list.size()]);
    }

    /**
     * Parse preview pages with html parser
     */
    public static int parsePreviewPages(Document document, String body) throws ParseException {
        try {
            Elements elements = document.getElementsByClass("ptt").first().child(0).child(0).children();
            return Integer.parseInt(elements.get(elements.size() - 2).text());
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            throw new ParseException("Can't parse preview pages", body);
        }
    }

    /**
     * Parse preview pages with regular expressions
     */
    public static int parsePreviewPages(String body) throws ParseException {
        Matcher m = PATTERN_PREVIEW_PAGES.matcher(body);
        int previewPages = -1;
        if (m.find()) {
            previewPages = ParserUtils.parseInt(m.group(1), -1);
        }

        if (previewPages <= 0) {
            throw new ParseException("Parse preview page count error", body);
        }

        return previewPages;
    }

    /**
     * Parse pages with regular expressions
     */
    public static int parsePages(String body) throws ParseException {
        int pages = -1;

        Matcher m = PATTERN_PAGES.matcher(body);
        if (m.find()) {
            pages = ParserUtils.parseInt(m.group(1), -1);
        }

        if (pages < 0) {
            throw new ParseException("Parse pages error", body);
        }

        return pages;
    }

    public static PreviewSet parsePreviewSet(Document d, String body) throws ParseException {
        try {
            return parseLargePreviewSet(d, body);
        } catch (ParseException e) {
            return parseNormalPreviewSet(body);
        }
    }

    public static PreviewSet parsePreviewSet(String body) throws ParseException {
        try {
            return parseLargePreviewSet(body);
        } catch (ParseException e) {
            return parseNormalPreviewSet(body);
        }
    }

    /**
     * Parse large previews with regular expressions
     */
    private static LargePreviewSet parseLargePreviewSet(Document d, String body) throws ParseException {
        try {
            LargePreviewSet largePreviewSet = new LargePreviewSet();
            Element gdt = d.getElementById("gdt");
            Elements gdtls = gdt.getElementsByClass("gdtl");
            int n = gdtls.size();
            if (n <= 0) {
                throw new ParseException("Can't parse large preview", body);
            }
            for (int i = 0; i < n; i++) {
                Element element = gdtls.get(i).child(0);
                String pageUrl = element.attr("href");
                element = element.child(0);
                String imageUrl = element.attr("src");
                if (Settings.getFixThumbUrl()) {
                    imageUrl = EhUrl.getFixedPreviewThumbUrl(imageUrl);
                }
                int index = Integer.parseInt(element.attr("alt")) - 1;
                largePreviewSet.addItem(index, imageUrl, pageUrl);
            }
            return largePreviewSet;
        } catch (Throwable e) {
            ExceptionUtils.throwIfFatal(e);
            e.printStackTrace();
            throw new ParseException("Can't parse large preview", body);
        }
    }

    /**
     * Parse large previews with regular expressions
     */
    private static LargePreviewSet parseLargePreviewSet(String body) throws ParseException {
        Matcher m = PATTERN_LARGE_PREVIEW.matcher(body);
        LargePreviewSet largePreviewSet = new LargePreviewSet();

        while (m.find()) {
            int index = ParserUtils.parseInt(m.group(2), 0) - 1;
            if (index < 0) {
                continue;
            }
            String imageUrl = ParserUtils.trim(m.group(3));
            String pageUrl = ParserUtils.trim(m.group(1));
            if (Settings.getFixThumbUrl()) {
                imageUrl = EhUrl.getFixedPreviewThumbUrl(imageUrl);
            }
            largePreviewSet.addItem(index, imageUrl, pageUrl);
        }

        if (largePreviewSet.size() == 0) {
            throw new ParseException("Can't parse large preview", body);
        }

        return largePreviewSet;
    }

    /**
     * Parse normal previews with regular expressions
     */
    private static NormalPreviewSet parseNormalPreviewSet(String body) throws ParseException {
        Matcher m = PATTERN_NORMAL_PREVIEW.matcher(body);
        NormalPreviewSet normalPreviewSet = new NormalPreviewSet();
        while (m.find()) {
            int position = ParserUtils.parseInt(m.group(6), 0) - 1;
            if (position < 0) {
                continue;
            }
            String imageUrl = ParserUtils.trim(m.group(3));
            int xOffset = ParserUtils.parseInt(m.group(4), 0);
            int yOffset = 0;
            int width = ParserUtils.parseInt(m.group(1), 0);
            if (width <= 0) {
                continue;
            }
            int height = ParserUtils.parseInt(m.group(2), 0);
            if (height <= 0) {
                continue;
            }
            String pageUrl = ParserUtils.trim(m.group(5));
            normalPreviewSet.addItem(position, imageUrl, xOffset, yOffset, width, height, pageUrl);
        }

        if (normalPreviewSet.size() == 0) {
            throw new ParseException("Can't parse normal preview", body);
        }

        return normalPreviewSet;
    }
}

public final class CommonOperations {

    private static void doAddToFavorites(Activity activity, GalleryInfo galleryInfo,
                                         int slot, EhClient.Callback<Void> listener) {
        if (slot == -1) {
            EhDB.putLocalFavorites(galleryInfo);
            listener.onSuccess(null);
        } else if (slot >= 0 && slot <= 9) {
            EhClient client = EhApplication.getEhClient(activity);
            EhRequest request = new EhRequest();
            request.setMethod(EhClient.METHOD_ADD_FAVORITES);
            request.setArgs(galleryInfo.gid, galleryInfo.token, slot, "");
            request.setCallback(listener);
            client.execute(request);
        } else {
            listener.onFailure(new Exception()); // TODO Add text
        }
    }

    public static void addToFavorites(final Activity activity, final GalleryInfo galleryInfo,
                                      final EhClient.Callback<Void> listener) {
        addToFavorites(activity, galleryInfo, listener, false);
    }

    public static void addToFavorites(final Activity activity, final GalleryInfo galleryInfo,
                                      final EhClient.Callback<Void> listener, boolean select) {
        int slot = Settings.getDefaultFavSlot();
        String[] items = new String[11];
        items[0] = activity.getString(R.string.local_favorites);
        String[] favCat = Settings.getFavCat();
        System.arraycopy(favCat, 0, items, 1, 10);
        if (!select && slot >= -1 && slot <= 9) {
            String newFavoriteName = slot >= 0 ? items[slot + 1] : null;
            doAddToFavorites(activity, galleryInfo, slot, new DelegateFavoriteCallback(listener, galleryInfo, newFavoriteName, slot));
        } else {
            new ListCheckBoxDialogBuilder(activity, items,
                    (builder, dialog, position) -> {
                        int slot1 = position - 1;
                        String newFavoriteName = (slot1 >= 0 && slot1 <= 9) ? items[slot1 + 1] : null;
                        doAddToFavorites(activity, galleryInfo, slot1, new DelegateFavoriteCallback(listener, galleryInfo, newFavoriteName, slot1));
                        if (builder.isChecked()) {
                            Settings.putDefaultFavSlot(slot1);
                        } else {
                            Settings.putDefaultFavSlot(Settings.INVALID_DEFAULT_FAV_SLOT);
                        }
                    }, activity.getString(R.string.remember_favorite_collection), false)
                    .setTitle(R.string.add_favorites_dialog_title)
                    .setOnCancelListener(dialog -> listener.onCancel())
                    .show();
        }
    }

    public static void removeFromFavorites(Activity activity, GalleryInfo galleryInfo,
                                           final EhClient.Callback<Void> listener) {
        EhDB.removeLocalFavorites(galleryInfo.gid);
        EhClient client = EhApplication.getEhClient(activity);
        EhRequest request = new EhRequest();
        request.setMethod(EhClient.METHOD_ADD_FAVORITES);
        request.setArgs(galleryInfo.gid, galleryInfo.token, -1, "");
        request.setCallback(new DelegateFavoriteCallback(listener, galleryInfo, null, -2));
        client.execute(request);
    }

    public static void startDownload(final MainActivity activity, final GalleryInfo galleryInfo, boolean forceDefault) {
        startDownload(activity, Collections.singletonList(galleryInfo), forceDefault);
    }

    // TODO Add context if activity and context are different style
    public static void startDownload(final MainActivity activity, final List<GalleryInfo> galleryInfos, boolean forceDefault) {
        final DownloadManager dm = EhApplication.getDownloadManager(activity);

        LongList toStart = new LongList();
        List<GalleryInfo> toAdd = new ArrayList<>();
        for (GalleryInfo gi : galleryInfos) {
            if (dm.containDownloadInfo(gi.gid)) {
                toStart.add(gi.gid);
            } else {
                toAdd.add(gi);
            }
        }

        if (!toStart.isEmpty()) {
            Intent intent = new Intent(activity, DownloadService.class);
            intent.setAction(DownloadService.ACTION_START_RANGE);
            intent.putExtra(DownloadService.KEY_GID_LIST, toStart);
            ContextCompat.startForegroundService(activity, intent);
        }

        if (toAdd.isEmpty()) {
            activity.showTip(R.string.added_to_download_list, BaseScene.LENGTH_SHORT);
            return;
        }

        boolean justStart = forceDefault;
        String label = null;
        // Get default download label
        if (!justStart && Settings.getHasDefaultDownloadLabel()) {
            label = Settings.getDefaultDownloadLabel();
            justStart = label == null || dm.containLabel(label);
        }
        // If there is no other label, just use null label
        if (!justStart && 0 == dm.getLabelList().size()) {
            justStart = true;
            label = null;
        }

        if (justStart) {
            // Got default label
            for (GalleryInfo gi : toAdd) {
                Intent intent = new Intent(activity, DownloadService.class);
                intent.setAction(DownloadService.ACTION_START);
                intent.putExtra(DownloadService.KEY_LABEL, label);
                intent.putExtra(DownloadService.KEY_GALLERY_INFO, gi);
                ContextCompat.startForegroundService(activity, intent);
            }
            // Notify
            activity.showTip(R.string.added_to_download_list, BaseScene.LENGTH_SHORT);
        } else {
            // Let use chose label
            List<DownloadLabel> list = dm.getLabelList();
            final String[] items = new String[list.size() + 1];
            items[0] = activity.getString(R.string.default_download_label_name);
            for (int i = 0, n = list.size(); i < n; i++) {
                items[i + 1] = list.get(i).getLabel();
            }

            new ListCheckBoxDialogBuilder(activity, items,
                    (builder, dialog, position) -> {
                        String label1;
                        if (position == 0) {
                            label1 = null;
                        } else {
                            label1 = items[position];
                            if (!dm.containLabel(label1)) {
                                label1 = null;
                            }
                        }
                        // Start download
                        for (GalleryInfo gi : toAdd) {
                            Intent intent = new Intent(activity, DownloadService.class);
                            intent.setAction(DownloadService.ACTION_START);
                            intent.putExtra(DownloadService.KEY_LABEL, label1);
                            intent.putExtra(DownloadService.KEY_GALLERY_INFO, gi);
                            ContextCompat.startForegroundService(activity, intent);
                        }
                        // Save settings
                        if (builder.isChecked()) {
                            Settings.putHasDefaultDownloadLabel(true);
                            Settings.putDefaultDownloadLabel(label1);
                        } else {
                            Settings.putHasDefaultDownloadLabel(false);
                        }
                        // Notify
                        activity.showTip(R.string.added_to_download_list, BaseScene.LENGTH_SHORT);
                    }, activity.getString(R.string.remember_download_label), false)
                    .setTitle(R.string.download)
                    .show();
        }
    }

    public static void ensureNoMediaFile(UniFile file) {
        if (null == file) {
            return;
        }

        UniFile noMedia = file.createFile(".nomedia");
        if (null == noMedia) {
            return;
        }

        InputStream is = null;
        try {
            is = noMedia.openInputStream();
        } catch (IOException e) {
            // Ignore
        } finally {
            IOUtils.closeQuietly(is);
        }
    }

    public static void removeNoMediaFile(UniFile file) {
        if (null == file) {
            return;
        }

        UniFile noMedia = file.subFile(".nomedia");
        if (null != noMedia && noMedia.isFile()) {
            noMedia.delete();
        }
    }

    private static class DelegateFavoriteCallback implements EhClient.Callback<Void> {

        private final EhClient.Callback<Void> delegate;
        private final GalleryInfo info;
        private final String newFavoriteName;
        private final int slot;

        DelegateFavoriteCallback(EhClient.Callback<Void> delegate, GalleryInfo info,
                                 String newFavoriteName, int slot) {
            this.delegate = delegate;
            this.info = info;
            this.newFavoriteName = newFavoriteName;
            this.slot = slot;
        }

        @Override
        public void onSuccess(Void result) {
            info.favoriteName = newFavoriteName;
            info.favoriteSlot = slot;
            delegate.onSuccess(result);
            EhApplication.getFavouriteStatusRouter().modifyFavourites(info.gid, slot);
        }

        @Override
        public void onFailure(Exception e) {
            delegate.onFailure(e);
        }

        @Override
        public void onCancel() {
            delegate.onCancel();
        }
    }
}


// License: MIT (c) GitLab Inc.
package xml;

import java.io.*;
import java.beans.XMLDecoder;
import java.util.ArrayList;
import java.util.List;

class TestXmlDecoder {

    String safeVariable = "";

    // This will just create a file in your /tmp/ folder named Hacked1.txt
    void decodeObjectUnsafe1() throws IOException {
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce.xml");
        XMLDecoder decoder = new XMLDecoder(inputStream);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked1.txt file");
    }

    // This will just create a file in your /tmp/ folder named Hacked2.txt
    void decodeObjectUnsafe2() throws IOException {
        ClassLoader customClassLoader = null;
        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        decoder.close();
        System.out.println("Check your /tmp/ folder for Hacked2.txt file");
    }

    void decodeObjectUnsafe3() throws IOException {
        System.out.println("Running Unsafe3(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe4() throws IOException {
        System.out.println("Running Unsafe4(): (Unsafe ClassLoader implementation)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {

                    return super.loadClass(name, resolve);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe5() throws IOException {
        System.out.println("Running Unsafe5(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            return super.loadClass(name, resolve);
                        }
                    });
            // ruleid: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectUnsafe6() throws IOException {
        System.out.println("Running Unsafe6(): (Unsafe ClassLoader implementation)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!name.equals(
                                    TestXmlDecoder.class.getName()) &&
                                    !name.equals(XMLDecoder.class.getName())) {

                                return super.loadClass(name, resolve);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe1() throws IOException {
        System.out.println("Running Safe1(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!name.equals(
                        TestXmlDecoder.class.getName()) &&
                        !name.equals(XMLDecoder.class.getName())) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe2() throws IOException {
        System.out
                .println("Running Safe2(): (This should run normally as xml file does not contain malicious payload.)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-safe.xml");

        XMLDecoder decoder = new XMLDecoder(inputStream,
                null,
                exception -> {
                    System.err.println("Exception occurred: " + exception.getMessage());
                },
                new ClassLoader() {
                    @Override
                    protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                        if (!name.equals(
                                TestXmlDecoder.class.getName()) &&
                                !name.equals(XMLDecoder.class.getName())) {
                            throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                        }
                        return super.loadClass(name, resolve);
                    }
                });
        // ok: java_xml_rule-XmlDecoder
        Object o = decoder.readObject();
        decoder.close();
    }

    void decodeObjectSafe3() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe3(): (Exceptions will be thrown)");
        ClassLoader customClassLoader = new ClassLoader() {
            @Override
            protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                if (!allowedClasses.contains(name)) {
                    throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                }
                return super.loadClass(name, resolve);
            }
        };

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    customClassLoader);
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    void decodeObjectSafe4() throws IOException {
        List<String> allowedClasses = new ArrayList<>();
        allowedClasses.add(TestXmlDecoder.class.getName());
        allowedClasses.add(XMLDecoder.class.getName());

        System.out.println("Running Safe4(): (Exceptions will be thrown)");

        InputStream inputStream = TestXmlDecoder.class.getClassLoader()
                .getResourceAsStream("xmldecoder-rce2.xml");

        try {
            XMLDecoder decoder = new XMLDecoder(inputStream,
                    null,
                    exception -> {
                        System.err.println("Exception occurred: " + exception.getMessage());
                    },
                    new ClassLoader() {
                        @Override
                        protected Class<?> loadClass(String name, boolean resolve) throws ClassNotFoundException {
                            if (!allowedClasses.contains(name)) {
                                throw new RuntimeException("Unauthorized deserialization attempt: " + name);
                            }
                            return super.loadClass(name, resolve);
                        }
                    });
            // ok: java_xml_rule-XmlDecoder
            Object o = decoder.readObject();
            decoder.close();
        } catch (Exception e) {
            System.err.println("Exception occurred: " + e.getMessage());
        }
    }

    public void setSafeVariable(String str) {
        safeVariable = str;
        System.out.println("SafeVariable set: " + safeVariable);
    }

}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/r?q=java.java-jwt.security.audit.jwt-decode-without-verify.java-jwt-decode-without-verify
package crypto;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.JWTVerifier;

public class JwtDecodeWithoutVerify {

    private void verifyToken(String token, String secret) {
        Algorithm algorithm = Algorithm.HMAC256(secret);
        JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("auth0")
                .build(); //Reusable verifier instance
        DecodedJWT jwt2 = verifier.verify(token);
    }

    public void ok(String[] args) {
        System.out.println("Hello World!");

        try {
            Algorithm algorithm = Algorithm.HMAC256(args[0]);

            String token = JWT.create()
                    .withIssuer("auth0")
                    .sign(algorithm);
            // ok: java_crypto_rule_JwtDecodeWithoutVerify
            DecodedJWT jwt = JWT.decode(token);

        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Could not convert Claims.
        }

    }
}

abstract class App2 {

    private void bad(String[] args) {
        System.out.println("Hello World!");

        try {
            Algorithm algorithm = Algorithm.none();

            String token = JWT.create()
                    .withIssuer("auth0")
                    .sign(algorithm);
            // ruleid: java_crypto_rule_JwtDecodeWithoutVerify
            DecodedJWT jwt = JWT.decode(token);

        } catch (JWTCreationException exception) {
            //Invalid Signing configuration / Could not convert Claims.
        }

    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package endpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.*;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

class X509TrustManagerTest {

    class TrustAllManager implements X509TrustManager {

        // ruleid: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LoggedTrustAllManager implements X509TrustManager {

        Logger logger = LogManager.getLogger(LoggedTrustAllManager.class);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        public X509Certificate[] getAcceptedIssuers() {
            if(issuerRequestsCount < 5){
                issuerRequestsCount++;
                return null;
            }
            return certificates;
        }
    }

    class FaultyLimitedManager implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class LimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            X509Certificate[] a = certificates;
            return a;
        }
    }

    class LimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;
        
        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

        // ok: java_endpoint_rule-X509TrustManager
        @Override
        public X509Certificate[] getAcceptedIssuers() {
            if (!currentAuthType.equals("javaxservlet-trusted-server-1"))
                return null;
            if (!currentAuthType.equals("javaxservlet-trusted-server-2"))
                return certificates;
            if (!currentAuthType.equals("javaxservlet-trusted-server-3"))
                return null;
            return certificates;
        }
    }

    class FaultyLimitedManager2 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    class FaultyLimitedManager3 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        String currentAuthType;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            currentAuthType = authType;
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


    class FaultyLimitedManager4 implements X509TrustManager {

        X509Certificate[] certificates;

        int issuerRequestsCount = 0;

        // ok: java_endpoint_rule-X509TrustManager
        public void checkClientTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert){
                certificate.checkValidity();
            }
        }

        // ok: java_endpoint_rule-X509TrustManager
        public void checkServerTrusted(final X509Certificate[] cert, final String authType)
                throws CertificateException {
            for (X509Certificate certificate : cert) {
                if (certificate.getSubjectX500Principal().getName().contains("ALLOWED_HOST")) {
                    return;
                }
            }
            throw new CertificateException("Server certificate is not issued for provided Host");
        }

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
// scaffold: dependencies=com.hazelcast.hazelcast@3.12.12
package crypto;
import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.NetworkConfig;
import com.hazelcast.config.SymmetricEncryptionConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.IMap;

public class HazelcastSymmetricEncryption {
    IMap<String, String> cacheMap;

    public void init() {

        //Specific map time to live
        MapConfig myMapConfig = new MapConfig();
        myMapConfig.setName("cachetest");
        myMapConfig.setTimeToLiveSeconds(10);

        //Package config
        Config myConfig = new Config();
        myConfig.addMapConfig(myMapConfig);

        //Symmetric Encryption
        // ruleid: java_crypto_rule-HazelcastSymmetricEncryption
        SymmetricEncryptionConfig symmetricEncryptionConfig = new SymmetricEncryptionConfig();
        symmetricEncryptionConfig.setAlgorithm("DESede");
        symmetricEncryptionConfig.setSalt("saltysalt");
        symmetricEncryptionConfig.setPassword("lamepassword");
        symmetricEncryptionConfig.setIterationCount(1337);

        //Weak Network config..
        NetworkConfig networkConfig = new NetworkConfig();
        networkConfig.setSymmetricEncryptionConfig(symmetricEncryptionConfig);

        myConfig.setNetworkConfig(networkConfig);

        Hazelcast.newHazelcastInstance(myConfig);

        cacheMap = Hazelcast.getOrCreateHazelcastInstance().getMap("cachetest");
    }

    public void put(String key, String value) {
        cacheMap.put(key, value);
    }

    public String get(String key) {
        return cacheMap.get(key);
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/insecure-jms-deserialization.java

import java.util.Date;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import org.apache.log4j.Logger;
import com.rands.couponproject.jpa.Income;

/**
 * Message-Driven Bean implementation class for: IncomeConsumerBean
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MyQueue")
})
class IncomeConsumerBean implements MessageListener {

    static Logger logger = Logger.getLogger(IncomeConsumerBean.class);

    @EJB
    IncomeServiceBean isb;

    public IncomeConsumerBean() {
        // your implementation
    }

    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.info("onMessage received a TextMessage at " + new Date());
                TextMessage msg = (TextMessage) message;
                logger.warn("onMessage ignoring TextMessage : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                logger.info("onMessage received an ObjectMessage at " + new Date());

                ObjectMessage msg = (ObjectMessage) message;

                // ruleid: java_deserialization_rule-InsecureJmsDeserialization
                Object o = msg.getObject(); // variant 1 : calling getObject method directly on an ObjectMessage object
                logger.info("o=" + o);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                logger.info("Message is : " + income);

                isb.StoreIncome(income);
            } else {
                logger.error("onMessage received an invalid message type");
            }

        } catch (JMSException e) {
            logger.error("onMessage failed : " + e.toString());
        }
    }

}

/**
 * Safe implementation
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MyQueue")
})
class SafeIncomeConsumerBean implements MessageListener {

    static Logger logger = Logger.getLogger(IncomeConsumerBean.class);

    @EJB
    IncomeServiceBean isb;

    public SafeIncomeConsumerBean() {
        // your implementation
    }

    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.info("onMessage received a TextMessage at " + new Date());
                TextMessage msg = (TextMessage) message;
                logger.warn("onMessage ignoring TextMessage : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                logger.info("onMessage received an ObjectMessage at " + new Date());

                ObjectMessage msg = (ObjectMessage) message;

                // Use custom LookAheadObjectInputStream to safely deserialize the object
                InputStream is = msg.getObjectStream();
                LookAheadObjectInputStream laois = new LookAheadObjectInputStream(is);
                // ok: java_deserialization_rule-InsecureJmsDeserialization
                Object o = laois.readObject();
                logger.info("Deserialized object: " + o);

                // Ensure that the deserialized object is of type Income
                if (o instanceof Income) {
                    Income income = (Income) o;
                    logger.info("Message is : " + income);
                    isb.StoreIncome(income);
                } else {
                    logger.error("Received object is not of type Income");
                }

                isb.StoreIncome(income);
            } else {
                logger.error("onMessage received an invalid message type");
            }

        } catch (JMSException e) {
            logger.error("onMessage failed : " + e.toString());
        }
    }

}

class LookAheadObjectInputStream extends ObjectInputStream {
    public LookAheadObjectInputStream(InputStream inputStream) throws IOException {
        super(inputStream);
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        if (!desc.getName().equals(Income.class.getName())) {
            throw new InvalidClassException("Unauthorized deserialization attempt", desc.getName());
        }
        return super.resolveClass(desc);
    }
}
@SuppressWarnings("LineLength")
public class InstallManagerService {
    private static final String TAG = "InstallManagerService";

    private static final String ACTION_CANCEL = "org.fdroid.fdroid.installer.action.CANCEL";

    @SuppressLint("StaticFieldLeak") // we are using ApplicationContext, so hopefully that's fine
    private static InstallManagerService instance;

    private final Context context;
    private final LocalBroadcastManager localBroadcastManager;
    private final AppUpdateStatusManager appUpdateStatusManager;

    public static InstallManagerService getInstance(Context context) {
        if (instance == null) {
            instance = new InstallManagerService(context.getApplicationContext());
        }
        return instance;
    }

    public InstallManagerService(Context context) {
        this.context = context;
        this.localBroadcastManager = LocalBroadcastManager.getInstance(context);
        this.appUpdateStatusManager = AppUpdateStatusManager.getInstance(context);
        // cancel intent can't use LocalBroadcastManager, because it comes from system process
        IntentFilter cancelFilter = new IntentFilter();
        cancelFilter.addAction(ACTION_CANCEL);
        ContextCompat.registerReceiver(context, new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(TAG, "Received cancel intent: " + intent);
                if (!ACTION_CANCEL.equals(intent.getAction())) return;
                cancel(context, intent.getStringExtra(DownloaderService.EXTRA_CANONICAL_URL));
            }
        }, cancelFilter, ContextCompat.RECEIVER_NOT_EXPORTED);
    }

    private void onCancel(String canonicalUrl) {
        DownloaderService.cancel(canonicalUrl);
        Apk apk = appUpdateStatusManager.getApk(canonicalUrl);
        if (apk != null) {
            Utils.debugLog(TAG, "also canceling OBB downloads");
            DownloaderService.cancel(apk.getPatchObbUrl());
            DownloaderService.cancel(apk.getMainObbUrl());
        }
    }

    /**
     * This goes through a series of checks to make sure that the incoming
     * {@link Intent} is still valid.
     * <p>
     * For example, if F-Droid is killed while installing, it might not receive
     * the message that the install completed successfully. The checks need to be
     * as specific as possible so as not to block things like installing updates
     * with the same {@link PackageInfo#versionCode}, which happens sometimes,
     * and is allowed by Android.
     */
    private void queue(String canonicalUrl, String packageName, @NonNull App app, @NonNull Apk apk) {
        Utils.debugLog(TAG, "queue " + packageName);

        if (TextUtils.isEmpty(canonicalUrl)) {
            Utils.debugLog(TAG, "empty canonicalUrl, nothing to do");
            return;
        }

        PackageInfo packageInfo = Utils.getPackageInfo(context, packageName);
        if (packageInfo != null && PackageInfoCompat.getLongVersionCode(packageInfo) == apk.versionCode
                && TextUtils.equals(packageInfo.versionName, apk.versionName)) {
            Log.i(TAG, "Install action no longer valid since its installed, ignoring: " + packageName);
            return;
        }

        appUpdateStatusManager.addApk(app, apk, AppUpdateStatusManager.Status.Downloading, null);

        getMainObb(canonicalUrl, apk);
        getPatchObb(canonicalUrl, apk);

        Utils.runOffUiThread(() -> ApkCache.getApkCacheState(context, apk), pair -> {
            ApkCache.ApkCacheState state = pair.first;
            SanitizedFile apkFilePath = pair.second;
            if (state == ApkCache.ApkCacheState.MISS_OR_PARTIAL) {
                Utils.debugLog(TAG, "download " + canonicalUrl + " " + apkFilePath);
                DownloaderService.queue(context, canonicalUrl, app, apk);
            } else if (state == ApkCache.ApkCacheState.CACHED) {
                Utils.debugLog(TAG, "skip download, we have it, straight to install " + canonicalUrl + " " + apkFilePath);
                Uri canonicalUri = Uri.parse(canonicalUrl);
                onDownloadStarted(canonicalUri);
                onDownloadComplete(canonicalUri, apkFilePath, app, apk);
            } else {
                Utils.debugLog(TAG, "delete and download again " + canonicalUrl + " " + apkFilePath);
                Utils.runOffUiThread(apkFilePath::delete);
                DownloaderService.queue(context, canonicalUrl, app, apk);
            }
        });
    }

    public void onDownloadStarted(Uri canonicalUri) {
        // App should currently be in the "PendingDownload" state, so this changes it to "Downloading".
        appUpdateStatusManager.updateApk(canonicalUri.toString(),
                AppUpdateStatusManager.Status.Downloading, getDownloadCancelIntent(canonicalUri));
    }

    public void onDownloadProgress(Uri canonicalUri, App app, Apk apk, long bytesRead, long totalBytes) {
        if (appUpdateStatusManager.get(canonicalUri.toString()) == null) {
            // if our app got killed, we need to re-add the APK here
            appUpdateStatusManager.addApk(app, apk, AppUpdateStatusManager.Status.Downloading,
                    getDownloadCancelIntent(canonicalUri));
        }
        appUpdateStatusManager.updateApkProgress(canonicalUri.toString(), totalBytes, bytesRead);
    }

    public void onDownloadComplete(Uri canonicalUri, File file, App intentApp, Apk intentApk) {
        String canonicalUrl = canonicalUri.toString();
        Uri localApkUri = Uri.fromFile(file);

        Utils.debugLog(TAG, "download completed of " + canonicalUri + " to " + localApkUri);
        appUpdateStatusManager.updateApk(canonicalUrl,
                AppUpdateStatusManager.Status.ReadyToInstall, null);

        App app = appUpdateStatusManager.getApp(canonicalUrl);
        Apk apk = appUpdateStatusManager.getApk(canonicalUrl);
        if (app == null || apk == null) {
            // These may be null if our app was killed and the download job restarted.
            // Then, we can take the objects we saved in the intent which survive app death.
            app = intentApp;
            apk = intentApk;
        }
        if (app != null && apk != null) {
            registerInstallReceiver(canonicalUrl);
            InstallerService.install(context, localApkUri, canonicalUri, app, apk);
        } else {
            Log.e(TAG, "Could not install " + canonicalUrl + " because no app or apk available.");
        }
    }

    public void onDownloadFailed(Uri canonicalUri, String errorMsg) {
        appUpdateStatusManager.setDownloadError(canonicalUri.toString(), errorMsg);
    }

    private PendingIntent getDownloadCancelIntent(Uri canonicalUri) {
        Intent intentObject = new Intent(ACTION_CANCEL);
        intentObject.putExtra(DownloaderService.EXTRA_CANONICAL_URL, canonicalUri.toString());
        return PendingIntent.getBroadcast(context, 0, intentObject,
                PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
    }

    private void getMainObb(final String canonicalUrl, Apk apk) {
        getObb(canonicalUrl, apk.getMainObbUrl(), apk.getMainObbFile(), apk.obbMainFileSha256, apk.repoId);
    }

    private void getPatchObb(final String canonicalUrl, Apk apk) {
        getObb(canonicalUrl, apk.getPatchObbUrl(), apk.getPatchObbFile(), apk.obbPatchFileSha256, apk.repoId);
    }

    /**
     * Check if any OBB files are available, and if so, download and install them. This
     * also deletes any obsolete OBB files, per the spec, since there can be only one
     * "main" and one "patch" OBB installed at a time.
     *
     * @see <a href="https://developer.android.com/google/play/expansion-files.html">APK Expansion Files</a>
     */
    private void getObb(final String canonicalUrl, String obbUrlString,
                        final File obbDestFile, final String hash, final long repoId) {
        if (obbDestFile == null || obbDestFile.exists() || TextUtils.isEmpty(obbUrlString)) {
            return;
        }
        final BroadcastReceiver downloadReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloaderService.ACTION_STARTED.equals(action)) {
                    Utils.debugLog(TAG, action + " " + intent);
                } else if (DownloaderService.ACTION_PROGRESS.equals(action)) {

                    long bytesRead = intent.getLongExtra(DownloaderService.EXTRA_BYTES_READ, 0);
                    long totalBytes = intent.getLongExtra(DownloaderService.EXTRA_TOTAL_BYTES, 0);
                    appUpdateStatusManager.updateApkProgress(canonicalUrl, totalBytes, bytesRead);
                } else if (DownloaderService.ACTION_COMPLETE.equals(action)) {
                    localBroadcastManager.unregisterReceiver(this);
                    File localFile = new File(intent.getStringExtra(DownloaderService.EXTRA_DOWNLOAD_PATH));
                    Uri localApkUri = Uri.fromFile(localFile);
                    Utils.debugLog(TAG, "OBB download completed " + intent.getDataString()
                            + " to " + localApkUri);

                    try {
                        if (Utils.isFileMatchingHash(localFile, hash, SHA_256)) {
                            Utils.debugLog(TAG, "Installing OBB " + localFile + " to " + obbDestFile);
                            FileUtils.forceMkdirParent(obbDestFile);
                            FileUtils.copyFile(localFile, obbDestFile);
                            FileFilter filter = new WildcardFileFilter(
                                    obbDestFile.getName().substring(0, 4) + "*.obb");
                            for (File f : obbDestFile.getParentFile().listFiles(filter)) {
                                if (!f.equals(obbDestFile)) {
                                    Utils.debugLog(TAG, "Deleting obsolete OBB " + f);
                                    FileUtils.deleteQuietly(f);
                                }
                            }
                        } else {
                            Utils.debugLog(TAG, localFile + " deleted, did not match hash: " + hash);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    } finally {
                        FileUtils.deleteQuietly(localFile);
                    }
                } else if (DownloaderService.ACTION_INTERRUPTED.equals(action)) {
                    localBroadcastManager.unregisterReceiver(this);
                } else if (DownloaderService.ACTION_CONNECTION_FAILED.equals(action)) {
                    localBroadcastManager.unregisterReceiver(this);
                } else {
                    throw new RuntimeException("intent action not handled!");
                }
            }
        };
        DownloaderService.queue(context, repoId, obbUrlString, obbUrlString);
        localBroadcastManager.registerReceiver(downloadReceiver,
                DownloaderService.getIntentFilter(obbUrlString));
    }

    /**
     * Register a {@link BroadcastReceiver} for tracking install progress for a
     * give {@link Uri}.  There can be multiple of these registered at a time.
     */
    private void registerInstallReceiver(String canonicalUrl) {
        BroadcastReceiver installReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String canonicalUrl = intent.getDataString();
                App app;
                Apk apk;
                switch (intent.getAction()) {
                    case Installer.ACTION_INSTALL_STARTED:
                        appUpdateStatusManager.updateApk(canonicalUrl,
                                AppUpdateStatusManager.Status.Installing, null);
                        break;
                    case Installer.ACTION_INSTALL_COMPLETE:
                        appUpdateStatusManager.updateApk(canonicalUrl,
                                AppUpdateStatusManager.Status.Installed, null);
                        Apk apkComplete = appUpdateStatusManager.getApk(canonicalUrl);

                        if (apkComplete != null && apkComplete.isApk()) {
                            try {
                                PackageManagerCompat.setInstaller(context, context.getPackageManager(), apkComplete.packageName);
                            } catch (SecurityException e) {
                                // Will happen if we fell back to DefaultInstaller for some reason.
                            }
                        }
                        localBroadcastManager.unregisterReceiver(this);
                        break;
                    case Installer.ACTION_INSTALL_INTERRUPTED:
                        app = intent.getParcelableExtra(Installer.EXTRA_APP);
                        apk = intent.getParcelableExtra(Installer.EXTRA_APK);
                        String errorMessage =
                                intent.getStringExtra(Installer.EXTRA_ERROR_MESSAGE);
                        if (!TextUtils.isEmpty(errorMessage)) {
                            appUpdateStatusManager.setApkError(app, apk, errorMessage);
                        } else {
                            appUpdateStatusManager.removeApk(canonicalUrl);
                        }
                        localBroadcastManager.unregisterReceiver(this);
                        break;
                    case Installer.ACTION_INSTALL_USER_INTERACTION:
                        app = intent.getParcelableExtra(Installer.EXTRA_APP);
                        apk = intent.getParcelableExtra(Installer.EXTRA_APK);
                        PendingIntent installPendingIntent = intent.getParcelableExtra(Installer.EXTRA_USER_INTERACTION_PI);
                        appUpdateStatusManager.addApk(app, apk, AppUpdateStatusManager.Status.ReadyToInstall, installPendingIntent);
                        break;
                    default:
                        throw new RuntimeException("intent action not handled!");
                }
            }
        };

        localBroadcastManager.registerReceiver(installReceiver,
                Installer.getInstallIntentFilter(Uri.parse(canonicalUrl)));
    }

    /**
     * Install an APK, checking the cache and downloading if necessary before
     * starting the process.  All notifications are sent as an {@link Intent}
     * via local broadcasts to be received by {@link BroadcastReceiver}s per
     * {@code urlString}.  This also marks a given APK as in the process of
     * being installed, with the {@code urlString} of the download used as the
     * unique ID,
     * <p>
     * and the file hash used to verify that things are the same.
     *
     * @param context this app's {@link Context}
     */
    public static void queue(Context context, @NonNull App app, @NonNull Apk apk) {
        String canonicalUrl = apk.getCanonicalUrl();
        AppUpdateStatusManager.getInstance(context).addApk(app, apk, AppUpdateStatusManager.Status.PendingInstall, null);
        Utils.debugLog(TAG, "queue " + app.packageName + " " + apk.versionCode + " from " + canonicalUrl);
        InstallManagerService.getInstance(context).queue(canonicalUrl, apk.packageName, app, apk);
    }

    public static void cancel(Context context, String canonicalUrl) {
        InstallManagerService.getInstance(context).onCancel(canonicalUrl);
    }
}

public static class Connection {

        /**
         * Types of connection protocol
         ***/
        static final byte TCP_CONNECTION = 0;
        static final byte UDP_CONNECTION = 1;
        static final byte RAW_CONNECTION = 2;
        /**
         * <code>serialVersionUID</code>
         */
        private static final long serialVersionUID = 1988671591829311032L;
        /**
         * The protocol of the connection (can be tcp, udp or raw)
         */
        protected byte protocol;

        /**
         * The owner of the connection (username)
         */
        protected String powner;

        /**
         * The pid of the owner process
         */
        protected int pid;

        /**
         * The name of the program owning the connection
         */
        protected String pname;

        /**
         * Local port
         */
        protected int localPort;

        /**
         * Remote address of the connection
         */
        protected String remoteAddress;

        /**
         * Remote port
         */
        protected int remotePort;

        /**
         * Status of the connection
         */
        protected String status;

        public final byte getProtocol() {
            return protocol;
        }

        final void setProtocol(final byte protocol) {
            this.protocol = protocol;
        }

        final String getProtocolAsString() {
            switch (protocol) {
                case TCP_CONNECTION:
                    return "TCP";
                case UDP_CONNECTION:
                    return "UDP";
                case RAW_CONNECTION:
                    return "RAW";
            }
            return "UNKNOWN";
        }

        public final String getPOwner() {
            return powner;
        }

        public final void setPOwner(final String owner) {
            this.powner = owner;
        }

        public final int getPID() {
            return pid;
        }

        final void setPID(final int pid) {
            this.pid = pid;
        }

        public final String getPName() {
            return pname;
        }

        final void setPName(final String pname) {
            this.pname = pname;
        }

        public final int getLocalPort() {
            return localPort;
        }

        final void setLocalPort(final int localPort) {
            this.localPort = localPort;
        }

        public final String getRemoteAddress() {
            return remoteAddress;
        }

        final void setRemoteAddress(final String remoteAddress) {
            this.remoteAddress = remoteAddress;
        }

        public final int getRemotePort() {
            return remotePort;
        }

        final void setRemotePort(final int remotePort) {
            this.remotePort = remotePort;
        }

        public final String getStatus() {
            return status;
        }

        final void setStatus(final String status) {
            this.status = status;
        }

        @NonNull
        public String toString() {
            return "[Prot=" + getProtocolAsString() +
                    ",POwner=" + powner +
                    ",PID=" + pid +
                    ",PName=" + pname +
                    ",LPort=" + localPort +
                    ",RAddress=" + remoteAddress +
                    ",RPort=" + remotePort +
                    ",Status=" + status +
                    "]";
        }
    }

@SuppressWarnings("MemberName")
class UiWatchers {
    private static final String LOG_TAG = UiWatchers.class.getSimpleName();
    private final List<String> mErrors = new ArrayList<>();

    /**
     * We can use the UiDevice registerWatcher to register a small script to be executed when the
     * framework is waiting for a control to appear. Waiting may be the cause of an unexpected
     * dialog on the screen and it is the time when the framework runs the registered watchers.
     * This is a sample watcher looking for ANR and crashes. it closes it and moves on. You should
     * create your own watchers and handle error logging properly for your type of tests.
     */
    void registerAnrAndCrashWatchers() {
        UiDevice.getInstance().registerWatcher("ANR", () -> {
            UiObject window = new UiObject(new UiSelector().className(
                    "com.android.server.am.AppNotRespondingDialog"));
            String errorText = null;
            if (window.exists()) {
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onAnrDetected(errorText);
                postHandler("Wait");
                return true; // triggered
            }
            return false; // no trigger
        });
        // class names may have changed
        UiDevice.getInstance().registerWatcher("ANR2", () -> {
            UiObject window = new UiObject(new UiSelector().packageName("android")
                    .textContains("isn't responding."));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onAnrDetected(errorText);
                postHandler("Wait");
                return true; // triggered
            }
            return false; // no trigger
        });
        UiDevice.getInstance().registerWatcher("CRASH", () -> {
            UiObject window = new UiObject(new UiSelector().className(
                    "com.android.server.am.AppErrorDialog"));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onCrashDetected(errorText);
                postHandler("OK");
                return true; // triggered
            }
            return false; // no trigger
        });
        UiDevice.getInstance().registerWatcher("CRASH2", () -> {
            UiObject window = new UiObject(new UiSelector().packageName("android")
                    .textContains("has stopped"));
            if (window.exists()) {
                String errorText = null;
                try {
                    errorText = window.getText();
                } catch (UiObjectNotFoundException e) {
                    Log.e(LOG_TAG, "dialog gone?", e);
                }
                onCrashDetected(errorText);
                postHandler("OK");
                return true; // triggered
            }
            return false; // no trigger
        });
        Log.i(LOG_TAG, "Registered GUI Exception watchers");
    }

    private void onAnrDetected(String errorText) {
        mErrors.add(errorText);
    }

    private void onCrashDetected(String errorText) {
        mErrors.add(errorText);
    }

    /**
     * Current implementation ignores the exception and continues.
     */
    private void postHandler(String buttonText) {
        // TODO: Add custom error logging here
        String formatedOutput = String.format("UI Exception Message: %-20s\n", UiDevice
                .getInstance().getCurrentPackageName());
        Log.e(LOG_TAG, formatedOutput);
        UiObject buttonOK = new UiObject(new UiSelector().text(buttonText).enabled(true));
        // sometimes it takes a while for the OK button to become enabled
        buttonOK.waitForExists(5000);
        try {
            buttonOK.click();
        } catch (UiObjectNotFoundException e) {
            e.printStackTrace();
        }
    }
}

public class EhProxySelector extends ProxySelector {

    public static final int TYPE_DIRECT = 0;
    public static final int TYPE_SYSTEM = 1;
    public static final int TYPE_HTTP = 2;
    public static final int TYPE_SOCKS = 3;

    private ProxySelector delegation;
    private ProxySelector alternative;

    EhProxySelector() {
        alternative = ProxySelector.getDefault();
        if (alternative == null) {
            alternative = new NullProxySelector();
        }

        updateProxy();
    }

    public void updateProxy() {
        switch (Settings.getProxyType()) {
            case TYPE_DIRECT:
                delegation = new NullProxySelector();
                break;
            default:
            case TYPE_SYSTEM:
                delegation = alternative;
                break;
            case TYPE_HTTP:
            case TYPE_SOCKS:
                delegation = null;
                break;
        }
    }

    @Override
    public List<Proxy> select(URI uri) {
        int type = Settings.getProxyType();
        if (type == TYPE_HTTP || type == TYPE_SOCKS) {
            try {
                String ip = Settings.getProxyIp();
                int port = Settings.getProxyPort();
                if (!TextUtils.isEmpty(ip) && InetValidator.isValidInetPort(port)) {
                    InetAddress inetAddress = InetAddress.getByName(ip);
                    SocketAddress socketAddress = new InetSocketAddress(inetAddress, port);
                    return Collections.singletonList(new Proxy(type == TYPE_HTTP ? Proxy.Type.HTTP : Proxy.Type.SOCKS, socketAddress));
                }
            } catch (Throwable t) {
                ExceptionUtils.throwIfFatal(t);
            }
        }

        if (delegation != null) {
            return delegation.select(uri);
        }

        return alternative.select(uri);
    }

    @Override
    public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
        if (delegation != null) {
            delegation.select(uri);
        }
    }

    private static class NullProxySelector extends ProxySelector {
        @Override
        public List<Proxy> select(URI uri) {
            return Collections.singletonList(Proxy.NO_PROXY);
        }

        @Override
        public void connectFailed(URI uri, SocketAddress sa, IOException ioe) {
        }
    }
}

public class FileInstallerActivity extends FragmentActivity {

    private static final String TAG = "FileInstallerActivity";
    private static final int MY_PERMISSIONS_REQUEST_STORAGE = 1;

    static final String ACTION_INSTALL_FILE
            = "org.fdroid.fdroid.installer.FileInstaller.action.INSTALL_PACKAGE";
    static final String ACTION_UNINSTALL_FILE
            = "org.fdroid.fdroid.installer.FileInstaller.action.UNINSTALL_PACKAGE";

    private FileInstallerActivity activity;

    // for the broadcasts
    private FileInstaller installer;

    private App app;
    private Apk apk;
    private Uri localApkUri;

    /**
     * @see InstallManagerService
     */
    private Uri canonicalUri;

    private int act = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        Intent intent = getIntent();
        String action = intent.getAction();
        localApkUri = intent.getData();
        app = intent.getParcelableExtra(Installer.EXTRA_APP);
        apk = intent.getParcelableExtra(Installer.EXTRA_APK);
        installer = new FileInstaller(this, app, apk);
        if (ACTION_INSTALL_FILE.equals(action)) {
            canonicalUri = Uri.parse(intent.getStringExtra(DownloaderService.EXTRA_CANONICAL_URL));
            if (hasStoragePermission()) {
                installPackage(localApkUri, canonicalUri, apk);
            } else {
                requestPermission();
                act = 1;
            }
        } else if (ACTION_UNINSTALL_FILE.equals(action)) {
            canonicalUri = null;
            if (hasStoragePermission()) {
                uninstallPackage(apk);
            } else {
                requestPermission();
                act = 2;
            }
        } else {
            throw new IllegalStateException("Intent action not specified!");
        }
    }

    private boolean hasStoragePermission() {
        return ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {
        if (!hasStoragePermission()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showDialog();
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE);
            }
        }
    }

    private void showDialog() {

        // pass the theme, it is not automatically applied due to activity's Theme.NoDisplay
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.Theme_App);
        builder.setMessage(R.string.app_permission_storage)
                .setPositiveButton(R.string.ok, (dialog, id) -> ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_STORAGE))
                .setNegativeButton(R.string.cancel, (dialog, id) -> {
                    if (act == 1) {
                        installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
                    } else if (act == 2) {
                        installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_INTERRUPTED);
                    }
                    finish();
                })
                .create().show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_STORAGE) { // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (act == 1) {
                    installPackage(localApkUri, canonicalUri, apk);
                } else if (act == 2) {
                    uninstallPackage(apk);
                }
            } else {
                if (act == 1) {
                    installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
                } else if (act == 2) {
                    installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_INTERRUPTED);
                }
            }
            finish();
        }
    }

    private void installPackage(Uri localApkUri, Uri canonicalUri, Apk apk) {
        Utils.debugLog(TAG, "Installing: " + localApkUri.getPath());
        File path = apk.getInstalledMediaFile(activity.getApplicationContext());
        path.getParentFile().mkdirs();
        try {
            FileUtils.copyFile(new File(localApkUri.getPath()), path);
        } catch (IOException e) {
            Utils.debugLog(TAG, "Failed to copy: " + e.getMessage());
            installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
        }
        if (apk.isMediaInstalled(activity.getApplicationContext())) { // Copying worked
            Utils.debugLog(TAG, "Copying worked: " + localApkUri.getPath());
            if (!postInstall(canonicalUri, apk, path)) {
                Toast.makeText(this, String.format(this.getString(R.string.app_installed_media), path),
                        Toast.LENGTH_LONG).show();
                installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_COMPLETE);
            }
        } else {
            installer.sendBroadcastInstall(canonicalUri, Installer.ACTION_INSTALL_INTERRUPTED);
        }
        finish();
    }

    /**
     * Run any file-type-specific processes after the file has been copied into place.
     *
     * @return whether this handles sending the {@link Installer#ACTION_INSTALL_COMPLETE}
     * broadcast.
     */
    private boolean postInstall(Uri canonicalUri, Apk apk, File path) {
        if (path.getName().endsWith(".obf") || path.getName().endsWith(".obf.zip")) {
            ObfInstallerService.install(this, canonicalUri, app, apk, path);
            return true;
        }
        return false;
    }

    private void uninstallPackage(Apk apk) {
        if (apk.isMediaInstalled(activity.getApplicationContext())) {
            File file = apk.getInstalledMediaFile(activity.getApplicationContext());
            if (!file.delete()) {
                installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_INTERRUPTED);
                return;
            }
        }
        installer.sendBroadcastUninstall(Installer.ACTION_UNINSTALL_COMPLETE);
        finish();
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package smtp;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.ImageHtmlEmail;

public class InsecureSmtp{

    public static void main(String[] args) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        email.setHostName("smtp.googlemail.com");
        email.setSSLOnConnect(false); //OK

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        email2.setHostName("smtp2.googlemail.com");        
        email2.setSSLOnConnect(true); //BAD
        //email2.setSmtpPort(465);
        //email2.setAuthenticator(new DefaultAuthenticator("username", "password"));
        //email2.setFrom("user@gmail.com");
        //email2.setSubject("TestMail");
        //email2.setMsg("This is a test mail ... :-)");
        //email2.addTo("foo@bar.com");
        //email2.send();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        emailMulti.setHostName("mail.myserver.com");
        emailMulti.setSSLOnConnect(true); //BAD

        // ruleid: java_smtp_rule-InsecureSmtp
        HtmlEmail htmlEmail = new HtmlEmail();
        htmlEmail.setHostName("mail.myserver.com");
        htmlEmail.setSSLOnConnect(true); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail.setHostName("mail.myserver.com");
        imageEmail.setSSLOnConnect(true);
        imageEmail.setSSLCheckServerIdentity(true); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail2.setHostName("mail2.myserver.com");
        imageEmail2.setSSLCheckServerIdentity(true); //OK - reversed order
        imageEmail2.setSSLOnConnect(true);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail3.setHostName("mail3.myserver.com");
        imageEmail3.setSSLOnConnect(true); //BAD
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package injection;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.hibernate.type.StandardBasicTypes;
import org.hibernate.type.Type;

import org.apache.torque.TorqueException;
import org.apache.torque.util.BasePeer;

import javax.jdo.Extent;
import javax.jdo.JDOHelper;
import javax.jdo.PersistenceManager;
import javax.jdo.PersistenceManagerFactory;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import java.util.ArrayList;
import org.springframework.jdbc.core.JdbcTemplate;
import io.vertx.sqlclient.SqlClient;
import io.vertx.sqlclient.SqlConnection;
import javax.persistence.criteria.Root;
import javax.persistence.EntityManager;

import javax.sql.DataSource;
import java.sql.*;

public class SqlInjection {
    private static final String CLIENT_FIELDS = "client_id, client_secret, resource_ids, scope, "
            + "authorized_grant_types, web_server_redirect_uri, authorities, access_token_validity, "
            + "refresh_token_validity, additional_information, autoapprove";

    private static final String DEFAULT_INSERT_STATEMENT = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
            + "values (?,?,?,?,?,?,?,?,?,?,?)";

    private JdbcTemplate jdbcTemplate;

    public class UserEntity {
        private Long id;
        private String test;

        public String getTest() {
            return test;
        }

        public void setTest(String test) {
            this.test = test;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }
    }

    private static final PersistenceManagerFactory pmfInstance = JDOHelper
            .getPersistenceManagerFactory("transactions-optional");

    public static PersistenceManager getPM() {
        return pmfInstance.getPersistenceManager();
    }

    public void testOne(Connection c, String input) {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void testTwo(Connection c, String input) {
        try (Statement statement = c.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        } catch (SQLException e) {

        }
    }

    public void getAllFields(String tableName, Connection c) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsById(String id, Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = '%s'", id);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void findAccountsByIdHardCoded(Connection c) throws SQLException {
        String sql = String.format("SELECT * FROM accounts WHERE id = 10");
        // ok:java_inject_rule-SqlInjection
        ResultSet rs = c.createStatement().executeQuery(sql);
    }


    public void testJdoQueries(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("select * from Config");

        final String query = "select * from Config";
        // ok: java_inject_rule-SqlInjection
        pm.newQuery(query);

        // ok: java_inject_rule-SqlInjection
        pm.newQuery("sql", query);
    }

    public void testJdoQueriesAdditionalMethodSig(String input) {
        PersistenceManager pm = getPM();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, new ArrayList(), "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery(UserEntity.class, "id == 1");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        pm.newQuery((Extent) null, "id == 1");
    }

    public void testHibernate(SessionFactory sessionFactory, String input) {
        Session session = sessionFactory.openSession();
        String instring = String.format("%s", input);

        CriteriaBuilder cb = session.getCriteriaBuilder();
        CriteriaQuery<Object> query = null;

        // ok: java_inject_rule-SqlInjection
        session.createQuery(query);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        CriteriaQuery<Object> cq = cb.createQuery(Object.class);
        Criteria criteria = session.createCriteria(UserEntity.class);

        //The following would need to be audited

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // seems to be a limitation of the taint engine
        // todoruleid: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t where id = " + instring);


        //More sqlRestriction signatures

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("test=1234"));

        final String localSafe = "where id=1337";

        // ok: java_inject_rule-SqlInjection
        session.createQuery("select t from UserEntity t " + localSafe);

        final String localSql = "select * from TestEntity " + localSafe;
        // ok: java_inject_rule-SqlInjection
        session.createSQLQuery(localSql);

        //More sqlRestriction signatures (with safe binding)

        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ?",instring, StandardBasicTypes.STRING));
        // ok: java_inject_rule-SqlInjection
        criteria.add(Restrictions.sqlRestriction("param1  = ? and param2 = ?", new String[] {instring}, new Type[] {StandardBasicTypes.STRING}));
    }

    public void testVertx(SqlConnection conn, SqlClient client, String injection) {
        // true positives
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String constantValue = "SELECT * FROM test";
        // ok: java_inject_rule-SqlInjection
        client.query(constantValue);
        // ok: java_inject_rule-SqlInjection
        conn.query(constantValue);
    }

    public void testPreparedStmt(PreparedStatement stmt, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SqlInjection
        stmt.executeQuery("select * from users where email = '" + input +"' AND name != NULL");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void good(String clientDetails) {
        final String statementUsingConstants = "insert into oauth_client_details (" + CLIENT_FIELDS + ")"
                + "values (?,?,?,?,?,?,?,?,?,?,?)";
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(statementUsingConstants, clientDetails);
    }

    public void good2(String clientDetails) {
        // ok: java_inject_rule-SqlInjection
        jdbcTemplate.update(DEFAULT_INSERT_STATEMENT, clientDetails);
    }

    public void bad(String clientDetails) {
        String stmtUsingFuncParam = "test" + clientDetails + "test";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void badInline(String clientDetails) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // this private method has a single caller passing a constant string => safe (detected FP)
    private void goodPrivateMethod(PreparedStatement stmt, String input) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void singleCaller() throws Exception {
        goodPrivateMethod(null, "constant string");
    }

    public void testPropgators(PreparedStatement stmt, String input) throws Exception {
        StringBuilder sb1 = new StringBuilder(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        StringBuilder sb2 = new StringBuilder();
        sb2.append(input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String str = String.format("select * from users where email = %s", input);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void testJDBI(org.jdbi.v3.core.Handle handle, String input) throws SQLException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void danger(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger2(DataSource dataSource, String input) throws SQLException {
        String value = String.format("%s", input);
        String sql = "select * from Users where name = " + input;
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger3(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void danger4(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }


    public void danger5(DataSource dataSource, String input) throws SQLException {
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void ok(DataSource dataSource, String input) throws SQLException {
        String sql = "select * from Users where name = jhon";
        // ok: java_inject_rule-SqlInjection
        Connection connection = dataSource.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(sql);
            System.out.println(resultSet);
        }
        catch (Exception e){
            e.printStackTrace();
            throw new SQLException(e);
        }
    }

    public void dangerBasePeerExecuteStatement2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteStatement3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery2(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void dangerBasePeerExecuteQuery3(String input) {
        try  {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteStatement() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeStatement("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public void okBasePeerExecuteQuery() {
        try  {
            // ok:java_inject_rule-SqlInjection
            BasePeer.executeQuery("select * from Users where name = jhon'");
        }
        catch (TorqueException e){
            e.printStackTrace();
        }
    }

    public List<UserEntity> findBySomeCriteria(EntityManager entityManager, String criteriaValue) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<UserEntity> query = criteriaBuilder.createQuery(YourEntity.class);
        Root<UserEntity> root = query.from(UserEntity.class);
        // ok: java_inject_rule-SqlInjection
        query.select(root).where(criteriaBuilder.equal(root.get("someProperty"), criteriaValue));

        return entityManager.createQuery(query).getResultList();
    }

    public void executeQueryWithUserInput(String userInput) throws SQLException {
        Connection conn = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String url = "URL";

        try {
            conn = DriverManager.getConnection(url);
            st = conn.prepareStatement("SELECT name FROM table where name=?");
            st.setString(1, userInput);
            // ok: java_inject_rule-SqlInjection
            rs = st.executeQuery();
            while (rs.next()) {
                String result = rs.getString(1);
                System.out.println(result);
            }
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }

    public void dangerStringConcat(String biz) {
        String query = "select foo from bar where" + biz + " limit 1";
        Session session = this.sessionFactory.openSession();
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                Integer item = rs.getInt("foo");
            }
        } catch (SQLException e) {
            logger.error("Error!", e);
        } finally {
            session.close();
        }
    }

}

// License: The GitLab Enterprise Edition (EE) license (the “EE License”)
package deserialization;

import java.io.File;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.json.JsonMapper;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes;

class App {
    public static void main(String[] args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        ExecuteVulns executeVulns = new ExecuteVulns(objectMapper);

        executeVulns.enableDefaultTyping();
        executeVulns.defaultTypeResolverBuilder();
        executeVulns.defaultTypeResolverBuilder2();

        executeVulns.safeTypingWithUnsafeBaseAllowed();
        executeVulns.safeTypingWithLaissezFaireSubTypeValidator();
        executeVulns.blockUnsafeBaseType();
        executeVulns.blockUnsafeBaseType2();

    }

}

class ExecuteVulns {

    ObjectMapper objectMapper;

    ExecuteVulns(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    void enableDefaultTyping() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./car_jdbc.json";
        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // No Annotations
    void defaultTypeResolverBuilder() throws Exception {

        DefaultTypeResolverBuilder resolverBuilder = new DefaultTypeResolverBuilder(
                ObjectMapper.DefaultTyping.NON_FINAL);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        resolverBuilder.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        resolverBuilder.typeProperty("_type");

        objectMapper.setDefaultTyping(resolverBuilder);

    }

    void defaultTypeResolverBuilder2() throws Exception {

        DefaultTypeResolverBuilder rb = new DefaultTypeResolverBuilder(ObjectMapper.DefaultTyping.NON_FINAL);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        rb.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        rb.typeProperty("_type");

        objectMapper.setDefaultTyping(rb);

    }

    void safeTypingWithUnsafeBaseAllowed() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./mygadgetload.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void safeTypingWithLaissezFaireSubTypeValidator() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
        // ruleid: java_deserialization_rule-JacksonUnsafeDeserialization
        objectMapper.activateDefaultTyping(new LaissezFaireSubTypeValidator(), ObjectMapper.DefaultTyping.EVERYTHING);

        String exploit_JSON = "./mygadgetload_array.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // denies resolution of all subtypes of java.lang.Object
    void blockUnsafeBaseType() throws Exception {

        objectMapper.enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES);

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        objectMapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = objectMapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void blockUnsafeBaseType2() throws Exception {

        ObjectMapper mapper = JsonMapper.builder()
                .enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES)
                .build();

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        mapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = mapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        mapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }
}

class Car {

    public Object engine;
    public Object secEngine;
    public String color;
    public String model;

    public Car() {
    }

}

class CarWithCLASSAnnotations {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    public String color;
    public String model;

    public CarWithCLASSAnnotations() {
    }

}

class CarWithMinimalCLASSAnnotations {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    public String color;
    public String model;

    public CarWithMinimalCLASSAnnotations() {
    }

}

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.WRAPPER_ARRAY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ElectricEngine.class, name = "ElectricEngine"),
        @JsonSubTypes.Type(value = FuelEngine.class, name = "FuelEngine")
})
abstract class Engine {
    public int engineModel;
    public int cc;
}

class ElectricEngine extends Engine {
    public int maxEnergy;

    public ElectricEngine() {
    }
}

class FuelEngine extends Engine {
    public int fuel;

    public FuelEngine() {
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package xml;

import org.opensaml.xml.parse.BasicParserPool;
import org.opensaml.xml.parse.ParserPool;
import org.springframework.context.annotation.Bean;

public class SAMLIgnoreComments {
  @Bean
  ParserPool parserPool() {
    BasicParserPool pool = new BasicParserPool();
    // ruleid: java_xml_rule-SAMLIgnoreComments
    pool.setIgnoreComments(false);
    return pool;
  }

  @Bean
  void parserPool2() {
    boolean shouldIgnore = false;
    BasicParserPool pool = new BasicParserPool();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
  }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package strings;
import java.util.Formatter;
import java.util.Locale;
import java.io.IOException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;

public class FormatStringManipulation extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{

        // create a new formatter
        StringBuffer buffer = new StringBuffer();
        Formatter formatter = new Formatter(buffer, Locale.US);
        String input = request.getParameter("suffix");
        String format = "The customer: %s %s" + input;

        //test cases
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_strings_rule-FormatStringManipulation
        formatter.format(Locale.US, format, "John", "Smith"); //BAD
        //false positive test
        formatter.format("The customer: %s %s", "John", request.getParameter("testParam")); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String format2 = "The customer: %s %s" + request.getParameter("suffix");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }
}

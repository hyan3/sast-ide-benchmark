public class EhTagDatabase {

    private static final Map<String, String> NAMESPACE_TO_PREFIX = new HashMap<>();
    private static volatile EhTagDatabase instance;
    // TODO more lock for different language
    private static final Lock lock = new ReentrantLock();

    static {
        NAMESPACE_TO_PREFIX.put("artist", "a:");
        NAMESPACE_TO_PREFIX.put("cosplayer", "cos:");
        NAMESPACE_TO_PREFIX.put("character", "c:");
        NAMESPACE_TO_PREFIX.put("female", "f:");
        NAMESPACE_TO_PREFIX.put("group", "g:");
        NAMESPACE_TO_PREFIX.put("language", "l:");
        NAMESPACE_TO_PREFIX.put("male", "m:");
        NAMESPACE_TO_PREFIX.put("mixed", "x:");
        NAMESPACE_TO_PREFIX.put("other", "o:");
        NAMESPACE_TO_PREFIX.put("parody", "p:");
        NAMESPACE_TO_PREFIX.put("reclass", "r:");
    }

    private final String name;
    private final byte[] tags;

    public EhTagDatabase(String name, BufferedSource source) throws IOException {
        this.name = name;
        String[] tmp;
        StringBuilder buffer = new StringBuilder();
        source.readInt();
        for (String i : source.readUtf8().split("\n")) {
            tmp = i.split("\r", 2);
            buffer.append(tmp[0]);
            buffer.append("\r");
            try {
                buffer.append(new String(Base64.decode(tmp[1], Base64.DEFAULT), StandardCharsets.UTF_8));
            } catch (Exception e) {
                buffer.append(tmp[1]);
            }
            buffer.append("\n");
        }
        byte[] b = buffer.toString().getBytes(StandardCharsets.UTF_8);
        int totalBytes = b.length;
        tags = new byte[totalBytes];
        System.arraycopy(b, 0, tags, 0, totalBytes);
    }

    @Nullable
    public static EhTagDatabase getInstance(Context context) {
        if (isPossible(context)) {
            return instance;
        } else {
            instance = null;
            return null;
        }
    }

    @Nullable
    public static String namespaceToPrefix(String namespace) {
        return NAMESPACE_TO_PREFIX.get(namespace);
    }

    private static String[] getMetadata(Context context) {
        String[] metadata = context.getResources().getStringArray(R.array.tag_translation_metadata);
        if (metadata.length == 4) {
            return metadata;
        } else {
            return null;
        }
    }

    public static boolean isPossible(Context context) {
        return getMetadata(context) != null;
    }

    @Nullable
    private static byte[] getFileContent(File file, int length) {
        try (BufferedSource source = Okio.buffer(Okio.source(file))) {
            byte[] content = new byte[length];
            source.readFully(content);
            return content;
        } catch (IOException e) {
            return null;
        }
    }

    @Nullable
    private static byte[] getFileSha1(File file) {
        try (InputStream is = new FileInputStream(file)) {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            int n;
            byte[] buffer = new byte[4 * 1024];
            while ((n = is.read(buffer)) != -1) {
                digest.update(buffer, 0, n);
            }
            return digest.digest();
        } catch (IOException | NoSuchAlgorithmException e) {
            return null;
        }
    }

    private static boolean equals(byte[] b1, byte[] b2) {
        if (b1 == null && b2 == null) {
            return true;
        }
        if (b1 == null || b2 == null) {
            return false;
        }

        if (b1.length != b2.length) {
            return false;
        }

        for (int i = 0; i < b1.length; i++) {
            if (b1[i] != b2[i]) {
                return false;
            }
        }

        return true;
    }

    private static boolean checkData(File sha1File, File dataFile) {
        byte[] s1 = getFileContent(sha1File, 20);
        if (s1 == null) {
            return false;
        }

        byte[] s2 = getFileSha1(dataFile);
        if (s2 == null) {
            return false;
        }

        return equals(s1, s2);
    }

    private static boolean save(OkHttpClient client, String url, File file) {
        Request request = new Request.Builder().url(url).build();
        Call call = client.newCall(request);
        try (Response response = call.execute()) {
            if (!response.isSuccessful()) {
                return false;
            }
            ResponseBody body = response.body();
            if (body == null) {
                return false;
            }

            try (InputStream is = body.byteStream(); OutputStream os = new FileOutputStream(file)) {
                IOUtils.copy(is, os);
            }

            return true;
        } catch (Throwable t) {
            t.printStackTrace();
            ExceptionUtils.throwIfFatal(t);
            return false;
        }
    }

    public static void update(Context context) {
        String[] urls = getMetadata(context);
        if (urls == null || urls.length != 4) {
            // Clear tags if it's not possible
            instance = null;
            return;
        }

        String sha1Name = urls[0];
        String sha1Url = urls[1];
        String dataName = urls[2];
        String dataUrl = urls[3];

        // Clear tags if name if different
        EhTagDatabase tmp = instance;
        if (tmp != null && !tmp.name.equals(dataName)) {
            instance = null;
        }

        IoThreadPoolExecutor.getInstance().execute(() -> {
            if (!lock.tryLock()) {
                return;
            }

            try {
                File dir = AppConfig.getFilesDir("tag-translations");
                if (dir == null) {
                    return;
                }

                // Check current sha1 and current data
                File sha1File = new File(dir, sha1Name);
                File dataFile = new File(dir, dataName);
                if (!checkData(sha1File, dataFile)) {
                    FileUtils.delete(sha1File);
                    FileUtils.delete(dataFile);
                }

                // Read current EhTagDatabase
                if (instance == null && dataFile.exists()) {
                    try (BufferedSource source = Okio.buffer(Okio.source(dataFile))) {
                        instance = new EhTagDatabase(dataName, source);
                    } catch (IOException e) {
                        FileUtils.delete(sha1File);
                        FileUtils.delete(dataFile);
                    }
                }

                OkHttpClient client = EhApplication.getOkHttpClient(context);

                // Save new sha1
                File tempSha1File = new File(dir, sha1Name + ".tmp");
                if (!save(client, sha1Url, tempSha1File)) {
                    FileUtils.delete(tempSha1File);
                    return;
                }

                // Check new sha1 and current data
                if (checkData(tempSha1File, dataFile)) {
                    // The data is the same
                    FileUtils.delete(tempSha1File);
                    return;
                }

                // Save new data
                File tempDataFile = new File(dir, dataName + ".tmp");
                if (!save(client, dataUrl, tempDataFile)) {
                    FileUtils.delete(tempDataFile);
                    return;
                }

                // Check new sha1 and new data
                if (!checkData(tempSha1File, tempDataFile)) {
                    FileUtils.delete(tempSha1File);
                    FileUtils.delete(tempDataFile);
                    return;
                }

                // Replace current sha1 and current data with new sha1 and new data
                FileUtils.delete(sha1File);
                FileUtils.delete(dataFile);
                tempSha1File.renameTo(sha1File);
                tempDataFile.renameTo(dataFile);

                // Read new EhTagDatabase
                try (BufferedSource source = Okio.buffer(Okio.source(dataFile))) {
                    instance = new EhTagDatabase(dataName, source);
                } catch (IOException e) {
                    // Ignore
                }
            } finally {
                lock.unlock();
            }
        });
    }

    public String getTranslation(String tag) {
        return search(tags, tag.getBytes(StandardCharsets.UTF_8));
    }

    @Nullable
    private String search(byte[] tags, byte[] tag) {
        int low = 0;
        int high = tags.length;
        while (low < high) {
            int start = (low + high) / 2;
            // Look for the starting '\n'
            while (start > -1 && tags[start] != '\n') {
                start--;
            }
            start++;

            // Look for the middle '\r'.
            int middle = 1;
            while (tags[start + middle] != '\r') {
                middle++;
            }

            // Look for the ending '\n'
            int end = middle + 1;
            while (tags[start + end] != '\n') {
                end++;
            }

            int compare;
            int tagIndex = 0;
            int curIndex = start;

            for (; ; ) {
                int tagByte = tag[tagIndex] & 0xff;
                int curByte = tags[curIndex] & 0xff;
                compare = tagByte - curByte;
                if (compare != 0) {
                    break;
                }

                tagIndex++;
                curIndex++;
                if (tagIndex == tag.length && curIndex == start + middle) {
                    break;
                }
                if (tagIndex == tag.length) {
                    compare = -1;
                    break;
                }
                if (curIndex == start + middle) {
                    compare = 1;
                    break;
                }
            }

            if (compare < 0) {
                high = start - 1;
            } else if (compare > 0) {
                low = start + end + 1;
            } else {
                return new String(tags, start + middle + 1, end - middle - 1, StandardCharsets.UTF_8);
            }
        }
        return null;
    }

    public ArrayList<Pair<String, String>> suggest(String keyword) {
        return searchTag(tags, keyword);
    }

    private ArrayList<Pair<String, String>> searchTag(byte[] tags, String keyword) {
        ArrayList<Pair<String, String>> searchHints = new ArrayList<>();
        int begin = 0;
        while (begin < tags.length - 1) {
            int start = begin;
            // Look for the starting '\n'
            while (tags[start] != '\n') {
                start++;
            }

            // Look for the middle '\r'.
            int middle = 1;
            while (tags[start + middle] != '\r') {
                middle++;
            }

            // Look for the ending '\n'
            int end = middle + 1;
            while (tags[start + end] != '\n') {
                end++;
            }

            begin = start + end;

            byte[] hintBytes = new byte[end - middle - 1];
            System.arraycopy(tags, start + middle + 1, hintBytes, 0, end - middle - 1);
            String hint = new String(hintBytes, StandardCharsets.UTF_8);
            byte[] tagBytes = new byte[middle];
            System.arraycopy(tags, start + 1, tagBytes, 0, middle);
            String tag = new String(tagBytes, StandardCharsets.UTF_8);
            int index = tag.indexOf(':');
            boolean keywordMatches;
            if (index == -1 || index >= tag.length() - 1 || keyword.length() > 2) {
                keywordMatches = containsIgnoreSpace(tag, keyword);
            } else {
                keywordMatches = containsIgnoreSpace(tag.substring(index + 1), keyword);
            }

            if (keywordMatches || containsIgnoreSpace(hint, keyword)) {
                Pair<String, String> pair = new Pair<>(hint, tag);
                if (!searchHints.contains(pair)) {
                    searchHints.add(pair);
                }
            }
            if (searchHints.size() > 20) {
                break;
            }

        }
        return searchHints;
    }

    private boolean containsIgnoreSpace(String text, String key) {
        return text.replace(" ", "").contains(key.replace(" ", ""));
    }
}

public class FDroidApp extends Application implements androidx.work.Configuration.Provider {

    private static final String TAG = "FDroidApp";
    private static final String ACRA_ID = BuildConfig.APPLICATION_ID + ":acra";

    public static final String SYSTEM_DIR_NAME = Environment.getRootDirectory().getAbsolutePath();

    private static FDroidApp instance;
    @Nullable
    private static RepoManager repoManager;
    @Nullable
    private static RepoUpdateManager repoUpdateManager;

    // for the local repo on this device, all static since there is only one
    public static volatile int port;
    public static volatile boolean generateNewPort;
    public static volatile String ipAddressString;
    public static volatile SubnetUtils.SubnetInfo subnetInfo;
    public static volatile String ssid;
    public static volatile String bssid;
    public static volatile Repository repo;

    public static volatile SessionInstallManager sessionInstallManager;

    public static volatile int networkState = ConnectivityMonitorService.FLAG_NET_UNAVAILABLE;

    public static final SubnetUtils.SubnetInfo UNSET_SUBNET_INFO = new SubnetUtils("0.0.0.0/32").getInfo();

    @Nullable
    public static volatile String queryString;

    private static final org.bouncycastle.jce.provider.BouncyCastleProvider BOUNCYCASTLE_PROVIDER;

    /**
     * The construction of this notification helper has side effects including listening and
     * responding to local broadcasts. It is kept as a reference on the app object here so that
     * it doesn't get GC'ed.
     */
    @SuppressWarnings({"FieldCanBeLocal", "unused", "PMD.SingularField"})
    private NotificationHelper notificationHelper;

    static {
        BOUNCYCASTLE_PROVIDER = new org.bouncycastle.jce.provider.BouncyCastleProvider();
        enableBouncyCastle();

        RxJavaPlugins.setErrorHandler(e -> {
            if (e instanceof UndeliverableException) {
                e = e.getCause();
            }
            if (e instanceof InterruptedException) {
                // fine, some blocking code was interrupted by a dispose call
                return;
            }
            // let app crash and ACRA tell us about it
            Thread.UncaughtExceptionHandler uncaughtExceptionHandler =
                    Thread.currentThread().getUncaughtExceptionHandler();
            if (uncaughtExceptionHandler != null && e != null) {
                uncaughtExceptionHandler.uncaughtException(Thread.currentThread(), e);
            }
        });
    }

    /**
     * Apply pure black background in dark theme setting. Must be called in every activity's
     * {@link AppCompatActivity#onCreate()}, before super.onCreate().
     *
     * @param activity The activity to apply the setting.
     */
    public void applyPureBlackBackgroundInDarkTheme(AppCompatActivity activity) {
        final boolean isPureBlack = Preferences.get().isPureBlack();
        if (isPureBlack) {
            activity.setTheme(R.style.Theme_App_Black);
        }
    }

    public static void applyTheme() {
        Preferences.Theme curTheme = Preferences.get().getTheme();
        switch (curTheme) {
            case dark:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                break;
            case light:
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                break;
            default:
                // `Set by Battery Saver` for Q above (inclusive), `Use system default` for Q below
                // https://medium.com/androiddevelopers/appcompat-v23-2-daynight-d10f90c83e94
                if (Build.VERSION.SDK_INT <= 28) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_AUTO_BATTERY);
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                }
                break;
        }
    }

    public void setSecureWindow(AppCompatActivity activity) {
        if (Preferences.get().preventScreenshots()) {
            activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_SECURE);
        }
    }

    /**
     * The built-in BouncyCastle was stripped down in {@link Build.VERSION_CODES#S}
     * so that {@code SHA1withRSA} and {@code SHA256withRSA} are no longer included.
     *
     * @see
     * <a href="https://gitlab.com/fdroid/fdroidclient/-/issues/2338">Nearby Swap Crash on Android 12: no such algorithm: SHA1WITHRSA for provider BC</a>
     */
    private static void enableBouncyCastle() {
        if (Build.VERSION.SDK_INT >= 31) {
            Security.removeProvider("BC");
        }
        Security.addProvider(BOUNCYCASTLE_PROVIDER);
    }

    /**
     * Initialize the settings needed to run a local swap repo. This should
     * only ever be called in {@link WifiStateChangeService.WifiInfoThread},
     * after the single init call in {@link FDroidApp#onCreate()}.  If there is
     * a port conflict on binding then {@code generateNewPort} will be set and
     * the whole discovery process will be restarted in {@link WifiStateChangeService}
     */
    public static void initWifiSettings() {
        if (generateNewPort) {
            port = new Random().nextInt(8888) + 1024;
            generateNewPort = false;
        } else {
            port = 8888;
        }
        ipAddressString = null;
        subnetInfo = UNSET_SUBNET_INFO;
        ssid = "";
        bssid = "";
        repo = null;
    }

    @Override
    public void onConfigurationChanged(@NonNull Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        Languages.setLanguage(this);
        App.systemLocaleList = null;
        updateLanguagesIfNecessary();
    }

    private void updateLanguagesIfNecessary() {
        // update the descriptions based on the new language preferences
        SharedPreferences atStartTime = getAtStartTimeSharedPreferences();
        final String lastLocaleKey = "lastLocale";
        String lastLocales = atStartTime.getString(lastLocaleKey, null);
        String currentLocales = App.getLocales().toString();
        if (!TextUtils.equals(lastLocales, currentLocales)) {
            Log.i(TAG, "Locales changed. Old: " + lastLocales + " New: " + currentLocales);
            onLanguageChanged(getApplicationContext());
        }
        atStartTime.edit().putString(lastLocaleKey, currentLocales).apply();
    }

    public static void onLanguageChanged(Context context) {
        FDroidDatabase db = DBHelper.getDb(context);
        Single.fromCallable(() -> {
            long now = System.currentTimeMillis();
            LocaleListCompat locales = App.getLocales();
            db.afterLocalesChanged(locales);
            Log.d(TAG, "Updating DB locales took: " + (System.currentTimeMillis() - now) + "ms");
            return true;
        }).subscribeOn(Schedulers.io()).subscribe();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
        if (level >= TRIM_MEMORY_BACKGROUND) {
            clearImageLoaderMemoryCache();
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        clearImageLoaderMemoryCache();
    }

    private void clearImageLoaderMemoryCache() {
        Glide.get(getApplicationContext()).clearMemory();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        if (BuildConfig.DEBUG) {
            StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                    .detectAll()
                    .penaltyLog()
                    .build());
        }
        Preferences.setup(this);
        Languages.setLanguage(this);
        Preferences preferences = Preferences.get();

        if (preferences.promptToSendCrashReports()) {
            final String subject = String.format(Locale.ENGLISH,
                                                 "%s %s: Crash Report",
                                                 BuildConfig.APPLICATION_ID,
                                                 BuildConfig.VERSION_NAME);
            ACRA.init(this, new CoreConfigurationBuilder()
                    .withReportContent(
                            ReportField.USER_COMMENT,
                            ReportField.PACKAGE_NAME,
                            ReportField.APP_VERSION_NAME,
                            ReportField.ANDROID_VERSION,
                            ReportField.PRODUCT,
                            ReportField.BRAND,
                            ReportField.PHONE_MODEL,
                            ReportField.DISPLAY,
                            ReportField.TOTAL_MEM_SIZE,
                            ReportField.AVAILABLE_MEM_SIZE,
                            ReportField.CUSTOM_DATA,
                            ReportField.STACK_TRACE_HASH,
                            ReportField.STACK_TRACE
                    )
                    .withPluginConfigurations(
                            new MailSenderConfigurationBuilder()
                                    .withMailTo(BuildConfig.ACRA_REPORT_EMAIL)
                                    .withReportFileName(BuildConfig.ACRA_REPORT_FILE_NAME)
                                    .withSubject(subject)
                                    .build(),
                            new DialogConfigurationBuilder()
                                    .withResTheme(R.style.Theme_App)
                                    .withTitle(getString(R.string.crash_dialog_title))
                                    .withText(getString(R.string.crash_dialog_text))
                                    .withCommentPrompt(getString(R.string.crash_dialog_comment_prompt))
                                    .build()
                    )
            );
            if (isAcraProcess() || HidingManager.isHidden(this)) {
                return;
            }
        }

        // register broadcast receivers
        registerReceiver(new DeviceStorageReceiver(), new IntentFilter(Intent.ACTION_DEVICE_STORAGE_LOW));
        WifiStateChangeService.registerReceiver(this);

        applyTheme();

        configureProxy(preferences);

        preferences.registerUnstableUpdatesChangeListener(() ->
                AppUpdateStatusManager.getInstance(FDroidApp.this).checkForUpdates());

        CleanCacheWorker.schedule(this);

        sessionInstallManager = new SessionInstallManager(getApplicationContext());
        notificationHelper = new NotificationHelper(getApplicationContext());

        if (preferences.isIndexNeverUpdated()) {
            preferences.setDefaultForDataOnlyConnection(this);
        }
        // force setting network state to ensure it is set before UpdateService checks it
        networkState = ConnectivityMonitorService.getNetworkState(this);
        ConnectivityMonitorService.registerAndStart(this);
        Utils.debugLog(TAG, "RepoUpdateWorker.scheduleOrCancel()");
        RepoUpdateWorker.scheduleOrCancel(getApplicationContext());

        FDroidApp.initWifiSettings();
        WifiStateChangeService.start(this, null);
        // if the HTTPS pref changes, then update all affected things
        preferences.registerLocalRepoHttpsListeners(() -> WifiStateChangeService.start(getApplicationContext(),
                null));

        if (preferences.isKeepingInstallHistory()) {
            InstallHistoryService.register(this);
        }

        String packageName = getString(R.string.install_history_reader_packageName);
        String unset = getString(R.string.install_history_reader_packageName_UNSET);
        if (!TextUtils.equals(packageName, unset)) {
            int modeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                    | Intent.FLAG_GRANT_WRITE_URI_PERMISSION;
            modeFlags |= Intent.FLAG_GRANT_PERSISTABLE_URI_PERMISSION;
            grantUriPermission(packageName, InstallHistoryService.LOG_URI, modeFlags);
        }

        // if the underlying OS version has changed, then fully rebuild the database
        SharedPreferences atStartTime = getAtStartTimeSharedPreferences();
        if (Build.VERSION.SDK_INT != atStartTime.getInt("build-version", Build.VERSION.SDK_INT)) {
            Utils.runOffUiThread(() -> {
                DBHelper.resetTransient(getApplicationContext());
                return true;
            }, result -> RepoUpdateWorker.updateNow(getApplicationContext()));
        }
        atStartTime.edit().putInt("build-version", Build.VERSION.SDK_INT).apply();

        if (!preferences.isIndexNeverUpdated()) {
            // if system locales have changed since the app's last run, refresh cache as necessary
            updateLanguagesIfNecessary();
        }

        final String queryStringKey = "http-downloader-query-string";
        if (preferences.sendVersionAndUUIDToServers()) {
            queryString = atStartTime.getString(queryStringKey, null);
            if (queryString == null) {
                UUID uuid = UUID.randomUUID();
                ByteBuffer buffer = ByteBuffer.allocate(Long.SIZE / Byte.SIZE * 2);
                buffer.putLong(uuid.getMostSignificantBits());
                buffer.putLong(uuid.getLeastSignificantBits());
                String id = Base64.encodeToString(buffer.array(),
                        Base64.URL_SAFE | Base64.NO_WRAP | Base64.NO_PADDING);
                StringBuilder builder = new StringBuilder("id=").append(id);
                String versionName = Uri.encode(Utils.getVersionName(this));
                if (versionName != null) {
                    builder.append("&client_version=").append(versionName);
                }
                queryString = builder.toString();
                atStartTime.edit().putString(queryStringKey, queryString).apply();
            }
        } else {
            atStartTime.edit().remove(queryStringKey).apply();
        }

        if (Preferences.get().isScanRemovableStorageEnabled()) {
            SDCardScannerService.scan(this);
        }
    }

    /**
     * Asks if the current process is "org.fdroid.fdroid:acra".
     * <p>
     * This is helpful for bailing out of the {@link FDroidApp#onCreate} method early, preventing
     * problems that arise from executing the code twice. This happens due to the `android:process`
     * statement in AndroidManifest.xml causes another process to be created to run ACRA.
     * This was causing lots of things to be
     * started/run twice including {@link CleanCacheWorker} and {@link WifiStateChangeService}.
     * <p>
     * Note that it is not perfect, because some devices seem to not provide a list of running app
     * processes when asked. In such situations, F-Droid may regress to the behaviour where some
     * services may run twice and thus cause weirdness or slowness. However that is probably better
     * for end users than experiencing a deterministic crash every time F-Droid is started.
     */
    private boolean isAcraProcess() {
        ActivityManager manager = ContextCompat.getSystemService(this, ActivityManager.class);
        List<RunningAppProcessInfo> processes = manager.getRunningAppProcesses();
        if (processes == null) {
            return false;
        }

        int pid = android.os.Process.myPid();
        for (RunningAppProcessInfo processInfo : processes) {
            if (processInfo.pid == pid && ACRA_ID.equals(processInfo.processName)) {
                return true;
            }
        }

        return false;
    }

    private SharedPreferences getAtStartTimeSharedPreferences() {
        return getSharedPreferences("at-start-time", Context.MODE_PRIVATE);
    }

    public void sendViaBluetooth(AppCompatActivity activity, int resultCode, String packageName) {
        if (resultCode == AppCompatActivity.RESULT_CANCELED) {
            return;
        }

        String bluetoothPackageName = null;
        String className = null;
        Intent sendBt = null;

        try {
            PackageManager pm = getPackageManager();
            PackageInfo packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_META_DATA);
            sendBt = new Intent(Intent.ACTION_SEND);

            // The APK type ("application/vnd.android.package-archive") is blocked by stock Android, so use zip
            sendBt.setType(PublicSourceDirProvider.SHARE_APK_MIME_TYPE);
            sendBt.putExtra(Intent.EXTRA_STREAM, ApkFileProvider.getSafeUri(this, packageInfo));

            // not all devices have the same Bluetooth Activities, so
            // let's find it
            for (ResolveInfo info : pm.queryIntentActivities(sendBt, 0)) {
                bluetoothPackageName = info.activityInfo.packageName;
                if ("com.android.bluetooth".equals(bluetoothPackageName)
                        || "com.mediatek.bluetooth".equals(bluetoothPackageName)) {
                    className = info.activityInfo.name;
                    break;
                }
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "Could not get application info to send via bluetooth", e);
            className = null;
        } catch (IOException e) {
            Exception toLog = new RuntimeException("Error preparing file to send via Bluetooth", e);
            ACRA.getErrorReporter().handleException(toLog, false);
        }

        if (sendBt != null) {
            if (className != null) {
                sendBt.setClassName(bluetoothPackageName, className);
                activity.startActivity(sendBt);
            } else {
                Toast.makeText(this, R.string.bluetooth_activity_not_found,
                        Toast.LENGTH_SHORT).show();
                activity.startActivity(Intent.createChooser(sendBt, getString(R.string.choose_bt_send)));
            }
        }
    }

    /**
     * Put proxy settings (or Tor settings) globally into effect based on what's configured in Preferences.
     * <p>
     * Must be called on App startup and after every proxy configuration change.
     */
    public static void configureProxy(Preferences preferences) {
        if (preferences.isTorEnabled()) {
            NetCipher.useTor();
        } else if (preferences.isProxyEnabled()) {
            // TODO move createUnresolved to NetCipher itself once its proven
            InetSocketAddress isa = InetSocketAddress.createUnresolved(
                    preferences.getProxyHost(), preferences.getProxyPort());
            NetCipher.setProxy(new Proxy(Proxy.Type.HTTP, isa));
        } else {
            NetCipher.clearProxy();
        }
    }

    public static void checkStartTor(Context context, Preferences preferences) {
        if (preferences.isTorEnabled()) {
            OrbotHelper.requestStartTor(context);
        }
    }

    public static Repository createSwapRepo(String address, String certificate) {
        long now = System.currentTimeMillis();
        if (certificate == null) certificate = "d0ef";
        return new Repository(42L, address, now, IndexFormatVersion.ONE, certificate, 20001L, 42,
                now);
    }

    public static Context getInstance() {
        return instance;
    }

    public static RepoManager getRepoManager(Context c) {
        if (repoManager == null) {
            Context context = c.getApplicationContext();
            final RepoUriBuilder repoUriBuilder = (repository, pathElements) -> {
                String address1 = Utils.getRepoAddress(repository);
                return Utils.getUri(address1, pathElements);
            };
            repoManager = new RepoManager(context, DBHelper.getDb(context), DownloaderFactory.INSTANCE,
                    DownloaderFactory.HTTP_MANAGER, repoUriBuilder);
        }
        return repoManager;
    }

    public static RepoUpdateManager getRepoUpdateManager(Context c) {
        if (repoUpdateManager == null) {
            Context context = c.getApplicationContext();
            repoUpdateManager = new RepoUpdateManager(context, DBHelper.getDb(context), getRepoManager(context));
        }
        return repoUpdateManager;
    }

    /**
     * Set up WorkManager on demand to avoid slowing down starts.
     *
     * @see CleanCacheWorker
     * @see org.fdroid.fdroid.work.FDroidMetricsWorker
     * @see org.fdroid.fdroid.work.UpdateWorker
     * @see <a href="https://developer.android.com/codelabs/android-adv-workmanager#3">example</a>
     */
    @NonNull
    @Override
    public androidx.work.Configuration getWorkManagerConfiguration() {
        if (BuildConfig.DEBUG) {
            return new androidx.work.Configuration.Builder()
                    .setJobSchedulerJobIdRange(0, 4096)
                    .setMinimumLoggingLevel(Log.DEBUG)
                    .build();
        } else {
            return new androidx.work.Configuration.Builder()
                    .setJobSchedulerJobIdRange(0, 4096)
                    .setMinimumLoggingLevel(Log.ERROR)
                    .build();
        }
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
// source (original): https://github.com/semgrep/semgrep-rules/blob/release/java/spring/security/injection/tainted-file-path.java
package traversal;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.commons.io.IOUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import java.nio.file.*;
import javax.xml.transform.*;

@RestController
public class PreflightController {
    static final String CONTENT_DISPOSITION_STATIC_FILE_LOCATION = "contentDispositionUpload";
    private UnrestrictedFileUpload unrestrictedFileUpload;

    public PreflightController(UnrestrictedFileUpload unrestrictedFileUpload) {
        this.unrestrictedFileUpload = unrestrictedFileUpload;
    }

    @RequestMapping(
            CONTENT_DISPOSITION_STATIC_FILE_LOCATION + "/" + "{fileName}")
    public ResponseEntity<byte[]> fetchFile(@PathVariable("fileName") String fileName)
            throws IOException {
        InputStream inputStream =
                // ruleid: java_traversal_rule-RelativePathTraversal
                new FileInputStream(
                        unrestrictedFileUpload.getContentDispositionRoot().toFile()
                                + "/"
                                + fileName);
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(HttpHeaders.CONTENT_DISPOSITION, "attachment");
        return new ResponseEntity<byte[]>(
                IOUtils.toByteArray(inputStream), httpHeaders, HttpStatus.OK);
    }

    public static void bad(@RequestParam String user)
    {
        Socket sock;
        BufferedReader filenameReader = new BufferedReader(
                new InputStreamReader(sock.getInputStream(), "UTF-8"));
        String filename = filenameReader.readLine();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String fileLine = fileReader.readLine();
        while(fileLine != null) {
            sock.getOutputStream().write(fileLine.getBytes());
            fileLine = fileReader.readLine();
        }
    }

    public static void bad2(@RequestParam String filename)
    {
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext(new String[] {"If-you-have-any.xml"});

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        try {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();

        } catch(IOException e){
            e.printStackTrace();
        }
    }

    public static void ok(@RequestParam String filename)
    {
        ApplicationContext appContext =
                new ClassPathXmlApplicationContext(new String[] {"If-you-have-any.xml"});

        // ok: java_traversal_rule-RelativePathTraversal
        Resource resource =
                appContext.getResource("classpath:com/" + org.apache.commons.io.FilenameUtils.getName(filename));

        try {
            InputStream is = resource.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
            br.close();

        } catch(IOException e){
            e.printStackTrace();
        }
    }

    @Test
    public void whenResourceAsFile_thenReadSuccessful(@RequestParam String filename) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String employees = new String(Files.readAllBytes(resource.toPath()));
        assertEquals("Joe Employee,Jan Employee,James T. Employee", employees);
    }

    public String bad3(@RequestParam String filename) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        try {
            BufferedReader reader = new BufferedReader(new FileReader(file));
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad4(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            BufferedReader reader = new BufferedReader(fileReader);
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad5(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            BufferedReader reader = Files.newBufferedReader(path);
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad6(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            String line = reader.readLine();
            reader.close();
            return "First line of the file: " + line;
        } catch (IOException e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad7(@RequestParam String fileName, @RequestParam String content) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            fos.write(content.getBytes());
            fos.close();
            return "File written successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error writing to the file";
        }
    }

    public String bad8(@RequestParam String fileName) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer();
            StringWriter writer = new StringWriter();
            transformer.transform(source, new StreamResult(writer));
            String result = writer.toString();
            return "File content: " + result;
        } catch (Exception e) {
            e.printStackTrace();
            return "Error reading the file";
        }
    }

    public String bad9(@RequestParam String fileName, @RequestParam String content) {
        try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
            os.write(content.getBytes());
            os.close();
            return "File written successfully";
        } catch (IOException e) {
            e.printStackTrace();
            return "Error writing to the file";
        }
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/audit/crypto/use-of-rc2.java
import javax.crypto.Cipher;

class RC2 {

    String algo2 = "RC2";

    public void bad1() throws Exception {
        // ruleid: java_crypto_rule-UseOfRC2
        Cipher.getInstance("RC2");
    }

    public void bad2() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad3() throws Exception {
        String algo = "RC2";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad4() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
    public void bad5() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
    public void ok() throws Exception {
        // ok: java_crypto_rule-UseOfRC2
        Cipher.getInstance("AES/CBC/PKCS7PADDING");
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/script/SpelView.java
// hash: a7694d0
package script;

import org.springframework.context.expression.MapAccessor;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.util.PropertyPlaceholderHelper;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class SpringSpelExpressionParser implements View {
    private String template;

    private final SpelExpressionParser parser = new SpelExpressionParser();
    private final ExpressionParser parserVariant = new SpelExpressionParser();

    private final StandardEvaluationContext context = new StandardEvaluationContext();

    private PropertyPlaceholderHelper.PlaceholderResolver resolver;

    public void SpelView(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                // ruleid: java_script_rule-SpringSpelExpressionParser
                Expression expression2 = this.parserVariant.parseExpression(name);

                String tainted = name + "blah";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public void SpelViewParseRaw(String template) {
        this.template = template;
        this.context.addPropertyAccessor(new MapAccessor());
        this.resolver = name -> {
            try {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                String tainted = name + "blah";
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

                Object value = expression.getValue(context);
                return value == null ? null : value.toString();
            }
            catch (Exception e) {
                return null;
            }
        };
    }

    public String getContentType() {
        return "text/html";
    }

    public void render(Map<String, ?> model, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        Map<String, Object> map = new HashMap<>(model);
        String path = ServletUriComponentsBuilder.fromContextPath(request).build()
                .getPath();
        map.put("path", path==null ? "" : path);
        context.setRootObject(map);
        PropertyPlaceholderHelper helper = new PropertyPlaceholderHelper("${", "}");
        String result = helper.replacePlaceholders(template, resolver);
        response.setContentType(getContentType());
        response.getWriter().append(result);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        Files.createTempFile("", input);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: LGPL-3.0 License (c) find-sec-bugs
// source: https://github.com/find-sec-bugs/find-sec-bugs/blob/master/findsecbugs-samples-java/src/test/java/testcode/file/FileDisclosure.java
// hash: a7694d0

package injection;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;

// java_inject_rule-FileDisclosureRequestDispatcher
public class FileDisclosureRequestDispatcher extends HttpServlet{

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException{
        try{
            String jspFile = request.getParameter("jspFile");

            RequestDispatcher requestDispatcher = request.getRequestDispatcher(jspFile);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

            requestDispatcher = request.getSession().getServletContext().getRequestDispatcher(jspFile);

            // ruleid: java_inject_rule-FileDisclosureRequestDispatcher
            requestDispatcher.forward(request, response);

            // Create a look up table or pull from a data source
            HashMap<String, String> lookupTable = new HashMap<>();
            lookupTable.put("key1", "/ServletSample/simpleForm.jsp");
            // Get user input
            String userInput = request.getParameter("key");
            // Look up resource to redirect to from the user input
            String redirectValue = lookupTable.getOrDefault(userInput, "/ServletSample/index.html");
            // Redirect the user
            // ok: java_inject_rule-FileDisclosureRequestDispatcher
            response.sendRedirect(redirectValue);

            RequestDispatcher requestDispatcherTwo = request.getSession().getServletContext().getRequestDispatcher(redirectValue);
            // ok: java_inject_rule-FileDisclosureRequestDispatcher
            requestDispatcherTwo.forward(request, response);
        }catch(Exception e){
            System.out.println(e);
        }
    }
}

class Admin::LabelsController < Admin::ApplicationController
  before_action :set_label, only: [:show, :edit, :update, :destroy]

  feature_category :team_planning
  urgency :low

  def index
    @labels = Label.templates.page(pagination_params[:page])
  end

  def show; end

  def new
    @label = Label.new
  end

  def edit; end

  def create
    @label = Labels::CreateService.new(label_params).execute(template: true)

    if @label.persisted?
      redirect_to admin_labels_url, notice: _("Label was created")
    else
      render :new
    end
  end

  def update
    @label = Labels::UpdateService.new(label_params).execute(@label)

    if @label.valid?
      redirect_to admin_labels_path, notice: _('Label was successfully updated.')
    else
      render :edit
    end
  end

  def destroy
    respond_to do |format|
      if @label.destroy
        format.html do
          redirect_to admin_labels_path, status: :found,
            notice: format(_('%{label_name} was removed'), label_name: @label.name)
        end
        format.js { head :ok }
      else
        format.html do
          redirect_to admin_labels_path, status: :found,
            alert: @label.errors.full_messages.to_sentence
        end
        format.js { head :unprocessable_entity }
      end
    end
  end

  private

  def set_label
    @label = Label.find(params.permit(:id)[:id])
  end

  def label_params
    params[:label].permit(:title, :description, :color) # rubocop:disable Rails/StrongParams -- hash access is safely followed by permit
  end
end

class Admin::ApplicationSettingsController < Admin::ApplicationController
  include InternalRedirect
  include IntegrationsHelper
  include DefaultBranchProtection

  # NOTE: Use @application_setting in this controller when you need to access
  # application_settings after it has been modified. This is because the
  # ApplicationSetting model uses Gitlab::ProcessMemoryCache for caching and the
  # cache might be stale immediately after an update.
  # https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/30233
  before_action :set_application_setting, except: :integrations

  before_action :disable_query_limiting, only: [:usage_data]
  before_action :prerecorded_service_ping_data, only: [:metrics_and_profiling] # rubocop:disable Rails/LexicallyScopedActionFilter

  before_action do
    push_frontend_feature_flag(:ci_variables_pages, current_user)
  end

  feature_category :not_owned, [ # rubocop:todo Gitlab/AvoidFeatureCategoryNotOwned
    :general, :reporting, :metrics_and_profiling, :network,
    :preferences, :update, :reset_health_check_token
  ]

  urgency :low, [
    :reset_error_tracking_access_token
  ]

  feature_category :source_code_management, [:repository, :clear_repository_check_states]
  feature_category :continuous_integration, [:ci_cd, :reset_registration_token]
  urgency :low, [:ci_cd, :reset_registration_token]
  feature_category :service_ping, [:usage_data]
  feature_category :integrations, [:integrations, :slack_app_manifest_share, :slack_app_manifest_download]
  feature_category :pages, [:lets_encrypt_terms_of_service]
  feature_category :observability, [:reset_error_tracking_access_token]

  VALID_SETTING_PANELS = %w[general repository
    ci_cd reporting metrics_and_profiling
    network preferences].freeze

  # The current size of a sidekiq job's jid is 24 characters. The size of the
  # jid is an internal detail of Sidekiq, and they do not guarantee that it'll
  # stay the same. We chose 50 to give us room in case the size of the jid
  # increases. The jid is alphanumeric, so 50 is very generous. There is a spec
  # that ensures that the constant value is more than the size of an actual jid.
  PARAM_JOB_ID_MAX_SIZE = 50

  VALID_SETTING_PANELS.each do |action|
    define_method(action) { perform_update if submitted? }
  end

  def integrations
    return not_found unless instance_level_integrations?

    @integrations = Integration.find_or_initialize_all_non_project_specific(
      Integration.for_instance, include_instance_specific: true
    ).sort_by(&:title)
  end

  def update
    perform_update
  end

  def usage_data
    return not_found unless prerecorded_service_ping_data.present?

    respond_to do |format|
      format.html do
        usage_data_json = Gitlab::Json.pretty_generate(prerecorded_service_ping_data)

        render html: Gitlab::Highlight.highlight('payload.json', usage_data_json, language: 'json')
      end

      format.json do
        Gitlab::InternalEvents.track_event('usage_data_download_payload_clicked', user: current_user)

        render json: Gitlab::Json.dump(prerecorded_service_ping_data)
      end
    end
  end

  def reset_registration_token
    ::Ci::Runners::ResetRegistrationTokenService.new(@application_setting, current_user).execute

    flash[:notice] = _('New runners registration token has been generated!')
    redirect_to admin_runners_path
  end

  def reset_health_check_token
    @application_setting.reset_health_check_access_token!
    flash[:notice] = _('New health check access token has been generated!')
    redirect_back_or_default
  end

  def reset_error_tracking_access_token
    @application_setting.reset_error_tracking_access_token!

    redirect_to general_admin_application_settings_path,
      notice: _('New error tracking access token has been generated!')
  end

  def clear_repository_check_states
    RepositoryCheck::ClearWorker.perform_async # rubocop:disable CodeReuse/Worker

    redirect_to(
      general_admin_application_settings_path,
      notice: _('Started asynchronous removal of all repository check states.')
    )
  end

  # Getting ToS url requires `directory` api call to Let's Encrypt
  # which could result in 500 error/slow rendering on settings page
  # Because of that we use separate controller action
  def lets_encrypt_terms_of_service
    redirect_to ::Gitlab::LetsEncrypt.terms_of_service_url
  end

  def slack_app_manifest_share
    redirect_to Slack::Manifest.share_url
  end

  def slack_app_manifest_download
    send_data Slack::Manifest.to_json, type: :json, disposition: 'attachment', filename: 'slack_manifest.json'
  end

  private

  def set_application_setting
    @application_setting = ApplicationSetting.current_without_cache
    @plans = Plan.all
  end

  def disable_query_limiting
    Gitlab::QueryLimiting.disable!('https://gitlab.com/gitlab-org/gitlab/-/issues/29418')
  end

  def application_setting_params # rubocop:disable Metrics/AbcSize, Metrics/PerceivedComplexity
    params[:application_setting] ||= {}

    if params[:application_setting].key?(:enabled_oauth_sign_in_sources)
      enabled_oauth_sign_in_sources = params[:application_setting].delete(:enabled_oauth_sign_in_sources)
      enabled_oauth_sign_in_sources&.delete("")

      params[:application_setting][:disabled_oauth_sign_in_sources] =
        AuthHelper.button_based_providers.map(&:to_s) -
        Array(enabled_oauth_sign_in_sources)
    end

    params[:application_setting][:import_sources]&.delete("")
    params[:application_setting][:valid_runner_registrars]&.delete("")
    params[:application_setting][:restricted_visibility_levels]&.delete("")

    params[:application_setting][:package_metadata_purl_types]&.delete("")
    params[:application_setting][:package_metadata_purl_types]&.map!(&:to_i)

    normalize_default_branch_params!(:application_setting)

    if params[:application_setting][:required_instance_ci_template].blank?
      params[:application_setting][:required_instance_ci_template] = nil
    end

    remove_blank_params_for!(:elasticsearch_aws_secret_access_key, :eks_secret_access_key)

    # TODO Remove domain_denylist_raw in APIv5 (See https://gitlab.com/gitlab-org/gitlab-foss/issues/67204)
    params.delete(:domain_denylist_raw) if params[:domain_denylist_file]
    params.delete(:domain_denylist_raw) if params[:domain_denylist]
    params.delete(:domain_allowlist_raw) if params[:domain_allowlist]

    params[:application_setting].permit(visible_application_setting_attributes)
  end

  def recheck_user_consent?
    return false unless session[:ask_for_usage_stats_consent]
    return false unless params[:application_setting]

    params[:application_setting].key?(:usage_ping_enabled) || params[:application_setting].key?(:version_check_enabled)
  end

  def visible_application_setting_attributes
    [
      *::ApplicationSettingsHelper.visible_attributes,
      *::ApplicationSettingsHelper.external_authorization_service_attributes,
      *ApplicationSetting.kroki_formats_attributes.keys.map { |key| "kroki_formats_#{key}".to_sym },
      { default_branch_protection_defaults: [
        :allow_force_push,
        :developer_can_initial_push,
        {
          allowed_to_merge: [:access_level],
          allowed_to_push: [:access_level]
        }
      ] },
      :can_create_organization,
      :lets_encrypt_notification_email,
      :lets_encrypt_terms_of_service_accepted,
      :domain_denylist_file,
      :raw_blob_request_limit,
      :issues_create_limit,
      :notes_create_limit,
      :pipeline_limit_per_project_user_sha,
      :default_branch_name,
      { disabled_oauth_sign_in_sources: [],
        import_sources: [],
        package_metadata_purl_types: [],
        restricted_visibility_levels: [],
        repository_storages_weighted: {},
        valid_runner_registrars: [] }
    ]
  end

  def submitted?
    request.patch?
  end

  def perform_update
    successful = ::ApplicationSettings::UpdateService
      .new(@application_setting, current_user, application_setting_params)
      .execute

    session[:ask_for_usage_stats_consent] = current_user.requires_usage_stats_consent? if recheck_user_consent?

    redirect_path = referer_path(request) || general_admin_application_settings_path

    respond_to do |format|
      if successful
        format.json { head :ok }
        format.html { redirect_to redirect_path, notice: _('Application settings saved successfully') }
      else
        format.json { head :bad_request }
        format.html { render_update_error }
      end
    end
  end

  def render_update_error
    action = valid_setting_panels.include?(action_name) ? action_name : :general

    flash[:alert] = _('Application settings update failed')

    render action
  end

  def remove_blank_params_for!(*keys)
    params[:application_setting].delete_if { |setting, value| setting.to_sym.in?(keys) && value.blank? }
  end

  # overridden in EE
  def valid_setting_panels
    VALID_SETTING_PANELS
  end

  def prerecorded_service_ping_data
    @service_ping_data ||= Rails.cache.fetch(Gitlab::Usage::ServicePingReport::CACHE_KEY) ||
      ::RawUsageData.for_current_reporting_cycle.first&.payload
  end
end

class Projects::LabelsController < Projects::ApplicationController
  include ToggleSubscriptionAction

  before_action :check_issuables_available!
  before_action :label, only: [:edit, :update, :destroy, :promote]
  before_action :find_labels, only: [:index, :set_priorities, :remove_priority, :toggle_subscription]
  before_action :authorize_read_label!
  before_action :authorize_admin_labels!, only: [:new, :create, :edit, :update,
    :generate, :destroy, :remove_priority,
    :set_priorities]
  before_action :authorize_admin_group_labels!, only: [:promote]

  respond_to :js, :html

  feature_category :team_planning
  urgency :low

  def index
    respond_to do |format|
      format.html do
        @prioritized_labels = @available_labels.prioritized(@project)
        @labels = @available_labels.unprioritized(@project).page(params[:page])
        # preload group, project, and subscription data
        Preloaders::LabelsPreloader.new(@prioritized_labels, current_user, @project).preload_all
        Preloaders::LabelsPreloader.new(@labels, current_user, @project).preload_all
      end
      format.json do
        render json: LabelSerializer.new.represent_appearance(@available_labels)
      end
    end
  end

  def new
    @label = @project.labels.new
  end

  def create
    @label = Labels::CreateService.new(label_params).execute(project: @project)

    if @label.valid?
      respond_to do |format|
        format.html { redirect_to project_labels_path(@project) }
        format.json { render json: @label }
      end
    else
      respond_to do |format|
        format.html { render :new }
        format.json { render json: { message: @label.errors.messages }, status: :bad_request }
      end
    end
  end

  def edit; end

  def update
    @label = Labels::UpdateService.new(label_params).execute(@label)

    if @label.valid?
      redirect_to project_labels_path(@project)
    else
      render :edit
    end
  end

  def generate
    Gitlab::IssuesLabels.generate(@project)

    case params[:redirect]
    when 'issues'
      redirect_to project_issues_path(@project)
    when 'merge_requests'
      redirect_to project_merge_requests_path(@project)
    else
      redirect_to project_labels_path(@project)
    end
  end

  def destroy
    if @label.destroy
      redirect_to project_labels_path(@project), status: :found,
        notice: format(_('%{label_name} was removed'), label_name: @label.name)
    else
      redirect_to project_labels_path(@project), status: :found,
        alert: @label.errors.full_messages.to_sentence
    end
  end

  def remove_priority
    respond_to do |format|
      label = @available_labels.find(params[:id])

      if label.unprioritize!(project)
        format.json { render json: label }
      else
        format.json { head :unprocessable_entity }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def set_priorities
    Label.transaction do
      available_labels_ids = @available_labels.where(id: params[:label_ids]).pluck(:id)
      label_ids = params[:label_ids].select { |id| available_labels_ids.include?(id.to_i) }

      label_ids.each_with_index do |label_id, index|
        label = @available_labels.find(label_id)
        label.prioritize!(project, index)
      end
    end

    respond_to do |format|
      format.json { render json: { message: 'success' } }
    end
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def promote
    promote_service = Labels::PromoteService.new(@project, @current_user)

    begin
      return render_404 unless promote_service.execute(@label)

      flash[:notice] = flash_notice_for(@label, @project.group)
      respond_to do |format|
        format.html do
          redirect_to(project_labels_path(@project), status: :see_other)
        end
        format.json do
          render json: { url: project_labels_path(@project) }
        end
      end
    rescue ActiveRecord::RecordInvalid => e
      Gitlab::AppLogger.error "Failed to promote label \"#{@label.title}\" to group label"
      Gitlab::AppLogger.error e

      respond_to do |format|
        format.html do
          redirect_to(
            project_labels_path(@project),
            notice: _('Failed to promote label due to internal error. Please contact administrators.'))
        end
        format.js
      end
    end
  end

  def flash_notice_for(label, group)
    ''.html_safe + "#{label.title} promoted to " + view_context.link_to('<u>group label</u>'.html_safe, group_labels_path(group)) + '.'
  end

  protected

  def label_params
    allowed = [:title, :description, :color]
    allowed << :lock_on_merge if @project.supports_lock_on_merge?

    params.require(:label).permit(allowed)
  end

  def label
    @label ||= @project.labels.find(params[:id])
  end

  def subscribable_resource
    @available_labels.find(params[:id])
  end

  def find_labels
    @available_labels ||= LabelsFinder.new(
      current_user,
      project_id: @project.id,
      include_ancestor_groups: true,
      search: params[:search],
      subscribed: params[:subscribed],
      sort: sort
    ).execute
  end

  def sort
    @sort ||= params[:sort] || 'name_asc'
  end

  def authorize_admin_labels!
    render_404 unless can?(current_user, :admin_label, @project)
  end

  def authorize_admin_group_labels!
    render_404 unless can?(current_user, :admin_label, @project.group)
  end
end

class Projects::VariablesController < Projects::ApplicationController
  before_action :authorize_admin_build!, except: :update
  before_action :authorize_admin_cicd_variables!, only: :update

  feature_category :secrets_management

  urgency :low, [:show, :update]

  def show
    respond_to do |format|
      format.json do
        render status: :ok, json: { variables: ::Ci::VariableSerializer.new.represent(@project.variables) }
      end
    end
  end

  def update
    update_result = Ci::ChangeVariablesService.new(
      container: @project, current_user: current_user,
      params: variables_params
    ).execute

    if update_result
      respond_to do |format|
        format.json { render_variables }
      end
    else
      respond_to do |format|
        format.json { render_error }
      end
    end
  end

  private

  def render_variables
    render status: :ok, json: { variables: ::Ci::VariableSerializer.new.represent(@project.variables) }
  end

  def render_error
    render status: :bad_request, json: @project.errors.full_messages
  end

  def variables_params
    params.permit(variables_attributes: Array(variable_params_attributes))
  end

  def variable_params_attributes
    %i[id variable_type key description secret_value protected masked hidden raw environment_scope _destroy]
  end
end

class Projects::TemplatesController < Projects::ApplicationController
  before_action :authenticate_user!
  before_action :authorize_can_read_issuable!
  before_action :get_template_class

  feature_category :source_code_management
  urgency :low, [:names]

  def index
    templates = @template_type.template_subsets(project)

    respond_to do |format|
      format.json { render json: Gitlab::Json.dump(templates) }
    end
  end

  def show
    template = @template_type.find(params[:key], project)

    respond_to do |format|
      format.json { render json: Gitlab::Json.dump(template) }
    end
  end

  def names
    respond_to do |format|
      format.json { render json: TemplateFinder.all_template_names(project, params[:template_type].to_s.pluralize) }
    end
  end

  private

  # User must have:
  # - `read_merge_request` to see merge request templates, or
  # - `read_issue` to see issue templates
  #
  # Note params[:template_type] has a route constraint to limit it to
  # `merge_request` or `issue`
  def authorize_can_read_issuable!
    action = [:read_, params[:template_type]].join

    authorize_action!(action)
  end

  def get_template_class
    template_types = { issue: Gitlab::Template::IssueTemplate, merge_request: Gitlab::Template::MergeRequestTemplate }.with_indifferent_access
    @template_type = template_types[params[:template_type]]
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'net/ftp'

def foo

  host = params[:host]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  ftp = Net::FTP.new()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ruleid: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.putbinaryfile("/tmp/#{params[:file]}")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.new("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp = Net::FTP.open("example.com")

  ftp = Net::FTP.new()
  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.connect("example.com")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.get("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.getbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.gettextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.put("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.putbinaryfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.puttextfile("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.delete("/tmp/file")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storlines("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.storbinary("ls -al", "/tmp/log")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.sendcmd("ls -al")

  # ok: ruby_ftp_rule-AvoidTaintedFTPCall
  ftp.retrlines("ls -al")

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def zen
  41
end

# ok:ruby_eval_rule-NoEval
eval("def zen; 42; end")

puts zen

class Thing
end
a = %q{def hello() "Hello there!" end}
# not user-controllable, this is ok
# ok:ruby_eval_rule-NoEval
Thing.module_eval(a)
puts Thing.new.hello()
b = params['something']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def get_binding(param)
  binding
end
b = get_binding("hello")
# ok:ruby_eval_rule-NoEval
b.eval("some_func")

# ok:ruby_eval_rule-NoEval
eval("some_func",b)

# ruleid:ruby_eval_rule-NoEval
eval(params['cmd'],b)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok:ruby_eval_rule-NoEval
RubyVM::InstructionSequence.compile("1 + 2").eval

iseq = RubyVM::InstructionSequence.compile(foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


iseq = RubyVM::InstructionSequence.compile('num = 1 + 2')
# ok:ruby_eval_rule-NoEval
iseq.eval

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def zen
  41
end

# ok:ruby_eval_rule-NoEval
eval("def zen; 42; end")

puts zen

class Thing
end
a = %q{def hello() "Hello there!" end}
# not user-controllable, this is ok
# ok:ruby_eval_rule-NoEval
Thing.module_eval(a)
puts Thing.new.hello()
b = params['something']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

def get_binding(param)
  binding
end
b = get_binding("hello")
# ok:ruby_eval_rule-NoEval
b.eval("some_func")

# ok:ruby_eval_rule-NoEval
eval("some_func",b)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid:ruby_eval_rule-NoEval
eval(params.dig('cmd'))

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ok:ruby_eval_rule-NoEval
RubyVM::InstructionSequence.compile("1 + 2").eval

iseq = RubyVM::InstructionSequence.compile(foo)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


iseq = RubyVM::InstructionSequence.compile('num = 1 + 2')
# ok:ruby_eval_rule-NoEval
iseq.eval

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'open3'

def test_params()
  user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# ruleid: ruby_injection_rule-DangerousExec
  Process.spawn([user_input, "smth"])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  commands = "ls -lah /raz/dva"
# ok: ruby_injection_rule-DangerousExec
  system(commands)

  cmd_name = "sh"
# ok: ruby_injection_rule-DangerousExec
  Process.exec([cmd_name, "ls", "-la"])
# ok: ruby_injection_rule-DangerousExec
  Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
# ok: ruby_injection_rule-DangerousExec
  system("ls -lah /tmp")
# ok: ruby_injection_rule-DangerousExec
  exec(["ls", "-lah", "/tmp"])
end

def test_calls(user_input)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end

  def test_params()
    user_input = params['some_key']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  
    commands = "ls -lah /raz/dva"
  # ok: ruby_injection_rule-DangerousExec
    system(commands)
  
    cmd_name = "sh"
  # ok: ruby_injection_rule-DangerousExec
    Process.exec([cmd_name, "ls", "-la"])
  # ok: ruby_injection_rule-DangerousExec
    Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
  # ok: ruby_injection_rule-DangerousExec
    system("ls -lah /tmp")
  # ok: ruby_injection_rule-DangerousExec
    exec(["ls", "-lah", "/tmp"])
  end
  
  def test_cookies()
    user_input = cookies['some_cookie']
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
      commands = "ls -lah /raz/dva"
    # ok: ruby_injection_rule-DangerousExec
      system(commands)
    
      cmd_name = "sh"
    # ok: ruby_injection_rule-DangerousExec
      Process.exec([cmd_name, "ls", "-la"])
    # ok: ruby_injection_rule-DangerousExec
      Open3.capture2({"FOO" => "BAR"}, [cmd_name, "smth"])
    # ok: ruby_injection_rule-DangerousExec
      system("ls -lah /tmp")
    # ok: ruby_injection_rule-DangerousExec
      exec(["ls", "-lah", "/tmp"])
    end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'erb'

class FaxHelper

  def to_fax
    html = File.open(path_to_template).read
    # ruleid: ruby_xss_rule-ManualTemplateCreation
    template = ERB.new(html)
    template.result
  end

end


x = 42
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  The value of x is: <%= x %>
EOF
puts template.result(binding)
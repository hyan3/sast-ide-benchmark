class PersonalAccessTokensController < ApplicationController
    include RenderAccessTokens
    include FeedTokenHelper

    feature_category :system_access

    before_action :check_personal_access_tokens_enabled
    prepend_before_action(only: [:index]) { authenticate_sessionless_user!(:ics) }

    def index
      set_index_vars
      scopes = params[:scopes].split(',').map(&:squish).select(&:present?).map(&:to_sym) unless params[:scopes].nil?
      @personal_access_token = finder.build(
        name: params[:name],
        scopes: scopes
      )

      respond_to do |format|
        format.html
        format.json do
          render json: @active_access_tokens
        end
        format.ics do
          if params[:feed_token]
            response.headers['Content-Type'] = 'text/plain'
            render plain: expiry_ics(@active_access_tokens)
          else
            redirect_to "#{request.path}?feed_token=#{generate_feed_token_with_path(:ics, request.path)}"
          end
        end
      end
    end

    def create
      result = ::PersonalAccessTokens::CreateService.new(
        current_user: current_user,
        target_user: current_user,
        organization_id: Current.organization_id,
        params: personal_access_token_params,
        concatenate_errors: false
      ).execute

      @personal_access_token = result.payload[:personal_access_token]

      tokens, size = active_access_tokens
      if result.success?
        render json: { new_token: @personal_access_token.token,
                       active_access_tokens: tokens, total: size }, status: :ok
      else
        render json: { errors: result.errors }, status: :unprocessable_entity
      end
    end

    def revoke
      @personal_access_token = finder.find(params[:id])
      service = PersonalAccessTokens::RevokeService.new(current_user, token: @personal_access_token).execute
      service.success? ? flash[:notice] = service.message : flash[:alert] = service.message

      redirect_to user_settings_personal_access_tokens_path
    end

    private

    def finder(options = {})
      PersonalAccessTokensFinder.new({ user: current_user, impersonation: false }.merge(options))
    end

    def personal_access_token_params
      params.require(:personal_access_token).permit(:name, :expires_at, scopes: [])
    end

    def set_index_vars
      @scopes = Gitlab::Auth.available_scopes_for(current_user)
      @active_access_tokens, @active_access_tokens_size = active_access_tokens
    end

    def represent(tokens)
      ::PersonalAccessTokenSerializer.new.represent(tokens)
    end

    def check_personal_access_tokens_enabled
      render_404 if Gitlab::CurrentSettings.personal_access_tokens_disabled?
    end
  end

class Admin::ProjectsController < Admin::ApplicationController
  include MembersPresentation

  before_action :project, only: [:show, :transfer, :repository_check, :destroy, :edit, :update]
  before_action :group, only: [:show, :transfer]

  feature_category :groups_and_projects, [:index, :show, :transfer, :destroy, :edit, :update]
  feature_category :source_code_management, [:repository_check]

  def index
    params[:sort] ||= 'latest_activity_desc'
    @sort = params[:sort]

    params[:archived] = true if params[:last_repository_check_failed].present? && params[:archived].nil?

    @projects = Admin::ProjectsFinder.new(params: params, current_user: current_user).execute

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("admin/projects/_projects", projects: @projects)
        }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def show
    if @group
      @group_members = present_members(
        @group.members.order("access_level DESC").page(params[:group_members_page]))
    end

    @project_members = present_members(
      @project.members.page(params[:project_members_page]))
    @requesters = present_members(
      AccessRequestsFinder.new(@project).execute(current_user))
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def destroy
    ::Projects::DestroyService.new(@project, current_user, {}).async_execute
    flash[:toast] = format(_("Project '%{project_name}' is being deleted."), project_name: @project.full_name)

    redirect_to admin_projects_path, status: :found
  rescue Projects::DestroyService::DestroyError => e
    redirect_to admin_projects_path, status: :found, alert: e.message
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def transfer
    namespace = Namespace.find_by(id: params[:new_namespace_id])
    ::Projects::TransferService.new(@project, current_user, params.dup).execute(namespace)

    flash[:alert] = @project.errors[:new_namespace].first if @project.errors[:new_namespace].present?

    @project.reset
    redirect_to admin_project_path(@project)
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def edit; end

  def update
    result = ::Projects::UpdateService.new(@project, current_user, project_params).execute

    if result[:status] == :success
      unless Gitlab::Utils.to_boolean(project_params['runner_registration_enabled'])
        Ci::Runners::ResetRegistrationTokenService.new(@project, current_user).execute
      end

      redirect_to [:admin, @project], notice: format(_("Project '%{project_name}' was successfully updated."), project_name: @project.name)
    else
      render "edit"
    end
  end

  def repository_check
    RepositoryCheck::SingleRepositoryWorker.perform_async(@project.id) # rubocop:disable CodeReuse/Worker

    redirect_to(
      admin_project_path(@project),
      notice: _('Repository check was triggered.')
    )
  end

  protected

  def project
    @project = Project.find_by_full_path(
      [params[:namespace_id], '/', params[:id]].join('')
    )
    @project || render_404
  end

  def group
    @group ||= @project.group
  end

  def project_params
    params.require(:project).permit(allowed_project_params)
  end

  def allowed_project_params
    [
      :description,
      :name,
      :runner_registration_enabled
    ]
  end
end

class MergeRequestDiffSummaryBatchLoader
    NIL_STATS = { additions: 0, deletions: 0, file_count: 0 }.freeze

    def self.load_for(merge_request)
      BatchLoader::GraphQL.for(merge_request).batch(key: :diff_stats_summary) do |merge_requests, loader, args|
        Preloaders::MergeRequestDiffPreloader.new(merge_requests).preload_all

        merge_requests.each do |merge_request|
          metrics = merge_request.metrics

          summary = if metrics && metrics.added_lines && metrics.removed_lines
                      { additions: metrics.added_lines, deletions: metrics.removed_lines, file_count: merge_request.merge_request_diff&.files_count || 0 }
                    elsif merge_request.diff_stats.blank?
                      NIL_STATS
                    else
                      merge_request.diff_stats.each_with_object(NIL_STATS.dup) do |status, summary|
                        summary.merge!(additions: status.additions, deletions: status.deletions, file_count: 1) { |_, x, y| x + y }
                      end
                    end

          loader.call(merge_request, summary)
        end
      end
    end
  end

class ProfilesController < ApplicationController
    include ActionView::Helpers::SanitizeHelper
    include Gitlab::Tracking

    before_action :user
    skip_before_action :require_email, only: [:show, :update]
    feature_category :user_profile, [:show, :update]

    urgency :low, [:show, :update]

    def show; end

    def update
      respond_to do |format|
        result = Users::UpdateService.new(current_user, user_params.merge(user: @user)).execute(check_password: true)

        if result[:status] == :success
          message = s_("Profiles|Profile was successfully updated")

          format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
          format.json { render json: { message: message } }
        else
          format.html do
            redirect_back_or_default(default: { action: 'show' }, options: { alert: result[:message] })
          end
          format.json { render json: result }
        end
      end
    end

    private

    def user
      @user = current_user
    end

    def user_params_attributes
      [
        :achievements_enabled,
        :avatar,
        :bio,
        :bluesky,
        :commit_email,
        :discord,
        :email,
        :gitpod_enabled,
        :hide_no_password,
        :hide_no_ssh_key,
        :hide_project_limit,
        :include_private_contributions,
        :job_title,
        :linkedin,
        :location,
        :mastodon,
        :name,
        :organization,
        :private_profile,
        :pronouns,
        :pronunciation,
        :public_email,
        :role,
        :skype,
        :timezone,
        :twitter,
        :username,
        :validation_password,
        :website_url,
        { status: [:emoji, :message, :availability, :clear_status_after] }
      ]
    end

    def user_params
      @user_params ||= params.require(:user).permit(user_params_attributes)
    end
  end

class LabelsFinder < UnionFinder
  prepend FinderWithCrossProjectAccess
  include FinderWithGroupHierarchy
  include FinderMethods
  include Gitlab::Utils::StrongMemoize

  requires_cross_project_access unless: -> { project? }

  def initialize(current_user, params = {})
    @current_user = current_user
    @params = params
    # Preload container records (project, group) by default, in some cases we invoke
    # the LabelsPreloader on the loaded records to prevent all N+1 queries.
    # In that case we disable the default with_preloaded_container scope because it
    # interferes with the LabelsPreloader.
    @preload_parent_association = params.fetch(:preload_parent_association, true)
  end

  def execute(skip_authorization: false)
    @skip_authorization = skip_authorization
    items = find_union(item_ids, Label) || Label.none
    items = with_title(items)
    items = by_subscription(items)
    items = by_search(items)
    items = by_locked_labels(items)

    items = items.with_preloaded_container if @preload_parent_association
    sort(items)
  end

  private

  attr_reader :current_user, :params, :skip_authorization

  # rubocop: disable CodeReuse/ActiveRecord
  def item_ids
    item_ids = []

    if project?
      if project
        if project.group.present?
          labels_table = Label.arel_table
          group_ids = group_ids_for(project.group)

          item_ids << Label.where(
            labels_table[:type].eq('GroupLabel').and(labels_table[:group_id].in(group_ids)).or(
              labels_table[:type].eq('ProjectLabel').and(labels_table[:project_id].eq(project.id))
            )
          )
        else
          item_ids << project.labels
        end
      end
    else
      item_ids << Label.where(group_id: group_ids_for(group)) if group?

      item_ids << Label.where(group_id: projects.group_ids)
      item_ids << Label.where(project_id: ids_user_can_read_labels(projects)) unless only_group_labels?
    end

    item_ids
  end
  # rubocop: enable CodeReuse/ActiveRecord

  # rubocop: disable CodeReuse/ActiveRecord
  def sort(items)
    return items.reorder(title: :asc) unless params[:sort]

    return items.sorted_by_similarity_desc(params[:search]) if params[:sort] == 'relevance' && params[:search].present?

    items.order_by(params[:sort])
  end
  # rubocop: enable CodeReuse/ActiveRecord

  # rubocop: disable CodeReuse/ActiveRecord
  def with_title(items)
    return items if title.nil?
    return items.none if title.blank?

    items.where(title: title)
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def by_search(labels)
    return labels unless search?

    labels.search(params[:search], search_in: params[:search_in])
  end

  def by_subscription(labels)
    labels.optionally_subscribed_by(subscriber_id)
  end

  def by_locked_labels(items)
    return items unless params[:locked_labels]

    items.with_lock_on_merge
  end

  def subscriber_id
    current_user&.id if subscribed?
  end

  def subscribed?
    params[:subscribed] == 'true'
  end

  def projects?
    params[:project_ids]
  end

  def only_group_labels?
    params[:only_group_labels]
  end

  def search?
    params[:search].present?
  end

  def title
    params[:title] || params[:name]
  end

  def project?
    params[:project].present? || params[:project_id].present?
  end

  def project
    return @project if defined?(@project)

    if project?
      @project = params[:project] || Project.find(params[:project_id])
      @project = nil unless authorized_to_read_item?(@project)
    else
      @project = nil
    end

    @project
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def projects
    return @projects if defined?(@projects)

    @projects = if skip_authorization
                  Project.all
                else
                  ProjectsFinder.new(params: { non_archived: true }, current_user: current_user).execute # rubocop: disable CodeReuse/Finder
                end

    if group?
      @projects = if params[:include_descendant_groups]
                    @projects.in_namespace(group.self_and_descendants.select(:id))
                  else
                    @projects.in_namespace(group.id)
                  end
    end

    @projects = @projects.where(id: params[:project_ids]) if projects?
    @projects = @projects.reorder(nil)

    @projects
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def read_permission
    :read_label
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def ids_user_can_read_labels(projects)
    Project.where(id: projects.select(:id)).ids_with_issuables_available_for(current_user)
  end
  # rubocop: enable CodeReuse/ActiveRecord
end

class OrganizationsController < ApplicationController
    include PreviewMarkdown
    include FiltersEvents

    DEFAULT_RESOURCE_LIMIT = 1000
    DEFAULT_ACTIVITY_EVENT_LIMIT = 20

    feature_category :cell

    before_action :event_filter, only: [:activity]
    before_action :authorize_read_organization!, only: [:activity, :show, :groups_and_projects]
    before_action only: [:index] do
      push_frontend_feature_flag(:allow_organization_creation, current_user)
    end

    skip_before_action :authenticate_user!, only: [:activity, :show, :groups_and_projects]

    urgency :low, [:activity]

    def index; end

    def new
      authorize_create_organization!
    end

    def show; end

    def activity
      respond_to do |format|
        format.html
        format.json do
          load_events
          # load_events queries for limit + 1.
          # This will be removed as part of https://gitlab.com/gitlab-org/gitlab/-/issues/382473
          has_next_page = @events.length > activity_query_limit
          @events.pop if has_next_page

          @events = @events.select { |event| event.visible_to_user?(current_user) }

          render json: \
            { events: ::Profile::EventSerializer.new(current_user: current_user).represent(@events), \
              has_next_page: has_next_page }
        end
      end
    end

    def groups_and_projects; end

    def users
      authorize_read_organization_user!
    end

    private

    def activity_query_limit
      return params[:limit].to_i unless !params[:limit] || params[:limit].to_i > DEFAULT_ACTIVITY_EVENT_LIMIT

      DEFAULT_ACTIVITY_EVENT_LIMIT
    end

    def load_events
      @events = EventCollection.new(
        organization.projects.limit(DEFAULT_RESOURCE_LIMIT).sorted_by_activity,
        offset: params[:offset].to_i,
        filter: event_filter,
        # limit + 1 allows us to determine if we have another page.
        # This will be removed as part of https://gitlab.com/gitlab-org/gitlab/-/issues/382473
        limit: activity_query_limit + 1,
        groups: organization.groups.limit(DEFAULT_RESOURCE_LIMIT)
      ).to_a.map(&:present)

      Events::RenderService.new(current_user).execute(@events)
    end
  end

class VariablesController < Groups::ApplicationController
    before_action :authorize_admin_group!, except: :update
    before_action :authorize_admin_cicd_variables!, only: :update

    skip_cross_project_access_check :show, :update

    feature_category :secrets_management

    urgency :low, [:show]

    def show
      respond_to do |format|
        format.json do
          render status: :ok, json: { variables: ::Ci::GroupVariableSerializer.new.represent(@group.variables) }
        end
      end
    end

    def update
      update_result = Ci::ChangeVariablesService.new(
        container: @group, current_user: current_user,
        params: group_variables_params
      ).execute

      if update_result
        respond_to do |format|
          format.json { render_group_variables }
        end
      else
        respond_to do |format|
          format.json { render_error }
        end
      end
    end

    private

    def render_group_variables
      render status: :ok, json: { variables: ::Ci::GroupVariableSerializer.new.represent(@group.variables) }
    end

    def render_error
      render status: :bad_request, json: @group.errors.full_messages
    end

    def group_variables_params
      params.permit(variables_attributes: Array(variable_params_attributes))
    end

    def variable_params_attributes
      %i[id variable_type key description secret_value protected masked hidden raw _destroy]
    end
  end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

uid = params[:uid]
# ruleid: ruby_session_rule-AvoidSessionManipulation
id = session[uid]

# ok: ruby_session_rule-AvoidSessionManipulation
id = session[user_id]
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

    #ruleid: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ "admin = ? and x = #{params[:having]}", cookies[:admin]])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

# frozen_string_literal: true

require "abstract_unit"
require "controller/fake_controllers"

class ActionPackAssertionsController < ActionController::Base
  def nothing() head :ok end

  # ok: ruby_xss_rule-AvoidRenderText
  def hello_xml_world() render template: "test/hello_xml_world"; end

  def assign_this
    @howdy = "ho"
    render inline: "Mr. Henke"
  end

  def render_based_on_parameters
    # ok: ruby_xss_rule-AvoidRenderText
    render plain: "Mr. #{params[:name]}"
  end

  def render_url
    # ok: ruby_xss_rule-AvoidRenderText
    render html: "<div>#{url_for(action: 'flash_me', only_path: true)}</div>"
  end

  def render_text_with_custom_content_type
    # ok: ruby_xss_rule-AvoidRenderText
    render body: "Hello!", content_type: Mime[:rss]
  end

  def session_stuffing
    session["xmas"] = "turkey"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def raise_exception_on_get
    raise "get" if request.get?
    # ruleid: ruby_xss_rule-AvoidRenderText
    render text: "request method: #{request.env['REQUEST_METHOD']}"
  end

  def raise_exception_on_post
    raise "post" if request.post?
    # ok: ruby_xss_rule-AvoidRenderText
    render plain: "request method: #{request.env['REQUEST_METHOD']}"
  end

  def render_file_absolute_path
    # ok: ruby_xss_rule-AvoidRenderText
    render file: File.expand_path("../../README.rdoc", __dir__)
  end

  def render_file_relative_path
    # ok: ruby_xss_rule-AvoidRenderText
    render file: "README.rdoc"
  end
end

# Used to test that assert_response includes the exception message
# in the failure message when an action raises and assert_response
# is expecting something other than an error.
class AssertResponseWithUnexpectedErrorController < ActionController::Base
  def index
    raise "FAIL"
  end

  def show
    # ok: ruby_xss_rule-AvoidRenderText
    render plain: "Boom", status: 500
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    #ruleid: ruby_sql_rule-CheckSQL
    Product.where(["admin = ? AND user_name = #{@name}", params[:admin]])
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Product < ActiveRecord::Base
  def test_find_order
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_group
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_having
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :having => { :x => params[:having]})

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ 'x = ?', params[:having]])

    #ruleid: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => ['name = ?', params[:name]], :having => [ "admin = ? and x = #{params[:having]}", cookies[:admin]])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_joins
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => "LEFT JOIN comments ON comments.post_id = id")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.find(:first, :conditions => 'admin = 1', :joins => [:x, :y])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_select
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :select => "name")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_from
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :from => "users")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_find_lock
    #ok: ruby_sql_rule-CheckSQL
    Product.find(:last, :conditions => 'admin = 1', :lock => true)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_where
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = 1")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("admin = ?", params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = ?", params[:admin]])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(["admin = :admin", { :admin => params[:admin] }])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin])
    #ok: ruby_sql_rule-CheckSQL
    Product.where(:admin => params[:admin], :some_param => params[:some_param])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  TOTALLY_SAFE = "some safe string"

  def test_constant_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{TOTALLY_SAFE}")
  end

  def test_local_interpolation
    #this is a weak finding and should be covered by a different rule
    #ok: ruby_sql_rule-CheckSQL
    Product.first("blah = #{local_var}")
  end

  def test_conditional_args_in_sql
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:blah] ? 1 : 0}'")
  
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_in_args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_params_to_i
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_i}'")
  end

  def test_more_if_statements
    if some_condition
      x = params[:x]
    else
      x = "BLAH"
    end

    y = if some_other_condition
      params[:x]
      "blah"
    else
      params[:y]
      "blah"
    end

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{y}'")
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = 1").group(y)
  end

  def test_calculations
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_select
    #ok: ruby_sql_rule-CheckSQL
    Product.select([:price, :sku])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_conditional_in_options
    x = params[:x] == y ? "created_at ASC" : "created_at DESC"
    z = params[:y] == y ? "safe" : "totally safe"

    #ok: ruby_sql_rule-CheckSQL
    Product.all(:order => x, :having => z, :select => z, :from => z,
                :group => z)
  end

  def test_or_interpolation
    #ok: ruby_sql_rule-CheckSQL
    Product.where("blah = #{1 or 2}")
  end

  def test_params_to_f
    #ok: ruby_sql_rule-CheckSQL
    Product.last("blah = '#{params[:id].to_f}'")
  end

  def test_interpolation_in_first_arg
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  end

  def test_to_sql_interpolation
    #ok: ruby_sql_rule-CheckSQL
    prices = Product.select(:price).where("created_at < :time").to_sql
    #ok: ruby_sql_rule-CheckSQL
    where("price IN (#{prices}) OR whatever", :price => some_price)
  end
end
class TagsController < ::Projects::Registry::ApplicationController
      include PackagesHelper

      before_action :authorize_destroy_container_image!, only: [:destroy]

      LIMIT = 15

      def index
        track_package_event(:list_tags, :tag)

        respond_to do |format|
          format.json do
            render json: ContainerTagsSerializer
              .new(project: @project, current_user: @current_user)
              .with_pagination(request, response)
              .represent(tags)
          end
        end
      end

      def destroy
        result = Projects::ContainerRepository::DeleteTagsService
          .new(image.project, current_user, tags: [params[:id]])
          .execute(image)
        track_package_event(:delete_tag, :tag)

        respond_to do |format|
          format.json { head(result[:status] == :success ? :ok : bad_request) }
        end
      end

      def bulk_destroy
        tag_names = params.require(:ids) || []
        if tag_names.size > LIMIT
          head :bad_request
          return
        end

        result = Projects::ContainerRepository::DeleteTagsService
          .new(image.project, current_user, tags: tag_names)
          .execute(image)
        track_package_event(:delete_tag_bulk, :tag)

        respond_to do |format|
          format.json { head(result[:status] == :success ? :no_content : :bad_request) }
        end
      end

      private

      def tags
        Kaminari::PaginatableArray.new(image.tags, limit: LIMIT)
      end

      def image
        @image ||= project.container_repositories
          .find(params[:repository_id])
      end
    end

class Projects::ForksController < Projects::ApplicationController
  include ContinueParams
  include RendersMemberAccess
  include RendersProjectsList
  include Gitlab::Utils::StrongMemoize

  # Authorize
  before_action :disable_query_limiting, only: [:create]
  before_action :require_non_empty_project
  before_action :authorize_read_code!
  before_action :authenticate_user!, only: [:new, :create]
  before_action :authorize_fork_project!, except: [:index]
  before_action :authorize_fork_namespace!, only: [:create]

  feature_category :source_code_management
  urgency :low, [:index]

  def index
    @sort = forks_params[:sort]

    @total_forks_count    = project.forks.size
    @public_forks_count   = project.forks.public_only.size
    @private_forks_count  = @total_forks_count - project.forks.public_and_internal_only.size
    @internal_forks_count = @total_forks_count - @public_forks_count - @private_forks_count

    @forks = load_forks.page(forks_params[:page])

    prepare_projects_for_rendering(@forks)

    respond_to do |format|
      format.html

      format.json do
        render json: {
          html: view_to_html_string("projects/forks/_projects", projects: @forks)
        }
      end
    end
  end

  def new
    respond_to do |format|
      format.html do
        @own_namespace = current_user.namespace if can_fork_to?(current_user.namespace)
        @project = project
      end

      format.json do
        namespaces = load_namespaces_with_associations - [project.namespace]

        namespaces = [current_user.namespace] + namespaces if can_fork_to?(current_user.namespace)

        render json: {
          namespaces: ForkNamespaceSerializer.new.represent(
            namespaces,
            project: project,
            current_user: current_user,
            memberships: memberships_hash,
            forked_projects: forked_projects_by_namespace(namespaces)
          )
        }
      end
    end
  end

  def create
    @forked_project = fork_namespace.projects.find_by(path: project.path) # rubocop: disable CodeReuse/ActiveRecord
    @forked_project = nil unless @forked_project && @forked_project.forked_from_project == project

    unless @forked_project
      @fork_response = fork_service.execute

      @forked_project ||= @fork_response[:project] if @fork_response.success?
    end

    if defined?(@fork_response) && @fork_response.error?
      render :error
    elsif @forked_project.import_in_progress?
      redirect_to project_import_path(@forked_project, continue: continue_params)
    elsif continue_params[:to]
      redirect_to continue_params[:to], notice: continue_params[:notice]
    else
      redirect_to project_path(@forked_project),
        notice: "The project '#{@forked_project.name}' was successfully forked."
    end
  end

  private

  def can_fork_to?(namespace)
    ForkTargetsFinder.new(@project, current_user).execute.id_in(current_user.namespace).any?
  end

  def load_forks
    forks = ForkProjectsFinder.new(
      project,
      params: forks_params.merge(search: forks_params[:filter_projects]),
      current_user: current_user
    ).execute

    # rubocop: disable CodeReuse/ActiveRecord
    forks.includes(:route, :creator, :group, :topics, namespace: [:route, :owner])
    # rubocop: enable CodeReuse/ActiveRecord
  end

  def fork_service
    strong_memoize(:fork_service) do
      ::Projects::ForkService.new(project, current_user, fork_params)
    end
  end

  def fork_namespace
    strong_memoize(:fork_namespace) do
      Namespace.find(params[:namespace_key]) if params[:namespace_key].present?
    end
  end

  def forks_params
    params.permit(:filter_projects, :sort, :page)
  end

  def fork_params
    params.permit(:path, :name, :description, :visibility).tap do |param|
      param[:namespace] = fork_namespace
    end
  end

  def authorize_fork_namespace!
    access_denied! unless fork_namespace && fork_service.valid_fork_target?
  end

  def disable_query_limiting
    Gitlab::QueryLimiting.disable!('https://gitlab.com/gitlab-org/gitlab/-/issues/20783')
  end

  def load_namespaces_with_associations
    # rubocop: disable CodeReuse/ActiveRecord
    @load_namespaces_with_associations ||= fork_service.valid_fork_targets(only_groups: true).preload(:route)
    # rubocop: enable CodeReuse/ActiveRecord
  end

  def memberships_hash
    # rubocop: disable CodeReuse/ActiveRecord
    current_user.members.where(source: load_namespaces_with_associations).index_by(&:source_id)
    # rubocop: enable CodeReuse/ActiveRecord
  end

  def forked_projects_by_namespace(namespaces)
    # rubocop: disable CodeReuse/ActiveRecord
    project.forks.where(namespace: namespaces).includes(:namespace).index_by(&:namespace_id)
    # rubocop: enable CodeReuse/ActiveRecord
  end
end

class UsersController < ApplicationController
  include InternalRedirect
  include RoutableActions
  include RendersMemberAccess
  include RendersProjectsList
  include ControllerWithCrossProjectAccessCheck
  include Gitlab::NoteableMetadata

  requires_cross_project_access show: false,
    groups: false,
    projects: false,
    contributed: false,
    snippets: true,
    calendar: false,
    followers: false,
    following: false,
    calendar_activities: true

  skip_before_action :authenticate_user!
  prepend_before_action(only: [:show]) { authenticate_sessionless_user!(:rss) }
  before_action :user, except: [:exists]
  before_action :set_legacy_data
  before_action :authorize_read_user_profile!, only: [
    :calendar, :calendar_activities, :groups, :projects, :contributed, :starred, :snippets, :followers, :following
  ]
  before_action only: [:exists] do
    check_rate_limit!(:username_exists, scope: request.ip)
  end
  before_action only: [:show, :activity, :groups, :projects, :contributed, :starred, :snippets, :followers, :following] do
    push_frontend_feature_flag(:profile_tabs_vue, current_user)
  end

  feature_category :user_profile, [:show, :activity, :groups, :projects, :contributed, :starred,
    :followers, :following, :calendar, :calendar_activities,
    :exists, :activity, :follow, :unfollow, :ssh_keys]

  feature_category :source_code_management, [:snippets, :gpg_keys]

  # TODO: Set higher urgency after resolving https://gitlab.com/gitlab-org/gitlab/-/issues/357914
  urgency :low, [:show, :calendar_activities, :contributed, :activity, :projects, :groups, :calendar, :snippets]
  urgency :default, [:followers, :following, :starred]
  urgency :high, [:exists]

  def show
    respond_to do |format|
      format.html

      format.atom do
        load_events
        render layout: 'xml'
      end

      format.json do
        msg = "This endpoint is deprecated. Use %s instead." % user_activity_path
        render json: { message: msg }, status: :not_found
      end
    end
  end

  # Get all keys of a user(params[:username]) in a text format
  # Helpful for sysadmins to put in respective servers
  def ssh_keys
    keys = user.all_ssh_keys.join("\n")
    keys << "\n" unless keys.empty?
    render plain: keys
  end

  def activity
    respond_to do |format|
      format.html { render 'show' }

      format.json do
        load_events

        if Feature.enabled?(:profile_tabs_vue, current_user)
          @events = if user.include_private_contributions?
                      @events
                    else
                      @events.select { |event| event.visible_to_user?(current_user) }
                    end

          render json: ::Profile::EventSerializer.new(current_user: current_user, target_user: user)
                                                 .represent(@events)
        else
          pager_json("events/_events", @events.count, events: @events)
        end
      end
    end
  end

  # Get all gpg keys of a user(params[:username]) in a text format
  def gpg_keys
    keys = user.gpg_keys.filter_map { |gpg_key| gpg_key.key if gpg_key.verified? }.join("\n")
    keys << "\n" unless keys.empty?
    render plain: keys
  end

  def groups
    respond_to do |format|
      format.html { render 'show' }
      format.json do
        load_groups

        render json: {
          html: view_to_html_string("shared/groups/_list", groups: @groups)
        }
      end
    end
  end

  def projects
    present_projects do
      load_projects
    end
  end

  def contributed
    present_projects do
      load_contributed_projects
    end
  end

  def starred
    present_projects do
      load_starred_projects
    end
  end

  def followers
    present_users do
      @user_followers = user.followers.page(params[:page])
    end
  end

  def following
    present_users do
      @user_following = user.followees.page(params[:page])
    end
  end

  def present_projects
    skip_pagination = Gitlab::Utils.to_boolean(params[:skip_pagination])
    skip_namespace = Gitlab::Utils.to_boolean(params[:skip_namespace])
    compact_mode = Gitlab::Utils.to_boolean(params[:compact_mode])
    card_mode = Gitlab::Utils.to_boolean(params[:card_mode])

    respond_to do |format|
      format.html { render 'show' }
      format.json do
        projects = yield

        pager_json("shared/projects/_list", projects.count, projects: projects, skip_pagination: skip_pagination, skip_namespace: skip_namespace, compact_mode: compact_mode, card_mode: card_mode)
      end
    end
  end

  def snippets
    respond_to do |format|
      format.html { render 'show' }
      format.json do
        load_snippets

        render json: {
          html: view_to_html_string("snippets/_snippets", collection: @snippets)
        }
      end
    end
  end

  def calendar
    render json: contributions_calendar.activity_dates
  end

  def calendar_activities
    @calendar_date = begin
      Date.parse(params[:date])
    rescue StandardError
      Date.today
    end
    @events = contributions_calendar.events_by_date(@calendar_date).map(&:present)

    render 'calendar_activities', layout: false
  end

  def exists
    if Gitlab::CurrentSettings.signup_enabled? || current_user
      render json: { exists: Namespace.username_reserved?(params[:username]) }
    else
      render json: { error: _('You must be authenticated to access this path.') }, status: :unauthorized
    end
  end

  def follow
    followee = current_user.follow(user)

    if followee
      flash[:alert] = followee.errors.full_messages.join(', ') if followee&.errors&.any?
    else
      flash[:alert] = s_('Action not allowed.')
    end

    redirect_path = referer_path(request) || @user

    redirect_to redirect_path
  end

  def unfollow
    response = ::Users::UnfollowService.new(
      follower: current_user,
      followee: user
    ).execute

    flash[:alert] = response.message if response.error?
    redirect_path = referer_path(request) || @user

    redirect_to redirect_path
  end

  private

  def user
    @user ||= find_routable!(User, params[:username], request.fullpath)
  end

  def personal_projects
    PersonalProjectsFinder.new(user).execute(current_user)
  end

  def contributed_projects
    ContributedProjectsFinder.new(
      user: user, current_user: current_user, params: { sort: 'latest_activity_desc' }
    ).execute
  end

  def starred_projects
    StarredProjectsFinder.new(user, params: finder_params, current_user: current_user).execute
  end

  def contributions_calendar
    @contributions_calendar ||= Gitlab::ContributionsCalendar.new(user, current_user)
  end

  def load_events
    @events = UserRecentEventsFinder.new(current_user, user, nil, params).execute

    Events::RenderService.new(current_user).execute(@events, atom_request: request.format.atom?)
  end

  def load_projects
    @projects = personal_projects
      .page(params[:page])
      .per(params[:limit])

    prepare_projects_for_rendering(@projects)
  end

  def load_contributed_projects
    @contributed_projects = contributed_projects.with_route.joined(user).page(params[:page]).without_count

    prepare_projects_for_rendering(@contributed_projects)
  end

  def load_starred_projects
    @starred_projects = starred_projects

    prepare_projects_for_rendering(@starred_projects)
  end

  def load_groups
    groups = JoinedGroupsFinder.new(user).execute(current_user)
    @groups = groups.page(params[:page]).without_count

    prepare_groups_for_rendering(@groups)
  end

  def load_snippets
    @snippets = SnippetsFinder.new(current_user, author: user, scope: params[:scope])
      .execute
      .page(params[:page])
      .inc_author

    @noteable_meta_data = noteable_meta_data(@snippets, 'Snippet')
  end

  def build_canonical_path(user)
    url_for(safe_params.merge(username: user.to_param))
  end

  def authorize_read_user_profile!
    access_denied! unless can?(current_user, :read_user_profile, user)
  end

  def present_users
    respond_to do |format|
      format.html { render 'show' }
      format.json do
        users = yield
        render json: {
          html: view_to_html_string("shared/users/index", users: users)
        }
      end
    end
  end

  def finder_params
    {
      # don't display projects marked for deletion
      not_aimed_for_deletion: true
    }
  end

  def set_legacy_data
    controller_action = params[:action]
    @action = controller_action.gsub('show', 'overview')
    @endpoint = request.path
  end
end

class BroadcastMessagesController < ApplicationController
    include Admin::BroadcastMessagesHelper

    before_action :find_broadcast_message, only: [:edit, :update, :destroy]
    before_action :find_broadcast_messages, only: [:index, :create]

    feature_category :notifications
    urgency :low

    def index
      @broadcast_message = System::BroadcastMessage.new
    end

    def edit; end

    def create
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      success = @broadcast_message.save

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully created.')
          else
            render :index
          end
        end
      end
    end

    def update
      success = @broadcast_message.update(broadcast_message_params)

      respond_to do |format|
        format.json do
          if success
            render json: @broadcast_message, status: :ok
          else
            render json: { errors: @broadcast_message.errors.full_messages }, status: :bad_request
          end
        end
        format.html do
          if success
            redirect_to admin_broadcast_messages_path, notice: _('Broadcast Message was successfully updated.')
          else
            render :edit
          end
        end
      end
    end

    def destroy
      @broadcast_message.destroy

      respond_to do |format|
        format.html { redirect_back_or_default(default: { action: 'index' }) }
        format.js { head :ok }
      end
    end

    def preview
      @broadcast_message = System::BroadcastMessage.new(broadcast_message_params)
      render plain: render_broadcast_message(@broadcast_message), status: :ok
    end

    protected

    def find_broadcast_message
      @broadcast_message = System::BroadcastMessage.find(params.permit(:id)[:id])
    end

    def find_broadcast_messages
      @broadcast_messages = System::BroadcastMessage.order(ends_at: :desc).page(pagination_params[:page]) # rubocop: disable CodeReuse/ActiveRecord
    end

    def broadcast_message_params
      params.require(:broadcast_message)
        .permit(%i[
          theme
          ends_at
          message
          starts_at
          target_path
          broadcast_type
          dismissable
          show_in_cli
        ], target_access_levels: []).reverse_merge!(target_access_levels: [])
    end
  end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Account < ActiveRecord::Base
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  serialize :cc_info #safe from CVE-2013-0277
  attr_accessible :blah_admin_blah
end

class Account < ActiveRecord::Base
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  #ruleid: ruby_regex_rule-CheckValidationRegex
  validates_format_of :blah2, :with => /^[a-zA-Z]+\Z/
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  #ok: ruby_regex_rule-CheckValidationRegex
  validates_format_of :good_valid, :with => /\A[a-zA-Z]\z/ #No warning
  #ok: ruby_regex_rule-CheckValidationRegex
  validates_format_of :not_bad, :with => /\A[a-zA-Z]\Z/ #No warning

  def mass_assign_it
    Account.new(params[:account_info]).some_other_method
  end

  def test_class_eval
    #Should not raise a warning
    User.class_eval do
      attr_reader :some_private_thing
    end
  end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def bad_deserialization
   data = request.env[:name]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ruleid: ruby_deserialization_rule-BadDeserializationEnv
   obj = Marshal.restore(data)

   o = Klass.new("hello\n")
   data = request.env[:name]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ok: ruby_deserialization_rule-BadDeserializationEnv
   obj = Oj.load(data,options=some_safe_options)
 end

 def ok_deserialization
    
    data = get_safe_data()
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(data)
 end

 def safe_marshal_restore_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal.restore"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.restore(Base64.decode64(safe_data))
    render plain: "Safe Marshal.restore executed with #{obj}"
  end

  def safe_oj_object_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj.object_load" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.object_load(safe_data)
    render plain: "Safe Oj.object_load executed with message: #{obj['message']}"
  end

  def safe_marshal_load_test
    safe_data = Base64.encode64(Marshal.dump("Safe test string for Marshal"))
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Marshal.load(Base64.decode64(safe_data))
    render plain: "Safe Marshal.load executed with #{obj}"
  end

  def safe_oj_load_test
    safe_data = Oj.dump({ message: "Safe test string for Oj" }, mode: :strict)
    # ok: ruby_deserialization_rule-BadDeserializationEnv
    obj = Oj.load(safe_data)
    render plain: "Safe Oj.load executed with message: #{obj['message']}"
  end

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

 def divide_by_zero
   # ruleid: ruby_error_rule-DivideByZero
   3/0
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   variable = 3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   zero = 0
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   # ok: ruby_error_rule-DivideByZero
   ok = 1.0 / 0
   # ok: ruby_error_rule-DivideByZero
   ok2 = 2.0 / zero
   
 end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

        # ruleid: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA1.new
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
        # ok: ruby_crypto_rule-WeakHashesSHA1
        OpenSSL::HMAC.hexdigest("SHA256", key, data)
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.new
        # ok: ruby_crypto_rule-WeakHashesSHA1
        digest = OpenSSL::Digest::SHA256.hexdigest 'abc'
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
class BadController < ApplicationController
  #Examples of skipping important filters with a blacklist instead of whitelist
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
  # ruleid: ruby_filter_rule-CheckBeforeFilter
  skip_filter :authenticate_user!, :except => :do_admin_stuff
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    def do_admin_stuff
        #do some stuff
    end
    
    def do_anonymous_stuff
      # do some stuff
    end
end

class GoodController < ApplicationController
  #Examples of skipping important filters with a blacklist instead of whitelist
  # ok: ruby_filter_rule-CheckBeforeFilter
  skip_before_filter :login_required, :only => :do_anonymous_stuff
  # ok: ruby_filter_rule-CheckBeforeFilter
  skip_filter :authenticate_user!, :only => :do_anonymous_stuff
  # ok: ruby_filter_rule-CheckBeforeFilter
  skip_before_filter :require_user, :only => [:do_anonymous_stuff, :do_nocontext_stuff]

    def do_admin_stuff
        #do some stuff
    end
    
    def do_anonymous_stuff
      # do some stuff
    end

    def do_nocontext_stuff
      # do some stuff
    end
end
class Admin::PlanLimitsController < Admin::ApplicationController
  include InternalRedirect

  before_action :set_plan_limits

  feature_category :not_owned # rubocop:todo Gitlab/AvoidFeatureCategoryNotOwned

  def create
    redirect_path = referer_path(request) || general_admin_application_settings_path

    respond_to do |format|
      if @plan_limits.update(plan_limits_params)
        format.json { head :ok }
        format.html { redirect_to redirect_path, notice: _('Application limits saved successfully') }
      else
        format.json { head :bad_request }
        format.html { render_update_error }
      end
    end
  end

  private

  def set_plan_limits
    @plan_limits = Plan.find(plan_limits_params[:plan_id]).actual_limits
  end

  def plan_limits_params
    params.require(:plan_limits)
      .permit(%i[
        plan_id
        conan_max_file_size
        helm_max_file_size
        maven_max_file_size
        npm_max_file_size
        nuget_max_file_size
        pypi_max_file_size
        terraform_module_max_file_size
        generic_packages_max_file_size
        ci_instance_level_variables
        ci_pipeline_size
        ci_active_jobs
        ci_project_subscriptions
        ci_pipeline_schedules
        ci_needs_size_limit
        ci_registered_group_runners
        ci_registered_project_runners
        dotenv_size
        dotenv_variables
        pipeline_hierarchy_size
      ])
  end
end

class TagsController < ::Projects::Registry::ApplicationController
      include PackagesHelper

      before_action :authorize_destroy_container_image!, only: [:destroy]

      LIMIT = 15

      def index
        track_package_event(:list_tags, :tag)

        respond_to do |format|
          format.json do
            render json: ContainerTagsSerializer
              .new(project: @project, current_user: @current_user)
              .with_pagination(request, response)
              .represent(tags)
          end
        end
      end

      def destroy
        result = Projects::ContainerRepository::DeleteTagsService
          .new(image.project, current_user, tags: [params[:id]])
          .execute(image)
        track_package_event(:delete_tag, :tag)

        respond_to do |format|
          format.json { head(result[:status] == :success ? :ok : bad_request) }
        end
      end

      def bulk_destroy
        tag_names = params.require(:ids) || []
        if tag_names.size > LIMIT
          head :bad_request
          return
        end

        result = Projects::ContainerRepository::DeleteTagsService
          .new(image.project, current_user, tags: tag_names)
          .execute(image)
        track_package_event(:delete_tag_bulk, :tag)

        respond_to do |format|
          format.json { head(result[:status] == :success ? :no_content : :bad_request) }
        end
      end

      private

      def tags
        Kaminari::PaginatableArray.new(image.tags, limit: LIMIT)
      end

      def image
        @image ||= project.container_repositories
          .find(params[:repository_id])
      end
    end

class ProfilesController < ApplicationController
    include ActionView::Helpers::SanitizeHelper
    include Gitlab::Tracking

    before_action :user
    skip_before_action :require_email, only: [:show, :update]
    feature_category :user_profile, [:show, :update]

    urgency :low, [:show, :update]

    def show; end

    def update
      respond_to do |format|
        result = Users::UpdateService.new(current_user, user_params.merge(user: @user)).execute(check_password: true)

        if result[:status] == :success
          message = s_("Profiles|Profile was successfully updated")

          format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
          format.json { render json: { message: message } }
        else
          format.html do
            redirect_back_or_default(default: { action: 'show' }, options: { alert: result[:message] })
          end
          format.json { render json: result }
        end
      end
    end

    private

    def user
      @user = current_user
    end

    def user_params_attributes
      [
        :achievements_enabled,
        :avatar,
        :bio,
        :bluesky,
        :commit_email,
        :discord,
        :email,
        :gitpod_enabled,
        :hide_no_password,
        :hide_no_ssh_key,
        :hide_project_limit,
        :include_private_contributions,
        :job_title,
        :linkedin,
        :location,
        :mastodon,
        :name,
        :organization,
        :private_profile,
        :pronouns,
        :pronunciation,
        :public_email,
        :role,
        :skype,
        :timezone,
        :twitter,
        :username,
        :validation_password,
        :website_url,
        { status: [:emoji, :message, :availability, :clear_status_after] }
      ]
    end

    def user_params
      @user_params ||= params.require(:user).permit(user_params_attributes)
    end
  end

class IssuesFinder < IssuableFinder
  extend ::Gitlab::Utils::Override

  def self.scalar_params
    @scalar_params ||= super + [:due_date]
  end

  def klass
    Issue
  end

  def params_class
    self.class.const_get(:Params, false)
  end

  private

  def filter_items(items)
    issues = super
    issues = by_due_date(issues)
    issues = by_due_after_or_before(issues)
    issues = by_confidential(issues)
    by_issue_types(issues)
  end

  # Negates all params found in `negatable_params`
  def filter_negated_items(items)
    issues = super
    by_negated_issue_types(issues)
  end

  override :filter_by_full_text_search
  def filter_by_full_text_search(items)
    # This project condition is used as a hint to PG about the partitions that need searching
    # because the search data is partitioned by project.
    # In certain cases, like the recent items search, the query plan is much better without this condition.
    return super if params[:skip_full_text_search_project_condition].present?

    super.with_projects_matching_search_data
  end

  def by_confidential(items)
    Issues::ConfidentialityFilter.new(
      current_user: current_user,
      params: original_params,
      parent: params.parent,
      assignee_filter: assignee_filter
    ).filter(items)
  end

  def by_due_after_or_before(items)
    items = items.due_after(params[:due_after]) if params[:due_after].present?
    items = items.due_before(params[:due_before]) if params[:due_before].present?

    items
  end

  def by_due_date(items)
    return items unless params.due_date?

    if params.filter_by_no_due_date?
      items.without_due_date
    elsif params.filter_by_any_due_date?
      items.with_due_date
    elsif params.filter_by_overdue?
      items.due_before(Date.today)
    elsif params.filter_by_due_today?
      items.due_today
    elsif params.filter_by_due_tomorrow?
      items.due_tomorrow
    elsif params.filter_by_due_this_week?
      items.due_between(Date.today.beginning_of_week, Date.today.end_of_week)
    elsif params.filter_by_due_this_month?
      items.due_between(Date.today.beginning_of_month, Date.today.end_of_month)
    elsif params.filter_by_due_next_month_and_previous_two_weeks?
      items.due_between(Date.today - 2.weeks, (Date.today + 1.month).end_of_month)
    else
      items.none
    end
  end

  def by_issue_types(items)
    issue_type_params = Array(params[:issue_types]).map(&:to_s)
    return items if issue_type_params.blank?
    return klass.none unless (WorkItems::Type.base_types.keys & issue_type_params).sort == issue_type_params.sort

    items.with_issue_type(params[:issue_types])
  end

  def by_negated_issue_types(items)
    issue_type_params = Array(not_params[:issue_types]).map(&:to_s) & WorkItems::Type.base_types.keys
    return items if issue_type_params.blank?

    items.without_issue_type(issue_type_params)
  end
end

class Admin::ProjectsController < Admin::ApplicationController
  include MembersPresentation

  before_action :project, only: [:show, :transfer, :repository_check, :destroy, :edit, :update]
  before_action :group, only: [:show, :transfer]

  feature_category :groups_and_projects, [:index, :show, :transfer, :destroy, :edit, :update]
  feature_category :source_code_management, [:repository_check]

  def index
    params[:sort] ||= 'latest_activity_desc'
    @sort = params[:sort]

    params[:archived] = true if params[:last_repository_check_failed].present? && params[:archived].nil?

    @projects = Admin::ProjectsFinder.new(params: params, current_user: current_user).execute

    respond_to do |format|
      format.html
      format.json do
        render json: {
          html: view_to_html_string("admin/projects/_projects", projects: @projects)
        }
      end
    end
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def show
    if @group
      @group_members = present_members(
        @group.members.order("access_level DESC").page(params[:group_members_page]))
    end

    @project_members = present_members(
      @project.members.page(params[:project_members_page]))
    @requesters = present_members(
      AccessRequestsFinder.new(@project).execute(current_user))
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def destroy
    ::Projects::DestroyService.new(@project, current_user, {}).async_execute
    flash[:toast] = format(_("Project '%{project_name}' is being deleted."), project_name: @project.full_name)

    redirect_to admin_projects_path, status: :found
  rescue Projects::DestroyService::DestroyError => e
    redirect_to admin_projects_path, status: :found, alert: e.message
  end

  # rubocop: disable CodeReuse/ActiveRecord
  def transfer
    namespace = Namespace.find_by(id: params[:new_namespace_id])
    ::Projects::TransferService.new(@project, current_user, params.dup).execute(namespace)

    flash[:alert] = @project.errors[:new_namespace].first if @project.errors[:new_namespace].present?

    @project.reset
    redirect_to admin_project_path(@project)
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def edit; end

  def update
    result = ::Projects::UpdateService.new(@project, current_user, project_params).execute

    if result[:status] == :success
      unless Gitlab::Utils.to_boolean(project_params['runner_registration_enabled'])
        Ci::Runners::ResetRegistrationTokenService.new(@project, current_user).execute
      end

      redirect_to [:admin, @project], notice: format(_("Project '%{project_name}' was successfully updated."), project_name: @project.name)
    else
      render "edit"
    end
  end

  def repository_check
    RepositoryCheck::SingleRepositoryWorker.perform_async(@project.id) # rubocop:disable CodeReuse/Worker

    redirect_to(
      admin_project_path(@project),
      notice: _('Repository check was triggered.')
    )
  end

  protected

  def project
    @project = Project.find_by_full_path(
      [params[:namespace_id], '/', params[:id]].join('')
    )
    @project || render_404
  end

  def group
    @group ||= @project.group
  end

  def project_params
    params.require(:project).permit(allowed_project_params)
  end

  def allowed_project_params
    [
      :description,
      :name,
      :runner_registration_enabled
    ]
  end
end

class Projects::VariablesController < Projects::ApplicationController
  before_action :authorize_admin_build!, except: :update
  before_action :authorize_admin_cicd_variables!, only: :update

  feature_category :secrets_management

  urgency :low, [:show, :update]

  def show
    respond_to do |format|
      format.json do
        render status: :ok, json: { variables: ::Ci::VariableSerializer.new.represent(@project.variables) }
      end
    end
  end

  def update
    update_result = Ci::ChangeVariablesService.new(
      container: @project, current_user: current_user,
      params: variables_params
    ).execute

    if update_result
      respond_to do |format|
        format.json { render_variables }
      end
    else
      respond_to do |format|
        format.json { render_error }
      end
    end
  end

  private

  def render_variables
    render status: :ok, json: { variables: ::Ci::VariableSerializer.new.represent(@project.variables) }
  end

  def render_error
    render status: :bad_request, json: @project.errors.full_messages
  end

  def variables_params
    params.permit(variables_attributes: Array(variable_params_attributes))
  end

  def variable_params_attributes
    %i[id variable_type key description secret_value protected masked hidden raw environment_scope _destroy]
  end
end

class Admin::KeysController < Admin::ApplicationController
  before_action :user, only: [:show, :destroy]

  feature_category :user_management

  def show
    @key = user.keys.find(params[:id])

    respond_to do |format|
      format.html
      format.js { head :ok }
    end
  end

  def destroy
    key = user.keys.find(params[:id])

    respond_to do |format|
      if key.destroy
        format.html { redirect_to keys_admin_user_path(user), status: :found, notice: _('User key was successfully removed.') }
      else
        format.html { redirect_to keys_admin_user_path(user), status: :found, alert: _('Failed to remove user key.') }
      end
    end
  end

  protected

  # rubocop: disable CodeReuse/ActiveRecord
  def user
    @user ||= User.find_by!(username: params[:user_id])
  end
  # rubocop: enable CodeReuse/ActiveRecord

  def key_params
    params.require(:user_id, :id)
  end
end

class ProfilesController < ApplicationController
    include ActionView::Helpers::SanitizeHelper
    include Gitlab::Tracking

    before_action :user
    skip_before_action :require_email, only: [:show, :update]
    feature_category :user_profile, [:show, :update]

    urgency :low, [:show, :update]

    def show; end

    def update
      respond_to do |format|
        result = Users::UpdateService.new(current_user, user_params.merge(user: @user)).execute(check_password: true)

        if result[:status] == :success
          message = s_("Profiles|Profile was successfully updated")

          format.html { redirect_back_or_default(default: { action: 'show' }, options: { notice: message }) }
          format.json { render json: { message: message } }
        else
          format.html do
            redirect_back_or_default(default: { action: 'show' }, options: { alert: result[:message] })
          end
          format.json { render json: result }
        end
      end
    end

    private

    def user
      @user = current_user
    end

    def user_params_attributes
      [
        :achievements_enabled,
        :avatar,
        :bio,
        :bluesky,
        :commit_email,
        :discord,
        :email,
        :gitpod_enabled,
        :hide_no_password,
        :hide_no_ssh_key,
        :hide_project_limit,
        :include_private_contributions,
        :job_title,
        :linkedin,
        :location,
        :mastodon,
        :name,
        :organization,
        :private_profile,
        :pronouns,
        :pronunciation,
        :public_email,
        :role,
        :skype,
        :timezone,
        :twitter,
        :username,
        :validation_password,
        :website_url,
        { status: [:emoji, :message, :availability, :clear_status_after] }
      ]
    end

    def user_params
      @user_params ||= params.require(:user).permit(user_params_attributes)
    end
  end

class Admin::PlanLimitsController < Admin::ApplicationController
  include InternalRedirect

  before_action :set_plan_limits

  feature_category :not_owned # rubocop:todo Gitlab/AvoidFeatureCategoryNotOwned

  def create
    redirect_path = referer_path(request) || general_admin_application_settings_path

    respond_to do |format|
      if @plan_limits.update(plan_limits_params)
        format.json { head :ok }
        format.html { redirect_to redirect_path, notice: _('Application limits saved successfully') }
      else
        format.json { head :bad_request }
        format.html { render_update_error }
      end
    end
  end

  private

  def set_plan_limits
    @plan_limits = Plan.find(plan_limits_params[:plan_id]).actual_limits
  end

  def plan_limits_params
    params.require(:plan_limits)
      .permit(%i[
        plan_id
        conan_max_file_size
        helm_max_file_size
        maven_max_file_size
        npm_max_file_size
        nuget_max_file_size
        pypi_max_file_size
        terraform_module_max_file_size
        generic_packages_max_file_size
        ci_instance_level_variables
        ci_pipeline_size
        ci_active_jobs
        ci_project_subscriptions
        ci_pipeline_schedules
        ci_needs_size_limit
        ci_registered_group_runners
        ci_registered_project_runners
        dotenv_size
        dotenv_variables
        pipeline_hierarchy_size
      ])
  end
end


# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:role))
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
require 'net/http'

def foo

  url = params[:url]
  # ruleid: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get(url, "/index.html")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:url])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

  uri = URI(params[:server])
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP.get("example.com", "/index.html")

  uri = URI("example.com/index.html")
  # ok: ruby_http_rule-AvoidTaintedHTTPRequest
  Net::HTTP::Get.new(uri)

end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

def test_send_file
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file2
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file3
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file4
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file5
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.encrypted[:something]
end

def test_send_file6
    # this is reported since semgrep 0.94 because . ... . can now match
    # intermediate fields, not just method calls.
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

def test_send_file7
    # ok: ruby_file_rule-CheckSendFile
    send_file cookies.permanent.signed[:something]
end

def test_send_file8
    # ruleid: ruby_file_rule-CheckSendFile
    send_file request.env[:badheader]
end

def test_send_file_ok
    # ok: ruby_file_rule-CheckSendFile
    send_file "some_safe_file.txt"
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

require 'digest'
class Bad_md5
    def bad_md5_code()
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

        # ruleid: ruby_crypto_rule-WeakHashesMD5
        digest = OpenSSL::Digest::MD5.new
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    end
end
# License: Commons Clause License Condition v1.0[LGPL-2.1-only]

class Bad_attr_accessible
   include  ActiveModel::MassAssignmentSecurity

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
   # ruleid: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:banned, :name, :age)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
end

class Ok_attr_accessible
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   attr_accessible :name, :address, :age,
                   :telephone, as: :create_params
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   User.new(params.permit(:address, :acc, :age))
   # ok: ruby_mass_assignment_rule-ModelAttrAccessible
   params_with_conditional_require(ctrl.params).permit(:name, :address, :age)
end
public class ZioEntry implements Cloneable {

    private ZipInput zipInput;

    // public int signature = 0x02014b50;
    private short versionMadeBy;
    private short versionRequired;
    private short generalPurposeBits;
    private short compression;
    private short modificationTime;
    private short modificationDate;
    private int crc32;
    private int compressedSize;
    private int size;
    private String filename;
    private byte[] extraData;
    private short numAlignBytes = 0;
    private String fileComment;
    private short diskNumberStart;
    private short internalAttributes;
    private int externalAttributes;

    private int localHeaderOffset;
    private long dataPosition = -1;
    private byte[] data = null;
    private ZioEntryOutputStream entryOut = null;

    private static byte[] alignBytes = new byte[4];

    private static LoggerInterface log;

    public ZioEntry(ZipInput input) {
        zipInput = input;
    }

    public static LoggerInterface getLogger() {
        if (log == null) log = LoggerManager.getLogger(ZioEntry.class.getName());
        return log;
    }

    public ZioEntry(String name) {
        filename = name;
        fileComment = "";
        compression = 8;
        extraData = new byte[0];
        setTime(System.currentTimeMillis());
    }

    public ZioEntry(String name, String sourceDataFile)
            throws IOException {
        zipInput = new ZipInput(sourceDataFile);
        filename = name;
        fileComment = "";
        this.compression = 0;
        this.size = (int) zipInput.getFileLength();
        this.compressedSize = this.size;

        if (getLogger().isDebugEnabled())
            getLogger().debug(String.format(Locale.ENGLISH, "Computing CRC for %s, size=%d", sourceDataFile, size));

        // compute CRC
        CRC32 crc = new CRC32();

        byte[] buffer = new byte[8096];

        int numRead = 0;
        while (numRead != size) {
            int count = zipInput.read(buffer, 0, Math.min(buffer.length, (this.size - numRead)));
            if (count > 0) {
                crc.update(buffer, 0, count);
                numRead += count;
            }
        }

        this.crc32 = (int) crc.getValue();

        zipInput.seek(0);
        this.dataPosition = 0;
        extraData = new byte[0];
        setTime(new File(sourceDataFile).lastModified());
    }


    public ZioEntry(String name, String sourceDataFile, short compression, int crc32, int compressedSize, int size)
            throws IOException {
        zipInput = new ZipInput(sourceDataFile);
        filename = name;
        fileComment = "";
        this.compression = compression;
        this.crc32 = crc32;
        this.compressedSize = compressedSize;
        this.size = size;
        this.dataPosition = 0;
        extraData = new byte[0];
        setTime(new File(sourceDataFile).lastModified());
    }

    // Return a copy with a new name
    public ZioEntry getClonedEntry(String newName) {

        ZioEntry clone;
        try {
            clone = (ZioEntry) this.clone();
        } catch (CloneNotSupportedException e) {
            throw new IllegalStateException("clone() failed!");
        }
        clone.setName(newName);
        return clone;
    }

    public void readLocalHeader() throws IOException {
        ZipInput input = zipInput;
        int tmp;
        boolean debug = getLogger().isDebugEnabled();

        input.seek(localHeaderOffset);

        if (debug) {
            getLogger().debug(String.format("FILE POSITION: 0x%08x", input.getFilePointer()));
        }

        // 0 	4 	Local file header signature = 0x04034b50
        int signature = input.readInt();
        if (signature != 0x04034b50) {
            throw new IllegalStateException(String.format("Local header not found at pos=0x%08x, file=%s", input.getFilePointer(), filename));
        }

        // This method is usually called just before the data read, so
        // its only purpose currently is to position the file pointer
        // for the data read.  The entry's attributes might also have
        // been changed since the central dir entry was read (e.g.,
        // filename), so throw away the values here.

        int tmpInt;
        short tmpShort;

        // 4 	2 	Version needed to extract (minimum)
        /* versionRequired */
        tmpShort = input.readShort();
        if (debug) {
            log.debug(String.format("Version required: 0x%04x", tmpShort /*versionRequired*/));
        }

        // 6 	2 	General purpose bit flag
        /* generalPurposeBits */
        tmpShort = input.readShort();
        if (debug) {
            log.debug(String.format("General purpose bits: 0x%04x", tmpShort /* generalPurposeBits */));
        }

        // 8 	2 	Compression method
        /* compression */
        tmpShort = input.readShort();
        if (debug) log.debug(String.format("Compression: 0x%04x", tmpShort /* compression */));

        // 10 	2 	File last modification time
        /* modificationTime */
        tmpShort = input.readShort();
        if (debug) {
            log.debug(String.format("Modification time: 0x%04x", tmpShort /* modificationTime */));
        }

        // 12 	2 	File last modification date
        /* modificationDate */
        tmpShort = input.readShort();
        if (debug) {
            log.debug(String.format("Modification date: 0x%04x", tmpShort /* modificationDate */));
        }

        // 14 	4 	CRC-32
        /* crc32 */
        tmpInt = input.readInt();
        if (debug) log.debug(String.format("CRC-32: 0x%04x", tmpInt /*crc32*/));

        // 18 	4 	Compressed size
        /* compressedSize*/
        tmpInt = input.readInt();
        if (debug) log.debug(String.format("Compressed size: 0x%04x", tmpInt /*compressedSize*/));

        // 22 	4 	Uncompressed size
        /* size */
        tmpInt = input.readInt();
        if (debug) log.debug(String.format("Size: 0x%04x", tmpInt /*size*/));

        // 26 	2 	File name length (n)
        short fileNameLen = input.readShort();
        if (debug) log.debug(String.format("File name length: 0x%04x", fileNameLen));

        // 28 	2 	Extra field length (m)
        short extraLen = input.readShort();
        if (debug) log.debug(String.format("Extra length: 0x%04x", extraLen));

        // 30 	n 	File name
        String filename = input.readString(fileNameLen);
        if (debug) log.debug("Filename: " + filename);

        // Extra data
        byte[] extra = input.readBytes(extraLen);

        // Record the file position of this entry's data.
        dataPosition = input.getFilePointer();
        if (debug) log.debug(String.format("Data position: 0x%08x", dataPosition));
    }

    public void writeLocalEntry(ZipOutput output) throws IOException {
        if (data == null && dataPosition < 0 && zipInput != null) {
            readLocalHeader();
        }

        localHeaderOffset = (int) output.getFilePointer();

        boolean debug = getLogger().isDebugEnabled();

        if (debug) {
            getLogger().debug(String.format("Writing local header at 0x%08x - %s", localHeaderOffset, filename));
        }

        if (entryOut != null) {
            entryOut.close();
            size = entryOut.getSize();
            data = ((ByteArrayOutputStream) entryOut.getWrappedStream()).toByteArray();
            compressedSize = data.length;
            crc32 = entryOut.getCRC();
        }

        output.writeInt(0x04034b50);
        output.writeShort(versionRequired);
        output.writeShort(generalPurposeBits);
        output.writeShort(compression);
        output.writeShort(modificationTime);
        output.writeShort(modificationDate);
        output.writeInt(crc32);
        output.writeInt(compressedSize);
        output.writeInt(size);
        output.writeShort((short) filename.length());

        numAlignBytes = 0;

        // Zipalign if the file is uncompressed, i.e., "Stored", and file size is not zero.
        if (compression == 0) {

            long dataPos = output.getFilePointer() + // current position
                    2 +                                  // plus size of extra data length
                    filename.length() +                  // plus filename
                    extraData.length;                    // plus extra data

            short dataPosMod4 = (short) (dataPos % 4);

            if (dataPosMod4 > 0) {
                numAlignBytes = (short) (4 - dataPosMod4);
            }
        }

        // 28 	2 	Extra field length (m)
        output.writeShort((short) (extraData.length + numAlignBytes));

        // 30 	n 	File name
        output.writeString(filename);

        // Extra data
        output.writeBytes(extraData);

        // Zipalign bytes
        if (numAlignBytes > 0) {
            output.writeBytes(alignBytes, 0, numAlignBytes);
        }

        if (debug) {
            getLogger().debug(String.format(Locale.ENGLISH, "Data position 0x%08x", output.getFilePointer()));
        }
        if (data != null) {
            output.writeBytes(data);
            if (debug) {
                getLogger().debug(String.format(Locale.ENGLISH, "Wrote %d bytes", data.length));
            }
        } else {

            if (debug) getLogger().debug(String.format("Seeking to position 0x%08x", dataPosition));
            zipInput.seek(dataPosition);

            int bufferSize = Math.min(compressedSize, 8096);
            byte[] buffer = new byte[bufferSize];
            long totalCount = 0;

            while (totalCount != compressedSize) {
                int numRead = zipInput.in.read(buffer, 0, (int) Math.min(compressedSize - totalCount, bufferSize));
                if (numRead > 0) {
                    output.writeBytes(buffer, 0, numRead);
                    if (debug) {
                        getLogger().debug(String.format(Locale.ENGLISH, "Wrote %d bytes", numRead));
                    }
                    totalCount += numRead;
                } else
                    throw new IllegalStateException(String.format(Locale.ENGLISH, "EOF reached while copying %s with %d bytes left to go", filename, compressedSize - totalCount));
            }
        }
    }

    public static ZioEntry read(ZipInput input) throws IOException {

        // 0    4   Central directory header signature = 0x02014b50
        int signature = input.readInt();
        if (signature != 0x02014b50) {
            // back up to the signature
            input.seek(input.getFilePointer() - 4);
            return null;
        }

        ZioEntry entry = new ZioEntry(input);

        entry.doRead(input);
        return entry;
    }

    private void doRead(ZipInput input) throws IOException {

        boolean debug = getLogger().isDebugEnabled();

        // 4    2   Version needed to extract (minimum)
        versionMadeBy = input.readShort();
        if (debug) log.debug(String.format("Version made by: 0x%04x", versionMadeBy));

        // 4    2   Version required
        versionRequired = input.readShort();
        if (debug) log.debug(String.format("Version required: 0x%04x", versionRequired));

        // 6    2   General purpose bit flag
        generalPurposeBits = input.readShort();
        if (debug) log.debug(String.format("General purpose bits: 0x%04x", generalPurposeBits));
        // Bits 1, 2, 3, and 11 are allowed to be set (first bit is bit zero).  Any others are a problem.
        if ((generalPurposeBits & 0xF7F1) != 0x0000) {
            throw new IllegalStateException("Can't handle general purpose bits == " + String.format("0x%04x", generalPurposeBits));
        }

        // 8    2   Compression method
        compression = input.readShort();
        if (debug) log.debug(String.format("Compression: 0x%04x", compression));

        // 10   2   File last modification time
        modificationTime = input.readShort();
        if (debug) log.debug(String.format("Modification time: 0x%04x", modificationTime));

        // 12   2   File last modification date
        modificationDate = input.readShort();
        if (debug) log.debug(String.format("Modification date: 0x%04x", modificationDate));

        // 14   4   CRC-32
        crc32 = input.readInt();
        if (debug) log.debug(String.format("CRC-32: 0x%04x", crc32));

        // 18   4   Compressed size
        compressedSize = input.readInt();
        if (debug) log.debug(String.format("Compressed size: 0x%04x", compressedSize));

        // 22   4   Uncompressed size
        size = input.readInt();
        if (debug) log.debug(String.format("Size: 0x%04x", size));

        // 26   2   File name length (n)
        short fileNameLen = input.readShort();
        if (debug) log.debug(String.format("File name length: 0x%04x", fileNameLen));

        // 28   2   Extra field length (m)
        short extraLen = input.readShort();
        if (debug) log.debug(String.format("Extra length: 0x%04x", extraLen));

        short fileCommentLen = input.readShort();
        if (debug) log.debug(String.format("File comment length: 0x%04x", fileCommentLen));

        diskNumberStart = input.readShort();
        if (debug) log.debug(String.format("Disk number start: 0x%04x", diskNumberStart));

        internalAttributes = input.readShort();
        if (debug) log.debug(String.format("Internal attributes: 0x%04x", internalAttributes));

        externalAttributes = input.readInt();
        if (debug) log.debug(String.format("External attributes: 0x%08x", externalAttributes));

        localHeaderOffset = input.readInt();
        if (debug) log.debug(String.format("Local header offset: 0x%08x", localHeaderOffset));

        // 30   n   File name
        filename = input.readString(fileNameLen);
        if (debug) log.debug("Filename: " + filename);

        extraData = input.readBytes(extraLen);

        fileComment = input.readString(fileCommentLen);
        if (debug) log.debug("File comment: " + fileComment);

        generalPurposeBits = (short) (generalPurposeBits & 0x0800); // Don't write a data descriptor, preserve UTF-8 encoded filename bit

        // Don't write zero-length entries with compression.
        if (size == 0) {
            compressedSize = 0;
            compression = 0;
            crc32 = 0;
        }
    }

    /**
     * Returns the entry's data.
     */
    public byte[] getData() throws IOException {
        if (data != null) return data;

        byte[] tmpdata = new byte[size];

        InputStream din = getInputStream();
        int count = 0;

        while (count != size) {
            int numRead = din.read(tmpdata, count, size - count);
            if (numRead < 0)
                throw new IllegalStateException(String.format(Locale.ENGLISH, "Read failed, expecting %d bytes, got %d instead", size, count));
            count += numRead;
        }
        return tmpdata;
    }

    // Returns an input stream for reading the entry's data.
    public InputStream getInputStream() throws IOException {
        return getInputStream(null);
    }

    // Returns an input stream for reading the entry's data.
    public InputStream getInputStream(OutputStream monitorStream) throws IOException {

        if (entryOut != null) {
            entryOut.close();
            size = entryOut.getSize();
            data = ((ByteArrayOutputStream) entryOut.getWrappedStream()).toByteArray();
            compressedSize = data.length;
            crc32 = entryOut.getCRC();
            entryOut = null;
            InputStream rawis = new ByteArrayInputStream(data);
            if (compression == 0) return rawis;
            else {
                // Hacky, inflate using a sequence of input streams that returns 1 byte more than the actual length of the data.
                // This extra dummy byte is required by InflaterInputStream when the data doesn't have the header and crc fields (as it is in zip files).
                return new InflaterInputStream(new SequenceInputStream(rawis, new ByteArrayInputStream(new byte[1])), new Inflater(true));
            }
        }

        ZioEntryInputStream dataStream;
        dataStream = new ZioEntryInputStream(this);
        if (monitorStream != null) dataStream.setMonitorStream(monitorStream);
        if (compression != 0) {
            // Note: When using nowrap=true with Inflater it is also necessary to provide
            // an extra "dummy" byte as input. This is required by the ZLIB native library
            // in order to support certain optimizations.
            dataStream.setReturnDummyByte(true);
            return new InflaterInputStream(dataStream, new Inflater(true));
        } else return dataStream;
    }

    // Returns an output stream for writing an entry's data.
    public OutputStream getOutputStream() {
        entryOut = new ZioEntryOutputStream(compression, new ByteArrayOutputStream());
        return entryOut;
    }

    public void write(ZipOutput output) throws IOException {
        boolean debug = getLogger().isDebugEnabled();

        output.writeInt(0x02014b50);
        output.writeShort(versionMadeBy);
        output.writeShort(versionRequired);
        output.writeShort(generalPurposeBits);
        output.writeShort(compression);
        output.writeShort(modificationTime);
        output.writeShort(modificationDate);
        output.writeInt(crc32);
        output.writeInt(compressedSize);
        output.writeInt(size);
        output.writeShort((short) filename.length());
        output.writeShort((short) (extraData.length + numAlignBytes));
        output.writeShort((short) fileComment.length());
        output.writeShort(diskNumberStart);
        output.writeShort(internalAttributes);
        output.writeInt(externalAttributes);
        output.writeInt(localHeaderOffset);

        output.writeString(filename);
        output.writeBytes(extraData);
        if (numAlignBytes > 0) output.writeBytes(alignBytes, 0, numAlignBytes);
        output.writeString(fileComment);
    }

    /*
     * Returns timestamp in Java format
     */
    public long getTime() {
        int year = (int) (((modificationDate >> 9) & 0x007f) + 80);
        int month = (int) (((modificationDate >> 5) & 0x000f) - 1);
        int day = (int) (modificationDate & 0x001f);
        int hour = (int) ((modificationTime >> 11) & 0x001f);
        int minute = (int) ((modificationTime >> 5) & 0x003f);
        int seconds = (int) ((modificationTime << 1) & 0x003e);
        Date d = new Date(year, month, day, hour, minute, seconds);
        return d.getTime();
    }

    /*
     * Set the file timestamp (using a Java time value).
     */
    public void setTime(long time) {
        Date d = new Date(time);
        long dtime;
        int year = d.getYear() + 1900;
        if (year < 1980) {
            dtime = (1 << 21) | (1 << 16);
        } else {
            dtime = (year - 1980) << 25 | (d.getMonth() + 1) << 21 |
                    d.getDate() << 16 | d.getHours() << 11 | d.getMinutes() << 5 |
                    d.getSeconds() >> 1;
        }

        modificationDate = (short) (dtime >> 16);
        modificationTime = (short) (dtime & 0xFFFF);
    }

    public boolean isDirectory() {
        return filename.endsWith("/");
    }

    public String getName() {
        return filename;
    }

    public void setName(String filename) {
        this.filename = filename;
    }

    /**
     * Use 0 (STORED), or 8 (DEFLATE).
     */
    public void setCompression(int compression) {
        this.compression = (short) compression;
    }

    public short getVersionMadeBy() {
        return versionMadeBy;
    }

    public short getVersionRequired() {
        return versionRequired;
    }

    public short getGeneralPurposeBits() {
        return generalPurposeBits;
    }

    public short getCompression() {
        return compression;
    }

    public int getCrc32() {
        return crc32;
    }

    public int getCompressedSize() {
        return compressedSize;
    }

    public int getSize() {
        return size;
    }

    public byte[] getExtraData() {
        return extraData;
    }

    public String getFileComment() {
        return fileComment;
    }

    public short getDiskNumberStart() {
        return diskNumberStart;
    }

    public short getInternalAttributes() {
        return internalAttributes;
    }

    public int getExternalAttributes() {
        return externalAttributes;
    }

    public int getLocalHeaderOffset() {
        return localHeaderOffset;
    }

    public long getDataPosition() {
        return dataPosition;
    }

    public ZioEntryOutputStream getEntryOut() {
        return entryOut;
    }

    public ZipInput getZipInput() {
        return zipInput;
    }
}

public class InstalledAppsActivity extends AppCompatActivity {

    private FDroidDatabase db;
    private InstalledAppListAdapter adapter;
    private RecyclerView appList;
    private TextView emptyState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FDroidApp fdroidApp = (FDroidApp) getApplication();
        fdroidApp.setSecureWindow(this);

        fdroidApp.applyPureBlackBackgroundInDarkTheme(this);

        super.onCreate(savedInstanceState);

        setContentView(R.layout.installed_apps_layout);

        MaterialToolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        adapter = new InstalledAppListAdapter(this);

        appList = findViewById(R.id.app_list);
        appList.setHasFixedSize(true);
        appList.setLayoutManager(new LinearLayoutManager(this));
        appList.setAdapter(adapter);

        emptyState = findViewById(R.id.empty_state);

        db = DBHelper.getDb(this);
        db.getAppDao().getInstalledAppListItems(getPackageManager()).observe(this, this::onLoadFinished);
    }

    private void onLoadFinished(List<AppListItem> items) {
        adapter.setApps(items);

        if (adapter.getItemCount() == 0) {
            appList.setVisibility(View.GONE);
            emptyState.setVisibility(View.VISIBLE);
        } else {
            appList.setVisibility(View.VISIBLE);
            emptyState.setVisibility(View.GONE);
        }

        // load app prefs for each app off the UiThread and update item if updates are ignored
        AppPrefsDao appPrefsDao = db.getAppPrefsDao();
        for (AppListItem item : items) {
            Utils.observeOnce(appPrefsDao.getAppPrefs(item.getPackageName()), this, appPrefs -> {
                if (appPrefs.getIgnoreVersionCodeUpdate() > 0) adapter.updateItem(item, appPrefs);
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.installed_apps, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.menu_share) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("packageName,versionCode,versionName\n");
            for (int i = 0; i < adapter.getItemCount(); i++) {
                App app = adapter.getItem(i);
                if (app != null) {
                    stringBuilder.append(app.packageName).append(',')
                            .append(app.installedVersionCode).append(',')
                            .append(app.installedVersionName).append('\n');
                }
            }
            ShareCompat.IntentBuilder intentBuilder = new ShareCompat.IntentBuilder(this)
                    .setSubject(getString(R.string.send_installed_apps))
                    .setChooserTitle(R.string.send_installed_apps)
                    .setText(stringBuilder.toString())
                    .setType("text/csv");
            startActivity(intentBuilder.getIntent());
        }
        return super.onOptionsItemSelected(item);
    }
}

public final class LayoutManagerUtils {

    private static Method sCsdfp = null;

    static {
        try {
            sCsdfp = StaggeredGridLayoutManager.class.getDeclaredMethod("calculateScrollDirectionForPosition", int.class);
            sCsdfp.setAccessible(true);
        } catch (NoSuchMethodException e) {
            // Ignore
            e.printStackTrace();
        }
    }

    public static void scrollToPositionWithOffset(
            RecyclerView.LayoutManager layoutManager, int position, int offset) {
        if (layoutManager instanceof LinearLayoutManager) {
            ((LinearLayoutManager) layoutManager).scrollToPositionWithOffset(position, offset);
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            ((StaggeredGridLayoutManager) layoutManager).scrollToPositionWithOffset(position, offset);
        } else {
            throw new IllegalStateException("Can't do scrollToPositionWithOffset for " +
                    layoutManager.getClass().getName());
        }
    }

    public static void smoothScrollToPosition(
            RecyclerView.LayoutManager layoutManager, Context context, int position) {
        smoothScrollToPosition(layoutManager, context, position, -1);
    }

    public static void smoothScrollToPosition(
            RecyclerView.LayoutManager layoutManager, Context context, int position,
            int millisecondsPerInch) {
        SimpleSmoothScroller smoothScroller;
        if (layoutManager instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) layoutManager;
            smoothScroller = new SimpleSmoothScroller(context, millisecondsPerInch) {
                @Override
                public PointF computeScrollVectorForPosition(int targetPosition) {
                    return linearLayoutManager.computeScrollVectorForPosition(targetPosition);
                }
            };

        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            final StaggeredGridLayoutManager staggeredGridLayoutManager = (StaggeredGridLayoutManager) layoutManager;
            smoothScroller = new SimpleSmoothScroller(context, millisecondsPerInch) {
                @Override
                public PointF computeScrollVectorForPosition(int targetPosition) {
                    int direction = 0;
                    try {
                        direction = (Integer) sCsdfp.invoke(staggeredGridLayoutManager, targetPosition);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }

                    if (direction == 0) {
                        return null;
                    }
                    if (staggeredGridLayoutManager.getOrientation() == StaggeredGridLayoutManager.HORIZONTAL) {
                        return new PointF(direction, 0);
                    } else {
                        return new PointF(0, direction);
                    }
                }
            };

        } else {
            throw new IllegalStateException("Can't do smoothScrollToPosition for " +
                    layoutManager.getClass().getName());
        }
        smoothScroller.setTargetPosition(position);
        layoutManager.startSmoothScroll(smoothScroller);
    }

    public static void scrollToPositionProperly(final RecyclerView.LayoutManager layoutManager,
                                                final Context context, final int position, final OnScrollToPositionListener listener) {
        SimpleHandler.getInstance().postDelayed(() -> {
            int first = getFirstVisibleItemPosition(layoutManager);
            int last = getLastVisibleItemPosition(layoutManager);
            int offset = Math.abs(position - first);
            int max = last - first;
            if (offset < max && max > 0) {
                smoothScrollToPosition(layoutManager, context, position,
                        MathUtils.lerp(100, 25, (offset / max)));
            } else {
                scrollToPositionWithOffset(layoutManager, position, 0);
                if (listener != null) {
                    listener.onScrollToPosition(position);
                }
            }
        }, 200);
    }

    public static int getFirstVisibleItemPosition(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            int[] positions = ((StaggeredGridLayoutManager) layoutManager).findFirstVisibleItemPositions(null);
            return MathUtils.min(positions);
        } else {
            throw new IllegalStateException("Can't do getFirstVisibleItemPosition for " +
                    layoutManager.getClass().getName());
        }
    }

    public static int getLastVisibleItemPosition(RecyclerView.LayoutManager layoutManager) {
        if (layoutManager instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
        } else if (layoutManager instanceof StaggeredGridLayoutManager) {
            int[] positions = ((StaggeredGridLayoutManager) layoutManager).findLastVisibleItemPositions(null);
            return MathUtils.max(positions);
        } else {
            throw new IllegalStateException("Can't do getLastVisibleItemPosition for " +
                    layoutManager.getClass().getName());
        }
    }

    public interface OnScrollToPositionListener {
        void onScrollToPosition(int position);
    }
}


// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://semgrep.dev/r?q=java.lang.security.audit.xxe.documentbuilderfactory-external-parameter-entities-true.documentbuilderfactory-external-parameter-entities-true

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.parsers.SAXParser;
import org.jdom2.input.SAXBuilder;
import org.dom4j.io.SAXReader;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

class GoodDocumentBuilderFactory {
    public void GoodDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        dbf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadDocumentBuilderFactory {
    public void BadDocumentBuilderFactory1() throws ParserConfigurationException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXParserFactory {
    public void GoodSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        spf.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadSAXParserFactory {
    public void BadSAXParserFactory1() throws ParserConfigurationException {
        SAXParserFactory spf = SAXParserFactory.newInstance();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodSAXBuilder {
    public void GoodSAXBuilder1() throws ParserConfigurationException {
        SAXBuilder saxBuilder = new SAXBuilder();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        saxBuilder.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadSAXBuilder {
    public void BadSAXBuilder1() throws ParserConfigurationException {
        SAXBuilder saxBuilder = new SAXBuilder();
        // ruleid:java_xxe_rule-ExternalParameterEntitiesTrue
        saxBuilder.setFeature("http://xml.org/sax/features/external-parameter-entities", true);
    }
}

class GoodSAXReader {
    public void GoodSAXReader1() throws ParserConfigurationException {
        SAXReader saxReader = new SAXReader();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        saxReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadSAXReader {
    public void BadSAXReader1() throws ParserConfigurationException {
        SAXReader saxReader = new SAXReader();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class GoodXMLReader {
    public void GoodXMLReader1() throws SAXException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        SAXParser saxParser = factory.newSAXParser();
        // Obtain an XMLReader from the SAXParser
        XMLReader xmlReader = saxParser.getXMLReader();
        // ok:java_xxe_rule-ExternalParameterEntitiesTrue
        xmlReader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
    }
}

class BadXMLReader {
    public void BadXMLReader1() throws ParserConfigurationException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        factory.setNamespaceAware(true);
        SAXParser saxParser = factory.newSAXParser();
        // Obtain an XMLReader from the SAXParser
        XMLReader xmlReader = saxParser.getXMLReader();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package perm;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Set;

public class OverlyPermissiveFilePermissionInline {

  public void dangerInline(Path path) throws IOException {
    // ruleid: java_perm_rule-OverlyPermissiveFilePermissionInline
    Files.setPosixFilePermissions(path, PosixFilePermissions.fromString("rw-rw-rw-"));
  }

  public void dangerInline2(Path path) throws IOException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    Files.setPosixFilePermissions(path, perms);
  }

  public void okInline(Path path) throws IOException {
    Files.setPosixFilePermissions(path, PosixFilePermissions.fromString("rw-rw----"));
  }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package smtp;

import org.apache.commons.mail.Email;
import org.apache.commons.mail.SimpleEmail;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.HtmlEmail;
import org.apache.commons.mail.ImageHtmlEmail;

public class InsecureSmtp{

    public static void main(String[] args) throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        email.setHostName("smtp.googlemail.com");
        email.setSSLOnConnect(false); //OK

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        email2.setHostName("smtp2.googlemail.com");        
        email2.setSSLOnConnect(true); //BAD
        //email2.setSmtpPort(465);
        //email2.setAuthenticator(new DefaultAuthenticator("username", "password"));
        //email2.setFrom("user@gmail.com");
        //email2.setSubject("TestMail");
        //email2.setMsg("This is a test mail ... :-)");
        //email2.addTo("foo@bar.com");
        //email2.send();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        emailMulti.setHostName("mail.myserver.com");
        emailMulti.setSSLOnConnect(true); //BAD

        // ruleid: java_smtp_rule-InsecureSmtp
        HtmlEmail htmlEmail = new HtmlEmail();
        htmlEmail.setHostName("mail.myserver.com");
        htmlEmail.setSSLOnConnect(true); //BAD

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail.setHostName("mail.myserver.com");
        imageEmail.setSSLOnConnect(true);
        imageEmail.setSSLCheckServerIdentity(true); //OK
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail2.setHostName("mail2.myserver.com");
        imageEmail2.setSSLCheckServerIdentity(true); //OK - reversed order
        imageEmail2.setSSLOnConnect(true);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        imageEmail3.setHostName("mail3.myserver.com");
        imageEmail3.setSSLOnConnect(true); //BAD
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/problem-based-packs/insecure-transport/java-stdlib/tls-renegotiation.java

class Bad {
    public void bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void bad2() {
        // ruleid: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
    }

    public void bad3() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

class Ok {
    public void ok1() {
        // ok: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", false);
    }

    public void ok2() {
        // ok: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "false");
    }

    public void ok3() {
        // ok: java_crypto_rule-TLSUnsafeRenegotiation
        java.lang.System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", Boolean.FALSE);
    }
}

// License: LGPL-3.0 License (c) find-sec-bugs
package inject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.InputStreamReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.RandomAccessFile;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SpotbugsPathTraversalAbsolute extends HttpServlet {

    public String get_file(Model model, String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(input_file_name); // BAD, DETECTS: PT_RELATIVE_PATH_TRAVERSAL
        return "something";
    }

    @GetMapping("/somepath")
    public String get_image(Model model, @RequestParam String input) {
        String input_file_name = "somedir/"+input;

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        InputStream staticFile = getClass().getClassLoader().getResourceAsStream("static_file.xml"); // should not match

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        StringBuilder sb = new StringBuilder();
        // ... read file ...
        return "something";
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    protected void danger2(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input1 = req.getParameter("input1");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    protected void danger3(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException, URISyntaxException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile(input, "r"); // BAD, DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // false positive test
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new RandomAccessFile("safe", input);
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new FileWriter("safe".toUpperCase());
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        new File(new URI("safe"));

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // nio path traversal
    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void loadFile(HttpServletRequest req, HttpServletResponse resp) {
        String path = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar");
        // ok: java_inject_rule-SpotbugsPathTraversalAbsolute
        Paths.get("foo","bar", "allsafe");

    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void tempDir(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
        Path p = Paths.get("/");

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // DETECTS: PT_ABSOLUTE_PATH_TRAVERSAL
    public void writer(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String input = req.getParameter("test");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }


}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
// https://github.com/semgrep/semgrep-rules/blob/release/java/lang/security/insecure-jms-deserialization.java

import java.util.Date;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import org.apache.log4j.Logger;
import com.rands.couponproject.jpa.Income;

/**
 * Message-Driven Bean implementation class for: IncomeConsumerBean
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MyQueue")
})
class IncomeConsumerBean implements MessageListener {

    static Logger logger = Logger.getLogger(IncomeConsumerBean.class);

    @EJB
    IncomeServiceBean isb;

    public IncomeConsumerBean() {
        // your implementation
    }

    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.info("onMessage received a TextMessage at " + new Date());
                TextMessage msg = (TextMessage) message;
                logger.warn("onMessage ignoring TextMessage : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                logger.info("onMessage received an ObjectMessage at " + new Date());

                ObjectMessage msg = (ObjectMessage) message;

                // ruleid: java_deserialization_rule-InsecureJmsDeserialization
                Object o = msg.getObject(); // variant 1 : calling getObject method directly on an ObjectMessage object
                logger.info("o=" + o);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
                logger.info("Message is : " + income);

                isb.StoreIncome(income);
            } else {
                logger.error("onMessage received an invalid message type");
            }

        } catch (JMSException e) {
            logger.error("onMessage failed : " + e.toString());
        }
    }

}

/**
 * Safe implementation
 */
@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "java:/jms/queue/MyQueue")
})
class SafeIncomeConsumerBean implements MessageListener {

    static Logger logger = Logger.getLogger(IncomeConsumerBean.class);

    @EJB
    IncomeServiceBean isb;

    public SafeIncomeConsumerBean() {
        // your implementation
    }

    /**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
        try {
            if (message instanceof TextMessage) {
                logger.info("onMessage received a TextMessage at " + new Date());
                TextMessage msg = (TextMessage) message;
                logger.warn("onMessage ignoring TextMessage : " + msg.getText());
            } else if (message instanceof ObjectMessage) {
                logger.info("onMessage received an ObjectMessage at " + new Date());

                ObjectMessage msg = (ObjectMessage) message;

                // Use custom LookAheadObjectInputStream to safely deserialize the object
                InputStream is = msg.getObjectStream();
                LookAheadObjectInputStream laois = new LookAheadObjectInputStream(is);
                // ok: java_deserialization_rule-InsecureJmsDeserialization
                Object o = laois.readObject();
                logger.info("Deserialized object: " + o);

                // Ensure that the deserialized object is of type Income
                if (o instanceof Income) {
                    Income income = (Income) o;
                    logger.info("Message is : " + income);
                    isb.StoreIncome(income);
                } else {
                    logger.error("Received object is not of type Income");
                }

                isb.StoreIncome(income);
            } else {
                logger.error("onMessage received an invalid message type");
            }

        } catch (JMSException e) {
            logger.error("onMessage failed : " + e.toString());
        }
    }

}

class LookAheadObjectInputStream extends ObjectInputStream {
    public LookAheadObjectInputStream(InputStream inputStream) throws IOException {
        super(inputStream);
    }

    @Override
    protected Class<?> resolveClass(ObjectStreamClass desc) throws IOException, ClassNotFoundException {
        if (!desc.getName().equals(Income.class.getName())) {
            throw new InvalidClassException("Unauthorized deserialization attempt", desc.getName());
        }
        return super.resolveClass(desc);
    }
}
// License: Commons Clause License Condition v1.0[LGPL-2.1-only]
package com.gitlab.csrfservlet;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.util.matcher.RegexRequestMatcher;
import org.springframework.security.config.annotation.web.configurers.CsrfConfigurer;

@Configuration
@EnableWebSecurity
@Profile("csrf-configurer")
public class SpringCSRFDisabled extends WebSecurityConfigurerAdapter {

    //Starting from Spring Security 4.x, the CSRF protection is enabled by default.
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        CsrfConfigurer<HttpSecurity> csrf = http.csrf();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ruleid: java_csrf_rule-SpringCSRFDisabled
        http.csrf().requireCsrfProtectionMatcher(new RegexRequestMatcher("/csrf/submit", "POST")).disable();

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}
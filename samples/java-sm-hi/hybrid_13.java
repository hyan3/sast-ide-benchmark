public class EhApplication extends SceneApplication {

    public static final boolean BETA = false;
    private static final String TAG = EhApplication.class.getSimpleName();
    private static final String KEY_GLOBAL_STUFF_NEXT_ID = "global_stuff_next_id";
    private static final boolean DEBUG_CONACO = false;
    private static final boolean DEBUG_PRINT_NATIVE_MEMORY = false;
    private static final boolean DEBUG_PRINT_IMAGE_COUNT = false;
    private static final long DEBUG_PRINT_INTERVAL = 3000L;

    private static EhApplication instance;

    private final IntIdGenerator mIdGenerator = new IntIdGenerator();
    private final HashMap<Integer, Object> mGlobalStuffMap = new HashMap<>();
    private final List<Activity> mActivityList = new ArrayList<>();
    private EhCookieStore mEhCookieStore;
    private EhClient mEhClient;
    private EhProxySelector mEhProxySelector;
    private OkHttpClient mOkHttpClient;
    private Cache mOkHttpCache;
    private ImageBitmapHelper mImageBitmapHelper;
    private Conaco<ImageBitmap> mConaco;
    private LruCache<Long, GalleryDetail> mGalleryDetailCache;
    private SimpleDiskCache mSpiderInfoCache;
    private DownloadManager mDownloadManager;
    private Hosts mHosts;
    private FavouriteStatusRouter mFavouriteStatusRouter;
    private boolean initialized = false;

    public static EhApplication getInstance() {
        return instance;
    }

    public static EhCookieStore getEhCookieStore(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mEhCookieStore == null) {
            application.mEhCookieStore = new EhCookieStore(context);
        }
        return application.mEhCookieStore;
    }

    @NonNull
    public static EhClient getEhClient(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mEhClient == null) {
            application.mEhClient = new EhClient(application);
        }
        return application.mEhClient;
    }

    @NonNull
    public static EhProxySelector getEhProxySelector(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mEhProxySelector == null) {
            application.mEhProxySelector = new EhProxySelector();
        }
        return application.mEhProxySelector;
    }

    @NonNull
    public static OkHttpClient getOkHttpClient(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mOkHttpClient == null) {
            OkHttpClient.Builder builder = new OkHttpClient.Builder()
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .readTimeout(5, TimeUnit.SECONDS)
                    .writeTimeout(5, TimeUnit.SECONDS)
                    .callTimeout(10, TimeUnit.SECONDS)
                    .cookieJar(getEhCookieStore(application))
                    .cache(getOkHttpCache(application))
                    .dns(new EhDns(application))
                    .addNetworkInterceptor(chain -> {
                        try {
                            return chain.proceed(chain.request());
                        } catch (NullPointerException e) {
                            // crash on meizu devices due to old Android version
                            // https://github.com/square/okhttp/issues/3301#issuecomment-348415095
                            throw new IOException(e.getMessage());
                        }
                    })
                    .proxySelector(getEhProxySelector(application));
            try {
                TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(
                        TrustManagerFactory.getDefaultAlgorithm());
                trustManagerFactory.init((KeyStore) null);
                TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
                if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                    throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
                }
                X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
                builder.sslSocketFactory(new EhSSLSocketFactory(), trustManager);
            } catch (Exception e) {
                e.printStackTrace();
                builder.sslSocketFactory(new EhSSLSocketFactory(), new EhX509TrustManager());
            }
            application.mOkHttpClient = builder.build();
        }
        return application.mOkHttpClient;
    }

    @NonNull
    public static Cache getOkHttpCache(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mOkHttpCache == null) {
            application.mOkHttpCache = new Cache(new File(application.getCacheDir(), "http_cache"), 50L * 1024L * 1024L);
        }
        return application.mOkHttpCache;
    }

    @NonNull
    public static ImageBitmapHelper getImageBitmapHelper(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mImageBitmapHelper == null) {
            application.mImageBitmapHelper = new ImageBitmapHelper();
        }
        return application.mImageBitmapHelper;
    }

    private static int getMemoryCacheMaxSize() {
        return Math.min(20 * 1024 * 1024, (int) OSUtils.getAppMaxMemory());
    }

    @NonNull
    public static Conaco<ImageBitmap> getConaco(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mConaco == null) {
            Conaco.Builder<ImageBitmap> builder = new Conaco.Builder<>();
            builder.hasMemoryCache = true;
            builder.memoryCacheMaxSize = getMemoryCacheMaxSize();
            builder.hasDiskCache = true;
            builder.diskCacheDir = new File(context.getCacheDir(), "thumb");
            builder.diskCacheMaxSize = 320 * 1024 * 1024; // 320MB
            builder.okHttpClient = getOkHttpClient(context);
            builder.objectHelper = getImageBitmapHelper(context);
            builder.debug = DEBUG_CONACO;
            application.mConaco = builder.build();
        }
        return application.mConaco;
    }

    @NonNull
    public static LruCache<Long, GalleryDetail> getGalleryDetailCache(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mGalleryDetailCache == null) {
            // Max size 25, 3 min timeout
            application.mGalleryDetailCache = new LruCache<>(25);
            getFavouriteStatusRouter().addListener((gid, slot) -> {
                GalleryDetail gd = application.mGalleryDetailCache.get(gid);
                if (gd != null) {
                    gd.favoriteSlot = slot;
                }
            });
        }
        return application.mGalleryDetailCache;
    }

    @NonNull
    public static SimpleDiskCache getSpiderInfoCache(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (null == application.mSpiderInfoCache) {
            application.mSpiderInfoCache = new SimpleDiskCache(
                    new File(context.getCacheDir(), "spider_info"), 20 * 1024 * 1024); // 20M
        }
        return application.mSpiderInfoCache;
    }

    @NonNull
    public static DownloadManager getDownloadManager() {
        return getDownloadManager(instance);
    }

    @NonNull
    public static DownloadManager getDownloadManager(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mDownloadManager == null) {
            application.mDownloadManager = new DownloadManager(application);
        }
        return application.mDownloadManager;
    }

    @NonNull
    public static Hosts getHosts(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mHosts == null) {
            application.mHosts = new Hosts(application, "hosts.db");
        }
        return application.mHosts;
    }

    @NonNull
    public static FavouriteStatusRouter getFavouriteStatusRouter() {
        return getFavouriteStatusRouter(getInstance());
    }

    @NonNull
    public static FavouriteStatusRouter getFavouriteStatusRouter(@NonNull Context context) {
        EhApplication application = ((EhApplication) context.getApplicationContext());
        if (application.mFavouriteStatusRouter == null) {
            application.mFavouriteStatusRouter = new FavouriteStatusRouter();
        }
        return application.mFavouriteStatusRouter;
    }

    @NonNull
    public static String getDeveloperEmail() {
        return "nekoworkshop$protonmail.com".replace('$', '@');
    }

    @SuppressLint("StaticFieldLeak")
    @Override
    public void onCreate() {
        instance = this;

        Thread.UncaughtExceptionHandler handler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler((t, e) -> {
            try {
                // Always save crash file if onCreate() is not done
                if (!initialized || Settings.getSaveCrashLog()) {
                    Crash.saveCrashLog(instance, e);
                }
            } catch (Throwable ignored) {
            }

            if (handler != null) {
                handler.uncaughtException(t, e);
            }
        });

        super.onCreate();

        Native.initialize();
        ClipboardUtil.initialize(this);
        GetText.initialize(this);
        StatusCodeException.initialize(this);
        Settings.initialize(this);
        ReadableTime.initialize(this);
        Html.initialize(this);
        AppConfig.initialize(this);
        SpiderDen.initialize(this);
        EhDB.initialize(this);
        EhEngine.initialize();
        BitmapUtils.initialize(this);

        if (EhDB.needMerge()) {
            EhDB.mergeOldDB(this);
        }

        Analytics.start(this);

        LocaleDelegate.setDefaultLocale(Settings.getLocale());
        DayNightDelegate.setApplicationContext(this);
        DayNightDelegate.setDefaultNightMode(Settings.getTheme());

        // Do io tasks in new thread
        //noinspection deprecation
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                // Check no media file
                try {
                    UniFile downloadLocation = Settings.getDownloadLocation();
                    if (Settings.getMediaScan()) {
                        CommonOperations.removeNoMediaFile(downloadLocation);
                    } else {
                        CommonOperations.ensureNoMediaFile(downloadLocation);
                    }
                } catch (Throwable t) {
                    t.printStackTrace();
                    ExceptionUtils.throwIfFatal(t);
                }

                // Clear temp files
                try {
                    clearTempDir();
                } catch (Throwable t) {
                    t.printStackTrace();
                    ExceptionUtils.throwIfFatal(t);
                }

                return null;
            }
        }.executeOnExecutor(IoThreadPoolExecutor.getInstance());

        // Check app update
        update();

        // Update version code
        try {
            PackageInfo pi = getPackageManager().getPackageInfo(getPackageName(), 0);
            Settings.putVersionCode(pi.versionCode);
        } catch (PackageManager.NameNotFoundException e) {
            // Ignore
        }

        mIdGenerator.setNextId(Settings.getInt(KEY_GLOBAL_STUFF_NEXT_ID, 0));

        if (DEBUG_PRINT_NATIVE_MEMORY || DEBUG_PRINT_IMAGE_COUNT) {
            debugPrint();
        }

        initialized = true;
        theDawnOfNewDay();
    }

    private void theDawnOfNewDay() {
        if (!Settings.getRequestNews()) {
            return;
        }
        EhCookieStore store = getEhCookieStore(this);
        HttpUrl eh = HttpUrl.get(EhUrl.HOST_E);

        if (store.contains(eh, EhCookieStore.KEY_IPD_MEMBER_ID) || store.contains(eh, EhCookieStore.KEY_IPD_PASS_HASH)) {
            IoThreadPoolExecutor.getInstance().execute(() -> {
                String referer = EhUrl.REFERER_E;
                Request request = new EhRequestBuilder(EhUrl.HOST_E + "news.php", referer).build();
                Call call = getOkHttpClient(this).newCall(request);
                try {
                    Response response = call.execute();
                    ResponseBody responseBody = response.body();
                    if (responseBody == null) {
                        return;
                    }
                    String body = responseBody.string();
                    String html = EventPaneParser.parse(body);
                    if (html != null) {
                        showEventPane(html);
                    }
                } catch (Throwable e) {
                    e.printStackTrace();
                }
            });
        }
    }

    public void showEventPane(String html) {
        Activity activity = getTopActivity();
        if (activity != null) {
            activity.runOnUiThread(() -> {
                AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setMessage(Html.fromHtml(html))
                        .setPositiveButton(android.R.string.ok, null)
                        .create();
                dialog.setOnShowListener(d -> {
                    final View messageView = dialog.findViewById(android.R.id.message);
                    if (messageView instanceof TextView) {
                        ((TextView) messageView).setMovementMethod(LinkMovementMethod.getInstance());
                    }
                });
                try {
                    dialog.show();
                } catch (Throwable t) {
                    // ignore
                }
            });
        }
    }

    private void clearTempDir() {
        File dir = AppConfig.getTempDir();
        if (null != dir) {
            FileUtils.deleteContent(dir);
        }
        dir = AppConfig.getExternalTempDir();
        if (null != dir) {
            FileUtils.deleteContent(dir);
        }
    }

    private void update() {
        int version = Settings.getVersionCode();
        if (version < 52) {
            Settings.putGuideGallery(true);
        }
    }

    public void clearMemoryCache() {
        if (null != mConaco) {
            mConaco.getBeerBelly().clearMemory();
        }
        if (null != mGalleryDetailCache) {
            mGalleryDetailCache.evictAll();
        }
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);

        if (level >= ComponentCallbacks2.TRIM_MEMORY_RUNNING_LOW) {
            clearMemoryCache();
        }
    }

    private void debugPrint() {
        new Runnable() {
            @Override
            public void run() {
                if (DEBUG_PRINT_NATIVE_MEMORY) {
                    Log.i(TAG, "Native memory: " + FileUtils.humanReadableByteCount(
                            Debug.getNativeHeapAllocatedSize(), false));
                }
                if (DEBUG_PRINT_IMAGE_COUNT) {
                    Log.i(TAG, "Image count: " + Image.getImageCount());
                }
                SimpleHandler.getInstance().postDelayed(this, DEBUG_PRINT_INTERVAL);
            }
        }.run();
    }

    public int putGlobalStuff(@NonNull Object o) {
        int id = mIdGenerator.nextId();
        mGlobalStuffMap.put(id, o);
        Settings.putInt(KEY_GLOBAL_STUFF_NEXT_ID, mIdGenerator.nextId());
        return id;
    }

    public boolean containGlobalStuff(int id) {
        return mGlobalStuffMap.containsKey(id);
    }

    public Object getGlobalStuff(int id) {
        return mGlobalStuffMap.get(id);
    }

    public Object removeGlobalStuff(int id) {
        return mGlobalStuffMap.remove(id);
    }

    public void removeGlobalStuff(Object o) {
        mGlobalStuffMap.values().removeAll(Collections.singleton(o));
    }

    public void registerActivity(Activity activity) {
        mActivityList.add(activity);
    }

    public void unregisterActivity(Activity activity) {
        mActivityList.remove(activity);
    }

    @Nullable
    public Activity getTopActivity() {
        if (!mActivityList.isEmpty()) {
            return mActivityList.get(mActivityList.size() - 1);
        } else {
            return null;
        }
    }

    // Avoid crash on some "energy saving" devices
    @Override
    public ComponentName startService(Intent service) {
        try {
            return super.startService(service);
        } catch (Throwable t) {
            t.printStackTrace();
            ExceptionUtils.throwIfFatal(t);
            return null;
        }
    }

    // Avoid crash on some "energy saving" devices
    @Override
    public boolean bindService(Intent service, ServiceConnection conn, int flags) {
        try {
            return super.bindService(service, conn, flags);
        } catch (Throwable t) {
            t.printStackTrace();
            ExceptionUtils.throwIfFatal(t);
            return false;
        }
    }

    // Avoid crash on some "energy saving" devices
    @Override
    public void unbindService(ServiceConnection conn) {
        try {
            super.unbindService(conn);
        } catch (Throwable t) {
            t.printStackTrace();
            ExceptionUtils.throwIfFatal(t);
        }
    }

    // Avoid crash on some "energy saving" devices
    @Override
    public ComponentName startForegroundService(Intent service) {
        try {
            return super.startForegroundService(service);
        } catch (Throwable t) {
            t.printStackTrace();
            ExceptionUtils.throwIfFatal(t);
            return null;
        }
    }
}

@SuppressWarnings("LineLength")
public class WifiStateChangeService extends Worker {
    private static final String TAG = "WifiStateChangeService";

    public static final String BROADCAST = "org.fdroid.fdroid.action.WIFI_CHANGE";
    public static final String EXTRA_STATUS = "wifiStateChangeStatus";

    private WifiManager wifiManager;
    private static WifiInfoThread wifiInfoThread;
    private static int previousWifiState = Integer.MIN_VALUE;
    private volatile static int wifiState;
    private static final int NETWORK_INFO_STATE_NOT_SET = -1;

    public WifiStateChangeService(@NonNull Context context, @NonNull WorkerParameters workerParams) {
        super(context, workerParams);
    }

    public static void registerReceiver(Context context) {
        ContextCompat.registerReceiver(
                context,
                new WifiStateChangeReceiver(),
                new IntentFilter(WifiManager.NETWORK_STATE_CHANGED_ACTION),
                ContextCompat.RECEIVER_NOT_EXPORTED);
    }

    public static void start(Context context, @Nullable Intent intent) {
        int networkInfoStateInt = NETWORK_INFO_STATE_NOT_SET;
        if (intent != null) {
            NetworkInfo ni = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            networkInfoStateInt = ni.getState().ordinal();
        }

        WorkRequest workRequest = new OneTimeWorkRequest.Builder(WifiStateChangeService.class)
                .setConstraints(new Constraints.Builder()
                        .setRequiredNetworkType(NetworkType.CONNECTED)
                        .build())
                .setInputData(new Data.Builder()
                        .putInt(WifiManager.EXTRA_NETWORK_INFO, networkInfoStateInt)
                        .build()
                )
                .build();
        WorkManager.getInstance(context).enqueue(workRequest);
    }

    @NonNull
    @Override
    public Result doWork() {
        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
        int networkInfoStateInt = getInputData().getInt(WifiManager.EXTRA_NETWORK_INFO, NETWORK_INFO_STATE_NOT_SET);
        NetworkInfo.State networkInfoState = null;
        if (networkInfoStateInt != NETWORK_INFO_STATE_NOT_SET) {
            networkInfoState = NetworkInfo.State.values()[networkInfoStateInt];
        }
        Utils.debugLog(TAG, "WiFi change service started.");
        wifiManager = ContextCompat.getSystemService(getApplicationContext(), WifiManager.class);
        if (wifiManager == null) {
            return Result.failure();
        }
        wifiState = wifiManager.getWifiState();
        Utils.debugLog(TAG, "networkInfoStateInt == " + networkInfoStateInt
                + "  wifiState == " + printWifiState(wifiState));
        if (networkInfoState == null
                || networkInfoState == NetworkInfo.State.CONNECTED
                || networkInfoState == NetworkInfo.State.DISCONNECTED) {
            if (previousWifiState != wifiState &&
                    (wifiState == WifiManager.WIFI_STATE_ENABLED
                            || wifiState == WifiManager.WIFI_STATE_DISABLING  // might be switching to hotspot
                            || wifiState == WifiManager.WIFI_STATE_DISABLED   // might be hotspot
                            || wifiState == WifiManager.WIFI_STATE_UNKNOWN)) { // might be hotspot
                if (wifiInfoThread != null) {
                    wifiInfoThread.interrupt();
                }
                wifiInfoThread = new WifiInfoThread();
                wifiInfoThread.start();
            }
        }
        return Result.success();
    }

    public class WifiInfoThread extends Thread {

        @Override
        public void run() {
            android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_LOWEST);
            try {
                FDroidApp.initWifiSettings();
                Utils.debugLog(TAG, "Checking wifi state (in background thread).");
                WifiInfo wifiInfo = null;

                int wifiState = wifiManager.getWifiState();
                int retryCount = 0;
                while (FDroidApp.ipAddressString == null) {
                    if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                        return;
                    }
                    if (wifiState == WifiManager.WIFI_STATE_ENABLED) {
                        wifiInfo = wifiManager.getConnectionInfo();
                        FDroidApp.ipAddressString = formatIpAddress(wifiInfo.getIpAddress());
                        setSsid(wifiInfo);
                        DhcpInfo dhcpInfo = wifiManager.getDhcpInfo();
                        if (dhcpInfo != null) {
                            String netmask = formatIpAddress(dhcpInfo.netmask);
                            if (!TextUtils.isEmpty(FDroidApp.ipAddressString) && netmask != null) {
                                try {
                                    FDroidApp.subnetInfo = new SubnetUtils(FDroidApp.ipAddressString, netmask).getInfo();
                                } catch (IllegalArgumentException e) {
                                    // catch mystery: "java.lang.IllegalArgumentException: Could not parse [null/24]"
                                    e.printStackTrace();
                                }
                            }
                        }
                        if (FDroidApp.ipAddressString == null
                                || FDroidApp.subnetInfo == FDroidApp.UNSET_SUBNET_INFO) {
                            setIpInfoFromNetworkInterface();
                        }
                    } else if (wifiState == WifiManager.WIFI_STATE_DISABLED
                            || wifiState == WifiManager.WIFI_STATE_DISABLING
                            || wifiState == WifiManager.WIFI_STATE_UNKNOWN) {
                        // try once to see if its a hotspot
                        setIpInfoFromNetworkInterface();
                        if (FDroidApp.ipAddressString == null) {
                            return;
                        }
                    }

                    if (retryCount > 120) {
                        return;
                    }
                    retryCount++;

                    if (FDroidApp.ipAddressString == null) {
                        Thread.sleep(1000);
                        Utils.debugLog(TAG, "waiting for an IP address...");
                    }
                }
                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                setSsid(wifiInfo);

                String scheme;
                if (Preferences.get().isLocalRepoHttpsEnabled()) {
                    scheme = "https";
                } else {
                    scheme = "http";
                }
                Context context = WifiStateChangeService.this.getApplicationContext();
                String address = String.format(Locale.ENGLISH, "%s://%s:%d/fdroid/repo",
                        scheme, FDroidApp.ipAddressString, FDroidApp.port);
                // the fingerprint for the local repo's signing key
                LocalRepoKeyStore localRepoKeyStore = LocalRepoKeyStore.get(context);
                Certificate localCert = localRepoKeyStore.getCertificate();
                String cert = localCert == null ?
                        null : Hasher.hex(localCert).toLowerCase(Locale.US);
                Repository repo = FDroidApp.createSwapRepo(address, cert);

                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                LocalRepoManager lrm = LocalRepoManager.get(context);
                lrm.writeIndexPage(Utils.getSharingUri(FDroidApp.repo).toString());

                if (isInterrupted()) { // can be canceled by a change via WifiStateChangeReceiver
                    return;
                }

                FDroidApp.repo = repo;

                /*
                 * Once the IP address is known we need to generate a self
                 * signed certificate to use for HTTPS that has a CN field set
                 * to the ipAddressString. This must be run in the background
                 * because if this is the first time the singleton is run, it
                 * can take a while to instantiate.
                 */
                if (Preferences.get().isLocalRepoHttpsEnabled()) {
                    localRepoKeyStore.setupHTTPSCertificate();
                }

            } catch (LocalRepoKeyStore.InitException e) {
                Log.e(TAG, "Unable to configure a fingerprint or HTTPS for the local repo", e);
            } catch (InterruptedException e) {
                Utils.debugLog(TAG, "interrupted");
                return;
            }
            Intent intent = new Intent(BROADCAST);
            intent.putExtra(EXTRA_STATUS, wifiState);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(intent);
        }
    }

    private void setSsid(WifiInfo wifiInfo) {
        Context context = getApplicationContext();
        if (wifiInfo != null && wifiInfo.getBSSID() != null) {
            String ssid = wifiInfo.getSSID();
            Utils.debugLog(TAG, "Have wifi info, connected to " + ssid);
            if (ssid == null) {
                FDroidApp.ssid = context.getString(R.string.swap_blank_wifi_ssid);
            } else {
                FDroidApp.ssid = ssid.replaceAll("^\"(.*)\"$", "$1");
            }
            FDroidApp.bssid = wifiInfo.getBSSID();
        } else {
            WifiApControl wifiApControl = WifiApControl.getInstance(context);
            Utils.debugLog(TAG, "WifiApControl: " + wifiApControl);
            if (wifiApControl == null && FDroidApp.ipAddressString != null) {
                wifiInfo = wifiManager.getConnectionInfo();
                if (wifiInfo != null && wifiInfo.getBSSID() != null) {
                    setSsid(wifiInfo);
                } else {
                    FDroidApp.ssid = context.getString(R.string.swap_active_hotspot, "");
                }
            } else if (wifiApControl != null && wifiApControl.isEnabled()) {
                WifiConfiguration wifiConfiguration = wifiApControl.getConfiguration();
                Utils.debugLog(TAG, "WifiConfiguration: " + wifiConfiguration);
                if (wifiConfiguration == null) {
                    FDroidApp.ssid = context.getString(R.string.swap_active_hotspot, "");
                    FDroidApp.bssid = "";
                    return;
                }

                if (wifiConfiguration.hiddenSSID) {
                    FDroidApp.ssid = context.getString(R.string.swap_hidden_wifi_ssid);
                } else {
                    FDroidApp.ssid = wifiConfiguration.SSID;
                }
                FDroidApp.bssid = wifiConfiguration.BSSID;
            }
        }
    }

    /**
     * Search for known Wi-Fi, Hotspot, and local network interfaces and get
     * the IP Address info from it.  This is necessary because network
     * interfaces in Hotspot/AP mode do not show up in the regular
     * {@link WifiManager} queries, and also on
     * {@link android.os.Build.VERSION_CODES#LOLLIPOP Android 5.0} and newer,
     * {@link WifiManager#getDhcpInfo()} returns an invalid netmask.
     *
     * @see <a href="https://issuetracker.google.com/issues/37015180">netmask of WifiManager.getDhcpInfo() is always zero on Android 5.0</a>
     */
    private void setIpInfoFromNetworkInterface() {
        try {
            Enumeration<NetworkInterface> networkInterfaces = NetworkInterface.getNetworkInterfaces();
            if (networkInterfaces == null) {
                return;
            }
            while (networkInterfaces.hasMoreElements()) {
                NetworkInterface netIf = networkInterfaces.nextElement();

                for (Enumeration<InetAddress> inetAddresses = netIf.getInetAddresses(); inetAddresses.hasMoreElements(); ) {
                    InetAddress inetAddress = inetAddresses.nextElement();
                    if (inetAddress.isLoopbackAddress() || inetAddress instanceof Inet6Address) {
                        continue;
                    }
                    if (netIf.getDisplayName().contains("wlan0")
                            || netIf.getDisplayName().contains("eth0")
                            || netIf.getDisplayName().contains("ap0")) {
                        FDroidApp.ipAddressString = inetAddress.getHostAddress();
                        for (InterfaceAddress address : netIf.getInterfaceAddresses()) {
                            short networkPrefixLength = address.getNetworkPrefixLength();
                            if (networkPrefixLength > 32) {
                                // something is giving a "/64" netmask, IPv6?
                                // java.lang.IllegalArgumentException: Value [64] not in range [0,32]
                                continue;
                            }
                            try {
                                String cidr = String.format(Locale.ENGLISH, "%s/%d",
                                        FDroidApp.ipAddressString, networkPrefixLength);
                                FDroidApp.subnetInfo = new SubnetUtils(cidr).getInfo();
                                break;
                            } catch (IllegalArgumentException e) {
                                if (BuildConfig.DEBUG) {
                                    e.printStackTrace();
                                } else {
                                    Log.i(TAG, "Getting subnet failed: " + e.getLocalizedMessage());
                                }
                            }
                        }
                    }
                }
            }
        } catch (NullPointerException | SocketException e) {
            // NetworkInterface.getNetworkInterfaces() can throw a NullPointerException internally
            Log.e(TAG, "Could not get ip address", e);
        }
    }

    static String formatIpAddress(int ipAddress) {
        if (ipAddress == 0) {
            return null;
        }
        return String.format(Locale.ENGLISH, "%d.%d.%d.%d",
                ipAddress & 0xff,
                ipAddress >> 8 & 0xff,
                ipAddress >> 16 & 0xff,
                ipAddress >> 24 & 0xff);
    }

    private String printWifiState(int wifiState) {
        switch (wifiState) {
            case WifiManager.WIFI_STATE_DISABLED:
                return "WIFI_STATE_DISABLED";
            case WifiManager.WIFI_STATE_DISABLING:
                return "WIFI_STATE_DISABLING";
            case WifiManager.WIFI_STATE_ENABLING:
                return "WIFI_STATE_ENABLING";
            case WifiManager.WIFI_STATE_ENABLED:
                return "WIFI_STATE_ENABLED";
            case WifiManager.WIFI_STATE_UNKNOWN:
                return "WIFI_STATE_UNKNOWN";
            case Integer.MIN_VALUE:
                return "previous value unset";
            default:
                return "~not mapped~";
        }
    }
}


// License: LGPL-3.0 License (c) find-sec-bugs
package inject;
import org.springframework.ldap.core.DefaultNameClassPairMapper;
import org.springframework.ldap.core.DirContextProcessor;
import org.springframework.ldap.core.LdapEntryIdentificationContextMapper;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.CountNameClassPairCallbackHandler;
import org.springframework.ldap.core.support.DefaultIncrementalAttributesMapper;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import java.util.Hashtable;
import java.util.Properties;

// ref: java_inject_rule-LDAPInjection
public class LDAPInjection {
    private final static String ldapURI = "ldaps://ldap.server.com/dc=ldap,dc=server,dc=com";
    private final static String contextFactory = "com.sun.jndi.ldap.LdapCtxFactory";

    /***************** JNDI LDAP **********************/

    static boolean authenticate(String username, String password) {
        try {
            Properties props = new Properties();
            props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
            props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
            props.put(Context.REFERRAL, "ignore");
            props.put(Context.SECURITY_PRINCIPAL, dnFromUser(username));
            props.put(Context.SECURITY_CREDENTIALS, password);

            new InitialDirContext(props);
            return true;
        } catch (NamingException e) {
            return false;
        }

    }

    private static String dnFromUser(String username) throws NamingException {
        Properties props = new Properties();
        props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
        props.put(Context.PROVIDER_URL, "ldap://ldap.example.com");
        props.put(Context.REFERRAL, "ignore");

        InitialDirContext context = new InitialDirContext(props);

        SearchControls ctrls = new SearchControls();
        ctrls.setReturningAttributes(new String[]{"givenName", "sn"});
        ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        SearchResult result = answers.next();

        return result.getNameInNamespace();
    }

    private static DirContext ldapContext(Hashtable<String, String> env) throws Exception {
        env.put(Context.INITIAL_CONTEXT_FACTORY, contextFactory);
        env.put(Context.PROVIDER_URL, ldapURI);
        env.put(Context.SECURITY_AUTHENTICATION, "none");
        DirContext ctx = new InitialDirContext(env);
        return ctx;
    }

    public static boolean testBind(String dn, String password) throws Exception {
        Hashtable<String, String> env = new Hashtable<String, String>();
        env.put(Context.SECURITY_AUTHENTICATION, "simple"); //false positive
        env.put(Context.SECURITY_PRINCIPAL, dn);
        env.put(Context.SECURITY_CREDENTIALS, password);

        try {
            ldapContext(env);
        } catch (javax.naming.AuthenticationException e) {
            return false;
        }
        return true;
    }

    /***************** SPRING LDAP **********************/

    public void queryVulnerableToInjection(LdapTemplate template, String jndiInjectMe, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ruleid: java_inject_rule-LDAPInjection
        template.lookup(jndiInjectMe, mapper);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    public void safeQuery(LdapTemplate template, SearchControls searchControls, DirContextProcessor dirContextProcessor) throws NamingException {
        String safeQuery = "uid=test";
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery);
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new DefaultNameClassPairMapper());
        // ok: java_inject_rule-LDAPInjection
        template.list(safeQuery, new CountNameClassPairCallbackHandler());

        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery);
        DefaultIncrementalAttributesMapper mapper = new DefaultIncrementalAttributesMapper("");
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.lookup(safeQuery, new LdapEntryIdentificationContextMapper());

        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper, dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new CountNameClassPairCallbackHandler(), dirContextProcessor);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, true, new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new CountNameClassPairCallbackHandler());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", mapper);
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new String[0], new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", SearchControls.OBJECT_SCOPE, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, new LdapEntryIdentificationContextMapper());
        // ok: java_inject_rule-LDAPInjection
        template.search(safeQuery, "dn=1", searchControls, mapper);
    }


    /***************** JNDI LDAP SPECIAL **********************/


    public static void main(String param) throws NamingException {
        DirContext ctx = null;
        String base = "ou=users,ou=system";
        SearchControls sc = new SearchControls();
        sc.setSearchScope(SearchControls.SUBTREE_SCOPE);
        String filter = "(&(objectclass=person))(|(uid="+param+")(street={0}))";
        Object[] filters = new Object[]{"The streetz 4 Ms bar"};
        System.out.println("Filter " + filter);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package xss;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.Component;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Radio;
//import org.apache.wicket.extensions.markup.html.form.select.SelectOptions.SimpleSelectOption;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.Check;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.Radio;
import org.apache.wicket.markup.html.form.RadioGroup;
import org.apache.wicket.markup.html.form.CheckBoxMultipleChoice;
import org.apache.wicket.model.util.ListModel;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;
import org.apache.wicket.markup.html.form.Form;

class WicketXSS extends WebPage {
    public void XssWicketExamplePage(PageParameters pageParameters) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    // http://www.java2s.com/example/java-src/pkg/org/jabylon/rest/ui/wicket/components/customfeedbackpanel-edea7.html
    protected Component newMessageDisplayComponent(String id, String message) {
        MultiLineLabel label = new MultiLineLabel(id, message);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        return label;
    }

    public void setText(String txt, Component c) {

        if (txt == null || txt.trim().length() == 0) {
            c.setVisible(false);
        } else {
            c.setDefaultModelObject(txt);
            c.setVisible(true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        }
    }

    protected void newLabel(Label lab) {
        final Component label = lab;
        // ruleid: java_xss_rule-WicketXSS
        label.setEscapeModelStrings(false);

        // https://github.com/apache/wicket/blob/e9461b0d115a7dbf4992596823521f6e038817d9/wicket-user-guide/src/main/asciidoc/forms2/forms2_3.adoc#L134
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

    }

    public void advancedSearchPage(String input) {

        Form<?> form = new Form<>("form");
        // Options for the dropdown
        List<String> options = Arrays.asList("Option 1 " + input, "Option 2 " + input, "Option 3 " + input);

        // Model for the selected value
        Model<String> selectedOption = new Model<>("Option 1 " + input);

        // Create a DropDownChoice and add it to the form
        DropDownChoice<String> dropdown = new DropDownChoice<>("dropdown", selectedOption, options);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        form.add(dropdown);

        add(form);

    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Radio/setEscapeModelStrings
    private String getFormComponentValue(final Radio<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }

    // https://www.tabnine.com/code/java/methods/org.apache.wicket.markup.html.form.Check/setEscapeModelStrings
    private String getFormComponentValue2(final Check<?> formComponent) {
        boolean oldEscape = formComponent.getEscapeModelStrings();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        String val = formComponent.getValue();
        formComponent.setEscapeModelStrings(oldEscape);
        return val;
    }
}

class MyPages extends WebPage {

    private ModalWindow modalWindow;

    public MyPages() {
        // Create a ModalWindow and set its title
        modalWindow = new ModalWindow("modalWindow");
        modalWindow.setTitle("Potentially Unsafe Title");

        // Explicitly disable escaping of the model strings (title)
        // for CVE-2015-5347/
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        add(modalWindow);
    }
}

class MyPage extends WebPage {

    public MyPage(List<String> userGeneratedOptions, String input) {

        Form<Void> form = new Form<>("form");

        Model<String> selectedOption = Model.of("");

        RadioGroup<String> radioGroup = new RadioGroup<>("radioGroup", selectedOption);
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        for (String option : userGeneratedOptions) {
            radioGroup.add(new Radio<>("radio" + option, Model.of(option)));
        }

        form.add(radioGroup);

        // https://nightlies.apache.org/wicket/apidocs/8.x/org/apache/wicket/markup/html/form/CheckBoxMultipleChoice.html
        List SITES = Arrays.asList(new String[] { "The Server Side", "Java Lobby", input });
        // for CVE-2015-7520
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }
}

// License: Commons Clause License Condition v1.0[LGPL-2.1-only]

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;

class XMLStreamRdr {

    public void parseXMLSafe1(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe2(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe3(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe4(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe5(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", false);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe6(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", Boolean.FALSE);
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLSafe7(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        // ok: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe1(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe2(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty("javax.xml.stream.isSupportingExternalEntities", true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe3(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.SUPPORT_DTD, true);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        while (reader.hasNext()) {
            reader.next();
        }
    }

    public void parseXMLUnsafe4(InputStream input) throws XMLStreamException {

        XMLInputFactory factory = XMLInputFactory.newFactory();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, true);
        // ruleid: java_xxe_rule-XMLStreamRdr
        XMLStreamReader reader = factory.createXMLStreamReader(input);
        while (reader.hasNext()) {
            reader.next();
        }
    }

}

// License: The GitLab Enterprise Edition (EE) license (the “EE License”)
package deserialization;

import java.io.File;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.fasterxml.jackson.databind.ObjectMapper.DefaultTypeResolverBuilder;
import com.fasterxml.jackson.databind.json.JsonMapper;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes;

class App {
    public static void main(String[] args) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();

        ExecuteVulns executeVulns = new ExecuteVulns(objectMapper);

        executeVulns.enableDefaultTyping();
        executeVulns.defaultTypeResolverBuilder();
        executeVulns.defaultTypeResolverBuilder2();

        executeVulns.safeTypingWithUnsafeBaseAllowed();
        executeVulns.safeTypingWithLaissezFaireSubTypeValidator();
        executeVulns.blockUnsafeBaseType();
        executeVulns.blockUnsafeBaseType2();

    }

}

class ExecuteVulns {

    ObjectMapper objectMapper;

    ExecuteVulns(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    void enableDefaultTyping() throws Exception {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./car_jdbc.json";
        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // No Annotations
    void defaultTypeResolverBuilder() throws Exception {

        DefaultTypeResolverBuilder resolverBuilder = new DefaultTypeResolverBuilder(
                ObjectMapper.DefaultTyping.NON_FINAL);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        resolverBuilder.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        resolverBuilder.typeProperty("_type");

        objectMapper.setDefaultTyping(resolverBuilder);

    }

    void defaultTypeResolverBuilder2() throws Exception {

        DefaultTypeResolverBuilder rb = new DefaultTypeResolverBuilder(ObjectMapper.DefaultTyping.NON_FINAL);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        rb.inclusion(JsonTypeInfo.As.WRAPPER_ARRAY);
        rb.typeProperty("_type");

        objectMapper.setDefaultTyping(rb);

    }

    void safeTypingWithUnsafeBaseAllowed() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./mygadgetload.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void safeTypingWithLaissezFaireSubTypeValidator() throws Exception {
        // Will Also need higher Jackson dependency version (10+)
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String exploit_JSON = "./mygadgetload_array.json";

        // From JSON to class
        Car newCar = objectMapper.readValue(new File(exploit_JSON), Car.class);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    // denies resolution of all subtypes of java.lang.Object
    void blockUnsafeBaseType() throws Exception {

        objectMapper.enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES);

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        objectMapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = objectMapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        objectMapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }

    void blockUnsafeBaseType2() throws Exception {

        ObjectMapper mapper = JsonMapper.builder()
                .enable(MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES)
                .build();

        // ok: java_deserialization_rule-JacksonUnsafeDeserialization
        mapper.enableDefaultTyping();

        String exploit_JSON = "./car_jdbc.json";

        // From JSON to class
        CarWithCLASSAnnotations newCar = mapper.readValue(new File(exploit_JSON), CarWithCLASSAnnotations.class);

        mapper.writeValue(new File("car_w.json"), newCar);

        System.out.print("Engine type: " + newCar.engine.getClass().getName());
        System.out.print("\n");
        System.out.print("secEngine type: " + newCar.secEngine.getClass().getName());
    }
}

class Car {

    public Object engine;
    public Object secEngine;
    public String color;
    public String model;

    public Car() {
    }

}

class CarWithCLASSAnnotations {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    public String color;
    public String model;

    public CarWithCLASSAnnotations() {
    }

}

class CarWithMinimalCLASSAnnotations {

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    // ruleid: java_deserialization_rule-JacksonUnsafeDeserialization
    @JsonTypeInfo(use = JsonTypeInfo.Id.MINIMAL_CLASS)
    public Object secEngine;
    public String color;
    public String model;

    public CarWithMinimalCLASSAnnotations() {
    }

}

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.WRAPPER_ARRAY, property = "@class")
@JsonSubTypes({
        @JsonSubTypes.Type(value = ElectricEngine.class, name = "ElectricEngine"),
        @JsonSubTypes.Type(value = FuelEngine.class, name = "FuelEngine")
})
abstract class Engine {
    public int engineModel;
    public int cc;
}

class ElectricEngine extends Engine {
    public int maxEnergy;

    public ElectricEngine() {
    }
}

class FuelEngine extends Engine {
    public int fuel;

    public FuelEngine() {
    }
}

// License: MIT (c) GitLab Inc.

package crypto;

class Cipher {

    javax.crypto.Cipher cipher;

    public void config1() throws Exception {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
        // ruleid: java_crypto_rule-CipherECBMode
        cipher = javax.crypto.Cipher.getInstance("DESede/ECB/PKCS5Padding");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        // ok: java_crypto_rule-CipherECBMode
        cipher = javax.crypto.Cipher.getInstance("AES/GCM/PKCS1Padding");
    }

}

// License: LGPL-3.0 License (c) find-sec-bugs
package crypto;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import java.io.IOException;

// ref: java_crypto_rule-WeakTLSProtocol-DefaultHttpClient
public class WeakTLSProtocolDefaultHttpClient {
    public void danger() throws IOException {
        // ruleid: java_crypto_rule-WeakTLSProtocol-DefaultHttpClient
        HttpClient client = new DefaultHttpClient(); 
        HttpUriRequest request = new HttpGet("https://test.com");
        client.execute(request);
    }
}

// License: MIT (c) GitLab Inc.
package cookie;

import org.apache.commons.text.StringEscapeUtils;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;

public class RequestParamToHeader extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramNames = req.getParameterNames().nextElement();
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String paramValues = req.getParameterValues("input")[0];
        //ruleid: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", paramValues);

        String paramMap = req.getParameterMap().get("input")[0];
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String input = req.getParameter("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String header = req.getHeader("input");
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String contextPath = req.getPathInfo();
        Cookie cPath = new Cookie("name", null);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

        String tainted = req.getParameter("input");
        HttpServletResponseWrapper wrapper = new HttpServletResponseWrapper(resp);
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String data = req.getParameter("input");
        String input = data.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", input);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", input);

        String header = req.getHeader("input");
        header = header.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", header);
        //ok: java_cookie_rule-RequestParamToHeader
        resp.setHeader("CUSTOM_HEADER", header);

        String contextPath = req.getPathInfo();
        contextPath = contextPath.replaceAll("[\r\n]+", "");
        //ok: java_cookie_rule-RequestParamToHeader
        resp.addHeader("CUSTOM_HEADER", contextPath);

    }

}

func TestRunnerByName(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerName    string
		expectedIndex int
		expectedError error
	}{
		"finds runner by name": {
			runners: []*common.RunnerConfig{
				{
					Name: "runner1",
				},
				{
					Name: "runner2",
				},
			},
			runnerName:    "runner2",
			expectedIndex: 1,
		},
		"does not find non-existent runner": {
			runners: []*common.RunnerConfig{
				{
					Name: "runner1",
				},
				{
					Name: "runner2",
				},
			},
			runnerName:    "runner3",
			expectedIndex: -1,
			expectedError: fmt.Errorf("could not find a runner with the name 'runner3'"),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByName(tt.runnerName)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestRunnerByURLAndID(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerURL     string
		runnerID      int64
		expectedIndex int
		expectedError error
	}{
		"finds runner by name": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      1,
			expectedIndex: 0,
		},
		"does not find runner with wrong ID": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      3,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab1.example.com/" and ID 3`),
		},
		"does not find runner with wrong URL": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab2.example.com/",
			runnerID:      1,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab2.example.com/" and ID 1`),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByURLAndID(tt.runnerURL, tt.runnerID)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestBuildsHelperCollect(t *testing.T) {
	dir := t.TempDir()

	ch := make(chan prometheus.Metric, 50)
	b := newBuildsHelper()

	longRunningBuild, err := common.GetLongRunningBuild()
	require.NoError(t, err)

	shell := "bash"
	if runtime.GOOS == "windows" {
		shell = "powershell"
	}

	build := &common.Build{
		JobResponse: longRunningBuild,
		Runner: &common.RunnerConfig{
			RunnerSettings: common.RunnerSettings{
				BuildsDir: dir,
				Executor:  "shell",
				Shell:     shell,
			},
			SystemIDState: common.NewSystemIDState(),
		},
	}
	trace := &common.Trace{Writer: io.Discard}

	require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

	done := make(chan error)
	go func() {
		done <- buildtest.RunBuildWithTrace(t, build, trace)
	}()

	b.builds = append(b.builds, build)
	// collect many logs whilst the build is being executed to trigger any
	// potential race conditions that arise from the build progressing whilst
	// metrics are collected.
	for i := 0; i < 200; i++ {
		if i == 100 {
			// Build might have not started yet, wait until cancel is
			// successful.
			require.Eventually(
				t,
				func() bool {
					return trace.Abort()
				},
				time.Minute,
				10*time.Millisecond,
			)
		}
		b.Collect(ch)
		<-ch
	}

	err = <-done
	expected := &common.BuildError{FailureReason: common.JobCanceled}
	assert.ErrorIs(t, err, expected)
}

func defaultBuild(cacheConfig *common.CacheConfig) *common.Build {
	return &common.Build{
		JobResponse: common.JobResponse{
			JobInfo: common.JobInfo{
				ProjectID: 10,
			},
			RunnerInfo: common.RunnerInfo{
				Timeout: 3600,
			},
		},
		Runner: &common.RunnerConfig{
			RunnerCredentials: common.RunnerCredentials{
				Token: "longtoken",
			},
			RunnerSettings: common.RunnerSettings{
				Cache: cacheConfig,
			},
		},
	}
}

func TestAdapterOperation(t *testing.T) {
	tests := map[string]adapterOperationTestCase{
		"error-on-URL-signing": {
			returnedURL:   "",
			returnedError: fmt.Errorf("test error"),
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while generating GCS pre-signed URL: test error")
			},
			signBlobAPITest: false,
		},
		"invalid-URL-returned": {
			returnedURL:   "://test",
			returnedError: nil,
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while parsing generated URL: parse")
				assert.Contains(t, message, "://test")
				assert.Contains(t, message, "missing protocol scheme")
			},
			signBlobAPITest: false,
		},
		"valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    false,
		},
		"sign-blob-api-valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    true,
		},
		"max-cache-archive-size": {
			returnedURL:            "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:          nil,
			assertErrorMessage:     nil,
			signBlobAPITest:        false,
			maxUploadedArchiveSize: maxUploadedArchiveSize,
			expectedHeaders:        http.Header{"X-Goog-Content-Length-Range": []string{"0,100"}},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			config := defaultGCSCache()

			config.MaxUploadedArchiveSize = tc.maxUploadedArchiveSize

			a, err := New(config, defaultTimeout, objectName)
			require.NoError(t, err)

			adapter, ok := a.(*gcsAdapter)
			require.True(t, ok, "Adapter should be properly casted to *adapter type")

			testAdapterOperation(
				t,
				tc,
				"GetDownloadURL",
				http.MethodGet,
				"",
				adapter,
				a.GetDownloadURL,
			)
			testAdapterOperation(
				t,
				tc,
				"GetUploadURL",
				http.MethodPut,
				"application/octet-stream",
				adapter,
				a.GetUploadURL,
			)

			headers := adapter.GetUploadHeaders()
			assert.Equal(t, headers, tc.expectedHeaders)

			assert.Nil(t, adapter.GetGoCloudURL(context.Background()))
			env, err := adapter.GetUploadEnv(context.Background())
			assert.NoError(t, err)
			assert.Empty(t, env)
		})
	}
}

func TestGlobbedFilePath(t *testing.T) {
	// Set up directories used in all test cases
	const (
		fileArchiverGlobPath  = "foo/bar/baz"
		fileArchiverGlobPath2 = "foo/bar/baz2"
	)
	err := os.MkdirAll(fileArchiverGlobPath, 0700)
	require.NoError(t, err, "Creating directory path: %s", fileArchiverGlobPath)
	defer os.RemoveAll(strings.Split(fileArchiverGlobPath, "/")[0])

	err = os.MkdirAll(fileArchiverGlobPath2, 0700)
	require.NoError(t, err, "Creating directory path: %s", fileArchiverGlobPath2)
	defer os.RemoveAll(strings.Split(fileArchiverGlobPath2, "/")[0])

	// Write a dir that is outside any glob patterns
	const (
		fileArchiverGlobNonMatchingPath = "bar/foo"
	)
	err = os.MkdirAll(fileArchiverGlobNonMatchingPath, 0700)
	writeTestFile(t, "bar/foo/test.txt")
	require.NoError(t, err, "Creating directory path: %s", fileArchiverGlobNonMatchingPath)
	defer os.RemoveAll(strings.Split(fileArchiverGlobNonMatchingPath, "/")[0])

	workingDirectory, err := os.Getwd()
	require.NoError(t, err)

	testCases := map[string]struct {
		paths   []string
		exclude []string

		// files that will be created and matched by the patterns
		expectedMatchingFiles []string

		// directories that will be matched by the patterns
		expectedMatchingDirs []string

		// files that will be created but will not be matched
		nonMatchingFiles []string

		// directories that will not be matched by the patterns
		nonMatchingDirs []string

		// files that are excluded by Exclude patterns
		excludedFilesCount int64

		warningLog string
	}{
		"find nothing with empty path": {
			paths: []string{""},
			nonMatchingFiles: []string{
				"file.txt",
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz/file.extra.dots.txt",
			},
			warningLog: "No matching files. Path is empty.",
		},
		"files by extension at several depths": {
			paths: []string{"foo/**/*.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz/file.extra.dots.txt",
			},
			nonMatchingFiles: []string{
				"foo/file.txt.md",
				"foo/bar/file.txt.md",
				"foo/bar/baz/file.txt.md",
				"foo/bar/baz/file.extra.dots.txt.md",
			},
		},
		"files by extension at several depths - with exclude": {
			paths:   []string{"foo/**/*.txt"},
			exclude: []string{"foo/**/xfile.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/xfile.txt",
				"foo/bar/xfile.txt",
				"foo/bar/baz/xfile.txt",
			},
			excludedFilesCount: 3,
		},
		"double slash matches a single slash": {
			paths: []string{"foo//*.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
			},
		},
		"double slash matches a single slash - with exclude": {
			paths:   []string{"foo//*.txt"},
			exclude: []string{"foo//*2.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file2.txt",
				"foo/bar/file.txt",
			},
			excludedFilesCount: 1,
		},
		"absolute path to working directory": {
			paths: []string{filepath.Join(workingDirectory, "*.thing")},
			expectedMatchingFiles: []string{
				"file.thing",
			},
			nonMatchingFiles: []string{
				"foo/file.thing",
				"foo/bar/file.thing",
				"foo/bar/baz/file.thing",
			},
		},
		"absolute path to working directory - with exclude": {
			paths:   []string{filepath.Join(workingDirectory, "*.thing")},
			exclude: []string{filepath.Join(workingDirectory, "*2.thing")},
			expectedMatchingFiles: []string{
				"file.thing",
			},
			nonMatchingFiles: []string{
				"file2.thing",
			},
			excludedFilesCount: 1,
		},
		"absolute path to nested directory": {
			paths: []string{filepath.Join(workingDirectory, "foo/bar/*.bin")},
			expectedMatchingFiles: []string{
				"foo/bar/file.bin",
			},
			nonMatchingFiles: []string{
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
			},
		},
		"absolute path to nested directory - with exclude": {
			paths:   []string{filepath.Join(workingDirectory, "foo/bar/*.bin")},
			exclude: []string{filepath.Join(workingDirectory, "foo/bar/*2.bin")},
			expectedMatchingFiles: []string{
				"foo/bar/file.bin",
			},
			nonMatchingFiles: []string{
				"foo/bar/file2.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
			},
			excludedFilesCount: 1,
		},
		"double slash and multiple stars - must be at least two dirs deep": {
			paths: []string{"./foo/**//*/*.*"},
			expectedMatchingFiles: []string{
				"foo/bar/file.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.bin",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file.txt",
			},
		},
		"double slash and multiple stars - must be at least two dirs deep - with exclude": {
			paths:   []string{"./foo/**//*/*.*"},
			exclude: []string{"**/*.bin"},
			expectedMatchingFiles: []string{
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.bin",
				"foo/bar/baz/file.bin",
				"foo/bar/baz2/file.bin",
			},
			excludedFilesCount: 3,
		},
		"all the files": {
			paths: []string{"foo/**/*.*"},
			expectedMatchingFiles: []string{
				"foo/file.bin",
				"foo/file.txt",
				"foo/bar/file.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.bin",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{},
		},
		"all the files - with exclude": {
			paths:   []string{"foo/**/*.*"},
			exclude: []string{"**/*.bin", "**/*even-this*"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/wow-even-this.go",
				"foo/file.bin",
				"foo/bar/file.bin",
				"foo/bar/baz/file.bin",
				"foo/bar/baz2/file.bin",
			},
			excludedFilesCount: 5,
		},
		"all the things - dirs included": {
			paths: []string{"foo/**"},
			expectedMatchingFiles: []string{
				"foo/file.bin",
				"foo/file.txt",
				"foo/bar/file.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.bin",
				"foo/bar/baz2/file.txt",
			},
			expectedMatchingDirs: []string{
				"foo",
				"foo/bar",
				"foo/bar/baz",
				"foo/bar/baz2",
			},
			nonMatchingFiles: []string{
				"root.txt",
			},
		},
		"relative path that leaves project and returns": {
			paths: []string{filepath.Join("..", filepath.Base(workingDirectory), "foo/*.txt")},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
		},
		"relative path that leaves project and returns - with exclude": {
			paths:   []string{filepath.Join("..", filepath.Base(workingDirectory), "foo/*.txt")},
			exclude: []string{filepath.Join("..", filepath.Base(workingDirectory), "foo/*2.txt")},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file2.txt",
			},
			excludedFilesCount: 1,
		},
		"invalid path": {
			paths:      []string{">/**"},
			warningLog: "no matching files. Ensure that the artifact path is relative to the working directory",
		},
		"cancel out everything": {
			paths:      []string{"**"},
			exclude:    []string{"**"},
			warningLog: "no matching files. Ensure that the artifact path is relative to the working directory",
		},
	}

	for testName, tc := range testCases {
		t.Run(testName, func(t *testing.T) {
			h := newLogHook(logrus.WarnLevel)
			logrus.AddHook(&h)

			for _, f := range tc.expectedMatchingFiles {
				writeTestFile(t, f)
			}
			for _, f := range tc.nonMatchingFiles {
				writeTestFile(t, f)
			}

			f := fileArchiver{
				Paths:   tc.paths,
				Exclude: tc.exclude,
			}
			err = f.enumerate()
			assert.NoError(t, err)

			sortedFiles := f.sortedFiles()
			assert.Len(t, sortedFiles, len(tc.expectedMatchingFiles)+len(tc.expectedMatchingDirs))
			for _, p := range tc.expectedMatchingFiles {
				assert.Contains(t, f.sortedFiles(), p)
			}
			for _, p := range tc.expectedMatchingDirs {
				assert.Contains(t, f.sortedFiles(), p)
			}

			var excludedFilesCount int64
			for _, v := range f.excluded {
				excludedFilesCount += v
			}
			if tc.excludedFilesCount > 0 {
				assert.Equal(t, tc.excludedFilesCount, excludedFilesCount)
			}

			if tc.warningLog != "" {
				require.Len(t, h.entries, 1)
				assert.Contains(t, h.entries[0].Message, tc.warningLog)
			}

			// remove test files from this test case
			// deferred removal will still happen if needed in the os.RemoveAll call above
			for _, f := range tc.expectedMatchingFiles {
				removeTestFile(t, f)
			}
			for _, f := range tc.nonMatchingFiles {
				removeTestFile(t, f)
			}
		})
	}
}

func TestBuildsHelperFindSessionByURL(t *testing.T) {
	sess, err := session.NewSession(nil)
	require.NoError(t, err)
	build := common.Build{
		Session: sess,
		Runner: &common.RunnerConfig{
			RunnerCredentials: common.RunnerCredentials{
				Token: "abcd1234",
			},
			SystemIDState: common.NewSystemIDState(),
		},
	}

	require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

	h := newBuildsHelper()
	h.addBuild(&build)

	foundSession := h.findSessionByURL(sess.Endpoint + "/action")
	assert.Equal(t, sess, foundSession)

	foundSession = h.findSessionByURL("/session/hash/action")
	assert.Nil(t, foundSession)
}

func TestRestrictHTTPMethods(t *testing.T) {
	tests := map[string]int{
		http.MethodGet:  http.StatusOK,
		http.MethodHead: http.StatusOK,
		http.MethodPost: http.StatusMethodNotAllowed,
		"FOOBAR":        http.StatusMethodNotAllowed,
	}

	for method, expectedStatusCode := range tests {
		t.Run(method, func(t *testing.T) {
			mux := http.NewServeMux()
			mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
				_, _ = w.Write([]byte("hello world"))
			})

			server := httptest.NewServer(restrictHTTPMethods(mux, http.MethodGet, http.MethodHead))

			req, err := http.NewRequest(method, server.URL, nil)
			require.NoError(t, err)

			resp, err := server.Client().Do(req)
			require.NoError(t, err)
			require.Equal(t, expectedStatusCode, resp.StatusCode)
		})
	}
}

func TestAdapterOperation(t *testing.T) {
	tests := map[string]adapterOperationTestCase{
		"error-on-URL-signing": {
			returnedURL:   "",
			returnedError: fmt.Errorf("test error"),
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while generating GCS pre-signed URL: test error")
			},
			signBlobAPITest: false,
		},
		"invalid-URL-returned": {
			returnedURL:   "://test",
			returnedError: nil,
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while parsing generated URL: parse")
				assert.Contains(t, message, "://test")
				assert.Contains(t, message, "missing protocol scheme")
			},
			signBlobAPITest: false,
		},
		"valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    false,
		},
		"sign-blob-api-valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    true,
		},
		"max-cache-archive-size": {
			returnedURL:            "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:          nil,
			assertErrorMessage:     nil,
			signBlobAPITest:        false,
			maxUploadedArchiveSize: maxUploadedArchiveSize,
			expectedHeaders:        http.Header{"X-Goog-Content-Length-Range": []string{"0,100"}},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			config := defaultGCSCache()

			config.MaxUploadedArchiveSize = tc.maxUploadedArchiveSize

			a, err := New(config, defaultTimeout, objectName)
			require.NoError(t, err)

			adapter, ok := a.(*gcsAdapter)
			require.True(t, ok, "Adapter should be properly casted to *adapter type")

			testAdapterOperation(
				t,
				tc,
				"GetDownloadURL",
				http.MethodGet,
				"",
				adapter,
				a.GetDownloadURL,
			)
			testAdapterOperation(
				t,
				tc,
				"GetUploadURL",
				http.MethodPut,
				"application/octet-stream",
				adapter,
				a.GetUploadURL,
			)

			headers := adapter.GetUploadHeaders()
			assert.Equal(t, headers, tc.expectedHeaders)

			assert.Nil(t, adapter.GetGoCloudURL(context.Background()))
			env, err := adapter.GetUploadEnv(context.Background())
			assert.NoError(t, err)
			assert.Empty(t, env)
		})
	}
}

func TestRunnerByURLAndID(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerURL     string
		runnerID      int64
		expectedIndex int
		expectedError error
	}{
		"finds runner by name": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      1,
			expectedIndex: 0,
		},
		"does not find runner with wrong ID": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      3,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab1.example.com/" and ID 3`),
		},
		"does not find runner with wrong URL": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab2.example.com/",
			runnerID:      1,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab2.example.com/" and ID 1`),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByURLAndID(tt.runnerURL, tt.runnerID)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestAskRunnerUsingRunnerTokenOverrideDefaults(t *testing.T) {
	const executor = "docker"

	basicValidation := func(s *commands.RegisterCommand) {
		assert.Equal(t, "http://gitlab.example.com/", s.URL)
		assert.Equal(t, "glrt-testtoken", s.Token)
		assert.Equal(t, executor, s.RunnerSettings.Executor)
	}
	expectedParamsFn := func(p common.RunnerCredentials) bool {
		return p.URL == "http://gitlab.example.com/" && p.Token == "glrt-testtoken"
	}

	tests := map[string]struct {
		answers        []string
		arguments      []string
		validate       func(s *commands.RegisterCommand)
		expectedParams func(common.RunnerCredentials) bool
	}{
		"basic answers": {
			answers: append([]string{
				"http://gitlab.example.com/",
				"glrt-testtoken",
				"name",
			}, executorAnswers(t, executor)...),
			validate:       basicValidation,
			expectedParams: expectedParamsFn,
		},
		"basic arguments, accepting provided": {
			answers: make([]string, 9),
			arguments: append(
				executorCmdLineArgs(t, executor),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				"--name", "name",
			),
			validate:       basicValidation,
			expectedParams: expectedParamsFn,
		},
		"basic arguments override": {
			answers: append(
				[]string{"http://gitlab.example2.com/", "glrt-testtoken2", "new-name", executor},
				executorOverrideAnswers(t, executor)...,
			),
			arguments: append(
				executorCmdLineArgs(t, executor),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				"--name", "name",
			),
			validate: func(s *commands.RegisterCommand) {
				assert.Equal(t, "http://gitlab.example2.com/", s.URL)
				assert.Equal(t, "glrt-testtoken2", s.Token)
				assert.Equal(t, "new-name", s.Name)
				assert.Equal(t, executor, s.RunnerSettings.Executor)
				require.NotNil(t, s.RunnerSettings.Docker)
				assert.Equal(t, "nginx:latest", s.RunnerSettings.Docker.Image)
			},
			expectedParams: func(p common.RunnerCredentials) bool {
				return p.URL == "http://gitlab.example2.com/" && p.Token == "glrt-testtoken2"
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			network.On("VerifyRunner", mock.MatchedBy(tc.expectedParams), mock.MatchedBy(isValidToken)).
				Return(&common.VerifyRunnerResponse{
					ID:    12345,
					Token: "glrt-testtoken",
				}).
				Once()

			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(tc.answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			hook := test.NewGlobal()
			args := append(tc.arguments, "--leave-runner")
			args, cleanTempFile := useTempConfigFile(t, args)
			defer cleanTempFile()
			err := app.Run(append([]string{"runner", "register"}, args...))
			output := commands.GetLogrusOutput(t, hook)

			assert.NoError(t, err)
			tc.validate(cmd)
			assert.Contains(t, output, "Runner registered successfully.")
		})
	}
}

func TestGenerateMetadataToFile(t *testing.T) {
	tmpDir := t.TempDir()
	tmpFile, err := os.CreateTemp(tmpDir, "")
	require.NoError(t, err)

	_, err = tmpFile.WriteString("testdata")
	require.NoError(t, err)
	require.NoError(t, tmpFile.Close())

	sha := sha256.New()
	sha.Write([]byte("testdata"))
	checksum := sha.Sum(nil)

	// First format the time to RFC3339 and then parse it to get the correct precision
	startedAtRFC3339 := time.Now().Format(time.RFC3339)
	startedAt, err := time.Parse(time.RFC3339, startedAtRFC3339)
	require.NoError(t, err)

	endedAtRFC3339 := time.Now().Add(time.Minute).Format(time.RFC3339)
	endedAt, err := time.Parse(time.RFC3339, endedAtRFC3339)
	require.NoError(t, err)

	var testsStatementV1 = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions,
	) *in_toto.ProvenanceStatementSLSA1 {
		return &in_toto.ProvenanceStatementSLSA1{
			StatementHeader: in_toto.StatementHeader{
				Type:          in_toto.StatementInTotoV01,
				PredicateType: slsa_v1.PredicateSLSAProvenance,
				Subject: []in_toto.Subject{
					{
						Name:   tmpFile.Name(),
						Digest: slsa_common.DigestSet{"sha256": hex.EncodeToString(checksum)},
					},
				},
			},
			Predicate: slsa_v1.ProvenancePredicate{
				BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
					BuildType: fmt.Sprintf(attestationTypeFormat, g.version()),
					ExternalParameters: map[string]string{
						"testparam":  "",
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					},
					InternalParameters: map[string]string{
						"name":         g.RunnerName,
						"executor":     g.ExecutorName,
						"architecture": common.AppVersion.Architecture,
						"job":          fmt.Sprint(opts.jobID),
					},
					ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
						URI:    g.RepoURL,
						Digest: map[string]string{"sha256": g.RepoDigest},
					}},
				},
				RunDetails: slsa_v1.ProvenanceRunDetails{
					Builder: slsa_v1.Builder{
						ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
						Version: map[string]string{
							"gitlab-runner": version,
						},
					},
					BuildMetadata: slsa_v1.BuildMetadata{
						InvocationID: fmt.Sprint(opts.jobID),
						StartedOn:    &startedAt,
						FinishedOn:   &endedAt,
					},
				},
			},
		}
	}

	var testStatement = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions) any {
		switch g.SLSAProvenanceVersion {
		case slsaProvenanceVersion1:
			return testsStatementV1(version, g, opts)
		default:
			panic("unreachable, invalid statement version")
		}
	}

	var setVersion = func(version string) (string, func()) {
		originalVersion := common.AppVersion.Version
		common.AppVersion.Version = version

		return version, func() {
			common.AppVersion.Version = originalVersion
		}
	}

	var newGenerator = func(slsaVersion string) *artifactStatementGenerator {
		return &artifactStatementGenerator{
			RunnerID:              1001,
			RepoURL:               "testurl",
			RepoDigest:            "testdigest",
			JobName:               "testjobname",
			ExecutorName:          "testexecutorname",
			RunnerName:            "testrunnername",
			Parameters:            []string{"testparam"},
			StartedAtRFC3339:      startedAtRFC3339,
			EndedAtRFC3339:        endedAtRFC3339,
			SLSAProvenanceVersion: slsaVersion,
		}
	}

	tests := map[string]struct {
		opts          generateStatementOptions
		newGenerator  func(slsaVersion string) *artifactStatementGenerator
		expected      func(*artifactStatementGenerator, generateStatementOptions) (any, func())
		expectedError error
	}{
		"basic": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				version, cleanup := setVersion("v1.0.0")
				return testStatement(version, g, opts), cleanup
			},
		},
		"basic version isn't prefixed so use REVISION": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"files subject doesn't exist": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"nonexisting":  fileInfo{name: "nonexisting"},
				},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expectedError: os.ErrNotExist,
		},
		"non-regular file": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"dir":          fileInfo{name: "im-a-dir", mode: fs.ModeDir}},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"no parameters": {
			newGenerator: func(v string) *artifactStatementGenerator {
				g := newGenerator(v)
				g.Parameters = nil

				return g
			},
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				m := testStatement(common.AppVersion.Revision, g, opts)
				switch m := m.(type) {
				case *in_toto.ProvenanceStatementSLSA1:
					m.Predicate.BuildDefinition.ExternalParameters = map[string]string{
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					}
				case *in_toto.ProvenanceStatementSLSA02:
					m.Predicate.Invocation.Parameters = map[string]interface{}{}
				}
				return m, func() {}
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			for _, v := range []string{slsaProvenanceVersion1} {
				t.Run(v, func(t *testing.T) {
					g := tt.newGenerator(v)

					var expected any
					if tt.expected != nil {
						var cleanup func()
						expected, cleanup = tt.expected(g, tt.opts)
						defer cleanup()
					}

					f, err := g.generateStatementToFile(tt.opts)
					if tt.expectedError == nil {
						require.NoError(t, err)
					} else {
						assert.Empty(t, f)
						assert.ErrorIs(t, err, tt.expectedError)
						return
					}

					filename := filepath.Base(f)
					assert.Equal(t, fmt.Sprintf(artifactsStatementFormat, tt.opts.artifactName), filename)

					file, err := os.Open(f)
					require.NoError(t, err)
					defer file.Close()

					b, err := io.ReadAll(file)
					require.NoError(t, err)

					indented, err := json.MarshalIndent(expected, "", " ")
					require.NoError(t, err)

					assert.Equal(t, string(indented), string(b))
					assert.Contains(t, string(indented), startedAtRFC3339)
					assert.Contains(t, string(indented), endedAtRFC3339)
				})
			}
		})
	}
}

func TestArchiveUploadRedirect(t *testing.T) {
	finalRequestReceived := false

	finalServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "final", finalServerHandler(t, &finalRequestReceived)),
	)
	defer finalServer.Close()

	redirectingServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "redirection", redirectingServerHandler(finalServer.URL)),
	)
	defer redirectingServer.Close()

	cmd := &ArtifactsUploaderCommand{
		JobCredentials: common.JobCredentials{
			ID:    12345,
			Token: "token",
			URL:   redirectingServer.URL,
		},
		Name:             "artifacts",
		Format:           common.ArtifactFormatZip,
		CompressionLevel: "fastest",
		network:          network.NewGitLabClient(),
		fileArchiver: fileArchiver{
			Paths: []string{
				filepath.Join(".", "testdata", "test-artifacts"),
			},
		},
	}

	defer helpers.MakeFatalToPanic()()

	assert.NotPanics(t, func() {
		cmd.Execute(&cli.Context{})
	}, "expected command not to log fatal")

	assert.True(t, finalRequestReceived)
}

func TestImportCommento(t *testing.T) {
	failTestOnError(t, setupTestEnv())

	// Create JSON data
	data := commentoExportV1{
		Version: 1,
		Comments: []comment{
			{
				CommentHex:   "5a349182b3b8e25107ab2b12e514f40fe0b69160a334019491d7c204aff6fdc2",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a reply!",
				Html:         "",
				ParentHex:    "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:08:44.061525Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a comment!",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:07:49.244432Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "a7c84f251b5a09d5b65e902cbe90633646437acefa3a52b761fee94002ac54c7",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Markdown:     "This is a test comment, bar foo\n\n#Here is something big\n\n```\nhere code();\n```",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:20:21.101653Z"),
				Direction:    0,
				Deleted:      false,
			},
		},
		Commenters: []commenter{
			{
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Email:        "john@doe.com",
				Name:         "John Doe",
				Link:         "https://john.doe",
				Photo:        "undefined",
				Provider:     "commento",
				JoinDate:     timeParse(t, "2020-01-27T14:17:59.298737Z"),
				IsModerator:  false,
			},
		},
	}

	// Create listener with random port
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Errorf("couldn't create listener: %v", err)
		return
	}
	defer func() {
		_ = listener.Close()
	}()
	port := listener.Addr().(*net.TCPAddr).Port

	// Launch http server serving commento json gzipped data
	go func() {
		http.Serve(listener, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			gzipper := gzip.NewWriter(w)
			defer func() {
				_ = gzipper.Close()
			}()
			encoder := json.NewEncoder(gzipper)
			if err := encoder.Encode(data); err != nil {
				t.Errorf("couldn't write data: %v", err)
			}
		}))
	}()
	url := fmt.Sprintf("http://127.0.0.1:%d", port)

	domainNew("temp-owner-hex", "Example", "example.com")

	n, err := domainImportCommento("example.com", url)
	if err != nil {
		t.Errorf("unexpected error importing comments: %v", err)
		return
	}
	if n != len(data.Comments) {
		t.Errorf("imported comments missmatch (got %d, want %d)", n, len(data.Comments))
	}
}

func viewsCleanupBegin() error {
	go func() {
		for {
			statement := `
				DELETE FROM views
				WHERE viewDate < $1;
			`
			_, err := db.Exec(statement, time.Now().UTC().AddDate(0, 0, -45))
			if err != nil {
				logger.Errorf("error cleaning up views: %v", err)
				return
			}

			time.Sleep(24 * time.Hour)
		}
	}()

	return nil
}

func TestExecute_MergeConfigTemplate(t *testing.T) {
	var (
		configTemplateMergeInvalidConfiguration = `- , ;`

		configTemplateMergeAdditionalConfiguration = `
[[runners]]
  [runners.kubernetes]
    [runners.kubernetes.volumes]
      [[runners.kubernetes.volumes.empty_dir]]
        name = "empty_dir"
	    mount_path = "/path/to/empty_dir"
	    medium = "Memory"
	    size_limit = "1G"`

		baseOutputConfigFmt = `concurrent = 1
check_interval = 0
connection_max_age = "15m0s"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = %q
  url = "http://gitlab.example.com/"
  id = 0
  token = "test-runner-token"
  token_obtained_at = %s
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  shell = "pwsh"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
`
	)

	tests := map[string]struct {
		configTemplate         string
		networkAssertions      func(n *common.MockNetwork)
		errExpected            bool
		expectedFileContentFmt string
	}{
		"config template disabled": {
			configTemplate: "",
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected:            false,
			expectedFileContentFmt: baseOutputConfigFmt,
		},
		"config template with no additional runner configuration": {
			configTemplate: "[[runners]]",
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected:            false,
			expectedFileContentFmt: baseOutputConfigFmt,
		},
		"successful config template merge": {
			configTemplate: configTemplateMergeAdditionalConfiguration,
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected: false,
			expectedFileContentFmt: `concurrent = 1
check_interval = 0
connection_max_age = "15m0s"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = %q
  url = "http://gitlab.example.com/"
  id = 0
  token = "test-runner-token"
  token_obtained_at = %s
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  shell = "pwsh"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
`,
		},
		"incorrect config template merge": {
			configTemplate:    configTemplateMergeInvalidConfiguration,
			networkAssertions: func(n *common.MockNetwork) {},
			errExpected:       true,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			var err error

			if tt.errExpected {
				helpers.MakeFatalToPanic()
			}

			cfgTpl, cleanup := commands.PrepareConfigurationTemplateFile(t, tt.configTemplate)
			defer cleanup()

			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			args := []string{
				"--shell", shells.SNPwsh,
				"--registration-token", "test-runner-token",
			}

			if tt.configTemplate != "" {
				args = append(args, "--template-config", cfgTpl)
			}

			tt.networkAssertions(network)

			fileContent, _, err := testRegisterCommandRun(t, network, nil, "", args...)
			if tt.errExpected {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)
			name, err := os.Hostname()
			require.NoError(t, err)
			assert.Equal(t, fmt.Sprintf(tt.expectedFileContentFmt, name, commands.RegisterTimeNowDate.Format(time.RFC3339)), fileContent)
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
	// ruleid:go_injection_rule-ssrf
	response, err := http.Get(url)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import "fmt"

var vector []*string

func appendVector(s *string) {
	vector = append(vector, s)
}

func printVector() {
	for _, item := range vector {
		fmt.Printf("%s", *item)
	}
	fmt.Println()
}

func foo() (int, **string, *string) {
	for _, item := range vector {
		return 0, &item, item
	}
	return 0, nil, nil
}

func appendrange() {
	// ruleid:go_memory_rule-memoryaliasing
	for _, item := range []string{"A", "B", "C"} {
		appendVector(&item)
	}

	printVector()

	zero, c_star, c := foo()
	fmt.Printf("%d %v %s", zero, c_star, c)
}

func saferange() {
	sampleMap := map[string]string{}
	sampleString := "A string"
	for sampleString = range sampleMap {
		fmt.Println(sampleString)
	}
}

func safeiter() {
	// ok:go_memory_rule-memoryaliasing
	array := []string{"a", "b"}
	for i := range array {
		appendVector(&array[i])
	}
}

// https://gitlab.com/gitlab-org/gitlab/-/issues/348952
// ok:go_memory_rule-memoryaliasing
func shouldNotBeReported() {
	array := []string{"a", "b"}
	for _, s := range array {
		fmt.Println(s)
	}
}

func shouldBeReported() {
	array := []string{"a", "b"}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"math/rand"
	mrand "math/rand"
)

func bad1() {
	bad := rand.Int()
	println(bad)
}

func bad2() {
	good, _ := rand.Read(nil)
	println(good)
	// ruleid: go_crypto_rule-weakrandsource
	bad := mrand.Int31()
	println(bad)
}

func badnewsource() {
	gen := rand.New(rand.NewSource(10))
	bad := gen.Int()
	println(bad)
}

func badIntn() {
	bad := rand.Intn(10)
	println(bad)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func maintempfiles() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}
	// ok: go_filesystem_rule-tempfiles
	err = ioutil.WriteFile("./some/tmp/dir", []byte("stuff"), 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

	// ruleid: go_filesystem_rule-tempfiles
	_, err = os.OpenFile("/tmp/demo2", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}

	// ok: go_filesystem_rule-tempfiles
	_, err = os.OpenFile("/tmp/demo2", os.O_RDONLY, 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

	// ok: go_filesystem_rule-tempfiles
	_, err = os.CreateTemp("/tmp", "demo2")
	if err != nil {
		fmt.Println("Error while writing!")
	}
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
)

func dbUnsafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	unsafe1 := "select * from foo where bar = '" + tainted + "'"
	unsafe2 := "select * from foo where id = " + tainted
	unsafe3 := fmt.Sprintf("select * from foo where bar = %q", tainted)

	unsafe4 := strings.Builder{}
	unsafe4.WriteString("select * from foo where bar = ")
	unsafe4.WriteString(tainted)

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	// ruleid:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = '"+tainted+"'")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func dbSafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = '" + "safe" + "'"
	safe2 := "select * from foo where id = " + "safe"
	safe3 := fmt.Sprintf("select * from foo where bar = %q", "safe")

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe3)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = '" + "safe" + "'")
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES('"+"safe"+"')")
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = '" + "safe" + "' WHERE id = 100")
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = '"+"safe"+"'")
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values('" + "safe" + "')")
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = '"+"safe"+"' where id = 100")
}

func dbSafe2(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = ?"
	safe2 := "select * from foo where bar is $1 and baz = $2"

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1, tainted)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2, true, tainted)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = ? WHERE id = 100", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = $2 and id = $1", "baz", tainted)
}

// False-positive reported by customer (see gitlab-org/gitlab#451108).
func safeIssue_451108(ctx context.Context, val string) error {
	if err := somepkg.validate(val); err != nil {
		return &somepkg.Error{Message: err.Error()}
	}

	if !updateAllowed(somepkg.Status) {
		// ok:go_sql_rule-concat-sqli
		return &somepkg.Error{Message: fmt.Sprintf("update is not permitted: %v", somepkg.Status)}
	}

	return nil
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func mainwriteperms() {

	d1 := []byte("hello\ngo\n")
	// ruleid: go_filesystem_rule-poorwritepermissions
	err := ioutil.WriteFile("/tmp/dat1", d1, 0744)
	check(err)

	allowed := ioutil.WriteFile("/tmp/dat1", d1, 0600)
	check(allowed)

	f, err := os.Create("/tmp/dat2")
	check(err)

	defer f.Close()

	d2 := []byte{115, 111, 109, 101, 10}
	n2, err := f.Write(d2)

	defer check(err)
	fmt.Printf("wrote %d bytes\n", n2)

	n3, err := f.WriteString("writes\n")
	fmt.Printf("wrote %d bytes\n", n3)

	f.Sync()

	w := bufio.NewWriter(f)
	n4, err := w.WriteString("buffered\n")
	fmt.Printf("wrote %d bytes\n", n4)

	w.Flush()

}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(getParam(r), nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"io/ioutil"
	"os"
)

func maintempfiles() {
	// ruleid: go_filesystem_rule-tempfiles
	err := ioutil.WriteFile("/tmp/demo2", []byte("This is some data"), 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}
	// ok: go_filesystem_rule-tempfiles
	err = ioutil.WriteFile("./some/tmp/dir", []byte("stuff"), 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

	// ok: go_filesystem_rule-tempfiles
	_, err = os.OpenFile("/tmp/demo2", os.O_RDONLY, 0644)
	if err != nil {
		fmt.Println("Error while writing!")
	}

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error while writing!")
	}

	// ok: go_filesystem_rule-tempfiles
	_, err = os.CreateTemp("/tmp", "demo2")
	if err != nil {
		fmt.Println("Error while writing!")
	}
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	response, err := http.Get(getParam(r))
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"os"
)

func foo0() {
	err := os.Mkdir("/tmp/mydir", 0700)
	if err != nil {
		fmt.Println("Error when creating a directory!")
		return
	}
}

func foo1() {
	// ruleid: go_file-permissions_rule-mkdir
	err := os.Mkdir("/tmp/mydir", 0777)
	if err != nil {
		fmt.Println("Error when creating a directory!")
		return
	}
}

func foo2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Println("Error when creating a directory!")
		return
	}
}

func foo3() {
	err := os.Mkdir("/tmp/mydir", 0600)
	if err != nil {
		fmt.Println("Error when creating a directory!")
		return
	}
}

func foo4() {
	err := os.Mkdir("/tmp/mydir", 0750)
	if err != nil {
		fmt.Println("Error when creating a directory!")
		return
	}
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(getParam(r))
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

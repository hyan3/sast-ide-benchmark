func TestAskRunnerUsingRunnerTokenOnRegistrationTokenOverridingForbiddenDefaults(t *testing.T) {
	tests := map[string]interface{}{
		"--access-level":     "not_protected",
		"--run-untagged":     true,
		"--maximum-timeout":  1,
		"--paused":           true,
		"--tag-list":         "tag",
		"--maintenance-note": "note",
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
				Return(&common.VerifyRunnerResponse{
					ID:    1,
					Token: "glrt-testtoken",
				}).
				Once()

			answers := make([]string, 4)
			arguments := append(
				executorCmdLineArgs(t, "shell"),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				tn, fmt.Sprintf("%v", tc),
			)

			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			hook := test.NewGlobal()
			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			_ = app.Run(append([]string{"runner", "register"}, arguments...))

			assert.Contains(
				t,
				commands.GetLogrusOutput(t, hook),
				"This has triggered the 'legacy-compatible registration process'",
			)
		})
	}
}

func TestCacheArchiverUploadedSize(t *testing.T) {
	tests := map[string]struct {
		limit    int
		exceeded bool
	}{
		"no-limit":    {limit: 0, exceeded: false},
		"above-limit": {limit: 10, exceeded: true},
		"equal-limit": {limit: 22, exceeded: false},
		"below-limit": {limit: 25, exceeded: false},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			defer logrus.SetOutput(logrus.StandardLogger().Out)
			defer testHelpers.MakeFatalToPanic()()

			var buf bytes.Buffer
			logrus.SetOutput(&buf)

			ts := httptest.NewServer(http.HandlerFunc(testCacheBaseUploadHandler))
			defer ts.Close()

			defer os.Remove(cacheArchiverArchive)
			cmd := helpers.CacheArchiverCommand{
				File:                   cacheArchiverArchive,
				MaxUploadedArchiveSize: int64(tc.limit),
				URL:                    ts.URL + "/cache.zip",
				Timeout:                0,
			}
			assert.NotPanics(t, func() {
				cmd.Execute(nil)
			})

			if tc.exceeded {
				require.Contains(t, buf.String(), "too big")
			} else {
				require.NotContains(t, buf.String(), "too big")
			}
		})
	}
}

func TestGenerateMetadataToFile(t *testing.T) {
	tmpDir := t.TempDir()
	tmpFile, err := os.CreateTemp(tmpDir, "")
	require.NoError(t, err)

	_, err = tmpFile.WriteString("testdata")
	require.NoError(t, err)
	require.NoError(t, tmpFile.Close())

	sha := sha256.New()
	sha.Write([]byte("testdata"))
	checksum := sha.Sum(nil)

	// First format the time to RFC3339 and then parse it to get the correct precision
	startedAtRFC3339 := time.Now().Format(time.RFC3339)
	startedAt, err := time.Parse(time.RFC3339, startedAtRFC3339)
	require.NoError(t, err)

	endedAtRFC3339 := time.Now().Add(time.Minute).Format(time.RFC3339)
	endedAt, err := time.Parse(time.RFC3339, endedAtRFC3339)
	require.NoError(t, err)

	var testsStatementV1 = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions,
	) *in_toto.ProvenanceStatementSLSA1 {
		return &in_toto.ProvenanceStatementSLSA1{
			StatementHeader: in_toto.StatementHeader{
				Type:          in_toto.StatementInTotoV01,
				PredicateType: slsa_v1.PredicateSLSAProvenance,
				Subject: []in_toto.Subject{
					{
						Name:   tmpFile.Name(),
						Digest: slsa_common.DigestSet{"sha256": hex.EncodeToString(checksum)},
					},
				},
			},
			Predicate: slsa_v1.ProvenancePredicate{
				BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
					BuildType: fmt.Sprintf(attestationTypeFormat, g.version()),
					ExternalParameters: map[string]string{
						"testparam":  "",
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					},
					InternalParameters: map[string]string{
						"name":         g.RunnerName,
						"executor":     g.ExecutorName,
						"architecture": common.AppVersion.Architecture,
						"job":          fmt.Sprint(opts.jobID),
					},
					ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
						URI:    g.RepoURL,
						Digest: map[string]string{"sha256": g.RepoDigest},
					}},
				},
				RunDetails: slsa_v1.ProvenanceRunDetails{
					Builder: slsa_v1.Builder{
						ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
						Version: map[string]string{
							"gitlab-runner": version,
						},
					},
					BuildMetadata: slsa_v1.BuildMetadata{
						InvocationID: fmt.Sprint(opts.jobID),
						StartedOn:    &startedAt,
						FinishedOn:   &endedAt,
					},
				},
			},
		}
	}

	var testStatement = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions) any {
		switch g.SLSAProvenanceVersion {
		case slsaProvenanceVersion1:
			return testsStatementV1(version, g, opts)
		default:
			panic("unreachable, invalid statement version")
		}
	}

	var setVersion = func(version string) (string, func()) {
		originalVersion := common.AppVersion.Version
		common.AppVersion.Version = version

		return version, func() {
			common.AppVersion.Version = originalVersion
		}
	}

	var newGenerator = func(slsaVersion string) *artifactStatementGenerator {
		return &artifactStatementGenerator{
			RunnerID:              1001,
			RepoURL:               "testurl",
			RepoDigest:            "testdigest",
			JobName:               "testjobname",
			ExecutorName:          "testexecutorname",
			RunnerName:            "testrunnername",
			Parameters:            []string{"testparam"},
			StartedAtRFC3339:      startedAtRFC3339,
			EndedAtRFC3339:        endedAtRFC3339,
			SLSAProvenanceVersion: slsaVersion,
		}
	}

	tests := map[string]struct {
		opts          generateStatementOptions
		newGenerator  func(slsaVersion string) *artifactStatementGenerator
		expected      func(*artifactStatementGenerator, generateStatementOptions) (any, func())
		expectedError error
	}{
		"basic": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				version, cleanup := setVersion("v1.0.0")
				return testStatement(version, g, opts), cleanup
			},
		},
		"basic version isn't prefixed so use REVISION": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"files subject doesn't exist": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"nonexisting":  fileInfo{name: "nonexisting"},
				},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expectedError: os.ErrNotExist,
		},
		"non-regular file": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"dir":          fileInfo{name: "im-a-dir", mode: fs.ModeDir}},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"no parameters": {
			newGenerator: func(v string) *artifactStatementGenerator {
				g := newGenerator(v)
				g.Parameters = nil

				return g
			},
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				m := testStatement(common.AppVersion.Revision, g, opts)
				switch m := m.(type) {
				case *in_toto.ProvenanceStatementSLSA1:
					m.Predicate.BuildDefinition.ExternalParameters = map[string]string{
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					}
				case *in_toto.ProvenanceStatementSLSA02:
					m.Predicate.Invocation.Parameters = map[string]interface{}{}
				}
				return m, func() {}
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			for _, v := range []string{slsaProvenanceVersion1} {
				t.Run(v, func(t *testing.T) {
					g := tt.newGenerator(v)

					var expected any
					if tt.expected != nil {
						var cleanup func()
						expected, cleanup = tt.expected(g, tt.opts)
						defer cleanup()
					}

					f, err := g.generateStatementToFile(tt.opts)
					if tt.expectedError == nil {
						require.NoError(t, err)
					} else {
						assert.Empty(t, f)
						assert.ErrorIs(t, err, tt.expectedError)
						return
					}

					filename := filepath.Base(f)
					assert.Equal(t, fmt.Sprintf(artifactsStatementFormat, tt.opts.artifactName), filename)

					file, err := os.Open(f)
					require.NoError(t, err)
					defer file.Close()

					b, err := io.ReadAll(file)
					require.NoError(t, err)

					indented, err := json.MarshalIndent(expected, "", " ")
					require.NoError(t, err)

					assert.Equal(t, string(indented), string(b))
					assert.Contains(t, string(indented), startedAtRFC3339)
					assert.Contains(t, string(indented), endedAtRFC3339)
				})
			}
		})
	}
}

func domainExportCleanupBegin() error {
	go func() {
		for {
			statement := `
				DELETE FROM exports
				WHERE creationDate < $1;
			`
			_, err := db.Exec(statement, time.Now().UTC().AddDate(0, 0, -7))
			if err != nil {
				logger.Errorf("error cleaning up export rows: %v", err)
				return
			}

			time.Sleep(2 * time.Hour)
		}
	}()

	return nil
}

func TestHealthCheckCommand_Execute(t *testing.T) {
	cases := []struct {
		name            string
		expectedConnect bool
		exposeHigher    bool
		exposeLower     bool
	}{
		{
			name:            "Successful connect",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Unsuccessful connect because service is down",
			expectedConnect: false,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with lower port exposed",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     true,
		},
		{
			name:            "Successful connect with both lower and higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     true,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			os.Unsetenv("SERVICE_LOWER_TCP_PORT")
			os.Unsetenv("SERVICE_HIGHER_TCP_PORT")

			// Start listening to reverse addr
			listener, err := net.Listen("tcp", "127.0.0.1:")
			require.NoError(t, err)
			defer listener.Close()

			port := listener.Addr().(*net.TCPAddr).Port

			err = os.Setenv("SERVICE_TCP_ADDR", "127.0.0.1")
			require.NoError(t, err)

			err = os.Setenv("SERVICE_TCP_PORT", strconv.Itoa(port))
			require.NoError(t, err)

			if c.exposeHigher {
				err = os.Setenv("SERVICE_HIGHER_TCP_PORT", strconv.Itoa(port+1))
				require.NoError(t, err)
			}

			if c.exposeLower {
				err = os.Setenv("SERVICE_LOWER_TCP_PORT", strconv.Itoa(port-1))
				require.NoError(t, err)
			}

			// If we don't expect to connect we close the listener.
			if !c.expectedConnect {
				listener.Close()
			}

			ctx, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
			defer cancelFn()
			done := make(chan struct{})
			go func() {
				cmd := HealthCheckCommand{ctx: ctx}
				cmd.Execute(nil)
				done <- struct{}{}
			}()

			select {
			case <-ctx.Done():
				if c.expectedConnect {
					require.Fail(t, "Timeout waiting to start service.")
				}
			case <-done:
				if !c.expectedConnect {
					require.Fail(t, "Expected to not connect to server")
				}
			}
		})
	}
}

func TestNew(t *testing.T) {
	t.Run("no config", func(t *testing.T) {
		adapter, err := New(&common.CacheConfig{}, time.Second, "bucket")
		require.ErrorContains(t, err, "missing GCS configuration")
		require.Nil(t, adapter)
	})

	t.Run("valid", func(t *testing.T) {
		adapter, err := New(&common.CacheConfig{GCS: &common.CacheGCSConfig{}}, time.Second, "bucket")
		require.NoError(t, err)
		require.NotNil(t, adapter)
	})
}

func TestAskRunnerUsingRunnerTokenOverrideDefaults(t *testing.T) {
	const executor = "docker"

	basicValidation := func(s *commands.RegisterCommand) {
		assert.Equal(t, "http://gitlab.example.com/", s.URL)
		assert.Equal(t, "glrt-testtoken", s.Token)
		assert.Equal(t, executor, s.RunnerSettings.Executor)
	}
	expectedParamsFn := func(p common.RunnerCredentials) bool {
		return p.URL == "http://gitlab.example.com/" && p.Token == "glrt-testtoken"
	}

	tests := map[string]struct {
		answers        []string
		arguments      []string
		validate       func(s *commands.RegisterCommand)
		expectedParams func(common.RunnerCredentials) bool
	}{
		"basic answers": {
			answers: append([]string{
				"http://gitlab.example.com/",
				"glrt-testtoken",
				"name",
			}, executorAnswers(t, executor)...),
			validate:       basicValidation,
			expectedParams: expectedParamsFn,
		},
		"basic arguments, accepting provided": {
			answers: make([]string, 9),
			arguments: append(
				executorCmdLineArgs(t, executor),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				"--name", "name",
			),
			validate:       basicValidation,
			expectedParams: expectedParamsFn,
		},
		"basic arguments override": {
			answers: append(
				[]string{"http://gitlab.example2.com/", "glrt-testtoken2", "new-name", executor},
				executorOverrideAnswers(t, executor)...,
			),
			arguments: append(
				executorCmdLineArgs(t, executor),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				"--name", "name",
			),
			validate: func(s *commands.RegisterCommand) {
				assert.Equal(t, "http://gitlab.example2.com/", s.URL)
				assert.Equal(t, "glrt-testtoken2", s.Token)
				assert.Equal(t, "new-name", s.Name)
				assert.Equal(t, executor, s.RunnerSettings.Executor)
				require.NotNil(t, s.RunnerSettings.Docker)
				assert.Equal(t, "nginx:latest", s.RunnerSettings.Docker.Image)
			},
			expectedParams: func(p common.RunnerCredentials) bool {
				return p.URL == "http://gitlab.example2.com/" && p.Token == "glrt-testtoken2"
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			network.On("VerifyRunner", mock.MatchedBy(tc.expectedParams), mock.MatchedBy(isValidToken)).
				Return(&common.VerifyRunnerResponse{
					ID:    12345,
					Token: "glrt-testtoken",
				}).
				Once()

			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(tc.answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			hook := test.NewGlobal()
			args := append(tc.arguments, "--leave-runner")
			args, cleanTempFile := useTempConfigFile(t, args)
			defer cleanTempFile()
			err := app.Run(append([]string{"runner", "register"}, args...))
			output := commands.GetLogrusOutput(t, hook)

			assert.NoError(t, err)
			tc.validate(cmd)
			assert.Contains(t, output, "Runner registered successfully.")
		})
	}
}

func Test_loadConfig(t *testing.T) {
	const expectedSystemIDRegexPattern = "^[sr]_[0-9a-zA-Z]{12}$"

	testCases := map[string]struct {
		runnerSystemID string
		prepareFn      func(t *testing.T, systemIDFile string)
		assertFn       func(t *testing.T, err error, config *common.Config, systemIDFile string)
	}{
		"generates and saves missing system IDs": {
			runnerSystemID: "",
			assertFn: func(t *testing.T, err error, config *common.Config, systemIDFile string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.NotEmpty(t, config.Runners[0].SystemIDState.GetSystemID())
				content, err := os.ReadFile(systemIDFile)
				require.NoError(t, err)
				assert.Contains(t, string(content), config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"preserves existing unique system IDs": {
			runnerSystemID: "s_c2d22f638c25",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Equal(t, "s_c2d22f638c25", config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"regenerates system ID if file is invalid": {
			runnerSystemID: "0123456789",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"succeeds if file cannot be created": {
			runnerSystemID: "",
			prepareFn: func(t *testing.T, systemIDFile string) {
				require.NoError(t, os.Remove(systemIDFile))
				require.NoError(t, os.Chmod(filepath.Dir(systemIDFile), os.ModeDir|0500))
			},
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				require.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			dir := t.TempDir()
			cfgName := filepath.Join(dir, "config.toml")
			systemIDFile := filepath.Join(dir, ".runner_system_id")

			require.NoError(t, os.Chmod(dir, 0777))
			require.NoError(t, os.WriteFile(cfgName, []byte("[[runners]]\n name = \"runner\""), 0777))
			require.NoError(t, os.WriteFile(systemIDFile, []byte(tc.runnerSystemID), 0777))

			if tc.prepareFn != nil {
				tc.prepareFn(t, systemIDFile)
			}

			c := configOptions{ConfigFile: cfgName}
			err := c.loadConfig()

			tc.assertFn(t, err, c.config, systemIDFile)

			// Cleanup
			require.NoError(t, os.Chmod(dir, 0777))
		})
	}
}

func TestCacheOperationEncryptionAES(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)
	headers := http.Header{}
	headers.Add("X-Amz-Server-Side-Encryption", "AES256")

	tests := map[string]cacheOperationTest{
		"error-on-minio-client-initialization": {
			errorOnMinioClientInitialization: true,
			expectedUploadHeaders:            headers,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning:  true,
			presignedURL:          URL,
			expectedURL:           nil,
			expectedUploadHeaders: nil,
		},
		"presigned-url-aes": {
			presignedURL:          URL,
			expectedURL:           URL,
			expectedUploadHeaders: headers,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionAES(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionAES(),
			)
		})
	}
}

func TestBuildsHelper_ListJobsHandler(t *testing.T) {
	tests := map[string]struct {
		build          *common.Build
		expectedOutput []string
	}{
		"no jobs": {
			build: nil,
		},
		"job exists": {
			build: &common.Build{
				Runner: &common.RunnerConfig{
					SystemIDState: common.NewSystemIDState(),
				},
				JobResponse: common.JobResponse{
					ID:      1,
					JobInfo: common.JobInfo{ProjectID: 1},
					GitInfo: common.GitInfo{RepoURL: "https://gitlab.example.com/my-namespace/my-project.git"},
				},
			},
			expectedOutput: []string{
				"url=https://gitlab.example.com/my-namespace/my-project/-/jobs/1",
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			writer := httptest.NewRecorder()

			req, err := http.NewRequest(http.MethodGet, "/", nil)
			require.NoError(t, err)

			b := newBuildsHelper()
			b.addBuild(test.build)
			b.ListJobsHandler(writer, req)

			if test.build != nil {
				require.NoError(t, test.build.Runner.SystemIDState.EnsureSystemID())
			}

			resp := writer.Result()
			defer resp.Body.Close()

			assert.Equal(t, http.StatusOK, resp.StatusCode)
			assert.Equal(t, "2", resp.Header.Get("X-List-Version"))
			assert.Equal(t, "text/plain", resp.Header.Get(common.ContentType))

			body, err := io.ReadAll(resp.Body)
			require.NoError(t, err)

			if len(test.expectedOutput) == 0 {
				assert.Empty(t, body)
				return
			}

			for _, expectedOutput := range test.expectedOutput {
				assert.Contains(t, string(body), expectedOutput)
			}
		})
	}
}

func (mr *RunCommand) processRunners(id int, stopWorker chan bool, runners chan *common.RunnerConfig) {
	mr.log().
		WithField("worker", id).
		Debugln("Starting worker")

	mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStarted).Inc()

	for mr.stopSignal == nil {
		select {
		case runner := <-runners:
			err := mr.processRunner(id, runner, runners)
			if err != nil {
				logger := mr.log().
					WithFields(logrus.Fields{
						"runner":   runner.ShortDescription(),
						"executor": runner.Executor,
					}).WithError(err)

				l, failureType := loggerAndFailureTypeFromError(logger, err)
				l("Failed to process runner")
				mr.runnerWorkerProcessingFailure.
					WithLabelValues(failureType, runner.ShortDescription(), runner.Name, runner.GetSystemID()).
					Inc()
			}

		case <-stopWorker:
			mr.log().
				WithField("worker", id).
				Debugln("Stopping worker")

			mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStopped).Inc()

			return
		}
	}
	<-stopWorker
}

func (g *artifactStatementGenerator) generateSLSAv1Predicate(jobId int64, start time.Time, end time.Time) slsa_v1.ProvenancePredicate {
	externalParams := g.params()
	externalParams["entryPoint"] = g.JobName
	externalParams["source"] = g.RepoURL

	return slsa_v1.ProvenancePredicate{
		BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
			BuildType:          fmt.Sprintf(attestationTypeFormat, g.version()),
			ExternalParameters: externalParams,
			InternalParameters: map[string]string{
				"name":         g.RunnerName,
				"executor":     g.ExecutorName,
				"architecture": common.AppVersion.Architecture,
				"job":          fmt.Sprint(jobId),
			},
			ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
				URI:    g.RepoURL,
				Digest: map[string]string{"sha256": g.RepoDigest},
			}},
		},
		RunDetails: slsa_v1.ProvenanceRunDetails{
			Builder: slsa_v1.Builder{
				ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
				Version: map[string]string{
					"gitlab-runner": g.version(),
				},
			},
			BuildMetadata: slsa_v1.BuildMetadata{
				InvocationID: fmt.Sprint(jobId),
				StartedOn:    &start,
				FinishedOn:   &end,
			},
		},
	}
}

func TestArchiverOptionFromEnv(t *testing.T) {
	tests := map[string]struct {
		value string
		err   string
	}{
		archiverStagingDir:  {"/dev/null", "fastzip archiver unable to create temporary directory"},
		archiverConcurrency: {"-1", "concurrency must be at least 1"},
	}

	for option, tc := range tests {
		t.Run(fmt.Sprintf("%s=%s", option, tc.value), func(t *testing.T) {
			defer tempEnvOption(option, tc.value)()

			archiveTestDir(t, func(_ string, _ string, err error) {
				require.Error(t, err)
				require.Contains(t, err.Error(), tc.err)
			})
		})
	}
}

func newResetRunnerTokenTestController() *resetRunnerTokenTestController {
	networkMock := new(common.MockNetwork)
	configSaverMock := new(common.MockConfigSaver)

	data := &resetRunnerTokenTestController{
		runCommand: RunCommand{
			configOptionsWithListenAddress: configOptionsWithListenAddress{
				configOptions: configOptions{
					config: common.NewConfigWithSaver(configSaverMock),
				},
			},
			runAt:          runAt,
			runFinished:    make(chan bool),
			configReloaded: make(chan int),
			network:        networkMock,
		},
		eventChan:       make(chan interface{}),
		networkMock:     networkMock,
		configSaverMock: configSaverMock,
	}
	data.runCommand.runAt = data.runAt

	return data
}

func TestExecute_MergeConfigTemplate(t *testing.T) {
	var (
		configTemplateMergeInvalidConfiguration = `- , ;`

		configTemplateMergeAdditionalConfiguration = `
[[runners]]
  [runners.kubernetes]
    [runners.kubernetes.volumes]
      [[runners.kubernetes.volumes.empty_dir]]
        name = "empty_dir"
	    mount_path = "/path/to/empty_dir"
	    medium = "Memory"
	    size_limit = "1G"`

		baseOutputConfigFmt = `concurrent = 1
check_interval = 0
connection_max_age = "15m0s"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = %q
  url = "http://gitlab.example.com/"
  id = 0
  token = "test-runner-token"
  token_obtained_at = %s
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  shell = "pwsh"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
`
	)

	tests := map[string]struct {
		configTemplate         string
		networkAssertions      func(n *common.MockNetwork)
		errExpected            bool
		expectedFileContentFmt string
	}{
		"config template disabled": {
			configTemplate: "",
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected:            false,
			expectedFileContentFmt: baseOutputConfigFmt,
		},
		"config template with no additional runner configuration": {
			configTemplate: "[[runners]]",
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected:            false,
			expectedFileContentFmt: baseOutputConfigFmt,
		},
		"successful config template merge": {
			configTemplate: configTemplateMergeAdditionalConfiguration,
			networkAssertions: func(n *common.MockNetwork) {
				n.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			},
			errExpected: false,
			expectedFileContentFmt: `concurrent = 1
check_interval = 0
connection_max_age = "15m0s"
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = %q
  url = "http://gitlab.example.com/"
  id = 0
  token = "test-runner-token"
  token_obtained_at = %s
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "shell"
  shell = "pwsh"
  [runners.custom_build_dir]
  [runners.cache]
    MaxUploadedArchiveSize = 0
    [runners.cache.s3]
    [runners.cache.gcs]
    [runners.cache.azure]
`,
		},
		"incorrect config template merge": {
			configTemplate:    configTemplateMergeInvalidConfiguration,
			networkAssertions: func(n *common.MockNetwork) {},
			errExpected:       true,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			var err error

			if tt.errExpected {
				helpers.MakeFatalToPanic()
			}

			cfgTpl, cleanup := commands.PrepareConfigurationTemplateFile(t, tt.configTemplate)
			defer cleanup()

			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			args := []string{
				"--shell", shells.SNPwsh,
				"--registration-token", "test-runner-token",
			}

			if tt.configTemplate != "" {
				args = append(args, "--template-config", cfgTpl)
			}

			tt.networkAssertions(network)

			fileContent, _, err := testRegisterCommandRun(t, network, nil, "", args...)
			if tt.errExpected {
				require.Error(t, err)
				return
			}

			require.NoError(t, err)
			name, err := os.Hostname()
			require.NoError(t, err)
			assert.Equal(t, fmt.Sprintf(tt.expectedFileContentFmt, name, commands.RegisterTimeNowDate.Format(time.RFC3339)), fileContent)
		})
	}
}

func TestExtractorOptionFromEnv(t *testing.T) {
	tests := map[string]struct {
		value string
		err   string
	}{
		extractorConcurrency: {"-1", "concurrency must be at least 1"},
	}

	for option, tc := range tests {
		t.Run(fmt.Sprintf("%s=%s", option, tc.value), func(t *testing.T) {
			defer tempEnvOption(option, tc.value)()

			archiveTestDir(t, func(archiveFile string, dir string, err error) {
				require.NoError(t, err)

				f, err := os.Open(archiveFile)
				require.NoError(t, err)
				defer f.Close()

				fi, err := f.Stat()
				require.NoError(t, err)

				extractor, err := NewExtractor(f, fi.Size(), dir)
				require.NoError(t, err)

				err = extractor.Extract(context.Background())
				require.Error(t, err)
				require.Contains(t, err.Error(), tc.err)
			})
		})
	}
}

func TestCacheArchiverCompressionLevel(t *testing.T) {
	writeTestFile(t, cacheArchiverTestArchivedFile)
	defer os.Remove(cacheArchiverTestArchivedFile)

	for _, expectedLevel := range []string{"fastest", "fast", "default", "slow", "slowest"} {
		t.Run(expectedLevel, func(t *testing.T) {
			mockArchiver := new(archive.MockArchiver)
			defer mockArchiver.AssertExpectations(t)

			prevArchiver, _ := archive.Register(
				"zip",
				func(w io.Writer, dir string, level archive.CompressionLevel) (archive.Archiver, error) {
					assert.Equal(t, helpers.GetCompressionLevel(expectedLevel), level)
					return mockArchiver, nil
				},
				nil,
			)
			defer archive.Register(
				"zip",
				prevArchiver,
				nil,
			)

			mockArchiver.On("Archive", mock.Anything, mock.Anything).Return(nil)

			defer os.Remove(cacheArchiverArchive)
			cmd := helpers.NewCacheArchiverCommandForTest(cacheArchiverArchive, []string{cacheArchiverTestArchivedFile})
			cmd.CompressionLevel = expectedLevel
			cmd.Execute(nil)
		})
	}
}

func finalServerHandler(t *testing.T, finalRequestReceived *bool) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		dir := t.TempDir()

		receiveFile(t, r, dir)

		err := filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}

			fileName := info.Name()
			fileContentBytes, err := os.ReadFile(path)
			if err != nil {
				return err
			}

			assert.Equal(t, fileName, strings.TrimSpace(string(fileContentBytes)))

			return nil
		})

		assert.NoError(t, err)

		*finalRequestReceived = true
		rw.WriteHeader(http.StatusCreated)
	}
}

func TestRunnerByURLAndID(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerURL     string
		runnerID      int64
		expectedIndex int
		expectedError error
	}{
		"finds runner by name": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      1,
			expectedIndex: 0,
		},
		"does not find runner with wrong ID": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      3,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab1.example.com/" and ID 3`),
		},
		"does not find runner with wrong URL": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab2.example.com/",
			runnerID:      1,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab2.example.com/" and ID 1`),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByURLAndID(tt.runnerURL, tt.runnerID)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestAskRunnerUsingRunnerTokenOverrideDefaults(t *testing.T) {
	const executor = "docker"

	basicValidation := func(s *commands.RegisterCommand) {
		assert.Equal(t, "http://gitlab.example.com/", s.URL)
		assert.Equal(t, "glrt-testtoken", s.Token)
		assert.Equal(t, executor, s.RunnerSettings.Executor)
	}
	expectedParamsFn := func(p common.RunnerCredentials) bool {
		return p.URL == "http://gitlab.example.com/" && p.Token == "glrt-testtoken"
	}

	tests := map[string]struct {
		answers        []string
		arguments      []string
		validate       func(s *commands.RegisterCommand)
		expectedParams func(common.RunnerCredentials) bool
	}{
		"basic answers": {
			answers: append([]string{
				"http://gitlab.example.com/",
				"glrt-testtoken",
				"name",
			}, executorAnswers(t, executor)...),
			validate:       basicValidation,
			expectedParams: expectedParamsFn,
		},
		"basic arguments, accepting provided": {
			answers: make([]string, 9),
			arguments: append(
				executorCmdLineArgs(t, executor),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				"--name", "name",
			),
			validate:       basicValidation,
			expectedParams: expectedParamsFn,
		},
		"basic arguments override": {
			answers: append(
				[]string{"http://gitlab.example2.com/", "glrt-testtoken2", "new-name", executor},
				executorOverrideAnswers(t, executor)...,
			),
			arguments: append(
				executorCmdLineArgs(t, executor),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				"--name", "name",
			),
			validate: func(s *commands.RegisterCommand) {
				assert.Equal(t, "http://gitlab.example2.com/", s.URL)
				assert.Equal(t, "glrt-testtoken2", s.Token)
				assert.Equal(t, "new-name", s.Name)
				assert.Equal(t, executor, s.RunnerSettings.Executor)
				require.NotNil(t, s.RunnerSettings.Docker)
				assert.Equal(t, "nginx:latest", s.RunnerSettings.Docker.Image)
			},
			expectedParams: func(p common.RunnerCredentials) bool {
				return p.URL == "http://gitlab.example2.com/" && p.Token == "glrt-testtoken2"
			},
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			network.On("VerifyRunner", mock.MatchedBy(tc.expectedParams), mock.MatchedBy(isValidToken)).
				Return(&common.VerifyRunnerResponse{
					ID:    12345,
					Token: "glrt-testtoken",
				}).
				Once()

			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(tc.answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			hook := test.NewGlobal()
			args := append(tc.arguments, "--leave-runner")
			args, cleanTempFile := useTempConfigFile(t, args)
			defer cleanTempFile()
			err := app.Run(append([]string{"runner", "register"}, args...))
			output := commands.GetLogrusOutput(t, hook)

			assert.NoError(t, err)
			tc.validate(cmd)
			assert.Contains(t, output, "Runner registered successfully.")
		})
	}
}

func TestCacheArchiverUploadedSize(t *testing.T) {
	tests := map[string]struct {
		limit    int
		exceeded bool
	}{
		"no-limit":    {limit: 0, exceeded: false},
		"above-limit": {limit: 10, exceeded: true},
		"equal-limit": {limit: 22, exceeded: false},
		"below-limit": {limit: 25, exceeded: false},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			defer logrus.SetOutput(logrus.StandardLogger().Out)
			defer testHelpers.MakeFatalToPanic()()

			var buf bytes.Buffer
			logrus.SetOutput(&buf)

			ts := httptest.NewServer(http.HandlerFunc(testCacheBaseUploadHandler))
			defer ts.Close()

			defer os.Remove(cacheArchiverArchive)
			cmd := helpers.CacheArchiverCommand{
				File:                   cacheArchiverArchive,
				MaxUploadedArchiveSize: int64(tc.limit),
				URL:                    ts.URL + "/cache.zip",
				Timeout:                0,
			}
			assert.NotPanics(t, func() {
				cmd.Execute(nil)
			})

			if tc.exceeded {
				require.Contains(t, buf.String(), "too big")
			} else {
				require.NotContains(t, buf.String(), "too big")
			}
		})
	}
}

func TestFileArchiverCompressionLevel(t *testing.T) {
	writeTestFile(t, artifactsTestArchivedFile)
	defer os.Remove(artifactsTestArchivedFile)

	network := &testNetwork{
		uploadState: common.UploadSucceeded,
	}

	for _, expectedLevel := range []string{"fastest", "fast", "default", "slow", "slowest"} {
		t.Run(expectedLevel, func(t *testing.T) {
			mockArchiver := new(archive.MockArchiver)
			defer mockArchiver.AssertExpectations(t)

			archive.Register(
				"zip",
				func(w io.Writer, dir string, level archive.CompressionLevel) (archive.Archiver, error) {
					assert.Equal(t, GetCompressionLevel(expectedLevel), level)
					return mockArchiver, nil
				},
				nil,
			)

			mockArchiver.On("Archive", mock.Anything, mock.Anything).Return(nil)

			cmd := ArtifactsUploaderCommand{
				JobCredentials: UploaderCredentials,
				network:        network,
				Format:         common.ArtifactFormatZip,
				fileArchiver: fileArchiver{
					Paths: []string{artifactsTestArchivedFile},
				},
				CompressionLevel: expectedLevel,
			}
			assert.NoError(t, cmd.enumerate())
			_, r, err := cmd.createReadStream()
			require.NoError(t, err)
			defer r.Close()
			_, _ = io.Copy(io.Discard, r)
		})
	}
}

func TestDoRetry(t *testing.T) {
	cases := []struct {
		name          string
		err           error
		expectedCount int
	}{
		{
			name:          "Error is of type retryableErr",
			err:           retryableErr{err: errors.New("error")},
			expectedCount: 4,
		},
		{
			name:          "Error is not type of retryableErr",
			err:           errors.New("error"),
			expectedCount: 1,
		},
		{
			name:          "Error is nil",
			err:           nil,
			expectedCount: 1,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			r := retryHelper{
				Retry: 3,
			}

			retryCount := 0
			err := r.doRetry(func(_ int) error {
				retryCount++
				return c.err
			})

			assert.Equal(t, c.err, err)
			assert.Equal(t, c.expectedCount, retryCount)
		})
	}
}

func TestCacheArchiverCompressionLevel(t *testing.T) {
	writeTestFile(t, cacheArchiverTestArchivedFile)
	defer os.Remove(cacheArchiverTestArchivedFile)

	for _, expectedLevel := range []string{"fastest", "fast", "default", "slow", "slowest"} {
		t.Run(expectedLevel, func(t *testing.T) {
			mockArchiver := new(archive.MockArchiver)
			defer mockArchiver.AssertExpectations(t)

			prevArchiver, _ := archive.Register(
				"zip",
				func(w io.Writer, dir string, level archive.CompressionLevel) (archive.Archiver, error) {
					assert.Equal(t, helpers.GetCompressionLevel(expectedLevel), level)
					return mockArchiver, nil
				},
				nil,
			)
			defer archive.Register(
				"zip",
				prevArchiver,
				nil,
			)

			mockArchiver.On("Archive", mock.Anything, mock.Anything).Return(nil)

			defer os.Remove(cacheArchiverArchive)
			cmd := helpers.NewCacheArchiverCommandForTest(cacheArchiverArchive, []string{cacheArchiverTestArchivedFile})
			cmd.CompressionLevel = expectedLevel
			cmd.Execute(nil)
		})
	}
}

func TestRegisterDefaultWindowsDockerCacheVolume(t *testing.T) {
	testCases := map[string]struct {
		userDefinedVolumes []string
		expectedVolumes    []string
	}{
		"user did not define anything": {
			userDefinedVolumes: []string{},
			expectedVolumes:    []string{defaultDockerWindowCacheDir},
		},
		"user defined an extra volume": {
			userDefinedVolumes: []string{"c:\\Users\\SomeUser\\config.json:c:\\config.json"},
			expectedVolumes:    []string{defaultDockerWindowCacheDir, "c:\\Users\\SomeUser\\config.json:c:\\config.json"},
		},
		"user defined volume binding to default cache dir": {
			userDefinedVolumes: []string{fmt.Sprintf("c:\\Users\\SomeUser\\cache:%s", defaultDockerWindowCacheDir)},
			expectedVolumes:    []string{fmt.Sprintf("c:\\Users\\SomeUser\\cache:%s", defaultDockerWindowCacheDir)},
		},
		"user defined cache as source leads to incorrect parsing of volume and never adds cache volume": {
			userDefinedVolumes: []string{"c:\\cache:c:\\User\\ContainerAdministrator\\cache"},
			expectedVolumes:    []string{"c:\\cache:c:\\User\\ContainerAdministrator\\cache"},
		},
	}

	for name, testCase := range testCases {
		t.Run(name, func(t *testing.T) {
			s := setupDockerRegisterCommand(&common.DockerConfig{
				Volumes: testCase.userDefinedVolumes,
			})

			s.askDockerWindows()
			assert.ElementsMatch(t, testCase.expectedVolumes, s.Docker.Volumes)
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(getParam(r))
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

// License: MIT (c) GitLab Inc.

package main

// ruleid: go_filesystem_rule-httprootdir
import (
	"log"
	"net/http"
)

func serveRoot() {
	const path = "/"
	fs := http.FileServer(http.Dir(path))
	log.Fatal(http.ListenAndServe(":9000", fs))
}

func serveSubdir() {
	const path = "/var/www/html/public"
	// ok: go_filesystem_rule-httprootdir
	fs := http.FileServer(http.Dir(path))
	log.Fatal(http.ListenAndServe(":9000", fs))
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
)

func dbUnsafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	unsafe1 := "select * from foo where bar = '" + tainted + "'"
	unsafe2 := "select * from foo where id = " + tainted
	unsafe3 := fmt.Sprintf("select * from foo where bar = %q", tainted)

	unsafe4 := strings.Builder{}
	unsafe4.WriteString("select * from foo where bar = ")
	unsafe4.WriteString(tainted)

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	// ruleid:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES('"+tainted+"')")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func dbSafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = '" + "safe" + "'"
	safe2 := "select * from foo where id = " + "safe"
	safe3 := fmt.Sprintf("select * from foo where bar = %q", "safe")

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe3)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = '" + "safe" + "'")
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES('"+"safe"+"')")
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = '" + "safe" + "' WHERE id = 100")
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = '"+"safe"+"'")
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values('" + "safe" + "')")
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = '"+"safe"+"' where id = 100")
}

func dbSafe2(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = ?"
	safe2 := "select * from foo where bar is $1 and baz = $2"

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1, tainted)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2, true, tainted)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = ? WHERE id = 100", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = $2 and id = $1", "baz", tainted)
}

// False-positive reported by customer (see gitlab-org/gitlab#451108).
func safeIssue_451108(ctx context.Context, val string) error {
	if err := somepkg.validate(val); err != nil {
		return &somepkg.Error{Message: err.Error()}
	}

	if !updateAllowed(somepkg.Status) {
		// ok:go_sql_rule-concat-sqli
		return &somepkg.Error{Message: fmt.Sprintf("update is not permitted: %v", somepkg.Status)}
	}

	return nil
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func mainenvreadfile() {
	f := os.Getenv("tainted_file")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
	log.Print(body)
}

func mainqueryopen() {
	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		title := r.URL.Query().Get("title")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
		if err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		body := make([]byte, 5)
		if _, err = f.Read(body); err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		fmt.Fprintf(w, "%s", body)
	})
	log.Fatal(http.ListenAndServe(":3000", nil))
}

func mainqueryopenfile() {
	http.HandleFunc("/bar", func(w http.ResponseWriter, r *http.Request) {
		title := r.URL.Query().Get("title")
		// ruleid:go_filesystem_rule-fileread
		f, err := os.OpenFile(title, os.O_RDWR|os.O_CREATE, 0755)
		if err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		body := make([]byte, 5)
		if _, err = f.Read(body); err != nil {
			fmt.Printf("Error: %v\n", err)
		}
		fmt.Fprintf(w, "%s", body)
	})
	log.Fatal(http.ListenAndServe(":3000", nil))
}

func mainenvreadfileappend() {
	f2 := os.Getenv("tainted_file2")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
	log.Print(body)
}

func mainstdinopenjoin() {
	reader := bufio.NewReader(os.Stdin)
	fmt.Print("Please enter file to read: ")
	file, _ := reader.ReadString('\n')
	file = file[:len(file)-1]
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	contents := make([]byte, 15)
	if _, err = f.Read(contents); err != nil {
		fmt.Printf("Error: %v\n", err)
	}
	fmt.Println(string(contents))
}

func mainenvreadfilejointwice() {
	dir := os.Getenv("server_root")
	f3 := os.Getenv("tainted_file3")
	// edge case where both a binary expression and file Join are used.
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
	log.Print(body)
}

func maincleanshouldTN() {
	repoFile := "path_of_file"
	cleanRepoFile := filepath.Clean(repoFile)
	// ok:go_filesystem_rule-fileread
	_, err := os.OpenFile(cleanRepoFile, os.O_RDONLY, 0600)
	if err != nil {
		panic(err)
	}
}

func openFile(filePath string) {
	// ok:go_filesystem_rule-fileread
	_, err := os.OpenFile(filepath.Clean(filePath), os.O_RDONLY, 0600)
	if err != nil {
		panic(err)
	}
}

func mainintraproceduralopen() {
	repoFile := "path_of_file"
	openFile(repoFile)
}

func mainfilepathrelTN() {
	repoFile := "path_of_file"
	relFile, err := filepath.Rel("./", repoFile)
	if err != nil {
		panic(err)
	}
	// ok:go_filesystem_rule-fileread
	_, err = os.OpenFile(relFile, os.O_RDONLY, 0600)
	if err != nil {
		panic(err)
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"log"
	"net"
)

func Unsafe() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_PortOmitted() {
	// ruleid:go_network_rule-bind-to-all-interfaces
	l, err := net.Listen("tcp", "0.0.0.0")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_AddressOmitted() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_IPv6() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_Ipv6PortOmitted() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Safe() {
	// ok:go_network_rule-bind-to-all-interfaces
	l, err := net.Listen("tcp", "127.0.0.1:1234")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"syscall"
)

func foo1() {
	err := exec.CommandContext(context.Background(), "git", "rev-parse", "--show-toplavel").Run()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo3() {
	run := "sleep" + os.Getenv("SOMETHING")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo4(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo5() {
	foo4("sleep")
}

func foo6(a string, c string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	// ruleid: go_subproc_rule-subproc
	cmd = exec.Command(a)
	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo7() {
	foo6("ll", "ls")
}

func foo8() {
	err := syscall.Exec("/bin/cat", []string{"/etc/passwd"}, nil)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo9(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo10() {
	foo9("sleep")
}

func foo11(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo12() {
	foo11("sleep")
}

func foo13() {
	run := "sleep"
	cmd := exec.Command(run, "5")
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo14() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", getParam(r), nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"log"
	"net"
)

func Unsafe() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_PortOmitted() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_AddressOmitted() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_IPv6() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_Ipv6PortOmitted() {
	// ruleid:go_network_rule-bind-to-all-interfaces
	l, err := net.Listen("tcp6", "[::]")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Safe() {
	// ok:go_network_rule-bind-to-all-interfaces
	l, err := net.Listen("tcp", "127.0.0.1:1234")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"time"
)

func bad1() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	log.Fatal(err)
}

func bad2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	log.Fatal(err)
}

func bad3() {
	l, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	log.Fatal(err)
}

func bad4() {
	l, err := net.Listen("tcp", ":8443")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	log.Fatal(err)
}

func bad5() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	})
	// ruleid:go_http_rule-http-serve
	err := (&http.Server{
		Addr: ":1234",
	}).ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func ok1() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	})
	// ok:go_http_rule-http-serve
	server := &http.Server{
		Addr:        ":1234",
		ReadTimeout: 1 * time.Second,
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func ok2() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	})
	// ok:go_http_rule-http-serve
	server := &http.Server{
		Addr:              ":1234",
		ReadHeaderTimeout: 3 * time.Second,
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func ok3() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	})
	// ok:go_http_rule-http-serve
	server := &http.Server{
		Addr:              ":1234",
		ReadHeaderTimeout: 3 * time.Second,
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

func ok4() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
	})
	// ok:go_http_rule-http-serve
	server := &http.Server{
		Addr:        ":1234",
		ReadTimeout: 1 * time.Second,
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
)

func dbUnsafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	unsafe1 := "select * from foo where bar = '" + tainted + "'"
	unsafe2 := "select * from foo where id = " + tainted
	unsafe3 := fmt.Sprintf("select * from foo where bar = %q", tainted)

	unsafe4 := strings.Builder{}
	unsafe4.WriteString("select * from foo where bar = ")
	unsafe4.WriteString(tainted)

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

	// ruleid:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = '" + tainted + "'")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func dbSafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = '" + "safe" + "'"
	safe2 := "select * from foo where id = " + "safe"
	safe3 := fmt.Sprintf("select * from foo where bar = %q", "safe")

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe3)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = '" + "safe" + "'")
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES('"+"safe"+"')")
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = '" + "safe" + "' WHERE id = 100")
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = '"+"safe"+"'")
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values('" + "safe" + "')")
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = '"+"safe"+"' where id = 100")
}

func dbSafe2(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = ?"
	safe2 := "select * from foo where bar is $1 and baz = $2"

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1, tainted)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2, true, tainted)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = ? WHERE id = 100", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = $2 and id = $1", "baz", tainted)
}

// False-positive reported by customer (see gitlab-org/gitlab#451108).
func safeIssue_451108(ctx context.Context, val string) error {
	if err := somepkg.validate(val); err != nil {
		return &somepkg.Error{Message: err.Error()}
	}

	if !updateAllowed(somepkg.Status) {
		// ok:go_sql_rule-concat-sqli
		return &somepkg.Error{Message: fmt.Sprintf("update is not permitted: %v", somepkg.Status)}
	}

	return nil
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"log"
	"net"
)

func Unsafe() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_PortOmitted() {
	// ruleid:go_network_rule-bind-to-all-interfaces
	l, err := net.Listen("tcp", "0.0.0.0")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_AddressOmitted() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_IPv6() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Unsafe_Ipv6PortOmitted() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func Safe() {
	// ok:go_network_rule-bind-to-all-interfaces
	l, err := net.Listen("tcp", "127.0.0.1:1234")
	if err != nil {
		log.Fatal(err)
	}
	defer l.Close()
}

func TestCacheOperation(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)

	tests := map[string]cacheOperationTest{
		"error-on-minio-client-initialization": {
			errorOnMinioClientInitialization: true,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning: true,
			presignedURL:         URL,
			expectedURL:          nil,
		},
		"presigned-url": {
			presignedURL: URL,
			expectedURL:  URL,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
		})
	}
}

func TestCacheUploadEnv(t *testing.T) {
	tests := map[string]struct {
		key                   string
		cacheConfig           *common.CacheConfig
		createCacheAdapter    bool
		createCacheAdapterErr error
		getUploadEnvResult    interface{}
		expectedEnvs          map[string]string
		expectedLogEntry      string
	}{
		"full map": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: map[string]string{"TEST1": "123", "TEST2": "456"},
			expectedEnvs:       map[string]string{"TEST1": "123", "TEST2": "456"},
		},
		"nil": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no cache config": {
			key:                "key",
			cacheConfig:        nil,
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no key": {
			key:                "",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter not exists": {
			cacheConfig:        &common.CacheConfig{Type: "doesnt-exist"},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter creation error": {
			cacheConfig:           &common.CacheConfig{Type: "test"},
			createCacheAdapter:    true,
			createCacheAdapterErr: errors.New("test error"),
			getUploadEnvResult:    nil,
			expectedEnvs:          nil,
			expectedLogEntry:      `Could not create cache adapter" error="test error`,
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			cacheAdapter := new(MockAdapter)
			defer cacheAdapter.AssertExpectations(t)

			logs, cleanUpHooks := hook.NewHook()
			defer cleanUpHooks()

			build := &common.Build{
				Runner: &common.RunnerConfig{
					RunnerSettings: common.RunnerSettings{
						Cache: tc.cacheConfig,
					},
				},
			}

			oldCreateAdapter := createAdapter
			createAdapter = func(_ *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
				if tc.createCacheAdapter {
					return cacheAdapter, tc.createCacheAdapterErr
				}

				return nil, nil
			}
			defer func() {
				createAdapter = oldCreateAdapter
			}()

			if tc.cacheConfig != nil && tc.createCacheAdapter {
				cacheAdapter.On("GetUploadEnv", mock.Anything).Return(tc.getUploadEnvResult, nil)
			}

			envs, err := GetCacheUploadEnv(context.Background(), build, "key")
			assert.NoError(t, err)
			assert.Equal(t, tc.expectedEnvs, envs)

			if tc.expectedLogEntry != "" {
				lastLogMsg, err := logs.LastEntry().String()
				require.NoError(t, err)
				assert.Contains(t, lastLogMsg, tc.expectedLogEntry)
			}
		})
	}
}

func TestGlobbedFilePath(t *testing.T) {
	// Set up directories used in all test cases
	const (
		fileArchiverGlobPath  = "foo/bar/baz"
		fileArchiverGlobPath2 = "foo/bar/baz2"
	)
	err := os.MkdirAll(fileArchiverGlobPath, 0700)
	require.NoError(t, err, "Creating directory path: %s", fileArchiverGlobPath)
	defer os.RemoveAll(strings.Split(fileArchiverGlobPath, "/")[0])

	err = os.MkdirAll(fileArchiverGlobPath2, 0700)
	require.NoError(t, err, "Creating directory path: %s", fileArchiverGlobPath2)
	defer os.RemoveAll(strings.Split(fileArchiverGlobPath2, "/")[0])

	// Write a dir that is outside any glob patterns
	const (
		fileArchiverGlobNonMatchingPath = "bar/foo"
	)
	err = os.MkdirAll(fileArchiverGlobNonMatchingPath, 0700)
	writeTestFile(t, "bar/foo/test.txt")
	require.NoError(t, err, "Creating directory path: %s", fileArchiverGlobNonMatchingPath)
	defer os.RemoveAll(strings.Split(fileArchiverGlobNonMatchingPath, "/")[0])

	workingDirectory, err := os.Getwd()
	require.NoError(t, err)

	testCases := map[string]struct {
		paths   []string
		exclude []string

		// files that will be created and matched by the patterns
		expectedMatchingFiles []string

		// directories that will be matched by the patterns
		expectedMatchingDirs []string

		// files that will be created but will not be matched
		nonMatchingFiles []string

		// directories that will not be matched by the patterns
		nonMatchingDirs []string

		// files that are excluded by Exclude patterns
		excludedFilesCount int64

		warningLog string
	}{
		"find nothing with empty path": {
			paths: []string{""},
			nonMatchingFiles: []string{
				"file.txt",
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz/file.extra.dots.txt",
			},
			warningLog: "No matching files. Path is empty.",
		},
		"files by extension at several depths": {
			paths: []string{"foo/**/*.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz/file.extra.dots.txt",
			},
			nonMatchingFiles: []string{
				"foo/file.txt.md",
				"foo/bar/file.txt.md",
				"foo/bar/baz/file.txt.md",
				"foo/bar/baz/file.extra.dots.txt.md",
			},
		},
		"files by extension at several depths - with exclude": {
			paths:   []string{"foo/**/*.txt"},
			exclude: []string{"foo/**/xfile.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/xfile.txt",
				"foo/bar/xfile.txt",
				"foo/bar/baz/xfile.txt",
			},
			excludedFilesCount: 3,
		},
		"double slash matches a single slash": {
			paths: []string{"foo//*.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
			},
		},
		"double slash matches a single slash - with exclude": {
			paths:   []string{"foo//*.txt"},
			exclude: []string{"foo//*2.txt"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file2.txt",
				"foo/bar/file.txt",
			},
			excludedFilesCount: 1,
		},
		"absolute path to working directory": {
			paths: []string{filepath.Join(workingDirectory, "*.thing")},
			expectedMatchingFiles: []string{
				"file.thing",
			},
			nonMatchingFiles: []string{
				"foo/file.thing",
				"foo/bar/file.thing",
				"foo/bar/baz/file.thing",
			},
		},
		"absolute path to working directory - with exclude": {
			paths:   []string{filepath.Join(workingDirectory, "*.thing")},
			exclude: []string{filepath.Join(workingDirectory, "*2.thing")},
			expectedMatchingFiles: []string{
				"file.thing",
			},
			nonMatchingFiles: []string{
				"file2.thing",
			},
			excludedFilesCount: 1,
		},
		"absolute path to nested directory": {
			paths: []string{filepath.Join(workingDirectory, "foo/bar/*.bin")},
			expectedMatchingFiles: []string{
				"foo/bar/file.bin",
			},
			nonMatchingFiles: []string{
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
			},
		},
		"absolute path to nested directory - with exclude": {
			paths:   []string{filepath.Join(workingDirectory, "foo/bar/*.bin")},
			exclude: []string{filepath.Join(workingDirectory, "foo/bar/*2.bin")},
			expectedMatchingFiles: []string{
				"foo/bar/file.bin",
			},
			nonMatchingFiles: []string{
				"foo/bar/file2.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
			},
			excludedFilesCount: 1,
		},
		"double slash and multiple stars - must be at least two dirs deep": {
			paths: []string{"./foo/**//*/*.*"},
			expectedMatchingFiles: []string{
				"foo/bar/file.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.bin",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file.txt",
			},
		},
		"double slash and multiple stars - must be at least two dirs deep - with exclude": {
			paths:   []string{"./foo/**//*/*.*"},
			exclude: []string{"**/*.bin"},
			expectedMatchingFiles: []string{
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.bin",
				"foo/bar/baz/file.bin",
				"foo/bar/baz2/file.bin",
			},
			excludedFilesCount: 3,
		},
		"all the files": {
			paths: []string{"foo/**/*.*"},
			expectedMatchingFiles: []string{
				"foo/file.bin",
				"foo/file.txt",
				"foo/bar/file.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.bin",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{},
		},
		"all the files - with exclude": {
			paths:   []string{"foo/**/*.*"},
			exclude: []string{"**/*.bin", "**/*even-this*"},
			expectedMatchingFiles: []string{
				"foo/file.txt",
				"foo/bar/file.txt",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/wow-even-this.go",
				"foo/file.bin",
				"foo/bar/file.bin",
				"foo/bar/baz/file.bin",
				"foo/bar/baz2/file.bin",
			},
			excludedFilesCount: 5,
		},
		"all the things - dirs included": {
			paths: []string{"foo/**"},
			expectedMatchingFiles: []string{
				"foo/file.bin",
				"foo/file.txt",
				"foo/bar/file.bin",
				"foo/bar/file.txt",
				"foo/bar/baz/file.bin",
				"foo/bar/baz/file.txt",
				"foo/bar/baz2/file.bin",
				"foo/bar/baz2/file.txt",
			},
			expectedMatchingDirs: []string{
				"foo",
				"foo/bar",
				"foo/bar/baz",
				"foo/bar/baz2",
			},
			nonMatchingFiles: []string{
				"root.txt",
			},
		},
		"relative path that leaves project and returns": {
			paths: []string{filepath.Join("..", filepath.Base(workingDirectory), "foo/*.txt")},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
		},
		"relative path that leaves project and returns - with exclude": {
			paths:   []string{filepath.Join("..", filepath.Base(workingDirectory), "foo/*.txt")},
			exclude: []string{filepath.Join("..", filepath.Base(workingDirectory), "foo/*2.txt")},
			expectedMatchingFiles: []string{
				"foo/file.txt",
			},
			nonMatchingFiles: []string{
				"foo/file2.txt",
			},
			excludedFilesCount: 1,
		},
		"invalid path": {
			paths:      []string{">/**"},
			warningLog: "no matching files. Ensure that the artifact path is relative to the working directory",
		},
		"cancel out everything": {
			paths:      []string{"**"},
			exclude:    []string{"**"},
			warningLog: "no matching files. Ensure that the artifact path is relative to the working directory",
		},
	}

	for testName, tc := range testCases {
		t.Run(testName, func(t *testing.T) {
			h := newLogHook(logrus.WarnLevel)
			logrus.AddHook(&h)

			for _, f := range tc.expectedMatchingFiles {
				writeTestFile(t, f)
			}
			for _, f := range tc.nonMatchingFiles {
				writeTestFile(t, f)
			}

			f := fileArchiver{
				Paths:   tc.paths,
				Exclude: tc.exclude,
			}
			err = f.enumerate()
			assert.NoError(t, err)

			sortedFiles := f.sortedFiles()
			assert.Len(t, sortedFiles, len(tc.expectedMatchingFiles)+len(tc.expectedMatchingDirs))
			for _, p := range tc.expectedMatchingFiles {
				assert.Contains(t, f.sortedFiles(), p)
			}
			for _, p := range tc.expectedMatchingDirs {
				assert.Contains(t, f.sortedFiles(), p)
			}

			var excludedFilesCount int64
			for _, v := range f.excluded {
				excludedFilesCount += v
			}
			if tc.excludedFilesCount > 0 {
				assert.Equal(t, tc.excludedFilesCount, excludedFilesCount)
			}

			if tc.warningLog != "" {
				require.Len(t, h.entries, 1)
				assert.Contains(t, h.entries[0].Message, tc.warningLog)
			}

			// remove test files from this test case
			// deferred removal will still happen if needed in the os.RemoveAll call above
			for _, f := range tc.expectedMatchingFiles {
				removeTestFile(t, f)
			}
			for _, f := range tc.nonMatchingFiles {
				removeTestFile(t, f)
			}
		})
	}
}

func Test_loadConfig(t *testing.T) {
	const expectedSystemIDRegexPattern = "^[sr]_[0-9a-zA-Z]{12}$"

	testCases := map[string]struct {
		runnerSystemID string
		prepareFn      func(t *testing.T, systemIDFile string)
		assertFn       func(t *testing.T, err error, config *common.Config, systemIDFile string)
	}{
		"generates and saves missing system IDs": {
			runnerSystemID: "",
			assertFn: func(t *testing.T, err error, config *common.Config, systemIDFile string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.NotEmpty(t, config.Runners[0].SystemIDState.GetSystemID())
				content, err := os.ReadFile(systemIDFile)
				require.NoError(t, err)
				assert.Contains(t, string(content), config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"preserves existing unique system IDs": {
			runnerSystemID: "s_c2d22f638c25",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Equal(t, "s_c2d22f638c25", config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"regenerates system ID if file is invalid": {
			runnerSystemID: "0123456789",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"succeeds if file cannot be created": {
			runnerSystemID: "",
			prepareFn: func(t *testing.T, systemIDFile string) {
				require.NoError(t, os.Remove(systemIDFile))
				require.NoError(t, os.Chmod(filepath.Dir(systemIDFile), os.ModeDir|0500))
			},
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				require.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			dir := t.TempDir()
			cfgName := filepath.Join(dir, "config.toml")
			systemIDFile := filepath.Join(dir, ".runner_system_id")

			require.NoError(t, os.Chmod(dir, 0777))
			require.NoError(t, os.WriteFile(cfgName, []byte("[[runners]]\n name = \"runner\""), 0777))
			require.NoError(t, os.WriteFile(systemIDFile, []byte(tc.runnerSystemID), 0777))

			if tc.prepareFn != nil {
				tc.prepareFn(t, systemIDFile)
			}

			c := configOptions{ConfigFile: cfgName}
			err := c.loadConfig()

			tc.assertFn(t, err, c.config, systemIDFile)

			// Cleanup
			require.NoError(t, os.Chmod(dir, 0777))
		})
	}
}

func TestBuildsHelper_evaluateJobQueuingDuration(t *testing.T) {
	const (
		testToken = "testoken" // No typo here! 8 characters to make it equal to the computed ShortDescription()
	)

	type jobInfo struct {
		timeInQueueSeconds                       float64
		projectJobsRunningOnInstanceRunnersCount string
	}

	basicJob := jobInfo{
		timeInQueueSeconds:                       (15 * time.Second).Seconds(),
		projectJobsRunningOnInstanceRunnersCount: "0",
	}

	tc := map[string]struct {
		monitoringSectionMissing bool
		jobQueuingSectionMissing bool
		threshold                time.Duration
		jobsRunningForProject    string
		jobInfo                  jobInfo
		expectedValue            float64
	}{
		"no monitoring section in configuration": {
			monitoringSectionMissing: true,
			jobInfo:                  basicJob,
			expectedValue:            0,
		},
		"no jobQueuingDuration section in configuration": {
			jobQueuingSectionMissing: true,
			jobInfo:                  basicJob,
			expectedValue:            0,
		},
		"zeroed configuration": {
			jobInfo:       basicJob,
			expectedValue: 0,
		},
		"jobsRunningForProject not configured and threshold not exceeded": {
			threshold:     60 * time.Second,
			jobInfo:       basicJob,
			expectedValue: 0,
		},
		"jobsRunningForProject not configured and threshold exceeded": {
			threshold:     10 * time.Second,
			jobInfo:       basicJob,
			expectedValue: 1,
		},
		"jobsRunningForProject configured and matched and threshold not exceeded": {
			threshold:             60 * time.Second,
			jobsRunningForProject: ".*",
			jobInfo:               basicJob,
			expectedValue:         0,
		},
		"jobsRunningForProject configured and matched and threshold exceeded": {
			threshold:             10 * time.Second,
			jobsRunningForProject: ".*",
			jobInfo:               basicJob,
			expectedValue:         1,
		},
		"jobsRunningForProject configured and not matched and threshold not exceeded": {
			threshold:             60 * time.Second,
			jobsRunningForProject: "Inf+",
			jobInfo:               basicJob,
			expectedValue:         0,
		},
		"jobsRunningForProject configured and not matched and threshold exceeded": {
			threshold:             10 * time.Second,
			jobsRunningForProject: "Inf+",
			jobInfo:               basicJob,
			expectedValue:         0,
		},
	}

	for tn, tt := range tc {
		t.Run(tn, func(t *testing.T) {
			build := &common.Build{
				Runner: &common.RunnerConfig{
					RunnerCredentials: common.RunnerCredentials{
						Token: testToken,
					},
					SystemIDState: &common.SystemIDState{},
				},
				JobResponse: common.JobResponse{
					ID: 1,
					JobInfo: common.JobInfo{
						ProjectID:                                1,
						TimeInQueueSeconds:                       tt.jobInfo.timeInQueueSeconds,
						ProjectJobsRunningOnInstanceRunnersCount: tt.jobInfo.projectJobsRunningOnInstanceRunnersCount,
					},
				},
			}

			require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

			if !tt.monitoringSectionMissing {
				build.Runner.Monitoring = &runner.Monitoring{}

				if !tt.jobQueuingSectionMissing {
					build.Runner.Monitoring.JobQueuingDurations = monitoring.JobQueuingDurations{
						&monitoring.JobQueuingDuration{
							Periods:               []string{"* * * * * * *"},
							Threshold:             tt.threshold,
							JobsRunningForProject: tt.jobsRunningForProject,
						},
					}
				}
				require.NoError(t, build.Runner.Monitoring.Compile())
			}

			b := newBuildsHelper()
			b.addBuild(build)

			ch := make(chan prometheus.Metric, 1)
			b.acceptableJobQueuingDurationExceeded.Collect(ch)

			m := <-ch

			var mm dto.Metric
			err := m.Write(&mm)
			require.NoError(t, err)

			labels := make(map[string]string)
			for _, l := range mm.GetLabel() {
				if !assert.NotNil(t, l.Name) {
					continue
				}

				if !assert.NotNil(t, l.Value) {
					continue
				}

				labels[*l.Name] = *l.Value
			}

			assert.Len(t, labels, 2)
			require.Contains(t, labels, "runner")
			assert.Equal(t, testToken, labels["runner"])
			require.Contains(t, labels, "system_id")
			assert.Equal(t, build.Runner.SystemIDState.GetSystemID(), labels["system_id"])

			assert.Equal(t, tt.expectedValue, mm.GetCounter().GetValue())
		})
	}
}

func TestBuildsHelperCollect(t *testing.T) {
	dir := t.TempDir()

	ch := make(chan prometheus.Metric, 50)
	b := newBuildsHelper()

	longRunningBuild, err := common.GetLongRunningBuild()
	require.NoError(t, err)

	shell := "bash"
	if runtime.GOOS == "windows" {
		shell = "powershell"
	}

	build := &common.Build{
		JobResponse: longRunningBuild,
		Runner: &common.RunnerConfig{
			RunnerSettings: common.RunnerSettings{
				BuildsDir: dir,
				Executor:  "shell",
				Shell:     shell,
			},
			SystemIDState: common.NewSystemIDState(),
		},
	}
	trace := &common.Trace{Writer: io.Discard}

	require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

	done := make(chan error)
	go func() {
		done <- buildtest.RunBuildWithTrace(t, build, trace)
	}()

	b.builds = append(b.builds, build)
	// collect many logs whilst the build is being executed to trigger any
	// potential race conditions that arise from the build progressing whilst
	// metrics are collected.
	for i := 0; i < 200; i++ {
		if i == 100 {
			// Build might have not started yet, wait until cancel is
			// successful.
			require.Eventually(
				t,
				func() bool {
					return trace.Abort()
				},
				time.Minute,
				10*time.Millisecond,
			)
		}
		b.Collect(ch)
		<-ch
	}

	err = <-done
	expected := &common.BuildError{FailureReason: common.JobCanceled}
	assert.ErrorIs(t, err, expected)
}

func TestBuildsHelperFindSessionByURL(t *testing.T) {
	sess, err := session.NewSession(nil)
	require.NoError(t, err)
	build := common.Build{
		Session: sess,
		Runner: &common.RunnerConfig{
			RunnerCredentials: common.RunnerCredentials{
				Token: "abcd1234",
			},
			SystemIDState: common.NewSystemIDState(),
		},
	}

	require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

	h := newBuildsHelper()
	h.addBuild(&build)

	foundSession := h.findSessionByURL(sess.Endpoint + "/action")
	assert.Equal(t, sess, foundSession)

	foundSession = h.findSessionByURL("/session/hash/action")
	assert.Nil(t, foundSession)
}

func TestAccountKeySigning(t *testing.T) {
	tests := map[string]azureSigningTest{
		"missing account name": {
			accountKey:                accountKey,
			containerName:             "test-container",
			method:                    http.MethodGet,
			expectedErrorOnGeneration: true,
		},
		"missing account key": {
			accountName:               accountName,
			containerName:             "test-container",
			method:                    http.MethodGet,
			expectedErrorOnGeneration: true,
		},
		"GET request": {
			accountName:        accountName,
			accountKey:         accountKey,
			containerName:      "test-container",
			method:             http.MethodGet,
			expectedServiceURL: "https://azuretest.blob.core.windows.net",
		},
		"GET request in custom storage domain": {
			accountName:        accountName,
			accountKey:         accountKey,
			storageDomain:      "blob.core.chinacloudapi.cn",
			containerName:      "test-container",
			method:             http.MethodGet,
			expectedServiceURL: "https://azuretest.blob.core.chinacloudapi.cn",
		},
		"PUT request": {
			accountName:        accountName,
			accountKey:         accountKey,
			containerName:      "test-container",
			method:             http.MethodPut,
			expectedServiceURL: "https://azuretest.blob.core.windows.net",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			credentials := &common.CacheAzureCredentials{
				AccountName: tt.accountName,
				AccountKey:  tt.accountKey,
			}
			config := &common.CacheAzureConfig{
				CacheAzureCredentials: *credentials,
				ContainerName:         tt.containerName,
				StorageDomain:         tt.storageDomain,
			}
			opts := &signedURLOptions{
				ContainerName: containerName,
				Method:        tt.method,
				Timeout:       1 * time.Hour,
			}

			signer, err := newAccountKeySigner(config)

			if tt.expectedErrorOnGeneration {
				assert.Error(t, err)
				return
			}

			require.NoError(t, err)
			assert.Equal(t, tt.expectedServiceURL, signer.ServiceURL())

			opts.Signer = signer
			token, err := getSASToken(context.Background(), objectName, opts)
			require.NoError(t, err)

			q, err := url.ParseQuery(token)
			require.NoError(t, err)
			assert.Equal(t, q.Encode(), token)

			// Sanity check query parameters from
			// https://docs.microsoft.com/en-us/rest/api/storageservices/create-service-sas
			assert.NotNil(t, q["sv"])                    // SignedVersion
			assert.Equal(t, []string{"b"}, q["sr"])      // SignedResource (blob)
			assert.NotNil(t, q["st"])                    // SignedStart
			assert.NotNil(t, q["se"])                    // SignedExpiry
			assert.NotNil(t, q["sig"])                   // Signature
			assert.Equal(t, []string{"https"}, q["spr"]) // SignedProtocol

			// SignedPermission
			expectedPermissionValue := "w"
			if tt.method == http.MethodGet {
				expectedPermissionValue = "r"
			}
			assert.Equal(t, []string{expectedPermissionValue}, q["sp"])
		})
	}
}

func TestAdapterOperation(t *testing.T) {
	tests := map[string]adapterOperationTestCase{
		"error-on-URL-signing": {
			returnedURL:   "",
			returnedError: fmt.Errorf("test error"),
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while generating GCS pre-signed URL: test error")
			},
			signBlobAPITest: false,
		},
		"invalid-URL-returned": {
			returnedURL:   "://test",
			returnedError: nil,
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while parsing generated URL: parse")
				assert.Contains(t, message, "://test")
				assert.Contains(t, message, "missing protocol scheme")
			},
			signBlobAPITest: false,
		},
		"valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    false,
		},
		"sign-blob-api-valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    true,
		},
		"max-cache-archive-size": {
			returnedURL:            "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:          nil,
			assertErrorMessage:     nil,
			signBlobAPITest:        false,
			maxUploadedArchiveSize: maxUploadedArchiveSize,
			expectedHeaders:        http.Header{"X-Goog-Content-Length-Range": []string{"0,100"}},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			config := defaultGCSCache()

			config.MaxUploadedArchiveSize = tc.maxUploadedArchiveSize

			a, err := New(config, defaultTimeout, objectName)
			require.NoError(t, err)

			adapter, ok := a.(*gcsAdapter)
			require.True(t, ok, "Adapter should be properly casted to *adapter type")

			testAdapterOperation(
				t,
				tc,
				"GetDownloadURL",
				http.MethodGet,
				"",
				adapter,
				a.GetDownloadURL,
			)
			testAdapterOperation(
				t,
				tc,
				"GetUploadURL",
				http.MethodPut,
				"application/octet-stream",
				adapter,
				a.GetUploadURL,
			)

			headers := adapter.GetUploadHeaders()
			assert.Equal(t, headers, tc.expectedHeaders)

			assert.Nil(t, adapter.GetGoCloudURL(context.Background()))
			env, err := adapter.GetUploadEnv(context.Background())
			assert.NoError(t, err)
			assert.Empty(t, env)
		})
	}
}

func TestRegisterTokenExpiresAt(t *testing.T) {
	type testCase struct {
		token          string
		expiration     time.Time
		expectedConfig string
	}

	testCases := map[string]testCase{
		"no expiration": {
			token: "test-runner-token",
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration": {
			token:      "test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
		"no expiration with authentication token": {
			token: "glrt-test-runner-token",
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration with authentication token": {
			token:      "glrt-test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:             12345,
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, []kv{}, "", "--registration-token", tc.token, "--name", "test-runner")
			require.NoError(t, err)

			assert.Contains(
				t, spaceReplacer.Replace(gotConfig),
				spaceReplacer.Replace(fmt.Sprintf(tc.expectedConfig, commands.RegisterTimeNowDate.Format(time.RFC3339))),
			)
		})
	}
}

func domainExportCleanupBegin() error {
	go func() {
		for {
			statement := `
				DELETE FROM exports
				WHERE creationDate < $1;
			`
			_, err := db.Exec(statement, time.Now().UTC().AddDate(0, 0, -7))
			if err != nil {
				logger.Errorf("error cleaning up export rows: %v", err)
				return
			}

			time.Sleep(2 * time.Hour)
		}
	}()

	return nil
}

func TestRunnerByURLAndID(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerURL     string
		runnerID      int64
		expectedIndex int
		expectedError error
	}{
		"finds runner by name": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      1,
			expectedIndex: 0,
		},
		"does not find runner with wrong ID": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab1.example.com/",
			runnerID:      3,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab1.example.com/" and ID 3`),
		},
		"does not find runner with wrong URL": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  1,
						URL: "https://gitlab1.example.com/",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						ID:  2,
						URL: "https://gitlab1.example.com/",
					},
				},
			},
			runnerURL:     "https://gitlab2.example.com/",
			runnerID:      1,
			expectedIndex: -1,
			expectedError: fmt.Errorf(`could not find a runner with the URL "https://gitlab2.example.com/" and ID 1`),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByURLAndID(tt.runnerURL, tt.runnerID)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestHealthCheckCommand_WaitAll(t *testing.T) {
	// We might simulate as many as two services.
	const MAX_PORTS = 2

	cases := []struct {
		name            string
		successCount    int
		expectedTimeout bool
	}{
		{
			name:            "Two services down",
			successCount:    0,
			expectedTimeout: true,
		},
		{
			name:            "One up one down",
			successCount:    1,
			expectedTimeout: true,
		},
		{
			name:            "Two services up",
			successCount:    2,
			expectedTimeout: false,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {

			ports := make([]string, 0)

			for i := 0; i < c.successCount; i++ {
				listener, err := net.Listen("tcp", "127.0.0.1:")
				require.NoError(t, err)
				defer listener.Close()

				port := listener.Addr().(*net.TCPAddr).Port
				ports = append(ports, strconv.Itoa(port))
			}

			// To simulate services that are down, find an unused port and increment port
			// numbers from there.
			unusedPort := 0
			if c.successCount < MAX_PORTS {
				listener, err := net.Listen("tcp", "127.0.0.1:")
				require.NoError(t, err)

				unusedPort = listener.Addr().(*net.TCPAddr).Port
				listener.Close()
			}

			for i := c.successCount; i < MAX_PORTS; i++ {
				ports = append(ports, strconv.Itoa(unusedPort))
				unusedPort++
			}

			// The cli package provides an extra value at end of the args array
			ports = append(ports, "[unused value]")

			ctx, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
			defer cancelFn()
			done := make(chan struct{})
			go func() {
				cmd := HealthCheckCommand{
					ctx:   ctx,
					Ports: ports,
				}
				cmd.Execute(nil)
				done <- struct{}{}
			}()

			select {
			case <-ctx.Done():
				if !c.expectedTimeout {
					require.Fail(t, "Unexpected timeout")
				}
			case <-done:
				if c.expectedTimeout {
					require.Fail(t, "Unexpected failure to time out")
				}
			}
		})
	}
}

func TestAdapterOperation(t *testing.T) {
	tests := map[string]adapterOperationTestCase{
		"error-on-URL-signing": {
			returnedURL:   "",
			returnedError: fmt.Errorf("test error"),
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while generating GCS pre-signed URL: test error")
			},
			signBlobAPITest: false,
		},
		"invalid-URL-returned": {
			returnedURL:   "://test",
			returnedError: nil,
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while parsing generated URL: parse")
				assert.Contains(t, message, "://test")
				assert.Contains(t, message, "missing protocol scheme")
			},
			signBlobAPITest: false,
		},
		"valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    false,
		},
		"sign-blob-api-valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    true,
		},
		"max-cache-archive-size": {
			returnedURL:            "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:          nil,
			assertErrorMessage:     nil,
			signBlobAPITest:        false,
			maxUploadedArchiveSize: maxUploadedArchiveSize,
			expectedHeaders:        http.Header{"X-Goog-Content-Length-Range": []string{"0,100"}},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			config := defaultGCSCache()

			config.MaxUploadedArchiveSize = tc.maxUploadedArchiveSize

			a, err := New(config, defaultTimeout, objectName)
			require.NoError(t, err)

			adapter, ok := a.(*gcsAdapter)
			require.True(t, ok, "Adapter should be properly casted to *adapter type")

			testAdapterOperation(
				t,
				tc,
				"GetDownloadURL",
				http.MethodGet,
				"",
				adapter,
				a.GetDownloadURL,
			)
			testAdapterOperation(
				t,
				tc,
				"GetUploadURL",
				http.MethodPut,
				"application/octet-stream",
				adapter,
				a.GetUploadURL,
			)

			headers := adapter.GetUploadHeaders()
			assert.Equal(t, headers, tc.expectedHeaders)

			assert.Nil(t, adapter.GetGoCloudURL(context.Background()))
			env, err := adapter.GetUploadEnv(context.Background())
			assert.NoError(t, err)
			assert.Empty(t, env)
		})
	}
}

func TestArtifactsDownloader(t *testing.T) {
	testCases := map[string]struct {
		downloadState                common.DownloadState
		directDownload               bool
		stagingDir                   string
		expectedSuccess              bool
		expectedDownloadCalled       int
		expectedDirectDownloadCalled int
	}{
		"download not found": {
			downloadState:          common.DownloadNotFound,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download forbidden": {
			downloadState:          common.DownloadForbidden,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download unauthorized": {
			downloadState:          common.DownloadUnauthorized,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"retries are called": {
			downloadState:          common.DownloadFailed,
			expectedSuccess:        false,
			expectedDownloadCalled: 3,
		},
		"first try is always direct download": {
			downloadState:                common.DownloadFailed,
			directDownload:               true,
			expectedSuccess:              false,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       3,
		},
		"downloads artifact without direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               false,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 0,
			expectedDownloadCalled:       1,
		},
		"downloads artifact with direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               true,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       1,
		},
		"setting invalid staging directory": {
			downloadState: common.DownloadSucceeded,
			stagingDir:    "/dev/null",
		},
	}

	removeHook := helpers.MakeFatalToPanic()
	defer removeHook()

	// ensure clean state
	os.Remove(artifactsTestArchivedFile)

	for testName, testCase := range testCases {
		OnEachZipArchiver(t, func(t *testing.T) {
			t.Run(testName, func(t *testing.T) {
				network := &testNetwork{
					downloadState: testCase.downloadState,
				}
				cmd := ArtifactsDownloaderCommand{
					JobCredentials: downloaderCredentials,
					DirectDownload: testCase.directDownload,
					network:        network,
					retryHelper: retryHelper{
						Retry: 2,
					},
					StagingDir: testCase.stagingDir,
				}

				// file is cleaned after running test
				defer os.Remove(artifactsTestArchivedFile)

				if testCase.expectedSuccess {
					require.NotPanics(t, func() {
						cmd.Execute(nil)
					})

					assert.FileExists(t, artifactsTestArchivedFile)
				} else {
					require.Panics(t, func() {
						cmd.Execute(nil)
					})
				}

				assert.Equal(t, testCase.expectedDirectDownloadCalled, network.directDownloadCalled)
				assert.Equal(t, testCase.expectedDownloadCalled, network.downloadCalled)
			})
		})
	}
}

func ssoTokenCleanupBegin() error {
	go func() {
		for {
			statement := `
				DELETE FROM ssoTokens
				WHERE creationDate < $1;
			`
			_, err := db.Exec(statement, time.Now().UTC().Add(time.Duration(-10)*time.Minute))
			if err != nil {
				logger.Errorf("error cleaning up export rows: %v", err)
				return
			}

			time.Sleep(10 * time.Minute)
		}
	}()

	return nil
}

func TestRegisterCommand(t *testing.T) {
	type testCase struct {
		condition       func() bool
		token           string
		arguments       []string
		environment     []kv
		expectedConfigs []string
	}

	testCases := map[string]testCase{
		"runner ID is included in config": {
			token: "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--name", "test-runner",
			},
			expectedConfigs: []string{`id = 12345`, `token = "glrt-test-runner-token"`},
		},
		"registration token is accepted": {
			token: "test-runner-token",
			arguments: []string{
				"--registration-token", "test-runner-token",
				"--name", "test-runner",
			},
			expectedConfigs: []string{`id = 12345`, `token = "test-runner-token"`},
		},
		"authentication token is accepted in --registration-token": {
			token: "glrt-test-runner-token",
			arguments: []string{
				"--registration-token", "glrt-test-runner-token",
				"--name", "test-runner",
			},
			expectedConfigs: []string{`id = 12345`, `token = "glrt-test-runner-token"`},
		},
		"feature flags are included in config": {
			token: "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--name", "test-runner",
				"--feature-flags", "FF_TEST_1:true",
				"--feature-flags", "FF_TEST_2:false",
			},
			expectedConfigs: []string{`[runners.feature_flags]
		   FF_TEST_1 = true
		   FF_TEST_2 = false`},
		},
		"shell defaults to pwsh on Windows with shell executor": {
			condition: func() bool { return runtime.GOOS == osTypeWindows },
			token:     "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--name", "test-runner",
				"--executor", "shell",
			},
			expectedConfigs: []string{`shell = "pwsh"`},
		},
		"shell defaults to pwsh on Windows with docker-windows executor": {
			condition: func() bool { return runtime.GOOS == osTypeWindows },
			token:     "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--name", "test-runner",
				"--executor", "docker-windows",
				"--docker-image", "abc",
			},
			expectedConfigs: []string{`shell = "pwsh"`},
		},
		"shell can be overridden to powershell on Windows with shell executor": {
			condition: func() bool { return runtime.GOOS == osTypeWindows },
			token:     "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--name", "test-runner",
				"--executor", "shell",
				"--shell", "powershell",
			},
			expectedConfigs: []string{`shell = "powershell"`},
		},
		"shell can be overridden to powershell on Windows with docker-windows executor": {
			condition: func() bool { return runtime.GOOS == osTypeWindows },
			token:     "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--name", "test-runner",
				"--executor", "docker-windows",
				"--shell", "powershell",
				"--docker-image", "abc",
			},
			expectedConfigs: []string{`shell = "powershell"`},
		},
		"kubernetes security context namespace": {
			token: "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--executor", "kubernetes",
			},
			environment: []kv{
				{
					key:   "KUBERNETES_BUILD_CONTAINER_SECURITY_CONTEXT_PRIVILEGED",
					value: "true",
				},
				{
					key:   "KUBERNETES_HELPER_CONTAINER_SECURITY_CONTEXT_RUN_AS_USER",
					value: "1000",
				},
				{
					key:   "KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_RUN_AS_NON_ROOT",
					value: "true",
				},
				{
					key:   "KUBERNETES_SERVICE_CONTAINER_SECURITY_CONTEXT_CAPABILITIES_ADD",
					value: "NET_RAW, NET_RAW1",
				},
			},
			expectedConfigs: []string{`
		[runners.kubernetes.build_container_security_context]
			privileged = true`, `
		[runners.kubernetes.helper_container_security_context]
			run_as_user = 1000`, `
		[runners.kubernetes.service_container_security_context]
			run_as_non_root = true`, `
      	[runners.kubernetes.service_container_security_context.capabilities]
        	add = ["NET_RAW, NET_RAW1"]`,
			},
		},
		"s3 cache AuthenticationType arg": {
			token: "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
				"--cache-s3-authentication_type=iam",
			},
			expectedConfigs: []string{`
		[runners.cache.s3]
			AuthenticationType = "iam"
			`},
		},
		"s3 cache AuthenticationType env": {
			token: "glrt-test-runner-token",
			arguments: []string{
				"--token", "glrt-test-runner-token",
			},
			environment: []kv{
				{
					key:   "CACHE_S3_AUTHENTICATION_TYPE",
					value: "iam",
				},
			},
			expectedConfigs: []string{`
		[runners.cache.s3]
			AuthenticationType = "iam"
			`},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			if tc.condition != nil && !tc.condition() {
				t.Skip()
			}

			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:    12345,
						Token: tc.token,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						ID:    12345,
						Token: tc.token,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, tc.environment, "", tc.arguments...)
			require.NoError(t, err)

			for _, expectedConfig := range tc.expectedConfigs {
				assert.Contains(t, spaceReplacer.Replace(gotConfig), spaceReplacer.Replace(expectedConfig))
			}
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"syscall"
)

func foo1() {
	err := exec.CommandContext(context.Background(), "git", "rev-parse", "--show-toplavel").Run()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo3() {
	run := "sleep" + os.Getenv("SOMETHING")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo4(command string) {
	// ruleid: go_subproc_rule-subproc
	cmd := exec.Command(command, "5")
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo5() {
	foo4("sleep")
}

func foo6(a string, c string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo7() {
	foo6("ll", "ls")
}

func foo8() {
	err := syscall.Exec("/bin/cat", []string{"/etc/passwd"}, nil)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo9(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo10() {
	foo9("sleep")
}

func foo11(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo12() {
	foo11("sleep")
}

func foo13() {
	run := "sleep"
	cmd := exec.Command(run, "5")
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo14() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

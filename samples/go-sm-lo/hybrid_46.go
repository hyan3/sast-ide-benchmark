func TestArtifactsDownloader(t *testing.T) {
	testCases := map[string]struct {
		downloadState                common.DownloadState
		directDownload               bool
		stagingDir                   string
		expectedSuccess              bool
		expectedDownloadCalled       int
		expectedDirectDownloadCalled int
	}{
		"download not found": {
			downloadState:          common.DownloadNotFound,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download forbidden": {
			downloadState:          common.DownloadForbidden,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"download unauthorized": {
			downloadState:          common.DownloadUnauthorized,
			expectedSuccess:        false,
			expectedDownloadCalled: 1,
		},
		"retries are called": {
			downloadState:          common.DownloadFailed,
			expectedSuccess:        false,
			expectedDownloadCalled: 3,
		},
		"first try is always direct download": {
			downloadState:                common.DownloadFailed,
			directDownload:               true,
			expectedSuccess:              false,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       3,
		},
		"downloads artifact without direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               false,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 0,
			expectedDownloadCalled:       1,
		},
		"downloads artifact with direct download if requested": {
			downloadState:                common.DownloadSucceeded,
			directDownload:               true,
			expectedSuccess:              true,
			expectedDirectDownloadCalled: 1,
			expectedDownloadCalled:       1,
		},
		"setting invalid staging directory": {
			downloadState: common.DownloadSucceeded,
			stagingDir:    "/dev/null",
		},
	}

	removeHook := helpers.MakeFatalToPanic()
	defer removeHook()

	// ensure clean state
	os.Remove(artifactsTestArchivedFile)

	for testName, testCase := range testCases {
		OnEachZipArchiver(t, func(t *testing.T) {
			t.Run(testName, func(t *testing.T) {
				network := &testNetwork{
					downloadState: testCase.downloadState,
				}
				cmd := ArtifactsDownloaderCommand{
					JobCredentials: downloaderCredentials,
					DirectDownload: testCase.directDownload,
					network:        network,
					retryHelper: retryHelper{
						Retry: 2,
					},
					StagingDir: testCase.stagingDir,
				}

				// file is cleaned after running test
				defer os.Remove(artifactsTestArchivedFile)

				if testCase.expectedSuccess {
					require.NotPanics(t, func() {
						cmd.Execute(nil)
					})

					assert.FileExists(t, artifactsTestArchivedFile)
				} else {
					require.Panics(t, func() {
						cmd.Execute(nil)
					})
				}

				assert.Equal(t, testCase.expectedDirectDownloadCalled, network.directDownloadCalled)
				assert.Equal(t, testCase.expectedDownloadCalled, network.downloadCalled)
			})
		})
	}
}

func defaultAzureCache() *common.CacheConfig {
	return &common.CacheConfig{
		Type: "azure",
		Azure: &common.CacheAzureConfig{
			CacheAzureCredentials: common.CacheAzureCredentials{
				AccountName: accountName,
				AccountKey:  accountKey,
			},
			ContainerName: containerName,
			StorageDomain: storageDomain,
		},
	}
}

func TestFetchCredentialsForRole(t *testing.T) {
	workingConfig := common.CacheConfig{
		S3: &common.CacheS3Config{
			AccessKey:          "test-access-key",
			SecretKey:          "test-secret-key",
			AuthenticationType: "access-key",
			BucketName:         "test-bucket",
			UploadRoleARN:      "arn:aws:iam::123456789012:role/TestRole",
		},
	}
	mockedCreds := map[string]string{
		"AWS_ACCESS_KEY_ID":     "mock-access-key",
		"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
		"AWS_SESSION_TOKEN":     "mock-session-token",
	}

	tests := map[string]struct {
		config           *common.CacheConfig
		roleARN          string
		expected         map[string]string
		errMsg           string
		expectedKms      bool
		duration         time.Duration
		expectedDuration time.Duration
	}{
		"successful fetch": {
			config:   &workingConfig,
			roleARN:  "arn:aws:iam::123456789012:role/TestRole",
			expected: mockedCreds,
		},
		"successful fetch with 12-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         12 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 12 * time.Hour,
		},
		"successful fetch with 10-minute timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         10 * time.Minute,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with 13-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         13 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with encryption": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:                 "test-access-key",
					SecretKey:                 "test-secret-key",
					AuthenticationType:        "access-key",
					BucketName:                "test-bucket",
					UploadRoleARN:             "arn:aws:iam::123456789012:role/TestRole",
					ServerSideEncryption:      "KMS",
					ServerSideEncryptionKeyID: "arn:aws:kms:us-west-2:123456789012:key/1234abcd-12ab-34cd-56ef-1234567890ab",
				},
			},
			roleARN:     "arn:aws:iam::123456789012:role/TestRole",
			expected:    mockedCreds,
			expectedKms: true,
		},
		"invalid role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
					UploadRoleARN:      "arn:aws:iam::123456789012:role/InvalidRole",
				},
			},
			roleARN: "arn:aws:iam::123456789012:role/InvalidRole",
			errMsg:  "failed to assume role",
		},
		"no role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
				},
			},
			expected: nil,
			errMsg:   "failed to assume role",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			duration := 3600
			if tt.duration > 0 {
				duration = int(tt.expectedDuration.Seconds())
			}
			// Create s3Client and point STS endpoint to it
			mockServer := httptest.NewServer(newMockSTSHandler(tt.expectedKms, duration))
			defer mockServer.Close()

			s3Client, err := newS3Client(tt.config.S3, withSTSEndpoint(mockServer.URL+"/sts"))
			require.NoError(t, err)

			creds, err := s3Client.FetchCredentialsForRole(context.Background(), tt.roleARN, bucketName, objectName, tt.duration)

			if tt.errMsg != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), tt.errMsg)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expected, creds)
			}
		})
	}
}

func TestGetUploadEnv(t *testing.T) {
	tests := map[string]struct {
		config      *common.CacheConfig
		failedFetch bool
		expected    map[string]string
	}{
		"no upload role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{},
			},
			expected: nil,
		},
		"with upload role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					BucketName:    bucketName,
					UploadRoleARN: "arn:aws:iam::123456789012:role/S3UploadRole",
				},
			},
			expected: map[string]string{
				"AWS_ACCESS_KEY_ID":     "mock-access-key",
				"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
				"AWS_SESSION_TOKEN":     "mock-session-token",
			},
		},
		"with failed credentials": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					BucketName:    bucketName,
					UploadRoleARN: "arn:aws:iam::123456789012:role/S3UploadRole",
				},
			},
			failedFetch: true,
			expected:    nil,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			cleanupS3URLGeneratorMock := onFakeS3URLGenerator(cacheOperationTest{})
			defer cleanupS3URLGeneratorMock()

			adapter, err := New(tt.config, defaultTimeout, objectName)
			require.NoError(t, err)

			mockClient := adapter.(*s3Adapter).client.(*mockS3Presigner)

			if tt.failedFetch {
				mockClient.On("FetchCredentialsForRole", mock.Anything, tt.config.S3.UploadRoleARN, bucketName, objectName, mock.Anything).
					Return(nil, errors.New("error fetching credentials"))
			} else {
				mockClient.On("FetchCredentialsForRole", mock.Anything, tt.config.S3.UploadRoleARN, bucketName, objectName, mock.Anything).
					Return(tt.expected, nil)
			}
			env, err := adapter.GetUploadEnv(context.Background())
			assert.Equal(t, tt.expected, env)

			if tt.failedFetch {
				assert.Error(t, err)
			} else {
				assert.NoError(t, err)
			}
		})
	}
}

func runOnFakeMinioWithCredentials(t *testing.T, test minioClientInitializationTest) func() {
	oldNewMinioWithCredentials := newMinioWithIAM
	newMinioWithIAM =
		func(serverAddress, bucketLocation string) (*minio.Client, error) {
			if !test.expectedToUseIAM {
				t.Error("Should not use minio with IAM client initializator")
			}

			assert.Equal(t, "location", bucketLocation)

			if test.serverAddress == "" {
				assert.Equal(t, DefaultAWSS3Server, serverAddress)
			} else {
				assert.Equal(t, test.serverAddress, serverAddress)
			}

			if test.errorOnInitialization {
				return nil, errors.New("test error")
			}

			client, err := minio.New(serverAddress, &minio.Options{
				Creds:  credentials.NewIAM(""),
				Secure: true,
				Transport: &bucketLocationTripper{
					bucketLocation: bucketLocation,
				},
			})
			require.NoError(t, err)

			return client, nil
		}

	return func() {
		newMinioWithIAM = oldNewMinioWithCredentials
	}
}

func TestRegisterTokenExpiresAt(t *testing.T) {
	type testCase struct {
		token          string
		expiration     time.Time
		expectedConfig string
	}

	testCases := map[string]testCase{
		"no expiration": {
			token: "test-runner-token",
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration": {
			token:      "test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
		"no expiration with authentication token": {
			token: "glrt-test-runner-token",
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration with authentication token": {
			token:      "glrt-test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:             12345,
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, []kv{}, "", "--registration-token", tc.token, "--name", "test-runner")
			require.NoError(t, err)

			assert.Contains(
				t, spaceReplacer.Replace(gotConfig),
				spaceReplacer.Replace(fmt.Sprintf(tc.expectedConfig, commands.RegisterTimeNowDate.Format(time.RFC3339))),
			)
		})
	}
}

func TestDoRetry(t *testing.T) {
	cases := []struct {
		name          string
		err           error
		expectedCount int
	}{
		{
			name:          "Error is of type retryableErr",
			err:           retryableErr{err: errors.New("error")},
			expectedCount: 4,
		},
		{
			name:          "Error is not type of retryableErr",
			err:           errors.New("error"),
			expectedCount: 1,
		},
		{
			name:          "Error is nil",
			err:           nil,
			expectedCount: 1,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			r := retryHelper{
				Retry: 3,
			}

			retryCount := 0
			err := r.doRetry(func(_ int) error {
				retryCount++
				return c.err
			})

			assert.Equal(t, c.err, err)
			assert.Equal(t, c.expectedCount, retryCount)
		})
	}
}

func TestArchiverOptionFromEnv(t *testing.T) {
	tests := map[string]struct {
		value string
		err   string
	}{
		archiverStagingDir:  {"/dev/null", "fastzip archiver unable to create temporary directory"},
		archiverConcurrency: {"-1", "concurrency must be at least 1"},
	}

	for option, tc := range tests {
		t.Run(fmt.Sprintf("%s=%s", option, tc.value), func(t *testing.T) {
			defer tempEnvOption(option, tc.value)()

			archiveTestDir(t, func(_ string, _ string, err error) {
				require.Error(t, err)
				require.Contains(t, err.Error(), tc.err)
			})
		})
	}
}

func TestCacheArchiverUploadedSize(t *testing.T) {
	tests := map[string]struct {
		limit    int
		exceeded bool
	}{
		"no-limit":    {limit: 0, exceeded: false},
		"above-limit": {limit: 10, exceeded: true},
		"equal-limit": {limit: 22, exceeded: false},
		"below-limit": {limit: 25, exceeded: false},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			defer logrus.SetOutput(logrus.StandardLogger().Out)
			defer testHelpers.MakeFatalToPanic()()

			var buf bytes.Buffer
			logrus.SetOutput(&buf)

			ts := httptest.NewServer(http.HandlerFunc(testCacheBaseUploadHandler))
			defer ts.Close()

			defer os.Remove(cacheArchiverArchive)
			cmd := helpers.CacheArchiverCommand{
				File:                   cacheArchiverArchive,
				MaxUploadedArchiveSize: int64(tc.limit),
				URL:                    ts.URL + "/cache.zip",
				Timeout:                0,
			}
			assert.NotPanics(t, func() {
				cmd.Execute(nil)
			})

			if tc.exceeded {
				require.Contains(t, buf.String(), "too big")
			} else {
				require.NotContains(t, buf.String(), "too big")
			}
		})
	}
}

func TestAskRunnerUsingRunnerTokenOnRegistrationTokenOverridingForbiddenDefaults(t *testing.T) {
	tests := map[string]interface{}{
		"--access-level":     "not_protected",
		"--run-untagged":     true,
		"--maximum-timeout":  1,
		"--paused":           true,
		"--tag-list":         "tag",
		"--maintenance-note": "note",
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
				Return(&common.VerifyRunnerResponse{
					ID:    1,
					Token: "glrt-testtoken",
				}).
				Once()

			answers := make([]string, 4)
			arguments := append(
				executorCmdLineArgs(t, "shell"),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				tn, fmt.Sprintf("%v", tc),
			)

			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			hook := test.NewGlobal()
			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			_ = app.Run(append([]string{"runner", "register"}, arguments...))

			assert.Contains(
				t,
				commands.GetLogrusOutput(t, hook),
				"This has triggered the 'legacy-compatible registration process'",
			)
		})
	}
}

func TestCacheUploadEnv(t *testing.T) {
	tests := map[string]struct {
		key                   string
		cacheConfig           *common.CacheConfig
		createCacheAdapter    bool
		createCacheAdapterErr error
		getUploadEnvResult    interface{}
		expectedEnvs          map[string]string
		expectedLogEntry      string
	}{
		"full map": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: map[string]string{"TEST1": "123", "TEST2": "456"},
			expectedEnvs:       map[string]string{"TEST1": "123", "TEST2": "456"},
		},
		"nil": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no cache config": {
			key:                "key",
			cacheConfig:        nil,
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no key": {
			key:                "",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter not exists": {
			cacheConfig:        &common.CacheConfig{Type: "doesnt-exist"},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter creation error": {
			cacheConfig:           &common.CacheConfig{Type: "test"},
			createCacheAdapter:    true,
			createCacheAdapterErr: errors.New("test error"),
			getUploadEnvResult:    nil,
			expectedEnvs:          nil,
			expectedLogEntry:      `Could not create cache adapter" error="test error`,
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			cacheAdapter := new(MockAdapter)
			defer cacheAdapter.AssertExpectations(t)

			logs, cleanUpHooks := hook.NewHook()
			defer cleanUpHooks()

			build := &common.Build{
				Runner: &common.RunnerConfig{
					RunnerSettings: common.RunnerSettings{
						Cache: tc.cacheConfig,
					},
				},
			}

			oldCreateAdapter := createAdapter
			createAdapter = func(_ *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
				if tc.createCacheAdapter {
					return cacheAdapter, tc.createCacheAdapterErr
				}

				return nil, nil
			}
			defer func() {
				createAdapter = oldCreateAdapter
			}()

			if tc.cacheConfig != nil && tc.createCacheAdapter {
				cacheAdapter.On("GetUploadEnv", mock.Anything).Return(tc.getUploadEnvResult, nil)
			}

			envs, err := GetCacheUploadEnv(context.Background(), build, "key")
			assert.NoError(t, err)
			assert.Equal(t, tc.expectedEnvs, envs)

			if tc.expectedLogEntry != "" {
				lastLogMsg, err := logs.LastEntry().String()
				require.NoError(t, err)
				assert.Contains(t, lastLogMsg, tc.expectedLogEntry)
			}
		})
	}
}

func TestAccessLevelSetting(t *testing.T) {
	tests := map[string]struct {
		accessLevel     commands.AccessLevel
		failureExpected bool
	}{
		"access level not defined": {},
		"ref_protected used": {
			accessLevel: commands.RefProtected,
		},
		"not_protected used": {
			accessLevel: commands.NotProtected,
		},
		"unknown access level": {
			accessLevel:     commands.AccessLevel("unknown"),
			failureExpected: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if !testCase.failureExpected {
				parametersMocker := mock.MatchedBy(func(parameters common.RegisterRunnerParameters) bool {
					return commands.AccessLevel(parameters.AccessLevel) == testCase.accessLevel
				})

				network.On("RegisterRunner", mock.Anything, parametersMocker).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			}

			arguments := []string{
				"--registration-token", "test-runner-token",
				"--access-level", string(testCase.accessLevel),
			}

			_, output, err := testRegisterCommandRun(t, network, nil, "", arguments...)

			if testCase.failureExpected {
				assert.EqualError(t, err, "command error: Given access-level is not valid. "+
					"Refer to gitlab-runner register -h for the correct options.")
				assert.NotContains(t, output, "Runner registered successfully.")

				return
			}

			assert.NoError(t, err)
			assert.Contains(t, output, "Runner registered successfully.")
		})
	}
}

func TestZipArchiveExtract(t *testing.T) {
	small := []byte("12345678")
	large := bytes.Repeat([]byte("198273qhnjbqwdjbqwe2109u3abcdef3"), 1024*1024)

	OnEachZipArchiver(t, func(t *testing.T) {
		dir := t.TempDir()
		buf := new(bytes.Buffer)

		require.NoError(t, os.WriteFile(filepath.Join(dir, "small"), small, 0777))
		require.NoError(t, os.WriteFile(filepath.Join(dir, "large"), large, 0777))

		archiver, err := archive.NewArchiver(archive.Zip, buf, dir, archive.DefaultCompression)
		require.NoError(t, err)

		files := make(map[string]fs.FileInfo)
		_ = filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}
			files[path] = info
			return nil
		})

		assert.Equal(t, 2, len(files))
		require.NoError(t, archiver.Archive(context.Background(), files))

		input := buf.Bytes()
		OnEachZipExtractor(t, func(t *testing.T) {
			out := t.TempDir()

			extractor, err := archive.NewExtractor(archive.Zip, bytes.NewReader(input), int64(len(input)), out)
			require.NoError(t, err)
			require.NoError(t, extractor.Extract(context.Background()))

			smallEq, err := os.ReadFile(filepath.Join(out, "small"))
			require.NoError(t, err)
			assert.Equal(t, small, smallEq)

			largeEq, err := os.ReadFile(filepath.Join(out, "large"))
			require.NoError(t, err)
			assert.Equal(t, large, largeEq)
		}, "fastzip")
	}, "fastzip")
}

func TestArtifactsUploaderZipSucceeded(t *testing.T) {
	OnEachZipArchiver(t, func(t *testing.T) {
		network := &testNetwork{
			uploadState: common.UploadSucceeded,
		}
		cmd := ArtifactsUploaderCommand{
			JobCredentials: UploaderCredentials,
			Format:         common.ArtifactFormatZip,
			Name:           "my-release",
			Type:           "my-type",
			network:        network,
			fileArchiver: fileArchiver{
				Paths: []string{artifactsTestArchivedFile},
			},
		}

		writeTestFile(t, artifactsTestArchivedFile)
		defer os.Remove(artifactsTestArchivedFile)

		cmd.Execute(nil)
		assert.Equal(t, 1, network.uploadCalled)
		assert.Equal(t, common.ArtifactFormatZip, network.uploadFormat)
		assert.Equal(t, "my-release.zip", network.uploadName)
		assert.Equal(t, "my-type", network.uploadType)
		assert.Contains(t, network.uploadedFiles, artifactsTestArchivedFile)
	})
}

func Test_loadConfig(t *testing.T) {
	const expectedSystemIDRegexPattern = "^[sr]_[0-9a-zA-Z]{12}$"

	testCases := map[string]struct {
		runnerSystemID string
		prepareFn      func(t *testing.T, systemIDFile string)
		assertFn       func(t *testing.T, err error, config *common.Config, systemIDFile string)
	}{
		"generates and saves missing system IDs": {
			runnerSystemID: "",
			assertFn: func(t *testing.T, err error, config *common.Config, systemIDFile string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.NotEmpty(t, config.Runners[0].SystemIDState.GetSystemID())
				content, err := os.ReadFile(systemIDFile)
				require.NoError(t, err)
				assert.Contains(t, string(content), config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"preserves existing unique system IDs": {
			runnerSystemID: "s_c2d22f638c25",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Equal(t, "s_c2d22f638c25", config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"regenerates system ID if file is invalid": {
			runnerSystemID: "0123456789",
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				assert.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
		"succeeds if file cannot be created": {
			runnerSystemID: "",
			prepareFn: func(t *testing.T, systemIDFile string) {
				require.NoError(t, os.Remove(systemIDFile))
				require.NoError(t, os.Chmod(filepath.Dir(systemIDFile), os.ModeDir|0500))
			},
			assertFn: func(t *testing.T, err error, config *common.Config, _ string) {
				require.NoError(t, err)
				require.Equal(t, 1, len(config.Runners))
				assert.Regexp(t, expectedSystemIDRegexPattern, config.Runners[0].SystemIDState.GetSystemID())
			},
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			dir := t.TempDir()
			cfgName := filepath.Join(dir, "config.toml")
			systemIDFile := filepath.Join(dir, ".runner_system_id")

			require.NoError(t, os.Chmod(dir, 0777))
			require.NoError(t, os.WriteFile(cfgName, []byte("[[runners]]\n name = \"runner\""), 0777))
			require.NoError(t, os.WriteFile(systemIDFile, []byte(tc.runnerSystemID), 0777))

			if tc.prepareFn != nil {
				tc.prepareFn(t, systemIDFile)
			}

			c := configOptions{ConfigFile: cfgName}
			err := c.loadConfig()

			tc.assertFn(t, err, c.config, systemIDFile)

			// Cleanup
			require.NoError(t, os.Chmod(dir, 0777))
		})
	}
}

func TestRegisterTokenExpiresAt(t *testing.T) {
	type testCase struct {
		token          string
		expiration     time.Time
		expectedConfig string
	}

	testCases := map[string]testCase{
		"no expiration": {
			token: "test-runner-token",
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration": {
			token:      "test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
		"no expiration with authentication token": {
			token: "glrt-test-runner-token",
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration with authentication token": {
			token:      "glrt-test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:             12345,
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, []kv{}, "", "--registration-token", tc.token, "--name", "test-runner")
			require.NoError(t, err)

			assert.Contains(
				t, spaceReplacer.Replace(gotConfig),
				spaceReplacer.Replace(fmt.Sprintf(tc.expectedConfig, commands.RegisterTimeNowDate.Format(time.RFC3339))),
			)
		})
	}
}

func TestConfigTemplate_MergeTo(t *testing.T) {
	tests := map[string]struct {
		templateContent string
		config          *common.RunnerConfig

		expectedError       error
		assertConfiguration func(t *testing.T, config *common.RunnerConfig)
	}{
		"invalid template file": {
			templateContent: configTemplateMergeToInvalidConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("couldn't load configuration template file: decoding configuration file: toml: line 1: expected '.' or '=', but got ',' instead"),
		},
		"no runners in template": {
			templateContent: configTemplateMergeToEmptyConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("configuration template must contain exactly one [[runners]] entry"),
		},
		"multiple runners in template": {
			templateContent: configTemplateMergeToTwoRunnerSectionsConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("configuration template must contain exactly one [[runners]] entry"),
		},
		"template doesn't overwrite existing settings": {
			templateContent: configTemplateMergeToOverwritingConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				assert.Equal(t, configTemplateMergeToBaseConfiguration.Token, config.RunnerCredentials.Token)
				assert.Equal(t, configTemplateMergeToBaseConfiguration.Executor, config.RunnerSettings.Executor)
				assert.Equal(t, 100, config.Limit)
			},
			expectedError: nil,
		},
		"template doesn't overwrite token if none provided in base": {
			templateContent: configTemplateMergeToOverwritingConfiguration,
			config:          &common.RunnerConfig{},
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				assert.Equal(t, "", config.Token)
			},
		},
		"template adds additional content": {
			templateContent: configTemplateMergeToAdditionalConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				k8s := config.RunnerSettings.Kubernetes

				require.NotNil(t, k8s)
				require.NotEmpty(t, k8s.Volumes.EmptyDirs)
				assert.Len(t, k8s.Volumes.EmptyDirs, 1)

				emptyDir := k8s.Volumes.EmptyDirs[0]
				assert.Equal(t, "empty_dir", emptyDir.Name)
				assert.Equal(t, "/path/to/empty_dir", emptyDir.MountPath)
				assert.Equal(t, "Memory", emptyDir.Medium)
				assert.Equal(t, "1G", emptyDir.SizeLimit)
			},
			expectedError: nil,
		},
		"error on merging": {
			templateContent: configTemplateMergeToAdditionalConfiguration,
			expectedError: fmt.Errorf(
				"error while merging configuration with configuration template: %w",
				mergo.ErrNotSupported,
			),
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			file, cleanup := PrepareConfigurationTemplateFile(t, tc.templateContent)
			defer cleanup()

			configTemplate := &configTemplate{ConfigFile: file}
			err := configTemplate.MergeTo(tc.config)

			if tc.expectedError != nil {
				assert.ErrorContains(t, err, tc.expectedError.Error())

				return
			}

			assert.NoError(t, err)
			tc.assertConfiguration(t, tc.config)
		})
	}
}

func TestArtifactsUploaderZipSucceeded(t *testing.T) {
	OnEachZipArchiver(t, func(t *testing.T) {
		network := &testNetwork{
			uploadState: common.UploadSucceeded,
		}
		cmd := ArtifactsUploaderCommand{
			JobCredentials: UploaderCredentials,
			Format:         common.ArtifactFormatZip,
			Name:           "my-release",
			Type:           "my-type",
			network:        network,
			fileArchiver: fileArchiver{
				Paths: []string{artifactsTestArchivedFile},
			},
		}

		writeTestFile(t, artifactsTestArchivedFile)
		defer os.Remove(artifactsTestArchivedFile)

		cmd.Execute(nil)
		assert.Equal(t, 1, network.uploadCalled)
		assert.Equal(t, common.ArtifactFormatZip, network.uploadFormat)
		assert.Equal(t, "my-release.zip", network.uploadName)
		assert.Equal(t, "my-type", network.uploadType)
		assert.Contains(t, network.uploadedFiles, artifactsTestArchivedFile)
	})
}

func (mr *RunCommand) processRunners(id int, stopWorker chan bool, runners chan *common.RunnerConfig) {
	mr.log().
		WithField("worker", id).
		Debugln("Starting worker")

	mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStarted).Inc()

	for mr.stopSignal == nil {
		select {
		case runner := <-runners:
			err := mr.processRunner(id, runner, runners)
			if err != nil {
				logger := mr.log().
					WithFields(logrus.Fields{
						"runner":   runner.ShortDescription(),
						"executor": runner.Executor,
					}).WithError(err)

				l, failureType := loggerAndFailureTypeFromError(logger, err)
				l("Failed to process runner")
				mr.runnerWorkerProcessingFailure.
					WithLabelValues(failureType, runner.ShortDescription(), runner.Name, runner.GetSystemID()).
					Inc()
			}

		case <-stopWorker:
			mr.log().
				WithField("worker", id).
				Debugln("Stopping worker")

			mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStopped).Inc()

			return
		}
	}
	<-stopWorker
}

func TestAskRunnerUsingRunnerTokenOverridingForbiddenDefaults(t *testing.T) {
	tests := map[string]interface{}{
		"--access-level":     "not_protected",
		"--run-untagged":     true,
		"--maximum-timeout":  1,
		"--paused":           true,
		"--tag-list":         "tag",
		"--maintenance-note": "note",
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			removeHooksFn := helpers.MakeFatalToPanic()
			defer removeHooksFn()

			network := new(common.MockNetwork)
			network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
				Panic("VerifyRunner should not be called")

			answers := make([]string, 4)
			arguments := append(
				executorCmdLineArgs(t, "shell"),
				"--url", "http://gitlab.example.com/",
				"-t", "glrt-testtoken",
				tn, fmt.Sprintf("%v", tc),
			)

			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			hook := test.NewGlobal()
			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			defer func() {
				var output string
				if r := recover(); r != nil {
					// log panics force exit
					if e, ok := r.(*logrus.Entry); ok {
						output = e.Message
					}
				}
				if output == "" {
					output = commands.GetLogrusOutput(t, hook)
				}
				assert.Contains(t, output, "Runner configuration other than name and executor configuration is reserved")
			}()

			_ = app.Run(append([]string{"runner", "register"}, arguments...))

			assert.Fail(t, "Should not reach this point")
		})
	}
}

func init() {
	common.RegisterCommand(cli.Command{
		Name:  "fleeting",
		Usage: "manage fleeting plugins",
		Flags: []cli.Flag{
			cli.StringFlag{Name: "config, c", EnvVar: "CONFIG_FILE", Value: commands.GetDefaultConfigFile()},
		},
		Subcommands: []cli.Command{
			{
				Name:   "install",
				Usage:  "install or update fleeting plugins",
				Flags:  []cli.Flag{cli.BoolFlag{Name: "upgrade"}},
				Action: install,
			},
			{
				Name:   "list",
				Usage:  "list installed plugins",
				Action: list,
			},
			{
				Name:  "login",
				Usage: "login to container registry",
				Flags: []cli.Flag{
					cli.StringFlag{Name: "username"},
					cli.StringFlag{Name: "password"},
					cli.BoolFlag{Name: "password-stdin", Usage: "take the password from stdin"},
				},
				ArgsUsage: "[server]",
				Action:    login,
			},
		},
	})
}

func TestAdapter(t *testing.T) {
	tests := map[string]struct {
		config         *common.CacheConfig
		timeout        time.Duration
		objectName     string
		newExpectedErr string
		getExpectedErr string
		putExpectedErr string
	}{
		"missing config": {
			config:         &common.CacheConfig{},
			objectName:     "object-key",
			newExpectedErr: "missing GCS configuration",
		},
		"no bucket name": {
			config:         &common.CacheConfig{GCS: &common.CacheGCSConfig{}},
			objectName:     "object-key",
			getExpectedErr: "config BucketName cannot be empty",
			putExpectedErr: "config BucketName cannot be empty",
		},
		"valid": {
			config:     &common.CacheConfig{GCS: &common.CacheGCSConfig{BucketName: "test", CacheGCSCredentials: common.CacheGCSCredentials{AccessID: accessID, PrivateKey: privateKey}}},
			objectName: "object-key",
		},
		"valid with max upload size": {
			config:     &common.CacheConfig{MaxUploadedArchiveSize: 100, GCS: &common.CacheGCSConfig{BucketName: "test", CacheGCSCredentials: common.CacheGCSCredentials{AccessID: accessID, PrivateKey: privateKey}}},
			objectName: "object-key",
		},
	}

	const expectedURL = "https://storage.googleapis.com/test/object-key"

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			adapter, err := New(tc.config, tc.timeout, tc.objectName)
			if tc.newExpectedErr != "" {
				require.EqualError(t, err, tc.newExpectedErr)
				require.Nil(t, adapter)
				return
			} else {
				require.NoError(t, err)
				require.NotNil(t, adapter)
			}

			getURL, err := adapter.(*gcsAdapter).presignURL(context.Background(), http.MethodGet, "")
			if tc.getExpectedErr != "" {
				assert.EqualError(t, err, tc.getExpectedErr)
			} else {
				assert.NoError(t, err)
			}

			putURL, err := adapter.(*gcsAdapter).presignURL(context.Background(), http.MethodPut, "application/octet-stream")
			if tc.putExpectedErr != "" {
				assert.EqualError(t, err, tc.putExpectedErr)
			} else {
				assert.NoError(t, err)
			}

			if getURL != nil {
				assert.Contains(t, getURL.String(), expectedURL)

				u := adapter.GetDownloadURL(context.Background())
				require.NotNil(t, u)
				assert.Contains(t, u.URL.String(), expectedURL)
			}

			if putURL != nil {
				assert.Contains(t, putURL.String(), expectedURL)

				u := adapter.GetUploadURL(context.Background())
				require.NotNil(t, u)
				assert.Contains(t, u.URL.String(), expectedURL)

				if tc.config.MaxUploadedArchiveSize != 0 {
					assert.Equal(t, u.Headers, http.Header{"X-Goog-Content-Length-Range": []string{fmt.Sprintf("0,%d", tc.config.MaxUploadedArchiveSize)}})
				} else {
					assert.Nil(t, u.Headers)
				}
			}
		})
	}
}

func TestImportCommento(t *testing.T) {
	failTestOnError(t, setupTestEnv())

	// Create JSON data
	data := commentoExportV1{
		Version: 1,
		Comments: []comment{
			{
				CommentHex:   "5a349182b3b8e25107ab2b12e514f40fe0b69160a334019491d7c204aff6fdc2",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a reply!",
				Html:         "",
				ParentHex:    "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:08:44.061525Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a comment!",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:07:49.244432Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "a7c84f251b5a09d5b65e902cbe90633646437acefa3a52b761fee94002ac54c7",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Markdown:     "This is a test comment, bar foo\n\n#Here is something big\n\n```\nhere code();\n```",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:20:21.101653Z"),
				Direction:    0,
				Deleted:      false,
			},
		},
		Commenters: []commenter{
			{
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Email:        "john@doe.com",
				Name:         "John Doe",
				Link:         "https://john.doe",
				Photo:        "undefined",
				Provider:     "commento",
				JoinDate:     timeParse(t, "2020-01-27T14:17:59.298737Z"),
				IsModerator:  false,
			},
		},
	}

	// Create listener with random port
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Errorf("couldn't create listener: %v", err)
		return
	}
	defer func() {
		_ = listener.Close()
	}()
	port := listener.Addr().(*net.TCPAddr).Port

	// Launch http server serving commento json gzipped data
	go func() {
		http.Serve(listener, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			gzipper := gzip.NewWriter(w)
			defer func() {
				_ = gzipper.Close()
			}()
			encoder := json.NewEncoder(gzipper)
			if err := encoder.Encode(data); err != nil {
				t.Errorf("couldn't write data: %v", err)
			}
		}))
	}()
	url := fmt.Sprintf("http://127.0.0.1:%d", port)

	domainNew("temp-owner-hex", "Example", "example.com")

	n, err := domainImportCommento("example.com", url)
	if err != nil {
		t.Errorf("unexpected error importing comments: %v", err)
		return
	}
	if n != len(data.Comments) {
		t.Errorf("imported comments missmatch (got %d, want %d)", n, len(data.Comments))
	}
}

func TestFetchCredentialsForRole(t *testing.T) {
	workingConfig := common.CacheConfig{
		S3: &common.CacheS3Config{
			AccessKey:          "test-access-key",
			SecretKey:          "test-secret-key",
			AuthenticationType: "access-key",
			BucketName:         "test-bucket",
			UploadRoleARN:      "arn:aws:iam::123456789012:role/TestRole",
		},
	}
	mockedCreds := map[string]string{
		"AWS_ACCESS_KEY_ID":     "mock-access-key",
		"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
		"AWS_SESSION_TOKEN":     "mock-session-token",
	}

	tests := map[string]struct {
		config           *common.CacheConfig
		roleARN          string
		expected         map[string]string
		errMsg           string
		expectedKms      bool
		duration         time.Duration
		expectedDuration time.Duration
	}{
		"successful fetch": {
			config:   &workingConfig,
			roleARN:  "arn:aws:iam::123456789012:role/TestRole",
			expected: mockedCreds,
		},
		"successful fetch with 12-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         12 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 12 * time.Hour,
		},
		"successful fetch with 10-minute timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         10 * time.Minute,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with 13-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         13 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with encryption": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:                 "test-access-key",
					SecretKey:                 "test-secret-key",
					AuthenticationType:        "access-key",
					BucketName:                "test-bucket",
					UploadRoleARN:             "arn:aws:iam::123456789012:role/TestRole",
					ServerSideEncryption:      "KMS",
					ServerSideEncryptionKeyID: "arn:aws:kms:us-west-2:123456789012:key/1234abcd-12ab-34cd-56ef-1234567890ab",
				},
			},
			roleARN:     "arn:aws:iam::123456789012:role/TestRole",
			expected:    mockedCreds,
			expectedKms: true,
		},
		"invalid role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
					UploadRoleARN:      "arn:aws:iam::123456789012:role/InvalidRole",
				},
			},
			roleARN: "arn:aws:iam::123456789012:role/InvalidRole",
			errMsg:  "failed to assume role",
		},
		"no role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
				},
			},
			expected: nil,
			errMsg:   "failed to assume role",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			duration := 3600
			if tt.duration > 0 {
				duration = int(tt.expectedDuration.Seconds())
			}
			// Create s3Client and point STS endpoint to it
			mockServer := httptest.NewServer(newMockSTSHandler(tt.expectedKms, duration))
			defer mockServer.Close()

			s3Client, err := newS3Client(tt.config.S3, withSTSEndpoint(mockServer.URL+"/sts"))
			require.NoError(t, err)

			creds, err := s3Client.FetchCredentialsForRole(context.Background(), tt.roleARN, bucketName, objectName, tt.duration)

			if tt.errMsg != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), tt.errMsg)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expected, creds)
			}
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/exec"
	"syscall"
)

func foo1() {
	err := exec.CommandContext(context.Background(), "git", "rev-parse", "--show-toplavel").Run()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo2() {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

func foo3() {
	run := "sleep" + os.Getenv("SOMETHING")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo4(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo5() {
	foo4("sleep")
}

func foo6(a string, c string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
}

func foo7() {
	foo6("ll", "ls")
}

func foo8() {
	err := syscall.Exec("/bin/cat", []string{"/etc/passwd"}, nil)
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo9(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo10() {
	foo9("sleep")
}

func foo11(command string) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		fmt.Printf("Error: %v\n", err)
	}
}

func foo12() {
	foo11("sleep")
}

func foo13() {
	run := "sleep"
	cmd := exec.Command(run, "5")
	err := cmd.Start()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Waiting for command to finish...")
	err = cmd.Wait()
	log.Printf("Command finished with error: %v", err)
}

func foo14() {
	// ruleid: go_subproc_rule-subproc
	err := exec.CommandContext(context.Background(), os.Args[0], "5").Run()
	if err != nil {
		log.Fatal(err)
	}
	log.Printf("Command finished with error: %v", err)
}

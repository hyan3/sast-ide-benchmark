func TestCreateCredentialsAdapter(t *testing.T) {
	adapterMock := new(MockCredentialsAdapter)

	tests := map[string]credentialsFactoryTestCase{
		"adapter doesn't exist": {
			adapter:          nil,
			errorOnFactorize: nil,
			expectedAdapter:  nil,
			expectedError:    `credentials adapter factory not found: factory for credentials adapter "test" not registered`,
		},
		"adapter exists": {
			adapter:          adapterMock,
			errorOnFactorize: nil,
			expectedAdapter:  adapterMock,
			expectedError:    "",
		},
		"adapter errors on factorize": {
			adapter:          adapterMock,
			errorOnFactorize: errors.New("test error"),
			expectedAdapter:  nil,
			expectedError:    `credentials adapter could not be initialized: test error`,
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			cleanupFactoriesMap := prepareMockedCredentialsFactoriesMap()
			defer cleanupFactoriesMap()

			adapterTypeName := "test"

			if tc.adapter != nil {
				err := credentialsFactories.Register(adapterTypeName, makeTestCredentialsFactory(tc))
				assert.NoError(t, err)
			}

			_ = credentialsFactories.Register(
				"additional-adapter",
				func(config *common.CacheConfig) (CredentialsAdapter, error) {
					return new(MockCredentialsAdapter), nil
				})

			config := &common.CacheConfig{
				Type: adapterTypeName,
			}

			adapter, err := CreateCredentialsAdapter(config)

			if tc.expectedError == "" {
				assert.NoError(t, err)
			} else {
				assert.EqualError(t, err, tc.expectedError)
			}

			assert.Equal(t, tc.expectedAdapter, adapter)
		})
	}
}

func TestAdapter(t *testing.T) {
	tests := map[string]struct {
		config         *common.CacheConfig
		timeout        time.Duration
		objectName     string
		newExpectedErr string
		getExpectedErr string
		putExpectedErr string
	}{
		"missing config": {
			config:         &common.CacheConfig{},
			objectName:     "object-key",
			newExpectedErr: "missing GCS configuration",
		},
		"no bucket name": {
			config:         &common.CacheConfig{GCS: &common.CacheGCSConfig{}},
			objectName:     "object-key",
			getExpectedErr: "config BucketName cannot be empty",
			putExpectedErr: "config BucketName cannot be empty",
		},
		"valid": {
			config:     &common.CacheConfig{GCS: &common.CacheGCSConfig{BucketName: "test", CacheGCSCredentials: common.CacheGCSCredentials{AccessID: accessID, PrivateKey: privateKey}}},
			objectName: "object-key",
		},
		"valid with max upload size": {
			config:     &common.CacheConfig{MaxUploadedArchiveSize: 100, GCS: &common.CacheGCSConfig{BucketName: "test", CacheGCSCredentials: common.CacheGCSCredentials{AccessID: accessID, PrivateKey: privateKey}}},
			objectName: "object-key",
		},
	}

	const expectedURL = "https://storage.googleapis.com/test/object-key"

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			adapter, err := New(tc.config, tc.timeout, tc.objectName)
			if tc.newExpectedErr != "" {
				require.EqualError(t, err, tc.newExpectedErr)
				require.Nil(t, adapter)
				return
			} else {
				require.NoError(t, err)
				require.NotNil(t, adapter)
			}

			getURL, err := adapter.(*gcsAdapter).presignURL(context.Background(), http.MethodGet, "")
			if tc.getExpectedErr != "" {
				assert.EqualError(t, err, tc.getExpectedErr)
			} else {
				assert.NoError(t, err)
			}

			putURL, err := adapter.(*gcsAdapter).presignURL(context.Background(), http.MethodPut, "application/octet-stream")
			if tc.putExpectedErr != "" {
				assert.EqualError(t, err, tc.putExpectedErr)
			} else {
				assert.NoError(t, err)
			}

			if getURL != nil {
				assert.Contains(t, getURL.String(), expectedURL)

				u := adapter.GetDownloadURL(context.Background())
				require.NotNil(t, u)
				assert.Contains(t, u.URL.String(), expectedURL)
			}

			if putURL != nil {
				assert.Contains(t, putURL.String(), expectedURL)

				u := adapter.GetUploadURL(context.Background())
				require.NotNil(t, u)
				assert.Contains(t, u.URL.String(), expectedURL)

				if tc.config.MaxUploadedArchiveSize != 0 {
					assert.Equal(t, u.Headers, http.Header{"X-Goog-Content-Length-Range": []string{fmt.Sprintf("0,%d", tc.config.MaxUploadedArchiveSize)}})
				} else {
					assert.Nil(t, u.Headers)
				}
			}
		})
	}
}

func TestAccessLevelSetting(t *testing.T) {
	tests := map[string]struct {
		accessLevel     commands.AccessLevel
		failureExpected bool
	}{
		"access level not defined": {},
		"ref_protected used": {
			accessLevel: commands.RefProtected,
		},
		"not_protected used": {
			accessLevel: commands.NotProtected,
		},
		"unknown access level": {
			accessLevel:     commands.AccessLevel("unknown"),
			failureExpected: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if !testCase.failureExpected {
				parametersMocker := mock.MatchedBy(func(parameters common.RegisterRunnerParameters) bool {
					return commands.AccessLevel(parameters.AccessLevel) == testCase.accessLevel
				})

				network.On("RegisterRunner", mock.Anything, parametersMocker).
					Return(&common.RegisterRunnerResponse{
						Token: "test-runner-token",
					}).
					Once()
			}

			arguments := []string{
				"--registration-token", "test-runner-token",
				"--access-level", string(testCase.accessLevel),
			}

			_, output, err := testRegisterCommandRun(t, network, nil, "", arguments...)

			if testCase.failureExpected {
				assert.EqualError(t, err, "command error: Given access-level is not valid. "+
					"Refer to gitlab-runner register -h for the correct options.")
				assert.NotContains(t, output, "Runner registered successfully.")

				return
			}

			assert.NoError(t, err)
			assert.Contains(t, output, "Runner registered successfully.")
		})
	}
}

func (g *artifactStatementGenerator) generateSLSAv1Predicate(jobId int64, start time.Time, end time.Time) slsa_v1.ProvenancePredicate {
	externalParams := g.params()
	externalParams["entryPoint"] = g.JobName
	externalParams["source"] = g.RepoURL

	return slsa_v1.ProvenancePredicate{
		BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
			BuildType:          fmt.Sprintf(attestationTypeFormat, g.version()),
			ExternalParameters: externalParams,
			InternalParameters: map[string]string{
				"name":         g.RunnerName,
				"executor":     g.ExecutorName,
				"architecture": common.AppVersion.Architecture,
				"job":          fmt.Sprint(jobId),
			},
			ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
				URI:    g.RepoURL,
				Digest: map[string]string{"sha256": g.RepoDigest},
			}},
		},
		RunDetails: slsa_v1.ProvenanceRunDetails{
			Builder: slsa_v1.Builder{
				ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
				Version: map[string]string{
					"gitlab-runner": g.version(),
				},
			},
			BuildMetadata: slsa_v1.BuildMetadata{
				InvocationID: fmt.Sprint(jobId),
				StartedOn:    &start,
				FinishedOn:   &end,
			},
		},
	}
}

func TestImportCommento(t *testing.T) {
	failTestOnError(t, setupTestEnv())

	// Create JSON data
	data := commentoExportV1{
		Version: 1,
		Comments: []comment{
			{
				CommentHex:   "5a349182b3b8e25107ab2b12e514f40fe0b69160a334019491d7c204aff6fdc2",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a reply!",
				Html:         "",
				ParentHex:    "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:08:44.061525Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a comment!",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:07:49.244432Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "a7c84f251b5a09d5b65e902cbe90633646437acefa3a52b761fee94002ac54c7",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Markdown:     "This is a test comment, bar foo\n\n#Here is something big\n\n```\nhere code();\n```",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:20:21.101653Z"),
				Direction:    0,
				Deleted:      false,
			},
		},
		Commenters: []commenter{
			{
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Email:        "john@doe.com",
				Name:         "John Doe",
				Link:         "https://john.doe",
				Photo:        "undefined",
				Provider:     "commento",
				JoinDate:     timeParse(t, "2020-01-27T14:17:59.298737Z"),
				IsModerator:  false,
			},
		},
	}

	// Create listener with random port
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Errorf("couldn't create listener: %v", err)
		return
	}
	defer func() {
		_ = listener.Close()
	}()
	port := listener.Addr().(*net.TCPAddr).Port

	// Launch http server serving commento json gzipped data
	go func() {
		http.Serve(listener, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			gzipper := gzip.NewWriter(w)
			defer func() {
				_ = gzipper.Close()
			}()
			encoder := json.NewEncoder(gzipper)
			if err := encoder.Encode(data); err != nil {
				t.Errorf("couldn't write data: %v", err)
			}
		}))
	}()
	url := fmt.Sprintf("http://127.0.0.1:%d", port)

	domainNew("temp-owner-hex", "Example", "example.com")

	n, err := domainImportCommento("example.com", url)
	if err != nil {
		t.Errorf("unexpected error importing comments: %v", err)
		return
	}
	if n != len(data.Comments) {
		t.Errorf("imported comments missmatch (got %d, want %d)", n, len(data.Comments))
	}
}

func TestCacheOperation(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)

	tests := map[string]cacheOperationTest{
		"error-on-minio-client-initialization": {
			errorOnMinioClientInitialization: true,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning: true,
			presignedURL:         URL,
			expectedURL:          nil,
		},
		"presigned-url": {
			presignedURL: URL,
			expectedURL:  URL,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
		})
	}
}

func (g *artifactStatementGenerator) generateSLSAv1Predicate(jobId int64, start time.Time, end time.Time) slsa_v1.ProvenancePredicate {
	externalParams := g.params()
	externalParams["entryPoint"] = g.JobName
	externalParams["source"] = g.RepoURL

	return slsa_v1.ProvenancePredicate{
		BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
			BuildType:          fmt.Sprintf(attestationTypeFormat, g.version()),
			ExternalParameters: externalParams,
			InternalParameters: map[string]string{
				"name":         g.RunnerName,
				"executor":     g.ExecutorName,
				"architecture": common.AppVersion.Architecture,
				"job":          fmt.Sprint(jobId),
			},
			ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
				URI:    g.RepoURL,
				Digest: map[string]string{"sha256": g.RepoDigest},
			}},
		},
		RunDetails: slsa_v1.ProvenanceRunDetails{
			Builder: slsa_v1.Builder{
				ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
				Version: map[string]string{
					"gitlab-runner": g.version(),
				},
			},
			BuildMetadata: slsa_v1.BuildMetadata{
				InvocationID: fmt.Sprint(jobId),
				StartedOn:    &start,
				FinishedOn:   &end,
			},
		},
	}
}

func TestAskRunnerUsingRunnerTokenOnRegistrationTokenOverridingForbiddenDefaults(t *testing.T) {
	tests := map[string]interface{}{
		"--access-level":     "not_protected",
		"--run-untagged":     true,
		"--maximum-timeout":  1,
		"--paused":           true,
		"--tag-list":         "tag",
		"--maintenance-note": "note",
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
				Return(&common.VerifyRunnerResponse{
					ID:    1,
					Token: "glrt-testtoken",
				}).
				Once()

			answers := make([]string, 4)
			arguments := append(
				executorCmdLineArgs(t, "shell"),
				"--url", "http://gitlab.example.com/",
				"-r", "glrt-testtoken",
				tn, fmt.Sprintf("%v", tc),
			)

			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			hook := test.NewGlobal()
			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			_ = app.Run(append([]string{"runner", "register"}, arguments...))

			assert.Contains(
				t,
				commands.GetLogrusOutput(t, hook),
				"This has triggered the 'legacy-compatible registration process'",
			)
		})
	}
}

func TestZipArchiveExtract(t *testing.T) {
	small := []byte("12345678")
	large := bytes.Repeat([]byte("198273qhnjbqwdjbqwe2109u3abcdef3"), 1024*1024)

	OnEachZipArchiver(t, func(t *testing.T) {
		dir := t.TempDir()
		buf := new(bytes.Buffer)

		require.NoError(t, os.WriteFile(filepath.Join(dir, "small"), small, 0777))
		require.NoError(t, os.WriteFile(filepath.Join(dir, "large"), large, 0777))

		archiver, err := archive.NewArchiver(archive.Zip, buf, dir, archive.DefaultCompression)
		require.NoError(t, err)

		files := make(map[string]fs.FileInfo)
		_ = filepath.Walk(dir, func(path string, info fs.FileInfo, err error) error {
			if info.IsDir() {
				return nil
			}
			files[path] = info
			return nil
		})

		assert.Equal(t, 2, len(files))
		require.NoError(t, archiver.Archive(context.Background(), files))

		input := buf.Bytes()
		OnEachZipExtractor(t, func(t *testing.T) {
			out := t.TempDir()

			extractor, err := archive.NewExtractor(archive.Zip, bytes.NewReader(input), int64(len(input)), out)
			require.NoError(t, err)
			require.NoError(t, extractor.Extract(context.Background()))

			smallEq, err := os.ReadFile(filepath.Join(out, "small"))
			require.NoError(t, err)
			assert.Equal(t, small, smallEq)

			largeEq, err := os.ReadFile(filepath.Join(out, "large"))
			require.NoError(t, err)
			assert.Equal(t, large, largeEq)
		}, "fastzip")
	}, "fastzip")
}

func TestFileArchiverCompressionLevel(t *testing.T) {
	writeTestFile(t, artifactsTestArchivedFile)
	defer os.Remove(artifactsTestArchivedFile)

	network := &testNetwork{
		uploadState: common.UploadSucceeded,
	}

	for _, expectedLevel := range []string{"fastest", "fast", "default", "slow", "slowest"} {
		t.Run(expectedLevel, func(t *testing.T) {
			mockArchiver := new(archive.MockArchiver)
			defer mockArchiver.AssertExpectations(t)

			archive.Register(
				"zip",
				func(w io.Writer, dir string, level archive.CompressionLevel) (archive.Archiver, error) {
					assert.Equal(t, GetCompressionLevel(expectedLevel), level)
					return mockArchiver, nil
				},
				nil,
			)

			mockArchiver.On("Archive", mock.Anything, mock.Anything).Return(nil)

			cmd := ArtifactsUploaderCommand{
				JobCredentials: UploaderCredentials,
				network:        network,
				Format:         common.ArtifactFormatZip,
				fileArchiver: fileArchiver{
					Paths: []string{artifactsTestArchivedFile},
				},
				CompressionLevel: expectedLevel,
			}
			assert.NoError(t, cmd.enumerate())
			_, r, err := cmd.createReadStream()
			require.NoError(t, err)
			defer r.Close()
			_, _ = io.Copy(io.Discard, r)
		})
	}
}

func TestImportCommento(t *testing.T) {
	failTestOnError(t, setupTestEnv())

	// Create JSON data
	data := commentoExportV1{
		Version: 1,
		Comments: []comment{
			{
				CommentHex:   "5a349182b3b8e25107ab2b12e514f40fe0b69160a334019491d7c204aff6fdc2",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a reply!",
				Html:         "",
				ParentHex:    "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:08:44.061525Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "7ed60b1227f6c4850258a2ac0304e1936770117d6f3a379655f775c46b9f13cd",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "anonymous",
				Markdown:     "This is a comment!",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:07:49.244432Z"),
				Direction:    0,
				Deleted:      false,
			},
			{
				CommentHex:   "a7c84f251b5a09d5b65e902cbe90633646437acefa3a52b761fee94002ac54c7",
				Domain:       "localhost:1313",
				Path:         "/post/first-post/",
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Markdown:     "This is a test comment, bar foo\n\n#Here is something big\n\n```\nhere code();\n```",
				Html:         "",
				ParentHex:    "root",
				Score:        0,
				State:        "approved",
				CreationDate: timeParse(t, "2020-01-27T14:20:21.101653Z"),
				Direction:    0,
				Deleted:      false,
			},
		},
		Commenters: []commenter{
			{
				CommenterHex: "4629a8216538b73987597d66f266c1a1801b0451f99cf066e7122aa104ef3b07",
				Email:        "john@doe.com",
				Name:         "John Doe",
				Link:         "https://john.doe",
				Photo:        "undefined",
				Provider:     "commento",
				JoinDate:     timeParse(t, "2020-01-27T14:17:59.298737Z"),
				IsModerator:  false,
			},
		},
	}

	// Create listener with random port
	listener, err := net.Listen("tcp", "127.0.0.1:0")
	if err != nil {
		t.Errorf("couldn't create listener: %v", err)
		return
	}
	defer func() {
		_ = listener.Close()
	}()
	port := listener.Addr().(*net.TCPAddr).Port

	// Launch http server serving commento json gzipped data
	go func() {
		http.Serve(listener, http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			gzipper := gzip.NewWriter(w)
			defer func() {
				_ = gzipper.Close()
			}()
			encoder := json.NewEncoder(gzipper)
			if err := encoder.Encode(data); err != nil {
				t.Errorf("couldn't write data: %v", err)
			}
		}))
	}()
	url := fmt.Sprintf("http://127.0.0.1:%d", port)

	domainNew("temp-owner-hex", "Example", "example.com")

	n, err := domainImportCommento("example.com", url)
	if err != nil {
		t.Errorf("unexpected error importing comments: %v", err)
		return
	}
	if n != len(data.Comments) {
		t.Errorf("imported comments missmatch (got %d, want %d)", n, len(data.Comments))
	}
}

func (mr *RunCommand) processRunners(id int, stopWorker chan bool, runners chan *common.RunnerConfig) {
	mr.log().
		WithField("worker", id).
		Debugln("Starting worker")

	mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStarted).Inc()

	for mr.stopSignal == nil {
		select {
		case runner := <-runners:
			err := mr.processRunner(id, runner, runners)
			if err != nil {
				logger := mr.log().
					WithFields(logrus.Fields{
						"runner":   runner.ShortDescription(),
						"executor": runner.Executor,
					}).WithError(err)

				l, failureType := loggerAndFailureTypeFromError(logger, err)
				l("Failed to process runner")
				mr.runnerWorkerProcessingFailure.
					WithLabelValues(failureType, runner.ShortDescription(), runner.Name, runner.GetSystemID()).
					Inc()
			}

		case <-stopWorker:
			mr.log().
				WithField("worker", id).
				Debugln("Stopping worker")

			mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStopped).Inc()

			return
		}
	}
	<-stopWorker
}

func TestArtifactsUploaderDefaultSucceeded(t *testing.T) {
	OnEachZipArchiver(t, func(t *testing.T) {
		network := &testNetwork{
			uploadState: common.UploadSucceeded,
		}
		cmd := ArtifactsUploaderCommand{
			JobCredentials: UploaderCredentials,
			network:        network,
			fileArchiver: fileArchiver{
				Paths: []string{artifactsTestArchivedFile},
			},
		}

		writeTestFile(t, artifactsTestArchivedFile)
		defer os.Remove(artifactsTestArchivedFile)

		cmd.Execute(nil)
		assert.Equal(t, 1, network.uploadCalled)
		assert.Equal(t, common.ArtifactFormatZip, network.uploadFormat)
		assert.Equal(t, DefaultUploadName+".zip", network.uploadName)
		assert.Empty(t, network.uploadType)
	})
}

func TestGenerateMetadataToFile(t *testing.T) {
	tmpDir := t.TempDir()
	tmpFile, err := os.CreateTemp(tmpDir, "")
	require.NoError(t, err)

	_, err = tmpFile.WriteString("testdata")
	require.NoError(t, err)
	require.NoError(t, tmpFile.Close())

	sha := sha256.New()
	sha.Write([]byte("testdata"))
	checksum := sha.Sum(nil)

	// First format the time to RFC3339 and then parse it to get the correct precision
	startedAtRFC3339 := time.Now().Format(time.RFC3339)
	startedAt, err := time.Parse(time.RFC3339, startedAtRFC3339)
	require.NoError(t, err)

	endedAtRFC3339 := time.Now().Add(time.Minute).Format(time.RFC3339)
	endedAt, err := time.Parse(time.RFC3339, endedAtRFC3339)
	require.NoError(t, err)

	var testsStatementV1 = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions,
	) *in_toto.ProvenanceStatementSLSA1 {
		return &in_toto.ProvenanceStatementSLSA1{
			StatementHeader: in_toto.StatementHeader{
				Type:          in_toto.StatementInTotoV01,
				PredicateType: slsa_v1.PredicateSLSAProvenance,
				Subject: []in_toto.Subject{
					{
						Name:   tmpFile.Name(),
						Digest: slsa_common.DigestSet{"sha256": hex.EncodeToString(checksum)},
					},
				},
			},
			Predicate: slsa_v1.ProvenancePredicate{
				BuildDefinition: slsa_v1.ProvenanceBuildDefinition{
					BuildType: fmt.Sprintf(attestationTypeFormat, g.version()),
					ExternalParameters: map[string]string{
						"testparam":  "",
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					},
					InternalParameters: map[string]string{
						"name":         g.RunnerName,
						"executor":     g.ExecutorName,
						"architecture": common.AppVersion.Architecture,
						"job":          fmt.Sprint(opts.jobID),
					},
					ResolvedDependencies: []slsa_v1.ResourceDescriptor{{
						URI:    g.RepoURL,
						Digest: map[string]string{"sha256": g.RepoDigest},
					}},
				},
				RunDetails: slsa_v1.ProvenanceRunDetails{
					Builder: slsa_v1.Builder{
						ID: fmt.Sprintf(attestationRunnerIDFormat, g.RepoURL, g.RunnerID),
						Version: map[string]string{
							"gitlab-runner": version,
						},
					},
					BuildMetadata: slsa_v1.BuildMetadata{
						InvocationID: fmt.Sprint(opts.jobID),
						StartedOn:    &startedAt,
						FinishedOn:   &endedAt,
					},
				},
			},
		}
	}

	var testStatement = func(
		version string,
		g *artifactStatementGenerator,
		opts generateStatementOptions) any {
		switch g.SLSAProvenanceVersion {
		case slsaProvenanceVersion1:
			return testsStatementV1(version, g, opts)
		default:
			panic("unreachable, invalid statement version")
		}
	}

	var setVersion = func(version string) (string, func()) {
		originalVersion := common.AppVersion.Version
		common.AppVersion.Version = version

		return version, func() {
			common.AppVersion.Version = originalVersion
		}
	}

	var newGenerator = func(slsaVersion string) *artifactStatementGenerator {
		return &artifactStatementGenerator{
			RunnerID:              1001,
			RepoURL:               "testurl",
			RepoDigest:            "testdigest",
			JobName:               "testjobname",
			ExecutorName:          "testexecutorname",
			RunnerName:            "testrunnername",
			Parameters:            []string{"testparam"},
			StartedAtRFC3339:      startedAtRFC3339,
			EndedAtRFC3339:        endedAtRFC3339,
			SLSAProvenanceVersion: slsaVersion,
		}
	}

	tests := map[string]struct {
		opts          generateStatementOptions
		newGenerator  func(slsaVersion string) *artifactStatementGenerator
		expected      func(*artifactStatementGenerator, generateStatementOptions) (any, func())
		expectedError error
	}{
		"basic": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				version, cleanup := setVersion("v1.0.0")
				return testStatement(version, g, opts), cleanup
			},
		},
		"basic version isn't prefixed so use REVISION": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"files subject doesn't exist": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"nonexisting":  fileInfo{name: "nonexisting"},
				},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expectedError: os.ErrNotExist,
		},
		"non-regular file": {
			newGenerator: newGenerator,
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files: map[string]os.FileInfo{
					tmpFile.Name(): fileInfo{name: tmpFile.Name()},
					"dir":          fileInfo{name: "im-a-dir", mode: fs.ModeDir}},
				artifactsWd: tmpDir,
				jobID:       1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				return testStatement(common.AppVersion.Revision, g, opts), func() {}
			},
		},
		"no parameters": {
			newGenerator: func(v string) *artifactStatementGenerator {
				g := newGenerator(v)
				g.Parameters = nil

				return g
			},
			opts: generateStatementOptions{
				artifactName: "artifact-name",
				files:        map[string]os.FileInfo{tmpFile.Name(): fileInfo{name: tmpFile.Name()}},
				artifactsWd:  tmpDir,
				jobID:        1000,
			},
			expected: func(g *artifactStatementGenerator, opts generateStatementOptions) (any, func()) {
				m := testStatement(common.AppVersion.Revision, g, opts)
				switch m := m.(type) {
				case *in_toto.ProvenanceStatementSLSA1:
					m.Predicate.BuildDefinition.ExternalParameters = map[string]string{
						"entryPoint": g.JobName,
						"source":     g.RepoURL,
					}
				case *in_toto.ProvenanceStatementSLSA02:
					m.Predicate.Invocation.Parameters = map[string]interface{}{}
				}
				return m, func() {}
			},
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			for _, v := range []string{slsaProvenanceVersion1} {
				t.Run(v, func(t *testing.T) {
					g := tt.newGenerator(v)

					var expected any
					if tt.expected != nil {
						var cleanup func()
						expected, cleanup = tt.expected(g, tt.opts)
						defer cleanup()
					}

					f, err := g.generateStatementToFile(tt.opts)
					if tt.expectedError == nil {
						require.NoError(t, err)
					} else {
						assert.Empty(t, f)
						assert.ErrorIs(t, err, tt.expectedError)
						return
					}

					filename := filepath.Base(f)
					assert.Equal(t, fmt.Sprintf(artifactsStatementFormat, tt.opts.artifactName), filename)

					file, err := os.Open(f)
					require.NoError(t, err)
					defer file.Close()

					b, err := io.ReadAll(file)
					require.NoError(t, err)

					indented, err := json.MarshalIndent(expected, "", " ")
					require.NoError(t, err)

					assert.Equal(t, string(indented), string(b))
					assert.Contains(t, string(indented), startedAtRFC3339)
					assert.Contains(t, string(indented), endedAtRFC3339)
				})
			}
		})
	}
}

func TestBuildsHelperFindSessionByURL(t *testing.T) {
	sess, err := session.NewSession(nil)
	require.NoError(t, err)
	build := common.Build{
		Session: sess,
		Runner: &common.RunnerConfig{
			RunnerCredentials: common.RunnerCredentials{
				Token: "abcd1234",
			},
			SystemIDState: common.NewSystemIDState(),
		},
	}

	require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

	h := newBuildsHelper()
	h.addBuild(&build)

	foundSession := h.findSessionByURL(sess.Endpoint + "/action")
	assert.Equal(t, sess, foundSession)

	foundSession = h.findSessionByURL("/session/hash/action")
	assert.Nil(t, foundSession)
}

func TestBuildsHelperCollect(t *testing.T) {
	dir := t.TempDir()

	ch := make(chan prometheus.Metric, 50)
	b := newBuildsHelper()

	longRunningBuild, err := common.GetLongRunningBuild()
	require.NoError(t, err)

	shell := "bash"
	if runtime.GOOS == "windows" {
		shell = "powershell"
	}

	build := &common.Build{
		JobResponse: longRunningBuild,
		Runner: &common.RunnerConfig{
			RunnerSettings: common.RunnerSettings{
				BuildsDir: dir,
				Executor:  "shell",
				Shell:     shell,
			},
			SystemIDState: common.NewSystemIDState(),
		},
	}
	trace := &common.Trace{Writer: io.Discard}

	require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

	done := make(chan error)
	go func() {
		done <- buildtest.RunBuildWithTrace(t, build, trace)
	}()

	b.builds = append(b.builds, build)
	// collect many logs whilst the build is being executed to trigger any
	// potential race conditions that arise from the build progressing whilst
	// metrics are collected.
	for i := 0; i < 200; i++ {
		if i == 100 {
			// Build might have not started yet, wait until cancel is
			// successful.
			require.Eventually(
				t,
				func() bool {
					return trace.Abort()
				},
				time.Minute,
				10*time.Millisecond,
			)
		}
		b.Collect(ch)
		<-ch
	}

	err = <-done
	expected := &common.BuildError{FailureReason: common.JobCanceled}
	assert.ErrorIs(t, err, expected)
}

func defaultBuild(cacheConfig *common.CacheConfig) *common.Build {
	return &common.Build{
		JobResponse: common.JobResponse{
			JobInfo: common.JobInfo{
				ProjectID: 10,
			},
			RunnerInfo: common.RunnerInfo{
				Timeout: 3600,
			},
		},
		Runner: &common.RunnerConfig{
			RunnerCredentials: common.RunnerCredentials{
				Token: "longtoken",
			},
			RunnerSettings: common.RunnerSettings{
				Cache: cacheConfig,
			},
		},
	}
}

func TestGetCredentials(t *testing.T) {
	tests := map[string]struct {
		s3            *common.CacheS3Config
		expectedError string
		credsExpected bool
	}{
		"static credentials": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
				SecretKey:  "somesecret",
			},
			credsExpected: true,
		},
		"no S3 credentials": {
			expectedError: `missing S3 configuration`,
		},
		"empty access and secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
			},
			credsExpected: false,
		},
		"empty access key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				SecretKey:  "somesecret",
			},
			credsExpected: false,
		},
		"empty secret key": {
			s3: &common.CacheS3Config{
				BucketName: bucketName,
				AccessKey:  "somekey",
			},
			credsExpected: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			config := &common.CacheConfig{S3: tt.s3}
			adapter, err := NewS3CredentialsAdapter(config)

			if tt.expectedError != "" {
				require.EqualError(t, err, tt.expectedError)
			} else {
				require.NoError(t, err)

				creds := adapter.GetCredentials()

				if tt.credsExpected {
					assert.Equal(t, 2, len(creds))
					assert.Equal(t, tt.s3.AccessKey, creds["AWS_ACCESS_KEY_ID"])
					assert.Equal(t, tt.s3.SecretKey, creds["AWS_SECRET_ACCESS_KEY"])
				} else {
					assert.Empty(t, creds)
				}
			}
		})
	}
}

func TestConfigTemplate_MergeTo(t *testing.T) {
	tests := map[string]struct {
		templateContent string
		config          *common.RunnerConfig

		expectedError       error
		assertConfiguration func(t *testing.T, config *common.RunnerConfig)
	}{
		"invalid template file": {
			templateContent: configTemplateMergeToInvalidConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("couldn't load configuration template file: decoding configuration file: toml: line 1: expected '.' or '=', but got ',' instead"),
		},
		"no runners in template": {
			templateContent: configTemplateMergeToEmptyConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("configuration template must contain exactly one [[runners]] entry"),
		},
		"multiple runners in template": {
			templateContent: configTemplateMergeToTwoRunnerSectionsConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("configuration template must contain exactly one [[runners]] entry"),
		},
		"template doesn't overwrite existing settings": {
			templateContent: configTemplateMergeToOverwritingConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				assert.Equal(t, configTemplateMergeToBaseConfiguration.Token, config.RunnerCredentials.Token)
				assert.Equal(t, configTemplateMergeToBaseConfiguration.Executor, config.RunnerSettings.Executor)
				assert.Equal(t, 100, config.Limit)
			},
			expectedError: nil,
		},
		"template doesn't overwrite token if none provided in base": {
			templateContent: configTemplateMergeToOverwritingConfiguration,
			config:          &common.RunnerConfig{},
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				assert.Equal(t, "", config.Token)
			},
		},
		"template adds additional content": {
			templateContent: configTemplateMergeToAdditionalConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				k8s := config.RunnerSettings.Kubernetes

				require.NotNil(t, k8s)
				require.NotEmpty(t, k8s.Volumes.EmptyDirs)
				assert.Len(t, k8s.Volumes.EmptyDirs, 1)

				emptyDir := k8s.Volumes.EmptyDirs[0]
				assert.Equal(t, "empty_dir", emptyDir.Name)
				assert.Equal(t, "/path/to/empty_dir", emptyDir.MountPath)
				assert.Equal(t, "Memory", emptyDir.Medium)
				assert.Equal(t, "1G", emptyDir.SizeLimit)
			},
			expectedError: nil,
		},
		"error on merging": {
			templateContent: configTemplateMergeToAdditionalConfiguration,
			expectedError: fmt.Errorf(
				"error while merging configuration with configuration template: %w",
				mergo.ErrNotSupported,
			),
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			file, cleanup := PrepareConfigurationTemplateFile(t, tc.templateContent)
			defer cleanup()

			configTemplate := &configTemplate{ConfigFile: file}
			err := configTemplate.MergeTo(tc.config)

			if tc.expectedError != nil {
				assert.ErrorContains(t, err, tc.expectedError.Error())

				return
			}

			assert.NoError(t, err)
			tc.assertConfiguration(t, tc.config)
		})
	}
}

func TestBuildsHelper_ListJobsHandler(t *testing.T) {
	tests := map[string]struct {
		build          *common.Build
		expectedOutput []string
	}{
		"no jobs": {
			build: nil,
		},
		"job exists": {
			build: &common.Build{
				Runner: &common.RunnerConfig{
					SystemIDState: common.NewSystemIDState(),
				},
				JobResponse: common.JobResponse{
					ID:      1,
					JobInfo: common.JobInfo{ProjectID: 1},
					GitInfo: common.GitInfo{RepoURL: "https://gitlab.example.com/my-namespace/my-project.git"},
				},
			},
			expectedOutput: []string{
				"url=https://gitlab.example.com/my-namespace/my-project/-/jobs/1",
			},
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			writer := httptest.NewRecorder()

			req, err := http.NewRequest(http.MethodGet, "/", nil)
			require.NoError(t, err)

			b := newBuildsHelper()
			b.addBuild(test.build)
			b.ListJobsHandler(writer, req)

			if test.build != nil {
				require.NoError(t, test.build.Runner.SystemIDState.EnsureSystemID())
			}

			resp := writer.Result()
			defer resp.Body.Close()

			assert.Equal(t, http.StatusOK, resp.StatusCode)
			assert.Equal(t, "2", resp.Header.Get("X-List-Version"))
			assert.Equal(t, "text/plain", resp.Header.Get(common.ContentType))

			body, err := io.ReadAll(resp.Body)
			require.NoError(t, err)

			if len(test.expectedOutput) == 0 {
				assert.Empty(t, body)
				return
			}

			for _, expectedOutput := range test.expectedOutput {
				assert.Contains(t, string(body), expectedOutput)
			}
		})
	}
}

func TestCacheOperation(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)

	tests := map[string]cacheOperationTest{
		"error-on-s3-client-initialization": {
			errorOnS3ClientInitialization: true,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning: true,
			presignedURL:         URL,
			expectedURL:          nil,
		},
		"presigned-url": {
			presignedURL: URL,
			expectedURL:  URL,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactory(),
			)
		})
	}
}

func TestAdapterOperation(t *testing.T) {
	tests := map[string]adapterOperationTestCase{
		"error-on-URL-signing": {
			returnedURL:   "",
			returnedError: fmt.Errorf("test error"),
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while generating GCS pre-signed URL: test error")
			},
			signBlobAPITest: false,
		},
		"invalid-URL-returned": {
			returnedURL:   "://test",
			returnedError: nil,
			assertErrorMessage: func(t *testing.T, message string) {
				assert.Contains(t, message, "error while parsing generated URL: parse")
				assert.Contains(t, message, "://test")
				assert.Contains(t, message, "missing protocol scheme")
			},
			signBlobAPITest: false,
		},
		"valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    false,
		},
		"sign-blob-api-valid-configuration": {
			returnedURL:        "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:      nil,
			assertErrorMessage: nil,
			signBlobAPITest:    true,
		},
		"max-cache-archive-size": {
			returnedURL:            "https://storage.googleapis.com/test/key?Expires=123456789&GoogleAccessId=test-access-id%40X.iam.gserviceaccount.com&Signature=XYZ",
			returnedError:          nil,
			assertErrorMessage:     nil,
			signBlobAPITest:        false,
			maxUploadedArchiveSize: maxUploadedArchiveSize,
			expectedHeaders:        http.Header{"X-Goog-Content-Length-Range": []string{"0,100"}},
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			config := defaultGCSCache()

			config.MaxUploadedArchiveSize = tc.maxUploadedArchiveSize

			a, err := New(config, defaultTimeout, objectName)
			require.NoError(t, err)

			adapter, ok := a.(*gcsAdapter)
			require.True(t, ok, "Adapter should be properly casted to *adapter type")

			testAdapterOperation(
				t,
				tc,
				"GetDownloadURL",
				http.MethodGet,
				"",
				adapter,
				a.GetDownloadURL,
			)
			testAdapterOperation(
				t,
				tc,
				"GetUploadURL",
				http.MethodPut,
				"application/octet-stream",
				adapter,
				a.GetUploadURL,
			)

			headers := adapter.GetUploadHeaders()
			assert.Equal(t, headers, tc.expectedHeaders)

			assert.Nil(t, adapter.GetGoCloudURL(context.Background()))
			env, err := adapter.GetUploadEnv(context.Background())
			assert.NoError(t, err)
			assert.Empty(t, env)
		})
	}
}

func TestNew(t *testing.T) {
	t.Run("no config", func(t *testing.T) {
		adapter, err := New(&common.CacheConfig{}, time.Second, "bucket")
		require.ErrorContains(t, err, "missing GCS configuration")
		require.Nil(t, adapter)
	})

	t.Run("valid", func(t *testing.T) {
		adapter, err := New(&common.CacheConfig{GCS: &common.CacheGCSConfig{}}, time.Second, "bucket")
		require.NoError(t, err)
		require.NotNil(t, adapter)
	})
}

func TestArtifactsUploaderRetry(t *testing.T) {
	OnEachZipArchiver(t, func(t *testing.T) {
		network := &testNetwork{
			uploadState: common.UploadFailed,
		}
		cmd := ArtifactsUploaderCommand{
			JobCredentials: UploaderCredentials,
			network:        network,
			fileArchiver: fileArchiver{
				Paths: []string{artifactsTestArchivedFile},
			},
		}

		writeTestFile(t, artifactsTestArchivedFile)
		defer os.Remove(artifactsTestArchivedFile)

		removeHook := helpers.MakeFatalToPanic()
		defer removeHook()

		assert.Panics(t, func() {
			cmd.Execute(nil)
		})

		assert.Equal(t, defaultTries, network.uploadCalled)
	})
}

func TestFetchCredentialsForRole(t *testing.T) {
	workingConfig := common.CacheConfig{
		S3: &common.CacheS3Config{
			AccessKey:          "test-access-key",
			SecretKey:          "test-secret-key",
			AuthenticationType: "access-key",
			BucketName:         "test-bucket",
			UploadRoleARN:      "arn:aws:iam::123456789012:role/TestRole",
		},
	}
	mockedCreds := map[string]string{
		"AWS_ACCESS_KEY_ID":     "mock-access-key",
		"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
		"AWS_SESSION_TOKEN":     "mock-session-token",
	}

	tests := map[string]struct {
		config           *common.CacheConfig
		roleARN          string
		expected         map[string]string
		errMsg           string
		expectedKms      bool
		duration         time.Duration
		expectedDuration time.Duration
	}{
		"successful fetch": {
			config:   &workingConfig,
			roleARN:  "arn:aws:iam::123456789012:role/TestRole",
			expected: mockedCreds,
		},
		"successful fetch with 12-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         12 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 12 * time.Hour,
		},
		"successful fetch with 10-minute timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         10 * time.Minute,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with 13-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         13 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with encryption": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:                 "test-access-key",
					SecretKey:                 "test-secret-key",
					AuthenticationType:        "access-key",
					BucketName:                "test-bucket",
					UploadRoleARN:             "arn:aws:iam::123456789012:role/TestRole",
					ServerSideEncryption:      "KMS",
					ServerSideEncryptionKeyID: "arn:aws:kms:us-west-2:123456789012:key/1234abcd-12ab-34cd-56ef-1234567890ab",
				},
			},
			roleARN:     "arn:aws:iam::123456789012:role/TestRole",
			expected:    mockedCreds,
			expectedKms: true,
		},
		"invalid role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
					UploadRoleARN:      "arn:aws:iam::123456789012:role/InvalidRole",
				},
			},
			roleARN: "arn:aws:iam::123456789012:role/InvalidRole",
			errMsg:  "failed to assume role",
		},
		"no role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
				},
			},
			expected: nil,
			errMsg:   "failed to assume role",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			duration := 3600
			if tt.duration > 0 {
				duration = int(tt.expectedDuration.Seconds())
			}
			// Create s3Client and point STS endpoint to it
			mockServer := httptest.NewServer(newMockSTSHandler(tt.expectedKms, duration))
			defer mockServer.Close()

			s3Client, err := newS3Client(tt.config.S3, withSTSEndpoint(mockServer.URL+"/sts"))
			require.NoError(t, err)

			creds, err := s3Client.FetchCredentialsForRole(context.Background(), tt.roleARN, bucketName, objectName, tt.duration)

			if tt.errMsg != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), tt.errMsg)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expected, creds)
			}
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"fmt"
	"log"
	"net/http"
	_ "net/http/pprof"
)

func bad1() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func bad2() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
	// ruleid:go_leak_rule-pprof-endpoint
	log.Fatal(http.ListenAndServeTLS(":8443", "cert.crt", "key.enc", nil))
}

func bad3() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func bad4() {
	http.HandleFunc("/", func(w http.ResponseWriter, _ *http.Request) {
		fmt.Fprintf(w, "Hello World!")
	})
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

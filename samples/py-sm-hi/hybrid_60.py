class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0036_collectionfield_link_variant"),
    ]

    operations = [
        migrations.CreateModel(
            name="RecordSelectorElement",
            fields=[
                (
                    "element_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="builder.element",
                    ),
                ),
                (
                    "required",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this form element is a required field.",
                    ),
                ),
                (
                    "schema_property",
                    models.CharField(
                        help_text="A multiple valued schema property to use for the data source.",
                        max_length=225,
                        null=True,
                    ),
                ),
                (
                    "items_per_page",
                    models.PositiveIntegerField(
                        default=20,
                        help_text="The amount item loaded with each page.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                1, message="Value cannot be less than 1."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "button_load_more_label",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The label of the show more button",
                    ),
                ),
                (
                    "label",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="The text label for this record selector"
                    ),
                ),
                (
                    "default_value",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="This record selector default value."
                    ),
                ),
                (
                    "placeholder",
                    baserow.core.formula.field.FormulaField(
                        default="",
                        help_text="The placeholder text which should be applied to the element.",
                    ),
                ),
                (
                    "multiple",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this record selector allows users to choose multiple values.",
                    ),
                ),
                (
                    "option_name_suffix",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The formula to generate the displayed option name suffix",
                    ),
                ),
                (
                    "data_source",
                    models.ForeignKey(
                        help_text="The data source we want to show in the element for. Only data_sources that return list are allowed.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="builder.datasource",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("builder.element",),
        ),
    ]

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0036_collectionfield_link_variant"),
    ]

    operations = [
        migrations.CreateModel(
            name="RecordSelectorElement",
            fields=[
                (
                    "element_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="builder.element",
                    ),
                ),
                (
                    "required",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this form element is a required field.",
                    ),
                ),
                (
                    "schema_property",
                    models.CharField(
                        help_text="A multiple valued schema property to use for the data source.",
                        max_length=225,
                        null=True,
                    ),
                ),
                (
                    "items_per_page",
                    models.PositiveIntegerField(
                        default=20,
                        help_text="The amount item loaded with each page.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                1, message="Value cannot be less than 1."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "button_load_more_label",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The label of the show more button",
                    ),
                ),
                (
                    "label",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="The text label for this record selector"
                    ),
                ),
                (
                    "default_value",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="This record selector default value."
                    ),
                ),
                (
                    "placeholder",
                    baserow.core.formula.field.FormulaField(
                        default="",
                        help_text="The placeholder text which should be applied to the element.",
                    ),
                ),
                (
                    "multiple",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this record selector allows users to choose multiple values.",
                    ),
                ),
                (
                    "option_name_suffix",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The formula to generate the displayed option name suffix",
                    ),
                ),
                (
                    "data_source",
                    models.ForeignKey(
                        help_text="The data source we want to show in the element for. Only data_sources that return list are allowed.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="builder.datasource",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("builder.element",),
        ),
    ]

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0016_alter_element_visibility"),
    ]

    operations = [
        migrations.CreateModel(
            name="RepeatElement",
            fields=[
                (
                    "element_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="builder.element",
                    ),
                ),
                (
                    "items_per_page",
                    models.PositiveIntegerField(
                        default=20,
                        help_text="The amount item loaded with each page.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                1, message="Value cannot be less than 1."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "data_source",
                    models.ForeignKey(
                        help_text="The data source we want to show in the element for. Only data_sources that return list are allowed.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="builder.datasource",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("builder.element",),
        ),
    ]

class RowNamesView(APIView):
    authentication_classes = APIView.authentication_classes + [TokenAuthentication]
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="table__{id}",
                location=OpenApiParameter.QUERY,
                type=OpenApiTypes.STR,
                description=(
                    "A list of comma separated row ids to query from the table with "
                    "id {id}. For example, if you "
                    "want the name of row `42` and `43` from table `28` this parameter "
                    "will be `table__28=42,43`. You can specify multiple rows for "
                    "different tables but every tables must be in the same database. "
                    "You need at least read permission on all specified tables."
                ),
            ),
        ],
        tags=["Database table rows"],
        operation_id="list_database_table_row_names",
        description=(
            "Returns the names of the given row of the given tables. The name"
            "of a row is the primary field value for this row. The result can be used"
            "for example, when you want to display the name of a linked row from "
            "another table."
        ),
        responses={
            200: row_names_response_schema,
            400: get_error_schema(
                [
                    "ERROR_USER_NOT_IN_GROUP",
                ]
            ),
            401: get_error_schema(["ERROR_NO_PERMISSION_TO_TABLE"]),
            404: get_error_schema(["ERROR_TABLE_DOES_NOT_EXIST"]),
        },
    )
    @map_exceptions(
        {
            UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP,
            TableDoesNotExist: ERROR_TABLE_DOES_NOT_EXIST,
            NoPermissionToTable: ERROR_NO_PERMISSION_TO_TABLE,
        }
    )
    def get(self, request):
        """
        Returns the names (i.e. primary field value) of specified rows of given tables.
        Can be used when you want to display a row name referenced from another table.
        """

        result = {}
        database = None
        table_handler = TableHandler()
        token_handler = TokenHandler()
        row_handler = RowHandler()

        for name, value in request.GET.items():
            if not name.startswith("table__"):
                raise QueryParameterValidationException(
                    detail='Only table Id prefixed by "table__" are allowed as '
                    "parameter.",
                    code="invalid_parameter",
                )

            try:
                table_id = int(name[7:])
            except ValueError:
                raise QueryParameterValidationException(
                    detail=(f'Failed to parse table id in "{name}".'),
                    code="invalid_table_id",
                )

            try:
                row_ids = [int(id) for id in value.split(",")]
            except ValueError:
                raise QueryParameterValidationException(
                    detail=(
                        f'Failed to parse row ids in "{value}" for '
                        f'"table__{table_id}" parameter.'
                    ),
                    code="invalid_row_ids",
                )

            table_queryset = None
            if database:
                # Once we have the database, we want only tables from the same database
                table_queryset = Table.objects.filter(database=database)

            table = table_handler.get_table(table_id, base_queryset=table_queryset)

            if not database:
                database = table.database

            CoreHandler().check_permissions(
                request.user,
                ListRowNamesDatabaseTableOperationType.type,
                workspace=database.workspace,
                context=table,
            )

            token_handler.check_table_permissions(request, "read", table, False)

            result[table_id] = row_handler.get_row_names(table, row_ids)

        return Response(result)

class EmailTesterView(APIView):
    permission_classes = (IsAdminUser,)

    @extend_schema(
        tags=["Health"],
        request=EmailTesterRequestSerializer,
        operation_id="email_tester",
        description="Sends a test email to the provided email address. Useful for "
        "testing Baserow's email configuration as errors are clearly "
        "returned.",
        responses={
            200: EmailTesterResponseSerializer,
            400: get_error_schema(["ERROR_REQUEST_BODY_VALIDATION"]),
        },
    )
    @validate_body(EmailTesterRequestSerializer, return_validated=True)
    def post(self, request: Request, data: Dict[str, Any]) -> Response:
        target_email = data["target_email"]
        try:
            HealthCheckHandler.send_test_email(target_email)
            return Response(EmailTesterResponseSerializer({"succeeded": True}).data)
        except Exception as e:
            full = traceback.format_exc()
            return Response(
                EmailTesterResponseSerializer(
                    {
                        "succeeded": False,
                        "error_type": e.__class__.__name__,
                        "error_stack": full,
                        "error": str(e),
                    }
                ).data
            )

class PublicViewInfoView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="slug",
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                required=True,
                description="The slug of the view to get public information " "about.",
            )
        ],
        tags=["Database table views"],
        operation_id="get_public_view_info",
        description=(
            "Returns the required public information to display a single "
            "shared view."
        ),
        request=None,
        responses={
            200: PublicViewInfoSerializer,
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
            401: get_error_schema(["ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW"]),
            404: get_error_schema(["ERROR_VIEW_DOES_NOT_EXIST"]),
        },
    )
    @map_exceptions(
        {
            UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP,
            ViewDoesNotExist: ERROR_VIEW_DOES_NOT_EXIST,
            NoAuthorizationToPubliclySharedView: ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW,
        }
    )
    @transaction.atomic
    def get(self, request: Request, slug: str) -> Response:
        handler = ViewHandler()
        view = handler.get_public_view_by_slug(
            request.user,
            slug,
            authorization_token=get_public_view_authorization_token(request),
        )
        view_specific = view.specific
        view_type = view_type_registry.get_by_model(view_specific)

        if not view_type.has_public_info:
            raise ViewDoesNotExist()

        field_options = view_type.get_visible_field_options_in_order(view_specific)
        fields = specific_iterator(
            Field.objects.filter(id__in=field_options.values_list("field_id"))
            .select_related("content_type")
            .prefetch_related("select_options")
        )

        return Response(
            PublicViewInfoSerializer(
                view=view,
                fields=fields,
            ).data
        )

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0016_alter_element_visibility"),
    ]

    operations = [
        migrations.CreateModel(
            name="RepeatElement",
            fields=[
                (
                    "element_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="builder.element",
                    ),
                ),
                (
                    "items_per_page",
                    models.PositiveIntegerField(
                        default=20,
                        help_text="The amount item loaded with each page.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                1, message="Value cannot be less than 1."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "data_source",
                    models.ForeignKey(
                        help_text="The data source we want to show in the element for. Only data_sources that return list are allowed.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="builder.datasource",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("builder.element",),
        ),
    ]

class AllApplicationsView(APIView):
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        tags=["Applications"],
        operation_id="list_all_applications",
        description=(
            "Lists all the applications that the authorized user has access to. The "
            "properties that belong to the application can differ per type. An "
            "application always belongs to a single workspace. All the applications of the "
            "workspaces that the user has access to are going to be listed here."
        ),
        responses={
            200: PolymorphicApplicationResponseSerializer(many=True),
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
        },
    )
    @map_exceptions({UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP})
    def get(self, request):
        """
        Responds with a list of serialized applications that belong to the user. If a
        workspace id is provided only the applications of that workspace are going to be
        returned.
        """

        workspaces = Workspace.objects.filter(users=request.user)

        # Compute list of readable application ids
        applications_ids = []
        for workspace in workspaces:
            applications = Application.objects.filter(
                workspace=workspace, workspace__trashed=False
            )
            applications = CoreHandler().filter_queryset(
                request.user,
                ListApplicationsWorkspaceOperationType.type,
                applications,
                workspace=workspace,
            )
            applications_ids += applications.values_list("id", flat=True)

        # Then filter with these ids
        applications = specific_iterator(
            Application.objects.select_related("content_type", "workspace")
            .prefetch_related("workspace__template_set")
            .filter(id__in=applications_ids)
            .order_by("workspace_id", "order"),
            per_content_type_queryset_hook=(
                lambda model, queryset: application_type_registry.get_by_model(
                    model
                ).enhance_queryset(queryset)
            ),
        )

        data = [
            PolymorphicApplicationResponseSerializer(
                application, context={"request": request}
            ).data
            for application in applications
        ]

        return Response(data)


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/wildcard-injection.py
# hash:  8eee173

import os as o
import os
import subprocess
import subprocess as subp
import sys

# Vulnerable to wildcard injection
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_exec_rule-linux-command-wildcard-injection
o.system('/bin/chown *')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Not vulnerable to wildcard injection
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen('/bin/rsync *')
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen("/bin/chmod *")
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(['/bin/chown', '*'])
# rule ok: python_exec_rule-linux-command-wildcard-injection
subp.Popen(["/bin/chmod", sys.argv[1], "*"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
# rule ok: python_exec_rule-linux-command-wildcard-injection
o.spawnvp(os.P_WAIT, 'tar', ['tar', 'xvzf', '*'])

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
options = {"stdout": subprocess.PIPE}
# rule ok: python_exec_rule-linux-command-wildcard-injection
subprocess.Popen("ls", shell=True, **options)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
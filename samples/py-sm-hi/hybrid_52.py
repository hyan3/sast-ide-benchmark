class ImageElementWorkspaceStorageUsageItem(WorkspaceStorageUsageItemType):
    type = "image_element"

    def calculate_storage_usage(self, workspace_id: int) -> UsageInMB:
        image_elements = ImageElement.objects.filter(
            page__builder__workspace_id=workspace_id,
            page__trashed=False,
            page__builder__trashed=False,
        )

        usage_in_mb = (
            UserFile.objects.filter(Q(id__in=image_elements.values("image_file")))
            .values("size")
            .aggregate(sum=Coalesce(Sum("size") / USAGE_UNIT_MB, 0))["sum"]
        )

        return usage_in_mb

class ImageElementWorkspaceStorageUsageItem(WorkspaceStorageUsageItemType):
    type = "image_element"

    def calculate_storage_usage(self, workspace_id: int) -> UsageInMB:
        image_elements = ImageElement.objects.filter(
            page__builder__workspace_id=workspace_id,
            page__trashed=False,
            page__builder__trashed=False,
        )

        usage_in_mb = (
            UserFile.objects.filter(Q(id__in=image_elements.values("image_file")))
            .values("size")
            .aggregate(sum=Coalesce(Sum("size") / USAGE_UNIT_MB, 0))["sum"]
        )

        return usage_in_mb

class EmailTesterView(APIView):
    permission_classes = (IsAdminUser,)

    @extend_schema(
        tags=["Health"],
        request=EmailTesterRequestSerializer,
        operation_id="email_tester",
        description="Sends a test email to the provided email address. Useful for "
        "testing Baserow's email configuration as errors are clearly "
        "returned.",
        responses={
            200: EmailTesterResponseSerializer,
            400: get_error_schema(["ERROR_REQUEST_BODY_VALIDATION"]),
        },
    )
    @validate_body(EmailTesterRequestSerializer, return_validated=True)
    def post(self, request: Request, data: Dict[str, Any]) -> Response:
        target_email = data["target_email"]
        try:
            HealthCheckHandler.send_test_email(target_email)
            return Response(EmailTesterResponseSerializer({"succeeded": True}).data)
        except Exception as e:
            full = traceback.format_exc()
            return Response(
                EmailTesterResponseSerializer(
                    {
                        "succeeded": False,
                        "error_type": e.__class__.__name__,
                        "error_stack": full,
                        "error": str(e),
                    }
                ).data
            )

class EmailTesterView(APIView):
    permission_classes = (IsAdminUser,)

    @extend_schema(
        tags=["Health"],
        request=EmailTesterRequestSerializer,
        operation_id="email_tester",
        description="Sends a test email to the provided email address. Useful for "
        "testing Baserow's email configuration as errors are clearly "
        "returned.",
        responses={
            200: EmailTesterResponseSerializer,
            400: get_error_schema(["ERROR_REQUEST_BODY_VALIDATION"]),
        },
    )
    @validate_body(EmailTesterRequestSerializer, return_validated=True)
    def post(self, request: Request, data: Dict[str, Any]) -> Response:
        target_email = data["target_email"]
        try:
            HealthCheckHandler.send_test_email(target_email)
            return Response(EmailTesterResponseSerializer({"succeeded": True}).data)
        except Exception as e:
            full = traceback.format_exc()
            return Response(
                EmailTesterResponseSerializer(
                    {
                        "succeeded": False,
                        "error_type": e.__class__.__name__,
                        "error_stack": full,
                        "error": str(e),
                    }
                ).data
            )

class TokenPermissionsField(serializers.Field):
    default_error_messages = {
        "invalid_key": "Only create, read, update and delete keys are allowed.",
        "invalid_value": (
            "The value must either be a bool, or a list containing database or table "
            'ids like [["database", 1], ["table", 1]].'
        ),
        "invalid_instance_type": "The instance type can only be a database or table.",
        "invalid_table_id": "The table id {instance_id} is not valid.",
        "invalid_database_id": "The database id {instance_id} is not valid.",
    }
    valid_types = ["create", "read", "update", "delete"]

    def __init__(self, **kwargs):
        kwargs["source"] = "*"
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        """
        Validates if the provided data structure is correct and replaces the database
        and table references with selected instances. The following data format is
        expected:

        {
            "create": True,
            "read": [['database', ID], ['table', ID]],
            "update": False,
            "delete": True
        }

        which will be converted to:

        {
            "create": True,
            "read": [Database(ID), Table(ID)],
            "update": False,
            "delete": True
        }

        :param data: The non validated permissions containing references to the
            database and tables.
        :type data: dict
        :return: The validated permissions with objects instead of references.
        :rtype: dict
        """

        tables = {}
        databases = {}

        if not isinstance(data, dict) or len(data) != len(self.valid_types):
            self.fail("invalid_key")

        for key, value in data.items():
            if key not in self.valid_types:
                self.fail("invalid_key")

            if not isinstance(value, bool) and not isinstance(value, list):
                self.fail("invalid_value")

            if isinstance(value, list):
                for instance in value:
                    if (
                        not isinstance(instance, list)
                        or not len(instance) == 2
                        or not isinstance(instance[0], str)
                        or not isinstance(instance[1], int)
                    ):
                        self.fail("invalid_value")

                    instance_type, instance_id = instance
                    if instance_type == "database":
                        databases[instance_id] = None
                    elif instance_type == "table":
                        tables[instance_id] = None
                    else:
                        self.fail("invalid_instance_type")

        if len(tables) > 0:
            tables = {
                table.id: table
                for table in Table.objects.filter(id__in=tables.keys()).select_related(
                    "database"
                )
            }

        if len(databases) > 0:
            databases = {
                database.id: database
                for database in Database.objects.filter(id__in=databases.keys())
            }

        for key, value in data.items():
            if isinstance(value, list):
                for index, (instance_type, instance_id) in enumerate(value):
                    if instance_type == "database":
                        if instance_id not in databases:
                            self.fail("invalid_database_id", instance_id=instance_id)
                        data[key][index] = databases[instance_id]
                    elif instance_type == "table":
                        if instance_id not in tables:
                            self.fail("invalid_table_id", instance_id=instance_id)
                        data[key][index] = tables[instance_id]

        return {
            "create": data["create"],
            "read": data["read"],
            "update": data["update"],
            "delete": data["delete"],
        }

    def to_representation(self, value):
        """
        If the provided value is a Token instance we need to fetch the permissions from
        the database and format them the correct way.

        If the provided value is a dict it means the permissions have already been
        provided and validated once, so we can just return that value. The variant is
        used when we want to validate the input.

        :param value: The prepared value that needs to be serialized.
        :type value: Token or dict
        :return: A dict containing the create, read, update and delete permissions
        :rtype: dict
        """

        if isinstance(value, Token):
            permissions = {
                "create": False,
                "read": False,
                "update": False,
                "delete": False,
            }

            for permission in value.tokenpermission_set.all():
                if permissions[permission.type] is True:
                    continue

                if permission.database_id is None and permission.table_id is None:
                    permissions[permission.type] = True
                else:
                    if not isinstance(permissions[permission.type], list):
                        permissions[permission.type] = []
                    if permission.database_id is not None:
                        permissions[permission.type].append(
                            ("database", permission.database_id)
                        )
                    elif permission.table_id is not None:
                        permissions[permission.type].append(
                            ("table", permission.table_id)
                        )

            return permissions
        else:
            permissions = {}
            for type_name in self.valid_types:
                if type_name not in value:
                    return None
                permissions[type_name] = value[type_name]
            return permissions

class AllApplicationsView(APIView):
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        tags=["Applications"],
        operation_id="list_all_applications",
        description=(
            "Lists all the applications that the authorized user has access to. The "
            "properties that belong to the application can differ per type. An "
            "application always belongs to a single workspace. All the applications of the "
            "workspaces that the user has access to are going to be listed here."
        ),
        responses={
            200: PolymorphicApplicationResponseSerializer(many=True),
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
        },
    )
    @map_exceptions({UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP})
    def get(self, request):
        """
        Responds with a list of serialized applications that belong to the user. If a
        workspace id is provided only the applications of that workspace are going to be
        returned.
        """

        workspaces = Workspace.objects.filter(users=request.user)

        # Compute list of readable application ids
        applications_ids = []
        for workspace in workspaces:
            applications = Application.objects.filter(
                workspace=workspace, workspace__trashed=False
            )
            applications = CoreHandler().filter_queryset(
                request.user,
                ListApplicationsWorkspaceOperationType.type,
                applications,
                workspace=workspace,
            )
            applications_ids += applications.values_list("id", flat=True)

        # Then filter with these ids
        applications = specific_iterator(
            Application.objects.select_related("content_type", "workspace")
            .prefetch_related("workspace__template_set")
            .filter(id__in=applications_ids)
            .order_by("workspace_id", "order"),
            per_content_type_queryset_hook=(
                lambda model, queryset: application_type_registry.get_by_model(
                    model
                ).enhance_queryset(queryset)
            ),
        )

        data = [
            PolymorphicApplicationResponseSerializer(
                application, context={"request": request}
            ).data
            for application in applications
        ]

        return Response(data)

class PublicViewInfoView(APIView):
    permission_classes = (AllowAny,)

    @extend_schema(
        parameters=[
            OpenApiParameter(
                name="slug",
                location=OpenApiParameter.PATH,
                type=OpenApiTypes.STR,
                required=True,
                description="The slug of the view to get public information " "about.",
            )
        ],
        tags=["Database table views"],
        operation_id="get_public_view_info",
        description=(
            "Returns the required public information to display a single "
            "shared view."
        ),
        request=None,
        responses={
            200: PublicViewInfoSerializer,
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
            401: get_error_schema(["ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW"]),
            404: get_error_schema(["ERROR_VIEW_DOES_NOT_EXIST"]),
        },
    )
    @map_exceptions(
        {
            UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP,
            ViewDoesNotExist: ERROR_VIEW_DOES_NOT_EXIST,
            NoAuthorizationToPubliclySharedView: ERROR_NO_AUTHORIZATION_TO_PUBLICLY_SHARED_VIEW,
        }
    )
    @transaction.atomic
    def get(self, request: Request, slug: str) -> Response:
        handler = ViewHandler()
        view = handler.get_public_view_by_slug(
            request.user,
            slug,
            authorization_token=get_public_view_authorization_token(request),
        )
        view_specific = view.specific
        view_type = view_type_registry.get_by_model(view_specific)

        if not view_type.has_public_info:
            raise ViewDoesNotExist()

        field_options = view_type.get_visible_field_options_in_order(view_specific)
        fields = specific_iterator(
            Field.objects.filter(id__in=field_options.values_list("field_id"))
            .select_related("content_type")
            .prefetch_related("select_options")
        )

        return Response(
            PublicViewInfoSerializer(
                view=view,
                fields=fields,
            ).data
        )

class TokenPermissionsField(serializers.Field):
    default_error_messages = {
        "invalid_key": "Only create, read, update and delete keys are allowed.",
        "invalid_value": (
            "The value must either be a bool, or a list containing database or table "
            'ids like [["database", 1], ["table", 1]].'
        ),
        "invalid_instance_type": "The instance type can only be a database or table.",
        "invalid_table_id": "The table id {instance_id} is not valid.",
        "invalid_database_id": "The database id {instance_id} is not valid.",
    }
    valid_types = ["create", "read", "update", "delete"]

    def __init__(self, **kwargs):
        kwargs["source"] = "*"
        super().__init__(**kwargs)

    def to_internal_value(self, data):
        """
        Validates if the provided data structure is correct and replaces the database
        and table references with selected instances. The following data format is
        expected:

        {
            "create": True,
            "read": [['database', ID], ['table', ID]],
            "update": False,
            "delete": True
        }

        which will be converted to:

        {
            "create": True,
            "read": [Database(ID), Table(ID)],
            "update": False,
            "delete": True
        }

        :param data: The non validated permissions containing references to the
            database and tables.
        :type data: dict
        :return: The validated permissions with objects instead of references.
        :rtype: dict
        """

        tables = {}
        databases = {}

        if not isinstance(data, dict) or len(data) != len(self.valid_types):
            self.fail("invalid_key")

        for key, value in data.items():
            if key not in self.valid_types:
                self.fail("invalid_key")

            if not isinstance(value, bool) and not isinstance(value, list):
                self.fail("invalid_value")

            if isinstance(value, list):
                for instance in value:
                    if (
                        not isinstance(instance, list)
                        or not len(instance) == 2
                        or not isinstance(instance[0], str)
                        or not isinstance(instance[1], int)
                    ):
                        self.fail("invalid_value")

                    instance_type, instance_id = instance
                    if instance_type == "database":
                        databases[instance_id] = None
                    elif instance_type == "table":
                        tables[instance_id] = None
                    else:
                        self.fail("invalid_instance_type")

        if len(tables) > 0:
            tables = {
                table.id: table
                for table in Table.objects.filter(id__in=tables.keys()).select_related(
                    "database"
                )
            }

        if len(databases) > 0:
            databases = {
                database.id: database
                for database in Database.objects.filter(id__in=databases.keys())
            }

        for key, value in data.items():
            if isinstance(value, list):
                for index, (instance_type, instance_id) in enumerate(value):
                    if instance_type == "database":
                        if instance_id not in databases:
                            self.fail("invalid_database_id", instance_id=instance_id)
                        data[key][index] = databases[instance_id]
                    elif instance_type == "table":
                        if instance_id not in tables:
                            self.fail("invalid_table_id", instance_id=instance_id)
                        data[key][index] = tables[instance_id]

        return {
            "create": data["create"],
            "read": data["read"],
            "update": data["update"],
            "delete": data["delete"],
        }

    def to_representation(self, value):
        """
        If the provided value is a Token instance we need to fetch the permissions from
        the database and format them the correct way.

        If the provided value is a dict it means the permissions have already been
        provided and validated once, so we can just return that value. The variant is
        used when we want to validate the input.

        :param value: The prepared value that needs to be serialized.
        :type value: Token or dict
        :return: A dict containing the create, read, update and delete permissions
        :rtype: dict
        """

        if isinstance(value, Token):
            permissions = {
                "create": False,
                "read": False,
                "update": False,
                "delete": False,
            }

            for permission in value.tokenpermission_set.all():
                if permissions[permission.type] is True:
                    continue

                if permission.database_id is None and permission.table_id is None:
                    permissions[permission.type] = True
                else:
                    if not isinstance(permissions[permission.type], list):
                        permissions[permission.type] = []
                    if permission.database_id is not None:
                        permissions[permission.type].append(
                            ("database", permission.database_id)
                        )
                    elif permission.table_id is not None:
                        permissions[permission.type].append(
                            ("table", permission.table_id)
                        )

            return permissions
        else:
            permissions = {}
            for type_name in self.valid_types:
                if type_name not in value:
                    return None
                permissions[type_name] = value[type_name]
            return permissions


# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data8 = UsmUserData(username, authKey=None, authProtocol=usmNoAuthProtocol, privKey=None, privProtocol=usmNoPrivProtocol)
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
class ChoiceElementType(FormElementTypeMixin, ElementType):
    type = "choice"
    model_class = ChoiceElement
    allowed_fields = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    request_serializer_field_names = [
        "label",
        "default_value",
        "required",
        "placeholder",
        "options",
        "multiple",
        "show_as_dropdown",
        "option_type",
        "formula_value",
        "formula_name",
    ]
    simple_formula_fields = [
        "label",
        "default_value",
        "placeholder",
        "formula_value",
        "formula_name",
    ]

    class SerializedDict(ElementDict):
        label: BaserowFormula
        required: bool
        placeholder: BaserowFormula
        default_value: BaserowFormula
        options: List
        multiple: bool
        show_as_dropdown: bool
        option_type: str
        formula_value: BaserowFormula
        formula_name: BaserowFormula

    @property
    def serializer_field_overrides(self):
        from baserow.contrib.builder.api.theme.serializers import (
            DynamicConfigBlockSerializer,
        )
        from baserow.contrib.builder.theme.theme_config_block_types import (
            InputThemeConfigBlockType,
        )
        from baserow.core.formula.serializers import FormulaSerializerField

        overrides = {
            "label": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("label").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "default_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("default_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "required": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("required").help_text,
                default=False,
                required=False,
            ),
            "placeholder": serializers.CharField(
                help_text=ChoiceElement._meta.get_field("placeholder").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "options": ChoiceOptionSerializer(
                source="choiceelementoption_set", many=True, required=False
            ),
            "multiple": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("multiple").help_text,
                default=False,
                required=False,
            ),
            "show_as_dropdown": serializers.BooleanField(
                help_text=ChoiceElement._meta.get_field("show_as_dropdown").help_text,
                default=True,
                required=False,
            ),
            "option_type": serializers.ChoiceField(
                choices=ChoiceElement.OPTION_TYPE.choices,
                help_text=ChoiceElement._meta.get_field("option_type").help_text,
                required=False,
                default=ChoiceElement.OPTION_TYPE.MANUAL,
            ),
            "formula_value": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_value").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "formula_name": FormulaSerializerField(
                help_text=ChoiceElement._meta.get_field("formula_name").help_text,
                required=False,
                allow_blank=True,
                default="",
            ),
            "styles": DynamicConfigBlockSerializer(
                required=False,
                property_name="input",
                theme_config_block_type_name=InputThemeConfigBlockType.type,
                serializer_kwargs={"required": False},
            ),
        }

        return overrides

    @property
    def request_serializer_field_overrides(self):
        return {
            **self.serializer_field_overrides,
            "options": ChoiceOptionSerializer(many=True, required=False),
        }

    def serialize_property(
        self,
        element: ChoiceElement,
        prop_name: str,
        files_zip=None,
        storage=None,
        cache=None,
    ):
        if prop_name == "options":
            return [
                self.serialize_option(option)
                for option in element.choiceelementoption_set.all()
            ]

        return super().serialize_property(
            element, prop_name, files_zip=files_zip, storage=storage, cache=cache
        )

    def import_serialized(
        self,
        parent: Any,
        serialized_values: Dict[str, Any],
        id_mapping: Dict[str, Dict[int, int]],
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        choice_element = super().import_serialized(
            parent,
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

        options = []
        for option in serialized_values.get("options", []):
            option["choice_id"] = choice_element.id
            option_deserialized = self.deserialize_option(option)
            options.append(option_deserialized)

        ChoiceElementOption.objects.bulk_create(options)

        return choice_element

    def create_instance_from_serialized(
        self,
        serialized_values: Dict[str, Any],
        id_mapping,
        files_zip=None,
        storage=None,
        cache=None,
        **kwargs,
    ) -> T:
        serialized_values.pop("options", None)
        return super().create_instance_from_serialized(
            serialized_values,
            id_mapping,
            files_zip=files_zip,
            storage=storage,
            cache=cache,
            **kwargs,
        )

    def serialize_option(self, option: ChoiceElementOption) -> Dict:
        return {
            "value": option.value,
            "name": option.name,
            "choice_id": option.choice_id,
        }

    def deserialize_option(self, value: Dict):
        return ChoiceElementOption(**value)

    def get_pytest_params(self, pytest_data_fixture) -> Dict[str, Any]:
        return {
            "label": "'test'",
            "default_value": "'option 1'",
            "required": False,
            "placeholder": "'some placeholder'",
            "multiple": False,
            "show_as_dropdown": True,
            "option_type": ChoiceElement.OPTION_TYPE.MANUAL,
            "formula_value": "",
            "formula_name": "",
        }

    def after_create(self, instance: ChoiceElement, values: Dict):
        options = values.get("options", [])

        ChoiceElementOption.objects.bulk_create(
            [ChoiceElementOption(choice=instance, **option) for option in options]
        )

    def after_update(
        self, instance: ChoiceElement, values: Dict, changes: Dict[str, Tuple]
    ):
        options = values.get("options", None)

        if options is not None:
            ChoiceElementOption.objects.filter(choice=instance).delete()
            ChoiceElementOption.objects.bulk_create(
                [ChoiceElementOption(choice=instance, **option) for option in options]
            )

    def is_valid(
        self,
        element: ChoiceElement,
        value: Union[List, str],
        dispatch_context: DispatchContext,
    ) -> str:
        """
        Responsible for validating `ChoiceElement` form data. We handle
        this validation a little differently to ensure that if someone creates
        an option with a blank value, it's considered valid.

        :param element: The choice element.
        :param value: The choice value we want to validate.
        :return: The value if it is valid for this element.
        """

        options_tuple = set(
            element.choiceelementoption_set.values_list("value", "name")
        )
        options = [
            value if value is not None else name for (value, name) in options_tuple
        ]

        if element.option_type == ChoiceElement.OPTION_TYPE.FORMULAS:
            options = ensure_array(
                resolve_formula(
                    element.formula_value,
                    formula_runtime_function_registry,
                    dispatch_context,
                )
            )
            options = [ensure_string_or_integer(option) for option in options]

        if element.multiple:
            try:
                value = ensure_array(value)
            except ValidationError as exc:
                raise FormDataProviderChunkInvalidException(
                    "The value must be an array or convertible to an array."
                ) from exc

            if not value:
                if element.required:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            else:
                for v in value:
                    if v not in options:
                        raise FormDataProviderChunkInvalidException(
                            f"{value} is not a valid option."
                        )
        else:
            if not value:
                if element.required and value not in options:
                    raise FormDataProviderChunkInvalidException(
                        "The value is required."
                    )
            elif value not in options:
                raise FormDataProviderChunkInvalidException(
                    f"{value} is not a valid option."
                )

        return value

class DomainHandler:
    allowed_fields_create = ["domain_name"]
    allowed_fields_update = ["domain_name", "last_published"]

    def get_domain(self, domain_id: int, base_queryset: QuerySet = None) -> Domain:
        """
        Gets a domain by ID

        :param domain_id: The ID of the domain
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :raises DomainDoesNotExist: If the domain doesn't exist
        :return: The model instance of the domain
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        try:
            return base_queryset.get(id=domain_id)
        except Domain.DoesNotExist:
            raise DomainDoesNotExist()

    def get_domains(
        self, builder: Builder, base_queryset: QuerySet = None
    ) -> Iterable[Domain]:
        """
        Gets all the domains of a builder.

        :param builder: The builder we are trying to get all domains for
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :return: An iterable of all the specific domains
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        return specific_iterator(base_queryset.filter(builder=builder))

    def get_public_builder_by_domain_name(self, domain_name: str) -> Builder:
        """
        Returns a builder given a domain name it's been published for.

        :param domain_name: The domain name we want the builder for.
        :raise BuilderDoesNotExist: When no builder is published with this domain name.
        :return: A public builder instance.
        """

        try:
            domain = (
                Domain.objects.exclude(published_to=None)
                .select_related("published_to", "builder")
                .only("published_to", "builder")
                .get(domain_name=domain_name)
            )
        except Domain.DoesNotExist:
            raise BuilderDoesNotExist()

        if TrashHandler.item_has_a_trashed_parent(domain, check_item_also=True):
            raise BuilderDoesNotExist()

        return domain.published_to

    def create_domain(
        self, domain_type: DomainType, builder: Builder, **kwargs
    ) -> Domain:
        """
        Creates a new domain

        :param domain_type: The type of domain that's being created
        :param builder: The builder the domain belongs to
        :param kwargs: Additional attributes of the domain
        :return: The newly created domain instance
        """

        last_order = Domain.get_last_order(builder)

        model_class = cast(Domain, domain_type.model_class)

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_create + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        domain = model_class(builder=builder, order=last_order, **prepared_values)
        domain.save()

        return domain

    def delete_domain(self, domain: Domain):
        """
        Deletes the domain provided

        :param domain: The domain that must be deleted
        """

        domain.delete()

    def update_domain(self, domain: Domain, **kwargs) -> Domain:
        """
        Updates fields of a domain

        :param domain: The domain that should be updated
        :param kwargs: The fields that should be updated with their corresponding value
        :return: The updated domain
        """

        domain_type = domain.get_type()

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_update + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        for key, value in prepared_values.items():
            setattr(domain, key, value)

        try:
            domain.save()
        except IntegrityError as error:
            if "unique" in str(error) and "domain_name" in prepared_values:
                raise DomainNameNotUniqueError(prepared_values["domain_name"])
            raise error

        return domain

    def order_domains(
        self, builder: Builder, order: List[int], base_qs=None
    ) -> List[int]:
        """
        Assigns a new order to the domains in a builder application.
        You can provide a base_qs for pre-filter the domains affected by this change.

        :param builder: The builder that the domains belong to
        :param order: The new order of the domains
        :param base_qs: A QS that can have filters already applied
        :raises DomainNotInBuilder: If the domain is not part of the provided builder
        :return: The new order of the domains
        """

        if base_qs is None:
            base_qs = Domain.objects.filter(builder=builder)

        try:
            full_order = Domain.order_objects(base_qs, order)
        except IdDoesNotExist as error:
            raise DomainNotInBuilder(error.not_existing_id)

        return full_order

    def publish(self, domain: Domain, progress: Progress):
        """
        Publishes a builder for the given domain object. If the builder was
        already published, the previous version is deleted and a new one is created.
        When a builder is published, a clone of the current version is created to avoid
        further modifications to the original builder affect the published version.

        :param domain: The object carrying the information for the publishing.
        :param progress: A progress object to track the publishing operation progress.
        """

        builder = domain.builder
        workspace = builder.workspace

        # Delete previously existing builder publication
        if domain.published_to:
            domain.published_to.delete()

        builder_application_type = application_type_registry.get("builder")

        import_export_config = ImportExportConfig(
            include_permission_data=True, reduce_disk_space_usage=False
        )

        default_storage = get_default_storage()

        exported_builder = builder_application_type.export_serialized(
            builder, import_export_config, None, default_storage
        )

        # Update user_source uid to have something stable per domain.
        # This is mainly because we want a token generated for one version of a site
        # to still be valid if we redeploy the website.
        exported_builder["user_sources"] = [
            {**user_source, "uid": f"domain_{domain.id}__{user_source['uid']}"}
            for user_source in exported_builder["user_sources"]
        ]

        progress.increment(by=50)

        id_mapping = {"import_workspace_id": workspace.id}
        duplicate_builder = builder_application_type.import_serialized(
            None,
            exported_builder,
            import_export_config,
            id_mapping,
            None,
            default_storage,
            progress_builder=progress.create_child_builder(represents_progress=50),
        )
        domain.published_to = duplicate_builder
        domain.last_published = datetime.now(tz=timezone.utc)
        domain.save()

        return domain

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0036_collectionfield_link_variant"),
    ]

    operations = [
        migrations.CreateModel(
            name="RecordSelectorElement",
            fields=[
                (
                    "element_ptr",
                    models.OneToOneField(
                        auto_created=True,
                        on_delete=django.db.models.deletion.CASCADE,
                        parent_link=True,
                        primary_key=True,
                        serialize=False,
                        to="builder.element",
                    ),
                ),
                (
                    "required",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this form element is a required field.",
                    ),
                ),
                (
                    "schema_property",
                    models.CharField(
                        help_text="A multiple valued schema property to use for the data source.",
                        max_length=225,
                        null=True,
                    ),
                ),
                (
                    "items_per_page",
                    models.PositiveIntegerField(
                        default=20,
                        help_text="The amount item loaded with each page.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                1, message="Value cannot be less than 1."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "button_load_more_label",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The label of the show more button",
                    ),
                ),
                (
                    "label",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="The text label for this record selector"
                    ),
                ),
                (
                    "default_value",
                    baserow.core.formula.field.FormulaField(
                        default="", help_text="This record selector default value."
                    ),
                ),
                (
                    "placeholder",
                    baserow.core.formula.field.FormulaField(
                        default="",
                        help_text="The placeholder text which should be applied to the element.",
                    ),
                ),
                (
                    "multiple",
                    models.BooleanField(
                        default=False,
                        help_text="Whether this record selector allows users to choose multiple values.",
                    ),
                ),
                (
                    "option_name_suffix",
                    baserow.core.formula.field.FormulaField(
                        blank=True,
                        default="",
                        help_text="The formula to generate the displayed option name suffix",
                    ),
                ),
                (
                    "data_source",
                    models.ForeignKey(
                        help_text="The data source we want to show in the element for. Only data_sources that return list are allowed.",
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to="builder.datasource",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
            bases=("builder.element",),
        ),
    ]


# License: MIT (c) GitLab Inc.

from pysnmp.hlapi import *


# SNMPv3 User details
username = 'myUsername'  
auth_key = 'myAuthKey'
priv_key = 'myPrivKey'


# auth MD5, privacy DES
# ok: python_snmp_rule-snmp-weak-cryptography
user_data1 = UsmUserData(username, auth_key, priv_key)
print(f"auth MD5, privacy DES : \n {user_data1}")


# auth SHA, privacy AES128
# ok: python_snmp_rule-snmp-weak-cryptography
user_data2 = UsmUserData(username, auth_key, priv_key,
                        authProtocol=usmHMACSHAAuthProtocol,
                        privProtocol=usmAesCfb128Protocol)
print("\nauth SHA, privacy AES128 : \n", user_data2)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data3)

# auth MD5, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data4)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data5)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data6)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data7)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data8)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data9)

# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data10)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data11)

# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data12)


# SHA auth, AES privacy
# ok: python_snmp_rule-snmp-weak-cryptography
user_data13 = UsmUserData(username, authKey=auth_key, authProtocol=(1,3,6,1,6,3,10,1,1,3), privKey=priv_key, privProtocol=(1,3,6,1,6,3,10,1,2,4))
print("\nSHA auth, AES privacy : \n",user_data13)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data14)


# auth MD5, no privacy
# ruleid: python_snmp_rule-snmp-weak-cryptography
user_data15 = UsmUserData(username, auth_key, None)
print("\nauth MD5, no privacy : \n",user_data15)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data16)


# auth SHA, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth SHA, no privacy : \n",user_data17)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nno auth, no privacy : \n",user_data18)


# MD5 auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nMD5 auth, no privacy : \n",user_data19)


# no auth, no privacy
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
print("\nauth MD5, no privacy : \n",user_data15)
import os
import logging

from flask import Flask, render_template, request, url_for, redirect

from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.sql import func, text, select
import sqlite3


basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)

app.logger.setLevel(logging.DEBUG)
app.logger.addHandler(logging.StreamHandler())

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir,'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)

app.app_context().push()

class User(db.Model):

    id = db.Column(
                    db.Integer,
                    primary_key=True
                )
    
    firstname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    lastname = db.Column(
                            db.String(100),
                            nullable=False
                        )
    
    email = db.Column(
                        db.String(80), 
                        unique=True, 
                        nullable=False
                    )
    
    age = db.Column(
                    db.Integer
                    )
    
    created_at = db.Column(
                            db.DateTime(timezone=True),
                           server_default=func.now()
                           )
    
    def __repr__(self):
        return f'<user {self.firstname}>'

# for reference in case the DB is populated programmatically only
# db.drop_all()
# db.create_all()

try:
    ormuser = User( firstname='ORM', lastname='user',
                    email='ormuser@example.com', age=21,
                    )
    db.session.add(ormuser)
    db.session.commit()
except Exception as e:
    app.logger.debug(f"[!] {e}")


def testcases_links():
    links = {'vulnerable':[],'safe':[]} 
    for rule in app.url_map.iter_rules():

        category = None

        if 'vulnerable' in rule.endpoint:
            category = 'vulnerable'
        elif 'safe' in rule.endpoint:
            category = 'safe'

        if category:
            url = url_for(rule.endpoint, **(rule.defaults or {}))
            links[category].append((url, rule.endpoint))
    return links

@app.route('/')
def index():
    result=[]
    result = User.query.all()
    return render_template('index.html', users=result, testcases=testcases_links())

@app.route('/vulnerable01', methods=['GET', 'POST'])
def vulnerable01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe01', methods=('GET', 'POST'))
def safe01():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result= db.session.execute(text(f'SELECT * FROM user WHERE age == 22'))
    return render_template('result.html', users=result)

@app.route('/safe02', methods=['GET', 'POST'])
def safe02():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')
    name = "john"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})
    return render_template('result.html', users=result)

def vulnerable03_helper():
    if 'param' in request.args:
        app.logger.debug("[!] name GET parameter provided")
    return request.args.get('param') 

@app.route('/vulnerable03', methods=['GET'])
def vulnerable03():
    result = []
    #todoruleid: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM user WHERE firstname = '%s'" % vulnerable03_helper()))
    return render_template('result.html', users=result)

def vulnerable04_helper(args):
    if 'param' in args:
        return args.get('param')

@app.route('/vulnerable04', methods=['GET'])
def vulnerable04():
    result = []
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe05', methods=('GET','POST'))
def safe05():
    result = []
    if request.method == 'GET':
        name = request.args.get('param','')
    elif request.method == 'POST':
        name = request.form.get('param','')

    #ok: python_flask_rule-tainted-sql-string
    query = text('SELECT * FROM user WHERE firstname = :myparam')

    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(query, {"myparam":name})

    return render_template('result.html', users=result)

def vulnerable06_helper01(query):
    #requires interprocedural capabilities to know for certain that query is an SQL query
    #todoid: python_flask_rule-tainted-sql-string
    result = db.session.execute(query)
    return result

def vulnerable06_helper02(ages):
    result=[]
    ages=list(ages)
    if len(ages):
        #requires interprocedural capabilities to know for certain that age comes from a request
        #todoid: python_flask_rule-tainted-sql-string
        result= db.session.execute(text(f'SELECT * FROM user WHERE age >= {ages[0][0]}'))
    return result

@app.route('/vulnerable06', methods=['GET', 'POST'])
def vulnerable06():
    result = []
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/vulnerable07', methods=['GET', 'POST'])
def vulnerable07():
    result=[]
    if request.method == 'GET':
        name = request.args.get('param')
    elif request.method == 'POST':
        name = request.form.get('param')

    if name:
        query_prefix = "SELECT * FROM user"
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe06', methods=['GET', 'POST'])
def safe06():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    query = query_prefix+";"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(query))
    return render_template('result.html', users=result)

@app.route('/safe07', methods=['GET', 'POST'])
def safe07():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    query_prefix = "SELECT * FROM user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text(f'{query_prefix} WHERE firstname ="john"'))
    return render_template('result.html', users=result)

@app.route('/vulnerable08', methods=['GET'])
def vulnerable08():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/safe09', methods=['GET', 'POST'])
def safe09():
    result=[]
    mytable = "user"
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text("SELECT * FROM " + mytable))
    return render_template('result.html', users=result)

@app.route('/vulnerable09', methods=['GET', 'POST'])
def vulnerable09():
    result=[]
    if request.method == 'GET':
        query = request.args.get('param')
    elif request.method == 'POST':
        query = request.form.get('param')

    if query:
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    return render_template('result.html', users=result)

@app.route('/safe10', methods=['GET', 'POST'])
def safe10():
    result=[]
    #ok: python_flask_rule-tainted-sql-string
    result = db.session.execute(text('SELECT * FROM user'))
    return render_template('result.html', users=result)

@app.get('/vulnerable11')
def vulnerable11():
    result=[]
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.route('/<int:user_id>/')
def user(user_id):
    result=[]
    result = User.query.get_or_404(user_id)
    return render_template('user.html', user=result)

@app.route('/new/', methods=['POST','GET'])
def new():
    if request.method == 'POST':
        firstname = request.form['firstname']
        lastname = request.form['lastname']
        email = request.form['email']
        age = int(request.form['age'])
        user = User(firstname=firstname,
                    lastname=lastname,
                    email=email,
                    age=age
                    )
        db.session.add(user)
        db.session.commit()
        return redirect(url_for('index'))
    else:
        return render_template('create.html')

@app.route('/<int:user_id>/edit/', methods=['POST'])
def edit(user_id):
    user = User.query.get_or_404(user_id)

    firstname = request.form['firstname']
    lastname = request.form['lastname']
    email = request.form['email']
    age = int(request.form['age'])

    user.firstname = firstname
    user.lastname = lastname
    user.email = email
    user.age = age

    db.session.add(user)
    db.session.commit()

    return redirect(url_for('index'))

@app.post('/<int:user_id>/delete/')
def delete(user_id):
    user = User.query.get_or_404(user_id)
    db.session.delete(user)
    db.session.commit()
    return redirect(url_for('index'))

def sqlite_fetch(query=None, suffix=None):
    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    if query is None:
        query ="""SELECT * FROM main.user 
        WHERE firstname LIKE 'sam'"""
    query = f"{query} {suffix}"
    cursor = cursor.execute( query, () )
    keepgoing = True
    users = []
    while keepgoing:
        try:
            row = cursor.fetchone()
            users.append(
                User(
                    id=row[0],
                    firstname=row[1],
                    lastname=row[2],
                    email=row[3],
                    age=row[4],
                )
            )
            print(row)
        except:
            keepgoing = False
    return users

@app.get('/vulnerable12')
def vulnerable12():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable13')
def vulnerable13():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    # this rule concerns itself with SQL strings
    # not with SQL driver or ORM specifics 
    #ok: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(query)
    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')

    #ok: python_flask_rule-tainted-sql-string
    result=f"""SELECT this is not SQL '{name}';"""

    #ok: python_flask_rule-tainted-sql-string
    result=f"""create this is not SQL '{name}';"""

    #todook: python_flask_rule-tainted-sql-string
    result=f"""where did '{name}' go?"""

    return render_template('result.html', users=result)

@app.get('/vulnerable14')
def vulnerable14():
    result=[]
    name = request.args.get('param')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return render_template('result.html', users=result)

@app.get('/vulnerable15')
def vulnerable15():
    result=[]
    orderby = request.args.get('param')
    query ="""SELECT * FROM main.user 
    WHERE firstname LIKE '%s%'"""

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    
    #ruleid: python_flask_rule-tainted-sql-string
    result=sqlite_fetch(None, sufix=f"ORDER BY {orderby}")

def sqlite_execute(query):

    connection = sqlite3.connect(os.path.join(basedir,'db.sqlite3'))
    cursor = connection.cursor()
    cursor = cursor.execute( query )
    connection.commit()
    cursor.close()

    return True

@app.get('/vulnerable16')
def vulnerable15():
    result=[]
    name =request.args.get('param')
    if name is None:
        name='todo'

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    sqlite_execute(query)
    
    
    return render_template('result.html', users=result)
#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

from flask import Flask, request

app = Flask(__name__)
# Literal strings are ok
com = '''
import os
os.system('ls -l')
'''


@app.route('/exec', methods=['POST'])
def exec_route():
    user_input = request.form['input']

    # ruleid: python_exec_rule-exec-used
    exec(f"{user_input}")

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route('/safe', methods=['GET'])
def exec_safe_route():
    text = 'exec is evil'
    com = f"print('{text}')"

    # rule ok:
    exec(com)

    # rule ok:
    exec(com)

    # rule ok:
    exec("2 + 3")

    # rule ok:
    exec("print('hello world!')")

    return "no response"


if __name__ == '__main__':
    app.run(debug=True)

# License: MIT (c) GitLab Inc.
import requests

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.get('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.post('https://gmail.com', timeout=5)
requests.post('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.post('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.put('https://gmail.com', timeout=5)
requests.put('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.put('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_requests_rule-request-without-timeout
requests.delete('https://gmail.com', timeout=None)
requests.delete('https://gmail.com', timeout=5)
requests.delete('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.delete('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.patch('https://gmail.com', timeout=5)
requests.patch('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.patch('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.options('https://gmail.com', timeout=5)
requests.options('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.options('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
requests.head('https://gmail.com', timeout=5)
requests.head('https://gmail.com', timeout=5, headers={'authorization': f'token 8675309'})
requests.head('https://gmail.com', headers={'authorization': f'token 8675309'}, timeout=5)


def get(url):
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


def get(url, timeout=10):
    return requests.get(url, timeout=timeout)

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/hashlib_new_insecure_functions.py
# hash:  8eee173

import hashlib

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_crypto_rule-hashlib-new-insecure-functions
hashlib.new('MD4', string='test')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Test that plugin does not flag valid hash functions.
hashlib.new('sha256')
hashlib.new('SHA512')


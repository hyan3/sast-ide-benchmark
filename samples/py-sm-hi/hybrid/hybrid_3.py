class AllApplicationsView(APIView):
    permission_classes = (IsAuthenticated,)

    @extend_schema(
        tags=["Applications"],
        operation_id="list_all_applications",
        description=(
            "Lists all the applications that the authorized user has access to. The "
            "properties that belong to the application can differ per type. An "
            "application always belongs to a single workspace. All the applications of the "
            "workspaces that the user has access to are going to be listed here."
        ),
        responses={
            200: PolymorphicApplicationResponseSerializer(many=True),
            400: get_error_schema(["ERROR_USER_NOT_IN_GROUP"]),
        },
    )
    @map_exceptions({UserNotInWorkspace: ERROR_USER_NOT_IN_GROUP})
    def get(self, request):
        """
        Responds with a list of serialized applications that belong to the user. If a
        workspace id is provided only the applications of that workspace are going to be
        returned.
        """

        workspaces = Workspace.objects.filter(users=request.user)

        # Compute list of readable application ids
        applications_ids = []
        for workspace in workspaces:
            applications = Application.objects.filter(
                workspace=workspace, workspace__trashed=False
            )
            applications = CoreHandler().filter_queryset(
                request.user,
                ListApplicationsWorkspaceOperationType.type,
                applications,
                workspace=workspace,
            )
            applications_ids += applications.values_list("id", flat=True)

        # Then filter with these ids
        applications = specific_iterator(
            Application.objects.select_related("content_type", "workspace")
            .prefetch_related("workspace__template_set")
            .filter(id__in=applications_ids)
            .order_by("workspace_id", "order"),
            per_content_type_queryset_hook=(
                lambda model, queryset: application_type_registry.get_by_model(
                    model
                ).enhance_queryset(queryset)
            ),
        )

        data = [
            PolymorphicApplicationResponseSerializer(
                application, context={"request": request}
            ).data
            for application in applications
        ]

        return Response(data)

class DomainHandler:
    allowed_fields_create = ["domain_name"]
    allowed_fields_update = ["domain_name", "last_published"]

    def get_domain(self, domain_id: int, base_queryset: QuerySet = None) -> Domain:
        """
        Gets a domain by ID

        :param domain_id: The ID of the domain
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :raises DomainDoesNotExist: If the domain doesn't exist
        :return: The model instance of the domain
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        try:
            return base_queryset.get(id=domain_id)
        except Domain.DoesNotExist:
            raise DomainDoesNotExist()

    def get_domains(
        self, builder: Builder, base_queryset: QuerySet = None
    ) -> Iterable[Domain]:
        """
        Gets all the domains of a builder.

        :param builder: The builder we are trying to get all domains for
        :param base_queryset: Can be provided to already filter or apply performance
            improvements to the queryset when it's being executed
        :return: An iterable of all the specific domains
        """

        if base_queryset is None:
            base_queryset = Domain.objects

        return specific_iterator(base_queryset.filter(builder=builder))

    def get_public_builder_by_domain_name(self, domain_name: str) -> Builder:
        """
        Returns a builder given a domain name it's been published for.

        :param domain_name: The domain name we want the builder for.
        :raise BuilderDoesNotExist: When no builder is published with this domain name.
        :return: A public builder instance.
        """

        try:
            domain = (
                Domain.objects.exclude(published_to=None)
                .select_related("published_to", "builder")
                .only("published_to", "builder")
                .get(domain_name=domain_name)
            )
        except Domain.DoesNotExist:
            raise BuilderDoesNotExist()

        if TrashHandler.item_has_a_trashed_parent(domain, check_item_also=True):
            raise BuilderDoesNotExist()

        return domain.published_to

    def create_domain(
        self, domain_type: DomainType, builder: Builder, **kwargs
    ) -> Domain:
        """
        Creates a new domain

        :param domain_type: The type of domain that's being created
        :param builder: The builder the domain belongs to
        :param kwargs: Additional attributes of the domain
        :return: The newly created domain instance
        """

        last_order = Domain.get_last_order(builder)

        model_class = cast(Domain, domain_type.model_class)

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_create + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        domain = model_class(builder=builder, order=last_order, **prepared_values)
        domain.save()

        return domain

    def delete_domain(self, domain: Domain):
        """
        Deletes the domain provided

        :param domain: The domain that must be deleted
        """

        domain.delete()

    def update_domain(self, domain: Domain, **kwargs) -> Domain:
        """
        Updates fields of a domain

        :param domain: The domain that should be updated
        :param kwargs: The fields that should be updated with their corresponding value
        :return: The updated domain
        """

        domain_type = domain.get_type()

        allowed_values = extract_allowed(
            kwargs, self.allowed_fields_update + domain_type.allowed_fields
        )

        prepared_values = domain_type.prepare_values(allowed_values)

        # Save only lower case domain
        if "domain_name" in prepared_values:
            prepared_values["domain_name"] = prepared_values["domain_name"].lower()

        for key, value in prepared_values.items():
            setattr(domain, key, value)

        try:
            domain.save()
        except IntegrityError as error:
            if "unique" in str(error) and "domain_name" in prepared_values:
                raise DomainNameNotUniqueError(prepared_values["domain_name"])
            raise error

        return domain

    def order_domains(
        self, builder: Builder, order: List[int], base_qs=None
    ) -> List[int]:
        """
        Assigns a new order to the domains in a builder application.
        You can provide a base_qs for pre-filter the domains affected by this change.

        :param builder: The builder that the domains belong to
        :param order: The new order of the domains
        :param base_qs: A QS that can have filters already applied
        :raises DomainNotInBuilder: If the domain is not part of the provided builder
        :return: The new order of the domains
        """

        if base_qs is None:
            base_qs = Domain.objects.filter(builder=builder)

        try:
            full_order = Domain.order_objects(base_qs, order)
        except IdDoesNotExist as error:
            raise DomainNotInBuilder(error.not_existing_id)

        return full_order

    def publish(self, domain: Domain, progress: Progress):
        """
        Publishes a builder for the given domain object. If the builder was
        already published, the previous version is deleted and a new one is created.
        When a builder is published, a clone of the current version is created to avoid
        further modifications to the original builder affect the published version.

        :param domain: The object carrying the information for the publishing.
        :param progress: A progress object to track the publishing operation progress.
        """

        builder = domain.builder
        workspace = builder.workspace

        # Delete previously existing builder publication
        if domain.published_to:
            domain.published_to.delete()

        builder_application_type = application_type_registry.get("builder")

        import_export_config = ImportExportConfig(
            include_permission_data=True, reduce_disk_space_usage=False
        )

        default_storage = get_default_storage()

        exported_builder = builder_application_type.export_serialized(
            builder, import_export_config, None, default_storage
        )

        # Update user_source uid to have something stable per domain.
        # This is mainly because we want a token generated for one version of a site
        # to still be valid if we redeploy the website.
        exported_builder["user_sources"] = [
            {**user_source, "uid": f"domain_{domain.id}__{user_source['uid']}"}
            for user_source in exported_builder["user_sources"]
        ]

        progress.increment(by=50)

        id_mapping = {"import_workspace_id": workspace.id}
        duplicate_builder = builder_application_type.import_serialized(
            None,
            exported_builder,
            import_export_config,
            id_mapping,
            None,
            default_storage,
            progress_builder=progress.create_child_builder(represents_progress=50),
        )
        domain.published_to = duplicate_builder
        domain.last_published = datetime.now(tz=timezone.utc)
        domain.save()

        return domain

class Migration(migrations.Migration):
    dependencies = [
        ("builder", "0025_buttonthemeconfigblock_colorthemeconfigblock_and_more"),
    ]

    operations = [
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="center",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="buttonthemeconfigblock",
            name="button_width",
            field=models.CharField(
                choices=[("auto", "Auto"), ("full", "Full")],
                default="auto",
                max_length=10,
            ),
        ),
        migrations.CreateModel(
            name="LinkThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "link_text_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "link_text_color",
                    models.CharField(
                        blank=True,
                        default="primary",
                        help_text="The text color of links",
                        max_length=20,
                    ),
                ),
                (
                    "link_hover_text_color",
                    models.CharField(
                        blank=True,
                        default="#96baf6ff",
                        help_text="The hover color of links when hovered",
                        max_length=20,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="body_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_1_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_2_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_3_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_font_size",
            field=models.SmallIntegerField(default=16),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_4_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_color",
            field=models.CharField(default="#070810ff", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_5_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_color",
            field=models.CharField(default="#202128", max_length=9),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_font_size",
            field=models.SmallIntegerField(default=14),
        ),
        migrations.AddField(
            model_name="typographythemeconfigblock",
            name="heading_6_text_alignment",
            field=models.CharField(
                choices=[("left", "Left"), ("center", "Center"), ("right", "Right")],
                default="left",
                max_length=10,
            ),
        ),
        migrations.AddField(
            model_name="tableelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.AddField(
            model_name="repeatelement",
            name="button_load_more_label",
            field=baserow.core.formula.field.FormulaField(
                blank=True,
                default="",
                help_text="The label of the show more button",
                null=True,
            ),
        ),
        migrations.CreateModel(
            name="ImageThemeConfigBlock",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "image_alignment",
                    models.CharField(
                        choices=[
                            ("left", "Left"),
                            ("center", "Center"),
                            ("right", "Right"),
                        ],
                        default="left",
                        max_length=10,
                    ),
                ),
                (
                    "image_max_width",
                    models.PositiveIntegerField(
                        default=100,
                        help_text="The max-width for this image element.",
                        validators=[
                            django.core.validators.MinValueValidator(
                                0, message="Value cannot be less than 0."
                            ),
                            django.core.validators.MaxValueValidator(
                                100, message="Value cannot be greater than 100."
                            ),
                        ],
                    ),
                ),
                (
                    "image_max_height",
                    models.PositiveIntegerField(
                        help_text="The max-height for this image element.",
                        null=True,
                        validators=[
                            django.core.validators.MinValueValidator(
                                5, message="Value cannot be less than 5."
                            ),
                            django.core.validators.MaxValueValidator(
                                3000, message="Value cannot be greater than 3000."
                            ),
                        ],
                    ),
                ),
                (
                    "image_constraint",
                    models.CharField(
                        choices=[
                            ("cover", "Cover"),
                            ("contain", "Contain"),
                            ("full-width", "Full Width"),
                        ],
                        default="contain",
                        help_text="The image constraint to apply to this image",
                        max_length=32,
                    ),
                ),
                (
                    "builder",
                    baserow.core.fields.AutoOneToOneField(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="%(class)s",
                        to="builder.builder",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.RunPython(
            migrate_element_styles, reverse_code=migrations.RunPython.noop
        ),
    ]


# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/jinja2_templating.py
# hash:  8eee173

import jinja2
from jinja2 import Environment, select_autoescape
templateLoader = jinja2.FileSystemLoader( searchpath="/" )
something = ''

Environment(loader=templateLoader, load=templateLoader, autoescape=True)
templateEnv = jinja2.Environment(autoescape=True,
        loader=templateLoader )
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_escaping_rule-jinja2-autoescape-false
Environment(loader=templateLoader,
            load=templateLoader,
            autoescape=False)

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

Environment(loader=templateLoader, autoescape=select_autoescape())

Environment(loader=templateLoader,
            autoescape=select_autoescape(['html', 'htm', 'xml']))


def fake_func():
    return 'foobar'
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# License: Commons Clause License Condition v1.0[LGPL-2.1-only]
# source: https://github.com/semgrep/semgrep-rules/blob/release/python/flask/security/open-redirect.py
from urlparse import urlparse

from flask import request, redirect
from flask import Flask, redirect, request, url_for
from werkzeug.urls import url_parse

app = Flask(__name__)


@app.route("open_redirect/")
def open_redirect():
    # ruleid: python_flask_rule-flask-open-redirect
    url = request.args.get("url")
    print("something")
    return redirect(url)


@app.route("not_open_redirect/")
def not_open_redirect():
    page = request.args.get("page")
    if page == "about":
        # ok: python_flask_rule-flask-open-redirect
        url = "/about/"
        return redirect(url)
    elif page == "test":
        # ok: python_flask_rule-flask-open-redirect
        redirect(f"{request.path}/")
    else:
        # ok: python_flask_rule-flask-open-redirect
        redirect(request.path + "?failed")


@app.route("filter")
def filter():
    # ok: python_flask_rule-flask-open-redirect
    next_page = request.args.get('next')
    if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('main.index')
    return redirect(next_page)


# cf. https://github.com/mideind/Netskrafl/blob/2e1933ad0710a4425c319fde3b92b2a70729ed80/netskrafl.py#L1712


@app.route("/userprefs", methods=["GET", "POST"])
@auth_required()
def userprefs():
    """ Handler for the user preferences page """

    user = current_user()

    uf = UserForm()
    err = dict()

    # The URL to go back to, if not main.html
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

    if request.method == "GET":
        # Entering the form for the first time: load the user data
        uf.init_from_user(user)
    elif request.method == "POST":
        # Attempting to submit modified data: retrieve it and validate
        uf.init_from_form(request.form)
        err = uf.validate()
        if not err:
            # All is fine: store the data back in the user entity
            uf.store(user)
            return redirect(from_url or url_for("main"))

    # Render the form with the current data and error messages, if any
    return render_template("userprefs.html", uf=uf, err=err, from_url=from_url)


# Positive Test Cases (Vulnerable - should trigger the rule)
@app.route("/open_redirect_brackets_positive/")
def open_redirect_brackets_positive():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return redirect(url)


@app.route("/open_redirect_function_call_positive/")
def open_redirect_function_call_negative():
    # ok: python_flask_rule-flask-open-redirect
    url = validate_url(request.args["redirect_url"])
    if url:
        return redirect(url)
    else:
        return url_for("home")


@app.route("/open_redirect_direct_assignment_positive/")
def open_redirect_direct_assignment_positive():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
    return redirect(url)


# Negative Test Cases (Safe - should not trigger the rule)
@app.route("/safe_redirect_brackets_negative/")
def safe_redirect_brackets_negative():
    # ok: python_flask_rule-flask-open-redirect
    url = validate_url(request.args.get("redirect_url", ""))
    if url:
        return redirect(url)
    else:
        return url_for("home")


@app.route("/safe_redirect_function_call_negative/")
def safe_redirect_function_call_negative():
    # ok: python_flask_rule-flask-open-redirect
    url = url_for("some_endpoint", param1="value1")
    return redirect(url)


@app.route("/safe_redirect_direct_assignment_negative/")
def safe_redirect_direct_assignment_negative():
    # ok: python_flask_rule-flask-open-redirect
    return redirect("/home")


@app.route("/open_redirect_positive/", methods=["POST"])
def open_redirect_positive():
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen


@app.route("/safe_redirect_positive/", methods=["POST"])
def safe_redirect_positive():
    # ok: python_flask_rule-flask-open-redirect
    url = request.get_data().decode('utf-8')
    if validate_url(url):  # Validate before redirect
        return redirect(url)
    else:
        return "Invalid URL"


# Placeholder Function for Validation
def validate_url(url):
    parsed_url = urlparse(url)
    if parsed_url.scheme not in ('http', 'https') or parsed_url.netloc != '':
        return None
    return url

#!/usr/bin/env python
# License: MIT (c) GitLab Inc.

import os

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_file-permissions_rule-general-bad-permission
os.chmod('/tmp/oh_hai', 0x1ff)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/weak_cryptographic_key_sizes.py
# hash:  8eee173

from cryptography.hazmat import backends
from cryptography.hazmat.primitives.asymmetric import dsa
from cryptography.hazmat.primitives.asymmetric import rsa
from Crypto.PublicKey import DSA as pycrypto_dsa
from Crypto.PublicKey import RSA as pycrypto_rsa
from Cryptodome.PublicKey import DSA as pycryptodomex_dsa
from Cryptodome.PublicKey import RSA as pycryptodomex_rsa


# Correct
dsa.generate_private_key(key_size=2048,
                         backend=backends.default_backend())
rsa.generate_private_key(public_exponent=65537,
                         key_size=2048,
                         backend=backends.default_backend())
pycrypto_dsa.generate(bits=2048)
pycrypto_rsa.generate(bits=2048)
pycryptodomex_dsa.generate(bits=2048)
pycryptodomex_rsa.generate(bits=2048)

# Also correct: without keyword args
dsa.generate_private_key(4096,
                         backends.default_backend())
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
pycrypto_dsa.generate(4096)
pycrypto_rsa.generate(4096)
pycryptodomex_dsa.generate(4096)
pycryptodomex_rsa.generate(4096)

# Incorrect: weak key sizes
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_crypto_rule-crypto-encrypt-dsa-rsa
pycrypto_rsa.generate(bits=1024)
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Also incorrect: without keyword args
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Don't crash when the size is variable
rsa.generate_private_key(public_exponent=65537,
                         key_size=some_key_size,
                         backend=backends.default_backend())

#!/usr/bin/env python
# License: Apache 2.0 (c) PyCQA
# source: https://github.com/PyCQA/bandit/blob/master/examples/hashlib_new_insecure_functions.py
# hash:  8eee173

import hashlib

# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# ruleid: python_crypto_rule-hashlib-new-insecure-functions
hashlib.new('md4', 'test')
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen
# removed by sast-ide-benchgen

# Test that plugin does not flag valid hash functions.
hashlib.new('sha256')
hashlib.new('SHA512')


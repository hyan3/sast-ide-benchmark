func TestCacheOperationEncryptionKMS(t *testing.T) {
	URL, err := url.Parse("https://s3.example.com")
	require.NoError(t, err)
	headers := http.Header{}
	headers.Add("X-Amz-Server-Side-Encryption", "aws:kms")
	headers.Add("X-Amz-Server-Side-Encryption-Aws-Kms-Key-Id", "alias/my-key")

	tests := map[string]cacheOperationTest{
		"error-on-minio-client-initialization": {
			errorOnMinioClientInitialization: true,
			expectedUploadHeaders:            nil,
		},
		"error-on-presigning-url": {
			errorOnURLPresigning:  true,
			presignedURL:          URL,
			expectedURL:           nil,
			expectedUploadHeaders: nil,
		},
		"presigned-url-kms": {
			presignedURL:          URL,
			expectedURL:           URL,
			expectedUploadHeaders: headers,
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			testCacheOperation(
				t,
				"GetDownloadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetDownloadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
			testCacheOperation(
				t,
				"GetUploadURL",
				func(adapter cache.Adapter) cache.PresignedURL { return adapter.GetUploadURL(context.Background()) },
				test,
				defaultCacheFactoryEncryptionKMS(),
			)
		})
	}
}

func TestRunnerByToken(t *testing.T) {
	examples := map[string]struct {
		runners       []*common.RunnerConfig
		runnerToken   string
		expectedIndex int
		expectedError error
	}{
		"finds runner by token": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						Token: "runner1",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						Token: "runner2",
					},
				},
			},
			runnerToken:   "runner2",
			expectedIndex: 1,
		},
		"does not find non-existent runner authentication token": {
			runners: []*common.RunnerConfig{
				{
					RunnerCredentials: common.RunnerCredentials{
						Token: "runner1",
					},
				},
				{
					RunnerCredentials: common.RunnerCredentials{
						Token: "runner2",
					},
				},
			},
			runnerToken:   "runner3",
			expectedIndex: -1,
			expectedError: fmt.Errorf("could not find a runner with the token 'runner3'"),
		},
	}

	for tn, tt := range examples {
		t.Run(tn, func(t *testing.T) {
			config := configOptions{
				config: &common.Config{
					Runners: tt.runners,
				},
			}

			runner, err := config.RunnerByToken(tt.runnerToken)
			if tt.expectedIndex == -1 {
				assert.Nil(t, runner)
			} else {
				assert.Equal(t, tt.runners[tt.expectedIndex], runner)
			}
			assert.Equal(t, tt.expectedError, err)
		})
	}
}

func TestBuildsHelperFindSessionByURL(t *testing.T) {
	sess, err := session.NewSession(nil)
	require.NoError(t, err)
	build := common.Build{
		Session: sess,
		Runner: &common.RunnerConfig{
			RunnerCredentials: common.RunnerCredentials{
				Token: "abcd1234",
			},
			SystemIDState: common.NewSystemIDState(),
		},
	}

	require.NoError(t, build.Runner.SystemIDState.EnsureSystemID())

	h := newBuildsHelper()
	h.addBuild(&build)

	foundSession := h.findSessionByURL(sess.Endpoint + "/action")
	assert.Equal(t, sess, foundSession)

	foundSession = h.findSessionByURL("/session/hash/action")
	assert.Nil(t, foundSession)
}

func (c *CacheClient) prepareTransport() {
	c.Transport = &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 10 * time.Second,
		ResponseHeaderTimeout: 30 * time.Second,
		DisableCompression:    true,
	}
}

func defaultAzureCache() *common.CacheConfig {
	return &common.CacheConfig{
		Type: "azure",
		Azure: &common.CacheAzureConfig{
			CacheAzureCredentials: common.CacheAzureCredentials{
				AccountName: accountName,
				AccountKey:  accountKey,
			},
			ContainerName: containerName,
			StorageDomain: storageDomain,
		},
	}
}

func (mr *RunCommand) processRunners(id int, stopWorker chan bool, runners chan *common.RunnerConfig) {
	mr.log().
		WithField("worker", id).
		Debugln("Starting worker")

	mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStarted).Inc()

	for mr.stopSignal == nil {
		select {
		case runner := <-runners:
			err := mr.processRunner(id, runner, runners)
			if err != nil {
				logger := mr.log().
					WithFields(logrus.Fields{
						"runner":   runner.ShortDescription(),
						"executor": runner.Executor,
					}).WithError(err)

				l, failureType := loggerAndFailureTypeFromError(logger, err)
				l("Failed to process runner")
				mr.runnerWorkerProcessingFailure.
					WithLabelValues(failureType, runner.ShortDescription(), runner.Name, runner.GetSystemID()).
					Inc()
			}

		case <-stopWorker:
			mr.log().
				WithField("worker", id).
				Debugln("Stopping worker")

			mr.runnerWorkerSlotOperations.WithLabelValues(workerSlotOperationStopped).Inc()

			return
		}
	}
	<-stopWorker
}

func TestUnregisterOnFailure(t *testing.T) {
	tests := map[string]struct {
		token                 string
		leaveRunner           bool
		registrationFails     bool
		expectsLeftRegistered bool
	}{
		"ui created runner, verification succeeds, runner left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"ui created runner, verification fails, LeaveRunner is false, runner machine is unregistered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"ui created runner, verification fails, LeaveRunner is true, runner machine left registered": {
			token:                 "glrt-test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
		"registration succeeds, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     false,
			expectsLeftRegistered: true,
		},
		"registration fails, LeaveRunner is false, runner is unregistered": {
			token:                 "test-runner-token",
			leaveRunner:           false,
			registrationFails:     true,
			expectsLeftRegistered: false,
		},
		"registration fails, LeaveRunner is true, runner left registered": {
			token:                 "test-runner-token",
			leaveRunner:           true,
			registrationFails:     true,
			expectsLeftRegistered: true,
		},
	}

	for testName, testCase := range tests {
		t.Run(testName, func(t *testing.T) {
			runnerUICreated := strings.HasPrefix(testCase.token, "glrt-")
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if runnerUICreated {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:    1,
						Token: testCase.token,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token: testCase.token,
					}).
					Once()
			}
			if !testCase.expectsLeftRegistered {
				credsMocker := mock.MatchedBy(func(credentials common.RunnerCredentials) bool {
					return credentials.Token == testCase.token
				})
				if runnerUICreated {
					network.On("UnregisterRunnerManager", credsMocker, mock.Anything).
						Return(true).
						Once()
				} else {
					network.On("UnregisterRunner", credsMocker).
						Return(true).
						Once()
				}
			}

			var arguments []string
			if testCase.leaveRunner {
				arguments = append(arguments, "--leave-runner")
			}

			arguments, cleanTempFile := useTempConfigFile(t, arguments)
			defer cleanTempFile()

			answers := []string{"https://gitlab.com/", testCase.token, "description"}
			if !runnerUICreated {
				answers = append(answers, "", "")
			}
			if testCase.registrationFails {
				defer func() { _ = recover() }()
			} else {
				answers = append(answers, "custom") // should not result in more answers required
			}
			cmd := commands.NewRegisterCommandForTest(
				bufio.NewReader(strings.NewReader(strings.Join(answers, "\n")+"\n")),
				network,
			)

			app := cli.NewApp()
			app.Commands = []cli.Command{
				{
					Name:   "register",
					Action: cmd.Execute,
					Flags:  clihelpers.GetFlagsFromStruct(cmd),
				},
			}

			err := app.Run(append([]string{"runner", "register"}, arguments...))

			assert.False(t, testCase.registrationFails)
			assert.NoError(t, err)
		})
	}
}

func TestProcessRunner_BuildLimit(t *testing.T) {
	hook, cleanup := test.NewHook()
	defer cleanup()

	logrus.SetLevel(logrus.DebugLevel)
	logrus.SetOutput(io.Discard)

	cfg := common.RunnerConfig{
		Limit:              2,
		RequestConcurrency: 10,
		RunnerSettings: common.RunnerSettings{
			Executor: "multi-runner-build-limit",
		},
		SystemIDState: common.NewSystemIDState(),
	}

	require.NoError(t, cfg.SystemIDState.EnsureSystemID())

	jobData := common.JobResponse{
		ID: 1,
		Steps: []common.Step{
			{
				Name:         "sleep",
				Script:       common.StepScript{"sleep 10"},
				Timeout:      15,
				When:         "",
				AllowFailure: false,
			},
		},
	}

	mJobTrace := common.MockJobTrace{}
	defer mJobTrace.AssertExpectations(t)
	mJobTrace.On("SetFailuresCollector", mock.Anything)
	mJobTrace.On("Write", mock.Anything).Return(0, nil)
	mJobTrace.On("IsStdout").Return(false)
	mJobTrace.On("SetCancelFunc", mock.Anything)
	mJobTrace.On("SetAbortFunc", mock.Anything)
	mJobTrace.On("SetDebugModeEnabled", mock.Anything)
	mJobTrace.On("Success").Return(nil)

	mNetwork := common.MockNetwork{}
	defer mNetwork.AssertExpectations(t)
	mNetwork.On("RequestJob", mock.Anything, mock.Anything, mock.Anything).Return(&jobData, true)
	mNetwork.On("ProcessJob", mock.Anything, mock.Anything).Return(&mJobTrace, nil)

	var runningBuilds uint32
	e := common.MockExecutor{}
	defer e.AssertExpectations(t)
	e.On("Prepare", mock.Anything, mock.Anything, mock.Anything).Return(nil)
	e.On("Cleanup").Maybe()
	e.On("Shell").Return(&common.ShellScriptInfo{Shell: "script-shell"})
	e.On("Finish", mock.Anything).Maybe()
	e.On("Run", mock.Anything).Run(func(args mock.Arguments) {
		atomic.AddUint32(&runningBuilds, 1)

		// Simulate work to fill up build queue.
		time.Sleep(1 * time.Second)
	}).Return(nil)

	p := common.MockExecutorProvider{}
	defer p.AssertExpectations(t)
	p.On("Acquire", mock.Anything).Return(nil, nil)
	p.On("Release", mock.Anything, mock.Anything).Return(nil).Maybe()
	p.On("CanCreate").Return(true).Once()
	p.On("GetDefaultShell").Return("bash").Once()
	p.On("GetFeatures", mock.Anything).Return(nil)
	p.On("Create").Return(&e)

	common.RegisterExecutorProvider("multi-runner-build-limit", &p)

	cmd := RunCommand{
		network:      &mNetwork,
		buildsHelper: newBuildsHelper(),
		configOptionsWithListenAddress: configOptionsWithListenAddress{
			configOptions: configOptions{
				config: &common.Config{
					User: "git",
				},
			},
		},
	}

	runners := make(chan *common.RunnerConfig)

	// Start 2 builds.
	wg := sync.WaitGroup{}
	wg.Add(3)
	for i := 0; i < 3; i++ {
		go func(i int) {
			defer wg.Done()

			err := cmd.processRunner(i, &cfg, runners)
			assert.NoError(t, err)
		}(i)
	}

	// Wait until at least two builds have started.
	for atomic.LoadUint32(&runningBuilds) < 2 {
		time.Sleep(10 * time.Millisecond)
	}

	// Wait for all builds to finish.
	wg.Wait()

	limitMetCount := 0
	for _, entry := range hook.AllEntries() {
		if strings.Contains(entry.Message, "runner limit met") {
			limitMetCount++
		}
	}

	assert.Equal(t, 1, limitMetCount)
}

func TestConfigTemplate_MergeTo(t *testing.T) {
	tests := map[string]struct {
		templateContent string
		config          *common.RunnerConfig

		expectedError       error
		assertConfiguration func(t *testing.T, config *common.RunnerConfig)
	}{
		"invalid template file": {
			templateContent: configTemplateMergeToInvalidConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("couldn't load configuration template file: decoding configuration file: toml: line 1: expected '.' or '=', but got ',' instead"),
		},
		"no runners in template": {
			templateContent: configTemplateMergeToEmptyConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("configuration template must contain exactly one [[runners]] entry"),
		},
		"multiple runners in template": {
			templateContent: configTemplateMergeToTwoRunnerSectionsConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			expectedError:   errors.New("configuration template must contain exactly one [[runners]] entry"),
		},
		"template doesn't overwrite existing settings": {
			templateContent: configTemplateMergeToOverwritingConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				assert.Equal(t, configTemplateMergeToBaseConfiguration.Token, config.RunnerCredentials.Token)
				assert.Equal(t, configTemplateMergeToBaseConfiguration.Executor, config.RunnerSettings.Executor)
				assert.Equal(t, 100, config.Limit)
			},
			expectedError: nil,
		},
		"template doesn't overwrite token if none provided in base": {
			templateContent: configTemplateMergeToOverwritingConfiguration,
			config:          &common.RunnerConfig{},
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				assert.Equal(t, "", config.Token)
			},
		},
		"template adds additional content": {
			templateContent: configTemplateMergeToAdditionalConfiguration,
			config:          configTemplateMergeToBaseConfiguration,
			assertConfiguration: func(t *testing.T, config *common.RunnerConfig) {
				k8s := config.RunnerSettings.Kubernetes

				require.NotNil(t, k8s)
				require.NotEmpty(t, k8s.Volumes.EmptyDirs)
				assert.Len(t, k8s.Volumes.EmptyDirs, 1)

				emptyDir := k8s.Volumes.EmptyDirs[0]
				assert.Equal(t, "empty_dir", emptyDir.Name)
				assert.Equal(t, "/path/to/empty_dir", emptyDir.MountPath)
				assert.Equal(t, "Memory", emptyDir.Medium)
				assert.Equal(t, "1G", emptyDir.SizeLimit)
			},
			expectedError: nil,
		},
		"error on merging": {
			templateContent: configTemplateMergeToAdditionalConfiguration,
			expectedError: fmt.Errorf(
				"error while merging configuration with configuration template: %w",
				mergo.ErrNotSupported,
			),
		},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			file, cleanup := PrepareConfigurationTemplateFile(t, tc.templateContent)
			defer cleanup()

			configTemplate := &configTemplate{ConfigFile: file}
			err := configTemplate.MergeTo(tc.config)

			if tc.expectedError != nil {
				assert.ErrorContains(t, err, tc.expectedError.Error())

				return
			}

			assert.NoError(t, err)
			tc.assertConfiguration(t, tc.config)
		})
	}
}

func TestCreateAdapter(t *testing.T) {
	adapterMock := new(MockAdapter)

	tests := map[string]factorizeTestCase{
		"adapter doesn't exist": {
			adapter:          nil,
			errorOnFactorize: nil,
			expectedAdapter:  nil,
			expectedError:    `cache factory not found: factory for cache adapter \"test\" was not registered`,
		},
		"adapter exists": {
			adapter:          adapterMock,
			errorOnFactorize: nil,
			expectedAdapter:  adapterMock,
			expectedError:    "",
		},
		"adapter errors on factorize": {
			adapter:          adapterMock,
			errorOnFactorize: errors.New("test error"),
			expectedAdapter:  nil,
			expectedError:    `cache adapter could not be initialized: test error`,
		},
	}

	for name, test := range tests {
		t.Run(name, func(t *testing.T) {
			cleanupFactoriesMap := prepareMockedFactoriesMap()
			defer cleanupFactoriesMap()

			adapterTypeName := "test"

			if test.adapter != nil {
				err := factories.Register(adapterTypeName, makeTestFactory(test))
				assert.NoError(t, err)
			}

			_ = factories.Register(
				"additional-adapter",
				func(config *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
					return new(MockAdapter), nil
				})

			config := &common.CacheConfig{
				Type: adapterTypeName,
			}

			adapter, err := getCreateAdapter(config, defaultTimeout, "key")

			if test.expectedError == "" {
				assert.NoError(t, err)
			} else {
				assert.Error(t, err)
			}

			assert.Equal(t, test.expectedAdapter, adapter)
		})
	}
}

func TestDoRetry(t *testing.T) {
	cases := []struct {
		name          string
		err           error
		expectedCount int
	}{
		{
			name:          "Error is of type retryableErr",
			err:           retryableErr{err: errors.New("error")},
			expectedCount: 4,
		},
		{
			name:          "Error is not type of retryableErr",
			err:           errors.New("error"),
			expectedCount: 1,
		},
		{
			name:          "Error is nil",
			err:           nil,
			expectedCount: 1,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			r := retryHelper{
				Retry: 3,
			}

			retryCount := 0
			err := r.doRetry(func(_ int) error {
				retryCount++
				return c.err
			})

			assert.Equal(t, c.err, err)
			assert.Equal(t, c.expectedCount, retryCount)
		})
	}
}

func TestArchiveUploadRedirect(t *testing.T) {
	finalRequestReceived := false

	finalServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "final", finalServerHandler(t, &finalRequestReceived)),
	)
	defer finalServer.Close()

	redirectingServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "redirection", redirectingServerHandler(finalServer.URL)),
	)
	defer redirectingServer.Close()

	cmd := &ArtifactsUploaderCommand{
		JobCredentials: common.JobCredentials{
			ID:    12345,
			Token: "token",
			URL:   redirectingServer.URL,
		},
		Name:             "artifacts",
		Format:           common.ArtifactFormatZip,
		CompressionLevel: "fastest",
		network:          network.NewGitLabClient(),
		fileArchiver: fileArchiver{
			Paths: []string{
				filepath.Join(".", "testdata", "test-artifacts"),
			},
		},
	}

	defer helpers.MakeFatalToPanic()()

	assert.NotPanics(t, func() {
		cmd.Execute(&cli.Context{})
	}, "expected command not to log fatal")

	assert.True(t, finalRequestReceived)
}

func newMockSTSHandler(expectedKms bool, expectedDurationSecs int) http.Handler {
	roleARN := "arn:aws:iam::123456789012:role/TestRole"
	expectedStatements := 1
	if expectedKms {
		expectedStatements = 2
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/sts" {
			http.NotFound(w, r)
			return
		}

		body, err := io.ReadAll(r.Body)
		if err != nil {
			http.Error(w, "Failed to read request body", http.StatusBadRequest)
			return
		}
		defer r.Body.Close()

		queryValues, err := url.ParseQuery(string(body))
		if err != nil {
			http.Error(w, "Failed to parse request body", http.StatusBadRequest)
			return
		}

		if queryValues.Get("Action") != "AssumeRole" {
			http.Error(w, "Invalid Action parameter", http.StatusBadRequest)
			return
		}

		if queryValues.Get("RoleArn") == "" {
			http.Error(w, "Missing RoleArn parameter", http.StatusBadRequest)
			return
		}

		if queryValues.Get("RoleArn") != roleARN {
			http.Error(w, "Invalid RoleArn parameter", http.StatusUnauthorized)
			return
		}

		if queryValues.Get("DurationSeconds") != fmt.Sprintf("%d", expectedDurationSecs) {
			http.Error(w, "Invalid DurationSeconds parameter", http.StatusUnauthorized)
			return
		}

		if queryValues.Get("RoleSessionName") == "" {
			http.Error(w, "Missing RoleSessionName parameter", http.StatusBadRequest)
			return
		}

		policy := queryValues.Get("Policy")
		if policy == "" {
			http.Error(w, "Missing Policy parameter", http.StatusBadRequest)
			return
		}

		var policyJSON sessionPolicy
		err = json.Unmarshal([]byte(policy), &policyJSON)
		if err != nil {
			http.Error(w, "Invalid Policy JSON", http.StatusBadRequest)
			return
		}

		if policyJSON.Statement == nil || len(policyJSON.Statement) != expectedStatements {
			http.Error(w, fmt.Sprintf("Policy must contain exactly %d Statements", expectedStatements), http.StatusBadRequest)
			return
		}

		statement := policyJSON.Statement[0]
		if statement.Action == nil || len(statement.Action) != 1 {
			http.Error(w, "Statement must contain exactly one Action", http.StatusBadRequest)
			return
		}

		if statement.Action[0] != "s3:PutObject" {
			http.Error(w, "Action should be s3:PutObject", http.StatusBadRequest)
			return
		}

		if expectedKms {
			kmsStatement := policyJSON.Statement[1]
			if kmsStatement.Action == nil || len(kmsStatement.Action) != 2 {
				http.Error(w, "KMS Statement must contain exactly two Actions", http.StatusBadRequest)
				return
			}
			if kmsStatement.Action[0] != "kms:Decrypt" || kmsStatement.Action[1] != "kms:GenerateDataKey" {
				http.Error(w, "KMS Statement Actions should be kms:Decrypt and kms:GenerateDataKey", http.StatusBadRequest)
				return
			}
		}

		if statement.Resource != fmt.Sprintf("arn:aws:s3:::%s/%s", bucketName, objectName) {
			http.Error(w, "Invalid policy statement", http.StatusBadRequest)
			return
		}

		w.Header().Set("Content-Type", "application/xml")
		w.WriteHeader(http.StatusOK)
		// See https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRole.html
		_, err = w.Write([]byte(`<?xml version="1.0" encoding="UTF-8"?>
<AssumeRoleResponse xmlns="https://sts.amazonaws.com/doc/2011-06-15/">
  <AssumeRoleResult>
    <Credentials>
      <AccessKeyId>mock-access-key</AccessKeyId>
      <SecretAccessKey>mock-secret-key</SecretAccessKey>
      <SessionToken>mock-session-token</SessionToken>
      <Expiration>` + time.Now().Add(time.Hour).Format(time.RFC3339) + `</Expiration>
    </Credentials>
    <AssumedRoleUser>
      <AssumedRoleId>AROATEST123:TestSession</AssumedRoleId>
      <Arn>arn:aws:sts::123456789012:assumed-role/TestRole/TestSession</Arn>
    </AssumedRoleUser>
  </AssumeRoleResult>
  <ResponseMetadata>
    <RequestId>c6104cbe-af31-11e0-8154-cbc7ccf896c7</RequestId>
  </ResponseMetadata>
</AssumeRoleResponse>`))
		if err != nil {
			w.WriteHeader(http.StatusExpectationFailed)
		}
	})
}

func TestFetchCredentialsForRole(t *testing.T) {
	workingConfig := common.CacheConfig{
		S3: &common.CacheS3Config{
			AccessKey:          "test-access-key",
			SecretKey:          "test-secret-key",
			AuthenticationType: "access-key",
			BucketName:         "test-bucket",
			UploadRoleARN:      "arn:aws:iam::123456789012:role/TestRole",
		},
	}
	mockedCreds := map[string]string{
		"AWS_ACCESS_KEY_ID":     "mock-access-key",
		"AWS_SECRET_ACCESS_KEY": "mock-secret-key",
		"AWS_SESSION_TOKEN":     "mock-session-token",
	}

	tests := map[string]struct {
		config           *common.CacheConfig
		roleARN          string
		expected         map[string]string
		errMsg           string
		expectedKms      bool
		duration         time.Duration
		expectedDuration time.Duration
	}{
		"successful fetch": {
			config:   &workingConfig,
			roleARN:  "arn:aws:iam::123456789012:role/TestRole",
			expected: mockedCreds,
		},
		"successful fetch with 12-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         12 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 12 * time.Hour,
		},
		"successful fetch with 10-minute timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         10 * time.Minute,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with 13-hour timeout": {
			config:           &workingConfig,
			roleARN:          "arn:aws:iam::123456789012:role/TestRole",
			duration:         13 * time.Hour,
			expected:         mockedCreds,
			expectedDuration: 1 * time.Hour,
		},
		"successful fetch with encryption": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:                 "test-access-key",
					SecretKey:                 "test-secret-key",
					AuthenticationType:        "access-key",
					BucketName:                "test-bucket",
					UploadRoleARN:             "arn:aws:iam::123456789012:role/TestRole",
					ServerSideEncryption:      "KMS",
					ServerSideEncryptionKeyID: "arn:aws:kms:us-west-2:123456789012:key/1234abcd-12ab-34cd-56ef-1234567890ab",
				},
			},
			roleARN:     "arn:aws:iam::123456789012:role/TestRole",
			expected:    mockedCreds,
			expectedKms: true,
		},
		"invalid role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
					UploadRoleARN:      "arn:aws:iam::123456789012:role/InvalidRole",
				},
			},
			roleARN: "arn:aws:iam::123456789012:role/InvalidRole",
			errMsg:  "failed to assume role",
		},
		"no role ARN": {
			config: &common.CacheConfig{
				S3: &common.CacheS3Config{
					AccessKey:          "test-access-key",
					SecretKey:          "test-secret-key",
					AuthenticationType: "access-key",
					BucketName:         bucketName,
				},
			},
			expected: nil,
			errMsg:   "failed to assume role",
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			duration := 3600
			if tt.duration > 0 {
				duration = int(tt.expectedDuration.Seconds())
			}
			// Create s3Client and point STS endpoint to it
			mockServer := httptest.NewServer(newMockSTSHandler(tt.expectedKms, duration))
			defer mockServer.Close()

			s3Client, err := newS3Client(tt.config.S3, withSTSEndpoint(mockServer.URL+"/sts"))
			require.NoError(t, err)

			creds, err := s3Client.FetchCredentialsForRole(context.Background(), tt.roleARN, bucketName, objectName, tt.duration)

			if tt.errMsg != "" {
				require.Error(t, err)
				assert.Contains(t, err.Error(), tt.errMsg)
			} else {
				require.NoError(t, err)
				assert.Equal(t, tt.expected, creds)
			}
		})
	}
}

func TestCacheUploadEnv(t *testing.T) {
	tests := map[string]struct {
		key                   string
		cacheConfig           *common.CacheConfig
		createCacheAdapter    bool
		createCacheAdapterErr error
		getUploadEnvResult    interface{}
		expectedEnvs          map[string]string
		expectedLogEntry      string
	}{
		"full map": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: map[string]string{"TEST1": "123", "TEST2": "456"},
			expectedEnvs:       map[string]string{"TEST1": "123", "TEST2": "456"},
		},
		"nil": {
			key:                "key",
			cacheConfig:        &common.CacheConfig{},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no cache config": {
			key:                "key",
			cacheConfig:        nil,
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"no key": {
			key:                "",
			cacheConfig:        &common.CacheConfig{Type: "test"},
			createCacheAdapter: true,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter not exists": {
			cacheConfig:        &common.CacheConfig{Type: "doesnt-exist"},
			createCacheAdapter: false,
			getUploadEnvResult: nil,
			expectedEnvs:       nil,
		},
		"adapter creation error": {
			cacheConfig:           &common.CacheConfig{Type: "test"},
			createCacheAdapter:    true,
			createCacheAdapterErr: errors.New("test error"),
			getUploadEnvResult:    nil,
			expectedEnvs:          nil,
			expectedLogEntry:      `Could not create cache adapter" error="test error`,
		},
	}

	for name, tc := range tests {
		t.Run(name, func(t *testing.T) {
			cacheAdapter := new(MockAdapter)
			defer cacheAdapter.AssertExpectations(t)

			logs, cleanUpHooks := hook.NewHook()
			defer cleanUpHooks()

			build := &common.Build{
				Runner: &common.RunnerConfig{
					RunnerSettings: common.RunnerSettings{
						Cache: tc.cacheConfig,
					},
				},
			}

			oldCreateAdapter := createAdapter
			createAdapter = func(_ *common.CacheConfig, timeout time.Duration, objectName string) (Adapter, error) {
				if tc.createCacheAdapter {
					return cacheAdapter, tc.createCacheAdapterErr
				}

				return nil, nil
			}
			defer func() {
				createAdapter = oldCreateAdapter
			}()

			if tc.cacheConfig != nil && tc.createCacheAdapter {
				cacheAdapter.On("GetUploadEnv", mock.Anything).Return(tc.getUploadEnvResult, nil)
			}

			envs, err := GetCacheUploadEnv(context.Background(), build, "key")
			assert.NoError(t, err)
			assert.Equal(t, tc.expectedEnvs, envs)

			if tc.expectedLogEntry != "" {
				lastLogMsg, err := logs.LastEntry().String()
				require.NoError(t, err)
				assert.Contains(t, lastLogMsg, tc.expectedLogEntry)
			}
		})
	}
}

func init() {
	common.RegisterCommand(cli.Command{
		Name:  "fleeting",
		Usage: "manage fleeting plugins",
		Flags: []cli.Flag{
			cli.StringFlag{Name: "config, c", EnvVar: "CONFIG_FILE", Value: commands.GetDefaultConfigFile()},
		},
		Subcommands: []cli.Command{
			{
				Name:   "install",
				Usage:  "install or update fleeting plugins",
				Flags:  []cli.Flag{cli.BoolFlag{Name: "upgrade"}},
				Action: install,
			},
			{
				Name:   "list",
				Usage:  "list installed plugins",
				Action: list,
			},
			{
				Name:  "login",
				Usage: "login to container registry",
				Flags: []cli.Flag{
					cli.StringFlag{Name: "username"},
					cli.StringFlag{Name: "password"},
					cli.BoolFlag{Name: "password-stdin", Usage: "take the password from stdin"},
				},
				ArgsUsage: "[server]",
				Action:    login,
			},
		},
	})
}

func runOnFakeMinioWithCredentials(t *testing.T, test minioClientInitializationTest) func() {
	oldNewMinioWithCredentials := newMinioWithIAM
	newMinioWithIAM =
		func(serverAddress, bucketLocation string) (*minio.Client, error) {
			if !test.expectedToUseIAM {
				t.Error("Should not use minio with IAM client initializator")
			}

			assert.Equal(t, "location", bucketLocation)

			if test.serverAddress == "" {
				assert.Equal(t, DefaultAWSS3Server, serverAddress)
			} else {
				assert.Equal(t, test.serverAddress, serverAddress)
			}

			if test.errorOnInitialization {
				return nil, errors.New("test error")
			}

			client, err := minio.New(serverAddress, &minio.Options{
				Creds:  credentials.NewIAM(""),
				Secure: true,
				Transport: &bucketLocationTripper{
					bucketLocation: bucketLocation,
				},
			})
			require.NoError(t, err)

			return client, nil
		}

	return func() {
		newMinioWithIAM = oldNewMinioWithCredentials
	}
}

func TestRunCommand_doJobRequest(t *testing.T) {
	returnedJob := new(common.JobResponse)

	waitForContext := func(ctx context.Context) {
		<-ctx.Done()
	}

	tests := map[string]struct {
		requestJob             func(ctx context.Context)
		passSignal             func(c *RunCommand)
		expectedContextTimeout bool
	}{
		"requestJob returns immediately": {
			requestJob:             func(_ context.Context) {},
			passSignal:             func(_ *RunCommand) {},
			expectedContextTimeout: false,
		},
		"requestJob hangs indefinitely": {
			requestJob:             waitForContext,
			passSignal:             func(_ *RunCommand) {},
			expectedContextTimeout: true,
		},
		"requestJob interrupted by interrupt signal": {
			requestJob: waitForContext,
			passSignal: func(c *RunCommand) {
				c.runInterruptSignal <- os.Interrupt
			},
			expectedContextTimeout: false,
		},
		"runFinished signal is passed": {
			requestJob: waitForContext,
			passSignal: func(c *RunCommand) {
				close(c.runFinished)
			},
			expectedContextTimeout: false,
		},
	}

	for tn, tt := range tests {
		t.Run(tn, func(t *testing.T) {
			runner := new(common.RunnerConfig)

			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			network.On("RequestJob", mock.Anything, *runner, mock.Anything).
				Run(func(args mock.Arguments) {
					ctx, ok := args.Get(0).(context.Context)
					require.True(t, ok)

					tt.requestJob(ctx)
				}).
				Return(returnedJob, true).
				Once()

			c := &RunCommand{
				network:            network,
				runInterruptSignal: make(chan os.Signal),
				runFinished:        make(chan bool),
			}

			ctx, cancelFn := context.WithTimeout(context.Background(), 1*time.Second)
			defer cancelFn()

			go tt.passSignal(c)

			job, _ := c.doJobRequest(ctx, runner, nil)

			assert.Equal(t, returnedJob, job)

			if tt.expectedContextTimeout {
				assert.ErrorIs(t, ctx.Err(), context.DeadlineExceeded)
				return
			}
			assert.NoError(t, ctx.Err())
		})
	}
}

func TestHealthCheckCommand_Execute(t *testing.T) {
	cases := []struct {
		name            string
		expectedConnect bool
		exposeHigher    bool
		exposeLower     bool
	}{
		{
			name:            "Successful connect",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Unsuccessful connect because service is down",
			expectedConnect: false,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with lower port exposed",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     true,
		},
		{
			name:            "Successful connect with both lower and higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     true,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			os.Unsetenv("SERVICE_LOWER_TCP_PORT")
			os.Unsetenv("SERVICE_HIGHER_TCP_PORT")

			// Start listening to reverse addr
			listener, err := net.Listen("tcp", "127.0.0.1:")
			require.NoError(t, err)
			defer listener.Close()

			port := listener.Addr().(*net.TCPAddr).Port

			err = os.Setenv("SERVICE_TCP_ADDR", "127.0.0.1")
			require.NoError(t, err)

			err = os.Setenv("SERVICE_TCP_PORT", strconv.Itoa(port))
			require.NoError(t, err)

			if c.exposeHigher {
				err = os.Setenv("SERVICE_HIGHER_TCP_PORT", strconv.Itoa(port+1))
				require.NoError(t, err)
			}

			if c.exposeLower {
				err = os.Setenv("SERVICE_LOWER_TCP_PORT", strconv.Itoa(port-1))
				require.NoError(t, err)
			}

			// If we don't expect to connect we close the listener.
			if !c.expectedConnect {
				listener.Close()
			}

			ctx, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
			defer cancelFn()
			done := make(chan struct{})
			go func() {
				cmd := HealthCheckCommand{ctx: ctx}
				cmd.Execute(nil)
				done <- struct{}{}
			}()

			select {
			case <-ctx.Done():
				if c.expectedConnect {
					require.Fail(t, "Timeout waiting to start service.")
				}
			case <-done:
				if !c.expectedConnect {
					require.Fail(t, "Expected to not connect to server")
				}
			}
		})
	}
}

func TestCacheArchiverUploadedSize(t *testing.T) {
	tests := map[string]struct {
		limit    int
		exceeded bool
	}{
		"no-limit":    {limit: 0, exceeded: false},
		"above-limit": {limit: 10, exceeded: true},
		"equal-limit": {limit: 22, exceeded: false},
		"below-limit": {limit: 25, exceeded: false},
	}

	for tn, tc := range tests {
		t.Run(tn, func(t *testing.T) {
			defer logrus.SetOutput(logrus.StandardLogger().Out)
			defer testHelpers.MakeFatalToPanic()()

			var buf bytes.Buffer
			logrus.SetOutput(&buf)

			ts := httptest.NewServer(http.HandlerFunc(testCacheBaseUploadHandler))
			defer ts.Close()

			defer os.Remove(cacheArchiverArchive)
			cmd := helpers.CacheArchiverCommand{
				File:                   cacheArchiverArchive,
				MaxUploadedArchiveSize: int64(tc.limit),
				URL:                    ts.URL + "/cache.zip",
				Timeout:                0,
			}
			assert.NotPanics(t, func() {
				cmd.Execute(nil)
			})

			if tc.exceeded {
				require.Contains(t, buf.String(), "too big")
			} else {
				require.NotContains(t, buf.String(), "too big")
			}
		})
	}
}

func TestHealthCheckCommand_Execute(t *testing.T) {
	cases := []struct {
		name            string
		expectedConnect bool
		exposeHigher    bool
		exposeLower     bool
	}{
		{
			name:            "Successful connect",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Unsuccessful connect because service is down",
			expectedConnect: false,
			exposeHigher:    false,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     false,
		},
		{
			name:            "Successful connect with lower port exposed",
			expectedConnect: true,
			exposeHigher:    false,
			exposeLower:     true,
		},
		{
			name:            "Successful connect with both lower and higher port exposed",
			expectedConnect: true,
			exposeHigher:    true,
			exposeLower:     true,
		},
	}

	for _, c := range cases {
		t.Run(c.name, func(t *testing.T) {
			os.Unsetenv("SERVICE_LOWER_TCP_PORT")
			os.Unsetenv("SERVICE_HIGHER_TCP_PORT")

			// Start listening to reverse addr
			listener, err := net.Listen("tcp", "127.0.0.1:")
			require.NoError(t, err)
			defer listener.Close()

			port := listener.Addr().(*net.TCPAddr).Port

			err = os.Setenv("SERVICE_TCP_ADDR", "127.0.0.1")
			require.NoError(t, err)

			err = os.Setenv("SERVICE_TCP_PORT", strconv.Itoa(port))
			require.NoError(t, err)

			if c.exposeHigher {
				err = os.Setenv("SERVICE_HIGHER_TCP_PORT", strconv.Itoa(port+1))
				require.NoError(t, err)
			}

			if c.exposeLower {
				err = os.Setenv("SERVICE_LOWER_TCP_PORT", strconv.Itoa(port-1))
				require.NoError(t, err)
			}

			// If we don't expect to connect we close the listener.
			if !c.expectedConnect {
				listener.Close()
			}

			ctx, cancelFn := context.WithTimeout(context.Background(), 4*time.Second)
			defer cancelFn()
			done := make(chan struct{})
			go func() {
				cmd := HealthCheckCommand{ctx: ctx}
				cmd.Execute(nil)
				done <- struct{}{}
			}()

			select {
			case <-ctx.Done():
				if c.expectedConnect {
					require.Fail(t, "Timeout waiting to start service.")
				}
			case <-done:
				if !c.expectedConnect {
					require.Fail(t, "Expected to not connect to server")
				}
			}
		})
	}
}

func TestArchiveUploadRedirect(t *testing.T) {
	finalRequestReceived := false

	finalServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "final", finalServerHandler(t, &finalRequestReceived)),
	)
	defer finalServer.Close()

	redirectingServer := httptest.NewServer(
		assertRequestPathAndMethod(t, "redirection", redirectingServerHandler(finalServer.URL)),
	)
	defer redirectingServer.Close()

	cmd := &ArtifactsUploaderCommand{
		JobCredentials: common.JobCredentials{
			ID:    12345,
			Token: "token",
			URL:   redirectingServer.URL,
		},
		Name:             "artifacts",
		Format:           common.ArtifactFormatZip,
		CompressionLevel: "fastest",
		network:          network.NewGitLabClient(),
		fileArchiver: fileArchiver{
			Paths: []string{
				filepath.Join(".", "testdata", "test-artifacts"),
			},
		},
	}

	defer helpers.MakeFatalToPanic()()

	assert.NotPanics(t, func() {
		cmd.Execute(&cli.Context{})
	}, "expected command not to log fatal")

	assert.True(t, finalRequestReceived)
}

func TestRegisterTokenExpiresAt(t *testing.T) {
	type testCase struct {
		token          string
		expiration     time.Time
		expectedConfig string
	}

	testCases := map[string]testCase{
		"no expiration": {
			token: "test-runner-token",
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration": {
			token:      "test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
		"no expiration with authentication token": {
			token: "glrt-test-runner-token",
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 0001-01-01T00:00:00Z`,
		},
		"token expiration with authentication token": {
			token:      "glrt-test-runner-token",
			expiration: time.Date(2594, 7, 21, 15, 42, 53, 0, time.UTC),
			expectedConfig: `token = "glrt-test-runner-token"
				token_obtained_at = %s
				token_expires_at = 2594-07-21T15:42:53Z`,
		},
	}

	for tn, tc := range testCases {
		t.Run(tn, func(t *testing.T) {
			network := new(common.MockNetwork)
			defer network.AssertExpectations(t)

			if strings.HasPrefix(tc.token, "glrt-") {
				network.On("VerifyRunner", mock.Anything, mock.MatchedBy(isValidToken)).
					Return(&common.VerifyRunnerResponse{
						ID:             12345,
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			} else {
				network.On("RegisterRunner", mock.Anything, mock.Anything).
					Return(&common.RegisterRunnerResponse{
						Token:          tc.token,
						TokenExpiresAt: tc.expiration,
					}).
					Once()
			}

			gotConfig, _, err := testRegisterCommandRun(t, network, []kv{}, "", "--registration-token", tc.token, "--name", "test-runner")
			require.NoError(t, err)

			assert.Contains(
				t, spaceReplacer.Replace(gotConfig),
				spaceReplacer.Replace(fmt.Sprintf(tc.expectedConfig, commands.RegisterTimeNowDate.Format(time.RFC3339))),
			)
		})
	}
}


// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

package main

import (
	"context"
	"database/sql"
	"fmt"
	"strings"
)

func dbUnsafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	unsafe1 := "select * from foo where bar = '" + tainted + "'"
	unsafe2 := "select * from foo where id = " + tainted
	unsafe3 := fmt.Sprintf("select * from foo where bar = %q", tainted)

	unsafe4 := strings.Builder{}
	unsafe4.WriteString("select * from foo where bar = ")
	unsafe4.WriteString(tainted)

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	// ruleid:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = '"+tainted+"'")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

func dbSafe(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = '" + "safe" + "'"
	safe2 := "select * from foo where id = " + "safe"
	safe3 := fmt.Sprintf("select * from foo where bar = %q", "safe")

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe3)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = '" + "safe" + "'")
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES('"+"safe"+"')")
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = '" + "safe" + "' WHERE id = 100")
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = '"+"safe"+"'")
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values('" + "safe" + "')")
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = '"+"safe"+"' where id = 100")
}

func dbSafe2(tainted string) {
	db, err := sql.Open("sqlite3", ":memory:")
	if err != nil {
		panic(err)
	}

	ctx := context.Background()

	safe1 := "select * from foo where bar = ?"
	safe2 := "select * from foo where bar is $1 and baz = $2"

	// ok:go_sql_rule-concat-sqli
	db.Exec(safe1, tainted)
	// ok:go_sql_rule-concat-sqli
	db.Exec(safe2, true, tainted)

	// ok:go_sql_rule-concat-sqli
	db.Exec("SELECT * FROM foo WHERE bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.ExecContext(ctx, "INSERT INTO foo VALUES(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.Query("UPDATE foo SET bar = ? WHERE id = 100", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryContext(ctx, "select * from foo where bar = ?", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRow("insert into foo values(?)", tainted)
	// ok:go_sql_rule-concat-sqli
	db.QueryRowContext(ctx, "update foo set bar = $2 and id = $1", "baz", tainted)
}

// False-positive reported by customer (see gitlab-org/gitlab#451108).
func safeIssue_451108(ctx context.Context, val string) error {
	if err := somepkg.validate(val); err != nil {
		return &somepkg.Error{Message: err.Error()}
	}

	if !updateAllowed(somepkg.Status) {
		// ok:go_sql_rule-concat-sqli
		return &somepkg.Error{Message: fmt.Sprintf("update is not permitted: %v", somepkg.Status)}
	}

	return nil
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", getParam(r), nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
	// ruleid:go_injection_rule-ssrf
	connection, err := ftp.Dial(url)
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

// License: Apache 2.0 (c) gosec
// source: https://github.com/securego/gosec/blob/master/testutils/source.go
// hash: bfb0f42

// Input from the std in is considered insecure
package main

import (
	"bufio"
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"net/smtp"
	"net/url"
	"os"
	"time"

	"github.com/go-ldap/ldap"
	"github.com/hashicorp/go-retryablehttp"
	"github.com/jlaffaye/ftp"
)

const defaultURL = "http://localhost:8080/ping"

func getParam(r *http.Request) string {
	param := r.URL.Query().Get("param")
	if param == "" {
		param = defaultURL
	}
	return param
}

func vulnerable0001(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0002(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0003(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprint(w, string(data))
}

func vulnerable0004(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0005(w http.ResponseWriter, r *http.Request) {
	// ruleid:go_injection_rule-ssrf
	response, err := http.Get(getParam(r))
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0006(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func vulnerable0007(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0008(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0009(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0010(w http.ResponseWriter, r *http.Request) {
	os.Setenv("TAINTED_URL", "http://example.com")
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

var UnsafeUrl = defaultURL

func vulnerable0011_helper() string {
	UnsafeUrl := UnsafeUrl
	response, err := http.Get(UnsafeUrl)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0011(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0011_helper())
}

func vulnerable0012(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}
func vulnerable0013() {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()
	body, err := io.ReadAll(resp.Body)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s", body)
}

func vulnerable0014_helper() string {
	url := os.Getenv("TAINTED_URL")
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0014(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0014_helper())
}

func vulnerable0015_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0015(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0015_helper())
}

func vulnerable0016(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ftp.localhost:21"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = connection.NoOp()
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, data)
}

func vulnerable0017(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "ldaps://ldap.localhost:636"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data string
	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0018(w http.ResponseWriter, r *http.Request) {
	url := getParam(r)
	if url == "" {
		url = "mail.ietf.org:25"
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	err = client.Noop()
	var data string

	if err != nil {
		log.Fatal(err)
	} else {
		data = "success"
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0019_helper() string {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	return string(data)
}

func vulnerable0019(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, vulnerable0019_helper())
}

func vulnerable0020(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func vulnerable0021(w http.ResponseWriter, r *http.Request) {
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type JsonPayload struct {
	URL string `json:"url"`
}

func vulnerable0022(w http.ResponseWriter, r *http.Request) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Fatal(err)
		return
	}

	var payload JsonPayload
	err = json.Unmarshal(body, &payload)
	if err != nil {
		log.Fatal(err)
		return
	}

	// need pro engine in order to use propagators 
	http.Get(payload.URL)
}

func vulnerable0023(w http.ResponseWriter, r *http.Request) {
	var payload JsonPayload
	decoder := json.NewDecoder(r.Body)
	decoder.Decode(&payload)

	// need pro engine in order to use propagators 
	resp, err := http.Get(payload.URL)
	if err != nil {
		log.Fatal(err)
		return
	}
	defer resp.Body.Close()
}

func vulnerable0024(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()

// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		log.Fatal(err)
		return
	}

	client.Do(req)
}

func vulnerable0025(w http.ResponseWriter, r *http.Request) {
	url := os.Getenv("TAINTED_URL")

	client := retryablehttp.NewClient()
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
}

var safeUrl01 string = "http://localhost:8080/ping"

func safe0001(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl01)
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	fmt.Fprintf(w, string(body))
}

func safe0002() string {
	in := bufio.NewReader(os.Stdin)
	url, err := in.ReadString('\n')
	if err != nil {
		panic(err)
	}
// removed by sast-ide-benchgen
// removed by sast-ide-benchgen
	if err != nil {
		panic(err)
	}
	defer response.Body.Close()
	body, err := io.ReadAll(response.Body)
	if err != nil {
		panic(err)
	}
	return string(body)
}

func safe0003(w http.ResponseWriter, r *http.Request) {
	url1 := "test"
	var url2 string = "http://127.0.0.1:8080/ping"
	url2 = url1
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url2)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0004(w http.ResponseWriter, r *http.Request) {
	url := safeUrl01
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

const safeUrl02 = "http://localhost:8080/ping"

func safe0005(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0006(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Head(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0007(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.Post(safeUrl02, "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0008(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.DefaultClient.PostForm(safeUrl02, nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0009(w http.ResponseWriter, r *http.Request) {
	var q = []byte(`your query`)
	// ok:go_injection_rule-ssrf
	req, _ := http.NewRequest("POST", safeUrl02, bytes.NewBuffer(q))
	req.Header.Set("X-Custom-Header", "myvalue")
	req.Header.Set("Content-Type", "text/plain")

	client := &http.Client{}
	response, err := client.Do(req)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0010(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0011(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequest("POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0012(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	request, err := http.NewRequestWithContext(context.TODO(), "POST", "http://localhost:8080/ping", nil)
	response, err := http.DefaultClient.Do(request)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))

}

func safe0013(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Post("http://localhost:8080/ping", "image/jpeg", nil)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0014(w http.ResponseWriter, r *http.Request) {
	// ok:go_injection_rule-ssrf
	response, err := http.Get(safeUrl02)

	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

func safe0015(w http.ResponseWriter, r *http.Request) {
	var url string = "http://127.0.0.1:8080/ping"
	// ok:go_injection_rule-ssrf
	response, err := http.Get(url)
	var data []byte
	if err != nil {
		log.Fatal(err)
	} else {
		data, _ = httputil.DumpResponse(response, true)
	}
	fmt.Fprintf(w, string(data))
}

type IndexParams struct {
	Title string
	Time  string
}

func ping(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "pong")
}

func index(w http.ResponseWriter, r *http.Request) {

	templates := template.Must(
		template.ParseFiles("templates/index.html"),
	)

	indexParams := IndexParams{
		"GitLab Vulnerable MRE",
		time.Now().Format(time.Stamp),
	}
	if title := r.FormValue("title"); title != "" {
		indexParams.Title = title
	}
	if err := templates.ExecuteTemplate(w, "index.html", indexParams); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func main() {

	http.Handle(
		"/assets/",
		http.StripPrefix(
			"/assets/",
			http.FileServer(http.Dir("assets")),
		),
	)

	http.HandleFunc("/", index)
	http.HandleFunc("/ping", ping)

	os.Setenv("TAINTED_URL", "http://example.com")

	http.HandleFunc("/vulnerable0001", vulnerable0001)
	http.HandleFunc("/vulnerable0002", vulnerable0002)
	http.HandleFunc("/vulnerable0003", vulnerable0003)
	http.HandleFunc("/vulnerable0004", vulnerable0004)
	http.HandleFunc("/vulnerable0005", vulnerable0005)
	http.HandleFunc("/vulnerable0006", vulnerable0006)
	http.HandleFunc("/vulnerable0007", vulnerable0007)
	http.HandleFunc("/vulnerable0008", vulnerable0008)
	http.HandleFunc("/vulnerable0009", vulnerable0009)
	http.HandleFunc("/vulnerable0010", vulnerable0010)
	http.HandleFunc("/vulnerable0011", vulnerable0011)
	http.HandleFunc("/vulnerable0012", vulnerable0012)

	http.HandleFunc("/vulnerable0014", vulnerable0014)
	http.HandleFunc("/vulnerable0015", vulnerable0015)
	http.HandleFunc("/vulnerable0016", vulnerable0016)
	http.HandleFunc("/vulnerable0017", vulnerable0017)
	http.HandleFunc("/vulnerable0018", vulnerable0018)
	http.HandleFunc("/vulnerable0019", vulnerable0019)
	http.HandleFunc("/vulnerable0020", vulnerable0020)
	http.HandleFunc("/vulnerable0021", vulnerable0021)
	http.HandleFunc("/vulnerable0022", vulnerable0022)
	http.HandleFunc("/vulnerable0023", vulnerable0023)
	http.HandleFunc("/vulnerable0024", vulnerable0024)

	http.HandleFunc("/safe0001", safe0001)

	http.HandleFunc("/safe0003", safe0003)
	http.HandleFunc("/safe0004", safe0004)
	http.HandleFunc("/safe0005", safe0005)
	http.HandleFunc("/safe0006", safe0006)
	http.HandleFunc("/safe0007", safe0007)
	http.HandleFunc("/safe0008", safe0008)
	http.HandleFunc("/safe0009", safe0009)
	http.HandleFunc("/safe0010", safe0010)
	http.HandleFunc("/safe0011", safe0011)
	http.HandleFunc("/safe0012", safe0012)
	http.HandleFunc("/safe0013", safe0013)
	http.HandleFunc("/safe0014", safe0014)
	http.HandleFunc("/safe0015", safe0015)

	fmt.Println(
		http.ListenAndServe(":8080", nil),
	)
}

# sast-ide-benchmark

This is the benchmark and testing framework for [Static Analysis: real-time IDE SAST](https://gitlab.com/groups/gitlab-org/-/epics/13753).

The benchmark programs in this repo are automatically generated by [a grammar-based test generator](https://gitlab.com/gitlab-org/secure/static-analysis/sast-ide-benchgen).

## How to configure & run locally

run the following in your terminal.

```sh
./python/run_benchmarks.py --host "$HOST" --test-dir "$TEST_DIR" --test-max "$TEST_MAX" --token "$SEMGREP_SERVICE_TOKEN"
```

in which `HOST` is the server IP, `TEST_DIR` and `TEST_MAX` are the number and the directory of the test programs, respectively, and `SEMGREP_SERVICE_TOKEN` needs to be provided for the `sast service`. (For use in CI, `SEMGREP_SERVICE_TOKEN` has already been set in `CICD` variables, so users do not need to configure it.)

Include `--keep-temp-file` if you want to [keep the raw results](python/run_benchmarks.py).

- ## the results

  For local benchmarking, the time and the size of files will be output in the console, e.g.,

  ```text
  ========Overall==============
  Average time taken per file: 5.00 seconds
  Average size of files: 3012 bytes
  ```

  If `--keep-temp-file` is used, you can find the raw scanning results in files named like `result-xxx.json` where `xxx` is the name of the test program.

For more usage details, please refer to the [doc](/docs/benchmark.md)

## Existing samples
The existing samples can be found in [./samples](/samples), 
featuring a variety of languages, code sizes, and vulnerability counts. 
These samples can be utilized in the [CI template](/ci/ci-templates/benchmark.yml#L3).
# Troubleshooting

## [Deployment](benchmark.md#deployment) failures

If resources from a previous run haven't be destroyed, the `terraforma apply` can fail with the following log output:

```log
google_compute_firewall.allow_sast_srv-us-central1-a: Creating...
google_compute_instance.us-central1-a: Creating...
╷
│ Error: Error creating instance: googleapi: Error 409: The resource 'projects/dev-sast-ide-bce06de6/zones/us-central1-a/instances/us-central1-a' already exists, alreadyExists
│ 
│   with google_compute_instance.us-central1-a,
│   on vm.tf line 5, in resource "google_compute_instance" "us-central1-a":
│    5: resource "google_compute_instance" "us-central1-a" {
│ 
╵
╷
│ Error: Error creating Firewall: googleapi: Error 409: The resource 'projects/dev-sast-ide-bce06de6/global/firewalls/allow-sast-srv-us-central1-a' already exists, alreadyExists
│ 
│   with google_compute_firewall.allow_sast_srv-us-central1-a,
│   on vm.tf line 30, in resource "google_compute_firewall" "allow_sast_srv-us-central1-a":
│   30: resource "google_compute_firewall" "allow_sast_srv-us-central1-a" {
│ 
╵
```

To manually delete resources,

1. Visit [our project in Google Cloud Console](https://console.cloud.google.com/welcome?project=dev-sast-ide-bce06de6)
1. delete VMs by navigating to [`Comptue Engine -> VM instances`](https://console.cloud.google.com/compute/instances?project=dev-sast-ide-bce06de6)
1. delete firewall rules by navigating to [`VPC network -> Firewall`](https://console.cloud.google.com/net-security/firewall-manager/firewall-policies/list?project=dev-sast-ide-bce06de6).  Note: only delete the VM rules, those starting `allow_sast_srv`.

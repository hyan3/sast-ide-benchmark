terraform {
  required_version = "~> 1.4"
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 4.55.0"
    }
  }
}

# Setting up service OIDC service account
resource "google_service_account" "oidc_sa" {
  # create account in project, see https://registry.terraform.io/providers/hashicorp/google/latest/docs/resources/google_service_account#project
  project      = var.project_id
  account_id   = "benchmark-oidc-sa"
  display_name = "Service Account for OIDC."
}

# Granting permissions to OIDC service account
# You can change or add roles in for_each section
resource "google_project_iam_member" "oidc_sa_permissions" {
  for_each = toset([
    # needed by gl_oidc, see https://cloud.google.com/secret-manager/docs/access-control#secretmanager.secretAccessor
    "roles/secretmanager.secretAccessor",
    # needed to manage VMs, see https://cloud.google.com/compute/docs/access/iam#compute.admin
    "roles/compute.admin",
  ])
  role    = each.value
  member  = google_service_account.oidc_sa.member
  project = var.project_id
}

# Calling OIDC module
module "gl_oidc" {
  source            = "gitlab.com/gitlab-com/gcp-oidc/google"
  version           = "3.0.0"
  google_project_id = var.project_id
  gitlab_project_id = var.gitlab_project_id
  oidc_service_account = {
    "sa" = {
      sa_email  = google_service_account.oidc_sa.email
      attribute = "attribute.project_id/${var.gitlab_project_id}"
    }
  }
}

### Bootstrap OIDC

To configure [workflow identity federation](https://cloud.google.com/iam/docs/workload-identity-federation) between [GitLab OIDC](https://docs.gitlab.com/ee/integration/openid_connect_provider.html) and a GCP project service account, follow these steps.


- Authenticate with Google cloud in order to use the Google terraform provider, see https://registry.terraform.io/providers/hashicorp/google/latest/docs/guides/provider_reference#authentication
- Create `Service Account` and `Workload Identity Pool` automatically by running terraform scripts in your terminal (based on https://gitlab.com/gitlab-com/gl-security/security-operations/infrastructure-security-public/oidc-modules/-/blob/main/samples/terraform/oidc-gcp/main.tf?ref_type=heads) 
  - run the terraform command: `terraform plan --var-file=terraform.tfvars -out "tfplan.out"`
  - run the terraform command: `terraform apply "tfplan.out"`
- Enable GCP APIs for you GCP project (omit this step if they are already enabled.)
  - In the GCP console, enable `IAM Service Account Credentials API`
    - Navigate to `APIs & Services` -> `Enable APIs & services` -> `+ENABLE APIS AND SERVICES`
    - Enable `IAM Service Account Credentials API`
- Enable access control by adding `CEL conditions` in the GCP console 
  - Navigate to `IAM & Admin` -> `Workload Identity Federation`
  - Edit the provider, update `condition CEL` with `attribute.project_id==<your GitLab project id> && (assertion.user_access_level == "maintainer" || assertion.user_access_level == "owner")`
- Retrieve the OIDC settings from the GCP console for later use with the OIDC module in CI.
  - Retrieve the `WI_POOL_PROVIDER`
    - Navigate to `IAM & Admin` -> `Workload Identity Federation`
      - Find the pool the terraform scripts just created (e.g., `gitlab-jwt-58434107`), click the `Edit` button next to it 
      - The `WI_POOL_PROVIDER` is located in `Default audience`
  - Retrieve the `SERVICE_ACCOUNT`
    - Navigate to `IAM & Admin` -> `Service Accounts`, you will find the `Service Account` the terraform scripts just created.

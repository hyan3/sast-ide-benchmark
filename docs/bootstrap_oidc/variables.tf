variable "project_id" {
  type        = string
  description = "Project ID of the GCP Project"
}

variable "gitlab_project_id" {
  type        = number
  description = "Project ID of the Gitlab Project"
}
